<?php
/**
 * AsalaeCore\Cron\LiberSign
 */

namespace AsalaeCore\Cron;

use AsalaeCore\Console\ConsoleLogTrait;
use Cake\Cache\Cache;
use Cake\Console\ConsoleOutput as CakeConsoleOutput;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\Http\Client;
use Datacompressor\Utility\DataCompressor;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;
use Phar;
use PharData;

/**
 * Met à jour LiberSign
 *
 * @category Shell\Cron
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class LiberSign implements CronInterface
{
    const DEFAULT_UPDATE_URL = 'https://libersign.libriciel.fr/libersign2_PROD.tgz';
    const DEFAULT_UPDATE_MD5 = 'https://libersign.libriciel.fr/libersign2_PROD.md5sum';
    const DEFAULT_TARGET_DIR = WWW_ROOT . 'libersign' . DS . 'update';

    /**
     * Traits
     */
    use ConsoleLogTrait;

    /**
     * @var string
     */
    private $proxyHost;

    /**
     * @var string
     */
    private $proxyUsername;

    /**
     * @var string
     */
    private $proxyPassword;

    /**
     * @var string
     */
    private $serverUrl;

    /**
     * @var string
     */
    private $serverMd5;

    /**
     * @var Client
     */
    public $client;
    /**
     * @var mixed|string
     */
    private $targetDir;

    /**
     * Constructeur de la classe du cron
     * @param array                  $params paramètres additionnels du
     *                                       cron
     * @param CakeConsoleOutput|null $out
     * @param CakeConsoleOutput|null $err
     */
    public function __construct(
        array $params = [],
        $out = null,
        $err = null
    ) {
        $this->client = new Client;
        $this->initializeOutput($out, $err);
        $params += [
            'update_url' => null,
            'update_md5' => null,
            'target_dir' => null,
        ];
        $this->serverUrl = $params['update_url'] ?: self::DEFAULT_UPDATE_URL;
        $this->serverMd5 = $params['update_md5'] ?: self::DEFAULT_UPDATE_MD5;
        $this->targetDir = $params['target_dir'] ?: self::DEFAULT_TARGET_DIR;
        if (!empty($params['use_proxy'])) {
            $conf = Configure::read('Proxy', [])
                + [
                    'host' => 'localhost',
                    'port' => null,
                    'username' => null,
                    'password' => null,
                ];
            $this->proxyHost = $conf['host'].($conf['port'] ? ':'.$conf['port'] : '');
            $this->proxyUsername = $conf['username'] ?: '';
            $this->proxyPassword = $conf['password'] ?: '';
        }
    }

    /**
     * @inheritDoc
     */
    public static function getVirtualFields(): array
    {
        return [
            'use_proxy' =>  [
                'type' => 'checkbox',
                'label' => __("Utiliser le proxy configuré (Configuration/Utilitaires)"),
                'default' => false,
            ],
            'update_url' => [
                'type' => 'url',
                'label' => 'update_url',
                'placeholder' => self::DEFAULT_UPDATE_URL,
                'required' => false,
            ],
            'update_md5' => [
                'type' => 'url',
                'label' => 'update_md5',
                'placeholder' => self::DEFAULT_UPDATE_MD5,
                'required' => false,
            ]
        ];
    }

    /**
     * Effectue le travail
     * @param EntityInterface|null $exec
     * @param EntityInterface|null $cron
     * @return string state success|warning|error
     * @throws Exception
     */
    public function work(EntityInterface $exec = null, EntityInterface $cron = null): string
    {
        $params = [];
        if ($this->proxyHost) {
            $params['proxy'] = ['proxy' => $this->proxyHost];
            if ($this->proxyUsername) {
                $params['proxy']['username'] = $this->proxyUsername;
                $params['proxy']['password'] = $this->proxyPassword;
            }
        }

        $this->out(__("Mise à jour de LiberSign, Récupération du numéro de version..."));
        $response = $this->client->head($this->serverUrl, [], $params + ['curl' => [CURLOPT_NOBODY => true]]);
        if (!$response->isOk()) {
            $this->out(
                '<error>'.__(
                    "Erreur {0} lors de la mise à jour de LiberSign",
                    $response->getStatusCode()
                ).'</error>'
            );
            return 'fail';
        }
        $etag = $response->getHeader('Etag')[0] ?? '';
        $cache = Cache::read('LiberSign.version');
        if ($etag === $cache) {
            $this->out('<success>'.__("LiberSign est déjà à la dernière version.").'</success>');
            return 'success';
        }

        $this->out(__("Téléchargement de la signature de fichier..."));
        $response = $this->client->get($this->serverMd5, [], $params);
        if (!$response->isOk()) {
            $this->out(
                '<error>'.__(
                    "Erreur {0} lors du téléchargement de la signature de LiberSign",
                    $response->getStatusCode()
                ).'</error>'
            );
            return 'fail';
        }
        $md5 = trim((string)$response->getBody());

        $this->out(__("Téléchargement du fichier de mise à jour..."));
        $response = $this->client->get($this->serverUrl, [], $params);
        if (!$response->isOk()) {
            $this->out(
                '<error>'.__(
                    "Erreur {0} lors du téléchargement de la mise à jour de LiberSign",
                    $response->getStatusCode()
                ).'</error>'
            );
            return 'fail';
        }
        $bin = (string)$response->getBody();
        $this->out(__("Comparaison de signature..."));
        if (md5($bin) !== $md5) { // NOSONAR md5 suffisant
            $this->out(
                '<error>'.__("La empreinte du fichier n'est pas celle attendue.").'</error>'
            );
            return 'fail';
        }
        $this->updateLiberSign($bin);
        $this->out('<success>'.__("Mise à jour effectué avec succès").'</success>');

        Cache::write('LiberSign.version', $etag);
        return "success";
    }

    /**
     * Met à jour les fichiers LiberSign à partir du binaire de réponse HTTP (tgz)
     * @param string $bin
     * @throws Exception
     */
    private function updateLiberSign(string $bin)
    {
        $tgz = sys_get_temp_dir().DS.uniqid('update-libersign-').'.tgz';
        $uncompressDir = sys_get_temp_dir().DS.'update-libersign';
        Filesystem::dumpFile($tgz, $bin);
        $phar = new PharData($tgz);

        // Note: un dossier nommé "." à la racine du tgz fait tout planter, seule solution trouvée, convertir en zip
        // @link https://stackoverflow.com/questions/38843938/phardata-extractto-method-failed-to-extract-tar-gz-on-linux-environment
        $zipname = $phar->convertToData(Phar::ZIP)->getPath();
        $files = DataCompressor::uncompress($zipname, $uncompressDir);
        $baseDirLen = strlen($uncompressDir);
        foreach ([$tgz, $zipname, $this->targetDir] as $filename) {
            if (file_exists($filename)) {
                Filesystem::remove($filename);
            }
        }
        $json = [];
        foreach ($files as $filename) {
            $jsonName = ltrim(substr($filename, $baseDirLen), DS);
            $json[$jsonName] = hash('md5', $filename); // NOSONAR
            $relativeName = substr($filename, $baseDirLen).'.b64';
            $base64 = base64_encode(file_get_contents($filename));
            Filesystem::dumpFile($this->targetDir.$relativeName, $base64);
            $this->out(__("Fichier écrit: {0}", h($this->targetDir.$relativeName)));
        }
        $jsonLocation = dirname($this->targetDir).DS.'update.json';
        Filesystem::dumpFile($jsonLocation, json_encode($json));
        $this->out(__("Fichier écrit: {0}", h($jsonLocation)));
    }
}
