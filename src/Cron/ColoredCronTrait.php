<?php
/**
 * AsalaeCore\Cron\ColoredCronTrait
 * @noinspection RedundantSuppression
 */

namespace AsalaeCore\Cron;

use AsalaeCore\Console\ConsoleLogTrait;

/**
 * Affichage de la couleur pour les crons (terminal)
 *
 * @category Shell\Cron
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin ConsoleLogTrait
 */
trait ColoredCronTrait
{

    /**
     * stderr ou stdout de $message selon $success
     * @param bool   $success
     * @param string $message
     * @noinspection PhpUnusedPrivateMethodInspection
     */
    private function o(bool $success, string $message)
    {
        if ($success) {
            $this->out($message . ' <success>OK</success>');
        } else {
            $this->out($message . ' <error>Fail</error>');
        }
    }

    /**
     * Affichage réduit du rapport pour la sortie console
     * @param string $report
     * @return string
     */
    public static function formatForConsole(string $report): string
    {
        $lines = array_filter(
            explode("\n", $report),
            function ($l) {
                return !preg_match('/console success/', $l);
            }
        );

        $colors = [
            'error',
            'warning',
        ];
        $search = ['</span>'];
        $replace = [''];
        foreach ($colors as $color) {
            $search[] = '<span class="console '.$color.'">';
            $replace[] = ': ';
        }
        return str_replace($search, $replace, implode("\n", $lines));
    }
}
