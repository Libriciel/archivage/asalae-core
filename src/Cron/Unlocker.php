<?php
/**
 * AsalaeCore\Cron\Unlocker
 */

namespace AsalaeCore\Cron;

use AsalaeCore\Console\ConsoleLogTrait;
use Cake\Console\ConsoleOutput;
use Cake\Datasource\EntityInterface;
use Cake\I18n\FrozenTime as Time;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\Stub\ConsoleOutput as TestConsoleOutput;
use DateInterval;
use DateTime;
use Exception;

/**
 * Cron qui permet de dévérouiller les crons buggés
 *
 * @category Shell\Cron
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Unlocker implements CronInterface
{
    /**
     * Traits
     */
    use ConsoleLogTrait;

    /**
     * @var DateInterval durée à partir de laquelle il faut déverrouiller un cron
     */
    public $unlockInterval;

    /**
     * Constructeur de la classe du cron
     * @param array                                $params paramètres additionnels
     *                                                     du cron
     * @param ConsoleOutput|TestConsoleOutput|null $out
     * @param ConsoleOutput|TestConsoleOutput|null $err
     * @throws Exception
     */
    public function __construct(
        array $params = [],
        $out = null,
        $err = null
    ) {
        $this->unlockInterval = new DateInterval($params['unlockInterval'] ?? 'PT1H');
        $this->initializeOutput($out, $err);
    }

    /**
     * @inheritDoc
     */
    public static function getVirtualFields(): array
    {
        return [];
    }

    /**
     * Effectue le travail
     * @param EntityInterface|null $exec
     * @param EntityInterface|null $cron
     * @return string state success|warning|error
     * @throws Exception
     */
    public function work(EntityInterface $exec = null, EntityInterface $cron = null): string
    {
        $noCronUnlocked = true;
        $Crons = TableRegistry::getTableLocator()->get('Crons');
        $CronExecutions = TableRegistry::getTableLocator()->get('CronExecutions');
        $baseUnlockInterval = $this->unlockInterval;
        $now = new DateTime;
        $state = 'success';
        /** @var EntityInterface $cronLocked */
        foreach ($Crons->find()->where(['Crons.locked' => true]) as $cronLocked) {
            /**
             * Cas pid non trouvé
             */
            /** @var EntityInterface $execLocked */
            $execLocked = $cronLocked->get('lastexec');
            if ($execLocked && $execLocked->get('pid') && !file_exists('/proc/'.$execLocked->get('pid'))) {
                $this->unlock(
                    $cronLocked,
                    $execLocked,
                    __(
                        "Le cron ''{0}'' a été déverrouillé car son processus a cesser de fonctionner",
                        $cronLocked->get('name')
                    )
                );
                $noCronUnlocked = false;
                continue;
            }

            /**
             * Cas execution trop longue
             */
            $this->unlockInterval = $cronLocked->get('max_duration')
                ? new DateInterval($cronLocked->get('max_duration'))
                : $baseUnlockInterval;
            if ($execLocked) {
                /** @var Time $last */
                $last = $execLocked->get('last_update') ?: $execLocked->get('date_begin');
            } else {
                $cronLocked->set('locked', false);
                $Crons->saveOrFail($cronLocked);
                $noCronUnlocked = false;
                $this->out(__("Le cron ''{0}'' a été vérrouillé sans raisons", $cronLocked->get('name')));
                continue;
            }
            if ($last->add($this->unlockInterval) < $now) {
                $s = (new DateTime('1970-01-01 00:00:00Z'))->add($this->unlockInterval)->getTimestamp();
                $msg = __(
                    "Le cron ''{0}'' a dépassé sa durée d'exécution maximum ({1}). Retrait du verrou.",
                    $cronLocked->get('name'),
                    sprintf(
                        '%02d:%02d:%02d',
                        $h = floor($s / 3600),
                        ($m = floor(($s - ($h * 3600)) / 60)),
                        ($s - ($h * 3600) - ($m * 60))
                    )
                );
                $this->unlock($cronLocked, $execLocked, $msg);
                $noCronUnlocked = false;
            }
        }

        /**
         * cas des exécutions non terminées alors que le cron n'est plus verrouillé
         */
        $cronexecutions = $CronExecutions->find()
            ->contain(['Crons'])
            ->where(
                [
                    'Crons.locked' => false,
                    'CronExecutions.date_end IS' => null
                ]
            );
        /** @var EntityInterface $cronexecution */
        foreach ($cronexecutions as $cronexecution) {
            /** @var Time $last */
            $last = $cronexecution->get('last_update') ?: $cronexecution->get('date_begin');
            if ($last->add($this->unlockInterval) < $now) {
                $s = (new DateTime('1970-01-01 00:00:00Z'))->add($this->unlockInterval)->getTimestamp();
                $msg = __(
                    "L'exécution {0} du cron non verrouillé ''{1}'' a dépassé sa durée"
                    ." d'exécution maximum ({2}) et est annulée.",
                    $cronexecution->get('id'),
                    $cronexecution->get('cron')->get('name'),
                    sprintf(
                        '%02d:%02d:%02d',
                        $h = floor($s / 3600),
                        ($m = floor(($s - ($h * 3600)) / 60)),
                        ($s - ($h * 3600) - ($m * 60))
                    )
                );
                $this->out('<warning>'.$msg.'</warning>');
                /** @noinspection PhpConditionAlreadyCheckedInspection faux positif */
                if ($state !== 'error') {
                    $state = 'warning';
                }
                $cronexecution->set('date_end', new \DateTime);
                $cronexecution->set('state', 'warning');
                $cronexecution->set(
                    'report',
                    $cronexecution->get('report').PHP_EOL.'<span class="console warning">'.$msg.'</span>'
                );
                $CronExecutions->saveOrFail($cronexecution);
                $noCronUnlocked = false;
            }
        }

        if ($noCronUnlocked) {
            $this->out(__('Aucun cron verrouillé'));
        }
        return $state;
    }

    /**
     * Retire le verrou d'un cron
     * @param EntityInterface $cronLocked
     * @param EntityInterface $execLocked
     * @param string          $msg
     * @throws Exception
     */
    private function unlock(EntityInterface $cronLocked, EntityInterface $execLocked, string $msg)
    {
        $Crons = TableRegistry::getTableLocator()->get('Crons');
        $CronExecutions = TableRegistry::getTableLocator()->get('CronExecutions');
        $this->out('<error>'.$msg.'</error>');

        $report = $execLocked->get('report').PHP_EOL.'<span class="console warning">'.$msg.'</span>';
        $execLocked->set('date_end', new \DateTime);
        $execLocked->set('report', $report);
        $execLocked->set('state', 'error');
        $CronExecutions->saveOrFail($execLocked);
        if ($cronLocked->get('frequency') === 'one_shot') {
            $CronExecutions->updateAll(['cron_id' => null], ['cron_id' => $cronLocked->get('id')]);
            $Crons->delete($cronLocked);
            return;
        }

        $cronLocked->set('locked', false);
        if (!$this->setNext($cronLocked)) {
            $report .= PHP_EOL.'<span class="console danger">'
                .__("Erreur sur l'interval, impossible de relancer le cron")
                .'</span>';
        }

        $execLocked->set('date_end', new \DateTime);
        $execLocked->set('state', 'error');
        $execLocked->set('report', $report);
        $Crons->saveOrFail($cronLocked);
        $CronExecutions->saveOrFail($execLocked);
    }

    /**
     * Défini la prochaine execution du cron
     * @param EntityInterface $cronLocked
     * @return bool
     * @throws Exception
     */
    private function setNext(EntityInterface $cronLocked): bool
    {
        $now = new DateTime;
        try {
            $interval = new DateInterval($cronLocked->get('frequency'));
        } catch (Exception $e) {
            return false;
        }
        /** @var \DateTime $next */
        $next = $cronLocked->get('next');
        $prev = clone $next;
        while ($next < $now) {
            $next = $next->add($interval);
            if ($next <= $prev) {
                break;
            }
        }
        $cronLocked->set('next', $next);
        return true;
    }
}
