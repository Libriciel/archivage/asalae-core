<?php
/**
 * AsalaeCore\Cron\CronInterface
 */

namespace AsalaeCore\Cron;

use Cake\Console\ConsoleOutput as CakeConsoleOutput;
use Cake\Datasource\EntityInterface;
use Cake\TestSuite\Stub\ConsoleOutput as TestConsoleOutput;

/**
 * Interface des crons
 *
 * @category Shell\Cron
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
interface CronInterface
{
    /**
     * Constructeur de la classe du cron
     * @param array                                    $params paramètres additionnels
     *                                                         du cron
     * @param CakeConsoleOutput|TestConsoleOutput|null $out
     * @param CakeConsoleOutput|TestConsoleOutput|null $err
     */
    public function __construct(
        array $params = [],
        $out = null,
        $err = null
    );

    /**
     * Liste des champs virtuels pour un cron donné, permet de générer le formulaire
     * À l'exception des checkbox, l'option 'required' est mise à true par défaut
     *
     * Exemple :
     * [
     *      'myField' => ['type' => 'date', 'label' => '...', 'default' => '', ...] // formControl
     * ]
     * @return array
     */
    public static function getVirtualFields(): array;

    /**
     * Effectue le travail
     * @param EntityInterface|null $exec
     * @param EntityInterface|null $cron
     * @return string state success|warning|error
     */
    public function work(EntityInterface $exec = null, EntityInterface $cron = null): string;

    /**
     * Permet d'obtenir les messages de sorti
     * @return string
     */
    public function getOutput(): string;

    /**
     * Outputs a single or multiple error messages to stderr. If no parameters
     * are passed outputs just a newline.
     * @param string|array|null $message  A string or an array of strings to output
     * @param int               $newlines Number of newlines to append
     * @return int|bool The number of bytes returned from writing to stderr.
     */
    public function err($message = null, int $newlines = 1);
}
