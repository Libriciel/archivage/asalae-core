<?php
/**
 * AsalaeCore\Cron\Pronom
 */

namespace AsalaeCore\Cron;

use AsalaeCore\Console\ConsoleLogTrait;
use Cake\Console\ConsoleOutput;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\I18n\FrozenTime;
use Cake\Log\Log;
use Cake\ORM\Association\BelongsToMany;
use Cake\ORM\TableRegistry;
use AsalaeCore\Utility\SoapClient;
use Cake\Utility\Hash;
use DateInterval;
use Exception;
use SoapFault;

/**
 * Cron qui permet de mettre à jour la table pronoms
 *
 * @category Shell\Cron
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Pronom implements CronInterface
{
    const VERSION_SUBJECT = 'PronomTable';

    /**
     * Traits
     */
    use ConsoleLogTrait;

    /**
     * @var array Paramètres d'initialisation
     */
    private $params;

    /**
     * Constructeur de la classe du cron
     * @param array              $params paramètres additionnels du cron
     * @param ConsoleOutput|null $out
     * @param ConsoleOutput|null $err
     */
    public function __construct(
        array $params = [],
        $out = null,
        $err = null
    ) {
        $this->initializeOutput($out, $err);
        $this->params = $params + [
            'wsdl' => 'https://www.nationalarchives.gov.uk/PRONOM/Services/Contract/PRONOM.wsdl',
            'endpoint' => 'https://www.nationalarchives.gov.uk/PRONOM/service.asmx',
            'action_version' => 'getSignatureFileVersionV1',
            'action_data' => 'getSignatureFileV1',
            'extract_version' => 'Version.Version',
            'extract_data' => 'SignatureFile.FFSignatureFile.FileFormatCollection.FileFormat',
            'use_proxy' => false,
            'retry' => 5,
            'retry_wait_duration' => 10,
            'min_last_update_days' => 7,
            'verify_peer' => true,
        ];
    }

    /**
     * @inheritDoc
     */
    public static function getVirtualFields(): array
    {
        return [
            'wsdl' => [
                'type' => 'url',
                'label' => 'swdl',
                'default' => 'https://www.nationalarchives.gov.uk/PRONOM/Services/Contract/PRONOM.wsdl',
            ],
            'endpoint' => [
                'type' => 'url',
                'label' => 'endpoint',
                'default' => 'https://www.nationalarchives.gov.uk/PRONOM/service.asmx',
            ],
            'action_version' => [
                'type' => 'text',
                'label' => 'action_version',
                'default' => 'getSignatureFileVersionV1',
            ],
            'action_data' => [
                'type' => 'text',
                'label' => 'action_data',
                'default' => 'getSignatureFileV1',
            ],
            'extract_version' => [
                'type' => 'text',
                'label' => 'extract_version',
                'default' => 'Version.Version',
            ],
            'extract_data' => [
                'type' => 'text',
                'label' => 'extract_data',
                'default' => 'SignatureFile.FFSignatureFile.FileFormatCollection.FileFormat',
            ],
            'retry' => [
                'type' => 'number',
                'label' => 'retry',
                'default' => 5,
                'min' => 0,
                'help' => __("Nombre d'essai de connexion"),
            ],
            'retry_wait_duration' => [
                'type' => 'number',
                'label' => 'retry_wait_duration',
                'default' => 10,
                'min' => 0,
                'help' => __("Durée d'attente entre les essais"),
            ],
            'min_last_update_days' => [
                'type' => 'number',
                'label' => 'min_last_update_days',
                'default' => 7,
                'min' => 0,
                'help' => __("Nombre de jours depuis la dernière réussite avant notification d'échec"),
            ],
            'use_proxy' => [
                'type' => 'checkbox',
                'label' => __("Utiliser le proxy configuré (Configuration/Utilitaires)"),
                'default' => false,
            ],
            'verify_peer' => [
                'type' => 'checkbox',
                'label' => 'verify_peer',
                'help' => __("Décocher si vous utilisez un proxy et que ça ne fonctionne pas"),
                'default' => true,
            ],
        ];
    }

    /**
     * Donne une nouvelle instance de SoapClient
     * @param array $params
     * @return \SoapClient
     * @throws SoapFault
     */
    protected function getSoapClient(array $params = []): \SoapClient
    {
        $params += $this->params;
        $client = new SoapClient($params['wsdl'], $this->getSoapParams($params));
        $client->__setLocation($params['endpoint']);
        return $client;
    }

    /**
     * Effectue le travail
     * @param EntityInterface|null $exec
     * @param EntityInterface|null $cron
     * @return string state success|warning|error
     * @throws Exception
     */
    public function work(EntityInterface $exec = null, EntityInterface $cron = null): string
    {
        $this->out(__("Mise à jour de la table pronoms..."));
        do {
            try {
                $client = $this->getSoapClient();
            } catch (Exception $e) {
                $this->out(__("Echec lors de la mise à jour de la table Pronom"));
                Log::error((string)$e);
                $client = null;
                if ($this->params['retry'] >= 1) {
                    $this->out(__("Nouvelle tentative dans {0} minutes...", $this->params['retry_wait_duration']));
                    sleep($this->params['retry_wait_duration'] * 60); // NOSONAR
                }
            }
            $this->params['retry']--;
        } while (!$client && $this->params['retry'] > 0 && $this->params['retry_wait_duration']);
        if (!$client) {
            if ($cron) {
                $minLastDateInterval = new DateInterval('P'.$this->params['min_last_update_days'].'D');
                $CronExecutions = TableRegistry::getTableLocator()->get('CronExecutions');
                $nearLastTimeUpdate = $CronExecutions->exists(
                    [
                        'cron_id' => $cron->id,
                        'state' => 'success',
                        'date_begin >=' => (new FrozenTime)->sub($minLastDateInterval),
                    ]
                );
                $this->out(__("Toutes les tentatives de mise à jour ont échoué"));
                return $nearLastTimeUpdate ? 'warning' : 'danger';
            }
            return 'warning';
        }

        $getVersion = json_decode(
            json_encode($client->__soapCall($this->params['action_version'], [])),
            true
        );
        $actualVersion = Hash::get($getVersion, $this->params['extract_version']);
        $versionExists = TableRegistry::getTableLocator()->get('Versions')->exists(
            [
                'subject' => self::VERSION_SUBJECT,
                'version' => $actualVersion
            ]
        );
        if ($versionExists) {
            $this->out(__("La table de pronom est déjà à jour (version {0})", $actualVersion));
            return 'success';
        }
        $this->out(
            __(
                "Une mise à jour a été trouvée vers la version {0}. Lancement du téléchargement...",
                $actualVersion
            )
        );
        $getData = json_decode(
            json_encode($client->__soapCall($this->params['action_data'], [])),
            true
        );
        $data = Hash::get($getData, $this->params['extract_data']);
        try {
            return $this->updatePronoms($actualVersion, $data);
        } catch (Exception $e) {
            Log::error((string)$e);
            $this->out(__("Echec lors de la mise à jour de la table Pronom"));
            if ($this->params['retry'] && $this->params['retry_wait_duration']) {
                $this->params['retry']--;
                $this->out(__("Nouvelle tentative dans {0} minutes...", $this->params['retry_wait_duration']));
                sleep($this->params['retry_wait_duration'] * 60); // NOSONAR
                return $this->work($exec, $cron);
            }
            return 'warning';
        }
    }

    /**
     * Donne les paramètres à passer au client soap
     * @param array $params
     * @return array
     */
    private function getSoapParams(array $params)
    {
        if ($this->params['use_proxy']) {
            $params += ['proxy_host' => Configure::read('Proxy.host')];
            if ($port = Configure::read('Proxy.port')) {
                $params += ['proxy_port' => $port];
            }
            if ($login = Configure::read('Proxy.username')) {
                $params += ['proxy_login' => $login];
            }
            if ($password = Configure::read('Proxy.password')) {
                $params += ['proxy_password' => $password];
            }
        }
        if (empty($this->params['verify_peer'])) {
            $params['stream_context'] = stream_context_create(
                [
                    'ssl' => [
                        'verify_peer' => false,
                    ]
                ]
            );
        }
        $params['cache_wsdl'] = WSDL_CACHE_NONE;
        return $params;
    }

    /**
     * Met à jour la table des pronoms et la table versions
     * @param string $actualVersion
     * @param array  $data
     * @return string
     * @throws Exception
     */
    private function updatePronoms($actualVersion, array $data): string
    {
        $this->out(__("Enregistrement en base de données...", $actualVersion));
        $Pronoms = TableRegistry::getTableLocator()->get('Pronoms');
        $Extensions = TableRegistry::getTableLocator()->get('FileExtensions');
        $conn = $Pronoms->getConnection();
        $conn->begin();

        foreach ($data as $value) {
            $entity = $Pronoms->find()
                ->where(['puid' => $value['PUID']])
                ->first();
            if (empty($entity)) {
                $entity = $Pronoms->newEntity([], ['validate' => false]);
            }
            $Pronoms->patchEntity(
                $entity,
                [
                    'name' => $value['Name'],
                    'puid' => $value['PUID'],
                    'mime' => $value['MIMEType'] ?? null,
                ]
            );
            $extensions = (array)($value['Extension'] ?? []);
            $exts = [];
            foreach ($extensions as $extension) {
                $exts[] = $Extensions->findOrCreate(['name' => $extension]);
            }
            if (!$Pronoms->save($entity)) {
                $conn->rollback();
                throw new Exception("Erreur lors de la sauvegarde d'un pronom: {0}", json_encode($entity));
            }
            if ($exts) {
                /** @var BelongsToMany $belongsToMany */
                $belongsToMany = $Pronoms->getAssociation('FileExtensions');
                $belongsToMany->link($entity, $exts);
            }
        }

        $Versions = TableRegistry::getTableLocator()->get('Versions');
        $version = $Versions->newEntity(
            [
                'version' => $actualVersion,
                'subject' => self::VERSION_SUBJECT
            ]
        );
        $Versions->saveOrFail($version);

        $conn->commit();
        $this->out(__("Le répertoire de pronoms a été mis à jour avec succès."));
        return "success";
    }
}
