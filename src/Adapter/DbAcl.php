<?php
/**
 * AsalaeCore\Adapter\DbAcl
 */

namespace AsalaeCore\Adapter;

use Acl\Adapter\DbAcl as AclDbAcl;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;

/**
 * Surcharge du DbAcl
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property Table $Aco
 * @property Table $Aro
 * @property Table $Permission
 */
class DbAcl extends AclDbAcl
{
    /**
     * Constructor
     * @noinspection PhpMissingParentConstructorInspection surcharge
     */
    public function __construct()
    {
        $this->Permission = TableRegistry::getTableLocator()->get('ArosAcos');
        $this->Aro = $this->Permission->Aros->getTarget();
        $this->Aco = $this->Permission->Acos->getTarget();
    }
}
