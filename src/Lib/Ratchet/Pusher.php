<?php
/**
 * AsalaeCore\Lib\Ratchet\Pusher
 */

namespace AsalaeCore\Lib\Ratchet;

use Beanstalk\Utility\Beanstalk;
use Exception;
use Ratchet\ConnectionInterface;
use Ratchet\Wamp\Topic;
use Ratchet\Wamp\WampConnection;
use Ratchet\Wamp\WampServerInterface;

/**
 * Utilisé par NotificationShell pour Ratchet
 *
 * @category    Lib
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2017, Libriciel
 * @see         http://socketo.me/docs/push
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Pusher implements WampServerInterface
{

    /**
     * @var array Liste des souscriptions de l'utilisateur
     */
    private $subscribedTopics = array();

    /**
     * @var bool Génère des messages si à vrai
     */
    public $verbose = false;

    /**
     * Pusher constructor.
     * @param bool $verbose
     */
    public function __construct($verbose = false)
    {
        $this->verbose = $verbose;
    }

    /**
     * {@inheritdoc}
     *
     * @param ConnectionInterface $conn
     * @param string|Topic        $topic The topic to subscribe to
     */
    public function onSubscribe(ConnectionInterface $conn, $topic)
    {
        if ($this->verbose) {
            $id = $this->getConnId($conn);
            $count = count($this->subscribedTopics[$topic->getId()] ?? []) + 1;
            echo __FUNCTION__."({$topic->getId()}, $id) subscribed count $count\n";
        }
        $this->subscribedTopics[$topic->getId()] = $topic;
    }

    /**
     * Lorsqu'un nouveau message est émis, on le propage vers le bon topic
     * Les personnes qui ont "souscrit" au topic, recevront le message
     *
     * @param string $json JSON obtenu depuis \AsalaeCore\Utility\Notify::send()
     */
    public function onNewMessage($json)
    {
        if ($this->verbose) {
            echo __FUNCTION__."($json)\n";
        }

        $data = json_decode($json, true);

        if (!array_key_exists($data['category'], $this->subscribedTopics)) {
            return;
        }

        $topic = $this->subscribedTopics[$data['category']];
        $topic->broadcast($data);
    }

    /**
     * {@inheritdoc}
     *
     * @param ConnectionInterface $conn
     * @param string|Topic        $topic The topic to unsubscribe from
     */
    public function onUnSubscribe(ConnectionInterface $conn, $topic)
    {
        if ($this->verbose) {
            $id = $this->getConnId($conn);
            echo __FUNCTION__."({$topic->getId()}, $id)\n";
        }
    }

    /**
     * {@inheritdoc}
     *
     * @param  ConnectionInterface $conn The socket/connection that just connected to your application
     * @throws \Exception
     */
    public function onOpen(ConnectionInterface $conn)
    {
        if ($this->verbose) {
            $id = $this->getConnId($conn);
            echo __FUNCTION__."($id)\n";
        }
    }

    /**
     * {@inheritdoc}
     *
     * @param  ConnectionInterface $conn The socket/connection that is closing/closed
     * @throws \Exception
     */
    public function onClose(ConnectionInterface $conn)
    {
        if ($this->verbose) {
            $id = $this->getConnId($conn);
            echo __FUNCTION__."($id)\n";
        }
    }

    /**
     * {@inheritdoc}
     *
     * @param ConnectionInterface $conn
     * @param string              $id     The unique ID of the RPC, required to respond to
     * @param string|Topic        $topic  The topic to execute the call against
     * @param array               $params Call parameters received from the client
     */
    public function onCall(ConnectionInterface $conn, $id, $topic, array $params)
    {
        if ($this->verbose) {
            $id = $this->getConnId($conn);
            echo __FUNCTION__."({$topic->getId()}, $id)\n";
        }
        if ($conn instanceof WampConnection) {
            $conn->callError($id, $topic, __('You are not allowed to make calls'))->close();
        }
    }

    /**
     * {@inheritdoc}
     *
     * @param ConnectionInterface $conn
     * @param string|Topic        $topic    The topic the user has attempted to publish to
     * @param string|array        $event    Payload of the publish
     * @param array               $exclude  A list of session IDs the message
     *                                      should be excluded from (blacklist)
     * @param array               $eligible A list of session Ids the message should be send to (whitelist)
     * @throws Exception
     */
    public function onPublish(ConnectionInterface $conn, $topic, $event, array $exclude, array $eligible)
    {
        if ($this->verbose) {
            $e = var_export($event, true);
            $id = $this->getConnId($conn);
            echo __FUNCTION__."({$topic->getId()}, $e, $id)\n";
        }
        if ($topic->getId() === 'pong') {
            $Beanstalk = new Beanstalk('notification');
            $Beanstalk->emit((int)$event);
            return;
        }
        if (!array_key_exists($topic->getId(), $this->subscribedTopics)) {
            return $conn->close();
        }
        if (!is_array($event)) {
            $event = ['value' => $event];
        }
        $data = [
            'category' => $topic->getId(),
        ] + $event;
        $topic->broadcast($data);
    }

    /**
     * {@inheritdoc}
     *
     * @param  ConnectionInterface $conn
     * @param  \Exception          $e
     * @throws \Exception
     */
    public function onError(ConnectionInterface $conn, Exception $e)
    {
        echo __FUNCTION__.": {$e->getMessage()}\n";
    }

    /**
     * Donne l'ID lié à une connexion
     * @param ConnectionInterface $conn
     * @return null
     */
    private function getConnId(ConnectionInterface $conn)
    {
        if ($conn instanceof WampConnection) {
            return $conn->WAMP->sessionId;
        }
        return null;
    }
}
