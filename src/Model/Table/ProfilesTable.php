<?php
/**
 * AsalaeCore\Model\Table\ProfilesTable
 */

namespace AsalaeCore\Model\Table;

use ArrayObject;
use AsalaeCore\Model\Entity\Fileupload;
use Cake\Event\Event;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Entity;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

/**
 * Table profiles
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin TimestampBehavior
 */
class ProfilesTable extends Table implements
    AfterSaveInterface,
    AfterDeleteInterface,
    BeforeSaveInterface,
    BeforeDeleteInterface
{

    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        $this->addBehavior('Timestamp');

        $this->belongsTo('OrgEntities');
        $this->belongsToMany('Fileuploads');
        $this->belongsToMany('Agreements');

        parent::initialize($config);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator->notEmptyString('name');
        $validator->notEmptyString('identifier');
        $validator->add(
            'identifier',
            'unique',
            [
                'rule' => ['validateUnique', ['scope' => 'org_entity_id']],
                'provider' => 'table',
                'message' => __("Cet identifiant est déjà utilisé")
            ]
        );

        return $validator;
    }

    /**
     * The Model.afterSave event is fired after an entity is saved.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     */
    public function afterSave(Event $event, Entity $entity, ArrayObject $options)
    {
        $Fileuploads = TableRegistry::getTableLocator()->get('Fileuploads');
        $dirties = $entity->get('_dirty');
        if (!$entity->isNew()
            && in_array('fileuploads', $dirties)
            && ($originals = $entity->get('_original')['fileuploads'])
        ) {
            $idsOriginal = array_map(
                function (Entity $v) {
                    return $v->get('id');
                },
                $originals
            );
            $idsNow = array_map(
                function (Entity $v) {
                    return $v->get('id');
                },
                $entity->get('fileuploads')
            );
            $toDelete = array_diff($idsOriginal, $idsNow);
            if ($toDelete) {
                $Fileuploads->deleteAll(['id IN' => $toDelete]);
            }
        }
        if (in_array('fileuploads', $dirties)) {
            $ids = array_map(
                function (Fileupload $v) {
                    return $v->get('id');
                },
                $entity->get('fileuploads')
            );
            if ($ids) {
                $Fileuploads->updateAll(['locked' => true], ['id IN' => $ids]);
            }
        }
    }
    /**
     * The Model.afterDelete event is fired after an entity is deleted.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     */
    public function afterDelete(Event $event, Entity $entity, ArrayObject $options)
    {
        if ($entity->get('_fileuploads')) {
            TableRegistry::getTableLocator()->get('Fileuploads')
                ->deleteAll(['id IN' => $entity->get('_fileuploads')]);
        }
    }

    /**
     * The Model.beforeSave event is fired before an entity is saved.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     */
    public function beforeSave(Event $event, Entity $entity, ArrayObject $options)
    {
        $entity->set('_dirty', $entity->getDirty());
        $entity->set('_original', $entity->getOriginalValues());
    }

    /**
     * The Model.beforeDelete Fired before an entity is deleted.
     * By stopping this event you will abort the delete operation.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     */
    public function beforeDelete(Event $event, Entity $entity, ArrayObject $options)
    {
        $ids = TableRegistry::getTableLocator()->get('FileuploadsProfiles')->find()
            ->select(['fileupload_id'])
            ->where(['profile_id' => $entity->get('id')])
            ->all()
            ->map(
                function (Entity $v) {
                    return $v->get('fileupload_id');
                }
            )
            ->toArray();
        $entity->set('_fileuploads', $ids);
    }
}
