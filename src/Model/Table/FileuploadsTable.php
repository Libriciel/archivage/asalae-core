<?php
/**
 * AsalaeCore\Model\Table\FileuploadsTable
 */

namespace AsalaeCore\Model\Table;

use ArrayObject;
use AsalaeCore\Error\AppErrorHandler;
use AsalaeCore\Model\Behavior\OptionsBehavior;
use AsalaeCore\Utility\PreControlMessages;
use AsalaeCore\Validation\ValidationRule;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Entity;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use DOMDocument;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;
use StateMachine\Model\Behavior\StateMachineBehavior;

/**
 * Table fileuploads
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin TimestampBehavior
 * @mixin StateMachineBehavior
 * @mixin OptionsBehavior
 * @property MediainfosTable Mediainfos
 * @property SiegfriedsTable Siegfrieds
 */
class FileuploadsTable extends Table implements BeforeDeleteInterface, AfterDeleteInterface
{
    const S_UPLOADED = 'uploaded';
    const S_HASHED = 'hashed';
    const S_VALIDED = 'valided';
    const S_INVALIDED = 'invalided';
    const S_HASHED_VALIDED = 'hashed_valided';
    const S_HASHED_INVALIDED = 'hashed_invalided';

    const T_HASH = 'hash';
    const T_VALIDATE = 'validate';
    const T_INVALIDATE = 'invalidate';
    const T_CONVERT = 'convert';

    /**
     * Etat initial
     *
     * @var string
     */
    public $initialState = self::S_UPLOADED;

    /**
     * Changement d'êtats selon action
     *
     * @var array
     */
    public $transitions = [
        self::T_HASH => [
            self::S_UPLOADED => self::S_HASHED,
            self::S_VALIDED => self::S_HASHED_VALIDED,
            self::S_INVALIDED => self::S_HASHED_INVALIDED
        ],
        self::T_VALIDATE => [
            self::S_UPLOADED => self::S_VALIDED,
            self::S_INVALIDED => self::S_VALIDED,
            self::S_HASHED_INVALIDED => self::S_HASHED_VALIDED,
            self::S_HASHED => self::S_HASHED_VALIDED,
        ],
        self::T_INVALIDATE => [
            self::S_UPLOADED => self::S_INVALIDED,
            self::S_VALIDED => self::S_INVALIDED,
            self::S_HASHED_VALIDED => self::S_HASHED_INVALIDED,
            self::S_HASHED => self::S_HASHED_INVALIDED,
        ],
        self::T_CONVERT => [
            self::S_UPLOADED => self::S_UPLOADED,
            self::S_VALIDED => self::S_VALIDED,
            self::S_HASHED_VALIDED => self::S_HASHED_VALIDED,
            self::S_HASHED => self::S_HASHED,
        ]
    ];
    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        $this->addBehavior('Timestamp');
        $this->addBehavior('StateMachine.StateMachine');
        $this->addBehavior(
            'AsalaeCore.Options',
            [
                'state' => [
                    self::S_UPLOADED,
                    self::S_HASHED,
                    self::S_VALIDED,
                    self::S_INVALIDED,
                    self::S_HASHED_VALIDED,
                    self::S_HASHED_INVALIDED
                ]
            ]
        );

        $this->hasOne('AsalaeCore.Mediainfos');
        $this->hasMany('AsalaeCore.Siegfrieds');

        parent::initialize($config);
    }

    /**
     * Permet de sauvegarder en base un fichier
     *
     * @param string   $name     nom du fichier avant envoi
     * @param string   $filename chemin vers le fichier
     *                           réel
     * @param int|null $user_id
     * @param array    $params
     * @return EntityInterface
     * @throws Exception
     */
    public function newUploadedEntity(
        string $name,
        string $filename,
        int $user_id = null,
        array $params = []
    ): EntityInterface {
        if (!is_file($filename) || !is_readable($filename)) {
            throw new Exception('Impossible de lire le fichier');
        }

        $params += [
            'associated' => [
                'Mediainfos' => [
                    'associated' => [
                        'MediainfoVideos',
                        'MediainfoAudios',
                        'MediainfoTexts'
                    ]
                ],
                'Siegfrieds'
            ]
        ];

        return $this->newEntity(
            [
                'name' => $name,
                'path' => $filename,
                'user_id' => $user_id,
                'size' => filesize($filename),
                'hash_algo' => Configure::read('hash_algo') ?: 'sha256',
                'mime' => mime_content_type($filename),
                'mediainfo' => $this->Mediainfos->newEntityByFilename($filename)->toArray(),
                'siegfrieds' => $this->Siegfrieds->newEntitiesByFilename($filename, true),
            ],
            $params
        );
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        return $validator;
    }

    /**
     * Validation SEDA
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationSeda(Validator $validator): Validator
    {
        $validator = $this->validationDefault($validator);

        $validator->add(
            'path',
            'precontrol',
            [
                'rule' => function ($value) {
                    $success = PreControlMessages::isValid($value);
                    if ($success) {
                        return true;
                    } else {
                        if (Configure::read('debug') && PreControlMessages::$lastException) {
                            return nl2br((string)PreControlMessages::$lastException);
                        }
                        return nl2br(PreControlMessages::getLastErrorMessage());
                    }
                },
                'message' => __("Le fichier a échoué aux tests préliminaires"),
            ]
        );
        return $validator;
    }

    /**
     * Validation SEDA
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationEaccpf(Validator $validator): Validator
    {
        return $this->validationDefault($validator)->add(
            'path',
            'custom',
            [
                'rule' => function ($value) {
                    $xml = new DOMDocument;
                    $xml->load($value);
                    return @$xml->schemaValidate(EAC_CPF_XSD);
                },
                'message' => __("Le fichier n'est pas une notice d'autorité valide.")
            ]
        );
    }

    /**
     * Validation d'un profil d'archivage (xsd ou rng)
     * @param Validator $validator
     * @return Validator
     */
    public function validationProfile(Validator $validator): Validator
    {
        $rule = new ValidationRule(
            [
                'rule' => function ($value, $context) {
                    $xml = new DOMDocument;
                    $xml->load($value);
                    $ext = strtolower(pathinfo($context['data']['name'], PATHINFO_EXTENSION));

                    $capture = AppErrorHandler::captureErrors(
                        function () use ($ext, $xml) {
                            $schemaXsd = WWW_ROOT . 'xmlSchemas/xml_schema/XMLSchema.xsd';
                            $schemaRelax = WWW_ROOT . 'xmlSchemas/relaxng/relaxng.rng';
                            return $ext === 'rng'
                            ? $xml->relaxNGValidate($schemaRelax)
                            : $xml->schemaValidate($schemaXsd);
                        }
                    );
                    if ($capture['errors']) {
                        $error = $capture['errors'][0]['description'];

                        /** @var ValidationRule $rule */
                        $rule = $this->getValidator('profile')
                            ->field('path')
                            ->rule('validSchema');
                        $rule->setMessage(
                            __("Le fichier n'est pas un schéma de validation valide : \n{0}", $error)
                        );
                        if (!$capture['result']) {
                            return __("Le fichier n'est pas un schéma de validation valide : \n{0}", $error);
                        }
                    }
                    return $capture['result'];
                },
                'message' => __("Le fichier n'est pas un schéma de validation valide")
            ]
        );
        $validator->add('path', 'validSchema', $rule);
        return $validator;
    }

    /**
     * Validation skos
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationImportKeywords(Validator $validator): Validator
    {
        $validator = $this->validationDefault($validator);
        $rule = new ValidationRule(
            [
                'rule' => function ($value, $context) {
                    $filename = $value;
                    if (!empty($context['data']['name'])) {
                        $filename = $context['data']['name'];
                    }
                    $ext = strtolower(pathinfo($filename, PATHINFO_EXTENSION));
                    if ($ext && in_array($ext, ['rdf', 'csv'])) {
                        $type = $ext;
                    } else {
                        $mime = mime_content_type($value);
                        $type = strpos($mime, 'xml') ? 'rdf' : 'csv';
                    }
                    switch ($type) {
                        case 'rdf':
                            $xml = new DOMDocument;
                            if (@$xml->load($value) === false) { // fichier vide
                                return false;
                            }
                            /** @var ValidationRule $rule */
                            $rule = $this->getValidator('profile')
                            ->field('path')
                            ->rule('validSchema');
                            $rule->setMessage(__("Un RDF doit contenir des champs Concept"));
                            return (bool)$xml->getElementsByTagName('Concept')->length;
                        /** @noinspection PhpConditionAlreadyCheckedInspection faux positif */
                        case 'csv':
                            return true;
                    }
                    return false;
                },
                'message' => __("Le fichier n'est pas un fichier d'import valide:")
            ]
        );
        $validator->add('path', 'validImport', $rule);
        return $validator;
    }

    /**
     * The Model.afterDelete event is fired after an entity has been deleted.
     * @param Event             $event
     * @param Entity            $entity
     * @param ArrayObject|array $options
     * @throws Exception
     */
    public function afterDelete(
        Event $event,
        Entity $entity,
        ArrayObject $options
    ) {
        if (is_file($entity->get('path'))) {
            Filesystem::remove($entity->get('path'));
            $this->deleteEmptyParents(dirname($entity->get('path')));
        }
    }

    /**
     * Supprime les dossiers parents s'ils sont vide
     * @param string $dir
     * @return void
     */
    private function deleteEmptyParents(string $dir)
    {
        if (count(glob($dir.DS."{*,.[!.]*,..?*}", GLOB_BRACE)) === 0) {
            rmdir($dir);
            $this->deleteEmptyParents(dirname($dir));
        }
    }

    /**
     * The Model.beforeDelete Fired before an entity is deleted.
     * By stopping this event you will abort the delete operation.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     * @return bool
     */
    public function beforeDelete(Event $event, Entity $entity, ArrayObject $options)
    {
        if (is_file($entity->get('path')) && !is_writable($entity->get('path'))) {
            trigger_error($entity->get('path') . ' IS NOT WRITABLE', E_USER_WARNING);
            return false;
        }

        if (!$entity->get('deletable')) {
            return false;
        }
        return true;
    }

    /**
     * Donne la liste des fichiers supprimés
     * @return array
     */
    public function findDeleted(): array
    {
        $deleted = [];
        foreach ($this->find('list', ['valueField' => 'path']) as $id => $path) {
            if (!is_file($path)) {
                $deleted[] = $id;
            }
        }
        return $deleted;
    }
}
