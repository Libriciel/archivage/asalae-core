<?php
/**
 * AsalaeCore\Model\Table\WebservicesTable
 */

namespace AsalaeCore\Model\Table;

use ArrayObject;
use Cake\Event\Event;
use Cake\ORM\Entity;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

/**
 * Table webservices
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @deprecated since 2.0.0a5 use UsersTable instead
 */
class WebservicesTable extends Table
{
    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        $this->addBehavior('AsalaeCore.Acl', ['type' => 'requester']);
        $this->addBehavior('Timestamp');

        parent::initialize($config);
    }

    /**
     * Met à jour l'alias de l'aro lors de la modification d'un webservice
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     */
    public function afterSave(/** @noinspection PhpUnusedParameterInspection */
        Event $event,
        Entity $entity,
        ArrayObject $options
    ) {
        $Aros = TableRegistry::getTableLocator()->get('Aros');
        $query = $this->find()
            ->select(['name'])
            ->where(['id' => $entity->get('id')])
            ->limit(1);
        $Aros->query()
            ->update()
            ->set(['alias' => $query])
            ->where(['model' => 'Webservices', 'foreign_key' => $entity->get('id')])
            ->execute();
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator->requirePresence('name');
        $validator
            ->add(
                'passwordSha256',
                'hex',
                [
                    'rule' => ['custom', '/^[0-9a-f]{64}$/'],
                    'message' => __("Doit être un SHA256 valide")
                ]
            );
        $validator->allowEmptyString('confirm-password');
        $validator->sameAs('confirm-password', 'password');
        $validator->add(
            'access',
            'custom',
            [
                'rule' => function ($value, $context) {
                    return !empty($context['data']['permissions']);
                },
                'message' => __("Vous devez définir des droits")
            ]
        );
        $validator->add(
            'name',
            ['unique' => [
                'rule' => 'validateUnique',
                'provider' => 'table',
                'message' => __("Ce nom de Webservice est déjà utilisé")
            ]
            ]
        );
        $validator->add(
            'username',
            ['unique' => [
                'rule' => 'validateUnique',
                'provider' => 'table',
                'message' => __("Ce nom d'utilisateur est déjà utilisé")
            ]
            ]
        );
        $validator->allowEmptyString(
            'username',
            null,
            function ($context) {
                return !empty($context['data']['callback']);
            }
        );
        $validator->allowEmptyString(
            'password',
            null,
            function ($context) {
                return empty($context['data']['username']);
            }
        );
        $validator->allowEmptyString('access');
        $validator->allowEmptyString('homepage');
        $validator->add('homepage', 'valid', ['rule' => 'url']);
        $validator->allowEmptyString('callback');
        $validator->add('callback', 'valid', ['rule' => 'url']);
        $validator->allowEmptyString('client_id');
        $validator->add(
            'client_id',
            ['unique' => [
                'rule' => 'validateUnique',
                'provider' => 'table',
                'message' => __("Ce client_id est déjà utilisé")
            ]
            ]
        );
        $validator->allowEmptyString('client_secret');

        return $validator;
    }
}
