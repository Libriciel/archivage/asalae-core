<?php
/**
 * AsalaeCore\Model\Table\BeforeFindInterface
 */

namespace AsalaeCore\Model\Table;

use Cake\Event\Event;
use Cake\ORM\Query;

/**
 * Interface pour le callback beforeFind
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
interface BeforeFindInterface
{
    /**
     * The Model.beforeFind
     *
     * @param \Cake\Event\Event $event The beforeFind event
     * @param Query             $query Query
     * @return Query
     */
    public function beforeFind(Event $event, $query);
}
