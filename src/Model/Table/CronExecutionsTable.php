<?php
/**
 * AsalaeCore\Model\Table\CronExecutionsTable
 */

namespace AsalaeCore\Model\Table;

use Cake\ORM\Table;

/**
 * Table cron_executions
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class CronExecutionsTable extends Table
{
    const S_SUCCESS = 'success';
    const S_WARNING = 'warning';
    const S_ERROR = 'error';
    const S_ABORTED = 'aborted';
    const S_RUNNING = 'running';
    const S_CANCELED = 'canceled';

    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        $this->belongsTo('Crons');

        parent::initialize($config);
    }
}
