<?php
/**
 * AsalaeCore\Model\Table\ArosAcosTable
 */

namespace AsalaeCore\Model\Table;

use Acl\Model\Table\PermissionsTable;
use Cake\ORM\Table;

/**
 * Table aros_acos
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property Table $Aro
 * @property Table $Aco
 */
class ArosAcosTable extends PermissionsTable
{
    /**
     * {@inheritDoc}
     *
     * @param array $config Configuration
     * @return void
     */
    public function initialize(array $config) :void
    {
        $this->setTable('aros_acos');
        $this->belongsTo('Aros');
        $this->belongsTo('Acos');
        $this->Aro = $this->Aros->getTarget();
        $this->Aco = $this->Acos->getTarget();
    }
}
