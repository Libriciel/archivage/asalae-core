<?php
/**
 * AsalaeCore\Model\Table\ConfigurationsTable
 */

namespace AsalaeCore\Model\Table;

use ArrayObject;
use AsalaeCore\Model\Entity\Configuration;
use Cake\Event\Event;
use Cake\ORM\Entity;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Exception;
use FilesystemIterator;
use Libriciel\Filesystem\Utility\Filesystem;

/**
 * Table configurations
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ConfigurationsTable extends Table implements AfterDeleteInterface
{

    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->belongsTo(
            'OrgEntities',
            [
                'foreignKey' => 'org_entity_id',
                'joinType' => 'INNER'
            ]
        );
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('setting')
            ->requirePresence('setting', 'create')
            ->notEmptyString('setting');

        return $validator;
    }

    /**
     * The Model.afterDelete event is fired after an entity is deleted.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     * @throws Exception
     */
    public function afterDelete(Event $event, Entity $entity, ArrayObject $options)
    {
        if (in_array($entity->get('name'), Configuration::URI_NAMES)) {
            $file1 = $entity->get('data_filename');
            $file2 = $entity->get('webroot_filename');
            $file3 = $entity->get('public_filename'); // remplace data_filename
            foreach ([$file1, $file2, $file3] as $file) {
                if (!is_writable($file) || !is_file($file)) {
                    continue;
                }
                $dir = dirname($file);
                $filesInDir = iterator_count(
                    new FilesystemIterator($dir, FilesystemIterator::SKIP_DOTS)
                );
                if ($filesInDir === 1 && is_writable($dir)) {
                    Filesystem::remove($dir);
                } else {
                    Filesystem::remove($file);
                }
            }
        }
    }
}
