<?php
/**
 * AsalaeCore\Model\Table\AuthUrlsTable
 */

namespace AsalaeCore\Model\Table;

use AsalaeCore\Model\Entity\AuthUrl;
use Cake\Event\Event;
use Cake\Http\ServerRequest;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use DateTime;

/**
 * Table auth_urls
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin TimestampBehavior
 */
class AuthUrlsTable extends Table implements BeforeFindInterface
{

    /**
     * La table est t-elle synchronisée ?
     *
     * @var boolean
     */
    public $sync = false;

    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->addBehavior('Timestamp');

        $this->hasMany('AuthSubUrls');
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator->notEmptyString('code');
        $validator->dateTime('expire');

        return $validator;
    }

    /**
     * Supprime urls expirés
     * @param \Cake\Event\Event $event The beforeFind event
     * @param Query             $query Query
     * @return Query
     */
    public function beforeFind(Event $event, $query)
    {
        if (!$this->sync) {
            $this->sync = true;
            $this->deleteAll(['expire <' => new DateTime]);
        }
        return $query;
    }

    /**
     * Vrai si $request correspond au code
     * @param ServerRequest $request
     * @param string|null   $code
     * @return bool
     */
    public function isAuthorized(ServerRequest $request, string $code = null): bool
    {
        $code = $code ?: $this->getCode($request);
        if (!$code) {
            return false;
        }
        /** @var AuthUrl $code */
        $code = $this->find()
            ->where(['code' => $code])
            ->contain(['AuthSubUrls'])
            ->first();
        return $code && $code->allowed($request);
    }

    /**
     * Supprime un accès après utilisation
     * @param ServerRequest $request
     * @param string|null   $code
     */
    public function consume(ServerRequest $request, string $code = null)
    {
        if (!$code) {
            $code = $request->getSession()->read('Auth.code');
        }
        if (!$code) {
            $code = $request->getHeaderLine('code');
        }
        $this->deleteAll(['code' => $code]);
    }

    /**
     * Donne le code lié à un request
     * @param ServerRequest $request
     * @return string|null
     */
    public function getCode(ServerRequest $request): ?string
    {
        $code = $request->getSession()->read('Auth.code');
        if (!$code) {
            $code = $request->getHeaderLine('code');
            $request->getSession()->write('Auth.code', $code);
        }
        if (!$code) {
            $code = $request->getData('code');
            $request->getSession()->write('Auth.code', $code);
        }
        if (!$code) {
            $code = $request->getQuery('code');
            $request->getSession()->write('Auth.code', $code);
        }
        return $code;
    }
}
