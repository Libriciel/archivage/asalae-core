<?php
/**
 * AsalaeCore\Model\Table\VersionsTable
 */

namespace AsalaeCore\Model\Table;

use ArrayObject;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Entity;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

/**
 * Table versions
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin TimestampBehavior
 */
class VersionsTable extends Table implements BeforeDeleteInterface
{
    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        $this->addBehavior('Timestamp');
        parent::initialize($config);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator->notEmptyString('subject');
        $validator->notEmptyString('version');
        $validator->add(
            'subject',
            [
                'unique' => [
                    'rule' => ['validateUnique', ['scope' => 'version']],
                    'provider' => 'table',
                    'message' => __("Une version pour ce sujet existe déjà")
                ]
            ]
        );
        return $validator;
    }

    /**
     * Donne la dernière version d'un sujet
     * @param string $subject
     * @return string
     */
    public function getLast(string $subject): string
    {
        return $this->find()
            ->where(['subject' => $subject])
            ->order(['id' => 'desc'])
            ->disableHydration()
            ->firstOrFail()
            ['version'];
    }

    /**
     * Donne l'entité de la version de l'application (version en cours)
     * @return EntityInterface
     */
    public function getAppVersion(): EntityInterface
    {
        $app = Configure::read('App.name');
        return $this->find()
            ->where(['subject' => $app])
            ->order(['id' => 'desc'])
            ->firstOrFail();
    }

    /**
     * The Model.beforeDelete Fired before an entity is deleted.
     * By stopping this event you will abort the delete operation.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     */
    public function beforeDelete(Event $event, Entity $entity, ArrayObject $options)
    {
        $event->stopPropagation(); // il ne faut pas supprimer les versions
    }

    /**
     * Deletes all records matching the provided conditions.
     *
     * This method will *not* trigger beforeDelete/afterDelete events. If you
     * need those first load a collection of records and delete them.
     *
     * This method will *not* execute on associations' `cascade` attribute. You should
     * use database foreign keys + ON CASCADE rules if you need cascading deletes combined
     * with this method.
     *
     * @param mixed $conditions Conditions to be used, accepts anything Query::where()
     *                          can take.
     * @return int Returns the number of affected rows.
     * @see \Cake\Datasource\RepositoryInterface::delete()
     */
    public function deleteAll($conditions): int
    {
        return 0; // il ne faut pas supprimer les versions
    }

    /**
     * Ajoute les versions liés au patchs stockés dans phinxlog
     */
    public function insertMissingVersions()
    {
        $migrations = glob(CONFIG.DS.'Migrations'.DS.'*_Patch*.php');
        $Phinxlog = TableRegistry::getTableLocator()->get('Phinxlog');
        $appName = Configure::read('App.name');
        foreach ($migrations as $migration) {
            [$timestamp, $name] = explode('_', basename($migration, '.php'));
            $version = implode('.', str_split(str_replace('Patch', '', $name)));
            if (!$this->exists(['subject' => $appName, 'version' => $version])) {
                $phinxDate = $Phinxlog->find()
                    ->where(['version' => $timestamp])
                    ->firstOrFail()
                    ->get('end_time');
                $this->query()
                    ->update()
                    ->insert(['subject', 'version', 'created'])
                    ->values(
                        [
                            'subject' => $appName,
                            'version' => $version,
                            'created' => $phinxDate,
                        ]
                    )
                    ->execute();
            }
        }
    }
}
