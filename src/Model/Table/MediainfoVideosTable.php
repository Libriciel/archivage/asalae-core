<?php
/**
 * AsalaeCore\Model\Table\MediainfoVideosTable
 */

namespace AsalaeCore\Model\Table;

use Cake\ORM\Table;

/**
 * Table mediainfo_videos
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class MediainfoVideosTable extends Table
{
    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        $this->belongsTo('AsalaeCore.Mediainfos');

        parent::initialize($config);
    }
}
