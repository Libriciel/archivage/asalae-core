<?php
/**
 * AsalaeCore\Model\Table\UsersTable
 */

namespace AsalaeCore\Model\Table;

use ArrayObject;
use AsalaeCore\Model\Behavior\AclBehavior;
use AsalaeCore\Model\Entity\Role;
use AsalaeCore\Form\AdminShellForm;
use AsalaeCore\Utility\Session;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\Log\Log;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Entity;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Cake\Validation\Validator;
use Exception;

/**
 * Table users
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin AclBehavior
 * @mixin TimestampBehavior
 */
class UsersTable extends Table implements AfterSaveInterface
{
    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        $this->addBehavior('AsalaeCore.Acl', ['type' => 'requester']);
        $this->addBehavior('Timestamp');

        $this->belongsTo('Ldaps');
        $this->belongsTo('OrgEntities');
        $this->belongsTo('Roles');

        $this->hasMany('Notifications')
            ->setDependent(true);
        $this->hasMany('SavedFilters')
            ->setDependent(true);
        $this->hasMany('Sessions')
            ->setDependent(true);

        parent::initialize($config);
    }

    /**
     * Met à jour l'alias de l'aro lors de la modification d'un user
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     */
    public function afterSave(Event $event, Entity $entity, ArrayObject $options)
    {
        if ($entity->isNew() || $entity->isDirty('username')) {
            TableRegistry::getTableLocator()->get('Aros')->query()
                ->update()
                ->set(['alias' => $entity->get('username')])
                ->where(['Aros.model' => 'Users', 'foreign_key' => $entity->get('id')])
                ->execute();
        }
        $role = $entity->get('role_id');
        if (!$entity->isNew() && $role && $entity->isDirty('role_id')) {
            try {
                Session::emitReset($entity->get('id'));
            } catch (Exception $e) {
                Log::error($e->getMessage());
            }
        }
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator->notEmptyString('username');

        $checkPassword = function ($value) {
            $table = $this;
            $score = $table->scorePassword($value);
            return $score >= Configure::read('Password.complexity');
        };

        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('username')
            ->maxLength('username', 512)
            ->allowEmptyString('username')
            ->add(
                'username',
                [
                    'unique' => [
                        'rule' => 'validateUnique',
                        'provider' => 'table',
                        'message' => __("Ce nom d'utilisateur est déjà utilisé")
                    ]
                ]
            );

        $validator
            ->scalar('password')
            ->maxLength('password', 255)
            ->allowEmptyString('password')
            ->add(
                'password',
                'custom',
                [
                    'rule' => $checkPassword,
                    'message' => __("Le password n'est pas assez complexe")
                ]
            );

        $validator
            ->allowEmptyString(
                'confirm-password',
                null,
                function ($context) {
                    return empty($context['data']['password']);
                }
            )
            ->sameAs('confirm-password', 'password');

        $validator
            ->scalar('cookies')
            ->allowEmptyString('cookies')
            ->add(
                'cookies',
                'custom',
                [
                    'rule' => function ($value) {
                        if (empty($value)) {
                            return true;
                        }
                        try {
                            json_decode($value);
                            return true;
                        } catch (Exception $e) {
                            return false;
                        }
                    },
                    'message' => __("N'est pas un JSON valide")
                ]
            );

        $validator
            ->scalar('menu')
            ->allowEmptyString('menu');

        $validator
            ->boolean('high_contrast')
            ->notEmptyString('high_contrast');

        $validator
            ->boolean('active')
            ->notEmptyString('active');

        $validator
            ->allowEmptyString(
                'role_id',
                null,
                function ($context) {
                    return Hash::get($context, 'data.agent_type') !== 'software';
                }
            )
            ->add(
                'role_id',
                'custom',
                [
                    'rule' => function ($value, $context) {
                        if (empty($value) && empty($context['data']['org_entity_id'])) {
                            return true;
                        } elseif (in_array(Hash::get($context, 'data.role.code'), Role::WEBSERVICES)) {
                            return true;
                        } elseif (empty($context['data']['org_entity_id'])) {
                            return false;
                        }
                        $roleCode = Hash::get($context, 'data.role.code');
                        $roleId = Hash::get($context, 'data.role_id');
                        if ($roleCode === null && $roleId !== null) {
                            $webServices = Hash::extract(
                                TableRegistry::getTableLocator()->get('Roles')->find()
                                    ->select(['id'])
                                    ->where(['code IN' => Role::WEBSERVICES])
                                    ->all()
                                    ->toArray(),
                                '{n}.id'
                            );
                            if (in_array(Hash::get($context, 'data.role_id'), $webServices)) {
                                return true;
                            }
                        }
                        $OrgEntity = TableRegistry::getTableLocator()->get('OrgEntities')->find()
                            ->where(['OrgEntities.id' => $context['data']['org_entity_id']])
                            ->contain(['TypeEntities' => ['Roles']])
                            ->first();
                        if (!$OrgEntity || !$OrgEntity->get('type_entity')) {
                            return false;
                        }
                        $roles = array_map(
                            function (Role $v) {
                                return $v->get('id');
                            },
                            $OrgEntity->get('type_entity')->get('roles')
                        );
                        return in_array($value, $roles);
                    },
                    'message' => __("Le rôle choisi ne fait pas partie des rôles autorisés par l'entité")
                ]
            );

        $validator
            ->allowEmptyString('org_entity_id');

        $validator
            ->email('email')
            ->allowEmptyString(
                'email',
                null,
                function ($context) {
                    return Hash::get($context, 'providers.entity.agent_type') === 'software'
                        || Hash::get($context, 'data.agent_type') === 'software';
                }
            );

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->allowEmptyString('name');

        $validator
            ->scalar('agent_type')
            ->maxLength('agent_type', 255)
            ->notEmptyString('agent_type');

        $validator
            ->boolean('is_validator')
            ->notEmptyString('is_validator');

        $validator
            ->boolean('use_cert')
            ->notEmptyString('use_cert');

        $validator
            ->scalar('ldap_login')
            ->maxLength('ldap_login', 512)
            ->allowEmptyString('ldap_login');

        return $validator;
    }

    /**
     * Permet d'obtenir un score à partir d'un password
     * @see https://www.ssi.gouv.fr/administration/precautions-elementaires/calculer-la-force-dun-mot-de-passe/
     * @param string $password
     * @return float
     */
    public function scorePassword(string $password): float
    {
        return AdminShellForm::scorePassword($password);
    }

    /**
     * Génère un password aléatoire
     *
     * La valeur 12 permet de générer des passwords avec un score situé entre
     * 74 et 95 (moyenne de 91) ce qui équivaut à peu près au niveau PASSWORD_MEDIUM
     * Si la force du mot de passe est insuffisante par rapport au niveau
     * configuré, la complexité est augmentée jusqu'à obtenir un mot de passe
     * suffisement puissant.
     * exemple d'un mot de passe généré : g0KlxQo9og94cyw0
     *
     * @return string
     * @throws Exception
     */
    public function generatePassword(): string
    {
        $bytes = 12;
        $targetComplexity = Configure::read('Password.complexity', 78);
        do {
            $password = preg_replace(
                '/\W+/',
                '',
                base64_encode(random_bytes($bytes))
            );
            $score = $this->scorePassword($password);
            $bytes++;
        } while ($score < $targetComplexity && $bytes < 1000);
        return $password;
    }
}
