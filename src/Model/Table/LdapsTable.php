<?php
/**
 * AsalaeCore\Model\Table\LdapsTable
 */

namespace AsalaeCore\Model\Table;

use Adldap\Schemas\ActiveDirectory;
use Adldap\Schemas\FreeIPA;
use Adldap\Schemas\OpenLDAP;
use ArrayObject;
use AsalaeCore\Model\Entity\Ldap;
use AsalaeCore\Model\Behavior\OptionsBehavior;
use Cake\Event\Event;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Entity;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

/**
 * Table ldaps
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin TimestampBehavior
 * @mixin OptionsBehavior
 */
class LdapsTable extends Table implements AfterSaveInterface, BeforeMarshalInterface
{

    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->addBehavior('Timestamp');
        $this->addBehavior(
            'AsalaeCore.Options',
            [
                'schema' => [
                    ActiveDirectory::class,
                    OpenLDAP::class,
                    FreeIPA::class,
                ],
                'version' => [2, 3],
                'append_custom_option' => Ldap::LDAP_OPTS,
            ]
        );

        $this->belongsTo('OrgEntities');
        $this->hasMany('Users');
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name')
            ->add('name', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('host')
            ->maxLength('host', 255)
            ->requirePresence('host', 'create')
            ->notEmptyString('host');

        $validator
            ->integer('port')
            ->notEmptyString('port');

        $validator
            ->scalar('user_query_login')
            ->maxLength('user_query_login', 255)
            ->allowEmptyString('user_query_login');

        $validator
            ->scalar('user_query_password')
            ->maxLength('user_query_password', 255)
            ->allowEmptyString('user_query_password');

        $validator
            ->scalar('ldap_root_search')
            ->maxLength('ldap_root_search', 255)
            ->requirePresence('ldap_root_search', 'create')
            ->notEmptyString('ldap_root_search');

        $validator
            ->scalar('user_login_attribute')
            ->maxLength('user_login_attribute', 512)
            ->requirePresence('user_login_attribute', 'create')
            ->notEmptyString('user_login_attribute');

        $validator
            ->scalar('ldap_users_filter')
            ->maxLength('ldap_users_filter', 65535)
            ->allowEmptyString('ldap_users_filter');

        $validator
            ->scalar('account_prefix')
            ->maxLength('account_prefix', 255)
            ->allowEmptyString('account_prefix');

        $validator
            ->scalar('account_suffix')
            ->maxLength('account_suffix', 255)
            ->allowEmptyString('account_suffix');

        $validator
            ->scalar('description')
            ->allowEmptyString('description');

        $validator
            ->boolean('use_proxy')
            ->allowEmptyString('use_proxy');

        $validator
            ->boolean('use_ssl')
            ->allowEmptyString('use_ssl');

        $validator
            ->boolean('use_tls')
            ->allowEmptyString('use_tls');

        $validator
            ->scalar('user_name_attribute')
            ->maxLength('user_name_attribute', 255)
            ->allowEmptyString('user_name_attribute');

        $validator
            ->scalar('user_mail_attribute')
            ->maxLength('user_mail_attribute', 255)
            ->allowEmptyString('user_mail_attribute');

        $validator
            ->scalar('schema')
            ->maxLength('schema', 255)
            ->notEmptyString('schema');

        $validator
            ->boolean('follow_referrals')
            ->notEmptyString('follow_referrals');

        $validator
            ->integer('version')
            ->notEmptyString('version');

        $validator
            ->integer('timeout')
            ->notEmptyString('timeout');

        $validator
            ->allowEmptyString('custom_options');

        $validator
            ->scalar('user_username_attribute')
            ->maxLength('user_username_attribute', 512)
            ->allowEmptyString('user_username_attribute');

        return $validator;
    }

    /**
     * The Model.beforeMarshall modify request data before it is converted into
     * entities.
     *
     * @param Event       $event
     * @param ArrayObject $data
     * @param ArrayObject $options
     */
    public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options)
    {
        if (!empty($data['custom_options']) && is_array($data['custom_options'])) {
            $data['meta'] = json_encode($data['custom_options']);
        }
    }

    /**
     * Mise à jour des utilisateurs ldap par rapport au nouveau mapping
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     */
    public function afterSave(Event $event, Entity $entity, ArrayObject $options)
    {
        $mappingChanged = $entity->isDirty('user_name_attribute')
            || $entity->isDirty('user_mail_attribute')
            || $entity->isDirty('user_username_attribute')
            || $entity->isDirty('user_login_attribute');
        if ($entity->isNew() === false && $mappingChanged && $entity instanceof Ldap) {
            $Users = TableRegistry::getTableLocator()->get('Users');
            $conn = $Users->getConnection();
            $users = $Users->find()
                ->where(['ldap_id' => $entity->id, 'ldap_login IS NOT' => null]);
            // mise à jour du mapping
            $conn->begin();
            foreach ($users as $user) {
                $entry = $entity->searchWithFilters()->where(
                    $entity->getOriginal('user_login_attribute'),
                    '=',
                    $user->get('ldap_login')
                )->first();
                if ($entry) {
                    $data = [
                        'username' => $entry->getFirstAttribute($entity->get('user_username_attribute')),
                        'name' => $entry->getFirstAttribute($entity->get('user_name_attribute')),
                        'email' => $entry->getFirstAttribute($entity->get('user_mail_attribute')),
                        'ldap_login' => $entry->getFirstAttribute($entity->get('user_login_attribute')),
                    ];
                    $Users->patchEntity($user, $data);
                    $Users->save($user);
                }
            }
            $conn->commit();
        }
    }
}
