<?php
/**
 * AsalaeCore\Model\Table\ArchivingSystemsTable
 */

namespace AsalaeCore\Model\Table;

use Cake\Datasource\EntityInterface;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Table archiving_systems
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin TimestampBehavior
 */
class ArchivingSystemsTable extends Table
{

    /**
     * Configuration initale de la table
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->addBehavior('Timestamp');

        $this->belongsTo('OrgEntities');
    }

    /**
     * Default validation rules.
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name')
            ->add(
                'name',
                [
                    'unique' => [
                        'rule' => ['validateUnique', ['scope' => 'org_entity_id']],
                        'provider' => 'table',
                        'message' => __("Cet identifiant est déjà utilisé pour ce service d'archive")
                    ]
                ]
            );

        $validator
            ->scalar('description')
            ->allowEmptyString('description');

        $validator
            ->scalar('url')
            ->maxLength('url', 255)
            ->requirePresence('url', 'create')
            ->notEmptyString('url');

        $validator
            ->scalar('username')
            ->maxLength('username', 255)
            ->requirePresence('username', 'create')
            ->notEmptyString('username');

        $validator
            ->requirePresence('password', 'create')
            ->notEmptyString('password');

        $validator
            ->boolean('use_proxy')
            ->requirePresence('use_proxy', 'create')
            ->notEmptyString('use_proxy');

        $validator
            ->boolean('active')
            ->requirePresence('active', 'create')
            ->notEmptyString('active');

        $validator
            ->boolean('ssl_verify_peer')
            ->notEmptyString('ssl_verify_peer');

        $validator
            ->boolean('ssl_verify_peer_name')
            ->notEmptyString('ssl_verify_peer_name');

        $validator
            ->integer('ssl_verify_depth')
            ->notEmptyString('ssl_verify_depth');

        $validator
            ->boolean('ssl_verify_host')
            ->notEmptyString('ssl_verify_host');

        $validator
            ->scalar('ssl_cafile')
            ->maxLength('ssl_cafile', 255)
            ->allowEmptyFile('ssl_cafile');

        return $validator;
    }

    /**
     * Allows listeners to modify the rules checker by adding more rules.
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->addDelete(
            function (EntityInterface $entity) {
                return $entity->get('deletable');
            }
        );
        return parent::buildRules($rules);
    }
}
