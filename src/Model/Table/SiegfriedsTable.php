<?php
/**
 * AsalaeCore\Model\Table\SiegfriedsTable
 */

namespace AsalaeCore\Model\Table;

use Cake\ORM\Table;
use Exception;

/**
 * Table siegfrieds
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class SiegfriedsTable extends Table
{
    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        $this->belongsTo('AsalaeCore.Fileuploads');

        parent::initialize($config);
    }

    /**
     * Permet d'obtenir un array d'entités avec les caractéristiques de $filename
     *
     * @param  string $filename
     * @param  bool   $toArray  Transforme les entités en array
     *                          si vrais
     * @return array
     * @throws Exception
     */
    public function newEntitiesByFilename(string $filename, bool $toArray = false): array
    {
        if (!is_file($filename) || !is_readable($filename)) {
            throw new Exception(__('Impossible de lire le fichier'));
        }

        $results = [];
        foreach ($this->siegfried($filename) as $entity) {
            $results[] = $toArray ? $entity->toArray() : $entity;
        }

        return $results;
    }


    /**
     * Effectue un sf et renvoi le rapport de façon structuré
     *
     * @param  string $filename
     * @return array
     */
    private function siegfried(string $filename): array
    {
        exec(
            sprintf('sf -json %s 2> /dev/null', escapeshellarg($filename)),
            $output
        );

        $json = !empty($output[0]) ? json_decode($output[0], true) : null;
        if (!$json || empty($json['files'][0]['matches'])) {
            return [];
        }

        foreach ($json['files'][0]['matches'] as $key => $values) {
            $json['files'][0]['matches'][$key]['pronom']
                = $json['files'][0]['matches'][$key]['id'];
            unset($json['files'][0]['matches'][$key]['id']);
        }

        return $this->newEntities($json['files'][0]['matches']);
    }
}
