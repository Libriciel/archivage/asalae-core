<?php
/**
 * AsalaeCore\Model\Table\AfterDeleteInterface
 */

namespace AsalaeCore\Model\Table;

use ArrayObject;
use Cake\Event\Event;
use Cake\ORM\Entity;

/**
 * Interface pour le callback afterDelete
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
interface AfterDeleteInterface
{
    /**
     * The Model.afterDelete event is fired after an entity is deleted.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     */
    public function afterDelete(Event $event, Entity $entity, ArrayObject $options);
}
