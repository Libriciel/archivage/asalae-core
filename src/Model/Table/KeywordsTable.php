<?php
/**
 * AsalaeCore\Model\Table\KeywordsTable
 */

namespace AsalaeCore\Model\Table;

use Cake\I18n\I18n;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Behavior\TreeBehavior;
use Cake\ORM\Table;
use Cake\Utility\Hash;
use Cake\Utility\Xml;
use Cake\Validation\Validator;
use CodeInc\StripAccents\StripAccents;
use ForceUTF8\Encoding;

/**
 * Table keywords
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin TimestampBehavior
 * @mixin TreeBehavior
 */
class KeywordsTable extends Table
{
    /**
     * Traits
     */
    use UniqueTrait;

    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->addBehavior('Timestamp');

        $this->addBehavior('Tree');

        $this->belongsTo('KeywordLists');
        $this->belongsTo('ParentKeywords')
            ->setClassName('Keywords')
            ->setForeignKey('parent_id');
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->integer('version')
            ->requirePresence('version', 'create')
            ->notEmptyString('version');

        $validator
            ->scalar('code')
            ->maxLength('code', 255)
            ->allowEmptyString('code');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('app_meta')
            ->maxLength('app_meta', 255)
            ->allowEmptyString('app_meta');

        $validator
            ->scalar('exact_match')
            ->maxLength('exact_match', 255)
            ->allowEmptyString('exact_match');

        $validator
            ->scalar('change_note')
            ->maxLength('change_note', 255)
            ->allowEmptyString('change_note');

        $this->addUniqueRule(
            $validator,
            'name',
            __("Ce nom est déjà utilisé"),
            ['scope' => ['keyword_list_id', 'version']]
        );

        return $validator;
    }

    /**
     * Importe un fichier en skos
     * @param string     $rdf             contenu du fichier rdf (fichier skos)
     * @param int|string $keyword_list_id
     * @param int|string $user_id
     * @return array entities
     */
    public function importSkos(string $rdf, $keyword_list_id, $user_id): array
    {
        $xml = Xml::build($rdf);
        $data = Xml::toArray($xml);
        $entities = [];
        $langFull = str_replace('_', '-', strtolower(I18n::getLocale()));
        $lang = explode('-', $langFull)[0];
        foreach (Hash::extract($data, '{s}.skos:Concept')[0] as $concept) {
            if (empty($concept['skos:prefLabel'])) {
                continue;
            }
            $codes = array_filter(
                array_keys($concept),
                function ($v) {
                    return strpos($v, 'identifier') !== false;
                }
            );
            $name = $this->getSkosLabel($concept, array_unique([$langFull, $lang, 'en', 'fr', 'es', 'it', 'de']));
            $code = !empty($codes) ? $concept[current($codes)] : $this->genCode($name);
            $entities[] = $this->newEntity(
                [
                    'keyword_list_id' => $keyword_list_id,
                    'version' => 0,
                    'code' => $code,
                    'name' => $name,
                    'imported_from' => 'rdf',
                    'created_user_id' => $user_id
                ]
            );
        }
        return $entities;
    }

    /**
     * Génère un code à partir d'un nom
     * @param string $name
     * @return string
     */
    private function genCode(string $name): string
    {
        return strtolower(
            preg_replace(
                '/[\W_]+/',
                '_',
                StripAccents::strip($name)
            )
        );
    }

    /**
     * Donne en priorité la première langue mentionnée dans $langs, les autres sinon.
     * Peut donner une langue qui n'est pas dans la liste si aucune langue n'est
     * Disponible.
     * @param array $concept
     * @param array $langs
     * @return string
     */
    private function getSkosLabel(array $concept, array $langs = []): string
    {
        if (empty($concept['skos:prefLabel'])) {
            return '';
        }
        if (isset($concept['skos:prefLabel']['@'])) {
            return $concept['skos:prefLabel']['@'];
        }
        usort(
            $concept['skos:prefLabel'],
            function ($a, $b) use ($langs) {
                $aI = array_search($a['@xml:lang'], $langs);
                $bI = array_search($b['@xml:lang'], $langs);
                if ($aI === false) {
                    $aI = INF;
                }
                if ($bI === false) {
                    $bI = INF;
                }
                return $aI > $bI;
            }
        );
        return current($concept['skos:prefLabel'])['@'];
    }

    /**
     * Importe un fichier en csv
     *
     * $params = [
     *  'separator' => ',',   // Séparateur de colonnes
     *  'delimiter' => '"',   // Délimiteur de texte
     *  'way' => 'row',       // row = un mot clé par ligne, col = un mot clé par colonne
     *  'pos_label_col' => 1, // Numéro de colonne du 1er label
     *  'pos_label_row' => 1, // Numéro de ligne du 1er label
     *  'auto_code' => false, // Défini le code à partir du nom si à true
     *  'pos_code_col' => 1,  // Numéro de colonne du 1er code
     *  'pos_code_row' => 1,  // Numéro de ligne du 1er code
     * ]
     *
     * @param resource   $file
     * @param int|string $keyword_list_id
     * @param int|string $user_id
     * @param array      $params
     * @return array $entities
     */
    public function importCsv($file, $keyword_list_id, $user_id, array $params = []): array
    {
        $params += [
            'separator' => ',',
            'delimiter' => '"',
            'way' => 'row',
            'pos_label_col' => 1,
            'pos_label_row' => 1,
            'auto_code' => false,
            'pos_code_col' => 2,
            'pos_code_row' => 1,
        ];
        $params['pos_label_col'] = (int)$params['pos_label_col'];
        $params['pos_label_row'] = (int)$params['pos_label_row'];
        $params['auto_code'] = (bool)$params['auto_code'];
        $params['pos_code_col'] = (int)$params['pos_code_col'];
        $params['pos_code_row'] = (int)$params['pos_code_row'];
        rewind($file);
        $row = 1;
        $labels = [];
        $codes = [];
        while (($data = fgetcsv($file, 0, $params['separator'], $params['delimiter'])) !== false) {
            if ($params['way'] === 'row') {
                if ($row >= $params['pos_label_row']) {
                    $labels[] = Encoding::toUTF8($data[$params['pos_label_col']-1] ?? '');
                }
                if (!$params['auto_code'] && $row >= $params['pos_code_row']) {
                    $codes[] = Encoding::toUTF8($data[$params['pos_code_col']-1] ?? '');
                }
            } else {
                $count = count($data);
                if ($row === $params['pos_label_row']) {
                    for ($i = $params['pos_label_col'] -1; $i < $count; $i++) {
                        $labels[] = Encoding::toUTF8($data[$i]);
                    }
                }
                if ($row === $params['pos_code_row']) {
                    for ($i = $params['pos_code_col'] -1; $i < $count; $i++) {
                        $codes[] = Encoding::toUTF8($data[$i]);
                    }
                }
            }
            $row++;
        }
        $entities = [];
        foreach ($labels as $key => $name) {
            if (empty($name)) {
                continue;
            }
            $code = $params['auto_code'] || empty($codes[$key])
                ? $this->genCode($name)
                : $codes[$key];
            $entities[] = $this->newEntity(
                [
                    'keyword_list_id' => $keyword_list_id,
                    'version' => 0,
                    'code' => $code,
                    'name' => $name,
                    'imported_from' => 'rdf',
                    'created_user_id' => $user_id
                ]
            );
        }
        return $entities;
    }
}
