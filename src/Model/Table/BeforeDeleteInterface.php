<?php
/**
 * AsalaeCore\Model\Table\BeforeDeleteInterface
 */

namespace AsalaeCore\Model\Table;

use ArrayObject;
use Cake\Event\Event;
use Cake\ORM\Entity;

/**
 * Interface pour le callback beforeDelete
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
interface BeforeDeleteInterface
{
    /**
     * The Model.beforeDelete Fired before an entity is deleted.
     * By stopping this event you will abort the delete operation.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     */
    public function beforeDelete(Event $event, Entity $entity, ArrayObject $options);
}
