<?php
/**
 * AsalaeCore\Model\Table\FileExtensionsPronomsTable
 */

namespace AsalaeCore\Model\Table;

use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Table;

/**
 * Table file_extensions_pronoms
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin TimestampBehavior
 */
class FileExtensionsPronomsTable extends Table
{

    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        $this->belongsTo('FileExtensions');
        $this->belongsTo('Pronoms');
        parent::initialize($config);
    }
}
