<?php
/**
 * AsalaeCore\Model\Table\ServiceLevelsTable
 */

namespace AsalaeCore\Model\Table;

use ArrayObject;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Entity;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Exception;

/**
 * Table service_levels
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin TimestampBehavior
 */
class ServiceLevelsTable extends Table implements BeforeSaveInterface, AfterSaveInterface
{

    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->addBehavior('Timestamp');

        $this->belongsTo('OrgEntities');
        $this->belongsTo('TsMsgs')->setClassName('OrgEntitiesTimestampers');
        $this->belongsTo('TsPjss')->setClassName('OrgEntitiesTimestampers');
        $this->belongsTo('TsConvs')->setClassName('OrgEntitiesTimestampers');
        $this->belongsTo('SecureDataSpaces');
        $this->hasMany('Archives');
        $this->hasMany('Transfers');
        $this->belongsToMany('Agreements');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('identifier')
            ->maxLength('identifier', 255)
            ->requirePresence('identifier', 'create')
            ->notEmptyString('identifier');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('description')
            ->allowEmptyString('description');

        $validator
            ->boolean('active')
            ->requirePresence('active', 'create')
            ->notEmptyString('active');

        $validator
            ->boolean('default_level')
            ->notEmptyString('default_level');

        $validator->add(
            'identifier',
            ['unique' => [
                'rule' => ['validateUnique', ['scope' => 'org_entity_id']],
                'provider' => 'table',
                'message' => __("Cet identifiant est déjà utilisé")
            ]
            ]
        );
        $validator->add(
            'default_level',
            'custom',
            [
                'rule' => function ($value, $context) {
                    if ($value) {
                        return true;
                    }
                    if (empty($context['data']['org_entity_id'])) {
                        throw new Exception('"org_entity_id" is needed for validate the "default_level" field');
                    }
                    $conditions = [
                        'org_entity_id' => $context['data']['org_entity_id'],
                        'default_level' => true,
                    ];
                    if (!$context['newRecord']) {
                        $conditions['id !='] = $context['data']['id'];
                    }
                    return $this->exists($conditions);
                },
                'message' => __("Impossible de retirer une valeur par défaut (veuillez en définir un autre avant)"),
            ]
        );

        return $validator;
    }

    /**
     * The Model.afterSave event is fired after an entity is saved.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     */
    public function afterSave(Event $event, Entity $entity, ArrayObject $options)
    {
        if (in_array('default_level', $entity->get('_dirty')) && $entity->get('default_level')) {
            $this->updateAll(
                ['default_level' => false],
                [
                    'id !=' => $entity->get('id'),
                    'org_entity_id' => $entity->get('org_entity_id'),
                ]
            );
        }
    }

    /**
     * The Model.beforeSave event is fired before an entity is saved.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     */
    public function beforeSave(Event $event, Entity $entity, ArrayObject $options)
    {
        $entity->set('_dirty', $entity->getDirty());
    }

    /**
     * Donne l'entité par défaut pour un service d'archive donné
     * @param int|string $org_entity_id
     * @return EntityInterface|null
     */
    public function getDefault($org_entity_id)
    {
        return $this->find()
            ->where(
                [
                    'ServiceLevels.org_entity_id' => $org_entity_id,
                    'ServiceLevels.default_level' => true,
                ]
            )
            ->contain(['SecureDataSpaces'])
            ->first();
    }
}
