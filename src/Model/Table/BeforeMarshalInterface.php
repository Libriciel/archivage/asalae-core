<?php
/**
 * AsalaeCore\Model\Table\BeforeMarshalInterface
 */

namespace AsalaeCore\Model\Table;

use ArrayObject;
use Cake\Event\Event;

/**
 * Interface pour le callback beforeMarshal
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
interface BeforeMarshalInterface
{
    /**
     * The Model.beforeMarshall modify request data before it is converted into
     * entities.
     *
     * @param Event       $event
     * @param ArrayObject $data
     * @param ArrayObject $options
     */
    public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options);
}
