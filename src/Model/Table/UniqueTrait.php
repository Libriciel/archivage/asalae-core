<?php
/**
 * AsalaeCore\Model\Table\UniqueTrait
 */

namespace AsalaeCore\Model\Table;

use AsalaeCore\Validation\ValidationRule;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Trait pour l'ajout de règle isUnique custom (avec contexte multiple)
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin Table
 */
trait UniqueTrait
{

    /**
     * Ajoute une règle d'unicité sur un champ
     * Il est possible d'avoir un scope à plusieurs colonnes contrairement à la
     * règle native "isUnique"
     * @param Validator $validator
     * @param string    $field
     * @param string    $message
     * @param array     $params    ['scope' => ['liste', 'des', 'champs',
     *                             'supplémentaires']]
     */
    public function addUniqueRule(
        Validator $validator,
        string $field,
        string $message,
        array $params = ['scope' => []]
    ) {
        $validator->add(
            $field,
            'unique_custom',
            new ValidationRule(
                [
                    'rule' => function ($value, $context) use ($validator, $params) {
                        $conditions = [$context['field'] => $value];
                        foreach ((array)($params['scope'] ?? []) as $field) {
                            if (!isset($context['data'][$field])) {
                                /** @var ValidationRule $rule */
                                $rule = $validator
                                ->field($context['field'])
                                ->rule('unique_custom');
                                $rule->setMessage(
                                    __(
                                        "Le champ ''{0}'' nécessaire à la validation de l'unicité est manquant.",
                                        $field
                                    )
                                );
                                return false;
                            }
                            $conditions[$field] = $context['data'][$field];
                        }
                        $count = $this->find()->where($conditions)->count();
                        return $context['newRecord'] ? $count === 0 : $count <= 1;
                    },
                    'message' => $message
                ]
            )
        );
    }
}
