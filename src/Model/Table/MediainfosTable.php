<?php
/**
 * AsalaeCore\Model\Table\MediainfosTable
 */

namespace AsalaeCore\Model\Table;

use Cake\Datasource\EntityInterface;
use Cake\ORM\Table;
use Exception;

/**
 * Table mediainfos
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class MediainfosTable extends Table
{
    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        $this->belongsTo('AsalaeCore.Fileuploads');
        $this->hasMany('AsalaeCore.MediainfoVideos');
        $this->hasMany('AsalaeCore.MediainfoAudios');
        $this->hasMany('AsalaeCore.MediainfoTexts');
        $this->hasMany('AsalaeCore.MediainfoImages');

        parent::initialize($config);
    }

    /**
     * Permet d'obtenir une entité avec les caractéristiques de $filename
     *
     * @param  string $filename
     * @return EntityInterface
     * @throws Exception
     */
    public function newEntityByFilename($filename): EntityInterface
    {
        if (!is_file($filename) || !is_readable($filename)) {
            throw new Exception('Impossible de lire le fichier');
        }

        $data = [];
        $mediainfo = $this->mediainfo($filename);

        foreach ($mediainfo as $category => $values) {
            if (preg_match('/^(\w+) #\d+$/', $category, $match)) {
                $category = end($match);
            }

            switch ($category) {
                case 'Video':
                    $data['mediainfo_videos'][] = [];
                    break;
                case 'Audio':
                    $data['mediainfo_audios'][] = [];
                    break;
                case 'Text':
                    $data['mediainfo_texts'][] = [];
                    break;
                case 'Image':
                    $data['mediainfo_images'][] = [];
                    break;
            }

            unset($values['id']);

            foreach ($values as $key => $value) {
                switch ($category) {
                    case 'General':
                        $data[$key] = $value;
                        break;
                    case 'Video':
                        $k = count($data['mediainfo_videos']) -1;
                        $data['mediainfo_videos'][$k][$key] = $value;
                        break;
                    case 'Audio':
                        $k = count($data['mediainfo_audios']) -1;
                        $data['mediainfo_audios'][$k][$key] = $value;
                        break;
                    case 'Text':
                        $k = count($data['mediainfo_texts']) -1;
                        $data['mediainfo_texts'][$k][$key] = $value;
                        break;
                    case 'Image':
                        $k = count($data['mediainfo_images']) -1;
                        $data['mediainfo_images'][$k][$key] = $value;
                        break;
                }
            }
        }

        return $this->newEntity(
            $data,
            [
                'associated' => ['MediainfoVideos', 'MediainfoAudios', 'MediainfoTexts']
            ]
        );
    }

    /**
     * Effectue un mediainfo et renvoi le rapport de façon structuré
     *
     * @param  string $filename
     * @return array
     */
    private function mediainfo(string $filename): array
    {
        exec(
            sprintf('mediainfo %s 2> /dev/null', escapeshellarg($filename)),
            $output
        );

        $cat = 'default';
        $result = [];
        foreach ($output as $str) {
            if (strpos($str, ':')) {
                list($field, $value) = explode(':', $str, 2);
                $result[$cat][preg_replace('/\W+/', '_', trim(strtolower($field)))]
                    = trim($value);
            } elseif (!empty($str)) {
                $cat = $str;
            }
        }

        return $result;
    }
}
