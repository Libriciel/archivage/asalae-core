<?php
/**
 * AsalaeCore\Model\Table\CronsTable
 */

namespace AsalaeCore\Model\Table;

use ArrayObject;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Entity;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

/**
 * Table crons
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property CronExecutionsTable|HasMany CronExecutions
 */
class CronsTable extends Table implements AfterDeleteInterface
{

    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->hasMany('CronExecutions')
            ->setDependent(true);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('description')
            ->requirePresence('description', 'create')
            ->notEmptyString('description');

        $validator
            ->scalar('classname')
            ->maxLength('classname', 255)
            ->requirePresence('classname', 'create')
            ->notEmptyString('classname');

        $validator
            ->boolean('active')
            ->requirePresence('active', 'create')
            ->notEmptyString('active');

        $validator
            ->boolean('locked')
            ->requirePresence('locked', 'create')
            ->notEmptyString('locked');

        $validator
            ->scalar('frequency')
            ->maxLength('frequency', 255)
            ->requirePresence('frequency', 'create')
            ->notEmptyString('frequency');

        $validator
            ->dateTime('next')
            ->allowEmptyDateTime('next');

        $validator
            ->scalar('app_meta')
            ->allowEmptyString('app_meta');

        $validator
            ->boolean('parallelisable')
            ->notEmptyString('parallelisable');

        $validator
            ->scalar('max_duration')
            ->maxLength('max_duration', 255)
            ->notEmptyString('max_duration');

        $validator
            ->dateTime('last_email')
            ->allowEmptyDateTime('last_email');

        return $validator;
    }

    /**
     * The Model.afterDelete event is fired after an entity is deleted.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     */
    public function afterDelete(Event $event, Entity $entity, ArrayObject $options)
    {
        $CronExecutions = TableRegistry::getTableLocator()->get('CronExecutions');
        $CronExecutions->deleteAll(
            [
                'cron_id IS' => null,
                'state !=' => 'success',
            ]
        );
    }

    /**
     * Allows listeners to modify the rules checker by adding more rules.
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->addDelete(
            function (EntityInterface $entity) {
                return $entity->get('deletable');
            }
        );
        return parent::buildRules($rules);
    }
}
