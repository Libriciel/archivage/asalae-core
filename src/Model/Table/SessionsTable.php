<?php
/**
 * AsalaeCore\Model\Table\SessionsTable
 */

namespace AsalaeCore\Model\Table;

use ArrayObject;
use AsalaeCore\Factory\Utility;
use Cake\Event\Event;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Exception;

/**
 * Table sessions
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin TimestampBehavior
 */
class SessionsTable extends Table implements BeforeSaveInterface, BeforeFindInterface
{

    /**
     * La table est t-elle synchronisée ?
     *
     * @var boolean
     */
    public $sync = false;

    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->addBehavior('Timestamp');

        $this->belongsTo('Users');
    }

    /**
     * Supprime les sessions expirés
     *
     * @param \Cake\Event\Event $event The beforeFind event
     * @param Query             $query Query
     * @return Query
     */
    public function beforeFind(Event $event, $query)
    {
        if (!$this->sync) {
            $this->sync = true;
            $this->deleteAll(['expires <' => time()]);
        }
        return $query;
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->scalar('id')
            ->maxLength('id', 40)
            ->allowEmptyString('id', null, 'create');

        $validator
            ->requirePresence('data', 'create')
            ->notEmptyString('data');

        $validator
            ->integer('expires')
            ->requirePresence('expires', 'create')
            ->notEmptyString('expires');

        return $validator;
    }

    /**
     * The Model.beforeSave event is fired before an entity is saved.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     * @throws Exception
     */
    public function beforeSave(Event $event, Entity $entity, ArrayObject $options)
    {
        $decoded = Utility::get('Session')->unserialize($entity->get('data'));
        $entity->set('token', $decoded['Session']['token'] ?? null);
        $entity->set('user_id', $decoded['Auth']['id'] ?? null);
    }
}
