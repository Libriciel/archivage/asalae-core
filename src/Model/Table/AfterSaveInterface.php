<?php
/**
 * AsalaeCore\Model\Table\AfterSaveInterface
 */

namespace AsalaeCore\Model\Table;

use ArrayObject;
use Cake\Event\Event;
use Cake\ORM\Entity;

/**
 * Interface pour le callback afterSave
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
interface AfterSaveInterface
{
    /**
     * The Model.afterSave event is fired after an entity is saved.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     */
    public function afterSave(Event $event, Entity $entity, ArrayObject $options);
}
