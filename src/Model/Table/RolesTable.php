<?php
/**
 * AsalaeCore\Model\Table\RolesTable
 */

namespace AsalaeCore\Model\Table;

use ArrayObject;
use AsalaeCore\Model\Behavior\AclBehavior;
use Cake\Event\Event;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Behavior\TreeBehavior;
use Cake\ORM\Entity;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Cake\Validation\Validator;

/**
 * Table roles
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin AclBehavior
 * @mixin TimestampBehavior
 * @mixin TreeBehavior
 */
class RolesTable extends Table implements AfterSaveInterface
{
    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->addBehavior('AsalaeCore.Acl', ['type' => 'requester']);
        $this->addBehavior('Timestamp');
        $this->addBehavior('Tree');

        $this->belongsTo('ParentRoles')
            ->setClassName('Roles')
            ->setForeignKey('parent_id');
        $this->belongsToMany('TypeEntities');
        $this->belongsTo('OrgEntities');
        $this->hasMany('ChildRoles')
            ->setClassName('Roles')
            ->setForeignKey('parent_id');
        $this->hasMany('Users');
        $this->hasMany('RolesTypeEntities');
    }

    /**
     * Met à jour l'alias de l'aro lors de la modification d'un role
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     */
    public function afterSave(Event $event, Entity $entity, ArrayObject $options)
    {
        $Aros = TableRegistry::getTableLocator()->get('Aros');
        $name = $entity->get('name');
        if (empty($name)) {
            $name = $this->find()
                ->select(['name'])
                ->where(['id' => $entity->get('id')])
                ->first()
                ->get('name');
        }
        $Aros->query()
            ->update()
            ->set(['alias' => $name])
            ->where(['Aros.model' => 'Roles', 'Aros.foreign_key' => $entity->get('id')])
            ->execute();
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->boolean('active')
            ->requirePresence('active', 'create')
            ->notEmptyString('active');

        $validator
            ->scalar('code')
            ->maxLength('code', 255)
            ->allowEmptyString('code');

        $validator->add(
            'code',
            [
                'unique' => [
                    'rule' => 'validateUnique',
                    'provider' => 'table',
                    'message' => __("Ce code de rôle est déjà utilisé")
                ]
            ]
        );

        $validator
            ->boolean('hierarchical_view')
            ->notEmptyString('hierarchical_view');

        $validator->add(
            'name',
            [
                'unique' => [
                    'rule' => function ($value, array $context) {
                        $conditions = [
                            'name' => $value,
                            'OR' => [
                                'org_entity_id IS' => null,
                                'org_entity_id' => Hash::get($context, 'data.org_entity_id', 0),
                            ],
                        ];
                        if (!$context['newRecord']) {
                            $conditions['id !='] = Hash::get($context, 'data.id', 0);
                        }
                        $count = $this->find()
                            ->where($conditions)
                            ->count();
                        return $count === 0;
                    },
                    'message' => __("Ce nom de rôle est déjà utilisé"),
                ]
            ]
        );

        $validator
            ->scalar('description')
            ->allowEmptyString('description');

        $validator
            ->scalar('agent_type')
            ->maxLength('agent_type', 255)
            ->notEmptyString('agent_type');

        return $validator;
    }


    /**
     * The Model.beforeDelete Fired before an entity is deleted.
     * By stopping this event you will abort the delete operation.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     */
    public function beforeDelete(Event $event, Entity $entity, ArrayObject $options)
    {
    }
}
