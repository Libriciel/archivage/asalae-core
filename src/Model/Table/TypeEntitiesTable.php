<?php
/**
 * AsalaeCore\Model\Table\TypeEntitiesTable
 */

namespace AsalaeCore\Model\Table;

use ArrayObject;
use AsalaeCore\Form\MessageSchema\MessageSchema;
use AsalaeCore\Model\Behavior\AclBehavior;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Entity;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

/**
 * Table type_entities
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin AclBehavior
 * @mixin TimestampBehavior
 */
class TypeEntitiesTable extends Table implements AfterSaveInterface, AfterDeleteInterface
{

    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        $this->addBehavior('Timestamp');

        $this->hasMany('OrgEntities');
        $this->hasMany('RolesTypeEntities');

        parent::initialize($config);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator->notEmptyString('name');
        $validator->add(
            'name',
            ['unique' => [
                'rule' => 'validateUnique',
                'provider' => 'table',
                'message' => __("Ce nom de type d'entité est déjà utilisé")
            ]
            ]
        );

        return $validator;
    }

    /**
     * The Model.afterSave event is fired after an entity is saved.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     */
    public function afterSave(Event $event, Entity $entity, ArrayObject $options)
    {
        if ($entity->isNew() || $entity->isDirty('name') || $entity->isDirty('code')) {
            $query = TableRegistry::getTableLocator()->get('OrgEntities')->find()
                ->where(['type_entity_id' => $entity->get('id')]);
            /** @var EntityInterface $orgEntity */
            foreach ($query as $orgEntity) {
                MessageSchema::clearCache($orgEntity->get('archival_agency_id'));
            }
        }
    }

    /**
     * The Model.afterDelete event is fired after an entity is deleted.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     */
    public function afterDelete(Event $event, Entity $entity, ArrayObject $options)
    {
        $query = TableRegistry::getTableLocator()->get('OrgEntities')->find()
            ->where(['type_entity_id' => $entity->get('id')]);
        /** @var EntityInterface $orgEntity */
        foreach ($query as $orgEntity) {
            MessageSchema::clearCache($orgEntity->get('archival_agency_id'));
        }
    }
}
