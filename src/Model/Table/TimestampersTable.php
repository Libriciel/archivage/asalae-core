<?php
/**
 * AsalaeCore\Model\Table\TimestampersTable
 */

namespace AsalaeCore\Model\Table;

use ArrayObject;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Entity;
use Cake\ORM\Table;
use Cake\Utility\Security;
use Cake\Validation\Validator;
use Exception;

/**
 * Table timestampers
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin TimestampBehavior
 */
class TimestampersTable extends Table implements BeforeSaveInterface
{
    const SECURITY_KEY = 'timestamper';

    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        $this->addBehavior('Timestamp');

        $this->belongsToMany('OrgEntities');

        parent::initialize($config);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator->notEmptyString('name');
        $validator->notEmptyString('fields');
        $validator->add(
            'name',
            ['unique' => [
                'rule' => 'validateUnique',
                'provider' => 'table',
                'message' => __("Ce nom est déjà utilisé")
            ]
            ]
        );
        $validator->add(
            'fields',
            'custom',
            [
                'rule' => function ($value) {
                    if (empty($value)) {
                        return false;
                    }
                    try {
                        json_decode($value);
                        return true;
                    } catch (Exception $e) {
                        return false;
                    }
                },
                'message' => __("N'est pas un JSON valide")
            ]
        );

        return $validator;
    }

    /**
     * The Model.beforeSave event is fired before an entity is saved.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     */
    public function beforeSave(Event $event, Entity $entity, ArrayObject $options)
    {
        $fields = Configure::read('Timestamping.drivers.'.$entity->get('driver').'.fields', []);
        foreach ($fields as $field => $params) {
            $type = $params['type'] ?? '';
            if ($type === 'password'
                && $entity->isDirty($field)
                && ($entity->get($field) !== $entity->getOriginal($field) || $entity->isNew())
            ) {
                $password = $entity->get($field);
                $entity->set(
                    $field,
                    base64_encode(
                        Security::encrypt(
                            $password,
                            md5(self::SECURITY_KEY) // NOSONAR permet simplement d'avoir la bonne taille
                        )
                    )
                );
            }
        }
    }
}
