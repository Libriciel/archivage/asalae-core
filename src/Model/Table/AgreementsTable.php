<?php
/**
 * AsalaeCore\Model\Table\AgreementsTable
 */

namespace AsalaeCore\Model\Table;

use ArrayObject;
use AsalaeCore\Model\Behavior\OptionsBehavior;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Entity;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Utility\Hash;
use Cake\Validation\Validator;

/**
 * Table Agreements
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin TimestampBehavior
 * @mixin OptionsBehavior
 */
class AgreementsTable extends Table implements BeforeSaveInterface, AfterSaveInterface
{

    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        $this->addBehavior('Timestamp');
        $this->addBehavior(
            'AsalaeCore.Options',
            [
                'transfer_period' => [
                    'year',
                    'month',
                    'week',
                    'day',
                ]
            ]
        );

        // service d'archives
        $this->belongsTo('OrgEntities');
        // circuit de validation des transferts conformes
        $this->belongsTo(
            'ProperChains',
            [
                'className' => 'ValidationChains'
            ]
        );
        // circuit de validation des transferts non conformes
        $this->belongsTo(
            'ImproperChains',
            [
                'className' => 'ValidationChains'
            ]
        );
        $this->belongsToMany(
            'TransferringAgencies',
            [
                'className' => 'OrgEntities',
                'joinTable' => 'agreements_transferring_agencies',
                'targetForeignKey' => 'org_entity_id',
            ]
        );
        $this->belongsToMany(
            'OriginatingAgencies',
            [
                'className' => 'OrgEntities',
                'joinTable' => 'agreements_originating_agencies',
                'targetForeignKey' => 'org_entity_id',
            ]
        );
        $this->belongsToMany('ServiceLevels');
        $this->belongsToMany('Profiles');

        parent::initialize($config);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator->notEmptyString('identifier');
        $validator->notEmptyString('name');
        $validator->notEmptyString('org_entity_id');
        $validator->notEmptyString('improper_chain_id');
        $validator->allowEmptyString(
            'proper_chain_id',
            __("Il faut choisir un circuit de validation des transferts conformes"),
            function ($context) {
                return !empty($context['data']['auto_validate']);
            }
        );
        $validator->add(
            'allow_all_transferring_agencies',
            'custom',
            [
                'rule' => function ($value, $context) {
                    if (!empty($context['data']['transferring_agencies']['_ids'])) {
                        return true;
                    }
                    return (bool)$value;
                },
                'message' => __("Il faut choisir un ou plusieurs services versants")
            ]
        );
        $validator->add(
            'allow_all_originating_agencies',
            'custom',
            [
                'rule' => function ($value, $context) {
                    if (!empty($context['data']['originating_agencies']['_ids'])) {
                        return true;
                    }
                    return (bool)$value;
                },
                'message' => __("Il faut choisir un ou plusieurs services producteurs")
            ]
        );
        $validator->add(
            'allow_all_service_levels',
            'custom',
            [
                'rule' => function ($value, $context) {
                    if (!empty($context['data']['service_levels']['_ids'])) {
                        return true;
                    }
                    return (bool)$value;
                },
                'message' => __("Il faut choisir un ou plusieurs niveaux de service")
            ]
        );
        $validator->add(
            'identifier',
            ['unique' => [
                'rule' => ['validateUnique', ['scope' => 'org_entity_id']],
                'provider' => 'table',
                'message' => __("Cet identifiant est déjà utilisé")
            ]
            ]
        );

        $validator->add(
            'default_agreement',
            'custom',
            [
                'rule' => function ($value, $context) {
                    if ($value || empty($context['data']['org_entity_id'])) {
                        return true;
                    }
                /** @var Table $table */
                    $table = $context['providers']['table'];
                    $conditions = [
                        'org_entity_id' => Hash::get($context, 'data.org_entity_id'),
                        'default_agreement IS' => true,
                    ];
                    if (!empty($id = Hash::get($context, 'data.id'))) {
                        $conditions['id !='] = $id;
                    }
                    $count = $table->find()
                    ->where($conditions)
                    ->count();
                    return $count > 0;
                },
                'message' => __("Il doit y avoir un accord de versement par défaut")
            ]
        );

        return $validator;
    }

    /**
     * The Model.afterSave event is fired after an entity is saved.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     */
    public function afterSave(Event $event, Entity $entity, ArrayObject $options)
    {
        if (in_array('default_agreement', $entity->get('_dirty')) && $entity->get('default_agreement')) {
            $this->updateAll(
                ['default_agreement' => false],
                [
                    'id !=' => $entity->get('id'),
                    'org_entity_id' => $entity->get('org_entity_id'),
                ]
            );
        }
    }

    /**
     * The Model.beforeSave event is fired before an entity is saved.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     */
    public function beforeSave(Event $event, Entity $entity, ArrayObject $options)
    {
        $entity->set('_dirty', $entity->getDirty());
    }

    /**
     * Allows listeners to modify the rules checker by adding more rules.
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->addDelete(
            function (EntityInterface $entity) {
                return $entity->get('deletable') && !$entity->get('default_agreement');
            }
        );
        return parent::buildRules($rules);
    }
}
