<?php
/**
 * AsalaeCore\Model\Table\StateChangeInterface
 */

namespace AsalaeCore\Model\Table;

use Cake\Datasource\EntityInterface;

/**
 * Interface pour le callback onStateChange
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
interface StateChangeInterface
{
    /**
     * Appelé après la transition
     * @param EntityInterface $entity
     * @param string          $state
     */
    public function onStateChange(EntityInterface $entity, string $state);
}
