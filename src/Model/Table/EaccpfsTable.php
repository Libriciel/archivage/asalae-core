<?php
/**
 * AsalaeCore\Model\Table\EaccpfsTable
 */

namespace AsalaeCore\Model\Table;

use AsalaeCore\Error\AppErrorHandler;
use AsalaeCore\Model\Behavior\AliasBehavior;
use AsalaeCore\Model\Behavior\OptionsBehavior;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\I18n\I18n;
use Cake\Log\Log;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Craur;
use DOMDocument;
use DOMElement;
use DateTime;
use ErrorException;
use Exception;
use IntlDateFormatter;
use Throwable;

/**
 * Table eaccpfs
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin TimestampBehavior
 * @mixin OptionsBehavior
 * @mixin AliasBehavior
 */
class EaccpfsTable extends Table
{
    /**
     * @var string Classe de DOMDocument à utiliser
     */
    public static $domDocumentClassname = DOMDocument::class;

    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        $this->addBehavior('Timestamp');
        $this->addBehavior('AsalaeCore.Alias');
        $this->addBehavior(
            'AsalaeCore.Options',
            [
                'entity_type' => [
                    'person',
                    'family',
                    'corporateBody',
                ]
            ]
        );

        $this->belongsTo('OrgEntities');

        parent::initialize($config);
    }

    /**
     * Crée une entité à partir d'un chemin vers un fichier xml
     *
     * @link http://eac.staatsbibliothek-berlin.de/Diagram/cpf.html
     *
     * @param string $xmlString chemin vers le fichier xml ou (string)xml
     * @param array  $options
     * @return EntityInterface
     * @throws Exception
     */
    public function newEntityFromXml(string $xmlString, array $options = [])
    {
        /**
         * @var DOMDocument $xml
         * @var DOMElement $recordId
         * @var DOMElement $cpfDescription
         * @var DOMElement $identity
         * @var DOMElement|null $descriptiveNote
         * @var DOMElement|null $p
         * @var DOMElement $description
         * @var DOMElement|null $existDates
         * @var DOMElement|null $entityId
         * @var DOMElement|null $entityType
         * @var DOMElement|null $nameEntry
         * @var DOMElement|null $parts
         * @var DOMElement $agencyName
         * @var DOMElement|null $dateRange
         * @var DOMElement|null $fromDate
         * @var DOMElement|null $toDate
         * @var DOMElement|null $date
         */
        $xml = new self::$domDocumentClassname;
        if (is_file($xmlString)) {
            $xml->load($xmlString);
        } else {
            $xml->loadXML($xmlString);
        }

        if (!@$xml->schemaValidate(EAC_CPF_XSD)) {
            return $this->newEntity(['name' => '', 'data' => '']);
        }

        $recordId = $xml->getElementsByTagName('recordId')->item(0);
        $cpfDescription = $xml->getElementsByTagName('cpfDescription')->item(0);
        $identity = $cpfDescription->getElementsByTagName('identity')->item(0);
        $descriptiveNote = $identity
            ? $cpfDescription->getElementsByTagName('descriptiveNote')->item(0)
            : null;
        $p = $descriptiveNote ? $descriptiveNote->getElementsByTagName('p')->item(0) : null;

        $description = $cpfDescription->getElementsByTagName('description')->item(0);
        $existDates = $description ? $description->getElementsByTagName('existDates')->item(0) : null;
        $entityId = $identity ? $identity->getElementsByTagName('entityId')->item(0) : null;
        $entityType = $identity ? $identity->getElementsByTagName('entityType')->item(0) : null;
        $nameEntry = $identity ? $identity->getElementsByTagName('nameEntry')->item(0) : null;
        $parts = $nameEntry ? $nameEntry->getElementsByTagName('part') : null;
        $agencyName = $xml->getElementsByTagName('agencyName')->item(0);
        $partArray = [];
        foreach ($parts as $part) {
            $partArray[] = $part->textContent;
        }

        $from_date = null;
        $to_date = null;
        if ($existDates) {
            // dateset contient plusieurs dates, on ne prend que la 1ère
            if (!$dateSet = $existDates->getElementsByTagName('dateSet')->item(0)) {
                $dateSet = $existDates;
            }

            if ($dateRange = $dateSet->getElementsByTagName('dateRange')->item(0)) {
                if ($fromDate = $dateRange->getElementsByTagName('fromDate')->item(0)) {
                    $from_date = $this->elementToDate($fromDate);
                }
                if ($toDate = $dateRange->getElementsByTagName('toDate')->item(0)) {
                    $to_date = $this->elementToDate($toDate);
                }
            } elseif ($date = $dateSet->getElementsByTagName('date')->item(0)) {
                $from_date = $this->elementToDate($date);
            }
        }

        $data = [
            'record_id' => $recordId->textContent,
            'name' => implode(', ', $partArray),
            'entity_id' => $entityId ? $entityId->textContent : null,
            'entity_type' => $entityType->textContent,
            'from_date' => $from_date,
            'to_date' => $to_date,
            'agency_name' => $agencyName->textContent,
            'data' => Craur::createFromXml($xml->saveXML())->toJsonString(),
            'description' => $p ? $p->textContent : null,
        ];

        return $this->newEntity($data, $options);
    }

    /**
     * Permet d'obtenir une date à partir d'une element date
     *
     * Récupère en priorité dans l'ordre :
     * - l'attribut standardDate
     * - l'attribut notBefore
     * - l'attribut notAfter
     * Sinon essaye de récupérer la date dans le texte
     *
     * @link http://eac.staatsbibliothek-berlin.de/Diagram/cpf.html#id57
     * @link https://en.wikipedia.org/wiki/ISO_8601
     *
     * @param DOMElement $element
     * @return DateTime|null Null si non trouvé
     */
    private function elementToDate(DOMElement $element)
    {
        if (($attr = $element->getAttribute('standardDate'))
            || ($attr = $element->getAttribute('notBefore'))
            || ($attr = $element->getAttribute('notAfter'))
        ) {
            if (preg_match('/^\d{4}$/', $attr)) {
                $format = '!Y';
            } elseif (preg_match('/^\d{4}-\d{2}$/', $attr)) {
                $format = '!Y-m';
            } elseif (preg_match('/^\d{6}$/', $attr)) {
                $format = '!Ym';
            } elseif (preg_match('/^\d{8}$/', $attr)) {
                $format = '!Ymd';
            } else {
                $format = '!Y-m-d';
            }
            return \DateTime::createFromFormat($format, $attr);
        }
        // Si une date normé n'est pas trouvé, on tente de récupérer la date dans le texte
        if (preg_match('/(\d{4})-(\d{1,2})-(\d{1,2})/', $element->textContent, $matches)) {
            return \DateTime::createFromFormat('!Y-m-d', "$matches[1]-$matches[2]-$matches[3]");
        } elseif (preg_match('/(\d{4})\/(\d{1,2})\/(\d{1,2})/', $element->textContent, $matches)) {
            return \DateTime::createFromFormat('!Y-m-d', "$matches[1]-$matches[2]-$matches[3]");
        } elseif (preg_match('/(\d{1,2})\/(\d{1,2})\/(\d{4})/', $element->textContent, $matches)) {
            return \DateTime::createFromFormat('!Y-m-d', "$matches[3]-$matches[2]-$matches[1]");
        } elseif (preg_match('/(\d{4})/', $element->textContent, $matches)) {
            return \DateTime::createFromFormat('!Y', $matches[1]);
        } else {
            return null;
        }
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator->notEmptyString('name');
        $validator->notEmptyString('record_id');
        $validator->add(
            'record_id',
            'sameAsEntity',
            [
                'rule' => function ($value, $context) {
                    if (empty($context['data']['org_entity_id'])) {
                        return true;
                    }
                    $OrgEntities = TableRegistry::getTableLocator()->get('OrgEntities');
                    $orgEntity = $OrgEntities->get($context['data']['org_entity_id']);
                    return $value === $orgEntity->get('identifier');
                },
                'message' => __("Cet identifiant est différent de celui de son entité")
            ]
        );

        $validator->notEmptyString('entity_type');
        $validator->notEmptyString('agency_name');
        $validator->notEmptyString('data');

        $formatter = new IntlDateFormatter(
            I18n::getDefaultLocale(),
            IntlDateFormatter::SHORT,
            IntlDateFormatter::NONE
        );
        $localFormat = preg_replace(
            '/\W/',
            '',
            preg_replace('/(\w)\w+/', '$1', strtolower($formatter->getPattern()))
        );

        $validator->date('from_date', [$localFormat])->allowEmptyDate('from_date');
        $validator->date('to_date', [$localFormat])->allowEmptyDate('to_date');

        $validator->add(
            'data',
            'validXml',
            [
                'rule' => function ($value) {
                    $Eaccpfs = $this;
                    $errors = $Eaccpfs->extractXsdValidationErrors($value);
                    if (empty($errors)) {
                        return true;
                    }

                    $messages = '<ul><li>'.implode('</li><li>', $errors).'</li></ul>';
                    return __("N'est pas une notice d'autorité valide."). ': '.$messages;
                },
                'message' => __("N'est pas une notice d'autorité valide.")
            ]
        );

        return $validator;
    }

    /**
     * Crée le json du champ data
     *
     * @param EntityInterface $entity
     * @param array           $user   data
     */
    public function initializeData(EntityInterface $entity, array $user)
    {
        $date = !empty($entity->to_date)
            ? [
                "dateRange" => [
                    "fromDate" => $entity->from_date,
                    "toDate" => $entity->to_date
                ]
            ]
            : ["date" => $entity->from_date];
        $identity = [
            "entityId" => $entity->entity_id,
            "entityType" => $entity->entity_type,
            "nameEntry" => [
                "part" => $entity->name
            ]
        ];
        if ($entity->get('description')) {
            $identity['descriptiveNote'] = ['p' => $entity->get('description')];
        }
        $entity->set(
            'data',
            json_encode(
                [
                    'eac-cpf' => [
                        "control" => [
                            "recordId" => $entity->record_id,
                            "maintenanceStatus" => "new",
                            "maintenanceAgency" => [
                                "agencyName" => $entity->agency_name
                            ],
                            "maintenanceHistory" => [
                                "maintenanceEvent" => [
                                    [
                                        "eventType" => "created",
                                        "eventDateTime" => [
                                            "@standardDateTime" => $h = date("Y-m-d\TH:i:s"),
                                            "@" => $h
                                        ],
                                        "agentType" => "human",
                                        "agent" => $user['name'],
                                        "eventDescription" => "Created in "
                                            . Configure::read('App.name', 'Application'),
                                    ]
                                ]
                            ]
                        ],
                        "cpfDescription" => [
                            "identity" => $identity,
                            "description" => [
                                "existDates" => $date
                            ]
                        ],
                        "@xmlns" => "urn:isbn:1-931666-33-4",
                        "@xmlns:xlink" => "http://www.w3.org/1999/xlink"
                    ]
                ]
            )
        );
    }

    /**
     * Récupère les informations envoyé sous forme d'erreur lors de la
     * validation via xsd d'un xml obtenu par le champ data de l'entité
     *
     * @param string $data
     * @return array liste d'erreurs
     * @throws Throwable
     */
    public function extractXsdValidationErrors(string $data)
    {
        set_error_handler(
            function ($errno, $errstr, $errfile, $errline) {
                throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
            }
        );
        $report = [];
        try {
            $initial = libxml_use_internal_errors();
            libxml_use_internal_errors(false);
            $extracted = AppErrorHandler::captureErrors(
                'AsalaeCore\Utility\XmlUtility::validateJson',
                $data,
                EAC_CPF_XSD,
                true
            );
            libxml_use_internal_errors($initial);
            foreach ($extracted['errors'] as $error) {
                $msg = str_replace('{urn:isbn:1-931666-33-4}', '', $error['description']);
                if (preg_match('/^DOMDocument::\w+\(\)(?: \[[^]]+])?: (.*)$/', $msg, $m)) {
                    $err = $this->translate($m[1]);
                    $report[] = __("Erreur : {0}", $err);
                } else {
                    $report[] = $msg;
                }
            }
        } catch (Throwable $e) {
            $report[] = $e->getMessage();
        }
        restore_error_handler();
        return $report;
    }

    /**
     * Possède la liste des traductions possibles, renvoi la chaine traduite
     *
     * @param string $string
     * @return string
     */
    public function translate(string $string): string
    {
        if (preg_match(
            "/^Namespace prefix ([\w:]+) for ([\w:]+) on source is not defined in Entity, line: (\d+)$/",
            $string,
            $m
        )
        ) {
            return __(
                "Namespace prefix {0} for {1} on source is not defined in Entity, line: {2}",
                $m[1],
                $m[2],
                $m[3]
            );
        } elseif (preg_match(
            "/^Element '([\w:]+)', attribute '([\w:]+)': The attribute '([\w:]+)' is not allowed.$/",
            $string,
            $m
        )
        ) {
            return __(
                "Element ''{0}'', attribute ''{1}'': The attribute ''{2}'' is not allowed.",
                $m[1],
                $m[2],
                $m[3]
            );
        } else {
            Log::error(__("Missing traduction for: `{0}`", $string));
            return I18n::getTranslator()->translate($string);
        }
    }

    /**
     * Allows listeners to modify the rules checker by adding more rules.
     * @param RulesChecker $rules
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add(
            function (EntityInterface $entity) {
                $OrgEntities = TableRegistry::getTableLocator()->get('OrgEntities');
                $orgEntity = $OrgEntities->get($entity->get('org_entity_id'));
                return $entity->get('record_id') === $orgEntity->get('identifier');
            }
        );
        return $rules;
    }
}
