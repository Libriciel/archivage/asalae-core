<?php
/**
 * AsalaeCore\Model\Table\PronomsTable
 */

namespace AsalaeCore\Model\Table;

use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Table pronoms
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin TimestampBehavior
 */
class PronomsTable extends Table
{

    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        $this->belongsToMany('FileExtensions');
        $this->hasMany('FileExtensionsPronoms');
        parent::initialize($config);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator->notEmptyString('name');
        $validator->notEmptyString('puid');
        $validator->add(
            'puid',
            [
                'unique' => [
                    'rule' => ['validateUnique'],
                    'provider' => 'table',
                    'message' => __("Un puid existe déjà pour ce pronom")
                ]
            ]
        );
        return $validator;
    }
}
