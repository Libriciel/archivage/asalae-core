<?php
/**
 * AsalaeCore\Model\Table\KeywordListsTable
 */

namespace AsalaeCore\Model\Table;

use AsalaeCore\Model\Behavior\AliasBehavior;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Table keyword_lists
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin TimestampBehavior
 * @mixin AliasBehavior
 */
class KeywordListsTable extends Table
{
    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        $this->addBehavior('Timestamp');

        $this->belongsTo('OrgEntities');
        $this->hasMany('Keywords')->setDependent(true);

        parent::initialize($config);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('identifier')
            ->maxLength('identifier', 255)
            ->requirePresence('identifier', 'create')
            ->notEmptyString('identifier');

        $validator->add(
            'identifier',
            [
                'unique' => [
                    'rule' => ['validateUnique', ['scope' => 'org_entity_id']],
                    'provider' => 'table',
                    'message' => __("Cet identifiant est déjà utilisé")
                ]
            ]
        );

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('description')
            ->requirePresence('description', 'create')
            ->allowEmptyString('description');

        $validator
            ->boolean('active')
            ->requirePresence('active', 'create')
            ->notEmptyString('active');

        $validator
            ->integer('version')
            ->notEmptyString('version');

        return $validator;
    }
}
