<?php
/**
 * AsalaeCore\Model\Table\OrgEntitiesTable
 */

namespace AsalaeCore\Model\Table;

use ArrayObject;
use AsalaeCore\Controller\Component\ConditionComponent;
use AsalaeCore\Form\MessageSchema\MessageSchema;
use AsalaeCore\Model\Entity\OrgEntity;
use Cake\Controller\ComponentRegistry;
use Cake\Database\Expression\IdentifierExpression;
use Cake\Database\Expression\QueryExpression;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Cake\Validation\Validator;
use DateTime;
use Exception;

/**
 * Table org_entities
 *
 * Autres alias:
 * ServiceArchives
 * TransferringAgencies
 * OriginatingAgencies
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class OrgEntitiesTable extends Table implements AfterDeleteInterface, AfterSaveInterface
{
    const CODE_OPERATING_DEPARTMENT = ['SE'];
    const CODE_ARCHIVAL_AGENCIES = ['SA'];
    const CODE_TRANSFERRING_AGENCIES = ['SA', 'SV'];
    const CODE_TRANSFERRING_AGENCIES_WITH_ORGANIZATIONAL = ['SA', 'SV', 'SO'];
    const CODE_ORIGINATING_AGENCIES = ['SA', 'SV', 'SP'];
    const CODE_ORIGINATING_AGENCIES_WITH_ORGANIZATIONAL = ['SA', 'SV', 'SP', 'SO'];
    const CODE_SUBMISSION_AGENCIES = ['SP', 'SV', 'SA', 'OV'];
    const CODE_REQUESTER_AGENCIES = ['SA', 'SV', 'SP', 'SD'];

    /**
     * @var string Chemin vers le fichier counters.json
     */
    const COUNTERS_JSON = RESOURCES . 'counters.json';

    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        $this->addBehavior('Tree');
        $this->addBehavior('Timestamp');

        $this->belongsTo('ArchivalAgencies')
            ->setClassName('OrgEntities')
            ->setForeignKey(false)
            ->setConditions(
                function (QueryExpression $qe, Query $q) {
                    $alias = $this->getAlias(); // OrgEntities
                    $assocAlias = $q->getRepository()->getAlias(); // ArchivalAgencies
                    $query = $this->query()
                        ->select(['aa.id'])
                        ->from(['aa' => 'org_entities'])
                        ->innerJoin(
                            ['aat' => 'type_entities'],
                            ['aat.id' => $q->identifier('aa.type_entity_id')]
                        )
                        ->where(
                            [
                                'aat.code IN' => array_merge(
                                    OrgEntitiesTable::CODE_OPERATING_DEPARTMENT,
                                    OrgEntitiesTable::CODE_ARCHIVAL_AGENCIES
                                ),
                                'aa.lft <=' => $q->identifier("$alias.lft"),
                                'aa.rght >=' => $q->identifier("$alias.rght"),
                            ]
                        )
                        ->orderDesc(new QueryExpression("aat.code = 'SA'"))
                        ->orderAsc('aa.lft')
                        ->limit(1);
                    return $qe->and(["$assocAlias.id" => $query]);
                }
            );
        $this->hasMany('Children', ['className' => 'OrgEntities'])
            ->setForeignKey('parent_id');
        $this->belongsTo('TypeEntities');
        $this->belongsTo('Parents', ['className' => 'OrgEntities'])
            ->setForeignKey('parent_id');
        $this->belongsToMany('Timestampers');
        $this->hasMany('Configurations')->setDependent(true);

        parent::initialize($config);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator->notEmptyString('name');
        $validator->notEmptyString('identifier');
        $validator->add(
            'identifier',
            ['unique' => [
                'rule' => 'validateUnique',
                'provider' => 'table',
                'message' => __("Cet identifiant est déjà utilisé"),
            ],
            ]
        );
        $validator->add(
            'timestamper_id',
            'custom',
            [
                'rule' => function ($value, $context) {
                    if (!empty($value) || empty($context['data']['type_entity_id'])) {
                        return true;
                    }
                    $Types = TableRegistry::getTableLocator()->get('TypeEntities');
                    $saId = $Types->find()->select(['id'])->where(['code' => 'SA'])->first()->get('id');
                    return (int)$context['data']['type_entity_id'] !== $saId;
                },
                'message' => __("Filiation impossible"),
            ]
        );

        return $validator;
    }

    /**
     * Permet d'obtenir le service d'archive d'un utilisateur
     * @param int|string $user_id
     * @return OrgEntity|EntityInterface
     */
    public function getUserSA($user_id): OrgEntity
    {
        /** @var OrgEntity $entity */
        return $this->find()
            ->innerJoinWith('TypeEntities')
            ->innerJoin(
                ['ChildsInc' => 'org_entities'],
                [
                    'ChildsInc.lft >=' => new QueryExpression('OrgEntities.lft'),
                    'ChildsInc.rght <=' => new QueryExpression('OrgEntities.rght'),
                ]
            )
            ->innerJoin(
                ['Users' => 'users'],
                [
                    'Users.org_entity_id' => new QueryExpression('ChildsInc.id'),
                ]
            )
            ->where(['Users.id' => $user_id])
            ->andWhere(['TypeEntities.code' => 'SA'])
            ->firstOrFail();
    }

    /**
     * Permet d'obtenir le service d'archives lié à une entité
     * @param int|string $orgEntityId
     * @return OrgEntity
     */
    public function getEntitySA($orgEntityId): OrgEntity
    {
        /** @var OrgEntity $entity */
        $entity = $this->find()
            ->where(['OrgEntities.id' => $orgEntityId])
            ->contain(['ArchivalAgencies'])
            ->firstOrFail();
        return $entity->get('archival_agency');
    }


    /**
     * Initialise les compteurs pour le service d'archive
     *
     * NOTE: Données exportés grâce à la commande SQL:
     * SELECT ROW_TO_JSON(c)
     * FROM (SELECT nom as name,
     *              nom as identifier,
     *              description,
     *              def_compteur as definition_mask
     *                  FROM "adm-compteurs") c
     *
     * @param string|int $id
     * @throws Exception
     */
    public function initCounters($id)
    {
        if (!is_readable(self::COUNTERS_JSON)) {
            throw new Exception("File not readable");
        }
        $data = json_decode(file_get_contents(self::COUNTERS_JSON), true);
        if (!$data) {
            throw new Exception("Not a valid json");
        }

        $OrgEntities = TableRegistry::getTableLocator()->get('OrgEntities');
        $prefix = $OrgEntities->get($id)->get('identifier') . '_';
        $Counters = TableRegistry::getTableLocator()->get('Counters');
        $Sequences = TableRegistry::getTableLocator()->get('Sequences');
        $sameSeq = [
            'ArchivalAgencyArchiveIdentifier',
            'ArchivalAgencyObjectIdentifier',
        ];

        $driver = $Sequences->getConnection()->getDriver();
        $same = $Sequences->find()
            ->where(['org_entity_id' => $id, 'name' => 'ArchivalAgencyIdentifier'])
            ->first();
        if ($same) {
            $same = $same->id;
        } else {
            $Sequences->query()
                ->insert(['org_entity_id', 'name', 'description', 'value', 'created', 'modified'])
                ->values(
                    [
                        'org_entity_id' => $id,
                        'name' => 'ArchivalAgencyIdentifier',
                        'description' => __(
                            "Utilisée par les compteurs {0} et {1}",
                            'ArchivalAgencyArchiveIdentifier',
                            'ArchivalAgencyObjectIdentifier'
                        ),
                        'value' => 0,
                        'created' => (new DateTime)->format(DATE_RFC3339),
                        'modified' => (new DateTime)->format(DATE_RFC3339),
                    ]
                )
                ->execute();
            $same = $driver->lastInsertId();
        }
        foreach ($data as $counter) {
            if ($Counters->exists(['org_entity_id' => $id, 'identifier' => $counter['identifier']])) {
                continue;
            }
            $counter = [
                'org_entity_id' => $id,
                'identifier' => $counter['identifier'],
                'name' => $counter['name'],
                'description' => $counter['description'],
                'type' => $counter['type'],
                'definition_mask' => $prefix . $counter['definition_mask'],
                'active' => $counter['active'],
                'created' => (new DateTime)->format(DATE_RFC3339),
                'modified' => (new DateTime)->format(DATE_RFC3339),
            ];
            if (in_array($counter['name'], $sameSeq)) {
                $counter['sequence_id'] = $same;
            } else {
                $Sequences->query()
                    ->insert(['org_entity_id', 'name', 'description', 'value', 'created', 'modified'])
                    ->values(
                        [
                            'org_entity_id' => $id,
                            'name' => $counter['identifier'],
                            'description' => __("Utilisée par le compteur {0}", $counter['identifier']),
                            'value' => 0,
                            'created' => (new DateTime)->format(DATE_RFC3339),
                            'modified' => (new DateTime)->format(DATE_RFC3339),
                        ]
                    )
                    ->execute();
                $counter['sequence_id'] = $driver->lastInsertId();
            }
            $Counters->query()
                ->insert(array_keys($counter))
                ->values($counter)
                ->execute();
        }
    }

    /**
     * The Model.afterSave event is fired after an entity is saved.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     */
    public function afterSave(Event $event, Entity $entity, ArrayObject $options)
    {
        $dirty = array_intersect(
            $entity->getDirty(),
            ['name', 'identifier', 'type_entity_id']
        );
        if ($entity->isNew() || $dirty) {
            MessageSchema::clearCache($entity->get('archival_agency_id'));
        }
    }

    /**
     * The Model.afterDelete event is fired after an entity is deleted.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     */
    public function afterDelete(Event $event, Entity $entity, ArrayObject $options)
    {
        MessageSchema::clearCache($entity->get('archival_agency_id'));
    }

    /**
     * Allows listeners to modify the rules checker by adding more rules.
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->addDelete(
            function (\Cake\Datasource\EntityInterface $entity) {
                return $entity->get('deletable');
            }
        );
        return parent::buildRules($rules);
    }

    /**
     * SubQuery construisant le path des entités parentes (sans le SE)
     * @return Query
     */
    public function queryParentGroupSelect(): Query
    {
        $cond = new ConditionComponent(new ComponentRegistry);
        return $this->find()
            ->select(['path' => $cond->stringAgg('o.name', ' / ', 'o.lft')])
            ->from(['o' => 'org_entities'])
            ->innerJoin(
                ['ote' => 'type_entities'],
                [
                    'ote.id' => new IdentifierExpression('o.type_entity_id'),
                ]
            )
            ->where(
                [
                    'o.lft < OrgEntities.lft',
                    'o.rght > OrgEntities.rght',
                    'ote.code NOT IN' => array_merge(
                        self::CODE_OPERATING_DEPARTMENT,
                        self::CODE_ARCHIVAL_AGENCIES
                    ),
                ]
            );
    }

    /**
     * Entités avec leurs roles
     * @param EntityInterface|null $orgEntity
     * @return Query
     */
    public function findByRoles(EntityInterface $orgEntity = null): Query
    {
        $joinRolesConditions = [
            'Roles.id' => new QueryExpression('TypesRoles.role_id'),
        ];
        if ($orgEntity) {
            $joinRolesConditions['OR'] = [
                'Roles.org_entity_id IS' => null,
                'Roles.org_entity_id' => $orgEntity->get('id'),
            ];
        }
        return $this->find()
            ->select(
                [
                    'Roles.id',
                    'Roles.name',
                ]
            )
            ->innerJoin(
                ['TypeEntities' => 'type_entities'],
                [
                    'TypeEntities.id'
                    => new IdentifierExpression('OrgEntities.type_entity_id'),
                ]
            )
            ->innerJoin(
                ['TypesRoles' => 'roles_type_entities'],
                [
                    'TypesRoles.type_entity_id'
                    => new QueryExpression('TypeEntities.id'),
                ]
            )
            ->innerJoin(
                ['Roles' => 'roles'],
                $joinRolesConditions
            )
            ->orderAsc('Roles.name');
    }

    /**
     * Donne les options de select d'entités
     * @param Query $query
     * @return array
     */
    public function listOptionsForSuperArchivist(Query $query): array
    {
        $query->select(
            [
                'id',
                'name',
                'identifier',
                'archival_agency' => 'ArchivalAgencies.name',
                'archival_agency_id' => 'ArchivalAgencies.name',
                'path' => $this->queryParentGroupSelect(),
            ]
        )
            ->contain(['ArchivalAgencies', 'TypeEntities'])
            ->order(['OrgEntities.lft', 'OrgEntities.identifier']);
        $options = [];
        /** @var EntityInterface $entity */
        foreach ($query as $entity) {
            $value = Hash::get($entity, 'identifier').' - '.Hash::get($entity, 'name');
            $parents = Hash::get($entity, 'path') ?: Hash::get($entity, 'archival_agency');
            $options[$parents][] = [
                'value' => $entity->id,
                'text' => $value,
                'data-archival-agency-id' => Hash::get($entity, 'archival_agency_id'),
            ];
        }
        return $options;
    }

   /**
    * Donne le subquery pour le ParentSO
    * @return Query
    */
    public function parentSoSubquery(): Query
    {
        return $this->query()
            ->select(['ParentSO.id'])
            ->from(['ParentSO' => 'org_entities'])
            ->innerJoin(
                ['type_e' => 'type_entities'],
                ['ParentSO.type_entity_id' => new IdentifierExpression('type_e.id')]
            )
            ->where(
                [
                    'ParentSO.lft <' => new IdentifierExpression('OrgEntities.lft'),
                    'ParentSO.rght >' => new IdentifierExpression('OrgEntities.rght'),
                    'type_e.code' => 'SO',
                ]
            )
            ->order(['ParentSO.lft' => 'desc'])
            ->limit(1);
    }

    /**
     * Donne les options de select d'entités
     * @param array $params
     * @return Query
     */
    public function listOptionsWithoutSeal(array $params = []): Query
    {
        $params += [
            'valueField' => function ($entity) {
                return Hash::get($entity, 'identifier').' - '.Hash::get($entity, 'name');
            },
            'groupField' => function ($entity) {
                $parents = Hash::get($entity, 'path');
                return $parents ?: Hash::get($entity, 'archival_agency');
            }
        ];
        $query = $this->find('list', $params);
        return $query
            ->select(
                [
                    'OrgEntities.id',
                    'OrgEntities.name',
                    'OrgEntities.identifier',
                    'archival_agency' => 'ArchivalAgencies.name',
                    'path' => $this->queryParentGroupSelect(),
                ]
            )
            ->contain(['ArchivalAgencies', 'TypeEntities'])
            ->order(['OrgEntities.lft', 'OrgEntities.identifier']);
    }
}
