<?php
/**
 * AsalaeCore\Model\Table\CountersTable
 */

namespace AsalaeCore\Model\Table;

use AsalaeCore\Model\Entity\OrgEntity;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use DateTime;
use DateTimeInterface;
use Exception;

/**
 * Table counters
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin TimestampBehavior
 * @property SequencesTable $Sequences
 */
class CountersTable extends Table
{
    /**
     * Configuration initale de la table
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        $this->addBehavior('Timestamp');

        $this->belongsTo('OrgEntities');
        $this->belongsTo('Sequences');

        parent::initialize($config);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('identifier')
            ->maxLength('identifier', 255)
            ->requirePresence('identifier', 'create')
            ->notEmptyString('identifier')
            ->add(
                'identifier',
                [
                    'unique' => [
                        'rule' => ['validateUnique', ['scope' => 'org_entity_id']],
                        'provider' => 'table',
                        'message' => __("Cet identifiant est déjà utilisé")
                    ]
                ]
            );

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('description')
            ->requirePresence('description', 'create')
            ->notEmptyString('description');

        $validator
            ->scalar('type')
            ->maxLength('type', 255)
            ->requirePresence('type', 'create')
            ->notEmptyString('type');

        $validator
            ->scalar('definition_mask')
            ->maxLength('definition_mask', 255)
            ->requirePresence('definition_mask', 'create')
            ->notEmptyString('definition_mask')
            ->add(
                'definition_mask',
                'pattern',
                [
                    'rule' => ['custom', '/#(s|0{1,10}|S{1,10})#/'],
                    'message' => __("Doit contenir au moins un élément de séquence")
                ]
            );

        $validator
            ->scalar('sequence_reset_mask')
            ->maxLength('sequence_reset_mask', 255)
            ->allowEmptyString('sequence_reset_mask');

        $validator
            ->scalar('sequence_reset_value')
            ->maxLength('sequence_reset_value', 255)
            ->allowEmptyString('sequence_reset_value');

        $validator
            ->boolean('active')
            ->requirePresence('active', 'create')
            ->notEmptyString('active');

        return $validator;
    }

    /**
     * Génère une chaine à partir de la sequence et du mask
     * @param int|string             $sequence
     * @param string                 $mask
     * @param DateTimeInterface|null $date
     * @return string
     * @throws Exception
     */
    public function generate($sequence, string $mask, DateTimeInterface $date = null): string
    {
        if ($date === null) {
            $date = new DateTime;
        }
        $sequence = (string)$sequence;
        $output = $mask;
        $output = str_replace('#s#', $sequence, $output);
        $output = str_replace('#AAAA#', $date->format('Y'), $output);
        $output = str_replace('#AA#', $date->format('y'), $output);
        $output = str_replace('#M#', $date->format('n'), $output);
        $output = str_replace('#MM#', $date->format('m'), $output);
        $output = str_replace('#J#', $date->format('j'), $output);
        $output = str_replace('#JJ#', $date->format('d'), $output);
        foreach (['S' => '_', '0' => '0'] as $key => $replacement) {
            if (!preg_match_all('/#'.$key.'+#/', $output, $m)) {
                continue;
            }
            $things = current($m);
            foreach (array_unique($things) as $sub) {
                $len = strlen($sub) -2;
                $background = str_repeat($replacement, $len);
                $output = str_replace($sub, substr($background.$sequence, strlen($sequence)), $output);
            }
        }
        return $output;
    }

    /**
     * Permet d'obtenir la prochaine valeur du compteur
     * @param OrgEntity|int|string $orgEntity
     * @param string               $identifier
     * @param array                $params
     *      DateTime    'date'    => date à utiliser pour générer le compteur - default now
     *      int|string  'value'   => valeur de sequence à utiliser - default: valeur dans table sequence
     *      bool        'save'    => effectue une mise à jour de la table sequence (value +1)
     *      array       'replace' => Remplace dans le compteur '#'.$key.'#' par $value
     * @param callable|null        $fn         si la valeur retournée est fausse, on donne un nouvel
     *                                         identifiant
     * @return string
     * @throws Exception
     */
    public function next($orgEntity, string $identifier, array $params = [], callable $fn = null): string
    {
        $params += [
            'date' => new DateTime,
            'value' => null,
            'save' => true,
            'replace' => [],
            'i' => 0,
        ];
        $org_entity_id = $orgEntity instanceof OrgEntity
            ? $orgEntity->get('id')
            : $orgEntity;

        $counter = $this->find()
            ->where(
                [
                    'Counters.org_entity_id' => $org_entity_id,
                    'Counters.identifier' => $identifier,
                ]
            )
            ->contain(['Sequences'])
            ->firstOrFail();

        // si la valeur est définie dans params, on l'utilise à la place de celle en base
        $sequence = $counter->get('sequence');
        if ($params['value'] === null) {
            $params['value'] = $sequence->get('value') +1;
        }

        // reset de la sequence si la valeur à changé
        $reset = $counter->get('sequence_reset_mask')
            ? $this->generate($params['value'], $counter->get('sequence_reset_mask'), $params['date'])
            : null;
        if ($reset && $counter->get('sequence_reset_value') !== $reset) {
            $counter->set('sequence_reset_value', $reset);
            $params['value'] = 1;
        }
        $sequence->set('value', $params['value']);

        $output = $this->generate($params['value'], $counter->get('definition_mask'), $params['date']);
        foreach ($params['replace'] as $key => $value) {
            $output = str_replace("#$key#", $value, $output);
        }
        if ($params['save']) {
            $this->save($counter);
            $this->Sequences->save($sequence);
        }

        $params['i']++;
        if ($fn && $params['i'] < 10000 && (!$fn($output))) {
            ini_set('xdebug.max_nesting_level', 10000);
            $params['value'] = null; // récupère la sequence
            $prev = $output;
            $output = $this->next(
                $orgEntity,
                $identifier,
                $params,
                $fn
            );
            if ($prev === $output) {
                trigger_error(__("L'identifiant n'a pas changé entre deux appels à Counters::next()"));
                return $output;
            }
        }

        return $output;
    }
}
