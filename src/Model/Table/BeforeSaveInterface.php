<?php
/**
 * AsalaeCore\Model\Table\BeforeSaveInterface
 */

namespace AsalaeCore\Model\Table;

use ArrayObject;
use Cake\Event\Event;
use Cake\ORM\Entity;

/**
 * Interface pour le callback beforeSave
 *
 * @category Table
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
interface BeforeSaveInterface
{
    /**
     * The Model.beforeSave event is fired before an entity is saved.
     *
     * @param Event       $event
     * @param Entity      $entity
     * @param ArrayObject $options
     */
    public function beforeSave(Event $event, Entity $entity, ArrayObject $options);
}
