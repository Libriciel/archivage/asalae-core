<?php
/**
 * AsalaeCore\Model\Behavior\OptionsBehavior
 */

namespace AsalaeCore\Model\Behavior;

use Cake\ORM\Behavior;
use Exception;

/**
 * Récupération de la liste des options possibles
 *
 * @category    Lib
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2017, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class OptionsBehavior extends Behavior
{

    /**
     * Default config
     *
     * suffix permet de "deviner" le nom du champ. ex: suffixe = trad... > state = statetrad
     *
     * @var array
     */
    protected $_defaultConfig = ['suffix' => 'trad'];

    /**
     * Permet d'obtenir une liste de traductions pour les options
     *
     * @param string $field
     * @return array
     * @throws Exception
     */
    public function options(string $field): array
    {
        $config = $this->getConfig();
        $translateField = $field . $config['suffix'];
        $entity = $this->_table->newEntity([], ['validate' => false]);
        $options = [];

        if (empty($config[$field])) {
            throw new Exception(__("Le champ {0} n'a pas d'options configurées", $field));
        }

        foreach ($config[$field] as $value) {
            $entity->set($field, $value);
            $options[$value] = $entity->get($translateField);
        }
        return $options;
    }
}
