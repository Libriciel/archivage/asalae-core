<?php
/**
 * AsalaeCore\Model\Behavior\AclBehavior
 */

namespace AsalaeCore\Model\Behavior;

use Acl\Model\Behavior\AclBehavior as AclAclBehavior;
use Cake\Core\Exception\Exception;
use Cake\Event\Event;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\Utility\Inflector;

/**
 * Surcharche du plugin Acl
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class AclBehavior extends AclAclBehavior
{
    /**
     * Maps ACL type options to ACL models
     *
     * @var array
     */
    protected $_typeMaps = ['requester' => 'Aro', 'controlled' => 'Aco', 'both' => ['Aro', 'Aco']];

    /**
     * Sets up the configuration for the model, and loads ACL models if they haven't been already
     *
     * @param Table $model  Table instance being attached
     * @param array $config Configuration
     * @return void
     * @noinspection PhpMissingParentConstructorInspection surcharge
     */
    public function __construct(Table $model, array $config = [])
    {
        $this->_table = $model;
        if (isset($config[0])) {
            $config['type'] = $config[0];
            unset($config[0]);
        }
        if (isset($config['type'])) {
            $config['type'] = strtolower($config['type']);
        }
        $config = $this->_resolveMethodAliases(
            'implementedFinders',
            $this->_defaultConfig,
            $config
        );
        $config = $this->_resolveMethodAliases(
            'implementedMethods',
            $this->_defaultConfig,
            $config
        );
        $this->_table = $model;
        $this->setConfig($config);
        $this->initialize($config);

        $types = $this->_typeMaps[$this->getConfig()['type']];

        if (!is_array($types)) {
            $types = [$types];
        }
        foreach ($types as $type) {
            $alias = Inflector::pluralize($type);
            $model->hasOne($alias)
                ->setForeignKey('foreign_key')
                ->setConditions([$alias.'.model' => $model->getAlias()]);
        }

        if (!method_exists($model->getEntityClass(), 'parentNode')) {
            trigger_error(
                __d('cake_dev', 'Callback {0} not defined in {1}', ['parentNode()', $model->getEntityClass()]),
                E_USER_WARNING
            );
        }
    }

    /**
     * Retrieves the Aro/Aco node for this model
     *
     * @param string|array|Table $ref  Array with 'model' and 'foreign_key', model object, or string value
     * @param string             $type Only needed when Acl is set up as 'both', specify 'Aro' or 'Aco'
     *                                 to get the correct node
     * @return Query
     * @link http://book.cakephp.org/2.0/en/core-libraries/behaviors/acl.html#node
     * @throws Exception
     */
    public function node($ref = null, $type = null)
    {
        if (empty($type)) {
            $type = $this->_typeMaps[$this->getConfig('type')];
            if (is_array($type)) {
                trigger_error(
                    __d(
                        'cake_dev',
                        'AclBehavior is setup with more then one type, please specify type parameter for node()'
                    ),
                    E_USER_WARNING
                );

                return null;
            }
        }
        if (empty($ref)) {
            throw new Exception(__d('cake_dev', 'ref parameter must be a string or an Entity'));
        }
        $alias = Inflector::pluralize($type);

        return $this->_table->$alias->node($ref);
    }

    /**
     * Creates a new ARO/ACO node bound to this record
     *
     * @param Event  $event  The afterSave event that was fired
     * @param Entity $entity The entity being saved
     * @return void
     */
    public function afterSave(Event $event, Entity $entity)
    {
        $model = $event->getSubject();
        $types = $this->_typeMaps[$this->getConfig('type')];
        if (!is_array($types)) {
            $types = [$types];
        }
        foreach ($types as $type) {
            $alias = Inflector::pluralize($type);
            if (!method_exists($entity, 'parentNode')) {
                continue;
            }
            $parent = $entity->parentNode();
            if (!empty($parent)) {
                $parent = $this->node($parent, $type)->first();
            }
            $data = [
                'parent_id' => $parent->id ?? null,
                'model' => $model->getAlias(),
                'foreign_key' => $entity->id,
            ];

            if (method_exists($entity, 'nodeAlias')) {
                $data['alias'] = $entity->nodeAlias();
            }

            if (!$entity->isNew()) {
                $node = $this->node($entity, $type)->first();
                $data['id'] = $node->id ?? null;
                $newData = $model->$alias->patchEntity($node, $data);
            } else {
                $newData = $model->$alias->newEntity($data);
            }

            $model->$alias->getTarget()->save($newData);
        }
    }

    /**
     * Destroys the ARO/ACO node bound to the deleted record
     *
     * @param Event  $event  The afterDelete event that was fired
     * @param Entity $entity The entity being deleted
     * @return void
     */
    public function afterDelete(Event $event, Entity $entity)
    {
        $types = $this->_typeMaps[$this->getConfig('type')];
        if (!is_array($types)) {
            $types = [$types];
        }
        foreach ($types as $type) {
            $node = $this->node($entity, $type)->toArray();
            if (!empty($node)) {
                $alias = Inflector::pluralize($type);
                $event->getSubject()->$alias->delete($node[0]);
            }
        }
    }
}
