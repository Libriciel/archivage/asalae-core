<?php
/**
 * AsalaeCore\Model\Behavior\AliasBehavior
 */

namespace AsalaeCore\Model\Behavior;

use Cake\ORM\Behavior;
use Cake\ORM\Table;

/**
 * Permet d'obtenir une table avec un alias différent de façon dynamique
 *
 * @category Behavior
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class AliasBehavior extends Behavior
{
    /**
     * Renvoi la table avec un alias différent
     * @param string $alias
     * @return Table
     */
    public function withAlias(string $alias): Table
    {
        $newTable = clone $this->_table;
        return $newTable->setAlias($alias);
    }
}
