<?php
/**
 * AsalaeCore\Model\Entity\ServiceLevel
 */

namespace AsalaeCore\Model\Entity;

use AsalaeCore\ORM\Entity;

/**
 * Entité de la table ServiceLevels
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ServiceLevel extends Entity
{
    /**
     * Champs virtuels
     *
     * @var array
     */
    protected $_virtual = ['deletable'];

    /**
     * Cette entité peut-elle être supprimée ?
     * @return bool
     */
    protected function _getDeletable(): bool
    {
        // NOTE: voir index.ctp:afterEditServiceLevel() si ce champ est modifié
        return empty($this->_fields['default_level']);
    }
}
