<?php

/**
 * AsalaeCore\Model\Entity\ArchivingSystem
 */

namespace AsalaeCore\Model\Entity;

use AsalaeCore\DataType\EncryptedString;
use AsalaeCore\Exception\GenericException;
use AsalaeCore\Factory\Utility;
use AsalaeCore\Http\Client;
use AsalaeCore\Http\Client\Adapter\StreamLargeFilesCurl;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\Http\Client\Response as HttpResponse;
use Cake\Http\Exception\HttpException;
use AsalaeCore\ORM\Entity;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Exception;
use Laminas\Diactoros\Stream;

/**
 * Entité de la table archiving_systems
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ArchivingSystem extends Entity
{
    /**
     * Urls des apis
     * @var string[]
     */
    protected $apiCalls = [
        'transfer' => '/api/transfers',
        'transfer_chunk' => '/api/transfers/prepare-chunked',
        // callbacks
        'dl_zip' => '/api/outgoing-transfers/zip',
        'after_dl_zip' => '/api/outgoing-transfers/delete-zip',
    ];

    /**
     * Champs virtuels
     * @var array
     */
    protected $_virtual = ['deletable', 'removable'];

    /**
     * Donne les informations issue du SAE distant
     * @return array|mixed
     * @throws Exception
     */
    public function whoami()
    {
        $client = get_class(Utility::get(Client::class));
        /** @var Client $client */
        $client = new $client($this->getClientParams());
        $url = rtrim($this->_fields['url'], '/') . "/api/webservices/whoami";
        $response = $client->get($url);
        return $response->getJson();
    }

    /**
     * Donne les params pour le client http
     * @return array[]
     */
    public function getClientParams()
    {
        /** @var EncryptedString $password */
        $password = $this->get('password');
        $params = [
            'adapter' => StreamLargeFilesCurl::class,
            'auth' => [
                'username' => $this->_fields['username'],
                'password' => $password->toFormData(),
            ],
            'headers' => ['Accept' => 'application/json', 'X-Asalae-Webservice' => 'true'],
            'redirect' => 5,
            'ssl_verify_peer' => $this->_fields['ssl_verify_peer'],
            'ssl_verify_peer_name' => $this->_fields['ssl_verify_peer_name'],
            'ssl_verify_depth' => $this->_fields['ssl_verify_depth'],
            'ssl_verify_host' => $this->_fields['ssl_verify_host'],
            'ssl_cafile' => $this->_fields['ssl_cafile'],
        ];
        if ($this->_fields['use_proxy'] && Configure::read('Proxy.host')) {
            $params['proxy'] = [
                'proxy' => Configure::read('Proxy.host') . ':' . Configure::read('Proxy.port'),
                'username' => Configure::read('Proxy.login'),
                'password' => Configure::read('Proxy.password'),
            ];
        }
        return $params;
    }

    /**
     * Donne les options pour un transfert sortant
     * @return array|mixed
     * @throws Exception
     */
    public function requestOptions()
    {
        /** @var Client $client */
        $client = get_class(Utility::get(Client::class));
        $client = new $client($this->getClientParams());
        $url = rtrim($this->_fields['url'], '/') . "/api/outgoing-transfer-requests/options";
        $response = $client->get($url);
        if ($response->getStatusCode() !== 200) {
            throw new HttpException($response->getReasonPhrase(), $response->getStatusCode());
        }
        return $response->getJson();
    }

    /**
     * Envoi un transfert au SAE distant
     * @param EntityInterface $outgoingTransfer ou "transfer" dans versae
     * @param bool            $getResponse
     * @param callable|null   $touchCallback
     * @return array|mixed
     * @throws Exception
     */
    public function postOutgoingTransfer(
        EntityInterface $outgoingTransfer,
        bool $getResponse = false,
        ?callable $touchCallback = null
    ) {
        $url = rtrim($this->_fields['url'], '/') . $this->apiCalls['transfer'];
        $zip = $outgoingTransfer->get('zip');
        $originalSize = $outgoingTransfer->get('data_size')
            ?: Hash::get($outgoingTransfer, 'outgoing_transfer_request.original_size');
        $client = get_class(Utility::get(Client::class));
        $options = $this->getClientParams();
        $chunkSize = $this->get('chunk_size') ? max($this->get('chunk_size'), 1024) : 0;
        $largeFileOpts = ['curl' => [CURLOPT_TIMEOUT => (int)($originalSize / 100000) + 30]]; // 2.5Go ~= 7h30
        if (!$chunkSize) {
            $options += $largeFileOpts;
        }
        /** @var Client $client */
        $client = new $client($options);
        $filename = basename($zip);
        $hash = hash_file('sha256', $zip);
        $data = [
            'transferDigest' => md5("$filename:sha256:$hash"), // NOSONAR md5 suffisant
            'transferIdentifier' => $outgoingTransfer->get('identifier'),
            'archivalAgencyIdentifier' => Hash::get(
                $outgoingTransfer,
                'outgoing_transfer_request.archival_agency_identifier'
            ),
            'transferringAgencyIdentifier' => Hash::get(
                $outgoingTransfer,
                'outgoing_transfer_request.transferring_agency_identifier'
            ),
        ];

        if ($chunkSize && filesize($zip) > $chunkSize) {
            $key = hash('sha256', random_bytes(40));
            $chunkUrl = rtrim($this->_fields['url'], '/') . $this->apiCalls['transfer_chunk'] . "/$key?";
            $params = [
                'filename' => basename($zip),
                'totalSize' => filesize($zip),
                'totalChunks' => (int)ceil(filesize($zip) / $chunkSize),
                'finalSha256' => $hash,
            ];
            $zipHandle = fopen($zip, 'r');
            $chunkCount = 0;
            while (!feof($zipHandle)) {
                $chunkCount++;
                $chunk = tmpfile();
                $currentChunkSize = 0;
                $crc32b = hash_init('crc32b');
                while ($currentChunkSize < $chunkSize && feof($zipHandle) === false) {
                    $read = fread($zipHandle, 1024);
                    hash_update($crc32b, $read);
                    $currentChunkSize += fwrite($chunk, $read);
                }
                rewind($chunk);
                $urlParams = [
                    'currentSize' => $currentChunkSize,
                    'currentChunk' => $chunkCount,
                    'crc32b' => hash_final($crc32b),
                ] + $params;
                $options = [
                    'headers' => [
                        'Accept' => 'application/json',
                        'Content-Type' => 'application/octet-stream',
                        'X-Asalae-Webservice' => 'true',
                    ],
                ];
                if (feof($zipHandle)) {
                    $options += $largeFileOpts;
                }
                try {
                    if ($touchCallback) {
                        $touchCallback($urlParams);
                    }
                    $response = $client->post(
                        $chunkUrl . http_build_query($urlParams),
                        new Stream($chunk),
                        $options
                    );
                } finally {
                    fclose($chunk);
                }
                if (!in_array($response->getStatusCode(), [201, 204, 206])) {
                    fclose($zipHandle);
                    throw new GenericException(
                        __(
                            "Erreur lors de l'envoi d'un chunk. Code HTTP {0} : {1}",
                            $response->getStatusCode(),
                            $response->getStringBody()
                        )
                    );
                }
            }
            fclose($zipHandle);
            $data['transfer'] = $key;
        } elseif ($chunkSize) {
            $data['transfer'] = fopen($zip, 'r');
        } else {
            $AuthUrls = TableRegistry::getTableLocator()->get('AuthUrls');
            /** @var AuthUrl $code */
            $code = $AuthUrls->newEntity(
                [
                    'url' => $this->apiCalls['dl_zip'] . '/' . $outgoingTransfer->id,
                    'expire' => date('Y-m-d H:i:s', strtotime('+1 minute'))
                ]
            );
            $AuthUrls->saveOrFail($code);
            $transferUrl = Configure::read('App.fullBaseUrl') . '/auth-urls/activate/' . $code->get('code');
            $data['transfer'] = $transferUrl;
            if ($this->apiCalls['after_dl_zip']) {
                /** @var AuthUrl $code */
                $code = $AuthUrls->newEntity(
                    [
                        'url' => $this->apiCalls['after_dl_zip'] . '/' . $outgoingTransfer->id,
                        'expire' => date('Y-m-d H:i:s', strtotime('+10 hours'))
                    ]
                );
                $AuthUrls->saveOrFail($code);
                $uploadedUrl = Configure::read('App.fullBaseUrl') . '/auth-urls/activate/' . $code->get('code');
                $data['transferUploadedUrl'] = $uploadedUrl;
            }
        }
        $options = ['headers' => ['File-Size' => $originalSize]] + $largeFileOpts;
        if ($touchCallback) {
            $touchCallback($data + $options + ['finalization' => true, 'url' => $url]);
        }
        $response = $client->post($url, $data, $options);

        if ($getResponse) {
            return $response;
        }
        return $response->getJson() ?: $response->getStringBody();
    }

    /**
     * Donne l'accusé de réception d'un transfert
     * @param EntityInterface $outgoingTransfer
     * @return HttpResponse
     * @throws Exception
     */
    public function getAcknowledgement(EntityInterface $outgoingTransfer): HttpResponse
    {
        $client = get_class(Utility::get(Client::class));
        $params = $this->getClientParams();
        $params['headers'] = ['Accept' => 'application/xml', 'X-Asalae-Webservice' => 'true'];
        /** @var Client $client */
        $client = new $client($params);
        $url = rtrim($this->_fields['url'], '/')
            . "/api/transfers/acknowledgement/" . urlencode($outgoingTransfer->get('identifier'));
        return $client->get($url);
    }

    /**
     * Donne l'accusé de réception d'un transfert
     * @param EntityInterface $outgoingTransfer
     * @return HttpResponse
     * @throws Exception
     */
    public function getReply(EntityInterface $outgoingTransfer): HttpResponse
    {
        $client = get_class(Utility::get(Client::class));
        $params = $this->getClientParams();
        $params['headers'] = ['Accept' => 'application/xml', 'X-Asalae-Webservice' => 'true'];
        /** @var Client $client */
        $client = new $client($params);
        $url = rtrim($this->_fields['url'], '/')
            . "/api/transfers/reply/" . urlencode($outgoingTransfer->get('identifier'));
        return $client->get($url);
    }

    /**
     * Getter du champ password
     * @return EncryptedString|mixed|null
     */
    protected function _getPassword()
    {
        if (empty($this->_fields['password'])) {
            return null;
        } elseif (is_resource($this->_fields['password'])) {
            rewind($this->_fields['password']);
            return new EncryptedString(stream_get_contents($this->_fields['password']));
        } elseif (is_string($this->_fields['password'])) {
            return new EncryptedString($this->_fields['password']);
        } else {
            return $this->_fields['password'];
        }
    }

    /**
     * Setter du champ password
     * @param mixed $value
     * @return EncryptedString|null
     */
    protected function _setPassword($value)
    {
        if (empty($value)) {
            return null;
        } elseif (is_string($value)) {
            return EncryptedString::createFromString($value);
        }
        return $value;
    }

    /**
     * Cette entité peut-elle être supprimée ?
     * @return bool
     */
    protected function _getDeletable(): bool
    {
        if (empty($this->_fields['id'])) {
            return true;
        }
        return empty($this->_fields['org_entity_id']);
    }

    /**
     * Cette entité peut-elle être retiré ?
     * @return bool
     */
    protected function _getRemovable(): bool
    {
        if (empty($this->_fields['id'])) {
            return false;
        }
        return true;
    }
}
