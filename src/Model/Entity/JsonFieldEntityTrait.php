<?php
/**
 * AsalaeCore\Model\Entity\JsonFieldEntityTrait
 */

namespace AsalaeCore\Model\Entity;

use AsalaeCore\DataType\JsonString;
use Cake\ORM\Entity;
use Cake\Utility\Inflector;
use ReflectionClass;
use ReflectionException;

/**
 * Gestion des entités avec champs de type json
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2023, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin Entity
 * @property string $jsonFields
 */
trait JsonFieldEntityTrait
{
    /**
     * @var string[] Surcharger avec $this->jsonFields
     */
    private array $defaultJsonFields = ['app_meta'];

    /**
     * @var JsonString[]
     */
    private array $jsonFieldObjects = [];

    /**
     * @var string nom de la méthode à executer
     */
    private string $defaultJsonFieldFilter = 'defaultJsonFieldFilter';

    /**
     * constructor.
     * @param array $properties
     * @param array $options
     */
    public function __construct(array $properties = [], array $options = [])
    {
        $jsonFields = $this->jsonFields ?? $this->defaultJsonFields;
        foreach ($jsonFields as $jsonField) {
            $properties[$jsonField] = JsonString::create($properties[$jsonField] ?? null);
            $this->jsonFieldObjects[$jsonField] = $properties[$jsonField];
            $this->jsonFieldObjects[$jsonField]->filter
                = [$this, $this->jsonFieldFilter ?? $this->defaultJsonFieldFilter];
            $this->jsonFieldObjects[$jsonField]->flags = JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT;
        }
        /** @noinspection PhpUndefinedClassInspection */
        parent::__construct($properties, $options);
        if (!isset($this->jsonVirtualFields)) {
            return;
        }
        foreach ($jsonFields as $jsonField) {
            $jsonFieldNames = $this->jsonVirtualFields[$jsonField] ?? [];
            foreach ($jsonFieldNames as $virtualField) {
                $this->_fields[$virtualField] =& $this->jsonFieldObjects[$jsonField][$virtualField];
            }
        }
    }

    /**
     * Fetch accessor method name
     * Accessor methods (available or not) are cached in $_accessors
     *
     * @param string $property the field name to derive getter name from
     * @param string $type     the accessor type ('get' or 'set')
     * @return string method name or empty string (no method available)
     * @throws ReflectionException
     */
    protected static function _accessor(string $property, string $type): string
    {
        /** @noinspection PhpUndefinedClassInspection */
        $accessor = parent::_accessor($property, $type);
        if ($accessor) {
            return $accessor;
        }
        $refl = new ReflectionClass(static::class);
        if (!$refl->hasProperty('jsonVirtualFields')) {
            return $accessor;
        }
        $instance = $refl->newInstanceWithoutConstructor();
        $jsonFields = $instance->listJsonVirtualFields();
        foreach ($jsonFields as $jsonField => $virtualFields) {
            if ($property === $jsonField || in_array($property, $virtualFields)) {
                return sprintf(
                    '_%s%s',
                    $type,
                    Inflector::camelize($property)
                );
            }
        }
        return $accessor;
    }

    /**
     * Donne la liste des champs virtuels reliés au json
     * @return array
     */
    public function listJsonVirtualFields(): array
    {
        return $this->jsonVirtualFields ?? [];
    }

    /**
     * Getter/setter générique pour les champs virtuels du json
     * @param string $name
     * @param array  $arguments
     * @return mixed|void|null
     */
    public function __call($name, $arguments)
    {
        if (!preg_match('/^_(get|set)(.*)$/', $name, $m)) {
            return null;
        }
        $field = Inflector::underscore($m[2]);
        $jsonFields = $this->listJsonVirtualFields();
        foreach ($jsonFields as $jsonField => $virtualFields) {
            if ($field === $jsonField) {
                if ($m[1] === 'get') {
                    return $this->_fields[$field] ?? ($arguments[0] ?? null);
                } else {
                    $this->_dirty[$jsonField] = true;
                    return $this->setterJsonString($field, $arguments[0]);
                }
            }
            if (in_array($field, $virtualFields)) {
                if ($m[1] === 'get') {
                    return $this->_fields[$field] ?? ($arguments[0] ?? null);
                } else {
                    $this->_dirty[$jsonField] = true;
                    return $arguments[0];
                }
            }
        }
    }

    /**
     * Setter du champ json
     * @param string $field
     * @param mixed  $value
     * @return JsonString
     */
    public function setterJsonString(string $field, $value): JsonString
    {
        $initialFilter = isset($this->jsonFieldObjects[$field])
            ? $this->jsonFieldObjects[$field]->filter
            : fn($v) => $v !== null;
        $this->jsonFieldObjects[$field] = JsonString::create($value);
        $jsonFieldNames = $this->jsonVirtualFields[$field] ?? [];
        foreach ($jsonFieldNames as $virtualField) {
            $this->_fields[$virtualField] =& $this->jsonFieldObjects[$field][$virtualField];
        }
        $this->jsonFieldObjects[$field]->setFilter($initialFilter);
        return $this->jsonFieldObjects[$field];
    }

    /**
     * Filtre par défaut (les valeurs ne seront pas enregistrés)
     * @param mixed $value
     * @return bool
     */
    public function defaultJsonFieldFilter($value): bool
    {
        return $value !== null
            && !in_array($value, [0, '', false, []], true);
    }
}
