<?php
/**
 * AsalaeCore\Model\Entity\Configuration
 */

namespace AsalaeCore\Model\Entity;

use Cake\Core\Configure;
use AsalaeCore\ORM\Entity;

/**
 * Entité de la table configurations
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Configuration extends Entity
{
    const URI_NAMES = ['background-image', 'logo-client'];

    /**
     * Donne le chemin vers le fichier dans /data
     * @return string
     */
    protected function _getDataFilename(): string
    {
        if (!in_array($this->_fields['name'] ?? '', self::URI_NAMES)
            || empty($this->_fields['setting'])
        ) {
            return '';
        }
        $publicDir = rtrim(Configure::read('App.paths.data'), DS);
        return $publicDir . DS . ltrim($this->_fields['setting'], DS);
    }

    /**
     * Donne le chemin vers le fichier dans /data/config (remplace data_filename)
     * @return string
     */
    protected function _getPublicFilename(): string
    {
        if (!in_array($this->_fields['name'] ?? '', self::URI_NAMES)
            || empty($this->_fields['setting'])
        ) {
            return '';
        }
        $dataDir = rtrim(Configure::read('App.paths.data'), DS);
        $publicDir = rtrim(
            Configure::read('App.paths.config', $dataDir . DS . 'config'),
            DS
        );
        return $publicDir . DS . ltrim($this->_fields['setting'], DS);
    }

    /**
     * Donne le chemin vers le fichier dans /webroot
     * @return string
     */
    protected function _getWebrootFilename(): string
    {
        if (!in_array($this->_fields['name'] ?? '', self::URI_NAMES)
            || empty($this->_fields['setting'])
        ) {
            return '';
        }
        $webrootDir = rtrim(Configure::read('OrgEntities.public_dir', WWW_ROOT), DS);
        return $webrootDir . DS . ltrim($this->_fields['setting'], DS);
    }
}
