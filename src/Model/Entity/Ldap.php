<?php
/**
 * AsalaeCore\Model\Entity\Ldap
 */

namespace AsalaeCore\Model\Entity;

use Adldap\Adldap;
use Adldap\Auth\Guard;
use Adldap\Connections\Provider;
use Adldap\Models\Model;
use Adldap\Query\Builder;
use Adldap\Query\Factory as SearchFactory;
use Adldap\Schemas\ActiveDirectory;
use Adldap\Schemas\FreeIPA;
use Adldap\Schemas\OpenLDAP;
use AsalaeCore\Factory\Utility;
use AsalaeCore\ORM\Entity;
use Cake\ORM\TableRegistry;
use ErrorException;
use Exception;

/**
 * Entité de la table ldaps
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Ldap extends Entity
{
    use AppMetaTrait;

    const LDAP_OPTS = [
        'DEREF',
        'SIZELIMIT',
        'TIMELIMIT',
        'ERROR_NUMBER',
        'RESTART',
        'HOST_NAME',
        'ERROR_STRING',
        'DIAGNOSTIC_MESSAGE',
        'MATCHED_DN',
        'SERVER_CONTROLS',
        'CLIENT_CONTROLS',
        'X_KEEPALIVE_IDLE',
        'X_KEEPALIVE_PROBES',
        'X_KEEPALIVE_INTERVAL',
        'X_TLS_CACERTDIR',
        'X_TLS_CACERTFILE',
        'X_TLS_CERTFILE',
        'X_TLS_CIPHER_SUITE',
        'X_TLS_CRLCHECK',
        'X_TLS_CRLFILE',
        'X_TLS_DHFILE',
        'X_TLS_KEYFILE',
        'X_TLS_PROTOCOL_MIN',
        'X_TLS_RANDOM_FILE',
        'X_TLS_REQUIRE_CERT',
    ];

    const LDAP_OPTS_INT = [
        'DEREF',
        'SIZELIMIT',
        'TIMELIMIT',
        'ERROR_NUMBER',
        'RESTART',
        'HOST_NAME',
        'ERROR_STRING',
        'DIAGNOSTIC_MESSAGE',
        'MATCHED_DN',
        'SERVER_CONTROLS',
        'CLIENT_CONTROLS',
        'X_KEEPALIVE_IDLE',
        'X_KEEPALIVE_PROBES',
        'X_KEEPALIVE_INTERVAL',
        'X_TLS_PROTOCOL_MIN',
        'X_TLS_REQUIRE_CERT',
    ];

    /**
     * @var Adldap
     */
    public $adldap;
    /**
     * @var Provider
     */
    public $provider;
    /**
     * @var Guard
     */
    public $auth;
    /**
     * @var SearchFactory
     */
    public $search;
    /**
     * @var Builder
     */
    public $users;

    /**
     * @var string champ app_meta
     */
    protected $metaFieldName = 'custom_options';

    /**
     * @var array Champs du app_meta
     */
    protected $_metaFields = self::LDAP_OPTS;

    /**
     * Champs virtuels
     *
     * @var array
     */
    protected $_virtual = ['deletable', 'schematrad', 'versiontrad'];

    /**
     * Donne la config pour initialiser un ldap via Adldap2
     * @return array
     */
    public function getLdapConfig(): array
    {
        $opts = [];
        foreach ($this->get('meta') ?? [] as $option => $val) {
            if (defined('LDAP_OPT_'.$option)) {
                $opts[constant('LDAP_OPT_'.$option)]
                    = in_array($option, Ldap::LDAP_OPTS_INT) ? (int)$val : $val;
            }
        }

        return [
            // Mandatory Configuration Options
            'hosts' => [$this->get('host')],
            'base_dn' => $this->get('ldap_root_search'),
            'username' => $this->get('user_query_login'),
            'password' => $this->get('user_query_password'),

            // Optional Configuration Options
            'schema' => $this->get('schema'),
            'account_prefix' => $this->get('account_prefix'),
            'account_suffix' => $this->get('account_suffix'),
            'port' => (int)$this->get('port'),
            'follow_referrals' => $this->get('follow_referrals'),
            'use_ssl' => (bool)$this->get('use_ssl'),
            'use_tls' => (bool)$this->get('use_tls'),
            'version' => $this->get('version'),
            'timeout' => $this->get('timeout'),

            // Custom LDAP Options
            'custom_options' => $opts,
        ];
    }

    /**
     * Donne une instance Adldap
     * @return Adldap
     */
    public function adldap(): Adldap
    {
        if (empty($this->adldap)) {
            try {
                $this->adldap = Utility::get(Adldap::class);
                $this->adldap->addProvider($this->getLdapConfig());
            } catch (Exception $e) {
            }
        }
        return $this->adldap;
    }

    /**
     * Connexion au LDAP
     * @return Provider|null
     */
    public function connect(): ?Provider
    {
        if (empty($this->provider)) {
            try {
                set_error_handler(
                    function ($errno, $errstr, $errfile, $errline) {
                        throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
                    }
                );
                $ad = $this->adldap();
                $this->provider = $ad->connect();
            } catch (Exception $e) {
            } finally {
                restore_error_handler();
            }
        }
        return $this->provider;
    }

    /**
     * Vérifi la connexion au LDAP
     * @return bool
     */
    public function ping(): bool
    {
        return (bool)$this->connect();
    }

    /**
     * Authentifie au LDAP
     * @return Guard|null
     */
    public function auth(): ?Guard
    {
        if (empty($this->auth) && $this->connect()) {
            $this->auth = $this->connect()->auth();
        }
        return $this->auth;
    }

    /**
     * Initialise une recherche
     * @return SearchFactory|null
     */
    public function search(): ?SearchFactory
    {
        if (empty($this->search) && $this->connect()) {
            $this->search = $this->connect()->search();
        }
        return $this->search;
    }

    /**
     * Initialise une recherche avec les filtres configurés
     * @param string|null $sortBy
     * @return Builder|null
     */
    public function searchWithFilters(string $sortBy = null): ?Builder
    {
        if (!$sortBy) {
            $sortBy = $this->get('user_login_attribute');
        }
        return $this->search()
            ? $this->search()
                ->newQuery()
                ->sortBy($sortBy)
                ->rawFilter($this->get('ldap_users_filter'))
            : null;
    }

    /**
     * Initialise une recherche d'utilisateur
     * @return Builder|null
     */
    public function users(): ?Builder
    {
        if (empty($this->users) && $this->search()) {
            $this->users = $this->search()->users();
        }
        return $this->users;
    }

    /**
     * Action de login
     * Renvoi l'entrée LDAP ou le succès de la vérification du password
     * @param string $login
     * @param string $password
     * @param bool   $bindAsUser
     * @return Model|bool
     * @throws Exception
     */
    public function log(string $login, string $password, bool $bindAsUser = true)
    {
        set_error_handler(
            function ($errno, $errstr, $errfile, $errline) {
                throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
            }
        );
        $passwordOk = false;
        try {
            if (!$this->auth()) {
                return false;
            }
            $passwordOk = $this->auth()->attempt($login, $password, $bindAsUser);
        } finally {
            restore_error_handler();
        }
        return $passwordOk
            ? ($this
                ->search()
                ->newQuery()
                ->where($this->get('user_login_attribute'), '=', $login)->first() ?: true)
            : false;
    }

    /**
     * Cette entité peut-elle être supprimée ?
     * @return bool
     */
    protected function _getDeletable(): bool
    {
        if (empty($this->_fields['id'])) {
            return true;
        }
        $Users = TableRegistry::getTableLocator()->get('Users');
        $query = $Users->find()->where(['ldap_id' => $this->_fields['id']]);
        return $query->count() === 0;
    }

    /**
     * Vrai si le ldap n'est pas utilisé
     * @return bool
     */
    protected function _getRemovable()
    {
        return $this->_getDeletable();
    }

    /**
     * Traductions du champ schema
     * @return string
     */
    protected function _getSchematrad()
    {
        switch ($schema = $this->_fields['schema'] ?? '') {
            case ActiveDirectory::class:
                return __dx('ldap', 'schema', "Active Directory");
            case OpenLDAP::class:
                return __dx('ldap', 'schema', "Open LDAP");
            case FreeIPA::class:
                return __dx('ldap', 'schema', "Free IPA");
            default:
                return $schema;
        }
    }

    /**
     * Traductions du champ version
     * @return string
     */
    protected function _getVersiontrad()
    {
        switch ($schema = $this->_fields['version'] ?? '') {
            case 2:
                return __dx('ldap', 'version', "LDAP version 2");
            case 3:
                return __dx('ldap', 'version', "LDAP version 3");
            default:
                return $schema;
        }
    }

    /**
     * Setter du champ custom_options
     * @param mixed $value
     * @return string|null
     */
    protected function _setCustomOptions($value)
    {
        return $this->_setAppMeta($value) ?: '';
    }
}
