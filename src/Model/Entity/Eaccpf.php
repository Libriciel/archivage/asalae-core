<?php
/**
 * AsalaeCore\Model\Entity\Eaccpf
 */

namespace AsalaeCore\Model\Entity;

use AsalaeCore\Utility\XmlUtility;
use Cake\I18n\FrozenTime as Time;
use AsalaeCore\ORM\Entity;
use DateTime;
use DateTimeInterface;
use Exception;

/**
 * Entité de la table eaccpfs
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Eaccpf extends Entity
{
    /**
     * Champs virtuels
     *
     * @var array
     */
    protected $_virtual = ['entity_typetrad'];

    /**
     * Traductions des entity_type
     *
     * @return string
     */
    protected function _getEntityTypetrad(): string
    {
        if (empty($this->_fields['entity_type'])) {
            return '';
        }
        switch ($this->_fields['entity_type']) {
            case 'person':
                return __dx('transfer', 'entity_type', "person");
            case 'family':
                return __dx('transfer', 'entity_type', "family");
            case 'corporateBody':
                return __dx('transfer', 'entity_type', "corporateBody");
        }
        return $this->_fields['entity_type'];
    }

    /**
     * Permet d'obtenir l'équivalent XML du champ data (json)
     *
     * @return string
     */
    protected function _getXML(): string
    {
        return XmlUtility::jsonToXmlString($this->_fields['data']);
    }

    /**
     * Permet de créer des dates à partir du format français
     * @param string|DateTimeInterface $value
     * @return Time
     */
    private function dateStringToDateObject($value)
    {
        if (is_string($value)) {
            $value = Time::parseDate($value);
        } elseif ($value instanceof DateTimeInterface) {
            $value = new Time($value);
        }

        return $value;
    }

    /**
     * Assure que le champ soit un objet
     * @param string|DateTimeInterface $value
     * @return Time
     * @throws Exception
     */
    protected function _setFromDate($value)
    {
        $newDate = $this->dateStringToDateObject($value);
        $from = $newDate ? $newDate->toDateString() : '';
        $toDate = $this->_fields['to_date'] ?? '';
        $to = $toDate
            ? $this->dateStringToDateObject($toDate)->toDateString()
            : '';

        $this->writeExistDatesInData($from, $to);
        return $newDate;
    }

    /**
     * Assure que le champ soit un objet
     * @param string|DateTimeInterface $value
     * @return Time
     * @throws Exception
     */
    protected function _setToDate($value)
    {
        $newDate = $this->dateStringToDateObject($value);
        $fromDate = $this->_fields['from_date'] ?? '';
        $from = $fromDate
            ? $this->dateStringToDateObject($fromDate)->toDateString()
            : '';

        $to = $newDate ? $newDate->toDateString() : '';
        $this->writeExistDatesInData($from, $to);
        return $newDate;
    }

    /**
     * Rempli le cpfDescription/description/existDates dans le data
     * @param string $from
     * @param string $to
     * @throws Exception
     */
    private function writeExistDatesInData(string $from, string $to)
    {
        if (isset($this->_fields['data'])) {
            $json = json_decode($this->_fields['data'], true);
            /** @noinspection PhpIssetCanBeReplacedWithCoalesceInspection ne fonctionne pas par reference */
            if (isset($json['eac-cpf']['cpfDescription'])) {
                $cpfDescription =& $json['eac-cpf']['cpfDescription'];
            } else {
                $cpfDescription =& $json['eac-cpf']['multipleIdentities']['cpfDescription'][0];
            }
            $exists = $this->getExistDates($from, $to);
            if ($exists && $this->updateExistsDates($cpfDescription, $exists)) {
                $cpfDescription['description']['existDates'] = $exists;
                $this->_fields['data'] = json_encode($json);
                $this->setDirty('data');
            }
        }
    }

    /**
     * Converti les champs from_date et to_date en array en vue d'insérer la
     * donnée dans le XML (data)
     * @param string $from
     * @param string $to
     * @return array
     */
    private function getExistDates(string $from, string $to): array
    {
        if ($from && $to) {
            return [
                'dateRange' => [
                    'fromDate' => $from,
                    'toDate' => $to,
                ]
            ];
        } elseif ($from || $to) {
            return ['date' => $from ?: $to];
        }
        return [];
    }

    /**
     * @var string chemin vers le fichier écrit de façon temporaire sur disque
     */
    private $tmpFilename;

    /**
     * Donne un fichier temporaire du content
     * Utile pour les fonctions qui n'acceptent pas les ressources
     * @return string
     */
    protected function _getTmpFilename(): string
    {
        if (empty($this->tmpFilename)) {
            $this->tmpFilename = sys_get_temp_dir().DS.uniqid('cpf-').'.xml';
            file_put_contents($this->tmpFilename, $this->_getXML());
        }
        return $this->tmpFilename;
    }

    /**
     * Détruit le fichier temporaire lié à l'entité
     */
    public function __destruct()
    {
        if (!empty($this->tmpFilename) && is_file($this->tmpFilename)) {
            unlink($this->tmpFilename);
        }
    }

    /**
     * Cet ensemble de blocs sert à comparer les dates par rapport à
     * celles existantes, si il y a changement on redefini data et on setDirty
     * @param array $cpfDescription
     * @param array $exists
     * @return void
     * @throws Exception
     */
    private function updateExistsDates(array $cpfDescription, array $exists): bool
    {
        if (isset($cpfDescription['description']['existDates']['date'])
            && isset($exists['date'])
        ) {
            $ref =& $cpfDescription['description']['existDates']['date'];
            $date = new DateTime(is_string($ref) ? $ref : $ref['@']);
            $newDate = new DateTime($exists['date']);
            if ($date->format('Y-m-d') === $newDate->format('Y-m-d')) {
                return false;
            }
        }
        if (isset($cpfDescription['description']['existDates']['dateRange']['fromDate'])
            && isset($exists['dateRange']['fromDate'])
        ) {
            $ref =& $cpfDescription['description']['existDates']['dateRange']['fromDate'];
            $fromdate = new DateTime(is_string($ref) ? $ref : $ref['@']);
            $newFromdate = new DateTime($exists['dateRange']['fromDate']);
            $same = false;
            if ($fromdate->format('Y-m-d') === $newFromdate->format('Y-m-d')) {
                $same = true;
            }
            if ($same
                && isset($cpfDescription['description']['existDates']['dateRange']['toDate'])
                && isset($exists['dateRange']['toDate'])
            ) {
                $ref =& $cpfDescription['description']['existDates']['dateRange']['toDate'];
                $todate = new DateTime(is_string($ref) ? $ref : $ref['@']);
                $newTodate = new DateTime($exists['dateRange']['toDate']);
                if ($todate->format('Y-m-d') === $newTodate->format('Y-m-d')) {
                    return false;
                }
            }
        } elseif (isset($cpfDescription['description']['existDates']['dateRange']['toDate'])
            && isset($exists['dateRange']['toDate'])
        ) {
            $ref =& $cpfDescription['description']['existDates']['dateRange']['toDate'];
            $todate = new DateTime(is_string($ref) ? $ref : $ref['@']);
            $newTodate = new DateTime($exists['dateRange']['toDate']);
            if ($todate->format('Y-m-d') === $newTodate->format('Y-m-d')) {
                return false;
            }
        }
        return true;
    }
}
