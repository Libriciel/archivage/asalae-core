<?php
/**
 * AsalaeCore\Model\Entity\Filter
 */

namespace AsalaeCore\Model\Entity;

use AsalaeCore\ORM\Entity;

/**
 * Entité de la table filters
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Filter extends Entity
{
}
