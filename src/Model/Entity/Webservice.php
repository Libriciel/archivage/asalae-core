<?php
/**
 * AsalaeCore\Model\Entity\Webservice
 */

namespace AsalaeCore\Model\Entity;

use AsalaeCore\ORM\Entity;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Utility\Security;
use Exception;

/**
 * Entité de la table webservices
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Webservice extends Entity
{
    /**
     * @var string clef de sécurité
     */
    private static $key;

    /**
     * Permet d'obtenir la clef de sécurité
     * @return string
     */
    private function getSecurityKey(): string
    {
        if (!self::$key) {
            self::$key = md5(__CLASS__); // NOSONAR permet simplement d'avoir la bonne taille
        }
        return self::$key;
    }

    /**
     * Permet d'obtenir le noeud parent d'un point de vue Permissions
     *
     * @return null
     */
    public function parentNode()
    {
        return null;
    }

    /**
     * Encrypte le mot de passe
     *
     * @param string $password
     * @return string
     */
    protected function _setPassword(string $password): string
    {
        $hasher = new DefaultPasswordHasher;
        return $hasher->hash($password) ?: '';
    }

    /**
     * Accès public au setter du client_secret
     * @param string $secret
     * @return string
     */
    public function clientSecretSetter(string $secret): string
    {
        return $this->_setClientSecret($secret);
    }

    /**
     * Protège le client_secret
     * @param string $secret
     * @return string
     */
    protected function _setClientSecret(string $secret): string
    {
        return bin2hex(Security::encrypt($secret, $this->getSecurityKey()));
    }

    /**
     * Getter du secret décripté
     * @return string
     */
    protected function _getClientSecretUnsecure(): string
    {
        return $this->_fields['client_secret']
            ? Security::decrypt(hex2bin($this->_fields['client_secret']), $this->getSecurityKey())
            : ''
        ;
    }

    /**
     * Permet de vérifier si un password correspond à celui de l'entité
     * @param string $password
     * @return bool
     */
    public function comparePassword(string $password): bool
    {
        $hasher = new DefaultPasswordHasher;
        return $hasher->check($password, $this->_fields['password']);
    }

    /**
     * Génère un code via random_bytes, qui est cryptographiquement sûr
     * @param int $size
     * @return string
     * @throws Exception
     */
    public function generateCode(int $size = 10): string
    {
        return bin2hex(random_bytes($size));
    }
}
