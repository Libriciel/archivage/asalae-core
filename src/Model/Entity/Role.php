<?php
/**
 * AsalaeCore\Model\Entity\Role
 */

namespace AsalaeCore\Model\Entity;

use Cake\Core\Configure;
use AsalaeCore\ORM\Entity;
use Cake\ORM\TableRegistry;

/**
 * Entité de la table roles
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Role extends Entity
{
    const WEBSERVICES = ['WSV', 'WSA', 'WSR'];

    /**
     * Champs virtuels
     *
     * @var array
     */
    protected $_virtual = ['deletable', 'editable'];

    /**
     * Permet d'obtenir le noeud parent d'un point de vue Permissions
     * @return null|array
     */
    public function parentNode()
    {
        if (!$parentId = $this->get('parent_id')) {
            return null;
        }
        return ['model' => 'Roles', 'foreign_key' => $parentId];
    }

    /**
     * Ce rôle peut-il être supprimé ?
     * @return bool
     */
    protected function _getDeletable(): bool
    {
        if (empty($this->_fields['id'])) {
            return true;
        }
        $Users = TableRegistry::getTableLocator()->get('Users');
        $query = $Users->find()->where(['role_id' => $this->_fields['id']]);
        return $query->count() === 0;
    }

    /**
     * Ce rôle peut-il être édité ?
     * @return bool
     */
    protected function _getEditable(): bool
    {
        if (empty($this->_fields['id'])) {
            return true;
        }
        if (empty($this->_fields['org_entity_id']) && !Configure::read('Roles.global_is_editable')) {
            return false;
        }
        return true;
    }
}
