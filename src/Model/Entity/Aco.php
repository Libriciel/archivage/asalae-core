<?php
/**
 * AsalaeCore\Model\Entity\Aco
 */

namespace AsalaeCore\Model\Entity;

use Acl\Model\Entity\Aco as AclAco;

/**
 * Entité de la table acos
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Aco extends AclAco
{
}
