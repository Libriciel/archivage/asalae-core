<?php
/**
 * AsalaeCore\Model\Entity\PremisAgentEntityInterface
 */

namespace AsalaeCore\Model\Entity;

use AsalaeCore\Utility\Premis;

/**
 * Interface pour transformer une entité en Agent Premis
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
interface PremisAgentEntityInterface
{
    /**
     * Donne un objet premis à partir d'une entité
     * @return Premis\Agent
     */
    public function toPremisAgent(): Premis\Agent;
}
