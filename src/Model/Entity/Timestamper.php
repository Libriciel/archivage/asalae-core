<?php
/**
 * AsalaeCore\Model\Entity\Timestamper
 */

namespace AsalaeCore\Model\Entity;

use AsalaeCore\Model\Table\TimestampersTable;
use AsalaeCore\ORM\Entity;
use Cake\Utility\Security;

/**
 * Entité de la table timestampers
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Timestamper extends Entity
{
    /**
     * Traits
     */
    use AppMetaTrait;

    /**
     * @var string Nom du champ app_meta (pour AppMetaTrait)
     */
    protected $metaFieldName = 'fields';

    /**
     * @var array Champs
     */
    protected $_metaFields = [
        'driver',
        'name',
        'url',
        'useProxy',
        'proxyHost',
        'proxyPort',
        'proxyLogin',
        'proxyPassword',
        'timestamp_format',
        'hash_algo',
        'login',
        'password',
        'ca',
        'crt',
        'pem',
        'cnf',
    ];

    /**
     * Setter de driver du app_meta
     * @param string $value
     * @return mixed
     */
    protected function _setDriver($value)
    {
        return $this->genericAppMetaSetter('driver', $value);
    }

    /**
     * Setter de name du app_meta
     * @param string $value
     * @return mixed
     */
    protected function _setName($value)
    {
        return $this->genericAppMetaSetter('name', $value);
    }

    /**
     * Setter de url du app_meta
     * @param string $value
     * @return mixed
     */
    protected function _setUrl($value)
    {
        return $this->genericAppMetaSetter('url', $value);
    }

    /**
     * Setter de proxy_host du app_meta
     * @param string $value
     * @return mixed
     */
    protected function _setProxyHost($value)
    {
        return $this->genericAppMetaSetter('proxy_host', $value);
    }

    /**
     * Setter de proxy_port du app_meta
     * @param string|int $value
     * @return mixed
     */
    protected function _setProxyPort($value)
    {
        return $this->genericAppMetaSetter('proxy_port', $value);
    }

    /**
     * Setter de proxy_login du app_meta
     * @param string $value
     * @return mixed
     */
    protected function _setProxyLogin($value)
    {
        return $this->genericAppMetaSetter('proxy_login', $value);
    }

    /**
     * Setter de proxy_password du app_meta
     * @param string $value
     * @return mixed
     */
    protected function _setProxyPassword($value)
    {
        return $this->genericAppMetaSetter('proxy_password', $value);
    }

    /**
     * Getter de proxy_password du app_meta
     * @return string|null
     */
    protected function _getProxyPasswordDecrypted()
    {
        return Security::decrypt(
            base64_decode($this->_fields['proxy_password']),
            md5(TimestampersTable::SECURITY_KEY) // NOSONAR permet simplement d'avoir la bonne taille
        );
    }

    /**
     * Setter de timestamp_format du app_meta
     * @param mixed $value
     * @return mixed
     */
    protected function _setTimestampFormat($value)
    {
        return $this->genericAppMetaSetter('timestamp_format', $value);
    }

    /**
     * Setter de hash_algo du app_meta
     * @param string $value
     * @return mixed
     */
    protected function _setHashAlgo($value)
    {
        return $this->genericAppMetaSetter('hash_algo', $value);
    }

    /**
     * Setter de login du app_meta
     * @param string $value
     * @return mixed
     */
    protected function _setLogin($value)
    {
        return $this->genericAppMetaSetter('login', $value);
    }

    /**
     * Setter de password du app_meta
     * @param string $value
     * @return mixed
     */
    protected function _setPassword($value)
    {
        return $this->genericAppMetaSetter('password', $value);
    }

    /**
     * Getter de password du app_meta
     * @return string|null
     */
    protected function _getPasswordDecrypted()
    {
        return Security::decrypt(
            base64_decode($this->_fields['password']),
            md5(TimestampersTable::SECURITY_KEY) // NOSONAR permet simplement d'avoir la bonne taille
        );
    }

    /**
     * Setter de ca du app_meta
     * @param string $value
     * @return mixed
     */
    protected function _setCa($value)
    {
        return $this->genericAppMetaSetter('ca', $value);
    }

    /**
     * Setter de crt du app_meta
     * @param string $value
     * @return mixed
     */
    protected function _setCrt($value)
    {
        return $this->genericAppMetaSetter('crt', $value);
    }

    /**
     * Setter de pem du app_meta
     * @param string $value
     * @return mixed
     */
    protected function _setPem($value)
    {
        return $this->genericAppMetaSetter('pem', $value);
    }

    /**
     * Setter de cnf du app_meta
     * @param string $value
     * @return mixed
     */
    protected function _setCnf($value)
    {
        return $this->genericAppMetaSetter('cnf', $value);
    }
}
