<?php
/**
 * AsalaeCore\Model\Entity\Cron
 */

namespace AsalaeCore\Model\Entity;

use AsalaeCore\Cron\CronInterface;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use AsalaeCore\ORM\Entity;
use Cake\ORM\TableRegistry;
use Cake\Utility\Inflector;
use Exception;

/**
 * Entité de la table crons
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Cron extends Entity
{
    /**
     * Traits
     */
    use IntervalTrait, AppMetaTrait {
        AppMetaTrait::__construct as private appMetaTraitContruct;
    }

    /**
     * Champs virtuels
     *
     * @var array
     */
    protected $_virtual = ['frequencytrad', 'lastexec', 'deletable'];


    /**
     * @var array Liste des champs virtuels du app_meta
     */
    protected $metaFields = [];

    /**
     * Cron constructor.
     * @param array $properties
     * @param array $options
     */
    public function __construct(array $properties = [], array $options = [])
    {
        parent::__construct($properties, $options);
        /** @var string|CronInterface $className */
        $className = $properties['classname'] ?? null;
        $this->metaFields = $className ? array_keys($className::getVirtualFields()) : [];
        $this->appMetaTraitContruct($properties, $options);
    }

    /**
     * Setter du champ virtuel wsdl (stocké dans app_meta)
     * @param string $value
     * @return string
     */
    protected function _setWsdl(string $value)
    {
        return $this->genericAppMetaSetter('wsdl', $value);
    }

    /**
     * Setter du champ virtuel endpoint (stocké dans app_meta)
     * @param string $value
     * @return string
     */
    protected function _setEndpoint(string $value)
    {
        return $this->genericAppMetaSetter('endpoint', $value);
    }

    /**
     * Setter du champ virtuel action_version (stocké dans app_meta)
     * @param string $value
     * @return string
     */
    protected function _setActionVersion(string $value)
    {
        return $this->genericAppMetaSetter('action_version', $value);
    }

    /**
     * Setter du champ virtuel action_data (stocké dans app_meta)
     * @param string $value
     * @return string
     */
    protected function _setActionData(string $value)
    {
        return $this->genericAppMetaSetter('action_data', $value);
    }

    /**
     * Setter du champ virtuel extract_version (stocké dans app_meta)
     * @param string $value
     * @return string
     */
    protected function _setExtractVersion(string $value)
    {
        return $this->genericAppMetaSetter('extract_version', $value);
    }

    /**
     * Setter du champ virtuel extract_data (stocké dans app_meta)
     * @param string $value
     * @return string
     */
    protected function _setExtractData(string $value)
    {
        return $this->genericAppMetaSetter('extract_data', $value);
    }

    /**
     * Setter du champ virtuel use_proxy (stocké dans app_meta)
     * @param bool $value
     * @return string
     */
    protected function _setUseProxy(bool $value)
    {
        return $this->genericAppMetaSetter('use_proxy', $value);
    }


    /**
     * Setter du champ virtuel max_execution_time (stocké dans app_meta)
     * @param int $value
     * @return string
     */
    protected function _setMaxExecutionTime(int $value)
    {
        return $this->genericAppMetaSetter('max_execution_time', $value);
    }

    /**
     * Setter du champ virtuel min_delay (stocké dans app_meta)
     * @param int $value
     * @return string
     */
    protected function _setMinDelay(int $value)
    {
        return $this->genericAppMetaSetter('min_delay', $value);
    }

    /**
     * Setter du champ virtuel freezedInterval (stocké dans app_meta)
     * @param string $value
     * @return string
     */
    protected function _setFreezedInterval(string $value)
    {
        return $this->genericAppMetaSetter('freezedInterval', $value);
    }

    /**
     * Setter du champ virtuel retention_delay (stocké dans app_meta)
     * @param string $value
     * @return string
     */
    protected function _setRetentionDelay(string $value)
    {
        return $this->genericAppMetaSetter('retention_delay', $value);
    }

    /**
     * Setter du champ virtuel update_url (stocké dans app_meta)
     * @param string $value
     * @return string
     */
    protected function _setUpdateUrl(string $value)
    {
        return $this->genericAppMetaSetter('update_url', $value);
    }

    /**
     * Setter du champ virtuel update_md5 (stocké dans app_meta)
     * @param string $value
     * @return string
     */
    protected function _setUpdateMd5(string $value)
    {
        return $this->genericAppMetaSetter('update_md5', $value);
    }

    /**
     * =========================================================================
     * Bloc interval
     * =========================================================================
     */

    /**
     * Traductions d'un interval (simplifié car 1 seule unité)
     * @return null|string
     * @throws Exception
     */
    protected function _getFrequencytrad()
    {
        return $this->tradInterval('frequency');
    }

    /**
     * Donne le nombre d'unité dans frequency
     * @return string|null
     */
    protected function _getFrequencyBuild()
    {
        return $this->getIntervalNumber('frequency');
    }

    /**
     * Set la valeur du champ frequency
     * @param string $build
     * @return string
     */
    protected function _setFrequencyBuild(string $build)
    {
        $unit = $this->_fields['frequency_build_unit'] ?? '';
        $this->set('frequency', str_replace('_', $build, $unit));
        return $build;
    }

    /**
     * Permet d'obtenir la valeur du select des unités de fréquence
     * @return null
     * @throws Exception
     */
    protected function _getFrequencyBuildUnit()
    {
        return $this->getIntervalOptionKey('frequency');
    }

    /**
     * Options des unités de période
     * @return array
     */
    protected function _getUnits(): array
    {
        return $this->getIntervalOptions();
    }

    /**
     * Converti la nouvelle période en interval
     * @param string $period
     * @return string
     * @throws Exception
     */
    protected function _setFrequency(string $period)
    {
        $this->setInterval('frequency');
        return $period;
    }

    /**
     * Donne l'unité utilisé pour la période dans le champ frequency
     * @return string|null
     * @throws Exception
     */
    protected function _getFrequencyUnit()
    {
        return $this->getIntervalUnit('frequency');
    }

    /**
     * =========================================================================
     * Fin du bloc interval
     * =========================================================================
     */

    /**
     * Permet d'obtenir la derniere execution
     * @return array|EntityInterface|null
     */
    protected function _getLastexec()
    {
        if (empty($this->_fields['id'])) {
            return null;
        }
        $execs = TableRegistry::getTableLocator()->get('CronExecutions');
        return $execs->find()
            ->where(['cron_id' => $this->_fields['id']])
            ->order(
                [
                    'date_begin' => 'desc',
                ]
            )
            ->first();
    }

    /**
     * Donne le contenu du log lié au cron s'il existe
     * @return string
     */
    protected function _getLog(): string
    {
        if (empty($this->_fields['classname'])) {
            return '';
        }
        $filename = $this->_getLogFilename();
        return is_readable($filename) ? file_get_contents($filename) : '';
    }

    /**
     * Donne le chemin vers le fichier log du cron
     * @return string
     */
    protected function _getLogFilename(): string
    {
        if (empty($this->_fields['classname'])) {
            return '';
        }
        $classname = substr(strrchr($this->_fields['classname'], "\\"), 1);
        return Configure::read('App.paths.logs', LOGS) . 'cron-' . Inflector::underscore($classname) . '.log';
    }

    /**
     * Vrai si supprimable
     * @return bool
     */
    protected function _getDeletable(): bool
    {
        if ($this->isNew() || $this->get('frequency') !== 'one_shot') {
            return false;
        }
        return empty($this->_fields['locked']);
    }
}
