<?php
/**
 * AsalaeCore\Model\Entity\Session
 */

namespace AsalaeCore\Model\Entity;

use AsalaeCore\Factory\Utility;
use AsalaeCore\Model\Table\OrgEntitiesTable;
use AsalaeCore\Utility\Session as SessionUtility;
use Cake\Datasource\EntityInterface;
use AsalaeCore\ORM\Entity;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Exception;
use JsonSerializable;

/**
 * Entité de la table sessions
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Session extends Entity implements JsonSerializable
{

    /**
     * Décode la ressource pour permettre la conversion en json
     * @return array|mixed
     * @throws Exception
     */
    public function jsonSerialize(): array
    {
        return $this->toArray();
    }

    /**
     * Converti les ressources en array
     * @return array
     * @throws Exception
     */
    public function toArray(): array
    {
        $data = parent::toArray();
        if (isset($data['data']) && !is_array($data['data'])) {
            if (gettype($data['data']) === 'resource') {
                rewind($data['data']);
                $data['data'] = stream_get_contents($data['data']);
            }
            /** @var SessionUtility $Session */
            $Session = Utility::get('Session');
            $data['data'] = $Session->unserialize($data['data']);
        }
        return $data;
    }

    /**
     * met à jour les ServiceArchives d'une session suite à une modification (lft et rght)
     * @throws Exception
     */
    public function updateOrgEntities()
    {
        $data = self::toArray()['data'];
        if (!Hash::get($data, 'Auth.org_entity')
            || !Hash::get($data, 'Auth.org_entity.archival_agency')
        ) {
            return;
        }
        $orgEntity =& $data['Auth']['org_entity'];
        if ($orgEntity instanceof EntityInterface) {
            $archivalAgency = $orgEntity->get('archival_agency');
        } else {
            $archivalAgency =& $data['Auth']['org_entity']['archival_agency'];
        }
        $dirtyEntity = $this->updateThisEntity($orgEntity);
        $dirtyArchivalAgency = $this->updateThisEntity($archivalAgency);
        if ($dirtyEntity || $dirtyArchivalAgency) {
            /** @var SessionUtility $Session */
            $Session = Utility::get('Session');
            $this->_fields['data'] = $Session->serialize($data);
            $this->setDirty('data');
        }
    }

    /**
     * Récupère les infos en base pour mettre à jour une entité
     * @param EntityInterface|array $orgEntity
     * @return bool dirty
     */
    private function updateThisEntity(&$orgEntity)
    {
        if ($orgEntity) {
            /** @var OrgEntitiesTable $OrgEntities */
            $OrgEntities = TableRegistry::getTableLocator()->get('OrgEntities');
            $dbEntity = $OrgEntities->get(Hash::get($orgEntity, 'id'));
            $dirtyEntity = ($dbEntity->get('lft') !== Hash::get($orgEntity, 'lft'))
                || ($dbEntity->get('rght') !== Hash::get($orgEntity, 'rght'))
                || ($dbEntity->get('name') !== Hash::get($orgEntity, 'name'));
            if ($dirtyEntity) {
                if ($orgEntity instanceof EntityInterface) {
                    $orgEntity->set('name', $dbEntity->get('name'));
                    $orgEntity->set('lft', $dbEntity->get('lft'));
                    $orgEntity->set('rght', $dbEntity->get('rght'));
                } else {
                    $orgEntity['name'] = $dbEntity->get('name');
                    $orgEntity['lft'] = $dbEntity->get('lft');
                    $orgEntity['rght'] = $dbEntity->get('rght');
                }
            }
        }
        return $dirtyEntity ?? false;
    }
}
