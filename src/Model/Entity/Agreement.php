<?php
/**
 * AsalaeCore\Model\Entity\Agreement
 */

namespace AsalaeCore\Model\Entity;

use AsalaeCore\ORM\Entity;
use Exception;

/**
 * Entité de la table Agreements
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Agreement extends Entity
{
    /**
     * Traits
     */
    use IntervalTrait;

    /**
     * Champs virtuels
     *
     * @var array
     */
    protected $_virtual = ['transfer_periodtrad', 'deletable'];

    /**
     * constructor.
     * @param array $properties
     * @param array $options
     * @throws Exception
     */
    public function __construct(array $properties = [], array $options = [])
    {
        if (empty($properties['mult_max_size_per_transfer'])) {
            $mult = 1;
            $bytes = $properties['max_size_per_transfer'] ?? 0;
            if ($bytes && (int)$bytes % 1024 === 0) {
                $pow = 0;
                while ($bytes >= 1024) {
                    $bytes /= 1024;
                    if (++$pow >= 5 || $bytes % 1024 !== 0) {
                        break;
                    }
                }
                $mult = pow(1024, $pow);
            }
            $properties['mult_max_size_per_transfer'] = $mult;
        }
        if (empty($properties['max_size_per_transfer_conv'])
            && !empty($properties['max_size_per_transfer'])
        ) {
            $properties['max_size_per_transfer_conv']
                = $properties['max_size_per_transfer'] / $properties['mult_max_size_per_transfer'];
        }
        parent::__construct($properties, $options);
    }

    /**
     * Set max_size_per_transfer en fonction de la valeur et du mult
     * @param int $value
     * @return int|null
     */
    protected function _setMaxSizePerTransferConv($value)
    {
        $this->setDirty('max_size_per_transfer');
        if (empty($value)) {
            $this->_fields['max_size_per_transfer'] = null;
            return null;
        }
        $mult = empty($this->_fields['mult_max_size_per_transfer'])
            ? 1
            : $this->_fields['mult_max_size_per_transfer'];
        $this->_fields['max_size_per_transfer'] = $value * $mult;
        return $value;
    }

    /**
     * Traductions du champ transfer_period
     * @return string
     */
    protected function _getTransferPeriodtrad()
    {
        switch ($period = $this->_fields['transfer_period'] ?? '') {
            case 'year':
                return __dx('agreement', 'transfer_period', "Année en cours");
            case 'month':
                return __dx('agreement', 'transfer_period', "Mois en cours");
            case 'week':
                return __dx('agreement', 'transfer_period', "Semaine en cours");
            case 'day':
                return __dx('agreement', 'transfer_period', "Jour en cours");
            default:
                return $period;
        }
    }

    /**
     * Cette entité peut-elle être supprimée ?
     * @return bool
     * @deprecated conditions sur des models non présent dans le core
     */
    protected function _getDeletable(): bool
    {
        return true;
    }
}
