<?php
/**
 * AsalaeCore\Model\Entity\IntervalTrait
 */

namespace AsalaeCore\Model\Entity;

use DateInterval;
use Exception;

/**
 * Permet une gestion des interval: PT2H en base, affiché '2 heures' dans la vue
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
trait IntervalTrait
{
    /**
     * @var DateInterval[]
     */
    private $intervals = [];

    /**
     * Traductions d'un interval (simplifié car 1 seule unité)
     * @param string $field nom du champ à convertir (P1D = 1 jour)
     * @return null|string
     * @throws Exception
     */
    protected function tradInterval($field)
    {
        if (empty($this->_fields[$field])) {
            return null;
        }
        $interval = $this->getInterval($field);
        if ($n = $interval->y) {
            return __n("{0} an", "{0} ans", $n, $n);
        } elseif ($n = $interval->m) {
            return __n("{0} mois", "{0} mois", $n, $n);
        } elseif ($n = $interval->d) {
            return __n("{0} jour", "{0} jours", $n, $n);
        } elseif ($n = $interval->h) {
            return __n("{0} heure", "{0} heures", $n, $n);
        } elseif ($n = $interval->i) {
            return __n("{0} minute", "{0} minutes", $n, $n);
        } elseif ($n = $interval->s) {
            return __n("{0} seconde", "{0} secondes", $n, $n);
        }
        return null;
    }


    /**
     * Donne l'objet interval issue de frequency
     * @param string $field
     * @return DateInterval
     * @throws Exception
     */
    private function getInterval($field): DateInterval
    {
        if (empty($this->_fields[$field])) {
            $this->_fields[$field] = 'PT0S';
        }
        if (empty($this->intervals[$field])) {
            try {
                $this->intervals[$field] = new DateInterval($this->_fields[$field]);
            } catch (Exception $e) {
            }
        }
        if (empty($this->intervals[$field])) {
            $this->intervals[$field] = new DateInterval('PT0S');
        }
        return $this->intervals[$field];
    }

    /**
     * Redéfini un interval sauvegardé
     * @param string $field
     * @throws Exception
     */
    private function setInterval($field)
    {
        unset($this->intervals[$field]);
        $this->getInterval($field);
    }

    /**
     * Donne les options d'interval
     * @return array
     */
    private function getIntervalOptions(): array
    {
        return [
            'P_Y' => __("ans"),
            'P_M' => __("mois"),
            'P_D' => __("jours"),
            'PT_H' => __("heures"),
            'PT_M' => __("minutes"),
        ];
    }

    /**
     * Donne l'unité utilisé dans l'interval (ex: P1D => 'd')
     * @param string $field
     * @return string|null
     * @throws Exception
     */
    private function getIntervalUnit(string $field)
    {
        if (empty($this->_fields[$field])) {
            return null;
        }
        $interval = $this->getInterval($field);
        foreach (['y', 'm', 'd', 'h', 'i', 's'] as $unit) {
            if ($interval->$unit) {
                return $unit;
            }
        }
        return null;
    }

    /**
     * Donne le nombre contenu dans un interval (ex: PT22H => 22)
     * @param string $field
     * @return null|string
     */
    private function getIntervalNumber(string $field)
    {
        if (empty($this->_fields[$field])) {
            return null;
        }
        $num = preg_replace('/\D+/', '', $this->_fields[$field]);
        return $num ?: null;
    }

    /**
     * Donne l'option sélectionné à partir d'une valeur (ex: PT12H => 'PT_H')
     * @param string $field
     * @return null
     * @throws Exception
     */
    private function getIntervalOptionKey(string $field)
    {
        if (empty($this->_fields[$field])) {
            return null;
        }

        $translation = [
            'y' => 'P_Y',
            'm' => 'P_M',
            'd' => 'P_D',
            'h' => 'PT_H',
            'i' => 'PT_M',
            's' => 'PT_S',
        ];
        return $translation[$this->getIntervalUnit($field)] ?? null;
    }
}
