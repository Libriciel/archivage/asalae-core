<?php
/**
 * AsalaeCore\Model\Entity\PremisObjectEntityInterface
 */

namespace AsalaeCore\Model\Entity;

use AsalaeCore\Utility\Premis;

/**
 * Interface pour transformer une entité en Objet Premis
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
interface PremisObjectEntityInterface
{
    /**
     * Donne un objet premis à partir d'une entité
     * @return Premis\ObjectInterface
     */
    public function toPremisObject(): Premis\ObjectInterface;
}
