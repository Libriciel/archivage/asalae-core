<?php
/**
 * AsalaeCore\Model\Entity\Fileupload
 */

namespace AsalaeCore\Model\Entity;

use AsalaeCore\ORM\Entity;
use FileConverters\Utility\FileConverters;
use JsonSerializable;

/**
 * Entité de la table fileuploads
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Fileupload extends Entity implements JsonSerializable
{
    /**
     * Champs virtuels
     *
     * @var array
     */
    protected $_virtual = ['convertable', 'openable', 'statetrad', 'filename'];

    /**
     * List of property names that should **not** be included in JSON or Array
     * representations of this Entity.
     *
     * @var array
     */
    protected $_hidden = ['content'];

    /**
     * @var resource content
     */
    protected $handle;

    /**
     * Permet d'obtenir la liste des formats disponible pour la conversion
     *
     * @return array
     */
    protected function _getConvertable()
    {
        return FileConverters::matchFormats(
            $this->_fields['path'] ?? ''
        );
    }

    /**
     * Permet de savoir si il existe un lecteur pour ce fichier
     *
     * @return bool
     */
    protected function _getOpenable(): bool
    {
        $mime = $this->_fields['mime'] ?? '';

        if (preg_match('/^(image\/.*|application\/pdf|application\/vnd\.oasis\.opendocument\..*)$/', $mime)) {
            return true;
        }

        return false;
    }

    /**
     * Traductions des états
     *
     * @return null|string
     */
    protected function _getStatetrad()
    {
        switch ($this->_fields['state'] ?? '') {
            case '':
                return '';
            case 'uploaded':
                return __dx('fileupload', 'state', 'uploaded');
            case 'hashed':
                return __dx('fileupload', 'state', 'hashed');
            case 'hashed_valided':
                return __dx('fileupload', 'state', 'hashed_valided');
            case 'hashed_invalided':
                return __dx('fileupload', 'state', 'hashed_invalided');
            case 'valided':
                return __dx('fileupload', 'state', 'valided');
            case 'invalided':
                return __dx('fileupload', 'state', 'invalided');
            default:
                return $this->_fields['state'];
        }
    }

    /**
     * Encode la ressource en base64 pour permettre la conversion en json
     * @return array|mixed
     */
    public function jsonSerialize(): array
    {
        $data = $this->toArray();
        if (isset($data['content']) && gettype($data['content']) === 'resource') {
            $data['content'] = $this->getBase64($data['content']);
        }
        return $data;
    }

    /**
     * Converti les ressources en base64
     * @return array
     */
    public function toArray(): array
    {
        $data = parent::toArray();
        foreach ($data as $key => $value) {
            if (gettype($value) === 'resource') {
                $data[$key] = $this->getBase64($value);
            }
        }
        return $data;
    }

    /**
     * @var string binary en base64
     */
    private $base64Binary = '';

    /**
     * Converti une ressource en base64
     * @param resource $value
     * @return string
     */
    private function getBase64($value): string
    {
        if (feof($value)) {
            return $this->base64Binary;
        }
        $binaries = '';
        while (!feof($value)) {
            $binaries .= fread($value, 8192);
        }
        $this->base64Binary = base64_encode($binaries);
        return $this->base64Binary;
    }

    /**
     * Getter du binary en base64
     * @return string
     */
    protected function _getContent64(): string
    {
        return $this->getBase64($this->_getContent());
    }

    /**
     * Permet d'insérer le binary directement en base64
     * @param string $value
     * @return string
     */
    protected function _setContent($value)
    {
        if (is_string($value)
            && preg_match('/^[a-zA-Z\d\/\r\n+]*={0,2}$/', $value)
            && $decoded = base64_decode($value, true)
        ) {
            return $decoded;
        }
        return $value;
    }

    /**
     * @var string chemin vers le fichier écrit de façon temporaire sur disque
     */
    private $tmpFilename;

    /**
     * Donne un fichier temporaire du content
     * Utile pour les fonctions qui n'acceptent pas les ressources
     * @return string
     */
    protected function _getTmpFilename(): string
    {
        if (empty($this->tmpFilename)) {
            $ext = pathinfo($this->_fields['name'] ?? '', PATHINFO_EXTENSION);
            $this->tmpFilename = sys_get_temp_dir().DS.uniqid('fileupload-').($ext ? '.'.$ext : '');
            file_put_contents($this->tmpFilename, $this->_getContent());
        }
        return $this->tmpFilename;
    }

    /**
     * Détruit le fichier temporaire lié à l'entité
     */
    public function __destruct()
    {
        if (!empty($this->tmpFilename) && is_file($this->tmpFilename)) {
            unlink($this->tmpFilename);
        }
        if ($this->handle) {
            $this->fclose();
        }
    }

    /**
     * Donne l'extention du nom de fichier
     * @return string
     */
    public function _getExt()
    {
        $name = $this->_fields['name'] ?? '';
        return strtolower(pathinfo($name, PATHINFO_EXTENSION));
    }

    /**
     * Donne le contenu du fichier uploadé
     * @return bool|null|resource
     */
    protected function _getContent()
    {
        if (empty($this->_fields['path'])) {
            return null;
        }
        if (!$this->handle) {
            $this->handle = fopen($this->_fields['path'], 'r');
        }
        return $this->handle;
    }

    /**
     * Ferme la resource créer par _getContent()
     */
    public function fclose()
    {
        if ($this->handle && is_resource($this->handle)) {
            fclose($this->handle);
            $this->handle = null;
        }
    }

    /**
     * @return bool
     */
    protected function _getDeletable(): bool
    {
        return true;
    }

    /**
     * alias de ->name
     * @return string
     */
    protected function _getFilename(): string
    {
        return $this->_fields['name'] ?? '';
    }
}
