<?php
/**
 * AsalaeCore\Model\Entity\Profile
 */

namespace AsalaeCore\Model\Entity;

use AsalaeCore\ORM\Entity;

/**
 * Entité de la table Profiles
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Profile extends Entity
{
}
