<?php
/**
 * AsalaeCore\Model\Entity\OrgEntity
 */

namespace AsalaeCore\Model\Entity;

use AsalaeCore\ORM\Entity;
use Cake\ORM\TableRegistry;

/**
 * Entité de la table org_entities
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class OrgEntity extends Entity
{
    /**
     * Champs virtuels
     *
     * @var array
     */
    protected $_virtual = ['deletable'];

    /**
     * Permet d'obtenir la liste des parents d'une entité
     */
    protected function _getParents(): array
    {
        if (!isset($this->_fields['lft']) || !isset($this->_fields['rght'])) {
            return [];
        }
        $lft = $this->_fields['lft'];
        $rght = $this->_fields['rght'];
        $OrgEntities = TableRegistry::getTableLocator()->get('OrgEntities');
        $results = $OrgEntities->find()
            ->select(['name'])
            ->where(['lft <' => $lft, 'rght >' => $rght, 'parent_id IS NOT' => null])
            ->order(['lft' => 'asc'])
            ->all();
        return array_map(
            function ($v) {
                return $v['name'];
            },
            $results->toArray()
        );
    }

    /**
     * @return bool
     */
    protected function _getDeletable(): bool
    {
        return $this->_getCode() !== 'SE';
    }

    /**
     * Permet d'obtenir le code du type d'entity du OrgEntity
     * @return string
     */
    protected function _getCode(): string
    {
        $type = $this->_fields['type_entity'] ?? [];
        if (empty($type) && !empty($this->_fields['type_entity_id'])) {
            $type = TableRegistry::getTableLocator()->get('TypeEntities')
                ->get($this->_fields['type_entity_id']);
            $this->_fields['type_entity'] = $type;
        }
        if (empty($type)) {
            return '';
        }
        return $type instanceof Entity
            ? $type->get('code')
            : $type['code'];
    }
}
