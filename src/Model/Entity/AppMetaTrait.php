<?php
/**
 * AsalaeCore\Model\Entity\AppMetaTrait
 */

namespace AsalaeCore\Model\Entity;

use AsalaeCore\ORM\Entity;
use InvalidArgumentException;
use stdClass;

/**
 * Utilisation générique du champ app_meta (json)
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin Entity
 * @property string metaFieldName
 */
trait AppMetaTrait
{
    /**
     * Contenu du app_meta sous forme d'array
     * @var stdClass
     */
    protected $meta;

    /**
     * @var array Liste des champs virtuels du app_meta
     */
    protected $metaFields = [];

    /**
     * AppMetaTrait constructor.
     * @param array $properties
     * @param array $options
     */
    public function __construct(array $properties = [], array $options = [])
    {
        $this->meta = (object)[];
        if (!empty($this->_metaFields)) {
            $this->metaFields = $this->_metaFields;
        }
        $metaFieldname = empty($this->metaFieldName) ? 'app_meta' : $this->metaFieldName;
        if (isset($properties[$metaFieldname])) {
            $this->meta = json_decode($properties[$metaFieldname]);
            if (!$this->meta) {
                $this->meta = (object)[];
            }
            foreach ($this->meta as $k => $v) {
                if (!isset($properties[$k])) {
                    $properties[$k] = $v;
                    if (!in_array($k, $this->metaFields)) {
                        $this->metaFields[] = $k;
                    }
                }
            }
        }
        parent::__construct($properties, $options);
        $this->metaFieldName = $metaFieldname;
        $this->setDirty('metaFieldName', false);
    }

    /**
     * @inheritDoc
     * @param string|array $property
     * @param mixed        $value
     * @param array        $options
     * @return $this
     */
    public function set($property, $value = null, array $options = [])
    {
        if (is_string($property) && $property !== '') {
            $guard = false;
            $property = [$property => $value];
        } else {
            $guard = true;
            $options = (array)$value;
        }

        if (!is_array($property)) {
            throw new InvalidArgumentException('Cannot set an empty property');
        }
        $options += ['setter' => true, 'guard' => $guard];

        foreach ($property as $p => $value) {
            if ($options['guard'] === true && !$this->isAccessible($p)) {
                continue;
            }

            $this->setDirty($p);

            if (!array_key_exists($p, $this->_original)
                && array_key_exists($p, $this->_fields)
                && $this->_fields[$p] !== $value
            ) {
                $this->_original[$p] = $this->_fields[$p];
            }

            if (!$options['setter']) {
                $this->_fields[$p] = $value;
                continue;
            }

            $setter = static::_accessor($p, 'set');
            if ($setter) {
                $value = $this->{$setter}($value);
            } elseif (in_array($p, $this->metaFields)) { // debut ajout
                $value = $this->genericAppMetaSetter($p, $value);
            } // fin ajout
            $this->_fields[$p] = $value;
        }

        return $this;
    }

    /**
     * ajoute une entrée dans le app_meta (modifieds)
     * @param null|integer|string $user_id
     * @return self
     */
    public function appendModified($user_id = null)
    {
        $dirty = [];
        foreach ($this->getDirty() as $key) {
            if (!in_array($key, $this->metaFields)) {
                $dirty[] = $key;
            }
        }
        if (empty($dirty)) {
            return $this;
        }
        if (!isset($this->meta->modifieds)) {
            $this->meta->modifieds = [];
        }
        $data = [
            'user_id' => $user_id,
            'date' => date('Y-m-d H:i:s'),
            'fields' => $this->getDirty()
        ];
        $this->meta->modifieds[] = $data;
        $this->rebuildAppMeta();
        return $this;
    }

    /**
     * Fait appel à set(app_meta) uniquement si la valeur a changé
     * @return $this
     */
    public function rebuildAppMeta()
    {
        $initial = $this->_fields[$this->metaFieldName] ?? '';
        $meta = $this->_setAppMeta($this->meta);
        if ($meta !== $initial) {
            $this->set($this->metaFieldName, $meta);
        }
        return $this;
    }

    /**
     * Setter du champ app_meta
     * @param mixed $value
     * @return string|null
     */
    protected function _setAppMeta($value)
    {
        if (is_string($value) && ($json = json_decode($value, true))) {
            $this->_setMeta($json);
            return $value;
        }
        if (!empty($value)) {
            $this->_setMeta($value);
        }
        if (is_array($value) || $value instanceof stdClass) {
            return json_encode($value, JSON_UNESCAPED_SLASHES);
        }
        return $value;
    }

    /**
     * Setter des champs virtuels app_meta
     * @param string|stdClass|array $value
     * @return stdClass
     */
    protected function _setMeta($value): stdClass
    {
        if (is_array($value)) {
            $value = (object)$value;
        } elseif (is_string($value)) {
            $value = json_decode($value);
        }
        if (!$value instanceof stdClass && !is_array($value)) {
            return (object)[];
        }
        $this->meta = new stdClass;
        foreach ($value as $k => $v) {
            if (is_numeric($k)) {
                continue;
            }
            if (in_array($k, $this->metaFields)) {
                $this->_fields[$k] = $v;
            } elseif (!isset($this->_fields[$k])) {
                $this->_fields[$k] = $v;
                $this->metaFields[] = $k;
            }
            $this->meta->$k = $v;
        }
        $this->_fields[$this->metaFieldName] = json_encode($this->meta, JSON_UNESCAPED_SLASHES);
        return $this->meta;
    }

    /**
     * Getter du champ app_meta sous forme d'objet (json)
     * @return stdClass
     */
    protected function _getMeta()
    {
        return $this->meta;
    }

    /**
     * Getter de la liste des champs virtuels du app_meta
     * @return array
     */
    public function getMetaFields()
    {
        return $this->metaFields;
    }

    /**
     * Setter generique (des champs stockés dans app_meta)
     * @param string $field
     * @param mixed  $value
     * @return mixed
     */
    protected function genericAppMetaSetter(string $field, $value)
    {
        $isDiff = !isset($this->meta->$field) || $this->meta->$field !== $value;
        $this->meta->$field = $value;
        if ($isDiff) {
            $this->rebuildAppMeta();
        }
        return $this->meta->$field;
    }

    /**
     * Setter du champ virtuel active (stocké dans app_meta)
     * @param bool $value
     * @return bool
     */
    protected function _setActive($value): bool
    {
        return $this->genericAppMetaSetter('active', (bool)$value);
    }

    /**
     * Setter du champ virtuel created_user_id (stocké dans app_meta)
     * @param bool $value
     * @return bool
     */
    protected function _setCreatedUserId($value)
    {
        return $this->genericAppMetaSetter('created_user_id', $value);
    }

    /**
     * Setter du champ virtuel default (stocké dans app_meta)
     * @param bool $value
     * @return bool
     */
    protected function _setDefault($value): bool
    {
        return $this->genericAppMetaSetter('default', $value);
    }

    /**
     * Permet d'obtenir l'id de l'utilisateur de la dernière entrée de modifieds
     * @return null
     */
    protected function _getCreatedUserId()
    {
        return $this->meta->created_user_id ?? null;
    }

    /**
     * Permet d'obtenir l'id de l'utilisateur de la dernière entrée de modifieds
     * @return null|integer
     */
    protected function _getModifiedUserId()
    {
        $modifieds = $this->meta->modifieds ?? [];
        $last = end($modifieds);
        if (is_array($last)) {
            return $last['user_id'] ?? null;
        } elseif ($last instanceof stdClass) {
            return $last->user_id ?? null;
        }
        return null;
    }
}
