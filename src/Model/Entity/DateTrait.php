<?php
/**
 * AsalaeCore\Model\Entity\DateTrait
 * @noinspection RedundantSuppression
 */

namespace AsalaeCore\Model\Entity;

use Cake\I18n\FrozenTime as Time;
use DateTime;
use DateTimeInterface;
use Exception;

/**
 * Permet une gestion des dates avec prise en compte de l'I18n
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
trait DateTrait
{
    /**
     * Permet de créer des dates à partir du format français sur champs virtuels
     * @param string|DateTimeInterface $value
     * @return Time|null
     * @throws Exception
     * @noinspection PhpUnusedPrivateMethodInspection
     */
    private function getDatetime($value)
    {
        if (is_string($value)) {
            if (preg_match('/^\d{4}-\d{2}-\d{2}([ T]\d{2}:\d{2}(:\d{2})?)?/', $value)) {
                $value = new DateTime($value);
            } else {
                $value = Time::parseDate($value);
            }
        } elseif ($value instanceof DateTimeInterface) {
            $tz = $value->getTimezone();
            $time = $value->format('Y-m-d H:i:s.u');
            $value = new Time($time, $tz);
        }

        return $value;
    }
}
