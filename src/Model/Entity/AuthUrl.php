<?php
/**
 * AsalaeCore\Model\Entity\AuthUrl
 */

namespace AsalaeCore\Model\Entity;

use Cake\Http\ServerRequest;
use AsalaeCore\ORM\Entity;
use Cake\Utility\Hash;
use Cake\Utility\Inflector;
use DateTime;
use DateTimeInterface;
use Exception;

/**
 * Entité de la table codes
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class AuthUrl extends Entity
{
    /**
     * Code constructor.
     * @param array $properties
     * @param array $options
     * @throws Exception
     */
    public function __construct(array $properties = [], array $options = [])
    {
        $properties += [
            'code' => uniqid(bin2hex(random_bytes(20))),
            'expire' => date('Y-m-d H:i:s', strtotime('+1 month'))
        ];
        parent::__construct($properties, $options);
    }

    /**
     * Permet de savoir si un AuthUrl authorise l'accès à une requète
     * @param ServerRequest $request
     * @return bool
     */
    public function allowed(ServerRequest $request)
    {
        if (empty($this->_fields['url'])
            || empty($this->_fields['expire'])
            || !$this->_fields['expire'] instanceof DateTimeInterface
            || $this->_fields['expire'] < new DateTime
        ) {
            return false;
        }
        $actual = explode('/', $request->getAttribute('here'));

        $urls = [$this->_fields['url']];
        foreach ($this->_fields['auth_sub_urls'] ?? [] as $asu) {
            $urls[] = Hash::get($asu, 'url');
        }

        foreach ($urls as $url) {
            $target = explode('/', $url);
            for ($i = 1; $i <= 2; $i++) {
                if (isset($actual[$i])) {
                    $actual[$i] = Inflector::camelize($actual[$i], '-');
                }
                if (isset($target[$i])) {
                    $target[$i] = Inflector::camelize($target[$i], '-');
                }
            }

            if ($actual === $target) {
                return true;
            }
        }
        return false;
    }
}
