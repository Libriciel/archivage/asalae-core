<?php
/**
 * AsalaeCore\Model\Entity\User
 */

namespace AsalaeCore\Model\Entity;

use Cake\Auth\DefaultPasswordHasher;
use AsalaeCore\ORM\Entity;

/**
 * Entité de la table users
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class User extends Entity
{
    /**
     * List of property names that should **not** be included in JSON or Array
     * representations of this Entity.
     *
     * @var array
     */
    protected $_hidden = ['password', 'confirm-password'];

    /**
     * Permet d'obtenir le noeud parent d'un point de vue Permissions
     * @return null|array
     */
    public function parentNode()
    {
        $roleId = $this->get('role_id');
        if (!$this->get('id') || !$roleId) {
            return null;
        }
        return ['model' => 'Roles', 'foreign_key' => $roleId];
    }

    /**
     * Encrypte le mot de passe
     *
     * @param string $password
     * @return string
     */
    protected function _setPassword(string $password): string
    {
        $hasher = new DefaultPasswordHasher;
        return $hasher->hash($password) ?: '';
    }

    /**
     * Vrai si l'utilisateur est lié à un LDAP
     * @return bool
     */
    public function _getIsLinkedToLdap(): bool
    {
        return (bool)$this->get('ldap_id');
    }
}
