<?php
/**
 * AsalaeCore\Model\Entity\CronExecution
 */

namespace AsalaeCore\Model\Entity;

use Cake\Core\Configure;
use AsalaeCore\ORM\Entity;

/**
 * Entité de la table cron_executions
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class CronExecution extends Entity
{
    /**
     * Donne des informations d'entête
     * @return array
     */
    protected function _getHeader(): array
    {
        if (empty($this->_fields['id'])) {
            return [];
        }
        $f = '%s: %s';
        $date = is_string($this->_fields['date_begin'])
            ? $this->_fields['date_begin']
            : $this->_fields['date_begin']->nice();
        return [
            sprintf($f, __("Hôte"), gethostname()),
            sprintf($f, __("Date de début du cron"), $date),
            sprintf($f, __("Êtat"), $this->_fields['state'] ?? ''),
            sprintf($f, __("Url"), Configure::read('App.fullBaseUrl')),
        ];
    }
}
