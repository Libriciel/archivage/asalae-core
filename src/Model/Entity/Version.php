<?php
/**
 * AsalaeCore\Model\Entity\Version
 */

namespace AsalaeCore\Model\Entity;

use AsalaeCore\ORM\Entity;

/**
 * Entité de la table versions
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Version extends Entity
{
}
