<?php
/**
 * AsalaeCore\Model\Entity\Sequence
 */

namespace AsalaeCore\Model\Entity;

use AsalaeCore\ORM\Entity;

/**
 * Entité de la table Sequences
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Sequence extends Entity
{
}
