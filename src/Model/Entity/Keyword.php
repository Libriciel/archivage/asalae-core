<?php
/**
 * AsalaeCore\Model\Entity\Keyword
 */

namespace AsalaeCore\Model\Entity;

use AsalaeCore\ORM\Entity;

/**
 * Entité de la table keywords
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Keyword extends Entity
{
    use AppMetaTrait;

    /**
     * @var array Champs du app_meta
     */
    protected $_metaFields = ['created_user_id', 'imported_from'];

    /**
     * Setter du champ virtuel imported_from (stocké dans app_meta)
     * @param string $value
     * @return string
     */
    protected function _setImportedFrom($value)
    {
        return $this->genericAppMetaSetter('imported_from', $value);
    }
}
