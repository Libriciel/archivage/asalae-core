<?php
/**
 * AsalaeCore\Database\Type\DateTimeFractionalType
 */
namespace AsalaeCore\Database\Type;

use Cake\Database\Type\BatchCastingInterface;
use Cake\Database\TypeInterface;

/**
 * Permet la triple compatibilité de dates : format DATE_RFC3339, format local et objet
 *
 * @category Database
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class DateTimeFractionalType extends DateTimeType implements TypeInterface, BatchCastingInterface
{
    /**
     * @inheritDoc
     */
    protected $_format = 'Y-m-d H:i:s.u';

    /**
     * @inheritDoc
     */
    protected $_marshalFormats = [
        'Y-m-d H:i',
        'Y-m-d H:i:s',
        'Y-m-d H:i:s.u',
        'Y-m-d\TH:i',
        'Y-m-d\TH:i:s',
        'Y-m-d\TH:i:sP',
        'Y-m-d\TH:i:s.u',
        'Y-m-d\TH:i:s.uP',
    ];
}
