<?php
/**
 * AsalaeCore\Database\Type\DateTimeType
 */
namespace AsalaeCore\Database\Type;

use Cake\Database\Type\DateType as CakeDateType;
use Cake\Database\Type\BatchCastingInterface;
use Cake\Database\TypeInterface;
use Cake\I18n\I18nDateTimeInterface;
use Cake\I18n\FrozenTime as Time;
use DateTimeInterface;

/**
 * Permet la triple compatibilité de dates : format DATE_RFC3339, format local et objet
 *
 * @category Database
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class DateType extends CakeDateType implements TypeInterface, BatchCastingInterface
{
    /**
     * Converts a string into a DateTime object after parsing it using the locale
     * aware parser with the specified format.
     *
     * @param string $value The value to parse and convert to an object.
     * @return DateTimeInterface|null
     */
    protected function _parseValue(string $value): ?DateTimeInterface
    {
        // Si format DATE_RFC3339
        if (preg_match('/^\d{4}-\d{2}-\d{2}/', $value)) {
            return new Time($value);
        }
        /** @var \Cake\I18n\Time $class */
        $class = $this->_className;

        return $class::parseDateTime($value, $this->_localeMarshalFormat);
    }

    /**
     * Parse la date
     * @param string $value
     * @return I18nDateTimeInterface|null
     */
    protected function _parseLocaleValue(string $value): ?I18nDateTimeInterface
    {
        $date = $this->_parseValue($value);
        return $date instanceof I18nDateTimeInterface ? $date : parent::_parseLocaleValue($value);
    }
}
