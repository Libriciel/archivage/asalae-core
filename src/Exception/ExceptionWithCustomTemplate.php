<?php

/**
 * AsalaeCore\Exception\ExceptionWithCustomTemplate
 */

namespace AsalaeCore\Exception;

/**
 * Permet de spécifier un template pour une exception
 *
 * @category Exception
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2024 Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
interface ExceptionWithCustomTemplate
{
    /**
     * Utilisera le layout custom qui si cette fonction renvoi true
     * @return bool
     */
    public function enabledCustomTemplate(): bool;

    /**
     * Donne le layout à utiliser
     * @return string|null
     */
    public function getCustomLayout(): ?string;

    /**
     * Donne le template à utiliser
     * @return string
     */
    public function getCustomTemplate(): string;
}
