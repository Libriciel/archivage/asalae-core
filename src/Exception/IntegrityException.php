<?php
/**
 * AsalaeCore\Exception\IntegrityException
 */

namespace AsalaeCore\Exception;

use Cake\Datasource\EntityInterface;
use Cake\Http\Exception\HttpException;

/**
 * Envoyé si l'integrité d'un fichier est mauvaise
 *
 * @category Form
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2022 Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class IntegrityException extends HttpException
{
    /**
     * @inheritDoc
     */
    protected $_defaultCode = 409;

    /**
     * @var string
     */
    private $originalMessage;

    /**
     * permet d'ajouter des informations utiles au message d'erreur
     * @param EntityInterface $volume
     * @param string|null     $destinationFilename
     * @return IntegrityException
     */
    public function wrap(EntityInterface $volume, string $destinationFilename = null): self
    {
        if (empty($this->originalMessage)) {
            $this->originalMessage = $this->message;
        }
        $file = $destinationFilename
            ? sprintf(' file: "%s"', h($destinationFilename))
            : '';
        $this->message = sprintf(
            'volume: "%s"(id=%d)%s - %s',
            h($volume->get('name')),
            $volume->get('id'),
            $file,
            $this->originalMessage
        );
        return $this;
    }
}
