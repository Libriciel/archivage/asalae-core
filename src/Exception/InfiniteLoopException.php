<?php
/**
 * AsalaeCore\Exception\InfiniteLoopException
 */
namespace AsalaeCore\Exception;

use Exception;

/**
 * Exception for potential infinite loop
 */
class InfiniteLoopException extends Exception
{
}
