<?php
/**
 * AsalaeCore\Middleware\MaintenanceMiddleware
 */

namespace AsalaeCore\Middleware;

use Cake\Core\Configure;
use Exception;
use http\Env\Response;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * Cross-Origin Resource Sharing
 *
 * @category Middleware
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class CorsMiddleware implements MiddlewareInterface
{
    /**
     * Process an incoming server request.
     *
     * Processes an incoming server request in order to produce a response.
     * If unable to produce the response itself, it may delegate to the provided
     * request handler to do so.
     * @param ServerRequestInterface  $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     * @throws Exception
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $response = $request->getMethod() === 'OPTIONS' ? new Response : $handler->handle($request);
        $origins = Configure::read('Cors.white_list', []);
        $requestOrigin = $request->getHeaderLine('Origin');
        if ($origins && in_array($requestOrigin, $origins)) {
            $response = $response->withHeader(
                'Access-Control-Allow-Origin',
                $requestOrigin
            )->withHeader(
                'Access-Control-Allow-Methods',
                'GET, HEAD, POST, PUT, DELETE, OPTIONS'
            )->withHeader('Access-Control-Allow-Headers', '*');
        }
        return $response;
    }
}
