<?php
/**
 * AsalaeCore\Middleware\UrlAuthMiddleware
 */

namespace AsalaeCore\Middleware;

use AsalaeCore\Http\Response;
use AsalaeCore\Utility\Notify;
use Cake\Datasource\Paging\Exception\PageOutOfBoundsException;
use Cake\Http\ServerRequest;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Throwable;
use ZMQSocketException;

/**
 * Middleware foure-tout
 *
 * @category Middleware
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2022, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class AsalaeMiddleware implements MiddlewareInterface
{
    /**
     * Méthode principale
     * @param ServerRequestInterface  $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     * @throws Throwable
     * @throws ZMQSocketException
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        try {
            return $handler->handle($request);
        } catch (PageOutOfBoundsException $e) {
            $url = $request->getUri()->getPath();
            $query = '?' . $request->getUri()->getQuery();
            if (preg_match('/(.*[?&])page=\d+(.*)/', $query, $m)) {
                $query = $m[1] . 'page=1' . $m[2];
            }
            $response = new Response(['status' => 302]);
            return $response->withLocation($url . $query);
        } catch (Throwable $e) {
            if ($request instanceof ServerRequest
                && $sessionFile = $request->getQuery('sessionFile')
            ) {
                Notify::emit(
                    'download_file',
                    [
                        'uuid' => $sessionFile,
                        'state' => 'error',
                        'message' => __("Erreur code {0} : {1}", $e->getCode(), $e->getMessage()),
                        'last_update' => time(),
                    ]
                );
            }
            throw $e;
        }
    }
}
