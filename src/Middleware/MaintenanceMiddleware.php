<?php
/**
 * AsalaeCore\Middleware\MaintenanceMiddleware
 */

namespace AsalaeCore\Middleware;

use AsalaeCore\Factory\Utility;
use AsalaeCore\Http\Response;
use AsalaeCore\Utility\Config;
use AsalaeCore\View\AppView;
use Cake\Http\ServerRequest;
use Cake\Utility\Inflector;
use DateTime;
use Exception;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Laminas\Diactoros\Stream;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * Interruption de service
 *
 * @category Middleware
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class MaintenanceMiddleware implements MiddlewareInterface
{
    /**
     * @var Response
     */
    public $response;

    /**
     * MaintenanceMiddleware constructor.
     */
    public function __construct()
    {
        $this->response = new Response;
    }

    /**
     * Process an incoming server request.
     *
     * Processes an incoming server request in order to produce a response.
     * If unable to produce the response itself, it may delegate to the provided
     * request handler to do so.
     * @param ServerRequestInterface  $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     * @throws Exception
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        if (!static::isInterrupted($request, $this->response)) {
            return $handler->handle($request);
        } else {
            $whitelist = Config::readAll('Interruption.whitelist_file');
            if (!$whitelist || !is_readable($whitelist)) {
                return $this->renderInterruption($request, $this->response);
            }
            $route = $this->parseRequest($request);
            $list = array_filter(
                explode(PHP_EOL, file_get_contents($whitelist)),
                function ($v) {
                    $v = ltrim($v);
                    return $v && $v[0] !== '#';
                }
            );
            $currentWild = "/{$route['controller']}/*";
            $currentUrl = "/{$route['controller']}/{$route['action']}";
            if (in_array($currentUrl, $list) || in_array($currentWild, $list)) {
                return $handler->handle($request);
            } else {
                return $this->renderInterruption($request, $this->response);
            }
        }
    }

    /**
     * Génère la vue de l'interruption
     * @param ServerRequestInterface|ServerRequest $request
     * @param ResponseInterface                    $response
     * @return ResponseInterface
     */
    public function renderInterruption(ServerRequestInterface $request, ResponseInterface $response)
    {
        if (strpos($request->getHeaderLine('Accept'), 'json')) {
            $path = 'Interruptions/json';
        } else {
            $path = 'Interruptions';
        }
        /** @noinspection PhpParamsInspection */
        $view = new AppView(
            $request,
            $response,
            null,
            [
                'className' => 'AsalaeCore\View\AppView',
                'templatePath' => $path,
                'template' => 'maintenance',
                'plugin' => null,
                'theme' => null,
                'layout' => $request->is('ajax') ? 'ajax' : 'Libriciel/Login.login',
                'layoutPath' => null,
            ]
        );
        if (strpos($request->getHeaderLine('Accept'), 'json')) {
            $view->disableAutoLayout();
            $response = $response->withHeader('Content-Type', 'application/json');
        }

        $stream = new Stream(fopen('php://memory', 'r+'));
        $stream->write($view->render());
        $response = $response->withBody($stream);
        return $response->withStatus(503);
    }

    /**
     * Permet de savoir si une interruption est en cours
     * @param ServerRequestInterface|null $request
     * @param ResponseInterface|null      $response
     * @return bool
     * @throws Exception
     */
    public static function isInterrupted(
        ServerRequestInterface $request = null,
        ResponseInterface &$response = null
    ): bool {
        $interrupted = false;
        $config = Config::readAll('Interruption', []);
        $config += [
            'enabled' => false,
            'scheduled' => [
                'begin' => null,
                'end' => null,
            ],
            'periodic' => [
                'begin' => null,
                'end' => null,
            ],
            'enable_whitelist' => true,
            'whitelist_headers' => [
                'X-Asalae-Webservice',
            ],
        ];
        $now = Utility::get(DateTime::class);
        if ($config['enabled']) {
            $interrupted = true;
        }
        if ($config['scheduled']['begin'] && new DateTime($config['scheduled']['begin']) < $now) {
            $interrupted = true;
        }
        if ($interrupted && $config['scheduled']['end'] && new DateTime($config['scheduled']['end']) < $now) {
            $interrupted = false;
        }
        if (!$interrupted && $config['periodic']['begin'] && $config['periodic']['end']) {
            $begin = new DateTime($config['periodic']['begin']);
            $end = new DateTime($config['periodic']['end']);
            // chevauchement des jours ex: 23h30 - 01h30 => si now est > 23h30 ou < 01h30
            if ($begin > $end && ($now > $begin || $now < $end)) {
                $interrupted = true;
                Config::write('Interruption.is_periodic', true);
            } elseif ($begin < $now && $end > $now) {
                $interrupted = true;
                Config::write('Interruption.is_periodic', true);
            }
        }
        if ($interrupted) {
            Config::write('Interruption.active', true);
            $response = $response
                ? $response->withHeader('X-Asalae-Interruption', 'active')
                : null;
        }
        if ($interrupted && $request && $config['enable_whitelist']) {
            foreach ($config['whitelist_headers'] as $header => $enabled) {
                if ($enabled && $request->hasHeader($header)) {
                    $interrupted = false;
                    break;
                }
            }
        }
        return $interrupted;
    }

    /**
     * Donne le controller/action d'une requete
     * @param ServerRequestInterface $request
     * @return array
     */
    private function parseRequest(ServerRequestInterface $request): array
    {
        $uri = $request->getUri();
        $url = explode('/', trim($uri->getPath(), '/'));
        if (($url[0] ?? null) === 'api') {
            array_shift($url);
        }
        $action = $url[1] ?? 'index';
        return [
            'controller' => Inflector::camelize($url[0] ?? 'Home', '-'),
            'action' => Inflector::variable(is_string($action) ? $action : 'index'),
        ];
    }
}
