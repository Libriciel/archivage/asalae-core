<?php
/**
 * AsalaeCore\Middleware\UrlAuthMiddleware
 * @noinspection RedundantSuppression
 */

namespace AsalaeCore\Middleware;

use AsalaeCore\Error\ContextDebugger;
use Authorization\Exception\ForbiddenException;
use AsalaeCore\Http\Response;
use AsalaeCore\View\AppView;
use Authentication\Identity;
use Cake\Http\ServerRequest;
use Cake\Routing\Router;
use Cake\Utility\Hash;
use Laminas\Diactoros\Stream;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * Gestion des erreurs lors d'un appel par url
 *
 * @category Middleware
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class UrlAuthMiddleware implements MiddlewareInterface
{
    const URL_LOGOUT = '/users/logout';

    /**
     * Middleware
     * @param ServerRequestInterface  $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface|void
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        ContextDebugger::logRequest($request);
        try {
            $response = $handler->handle($request);
        } catch (ForbiddenException $e) {
            /** @var Identity $identity */
            $identity = $request->getAttribute('identity');
            $data = $identity ? ($identity->getOriginalData() ?: []) : [];
            $user_id = Hash::get($data, 'id');
            $url_id = Hash::get($data, 'url_id');
            if (!$user_id && $url_id) {
                $url = Router::parseRequest($request);
                if (in_array($url['controller'], ['Home', 'Users', 'Admins'])) {
                    $response = $this->redirectLogout($request);
                } else {
                    $response = $this->render403($request);
                }
            } else {
                throw $e;
            }
        }
        ContextDebugger::logResponse($response);
        return $response;
    }

    /**
     * Génère la vue de l'erreur
     * @param ServerRequestInterface|ServerRequest $request
     * @return ResponseInterface
     */
    public function render403(ServerRequestInterface $request): ResponseInterface
    {
        $response = new Response(['status' => 403]);
        /** @noinspection PhpParamsInspection */
        $view = new AppView(
            $request,
            $response,
            null,
            [
                'className' => 'AsalaeCore\View\AppView',
                'templatePath' => 'Error',
                'template' => 'url_auth_403',
                'plugin' => 'AsalaeCore',
                'theme' => null,
                'layout' => 'not_connected',
                'layoutPath' => null,
            ]
        );

        $stream = new Stream(fopen('php://memory', 'r+'));
        $stream->write($view->render());
        $response = $response->withBody($stream);
        return $response->withStatus(503);
    }

    /**
     * Déconnecte l'url
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     * @noinspection PhpUnusedParameterInspection
     */
    public function redirectLogout(ServerRequestInterface $request): ResponseInterface
    {
        $response = new Response(['status' => 302]);
        return $response->withLocation(Router::url(self::URL_LOGOUT, true));
    }
}
