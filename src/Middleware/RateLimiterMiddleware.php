<?php
/**
 * AsalaeCore\Middleware\RateLimiterMiddleware
 */

namespace AsalaeCore\Middleware;

use Cake\Core\Configure;
use Cake\Http\Exception\HttpException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * Empêche une tentative d'intrusion par brute-force
 * Doit être placé après le middleware d'authentication
 * (les Identifiers garnissent le cache et définissent self::$username et self::$attempt)
 *
 * @category Middleware
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2022, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class RateLimiterMiddleware implements MiddlewareInterface
{
    /**
     * @var string donné par l'identifier (PasswordIdentifier ou LdapIndentifier)
     */
    public static $username;
    /**
     * @var int
     */
    public static int $attempts = 0;
    /**
     * @var int
     */
    public static int $consecutiveFailedAttempts = 0;
    /**
     * @var bool
     */
    public static bool $attemptLogged = false;

    /**
     * Middleware
     * @param ServerRequestInterface  $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface|void
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $enabled = Configure::read('Login.attempt_limit.enabled');
        $max = Configure::read('Login.attempt_limit.max_amount', 100);
        if ($enabled && self::$username && self::$attempts > $max) {
            throw new HttpException('429 Too Many Requests', 429);
        }
        return $handler->handle($request);
    }
}
