<?php

/**
 * AsalaeCore\Http\Session\DatabaseSession
 */

namespace AsalaeCore\Http\Session;

use Cake\Http\Session\DatabaseSession as CakeDatabaseSession;

/**
 * Surcharge de la classe de DatabaseSession
 * Evite une erreur lors du session renew à cause du token unique
 *
 * @category Http
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2024, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class DatabaseSession extends CakeDatabaseSession
{
    public const TOKEN_PATH_ROOT = 'Session';
    public const TOKEN_PATH_KEY = 'token';

    /**
     * Helper function called on write for database sessions.
     *
     * @param string $id   ID that uniquely identifies session in database.
     * @param string $data The data to be saved.
     * @return bool True for successful write, false otherwise.
     */
    public function write($id, $data): bool
    {
        // récupère la valeur du token dans Session|a:1:{s:5:"token";s:16:"9ca9b2cea6d1a23d";}
        $regex = sprintf('/%s\|.*?s:\d+:"%s";s:\d+:"([^"]+)/', self::TOKEN_PATH_ROOT, self::TOKEN_PATH_KEY);
        if (preg_match($regex, $data, $m)) {
            /** @var string $pkField */
            $pkField = $this->_table->getPrimaryKey();
            $oldEntity = $this->_table->find()->where([$pkField . ' !=' => $id, 'token' => $m[1]])->first();
            if ($oldEntity) {
                return parent::write($oldEntity->id, $data);
            }
        }
        return parent::write($id, $data);
    }
}
