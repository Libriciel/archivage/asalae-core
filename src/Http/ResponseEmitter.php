<?php
/**
 * AsalaeCore\Http\ResponseEmitter
 */

namespace AsalaeCore\Http;

use Cake\Http\Cookie\Cookie;
use Cake\Http\ResponseEmitter as CakeResponseEmitter;
use Cake\Core\Configure;
use Cake\Log\Log;
use Psr\Http\Message\ResponseInterface;

/**
 * Surcharge permettant l'envoi partiel de la réponse
 *
 * @category Http
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ResponseEmitter extends CakeResponseEmitter
{
    /**
     * @var bool partialEmit() utilisé
     */
    protected static $partiallyEmitted = false;

    /**
     * Getter du self::$partiallyEmitted
     * @return bool
     */
    public static function isPartiallyEmitted(): bool
    {
        return self::$partiallyEmitted;
    }

    /**
     * ResponseEmitter constructor.
     */
    public function __construct()
    {
        parent::__construct();
        self::$partiallyEmitted = headers_sent();
    }

    /**
     * Permets l'envoi d'une réponse partielle sans envoyer d'erreurs
     * @param ResponseInterface $response
     */
    public function partialEmit(ResponseInterface $response)
    {
        $this->emit($response, false);
        self::$partiallyEmitted = true;
        if (ob_get_level() === 1) { // evite l'envoi lors d'un test unitaire
            ob_flush();
        }
        flush();
    }

    /**
     * Emit a response.
     *
     * Emits a response, including status line, headers, and the message body,
     * according to the environment.
     *
     * @param ResponseInterface $response   The response to emit.
     * @param bool              $endRequest
     * @return bool
     */
    public function emit(ResponseInterface $response, $endRequest = true): bool
    {
        $file = $line = null;
        if (!self::$partiallyEmitted && headers_sent($file, $line)) {
            $message = "Unable to emit headers. Headers sent in file=$file line=$line";
            if (Configure::read('debug')) {
                trigger_error($message, E_USER_WARNING);
            } else {
                Log::warning($message);
            }
        }
        if (!headers_sent()) {
            $this->emitStatusLine($response);
            $this->emitHeaders($response);
        }
        $this->flush();

        $range = $this->parseContentRange($response->getHeaderLine('Content-Range'));
        if (is_array($range)) {
            $this->emitBodyRange($range, $response);
        } else {
            $this->emitBody($response);
        }

        if ($endRequest && function_exists('fastcgi_finish_request')) {
            fastcgi_finish_request();
        }

        return true;
    }

    /**
     * Emit cookies using setcookie()
     *
     * @param array $cookies An array of Set-Cookie headers.
     * @return void
     */
    protected function emitCookies(array $cookies): void
    {
        foreach ($cookies as $cookie) {
            if ($cookie instanceof Cookie) {
                if (version_compare(PHP_VERSION, '7.3.0') >= 0) {
                    setcookie($cookie->getName(), $cookie->getScalarValue(), $cookie->getOptions());
                    continue;
                } else {
                    $cookie = $cookie->toArray();
                }
            }
            if (is_array($cookie)) {
                if (version_compare(PHP_VERSION, '7.3.0') >= 0 && isset($cookie['options'])) {
                    setcookie(
                        $cookie['name'],
                        $cookie['value'],
                        $cookie['options']
                    );
                    continue;
                }
                setcookie(
                    $cookie['name'],
                    $cookie['value'],
                    $cookie['expires'],
                    $cookie['path'],
                    $cookie['domain'],
                    $cookie['secure'],
                    $cookie['httponly']
                );
                continue;
            }

            if (strpos($cookie, '";"') !== false) {
                $cookie = str_replace('";"', '{__cookie_replace__}', $cookie);
                $parts = str_replace('{__cookie_replace__}', '";"', explode(';', $cookie));
            } else {
                $parts = preg_split('/;[ \t]*/', $cookie);
            }

            [$name, $value] = explode('=', array_shift($parts), 2);
            $data = [
                'name' => urldecode($name),
                'value' => urldecode($value),
                'expires' => 0,
                'path' => '',
                'domain' => '',
                'secure' => false,
                'httponly' => false,
            ];

            foreach ($parts as $part) {
                if (strpos($part, '=') !== false) {
                    [$key, $value] = explode('=', $part);
                } else {
                    $key = $part;
                    $value = true;
                }

                $key = strtolower($key);
                $data[$key] = $value;
            }
            if (!empty($data['expires'])) {
                $data['expires'] = strtotime($data['expires']);
            }
            setcookie(
                $data['name'],
                $data['value'],
                $data['expires'],
                $data['path'],
                $data['domain'],
                $data['secure'],
                $data['httponly']
            );
        }
    }
}
