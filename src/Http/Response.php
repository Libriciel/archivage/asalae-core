<?php
/**
 * AsalaeCore\Http\Response
 */

namespace AsalaeCore\Http;

use Cake\Http\Cookie\CookieInterface;
use Cake\Http\Exception\NotFoundException;
use Cake\Http\Response as CakeResponse;
use InvalidArgumentException;
use Psr\Http\Message\MessageInterface;

/**
 * Surcharge permettant l'envoi partiel de la réponse
 *
 * @category Http
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Response extends CakeResponse
{
    /**
     * @var bool Vrai si success(true) par asalae-modal
     */
    public $success = null;

    /**
     * @var bool Vrai si partialEmit() a été utilisé sur cette response
     */
    protected $isPartiallyEmitted = false;

    /**
     * Envoi la réponse dans son état actuel sans cloturer la connexion
     */
    public function partialEmit()
    {
        $emmiter = new ResponseEmitter;
        $emmiter->partialEmit($this);
        $this->headers = [];
        $this->headerNames = [];
        $this->_createStream();
        $this->isPartiallyEmitted = true;
        return $this;
    }

    /**
     * Empeche de modifier les headers d'une réponse envoyée partiellement, les
     * entêtes ont déja été envoyés...
     * @param string          $name  Case-insensitive header field name.
     * @param string|string[] $value Header value(s).
     * @return static
     * @throws InvalidArgumentException for invalid header names or values.
     */
    public function withHeader($name, $value): MessageInterface
    {
        if ($this->isPartiallyEmitted) {
            return $this;
        }
        return parent::withHeader($name, $value);
    }

    /**
     * Convert the cookie into an array of its properties.
     *
     * This method is compatible with the historical behavior of Cake\Http\Response,
     * where `httponly` is `httponly` and `expires` is `expire`
     *
     * @param \Cake\Http\Cookie\CookieInterface $cookie Cookie object.
     * @return array
     */
    protected function convertCookieToArray(CookieInterface $cookie)
    {
        $output = [
            'name' => $cookie->getName(),
            'value' => $cookie->getScalarValue(),
            'path' => $cookie->getPath(),
            'domain' => $cookie->getDomain(),
            'secure' => $cookie->isSecure(),
            'httponly' => $cookie->isHttpOnly(),
            'expire' => $cookie->getExpiresTimestamp(),
        ];
        $output['options'] = [
            'path' => $output['path'],
            'domain' => $output['domain'],
            'secure' => $output['secure'],
            'httponly' => $output['httponly'],
            'expires' => $output['expire'],
            'SameSite' => 'Strict',
        ];
        return $output;
    }

    /**
     * WithFile en CallbackStream
     * @param string $filepath
     * @param array  $options
     * @return Response|MessageInterface
     */
    public function withFileStream(string $filepath, array $options = [])
    {
        if (!is_file($filepath)) {
            throw new NotFoundException('File not found');
        }
        $name = $options['name'] ?? basename($filepath);
        $filesize = $options['filesize'] ?? filesize($filepath);
        $mime = $options['mime'] ?? mime_content_type($filepath);
        $delete = $options['delete'] ?? false;

        return $this
            ->withHeader(
                'Content-Disposition',
                'attachment; filename="' . $name . '"'
            )
            ->withHeader('Content-Type', $mime)
            ->withHeader('Content-Length', $filesize)
            ->withHeader('File-Size', $filesize)
            ->withBody(
                new CallbackStream(
                    function () use ($filepath, $delete) {
                        ob_end_flush();
                        ob_implicit_flush();
                        set_time_limit(0);
                        readfile($filepath);
                        if ($delete) {
                            unlink($filepath);
                        }
                    }
                )
            );
    }
}
