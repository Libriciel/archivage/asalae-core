<?php
/**
 * AsalaeCore\Http\Client\Request
 */

namespace AsalaeCore\Http\Client;

use Cake\Http\Client\FormData;
use Cake\Http\Client\Request as CakeRequest;
use Laminas\Diactoros\RequestTrait;
use Laminas\Diactoros\Stream;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\StreamInterface;

/**
 * Copie modifiée de la classe de Request
 * ajout de :
 * if ($data instanceof StreamInterface) {
 *   $this->stream = $data;
 * }
 */
class Request extends CakeRequest implements RequestInterface
{
    use RequestTrait;

    /**
     * Constructor
     *
     * Provides backwards compatible defaults for some properties.
     *
     * @param string            $url     The request URL
     * @param string            $method  The HTTP method to use.
     * @param array             $headers The HTTP headers to set.
     * @param array|string|null $data    The request body to use.
     * @noinspection PhpMissingParentConstructorInspection override
     */
    public function __construct(string $url = '', string $method = self::METHOD_GET, array $headers = [], $data = null)
    {
        $this->setMethod($method);
        $this->uri = $this->createUri($url);
        $headers += [
            'Connection' => 'close',
            'User-Agent' => 'CakePHP',
        ];
        $this->addHeaders($headers);

        if ($data instanceof StreamInterface) {
            $this->stream = $data;
        } elseif ($data === null) {
            $this->stream = new Stream('php://memory', 'rw');
        } else {
            $this->setContent($data);
        }
    }

    /**
     * Add an array of headers to the request.
     *
     * @param array $headers The headers to add.
     * @return void
     */
    protected function addHeaders(array $headers): void
    {
        foreach ($headers as $key => $val) {
            $normalized = strtolower($key);
            $this->headers[$key] = (array)$val;
            $this->headerNames[$normalized] = $key;
        }
    }

    /**
     * Set the body/payload for the message.
     *
     * Array data will be serialized with Cake\Http\FormData,
     * and the content-type will be set.
     *
     * @param string|array $content The body for the request.
     * @return Request
     */
    protected function setContent($content)
    {
        if (is_array($content)) {
            $formData = new FormData();
            $formData->addMany($content);
            $this->addHeaders(['Content-Type' => $formData->contentType()]);
            $content = (string)$formData;
        }

        $stream = new Stream('php://memory', 'rw');
        $stream->write($content);
        $this->stream = $stream;

        return $this;
    }
}
