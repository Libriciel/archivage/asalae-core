<?php
/**
 * AsalaeCore\Http\Client\Request
 */

namespace AsalaeCore\Http\Client;

use Cake\Http\Client\Response;
use Psr\Http\Message\MessageInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;
use Laminas\Diactoros\Exception\InvalidArgumentException;

/**
 * A utiliser avec Adapter\StreamLargeFilesCurl
 * La réponse comprend un handle sur un fichier qui contient également les entêtes
 * Cette classe permet d'éviter de retourner la partie entêtes
 */
class LargeFileResponse extends Response implements ResponseInterface
{
    /**
     * @var int
     */
    private $pointerLocation;

    /**
     * @var StreamInterface
     */
    private $stream;

    /**
     * Donne une instance pour fichier large (stocké sur disque)
     * @param array           $headers
     * @param StreamInterface $body
     * @return LargeFileResponse
     */
    public static function createWithStreamBody(array $headers, StreamInterface $body): LargeFileResponse
    {
        $instance = new LargeFileResponse($headers);
        $instance->stream = $body;
        $instance->pointerLocation = $body->tell();
        return $instance;
    }

    /**
     * Constructor
     *
     * @param array  $headers Unparsed headers.
     * @param string $body    The response body.
     */
    public function __construct(array $headers = [], string $body = '')
    {
        parent::__construct($headers, $body);
        $this->stream = '';
    }

    /**
     * Gets the body of the message.
     *
     * @return StreamInterface Returns the body as a stream.
     */
    public function getBody() : StreamInterface
    {
        return $this->stream;
    }

    /**
     * Return an instance with the specified message body.
     *
     * The body MUST be a StreamInterface object.
     *
     * This method MUST be implemented in such a way as to retain the
     * immutability of the message, and MUST return a new instance that has the
     * new body stream.
     *
     * @param StreamInterface $body Body.
     * @return static
     * @throws InvalidArgumentException When the body is not valid.
     */
    public function withBody(StreamInterface $body) : MessageInterface
    {
        $new = clone $this;
        $new->stream = $body;
        return $new;
    }


    /**
     * Provides magic __get() support.
     *
     * @return string
     */
    protected function _getBody(): string
    {
        $this->stream->rewind();
        $this->stream->seek($this->pointerLocation);
        return $this->stream->getContents();
    }
}
