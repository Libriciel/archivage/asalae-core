<?php
/**
 * AsalaeCore\Http\Client\Adapter\CustomCurl
 */

namespace AsalaeCore\Http\Client\Adapter;

use AsalaeCore\Http\Client\LargeFileResponse;
use Cake\Core\Configure;
use Cake\Http\Client\Adapter\Curl;
use Cake\Http\Client\AdapterInterface;
use Cake\Http\Client\Exception\ClientException;
use Cake\Http\Client\Exception\NetworkException;
use Cake\Http\Client\Exception\RequestException;
use Cake\Http\Client\Response;
use Exception;
use GuzzleHttp\Psr7\Stream;
use Psr\Http\Message\RequestInterface;
use const DS;

/**
 * Surcharge de la classe de Curl
 * la réponse est écrite dans un fichier pour éviter de remplir la ram
 */
class StreamLargeFilesCurl extends Curl implements AdapterInterface
{
    /**
     * @var string filename path
     */
    private $tmpFile;

    /**
     * Send a request and get a response back.
     *
     * @param RequestInterface     $request The request object to send.
     * @param array<string, mixed> $options Array of options for the stream.
     * @return array<Response> Array of populated Response objects
     * @throws Exception
     */
    public function send(RequestInterface $request, array $options): array
    {
        if (!extension_loaded('curl')) {
            throw new ClientException('curl extension is not loaded.');
        }

        $ch = curl_init();
        $options = $this->buildOptions($request, $options);

        $dirname = Configure::read('App.paths.data').DS.'tmp';
        if (!is_dir($dirname)) {
            mkdir($dirname, 0777, true);
        }
        $this->tmpFile = $dirname . DS . uniqid('dl-');
        $tmpfile = fopen($this->tmpFile, 'w+');
        $options[CURLOPT_FILE] = $tmpfile;
        try {
            curl_setopt_array($ch, $options);

            /** @var string|false $body */
            $body = $this->exec($ch);
            if ($body === false) {
                $errorCode = curl_errno($ch);
                $error = curl_error($ch);
                curl_close($ch);

                $message = "cURL Error ($errorCode) $error";
                $errorNumbers = [
                    CURLE_FAILED_INIT,
                    CURLE_URL_MALFORMAT,
                    CURLE_URL_MALFORMAT_USER,
                ];
                if (in_array($errorCode, $errorNumbers, true)) {
                    throw new RequestException($message, $request);
                }
                throw new NetworkException($message, $request);
            }

            $responses = $this->createResponse($ch, '');
            curl_close($ch);
            fclose($tmpfile);

            return $responses;
        } catch (Exception $e) {
            fclose($tmpfile);
            if (is_file($this->tmpFile)) {
                unlink($this->tmpFile);
            }
            throw $e;
        }
    }

    /**
     * Convert the raw curl response into an Http\Client\Response
     *
     * @param resource $handle       Curl handle
     * @param string   $responseData string The response data from curl_exec
     * @return array<Response>
     * @psalm-suppress UndefinedDocblockClass
     */
    protected function createResponse($handle, $responseData): array
    {
        /** @psalm-suppress PossiblyInvalidArgument */
        $headerSize = curl_getinfo($handle, CURLINFO_HEADER_SIZE);
        $fh = fopen($this->tmpFile, 'r');
        $headers = trim(fread($fh, $headerSize));
        $response = LargeFileResponse::createWithStreamBody(
            explode("\r\n", $headers),
            new Stream($fh)
        );

        return [$response];
    }

    /**
     * Destructeur pour nettoyer les fichiers temporaires
     */
    public function __destruct()
    {
        if (is_file($this->tmpFile)) {
            unlink($this->tmpFile);
        }
    }
}
