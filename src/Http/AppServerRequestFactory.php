<?php
/**
 * AsalaeCore\Http\AppServerRequestFactory
 * @noinspection PhpInternalEntityUsedInspection - CakeUri
 */
namespace AsalaeCore\Http;

use Cake\Core\Configure;
use Cake\Database\Connection;
use Cake\Database\Exception\MissingConnectionException;
use Cake\Datasource\ConnectionManager;
use Cake\Http\ServerRequest;
use Cake\Http\ServerRequestFactory;
use Cake\Http\Session;
use Cake\Http\Uri as CakeUri;
use InvalidArgumentException;
use PDOException;
use function Laminas\Diactoros\normalizeServer;

/**
 * Permet d'utiliser AppServerRequest plutôt que le ServerRequest de cake
 *
 * @category Form
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
abstract class AppServerRequestFactory extends ServerRequestFactory
{
    /**
     * Function to use to get apache request headers; present only to simplify mocking.
     *
     * @var callable
     */
    private static $apacheRequestHeaders = 'apache_request_headers';

    /**
     * {@inheritDoc}
     *
     * @param array $server     $_SERVER superglobal
     * @param array $query      $_GET superglobal
     * @param array $parsedBody $_POST superglobal
     * @param array $cookies    $_COOKIE superglobal
     * @param array $files      $_FILES superglobal
     * @return ServerRequest
     * @throws InvalidArgumentException for invalid file values
     */
    public static function fromGlobals(
        ?array $server = null,
        ?array $query = null,
        ?array $parsedBody = null,
        ?array $cookies = null,
        ?array $files = null
    ): ServerRequest {
        $server = normalizeServer(
            $server ?: $_SERVER,
            is_callable(self::$apacheRequestHeaders) ? self::$apacheRequestHeaders : null
        );
        /** @var CakeUri $uri */
        $uri = static::createUri($server);
        $sessionConfig = (array)Configure::read('Session') + [
            'defaults' => 'php',
            'cookiePath' => $uri->getWebroot()
        ];
        if (($sessionConfig['defaults'] === 'database' && !self::isConnected())
            || strtolower($uri->getPath()) === '/install'
        ) {
            $sessionConfig['defaults'] = 'php';
            unset($sessionConfig['handler']);
        }
        $url = $uri->getUri()->getPath();
        if (strpos($url, '/no-session-renew/') === 0) {
            $session = ConnSession::create($sessionConfig);
        } else {
            $session = Session::create($sessionConfig);
        }

        $request = AppServerRequest::getInstance(
            [
                'environment' => $server,
                'uri' => $uri,
                'cookies' => $cookies ?: $_COOKIE,
                'query' => $query ?: $_GET,
                'webroot' => $uri->getWebroot(),
                'base' => $uri->getBase(),
                'session' => $session,
                'input' => $server['CAKEPHP_INPUT'] ?? null,
            ]
        );

        $request = static::marshalBodyAndRequestMethod($parsedBody ?? $_POST, $request);
        return static::marshalFiles($files ?? $_FILES, $request);
    }

    /**
     * Vérifi que la base de donnée est accessible
     * @return bool
     */
    private static function isConnected()
    {
        try {
            $conn = ConnectionManager::get('default');
            if ($conn instanceof Connection) {
                $conn->query('select 1');
            }
            return true;
        } catch (PDOException|MissingConnectionException $e) {
            return false;
        }
    }

    /**
     * Sets the REQUEST_METHOD environment variable based on the simulated _method
     * HTTP override value. The 'ORIGINAL_REQUEST_METHOD' is also preserved, if you
     * want the read the non-simulated HTTP method the client used.
     *
     * Request body of content type "application/x-www-form-urlencoded" is parsed
     * into array for PUT/PATCH/DELETE requests.
     *
     * @param array         $parsedBody Parsed body.
     * @param ServerRequest $request    Request instance.
     * @return ServerRequest
     */
    protected static function marshalBodyAndRequestMethod(array $parsedBody, ServerRequest $request): ServerRequest
    {
        $method = $request->getMethod();
        if (in_array($method, ['PUT', 'DELETE', 'PATCH'], true)
            && strpos((string)$request->contentType(), 'multipart/form-data') === 0
            && $request instanceof AppServerRequest
        ) {
            $parsedBody = $request->parseMultipartFormData();
            return $request->withParsedBody($parsedBody);
        }
        return parent::marshalBodyAndRequestMethod($parsedBody, $request);
    }
}
