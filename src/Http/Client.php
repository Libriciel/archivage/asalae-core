<?php
/**
 * AsalaeCore\Http\Client
 */

namespace AsalaeCore\Http;

use AsalaeCore\Http\Client\Request;
use Cake\Http\Client as CakeClient;
use Cake\Http\Client\Request as CakeRequest;

/**
 * Surcharge de la classe de Client
 */
class Client extends CakeClient
{
    /**
     * Creates a new request object based on the parameters.
     *
     * @param string $method  HTTP method name.
     * @param string $url     The url including query string.
     * @param mixed  $data    The request body.
     * @param array  $options The options to use. Contains auth, proxy, etc.
     * @return CakeRequest
     */
    protected function _createRequest(string $method, string $url, $data, $options): CakeRequest
    {
        $headers = (array)($options['headers'] ?? []);
        if (isset($options['type'])) {
            $headers = array_merge($headers, $this->_typeHeaders($options['type']));
        }
        if (is_string($data) && !isset($headers['Content-Type']) && !isset($headers['content-type'])) {
            $headers['Content-Type'] = 'application/x-www-form-urlencoded';
        }

        $request = new Request($url, $method, $headers, $data);
        /** @var Request $request */
        $request = $request->withProtocolVersion($this->getConfig('protocolVersion'));
        $cookies = $options['cookies'] ?? [];
        /** @var Request $request */
        $request = $this->_cookies->addToRequest($request, $cookies);
        if (isset($options['auth'])) {
            $request = $this->_addAuthentication($request, $options);
        }
        if (isset($options['proxy'])) {
            $request = $this->_addProxy($request, $options);
        }

        return $request;
    }
}
