<?php
/**
 * AsalaeCore\Http\AppServerRequest
 */
namespace AsalaeCore\Http;

use ArrayAccess;
use Cake\Core\Configure;
use Cake\Http\ServerRequest;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Surcharge de ServerRequest afin de permettre une utilisation des formulaires
 * sous les verbes != POST (ex: PUT, PATCH)
 *
 * @category Http
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class AppServerRequest extends ServerRequest implements ArrayAccess, ServerRequestInterface
{
    /**
     * @var AppServerRequest instance singleton
     */
    private static $instance;

    /**
     * Create a new request object.
     *
     * You can supply the data as either an array or as a string. If you use
     * a string you can only supply the URL for the request. Using an array will
     * let you provide the following keys:
     *
     * - `post` POST data or non query string data
     * - `query` Additional data from the query string.
     * - `files` Uploaded files in a normalized structure, with each leaf an instance of UploadedFileInterface.
     * - `cookies` Cookies for this request.
     * - `environment` $_SERVER and $_ENV data.
     * - `url` The URL without the base path for the request.
     * - `uri` The PSR7 UriInterface object. If null, one will be created from `url` or `environment`.
     * - `base` The base URL for the request.
     * - `webroot` The webroot directory for the request.
     * - `input` The data that would come from php://input this is useful for simulating
     *   requests with put, patch or delete data.
     * - `session` An instance of a Session object
     *
     * @param array<string, mixed> $config An array of request data to create a request with.
     */
    public function __construct(array $config = [])
    {
        parent::__construct($config);
        if ($this->uri->getScheme() === 'http'
            && ($fullBaseUrl = Configure::read('App.fullBaseUrl'))
            && substr($fullBaseUrl, 0, 8) === 'https://'
        ) {
            $this->uri = $this->uri->withScheme('https');
        }
    }

    /**
     * Permet de récupérer l'instance unique de la classe
     * @param array $config
     * @return AppServerRequest
     */
    public static function getInstance(array $config = [])
    {
        return self::$instance = self::$instance ?? new self($config);
    }

    /**
     * Nettoyage des fichiers temporaires
     */
    public function __destruct()
    {
        foreach (array_keys($this->files) as $path) {
            if (is_file($path)) {
                unlink($path);
            }
        }
    }

    /**
     * @var array fichiers à supprimer
     */
    private $files = [];

    /**
     * PHP ne traite que les méthodes POST pour remplir $_POST et $_FILES
     * La surcharge de cette méthode permet d'utiliser par exemple PUT pour
     * envoyer un formulaire.
     * Le formulaire est parsé manuellement à partir d'un multipart/form-data
     * @param array $data Array of post data.
     * @return array
     * @deprecated ne semble plus être utilisé, remplacé par marshalBodyAndRequestMethod()
     */
    protected function _processPost($data)
    {
        $method = $this->getEnv('REQUEST_METHOD');
        if (in_array($method, ['PUT', 'DELETE', 'PATCH'])
            && strpos($this->contentType(), 'multipart/form-data') === 0
        ) {
            return $this->parseMultipartFormData();
        }

        $method = $this->getEnv('REQUEST_METHOD');
        $override = false;

        if (in_array($method, ['PUT', 'DELETE', 'PATCH'], true)
            && strpos($this->contentType(), 'application/x-www-form-urlencoded') === 0
        ) {
            $this->stream->rewind();
            $data = $this->stream->getContents();
            parse_str($data, $data);
        }
        if ($this->hasHeader('X-Http-Method-Override')) {
            $data['_method'] = $this->getHeaderLine('X-Http-Method-Override');
            $override = true;
        }
        $this->_environment['ORIGINAL_REQUEST_METHOD'] = $method;
        if (isset($data['_method'])) {
            $this->_environment['REQUEST_METHOD'] = $data['_method'];
            unset($data['_method']);
            $override = true;
        }

        if ($override && !in_array($this->_environment['REQUEST_METHOD'], ['PUT', 'POST', 'DELETE', 'PATCH'], true)) {
            $data = [];
        }

        return $data;
    }

    /**
     * Parse un multipart/form-data
     * @return array
     */
    public function parseMultipartFormData()
    {
        if (!preg_match('/boundary=(.*)$/', $this->contentType(), $matches)) {
            return [];
        }
        $boundary = $matches[1];
        $this->stream->rewind();
        $input = $this->stream->getContents();
        $rawDatas = preg_split('/-*'.$boundary.'/', $input);
        $data = [];
        foreach ($rawDatas as $raw) {
            $nl = strpos($raw, "\n\r");
            if ($nl) {
                $header = trim(substr($raw, 0, $nl));
                $value = substr($raw, $nl + strlen("\n\r") +1);
                $value = substr($value, 0, strlen($value) - strlen("\n\r"));
                $this->rawDataIntoCake($header, $value, $data);
            }
        }
        return $data;
    }

    /**
     * Transforme les données brutes en habituel request->data
     * @param string $header
     * @param string $value
     * @param array  $data
     */
    private function rawDataIntoCake(string $header, string $value, array &$data)
    {
        $headers = [];
        foreach (explode(';', preg_replace('/\n\s*/', '; ', $header)) as $header) {
            [$k, $v] = preg_split('/[:=]/', trim($header));
            $headers[$k] = trim($v, " \t\n\r\0\x0B\"");
        }

        if (isset($headers['filename'])) {
            $file = [
                'name' => $headers['filename'],
                'size' => strlen($value),
                'type' => $headers['Content-Type'] ?? 'application/octet-stream'
            ];
            $tmpDir = sys_get_temp_dir();
            if ($file['size'] > $this->uploadMaxFilesize()) {
                $file['error'] = UPLOAD_ERR_INI_SIZE;
            } elseif (empty($file['size'])) {
                $file['error'] = UPLOAD_ERR_NO_FILE;
            } elseif (empty($tmpDir)) {
                $file['error'] = UPLOAD_ERR_NO_TMP_DIR;
            } elseif (!is_writable($tmpDir)) {
                $file['error'] = UPLOAD_ERR_CANT_WRITE;
            } else {
                $file['error'] = UPLOAD_ERR_OK;
                $tmp = tmpfile();
                $file['tmp_name'] = stream_get_meta_data($tmp)['uri'];
                fwrite($tmp, $value);
                $this->files[$file['tmp_name']] = $tmp;
            }
            $value = $file;
        }
        $data[$headers['name']] = $value;
    }

    /**
     * @var int mémorise la valeur upload_max_filesize configurée en octet
     */
    private $uploadMaxFilesize = 0;

    /**
     * Converti le upload_max_filesize en octet (ex: 100K = 102400)
     * @link http://www.php.net/manual/en/function.ini-get.php
     * @return int|string
     */
    private function uploadMaxFilesize()
    {
        if (!$this->uploadMaxFilesize) {
            $val = trim(ini_get('upload_max_filesize'));
            $last = strtolower($val[strlen($val)-1]);
            switch ($last) {/** @noinspection PhpMissingBreakStatementInspection */
                case 'g':
                    $val *= 1024;
                    // fall-through
                /** @noinspection PhpMissingBreakStatementInspection */
                // no break
                case 'm':
                    $val *= 1024;
                    // fall-through
                    // no break
                case 'k':
                    $val *= 1024;
            }
            $this->uploadMaxFilesize = $val;
        }
        return $this->uploadMaxFilesize;
    }

    /**
     * Whether a offset exists
     * @link https://php.net/manual/en/arrayaccess.offsetexists.php
     * @param mixed $offset <p>
     *                      An offset to check for.
     *                      </p>
     * @return bool true on success or false on failure.
     * </p>
     * <p>
     * The return value will be casted to boolean if non-boolean was returned.
     */
    public function offsetExists($offset)
    {
        return isset($this[$offset]);
    }

    /**
     * Offset to retrieve
     * @link https://php.net/manual/en/arrayaccess.offsetget.php
     * @param mixed $offset <p>
     *                      The offset to retrieve.
     *                      </p>
     * @return mixed Can return all value types.
     */
    public function offsetGet($offset)
    {
        return $this[$offset];
    }

    /**
     * Offset to set
     * @link https://php.net/manual/en/arrayaccess.offsetset.php
     * @param mixed $offset <p>
     *                      The offset to assign the value to.
     *                      </p>
     * @param mixed $value  <p>
     *                      The
     *                      value
     *                      to
     *                      set.
     *                      </p>
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        $this[$offset] = $value;
    }

    /**
     * Offset to unset
     * @link https://php.net/manual/en/arrayaccess.offsetunset.php
     * @param mixed $offset <p>
     *                      The offset to unset.
     *                      </p>
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this[$offset]);
    }
}
