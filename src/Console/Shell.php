<?php /** @noinspection PhpDeprecationInspection */
/**
 * AsalaeCore\Console\Shell
 */

namespace AsalaeCore\Console;

use Cake\Console\Shell as CakeShell;
use Cake\ORM\Locator\LocatorInterface;

/**
 * Shell d'asalae
 *
 * @category    Shell
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2018, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 * @deprecated since cakephp 4.0.0
 */
class Shell extends CakeShell
{
    /**
     * @var array $argv
     */
    public $originalArgv = [];

    /**
     * Monte le ConsoleIo d'asalae
     *
     * @param \Cake\Console\ConsoleIo|null            $io      An io instance.
     * @param \Cake\ORM\Locator\LocatorInterface|null $locator Table locator instance.
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#Shell
     */
    public function __construct(\Cake\Console\ConsoleIo $io = null, LocatorInterface $locator = null)
    {
        parent::__construct($io ?: new ConsoleIo(), $locator);
    }

    /**
     * Garde en mémoire les paramètres originaux. Permet nottament d'avoir un
     * argument de la même valeur qu'une méthode publique du shell
     *
     * Permet également d'inverser la sous-commande avec l'identifiant cible
     * pour un appel plus naturel ex: bin/cake volume_manager 1 ls LIBSCOP/foo/
     *
     * {@inheritDoc}
     *
     * @param array $argv
     * @param bool  $autoMethod
     * @param array $extra
     * @return bool|int|null
     */
    public function runCommand($argv, $autoMethod = false, $extra = [])
    {
        $this->originalArgv = $argv;
        if (count($argv) >= 2 && is_numeric($argv[0]) && method_exists($this, $argv[1])) {
            $id = array_shift($argv);
            $method = array_shift($argv);
            array_unshift($argv, $method, $id);
        }
        return parent::runCommand($argv, $autoMethod, $extra);
    }
}
