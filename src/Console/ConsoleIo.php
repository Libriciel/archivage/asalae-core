<?php
/**
 * AsalaeCore\Console\ConsoleIo
 */

namespace AsalaeCore\Console;

use Cake\Console\ConsoleInput;
use Cake\Console\ConsoleIo as CakeConsoleIo;
use Cake\Console\ConsoleOutput as CakeConsoleOutput;
use Cake\Console\HelperRegistry;

/**
 * Surcharge du ConsoleIo pour éviter les erreurs d'écriture en cas d'erreur
 *
 * @category Console
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ConsoleIo extends CakeConsoleIo
{

    /**
     * Constructor
     *
     * @param \Cake\Console\ConsoleOutput|null  $out     A ConsoleOutput object for stdout.
     * @param \Cake\Console\ConsoleOutput|null  $err     A ConsoleOutput object for stderr.
     * @param \Cake\Console\ConsoleInput|null   $in      A ConsoleInput object for stdin.
     * @param \Cake\Console\HelperRegistry|null $helpers A HelperRegistry instance
     */
    public function __construct(
        CakeConsoleOutput $out = null,
        CakeConsoleOutput $err = null,
        ConsoleInput $in = null,
        HelperRegistry $helpers = null
    ) {
        $out = $out ?: new ConsoleOutput('php://stdout');
        $err = $err ?: new ConsoleOutput('php://stderr');
        $in = $in ?: new ConsoleInput('php://stdin');
        $helpers = $helpers ?: new HelperRegistry();
        parent::__construct($out, $err, $in, $helpers);
        $this->_helpers->setIo($this);
    }

    /**
     * Getter du stdout
     * @return CakeConsoleOutput
     */
    public function getOut(): CakeConsoleOutput
    {
        return $this->_out;
    }

    /**
     * Getter du stderr
     * @return CakeConsoleOutput
     */
    public function getErr(): CakeConsoleOutput
    {
        return $this->_err;
    }

    /**
     * Getter du stdin
     * @return ConsoleInput
     */
    public function getIn(): ConsoleInput
    {
        return $this->_in;
    }
}
