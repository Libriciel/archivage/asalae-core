<?php
/**
 * AsalaeCore\Console\ConsoleTrait
 */

namespace AsalaeCore\Console;

use Cake\Console\ConsoleOutput as CakeConsoleOutput;
use Cake\TestSuite\Stub\ConsoleOutput as TestConsoleOutput;

/**
 * Permet d'utiliser à partir d'un script non shell, les commandes de sorti out() et err()
 *
 * @category Console
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
trait ConsoleTrait
{
    /**
     * @var ConsoleOutput stderr
     */
    protected $err;

    /**
     * @var ConsoleOutput stdout
     */
    protected $out;

    /**
     * @var string sorti stdout + stderr
     */
    private $output = '';

    /**
     * Converti les couleurs console (ex: <success>) en span (ex: <span class="console success">)
     * @see ConsoleOutput::$_styles
     * @param string $raw
     * @param int    $newlines
     * @return string
     */
    private function consoleToHtml(string $raw, int $newlines): string
    {
        $colors = [
            'emergency',
            'alert',
            'critical',
            'error',
            'warning',
            'info',
            'debug',
            'success',
            'comment',
            'question',
            'notice'
        ];
        $search = [];
        $replace = [];
        foreach ($colors as $color) {
            $search[] = '<'.$color.'>';
            $search[] = '</'.$color.'>';
            $replace[] = '<span class="console '.$color.'">';
            $replace[] = '</span>';
        }
        return str_replace($search, $replace, $raw)
            . str_repeat(CakeConsoleOutput::LF, $newlines);
    }

    /**
     * Outputs a single or multiple messages to stdout. If no parameters
     * are passed outputs just a newline.
     * @param string|array|null $message  A string or an array of strings to output
     * @param int               $newlines Number of newlines to append
     * @return int|bool The number of bytes returned from writing to stdout.
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#Shell::out
     */
    public function out($message = null, int $newlines = 1)
    {
        if (empty($this->out)) {
            $this->out = new ConsoleOutput('php://stdout');
        }
        $this->output .= $this->consoleToHtml($message, $newlines);
        return $this->out->write($message, $newlines);
    }

    /**
     * Outputs a single or multiple error messages to stderr. If no parameters
     * are passed outputs just a newline.
     * @param string|array|null $message  A string or an array of strings to output
     * @param int               $newlines Number of newlines to append
     * @return int|bool The number of bytes returned from writing to stderr.
     */
    public function err($message = null, int $newlines = 1)
    {
        if (empty($this->err)) {
            $this->err = new ConsoleOutput('php://stderr');
        }
        $this->output .= $this->consoleToHtml($message, $newlines);
        return $this->err->write($message, $newlines);
    }

    /**
     * Outputs a series of minus characters to the standard output, acts as a visual separator.
     *
     * @param int $newlines Number of newlines to pre- and append
     * @param int $width    Width of the line, defaults to 63
     * @return void
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#Shell::hr
     */
    public function hr(int $newlines = 0, int $width = 63)
    {
        $this->out(null, $newlines);
        $this->out(str_repeat('-', $width));
        $this->out(null, $newlines);
    }

    /**
     * Permet d'obtenir les messages de sorti
     * @return string
     */
    public function getOutput(): string
    {
        return $this->output;
    }

    /**
     * Initialise les ConsoleOutput
     * @param ConsoleOutput|TestConsoleOutput|null $out
     * @param ConsoleOutput|TestConsoleOutput|null $err
     */
    private function initializeOutput(
        $out = null,
        $err = null
    ) {
        $this->out = $out ?: new ConsoleOutput('php://stdout');
        $this->err = $err ?: new ConsoleOutput('php://stderr');
        $this->out->setOutputAs(CakeConsoleOutput::COLOR);
        $this->err->setOutputAs(CakeConsoleOutput::COLOR);
    }
}
