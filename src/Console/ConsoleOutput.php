<?php
/**
 * AsalaeCore\Console\ConsoleOutput
 */

namespace AsalaeCore\Console;

use Cake\Console\ConsoleOutput as CakeConsoleOutput;

/**
 * Surcharge du ConsoleOutput pour éviter les erreurs d'écriture en cas d'erreur
 *
 * @category Console
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ConsoleOutput extends CakeConsoleOutput
{
    /**
     * Writes a message to the output stream.
     *
     * @param string $message Message to write.
     * @return int|bool The number of bytes returned from writing to output.
     */
    protected function _write($message): int
    {
        return (int)@fwrite($this->_output, $message);
    }
}
