<?php
/**
 * AsalaeCore\Command\AppConsoleOptionParser
 */

namespace AsalaeCore\Console;

use Cake\Console\ConsoleInputOption;
use Cake\Console\ConsoleOptionParser;

/**
 * Conserve sous forme d'array les options configurés
 *
 * @category    Shell
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2017, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class AppConsoleOptionParser extends ConsoleOptionParser
{
    /**
     * @var array options du ConsoleOptionParser
     */
    public $options = [];

    /**
     * Surcharge de addOption permettant de stocker dans $this->options les
     * options définis.
     * @param ConsoleInputOption|string $name
     * @param array                     $options
     * @return ConsoleOptionParser
     */
    public function addOption($name, array $options = [])
    {
        $this->options[$name] = $options;
        return parent::addOption($name, $options);
    }
}
