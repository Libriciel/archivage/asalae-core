<?php
/**
 * AsalaeCore\Console\ConsoleLogTrait
 */

namespace AsalaeCore\Console;

use Cake\Core\Configure;
use Cake\TestSuite\Stub\ConsoleOutput as TestConsoleOutput;
use Cake\Utility\Inflector;

/**
 * Envoi dans un fichier log en plus du stdout/stderr
 *
 * @category Console
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
trait ConsoleLogTrait
{

    /**
     * Traits
     */
    use ConsoleTrait {
        initializeOutput as basicInitializeOutput;
        out as stdout;
        err as stderr;
    }

    /**
     * @var ConsoleOutput sortie log
     */
    public $logfile;

    /**
     * Outputs a single or multiple messages to stdout. If no parameters
     * are passed outputs just a newline.
     * @param string|array|null $message  A string or an array of strings to output
     * @param int               $newlines Number of newlines to append
     * @return int|bool The number of bytes returned from writing to stdout.
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#Shell::out
     */
    public function out($message = null, int $newlines = 1)
    {
        $this->logfile->write($message, $newlines);
        return $this->stdout($message, $newlines);
    }

    /**
     * Outputs a single or multiple error messages to stderr. If no parameters
     * are passed outputs just a newline.
     * @param string|array|null $message  A string or an array of strings to output
     * @param int               $newlines Number of newlines to append
     * @return int|bool The number of bytes returned from writing to stderr.
     */
    public function err($message = null, int $newlines = 1)
    {
        $this->logfile->write($message, $newlines);
        return $this->stderr($message, $newlines);
    }

    /**
     * Initialise les ConsoleOutput
     * @param ConsoleOutput|TestConsoleOutput|null $out
     * @param ConsoleOutput|TestConsoleOutput|null $err
     */
    private function initializeOutput(
        $out = null,
        $err = null
    ) {
        $this->basicInitializeOutput($out, $err);
        $classname = substr(strrchr(__CLASS__, "\\"), 1);
        $this->logfile = new ConsoleOutput(
            Configure::read('App.paths.logs', LOGS) . 'cron-'. Inflector::underscore($classname) . '.log'
        );
    }
}
