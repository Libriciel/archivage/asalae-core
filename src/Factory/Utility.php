<?php
/**
 * AsalaeCore\Factory\Utility
 */

namespace AsalaeCore\Factory;

use AsalaeCore\Utility\Check;
use AsalaeCore\Utility\Exec;
use AsalaeCore\Utility\Notify;
use AsalaeCore\Utility\Session;
use AsalaeCore\Utility\ValidatorXml;
use Beanstalk\Utility\Beanstalk;
use Exception;
use ReflectionClass;
use ReflectionException;

/**
 * Factory des utilitaires
 *
 * @category    Factory
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2018, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
abstract class Utility
{
    /**
     * @var array Liste des namespaces à parcourir
     */
    private static $namespaces = ['', 'AsalaeCore\\Utility\\'];

    /**
     * @var array Garde en mémoire les [$name => $instance]
     */
    private static $instances = [];

    /**
     * Permet d'ajouter un namespace vers une classe utilitaire
     * @param string $namespace
     */
    public static function addNamespace(string $namespace)
    {
        if (!in_array($namespace, self::$namespaces)) {
            self::$namespaces[] = trim($namespace, '\\').'\\';
        }
    }

    /**
     * Permet d'obtenir une instance de la classe utilitaire
     * @param string $name
     * @param array  ...$args pour le constructeur
     * @return mixed|UtilityObject|Check|Beanstalk|Notify|Exec|Session|ValidatorXml
     * @throws Exception
     */
    public static function get(string $name, ...$args)
    {
        if (isset(self::$instances[$name])) {
            return self::$instances[$name];
        }
        $instance = '';
        foreach (self::$namespaces as $namespace) {
            if (class_exists($namespace.$name)) {
                $instance = $namespace.$name;
                break;
            }
        }
        if (empty($instance)) {
            throw new Exception(sprintf("Class %s not found", $name));
        }
        $class = new ReflectionClass($instance);
        $constructor = $class->getConstructor();
        if (!$class->isAbstract() && (empty($constructor) || $constructor->isPublic())) {
            self::$instances[$name] = $class->newInstanceArgs($args);
        } else {
            self::$instances[$name] = new UtilityObject($instance);
        }
        return self::$instances[$name];
    }

    /**
     * Permet de définir la classe pour un nom donné
     * @param string       $name
     * @param string|mixed $instance
     * @param array        ...$args  pour le constructeur
     * @return mixed|UtilityObject
     * @throws ReflectionException
     */
    public static function set(string $name, $instance, ...$args)
    {
        if (is_string($instance)) {
            foreach (self::$namespaces as $namespace) {
                if (class_exists($namespace.$instance)) {
                    $instance = $namespace.$instance;
                    break;
                }
            }
            if (!class_exists($instance)) {
                throw new Exception(sprintf("Class %s not found", $instance));
            }
            $class = new ReflectionClass($instance);
            $constructor = $class->getConstructor();
            if (!$class->isAbstract() && (empty($constructor) || $constructor->isPublic())) {
                self::$instances[$name] = $class->newInstanceArgs($args);
            } else {
                self::$instances[$name] = new UtilityObject($instance);
            }
        } else {
            self::$instances[$name] = $instance;
        }
        return self::$instances[$name];
    }

    /**
     * Si on a besoin d'une nouvelle instance, on détruit celle en mémoire
     * @param string $name
     */
    public static function destruct(string $name)
    {
        unset(self::$instances[$name]);
    }

    /**
     * Détruit toutes les instances
     */
    public static function reset()
    {
        foreach (array_keys(self::$instances) as $name) {
            self::destruct($name);
        }
    }
}
