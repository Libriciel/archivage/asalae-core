<?php
/**
 * AsalaeCore\Factory\Utility
 */

namespace AsalaeCore\Factory;

/**
 * Factory des utilitaires statiques
 * Permet d'utiliser une classe abstraite comme un objet instancié
 *
 * @category    Factory
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2018, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class UtilityObject
{

    /**
     * @var string instance de la classe statique
     */
    private $instance;

    /**
     * Constructeur de classe
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->instance = $name;
    }

    /**
     * Redirige les appels de fonction vers la classe statique
     * @param string $func
     * @param array  $args
     * @return mixed
     */
    public function __call(string $func, array $args)
    {
        return forward_static_call_array([$this->instance, $func], $args);
    }
}
