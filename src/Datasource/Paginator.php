<?php
/**
 * AsalaeCore\Datasource\Paginator
 */

namespace AsalaeCore\Datasource;

use AsalaeCore\Http\AppServerRequest;
use Cake\Database\Expression\OrderByExpression;
use Cake\Datasource\Paging\Exception\PageOutOfBoundsException;
use Cake\Datasource\Paging\NumericPaginator;
use Cake\Datasource\QueryInterface;
use Cake\Datasource\RepositoryInterface;
use Cake\Datasource\ResultSetInterface;
use Cake\ORM\Query;
use Cake\ORM\Table;

/**
 * Gestion de la pagination
 *
 * @category Datasource
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Paginator extends NumericPaginator
{
    /**
     * {@inheritDoc}
     * @param QueryInterface|RepositoryInterface $object
     * @param array                              $params
     * @param array                              $settings
     * @return ResultSetInterface
     */
    public function paginate($object, array $params = [], array $settings = []): ResultSetInterface
    {
        $query = null;
        if ($object instanceof QueryInterface) {
            $query = $object;
            $object = $query->getRepository();
        }

        $alias = $object->getAlias();
        $defaults = $this->getDefaults($alias, $settings);
        $options = $this->mergeOptions($params, $defaults);
        $options = $this->validateSort($object, $options);
        $options = $this->checkLimit($options);
        $options['order'] = $defaults['order'] ?? [];
        /** @var AppServerRequest $request */
        $request = $options['request'] ?? null;
        $options['direction'] = $request ? $request->getQuery('direction') : null;

        $options += ['page' => 1, 'scope' => null];
        $options['page'] = max((int)$options['page'], 1);
        list($finder, $options) = $this->_extractFinder($options);
        $this->sortable($query, $options);

        if (empty($query)) {
            $query = $object->find($finder, $options);
        } else {
            $query->applyOptions($options);
        }

        $cleanQuery = clone $query;
        $results = $query->all();
        $numResults = count($results);
        $count = $cleanQuery->count();

        $page = $options['page'];
        $limit = $options['limit'];
        $pageCount = max((int)ceil($count / $limit), 1);
        $requestedPage = $page;
        $page = min($page, $pageCount);

        $order = (array)$options['order'];
        $sortDefault = $directionDefault = false;
        if (!empty($defaults['order']) && count($defaults['order']) === 1) {
            $sortDefault = key($defaults['order']);
            $directionDefault = current($defaults['order']);
        }

        $start = 0;
        if ($count >= 1) {
            $start = (($page - 1) * $limit) + 1;
        }
        $end = $start + $limit - 1;
        if ($count < $end) {
            $end = $count;
        }

        $paging = [
            'finder' => $finder,
            'requestedPage' => $page,
            'page' => $page,
            'current' => $numResults,
            'count' => $count,
            'perPage' => $limit,
            'start' => $start,
            'end' => $end,
            'prevPage' => $page > 1,
            'nextPage' => $count > ($page * $limit),
            'pageCount' => $pageCount,
            'sort' => $options['sort'] ?? null,
            'direction' => $options['direction'] ?: (isset($options['sort']) ? current($order) : null),
            'limit' => $defaults['limit'] != $limit ? $limit : null,
            'sortDefault' => $sortDefault,
            'directionDefault' => $directionDefault,
            'scope' => $options['scope'],
            'completeSort' => $order,
        ];

        $this->_pagingParams = [$alias => $paging];

        if ($requestedPage > $page) {
            throw new PageOutOfBoundsException(
                [
                    'requestedPage' => $requestedPage,
                    'pagingParams' => $this->_pagingParams,
                ]
            );
        }

        return $results;
    }

    /**
     * Permet d'effectuer des tris sur d'autres models à partir de liens prédéfinis
     * @param Query|null $query
     * @param array      $options
     */
    private function sortable($query, &$options)
    {
        $sort = $options['sort'] ?? null;
        if (!$sort || !$query) {
            return;
        }
        $Table = $query->getRepository();
        /** @var OrderByExpression $exp */
        $exp = $query->clause('order');
        $order = [];
        if ($exp) {
            $exp->iterateParts(
                function ($value, $key) use (&$order, $Table) {
                    if ($Table instanceof Table && $Table->hasField($key)) {
                        $key = $Table->getAlias().'.'.$key;
                    }
                    $order[$key] = $value;
                }
            );
        }
        $sortable = $options['sortable'] ?? [];
        if (isset($sortable[$sort])) {
            $options['order'] = [$sortable[$sort] => $options['direction'] ?: 'asc'] + $order;
        } elseif ($Table instanceof Table && $Table->hasField($sort)) {
            $fieldname = $Table->getAlias().'.'.$sort;
            $options['order'] = [$fieldname => $options['direction'] ?: 'asc'] + $order;
        }
        $query->order($options['order'], true);
    }
}
