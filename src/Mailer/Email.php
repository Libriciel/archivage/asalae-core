<?php
/**
 * AsalaeCore\Mailer\Email
 */

namespace AsalaeCore\Mailer;

use Cake\Mailer\Mailer;

/**
 * Surcharge pour encodage complet du sujet pour éviter la perte d'espaces
 *
 * @category Http
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @deprecated since cakephp 4 - use Cake\Mailer\Mailer instead
 */
class Email extends Mailer
{
    /**
     * Encode the specified string using the current charset
     *
     * @param string $text String to encode
     * @return string Encoded string
     */
    protected function _encode($text)
    {
        if (empty($this->headerCharset)) {
            $this->headerCharset = $this->charset;
        }
        return sprintf("=?%s?B?%s?=", $this->headerCharset, base64_encode($text));
    }
}
