<?php
/**
 * AsalaeCore\Controller\Component\FilterComponent
 */

namespace AsalaeCore\Controller\Component;

use AsalaeCore\Controller\Controller;
use AsalaeCore\Factory\Utility;
use Authorization\Identity;
use Cake\Controller\Component;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use ZMQSocketException;

/**
 * Component d'un controller utilisant les filtres de recherche
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class FilterComponent extends Component
{

    /**
     * Permet de charger dans la vue les filtres sauvegardés dans
     * le contexte actuel sans la variable <b>$savedFilters<b>
     */
    public function loadAndSave()
    {
        /** @var Controller $controller */
        $controller = $this->getController();
        $SavedFilters = TableRegistry::getTableLocator()->get('SavedFilters');

        $this->saveFilters();
        $request = $controller->getRequest();
        /** @var Identity $identity */
        $identity = $request->getAttribute('identity');
        $user = $identity ? $identity->getOriginalData() : null;
        $user_id = Hash::get($user ?: [], 'id');
        $ctrl = $request->getParam('controller');
        $action = $request->getParam('action');
        if (!$user_id || !$ctrl || !$action) {
            return;
        }

        $result = $SavedFilters->find()
            ->where(
                [
                    'user_id' => $user_id,
                    'controller' => $ctrl,
                    'action' => $action,
                ]
            )
            ->contain(['Filters'])
            ->all()
            ->toArray();
        $controller->set('savedFilters', $result);
    }


    /**
     * Permet d'obtenir la liste des filtres dans l'url
     *
     * @return void|array
     * @throws ZMQSocketException
     */
    private function extractFilters()
    {
        $SavedFilters = TableRegistry::getTableLocator()->get('SavedFilters');
        /** @var Controller $controller */
        $controller = $this->getController();
        $request = $controller->getRequest();
        $params = $request->getParam('?');
        $exists = $SavedFilters->exists(
            [
                'name' => $params['SaveFilterName'],
                'user_id' => $controller->Authentication->getIdentityData('id'),
                'controller' => $request->getParam('controller'),
                'action' => $request->getParam('action'),
            ]
        );
        if ($exists) {
            Utility::get('Notify')->send(
                $controller->Authentication->getIdentityData('id'),
                [$request->getSession()->read('User.token')],
                __("Les filtres n'ont pas été sauvegardés car un nom de sauvegarde identique existe déjà")
            );
            return;
        }
        unset($params['SaveFilterSelect'], $params['SaveFilterName'], $params['filter'], $params['OverrideFilter']);
        $filters = $this->dataToFilters($params);
        return array_values($filters);
    }

    /**
     * Converti les données brut en filtres près à être enregistré
     * ['monfiltre' => ['mavaleur']] => [['key' => 'monfiltre[0]', 'value' => 'mavaleur']]
     * @param array $data
     * @return array
     */
    public function dataToFilters(array $data)
    {
        $filters = [];
        foreach ($data as $field => $values) {
            $this->recursiveDataToFilters($values, $field, $filters);
        }
        return $filters;
    }

    /**
     * Permet une profondeur importante au dataToFilters()
     * @param array  $data
     * @param string $field
     * @param array  $filters
     */
    private function recursiveDataToFilters($data, $field, &$filters)
    {
        foreach ($data as $k => $value) {
            if (is_array($value)) {
                $this->recursiveDataToFilters($value, $field.'['.$k.']', $filters);
            } else {
                $key = $field . '['.$k.']';
                $filters[$key] = compact('key', 'value');
            }
        }
    }

    /**
     * Logique de sauvegarde pour écraser une sauvegarde existante
     * (ne sauvegarde que si modifications, supprime le reste)
     *
     * @param Entity $entity  SavedFilter
     * @param array  $filters
     */
    private function overrideSavedFilters(Entity $entity, array $filters)
    {
        $SavedFilters = TableRegistry::getTableLocator()->get('SavedFilters');
        $Filters = TableRegistry::getTableLocator()->get('Filters');
        if (empty($filters)) {
            $SavedFilters->delete($entity);
            return;
        }
        foreach ($entity->filters as $k => $e_filter) {
            $exists = false;
            foreach ($filters as $key => $filter) {
                if ($filter['key'] === $e_filter->key
                    && $filter['value'] === $e_filter->value
                ) {
                    $exists = true;
                    unset($filters[$key]);
                    break;
                }
            }
            if (!$exists) {
                $Filters->delete($e_filter);
                unset($entity->filters[$k]);
            }
        }
        foreach ($filters as $filter) {
            $newFilter = $Filters->newEntity(
                [
                    'saved_filter_id' => $entity->id,
                    'key' => $filter['key'],
                    'value' => $filter['value'],
                ]
            );
            $Filters->save($newFilter);
            $entity->filters[] = $newFilter;
        }
    }

    /**
     * Sauvegarde des filtres
     */
    private function saveFilters()
    {
        /** @var Controller $controller */
        $controller = $this->getController();
        $request = $controller->getRequest();
        $SavedFilters = TableRegistry::getTableLocator()->get('SavedFilters');
        $newSave = $request->getParam('?.SaveFilterSelect') === '-1'
            && $request->getParam('?.SaveFilterName');
        $overrideSave = $request->getParam('?.SaveFilterSelect') !== '-1'
            && $request->getParam('?.OverrideFilter');
        $entity = null;
        if ($overrideSave) {
            $entity = $SavedFilters->find()
                ->where(
                    [
                        'id' => $request->getParam('?.SaveFilterSelect'),
                        'user_id' => $controller->Authentication->getIdentityData('id'),
                        'controller' => $request->getParam('controller'),
                        'action' => $request->getParam('action'),
                    ]
                )
                ->contain(['Filters'])
                ->first();
            if (!$entity) {
                $overrideSave = false;
            }
        }

        if ($newSave) {
            $filters = $this->extractFilters();
            if (empty($filters)) {
                return;
            }
            $entity = $SavedFilters->newEntity(
                [
                    'name' => $request->getParam('?.SaveFilterName'),
                    'user_id' => $controller->Authentication->getIdentityData('id'),
                    'controller' => $request->getParam('controller'),
                    'action' => $request->getParam('action'),
                    'filters' => $filters
                ],
                ['associated' => 'Filters']
            );
            $SavedFilters->save($entity);
        } elseif ($overrideSave) {
            $params = $request->getParam('?');
            unset($params['SaveFilterSelect'], $params['SaveFilterName'], $params['filter'], $params['OverrideFilter']);
            $filters = $this->dataToFilters($params);
            /* @var Entity $entity */
            $this->overrideSavedFilters($entity, array_values($filters));
        }
    }
}
