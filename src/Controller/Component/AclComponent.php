<?php
/**
 * AsalaeCore\Controller\Component\AclComponent
 */

namespace AsalaeCore\Controller\Component;

use Acl\Controller\Component\AclComponent as AclAclComponent;
use Cake\Controller\ComponentRegistry;
use Cake\Core\App;
use Cake\Core\Configure;
use Cake\Core\Exception\Exception;

/**
 * Surcharge du AclComponent
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class AclComponent extends AclAclComponent
{
    /**
     * Constructor. Will return an instance of the correct ACL class as defined in `Configure::read('Acl.classname')`
     *
     * @param ComponentRegistry $collection A ComponentRegistry
     * @param array             $config     Array of configuration settings
     * @throws \Cake\Core\Exception\Exception when Acl.classname could not be loaded.
     */
    public function __construct(ComponentRegistry $collection, array $config = [])
    {
        parent::__construct($collection, $config);
        $className = $name = Configure::read('Acl.classname');
        if (!class_exists($className)) {
            $className = App::className($name, 'Adapter');
            if (!$className) {
                throw new Exception(sprintf('Could not find %s.', $name));
            }
        }
        $this->adapter($className);
    }
}
