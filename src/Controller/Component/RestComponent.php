<?php
/**
 * AsalaeCore\Controller\Component\RestComponent
 */

namespace AsalaeCore\Controller\Component;

use AsalaeCore\Controller\Controller;
use AsalaeCore\Model\Entity\Webservice;
use Authorization\Identity;
use Cake\Controller\Component;
use Cake\Http\Exception\NotAcceptableException;
use Cake\Utility\Hash;
use Cake\Utility\Inflector;

/**
 * Permet de désigner des actions comme action / rest
 *
 * @category Component
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class RestComponent extends Component
{
    /**
     * @var string Identité ex: webservice_1
     */
    public $identity = 'anonymous';

    /**
     * @var Webservice Entité du webservice
     */
    public $webservice;

    /**
     * @var array Règles par défaut pour self::route()
     */
    private $defaultRules = [
        'auth' => false,
        'accept' => '*',
    ];

    /**
     * @var array Liste des actions possibles
     */
    private $actions = [];

    /**
     * Constructor hook method.
     *
     * Implement this method to avoid having to overwrite
     * the constructor and call parent.
     *
     * @param array $config The configuration settings provided to this component.
     * @return void
     */
    public function initialize(array $config): void
    {
        /** @var Controller $Controller */
        $Controller = $this->getController();
        if ($Controller->getRequest()->getParam('action') === 'api') {
            $this->getController()->disableAutoRender();
        }
        $request = $Controller->getRequest();
        /** @var Identity $identity */
        $identity = $request->getAttribute('identity');
        $user = $identity ? $identity->getOriginalData() : null;
        $this->identity = 'webservice_'.($user ? Hash::get($user, 'id') : '');
        $this->webservice = $user;
    }

    /**
     * Permet de renvoyer une route vers une fonction privée choisie (callable)
     * tout en controllant les droits si besoin
     * @return array|bool
     */
    public function route()
    {
        $Controller = $this->getController();
        $routes = array_map(
            function ($v) {
                return array_change_key_case((array)$v) + ['*' => []];
            },
            Hash::normalize($this->actions)
        );
        $action = $this->getAction();
        if (is_numeric($action) || !$action) {
            $route = 'default';
        } else {
            $route = $action;
        }
        if (array_key_exists($route, $routes)) {
            $method = strtolower($Controller->getRequest()->getMethod());
            if (!isset($routes[$route][$method])) {
                $method = '*';
            }
            $rules = $routes[$route][$method];
            if (!empty($routes[$route]['*']) && $method !== '*') {
                $rules += $routes[$route]['*'];
            }
            $rules += ['function' => $route] + $this->defaultRules;
            if ($rules['accept'] !== '*') {
                if (!$Controller->getRequest()->accepts($rules['accept'])) {
                    throw new NotAcceptableException(__("406 Not Acceptable"));
                }
            }
            $args = $Controller->getRequest()->getParam('pass');
            if ($route !== 'default') {
                unset($args[0]);
            }
            return [
                'callback' => [$Controller, $rules['function']],
                'args' => $args,
            ] + $rules;
        }
        return false;
    }
    /**
     * Donne l'action api (paramètre parsé comme l'action)
     * @return string
     */
    private function getAction(): string
    {
        $action = $this->getController()->getRequest()->getParam('pass.0');
        if (empty($action) || is_numeric($action)) {
            return '';
        }
        $camel = Inflector::camelize($action, '-');
        return substr($action, 0, 1).substr($camel, 1);
    }

    /**
     * Définit les actions possibles via l'api
     *
     * exemple :
     * Le controller Users permet d'obtenir les infos d'un utilisateur et
     * possède également une méthode pour réinitialiser son mot de passe :
     *  $this->setApiActions([
     *      'default' => [          // pour https://mon.url/users[/:id]
     *          'get' => [
     *              'params' => ['id' => ['numeric', 'optional']],
     *              'function' => 'apiGet',
     *          ]
     *      ],
     *      'resetPassword' => [    // pour https://mon.url/users/reset-password/15
     *          'put' => [
     *              'function' => 'resetPassword'
     *          ]
     *      ]
     *  ])
     *
     * @param array $actions
     */
    public function setApiActions(array $actions)
    {
        $this->actions = $actions;
    }
}
