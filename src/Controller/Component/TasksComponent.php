<?php
/**
 * AsalaeCore\Controller\Component\TasksComponent
 */

namespace AsalaeCore\Controller\Component;

use AsalaeCore\Factory\Utility;
use Cake\Controller\Component;
use Exception;
use Pheanstalk\Job;

/**
 * Administration technique de asalae2
 *
 * @category Component
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class TasksComponent extends Component
{
    /**
     * Suppression du tube
     *
     * @param string $tubeName
     * @return bool success
     */
    public function deleteTube(string $tubeName): bool
    {
        try {
            $Beanstalk = Utility::get('Beanstalk');
            if (!method_exists($Beanstalk, 'getPheanstalk')) {
                return false;
            }
            $Beanstalk->params['disable_check_ttr'] = true;
            $pheanstalk = $Beanstalk->getPheanstalk();
            /** @var Job $job */
            while ($pheanstalk->statsTube($tubeName)->{'current-jobs-buried'}) {
                $job = $pheanstalk->peekBuried($tubeName);
                $pheanstalk->delete($job);
            }
            while ($pheanstalk->statsTube($tubeName)->{'current-jobs-delayed'}) {
                $job = $pheanstalk->peekDelayed($tubeName);
                $pheanstalk->delete($job);
            }
            while ($pheanstalk->statsTube($tubeName)->{'current-jobs-ready'}) {
                $job = $pheanstalk->peekReady($tubeName);
                $pheanstalk->delete($job);
            }
            return true;
        } catch (Exception $ex) {
            return false;
        }
    }
}
