<?php
/**
 * AsalaeCore\Controller\Component\ReaderComponent
 */

namespace AsalaeCore\Controller\Component;

use AsalaeCore\Controller\Component\Reader\ReaderInterface;
use Cake\Controller\Component;
use Cake\Core\Configure;
use Cake\Http\Exception\NotImplementedException;
use Exception;

/**
 * Permet de sélectionner un reader par rapport à un type mime
 *
 * @category Component
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ReaderComponent extends Component
{
    /**
     * Donne la liste des readers
     * @return array
     */
    public static function getReaders(): array
    {
        return Configure::read('Readers', []);
    }

    /**
     * Permet de savoir si un mime correspond à un reader
     * @param string $mime
     * @return bool
     * @throws Exception
     */
    public function canRead(string $mime): bool
    {
        $Controller = $this->getController();
        foreach (static::getReaders() as $classname) {
            /** @var ReaderInterface $reader */
            $reader = new $classname($Controller);
            if ($reader->canRead($mime)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Récupère le reader associé à un mime
     * @param string $mime
     * @return ReaderInterface
     * @throws Exception
     */
    public function get(string $mime): ReaderInterface
    {
        $Controller = $this->getController();
        foreach ($this->getReaders() as $classname) {
            /** @var ReaderInterface $reader */
            $reader = new $classname($Controller);
            if ($reader->canRead($mime)) {
                return $reader;
            }
        }
        throw new NotImplementedException;
    }
}
