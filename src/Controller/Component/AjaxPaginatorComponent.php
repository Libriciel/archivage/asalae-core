<?php
/**
 * AsalaeCore\Controller\Component\AjaxPaginatorComponent
 */

namespace AsalaeCore\Controller\Component;

use AsalaeCore\Http\Response;
use Cake\Controller\Component;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\Datasource\ResultSetInterface;
use Cake\ORM\Query;
use Psr\Http\Message\ResponseInterface;

/**
 * Pagination ajax
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019 Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class AjaxPaginatorComponent extends Component
{
    /**
     * Default config
     *
     * These are merged with user-provided config when the component is used.
     *
     * @var array
     */
    protected $_defaultConfig = [
        'limit' => 100,
    ];

    /**
     * Constructor hook method.
     *
     * Implement this method to avoid having to overwrite
     * the constructor and call parent.
     *
     * @param array $config The configuration settings provided to this component.
     * @return void
     */
    public function initialize(array $config): void
    {
        if (empty($config['limit'])) {
            $this->setConfig('limit', Configure::read('AjaxPaginator.limit', 100));
        }
    }

    /**
     * Pagine une requète
     * @param Query $query
     * @return ResultSetInterface
     */
    public function paginate(Query $query)
    {
        $request = $this->getController()->getRequest();
        $limit = $request->getParam('?.limit', $this->getConfig('limit') ?: 100);
        $page = $request->getParam('?.page', 1);
        $sort = $request->getParam('?.sort');
        $direction = $request->getParam('?.direction', 'asc');

        if ($sort) {
            $object = $query->getRepository();
            $tableAlias = $object->getAlias();
            $tableOrder = [];

            $key = $sort;
            $value = $direction;

            // import de PaginatorComponent::_prefix
            $field = $key;
            $alias = $tableAlias;

            if (strpos($key, '.') !== false) {
                [$alias, $field] = explode('.', $key);
            }
            $correctAlias = ($tableAlias === $alias);

            if ($correctAlias) {
                // Disambiguate fields in schema. As id is quite common.
                if ($object->hasField($field)) {
                    $field = $alias . '.' . $field;
                }
                $tableOrder[$field] = $value;
            } else {
                $tableOrder[$alias . '.' . $field] = $value;
            }
            // fin import

            $query->order($tableOrder, true);
        }

        return $query->limit($limit)
            ->offset(($page * $limit) - $limit)
            ->all();
    }

    /**
     * Rendu json de la pagination ajax
     * @param Query         $query
     * @param bool          $count
     * @param callable|null $callback
     * @return \Cake\Http\Response|Response|ResponseInterface
     */
    public function json(Query $query, bool $count = false, callable $callback = null): ResponseInterface
    {
        $headerCount = $this->getController()->getRequest()->getHeader('X-Paginator-Count');
        if ($headerCount && $headerCount[0]) {
            $count = true;
        }
        $data = $this->paginate($query);
        if ($callback) {
            $ndata = [];
            foreach ($callback($data) as $row) {
                $ndata[] = $row;
            }
            $data = $ndata;
        }
        if ($data instanceof ResultSetInterface) {
            $data = $data->toArray();
        }
        foreach ($data as $index => $values) {
            if ($values instanceof EntityInterface) {
                $values = $values->toArray();
            }
            foreach ($values as $field => $value) {
                if (is_resource($value)) {
                    rewind($value);
                    $data[$index][$field] = stream_get_contents($value);
                }
            }
        }
        $response = $count
            ? $this->getResponseWithCountHeaders($query->count())
            : $this->getController()->getResponse();
        $body = $response->getBody();
        $body->write(json_encode($data));
        $body->rewind();
        return $response
            ->withBody($body)
            ->withType('json');
    }

    /**
     * Donne un objet Response avec des headers donnant le nombre de résultats
     * au javascript
     * @param int $count
     * @param int $page
     * @return \Cake\Http\Response|Response|ResponseInterface
     */
    public function getResponseWithCountHeaders(int $count, int $page = 1): ResponseInterface
    {
        $limit = $this->getConfig('limit');
        $pageCount = max((int)ceil($count / $limit), 1);
        $page = min($page, $pageCount);

        $start = 0;
        if ($count >= 1) {
            $start = (($page - 1) * $limit) + 1;
        }
        $end = $start + $limit - 1;
        if ($count < $end) {
            $end = $count;
        }

        $response = $this->getController()->getResponse();
        $params = [
            'page' => $page,
            'count' => $count,
            'perPage' => $limit,
            'start' => $start,
            'end' => $end,
            'prevPage' => $page > 1 ? 'true' : 'false',
            'nextPage' => $count > ($page * $limit) ? 'true' : 'false',
            'pageCount' => $pageCount,
            'limit' => $limit,
        ];
        foreach ($params as $key => $value) {
            $response = $response->withHeader('X-Paginator-'.ucfirst($key), $value);
        }
        return $response;
    }
}
