<?php
/**
 * AsalaeCore\Controller\Component\Reader\OpenDocumentReader
 */

namespace AsalaeCore\Controller\Component\Reader;

/**
 * Lecteur de open-document
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019 Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class OpenDocumentReader extends PdfReader implements ReaderInterface
{
    /**
     * Vérifi qu'un mime est accepté par le reader
     * @param string $mime
     * @return bool
     */
    public static function canRead(string $mime): bool
    {
        return preg_match('/^application\/vnd\.oasis\.opendocument\..*$/', $mime);
    }
}
