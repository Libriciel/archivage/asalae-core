<?php
/**
 * AsalaeCore\Controller\Component\Reader\ReaderInterface
 */

namespace AsalaeCore\Controller\Component\Reader;

use Cake\Controller\Controller;
use Cake\Http\Response;

/**
 * Lecteurs
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019 Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
interface ReaderInterface
{
    /**
     * ReaderInterface constructor.
     * @param Controller $controller
     */
    public function __construct(Controller $controller);

    /**
     * Vérifi qu'un mime est accepté par le reader
     * @param string $mime
     * @return bool
     */
    public static function canRead(string $mime): bool;

    /**
     * Typiquement, mettre le fichier dans le body de la réponse et
     * renvoyer la méthode read()
     * @return Response
     */
    public function getResponse(): Response;

    /**
     * Défini l'url où on peut trouver la ressource
     * @param string $url
     * @return ReaderInterface
     */
    public function setDownloadUrl(string $url): ReaderInterface;
}
