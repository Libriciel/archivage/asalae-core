<?php
/**
 * AsalaeCore\Controller\Component\Reader\ImageReader
 */

namespace AsalaeCore\Controller\Component\Reader;

use Cake\Controller\Controller;
use Cake\Http\Response;

/**
 * Lecteur de open-document
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019 Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ImageReader implements ReaderInterface
{
    /**
     * @var Controller
     */
    protected $controller;

    /**
     * @var string url vers la ressource
     */
    protected $url;

    /**
     * ReaderInterface constructor.
     * @param Controller $controller
     */
    public function __construct(Controller $controller)
    {
        $this->controller = $controller;
    }

    /**
     * Vérifi qu'un mime est accepté par le reader
     * @param string $mime
     * @return bool
     */
    public static function canRead(string $mime): bool
    {
        return substr($mime, 0, 5) === 'image';
    }

    /**
     * Typiquement, mettre le fichier dans le body de la réponse et
     * renvoyer la méthode read()
     * @return Response
     */
    public function getResponse(): Response
    {
        return $this->controller->redirect($this->url);
    }

    /**
     * Défini l'url où on peut trouver la ressource
     * @param string $url
     * @return ReaderInterface
     */
    public function setDownloadUrl(string $url): ReaderInterface
    {
        $this->url = $url;
        return $this;
    }
}
