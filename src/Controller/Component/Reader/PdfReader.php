<?php
/**
 * AsalaeCore\Controller\Component\Reader\PdfReader
 */

namespace AsalaeCore\Controller\Component\Reader;

use Cake\Controller\Controller;
use Cake\Http\Response;

/**
 * Lecteur de pdf
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019 Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class PdfReader implements ReaderInterface
{
    /**
     * @var Controller
     */
    protected $controller;

    /**
     * @var string url vers la ressource
     */
    protected $url;

    /**
     * ReaderInterface constructor.
     * @param Controller $controller
     */
    public function __construct(Controller $controller)
    {
        $this->controller = $controller;
    }

    /**
     * Vérifi qu'un mime est accepté par le reader
     * @param string $mime
     * @return bool
     */
    public static function canRead(string $mime): bool
    {
        return strpos($mime, 'pdf') !== false;
    }

    /**
     * Permet d'obtenir la réponse à envoyer à l'utilisateur
     * @return Response
     */
    public function getResponse(): Response
    {
        return $this->controller
            ->redirect('/webroot/js/ViewerJS/index.html#'.$this->url);
    }

    /**
     * Défini l'url où on peut trouver la ressource
     * @param string $url
     * @return ReaderInterface
     */
    public function setDownloadUrl(string $url): ReaderInterface
    {
        $this->url = $url;
        return $this;
    }
}
