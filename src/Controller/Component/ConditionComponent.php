<?php
/**
 * AsalaeCore\Controller\Component\ConditionComponent
 */

namespace AsalaeCore\Controller\Component;

use AsalaeCore\Controller\Controller;
use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\Database\Connection;
use Cake\Database\Driver;
use Cake\Database\Expression\IdentifierExpression;
use Cake\Database\Expression\QueryExpression;
use Cake\Datasource\ConnectionInterface;
use Cake\Datasource\ConnectionManager;
use Cake\I18n\FrozenDate as Date;
use Cake\Http\Exception\BadRequestException;
use Cake\Utility\Inflector;
use DateInterval;
use DateTime;
use DateTimeInterface;
use Exception;
use PDO;

/**
 * Component d'un controller utilisant les filtres de recherche
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, 2018 Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ConditionComponent extends Component
{
    const DATE_GAP_TYPE_YEAR = 'year';
    const DATE_GAP_TYPE_MONTH = 'month';
    const DATE_GAP_TYPE_DAY = 'day';

    /**
     * @var ConnectionInterface Classe de connexion
     */
    public $connection;

    /**
     * @var string driver name (ex: mysql, postgres etc...)
     */
    public $driverName = '';

    /**
     * ConditionComponent constructor.
     * @param ComponentRegistry $registry
     * @param array             $config
     */
    public function __construct(ComponentRegistry $registry, array $config = [])
    {
        parent::__construct($registry, $config);
        $this->connection = ConnectionManager::get('default');
        if ($this->connection instanceof Connection) {
            /** @var Driver $driver */
            $driver = $this->connection->getDriver();
            if (!$driver->isConnected()) {
                $driver->connect();
            }
            $pdo = $driver->getConnection();
            $this->driverName = $pdo->getAttribute(PDO::ATTR_DRIVER_NAME);
        }
    }

    /**
     * Condition pour un $field like $value
     * @param string $field
     * @param string $value
     * @param bool   $string
     * @return array|string
     */
    public function like(string $field, string $value, bool $string = false)
    {
        $value = preg_replace('/\?/', '_', $value);
        $value = preg_replace('/\*/', '%', $value);
        return $string
            ? $field.' LIKE '.$this->connection->getDriver()->quote($value)
            : [$field.' LIKE' => $value];
    }

    /**
     * Condition pour un $field ilike $value
     * @param string $field
     * @param string $value
     * @return array
     */
    public function ilike(string $field, string $value)
    {
        $value = preg_replace('/\?/', '_', $value);
        $value = preg_replace('/\*/', '%', $value);
        if ($this->driverName === 'pgsql') {
            return [$field.' ILIKE' => $value];
        }
        return [$field.' LIKE' => $value];
    }

    /**
     * Conditions pour un $field in $values
     * @param string $field
     * @param mixed  $values
     * @return array
     */
    public function in(string $field, $values)
    {
        if (empty($values)) {
            return ['0 = 1'];
        }
        return [$field.' IN' => $values];
    }

    /**
     * Conditions pour un $field not in $values
     * @param string $field
     * @param mixed  $values
     * @return array
     */
    public function notIn(string $field, $values)
    {
        if (empty($values)) {
            return ['1 = 1'];
        }
        return [$field.' NOT IN' => $values];
    }

    /**
     * Permet d'obtenir la condition pour $fieldBegin <= $date <= $fieldEnd
     * @param string|DateTime $date
     * @param string          $fieldBegin
     * @param string          $fieldEnd
     * @param string          $format     Format de $date (default= 'auto')
     * @return array
     */
    public function dateBetweenFields($date, string $fieldBegin, string $fieldEnd, string $format = 'auto')
    {
        if (is_string($date)) {
            $date = $format === 'auto'
                ? Date::parseDateTime($date)->format('Y-m-d')
                : DateTime::createFromFormat($format, $date)->format('Y-m-d');
        } else {
            $date = $date->format('Y-m-d');
        }
        return [
            "date($fieldBegin) <=" => $date,
            "date($fieldEnd) >=" => $date,
        ];
    }

    /**
     * Le champ date $field doit se situer entre $dateMin et $dateMax
     * @param string          $field
     * @param string|DateTime $dateMin
     * @param string|DateTime $dateMax
     * @return QueryExpression
     */
    public function dateBetween(string $field, $dateMin, $dateMax)
    {
        $exp = new QueryExpression;
        if ($dateMin instanceof DateTimeInterface) {
            $dateMin = $dateMin->format('Y-m-d');
        }
        $dateMin = $this->connection->getDriver()->quote($dateMin);
        if ($dateMax instanceof DateTimeInterface) {
            $dateMax = $dateMax->format('Y-m-d');
        }
        $dateMax = $this->connection->getDriver()->quote($dateMax);
        return $exp->between(
            new IdentifierExpression("date($field)"),
            new IdentifierExpression("date($dateMin)"),
            new IdentifierExpression("date($dateMax)")
        );
    }

    /**
     * Condition $field $operator $date
     * @param string $field
     * @param string $operator
     * @param mixed  $date
     * @param string $format
     * @return array|QueryExpression
     * @throws Exception
     */
    public function dateOperator(string $field, string $operator, $date, string $format = 'auto')
    {
        if (preg_match('/^PT?\d+[HDMY]$/', $operator)) {
            $dateMin = (new DateTime)->sub(new DateInterval($operator));
            $dateMax = new DateTime;
            return $this->dateBetween($field, $dateMin, $dateMax);
        } elseif ($operator === 'today') {
            $now = new DateTime;
            return ["date($field)" => $now->format('Y-m-d')];
        } elseif ($operator === 'week') {
            $dateMin = (new DateTime)->modify('monday this week');
            $dateMax = (new DateTime)->modify('sunday this week');
            return $this->dateBetween($field, $dateMin, $dateMax);
        } elseif ($operator === 'month') {
            $dateMin = (new DateTime)->modify('first day of this month');
            $dateMax = (new DateTime)->modify('last day of this month');
            return $this->dateBetween($field, $dateMin, $dateMax);
        } elseif ($operator === 'year') {
            $dateMin = (new DateTime)->modify('first day of january');
            $dateMax = (new DateTime)->modify('last day of december');
            return $this->dateBetween($field, $dateMin, $dateMax);
        } elseif (!in_array($operator, ['=', '<=', '>=', '<', '>'])) {
            throw new BadRequestException(__("L'opérateur ({0}) n'est pas autorisé", h($operator)));
        }
        if ($date === 'NOW') {
            $date = new DateTime;
        }
        if (is_string($date)) {
            $date = $format === 'auto'
                ? Date::parseDateTime($date)->format('Y-m-d')
                : DateTime::createFromFormat($format, $date)->format('Y-m-d');
        } elseif (method_exists($date, 'format')) {
            $date = $date->format('Y-m-d');
        }
        if ($date[0] === '-' || substr($date, 0, 4) === '0000') {
            throw new BadRequestException(__("La date saisie ne peut pas être aussi lointaine"));
        }
        return ["date($field) ".$operator => $date];
    }

    /**
     * Condition $field IN favorites
     * @param string $field
     * @param string $table Si défini sur 'auto', essayera de trouver le nom
     *                      par rapport à $field, sinon par le nom du Controller
     * @return array
     */
    public function inFavorites(string $field, string $table = 'auto')
    {
        if ($table === 'auto') {
            if (strpos($field, '.')) {
                $table = explode('.', $field, 2)[0];
            } else {
                $table = $this->getController()->getName();
            }
        }
        $table = Inflector::dasherize($table);
        return $this->in($field, $this->getFavoritesCookies($table));
    }

    /**
     * Order by favorites $direction
     * @param string $direction
     * @param string $table
     * @return array
     */
    public function orderByFavorites(string $direction = 'asc', string $table = 'auto')
    {
        if ($table === 'auto') {
            $table = $this->getController()->getName();
        }
        $table = Inflector::dasherize($table);
        $favoritesIds = $this->getFavoritesCookies($table);
        if (empty($favoritesIds)) {
            return [];
        }
        return [Inflector::camelize($table, '-').'.id NOT IN ('.implode(', ', $favoritesIds).')' => $direction];
    }

    /**
     * Permet d'obtenir les ids des favories d'une table
     * @param string $table
     * @return array
     */
    private function getFavoritesCookies(string $table)
    {
        /** @var Controller $controller */
        $controller = $this->getController();
        $request = $controller->getRequest();
        $action = Inflector::dasherize($request->getParam('action', 'index'));
        $cookies = $request->getCookieParams();
        $sessionCookies = $controller->cookies;
        if (is_array($sessionCookies)) {
            $cookies = array_merge($cookies, $sessionCookies);
        }
        $favoritesIds = [];
        foreach (array_keys($cookies) as $cookieName) {
            if (preg_match("/^table-favorite-$table-$action-table-(\d+)$/", $cookieName, $match)) {
                $favoritesIds[] = $match[1];
            }
        }
        return $favoritesIds;
    }

    /**
     * Donne l'eccart entre deux dates
     *
     * DATE_PART('$type', $field1) - DATE_PART('$type', $field2)
     *
     * @param string $field1
     * @param string $field2
     * @param string $type
     * @return string
     */
    public function dateDiff(
        string $field1,
        string $field2,
        string $type = self::DATE_GAP_TYPE_YEAR
    ): string {
        if ($this->driverName === 'pgsql') {
            return "DATE_PART('$type', $field1) - DATE_PART('$type', $field2)";
        } elseif ($this->driverName === 'sqlite') {
            switch ($type) {
                case self::DATE_GAP_TYPE_DAY:
                    $type = 'd';
                    break;
                case self::DATE_GAP_TYPE_MONTH:
                    $type = 'm';
                    break;
                case self::DATE_GAP_TYPE_YEAR:
                    $type = 'Y';
            }
            return "strftime('%$type', $field1) - strftime('%$type', $field2)";
        } else {
            return "DATEDIFF($type, $field1, $field2)";
        }
    }

    /**
     * Condition sur expression régulière
     * @param string $field
     * @param string $regex
     * @param bool   $string
     * @return array|string
     */
    public function regex(string $field, string $regex, bool $string = false)
    {
        $operator = $this->driverName === 'pgsql' ? '~' : 'REGEXP';
        return $string
            ? $field.' '.$operator.' '.$this->connection->getDriver()->quote($regex)
            : ["$field $operator" => $regex];
    }

    /**
     * string_agg compatible sqlite
     * pour le order by, en sqlite, il faut que $field soit déjà ordonné:
     *  ->from(['select $field from ma_table order by $field' => 'mon_alias'])
     * @param string      $field
     * @param string      $separator
     * @param string|null $order
     * @return string
     */
    public function stringAgg(string $field, string $separator = ', ', string $order = null)
    {
        if ($this->driverName === 'sqlite') {
            return "group_concat($field, '$separator')";
        } else {
            return "string_agg($field, '$separator'".($order ? " order by $order" : '').")";
        }
    }
}
