<?php
/**
 * AsalaeCore\Controller\Component\AuthorizationComponent
 */

namespace AsalaeCore\Controller\Component;

use AsalaeCore\Controller\ApiInterface;
use AsalaeCore\Controller\Controller;
use Authentication\Authenticator\UnauthenticatedException;
use Authorization\AuthorizationServiceInterface;
use Authorization\Controller\Component\AuthorizationComponent as CakeAuthorizationComponent;
use Authorization\Identity;
use Cake\Controller\Component;
use Cake\Event\EventInterface;
use Cake\Http\Exception\UnauthorizedException;
use Cake\Http\Response;
use Cake\Utility\Inflector;
use Exception;
use InvalidArgumentException;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Logique d'authorization avec ACL et commeDroit
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020 Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property AclComponent Acl
 * @property CakeAuthorizationComponent CakeAuthorization
 */
class AuthorizationComponent extends Component
{
    /**
     * Other Components this component uses.
     *
     * @var array
     */
    public $components = [
        'AsalaeCore.Acl',
        'Authentication.Authentication',
    ];

    /**
     * Default config
     * @var array
     */
    protected $_defaultConfig = [
        'serviceAttribute' => 'authorization',
    ];

    /**
     * Vérifi l'accès à la vue
     * @param EventInterface $event
     * @return Response|void
     * @throws Exception
     */
    public function beforeFilter(EventInterface $event)
    {
        /** @var Controller $controller */
        $controller = $this->getController();
        if (method_exists($controller, 'beforeFilter')) {
            $response = $controller->beforeFilter($event);
            if ($response) {
                return $response;
            }
            $event->stopPropagation(); // évite le double appel du beforeFilter du controlleur
        }
        $request = $controller->getRequest();
        $isApi = $controller instanceof ApiInterface && ($request->getParam('action')) === 'api';

        if ($isApi) {
            $action = $this->getController()->getRequest()->getParam('pass.0');
            if (!$action || is_numeric($action)) {
                $action = '';
            }
            $camel = Inflector::camelize($action, '-');
            $action = substr($action, 0, 1).substr($camel, 1);
        } else {
            $action = $request->getParam('action');
        }
        if (in_array($action, $controller->Authentication->getUnauthenticatedActions())) {
            if ($isApi) {
                $controller->Authentication->addUnauthenticatedActions(['api']);
            }
            $this->skipAuthorization();
            return;
        }
        $request = $this->getController()->getRequest();
        /** @var Identity $identity */
        $identity = $request->getAttribute('identity');
        $user = $identity ? $identity->getOriginalData() : null;
        if (!$user) {
            if ($request->is('ajax') || $isApi) {
                throw new UnauthorizedException(__("401 Unauthorized"));
            }
            throw new UnauthenticatedException; // redirige sur la page de login
        }
    }

    /**
     * Skips the authorization check.
     * @return self
     */
    public function skipAuthorization()
    {
        $request = $this->getController()->getRequest();
        $service = $this->getService($request);

        $service->skipAuthorization();

        return $this;
    }

    /**
     * Get the authorization service from a request.
     * @param ServerRequestInterface $request The request
     * @return AuthorizationServiceInterface
     * @throws InvalidArgumentException When invalid authorization service encountered.
     */
    protected function getService(ServerRequestInterface $request): AuthorizationServiceInterface
    {
        $serviceAttribute = $this->getConfig('serviceAttribute');
        $service = $request->getAttribute($serviceAttribute);
        if (!$service instanceof AuthorizationServiceInterface) {
            $type = is_object($service) ? get_class($service) : gettype($service);
            throw new InvalidArgumentException(
                sprintf(
                    'Expected that `%s` would be an instance of %s, but got %s',
                    $serviceAttribute,
                    AuthorizationServiceInterface::class,
                    $type
                )
            );
        }

        return $service;
    }
}
