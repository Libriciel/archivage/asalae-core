<?php
/**
 * AsalaeCore\Controller\Component\IndexComponent
 */

namespace AsalaeCore\Controller\Component;

use AsalaeCore\View\Helper\TranslateHelper;
use Cake\Controller\Component;
use Cake\Core\Configure;
use Cake\Http\Exception\BadRequestException;
use Cake\Http\Response;
use Cake\ORM\Query;
use Cake\Utility\Hash;
use Cake\Utility\Inflector;
use Cake\View\View;
use DateTime;
use Exception;

/**
 * Pour générer un affichage de résultats
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019 Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property \AsalaeCore\Controller\Component\AjaxPaginatorComponent AjaxPaginator
 * @property \AsalaeCore\Controller\Component\ConditionComponent Condition
 * @property \AsalaeCore\Controller\Component\FilterComponent Filter
 * @property \AsalaeCore\Controller\Component\JstableComponent Jstable
 */
class IndexComponent extends Component
{
    const FILTER_COUNTOPERATOR = 'countoperator';
    const FILTER_DATECOMPARENOW = 'datecompare';
    const FILTER_DATEOPERATOR = 'dateoperator';
    const FILTER_FAVORITES = 'favorites';
    const FILTER_ILIKE = 'ilike';
    const FILTER_IN = 'in';
    const FILTER_IS = 'is';
    const FILTER_IS_NOT = 'is-not';
    const FILTER_NOT_IN = 'not-in';
    const FILTER_EQUAL = '=';
    const FILTER_GREATER_OR_EQUAL = '>=';
    const FILTER_LESS_OR_EQUAL = '<=';
    const FILTER_GREATER = '>';
    const FILTER_LESS = '<';
    const FILTER_NOT_EQUAL = '!=';
    const FILTER_SIZEOPERATOR = 'sizeoperator';

    /**
     * Other Components this component uses.
     *
     * @var array
     */
    public $components = [
        'AsalaeCore.AjaxPaginator',
        'AsalaeCore.Condition',
        'AsalaeCore.Filter',
        'AsalaeCore.Jstable',
    ];

    /**
     * Default config
     *
     * These are merged with user-provided config when the component is used.
     *
     * @var array<string, mixed>
     */
    protected $_defaultConfig = [
        'csv_filename' => null, // Traduction de Controller/Action si null
    ];

    /**
     * Initialise un index
     */
    public function init()
    {
        $controller = $this->getController();
        $request = $controller->getRequest();
        $action = $request->getParam('action');
        $controller->paginate['limit'] = $controller->paginate['limit']
            ?? Configure::read('Pagination.limit', 20);
        $viewBuilder = $controller->viewBuilder();
        $model = $this->getConfig('model', $controller->getName());
        $tableId = sprintf(
            '%s-%s-table',
            Inflector::dasherize($model),
            Inflector::dasherize($action)
        );
        $controller->set('tableId', $tableId);
        $this->Jstable->configurablePagination($tableId);
        $this->Filter->loadAndSave();

        if ($request->is('ajax') && $viewBuilder->getLayout() === null) {
            $viewBuilder->setTemplate('ajax_'.$action);
        }
    }

    /**
     * @var Query $query
     */
    private $query;

    /**
     * Set le query de l'index pour le component
     * @param Query $query
     * @return $this
     */
    public function setQuery(Query $query)
    {
        $this->query = $query;
        $controller = $this->getController();
        $request = $controller->getRequest();
        if ($request->getParam('?.sort') === 'favorite') {
            $dir = $request->getParam('?.direction') ?: 'asc';
            $controller->paginate['order'] = $this->Condition->orderByFavorites($dir);
            $query->order($this->Condition->orderByFavorites($dir));
        }
        return $this;
    }

    /**
     * Permet de filtrer simplement les résultats d'un index
     * @param string               $urlParam
     * @param string|null|callable $type     type prédéfini ou callback(datavalue, query,
     *                                       fieldname)
     * @param string|null          $field
     * @return $this
     * @throws Exception
     */
    public function filter(string $urlParam, $type = null, string $field = null)
    {
        $q = $this->query;
        if (!$q instanceof Query) {
            throw new Exception(__("\$this->Index->setQuery() n'a pas été appelé avant \$this->Index->filter()"));
        }
        $controller = $this->getController();
        $model = $this->getConfig('model', $controller->getName());
        if ($field === null) {
            $field = strpos($urlParam, '.') !== false ? $urlParam : "$model.$urlParam";
        }
        $request = $controller->getRequest();
        $params = $request->getQuery($urlParam, []);
        foreach ((array)$params as $i => $value) {
            if ($value === ''
                || (is_array($value)
                && (!isset($value['value']) || $value['value'] === '')
                && (!isset($value[0]) || $value[0] === ''))
            ) {
                continue;
            }
            $this->filterParam($i, $value, $urlParam, $type, $field);
        }
        return $this;
    }

    /**
     * Condition ["$field $operator" => now]
     * @param string $field
     * @param string $operator
     * @return void
     * @throws Exception
     */
    private function filterDateCompareNow(string $field, string $operator)
    {
        $q = $this->query;
        if (!in_array($operator, ['=', '<=', '>=', '<', '>'])) {
            throw new BadRequestException(__("L'opérateur indiqué n'est pas autorisé"));
        }
        $q->where($this->Condition->dateOperator($field, $operator, new DateTime));
    }

    /**
     * Compare une taille
     * @param string $field
     * @param string $values
     * @return void
     */
    private function filterSizeOperator(string $field, $values)
    {
        $q = $this->query;
        if (!is_array($values) || !isset($values['value'])) {
            return;
        }
        $v = $values['value'] * $values['mult'];
        $operator = $values['operator'];
        if (!in_array($operator, ['=', '<=', '>=', '<', '>'])) {
            throw new BadRequestException(__("L'opérateur indiqué n'est pas autorisé"));
        }
        $q->where([$field . ' ' . $operator => $v]);
    }

    /**
     * Compare une quantité
     * @param string $field
     * @param string $values
     * @return void
     */
    private function filterCountOperator(string $field, $values)
    {
        $q = $this->query;
        if (!is_array($values) || !isset($values['value'])) {
            return;
        }
        $operator = $values['operator'];
        if (!in_array($operator, ['=', '<=', '>=', '<', '>'])) {
            throw new BadRequestException(__("L'opérateur indiqué n'est pas autorisé"));
        }
        $q->where([$field . ' ' . $operator => $values['value']]);
    }

    /**
     * Compare une quantité
     * @param string $field
     * @param string $value
     * @param string $operator
     * @return void
     * @throws Exception
     */
    private function filterDateOperator(string $field, $value, $operator)
    {
        $q = $this->query;
        if ($value) {
            $q->where($this->Condition->dateOperator($field, $operator, $value));
        }
    }

    /**
     * Compare une quantité
     * @param callable $type
     * @param string   $field
     * @param string   $value
     * @return void
     */
    private function filterCallable(callable $type, string $field, $value)
    {
        $q = $this->query;
        $call = call_user_func($type, $value, $q, $field);
        if (is_array($call)) {
            $q->where($call);
        }
    }

    /**
     * Applique les filtres sur un query param
     * @param int             $i
     * @param mixed           $value
     * @param string          $urlParam
     * @param string|callable $type
     * @param string          $field
     * @return void
     * @throws Exception
     */
    private function filterParam(int $i, $value, string $urlParam, $type, string $field)
    {
        $controller = $this->getController();
        $model = $this->getConfig('model', $controller->getName());
        $request = $controller->getRequest();
        $q = $this->query;
        switch ($type) {
            case null:
            default:
                $q->where([$field => $value]);
                break;
            case self::FILTER_DATECOMPARENOW:
                $this->filterDateCompareNow($field, $value);
                break;
            case self::FILTER_DATEOPERATOR:
                $operator = $request->getQuery("dateoperator_$urlParam.$i");
                $this->filterDateOperator($field, $value, $operator);
                break;
            case self::FILTER_FAVORITES:
                $q->where($this->Condition->inFavorites($model.'.id'));
                break;
            case self::FILTER_ILIKE:
                $q->where($this->Condition->ilike($field, $value));
                break;
            case self::FILTER_IN:
                $q->where($this->Condition->in($field, $value));
                break;
            case self::FILTER_IS:
                $q->where([$field.' IS' => $value]);
                break;
            case self::FILTER_IS_NOT:
                $q->where([$field.' IS NOT' => $value]);
                break;
            case self::FILTER_NOT_IN:
                $q->where($this->Condition->notIn($field, $value));
                break;
            case self::FILTER_SIZEOPERATOR:
                $this->filterSizeOperator($field, $value);
                break;
            case self::FILTER_COUNTOPERATOR:
                $this->filterCountOperator($field, $value);
                break;
            case self::FILTER_EQUAL:
            case self::FILTER_GREATER:
            case self::FILTER_GREATER_OR_EQUAL:
            case self::FILTER_LESS:
            case self::FILTER_LESS_OR_EQUAL:
            case self::FILTER_NOT_EQUAL:
                $q->where([$field.' '.$type => $value]);
                break;
            case is_callable($type):
                $this->filterCallable($type, $field, $value);
                break;
        }
    }

    /**
     * Bouton Télécharger en CSV / rendu CSV
     * Nécéssite un template en templates/<Controller>/csv/<action>.php
     * @return Response|void
     */
    public function exportCsv()
    {
        $controller = $this->getController();
        $request = $controller->getRequest();
        if ($request->getQuery('export_csv')) {
            return $this->renderCsv();
        } else {
            $uri = $controller->getRequest()->getUri();
            $queryParams = $uri->getQuery();
            if ($queryParams) {
                $queryParams .= '&';
            }
            $queryParams .= 'export_csv=true';
            $downloadCsv = [
                'href' => (string)$uri->withQuery($queryParams),
                'download' => $this->getCsvFilename(),
            ];
            $controller->set('downloadCsv', $downloadCsv);
        }
    }

    /**
     * Rendu CSV
     * @param null $data
     * @return Response
     * @see IndexComponent::exportCsv()
     */
    public function renderCsv($data = null)
    {
        $controller = $this->getController();
        $request = $controller->getRequest();
        $viewBuilder = $controller->viewBuilder();
        $controller->setResponse(
            $controller->getResponse()
                ->withType('csv')
                ->withHeader(
                    'Content-Disposition',
                    'attachment; filename="' . $this->getCsvFilename() . '"'
                )
        );
        if (empty($data)) {
            $data = $this->query->limit(Configure::read('Index.exportCsv.limit', 1000));
        }
        $controller->set('results', $data);
        $this->addPaginationSort();
        $viewBuilder->setLayout('ajax');
        $action = Inflector::underscore($request->getParam('action'));
        return $controller->render(
            $viewBuilder->getTemplate() ?: 'csv/'.$action
        );
    }

    /**
     * Donne le filename du csv sous la forme
     * "YYYY-MM-DD_HHMMSS_NomActionFrançisé.csv"
     * @return string
     */
    private function getCsvFilename()
    {
        $request = $this->getController()->getRequest();
        $traductionHelper = new TranslateHelper(new View);
        $trad = Inflector::camelize(
            $traductionHelper->permission(
                $request->getParam('controller'),
                $request->getParam('action') === 'index'
                    ? null
                    : $request->getParam('action')
            )
        );
        return sprintf(
            '%s_%s.csv',
            (new DateTime)->format('Y-m-d_His'),
            $this->getConfig('csv_filename', $trad)
        );
    }

    /**
     * Ajoute, pour l'export CSV, la clause order by comme le fait Paginator
     * @return void
     */
    public function addPaginationSort()
    {
        if (empty($this->query)) {
            return;
        }
        $controller = $this->getController();
        $request = $this->getController()->getRequest();
        $sort = $request->getQuery('sort');
        if (!empty($sort)) {
            if ($sortable = Hash::get($controller->paginate, "sortable.$sort")) {
                $sort = $sortable;
            }
            $direction = $request->getQuery('direction', 'asc');
            $order = [$sort => $direction];
        } else {
            $order = Hash::get($controller->paginate, 'order');
        }
        $nOrder = [];
        foreach ($order as $field => $value) {
            if (strpos($field, '.') === false) {
                $field = sprintf('%s.%s', $this->query->getRepository()->getAlias(), $field);
            }
            $nOrder[$field] = $value;
        }
        $this->query->order($nOrder);
    }
}
