<?php
/**
 * AsalaeCore\Controller\Component\ModalComponent
 */

namespace AsalaeCore\Controller\Component;

use AsalaeCore\Http\Response;
use Cake\Controller\Component;
use Cake\Http\Response as CakeResponse;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Table;
use Psr\Http\Message\MessageInterface;

/**
 * Component d'un controller utilisant des modales
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017-2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ModalComponent extends Component
{

    /**
     * Envoi un code HTTP X-Asalae-Success pour les modal de type formulaire
     * @param bool $success si faux, renvoi sur fail()
     * @return CakeResponse|MessageInterface
     */
    public function success(bool $success = true)
    {
        $response = $this->getController()
            ->getResponse()
            ->withHeader('X-Asalae-Success', $success ? 'true' : 'false');
        /** @noinspection PhpPossiblePolymorphicInvocationInspection */
        $response->success = $success;
        $this->getController()->setResponse($response);
        return $response;
    }

    /**
     * Envoi un code HTTP X-Asalae-Success pour les modal de type formulaire
     * @return CakeResponse|MessageInterface
     */
    public function fail()
    {
        return $this->success(false);
    }

    /**
     * @var bool Valeur de retour du dernier $model->save()
     */
    private $lastSaveIsSuccess = false;

    /**
     * Getter de $lastSaveIsSuccess
     * @return bool
     */
    public function lastSaveIsSuccess(): bool
    {
        return $this->lastSaveIsSuccess;
    }

    /**
     * Lance la sauvegarde et modifie la Response en conséquence (json de l'entité on-success)
     * @param Table           $model
     * @param EntityInterface $entity
     * @param array           $data
     * @param array           $saveParams
     * @param callable|null   $callback
     * @return CakeResponse|Response
     */
    public function save(
        Table $model,
        EntityInterface $entity,
        array $data = [],
        array $saveParams = [],
        callable $callback = null
    ) {
        /** @var Response $response */
        $response = $this->getController()->getResponse();
        $model->patchEntity($entity, $data);
        $this->lastSaveIsSuccess = (bool)$model->save($entity, $saveParams);
        $response->success = $this->lastSaveIsSuccess;
        if ($this->lastSaveIsSuccess) {
            $body = $response->getBody();
            $output = $callback
                ? call_user_func($callback, $entity, $model)
                : $entity->toArray();
            $body->write(json_encode($output));
            $response = $response->withBody($body)
                ->withType('json')
                ->withHeader('X-Asalae-Success', 'true');
            $this->getController()->disableAutoRender()->setResponse($response);
        } else {
            $response = $this->fail();
        }
        return $response;
    }

    /**
     * La modale se ferme pour en ouvrir une nouvelle
     * @param string $fn
     * @return CakeResponse|MessageInterface
     */
    public function step(string $fn)
    {
        $response = $this->getController()
            ->getResponse()
            ->withHeader('X-Asalae-Step', $fn);
        $this->getController()->setResponse($response);
        return $response;
    }
}
