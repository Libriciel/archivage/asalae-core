<?php
/**
 * AsalaeCore\Controller\Component\JstableComponent
 */

namespace AsalaeCore\Controller\Component;

use AsalaeCore\Controller\Controller;
use AsalaeCore\Exception\GenericException;
use Authorization\Identity;
use Cake\Controller\Component;
use Cake\Core\Configure;
use Cake\Utility\Hash;
use Cake\Utility\Inflector;
use Cake\ORM\TableRegistry;

/**
 * Component d'un controller utilisant des tables js (asalae.table-generator.js)
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class JstableComponent extends Component
{

    /**
     * Permet de modifier le nombre d'enregistrements affichés
     * dans la configuration du tableau
     *
     * @param string $idTable
     */
    public function configurablePagination(string $idTable)
    {
        $cookie = 'table-generator-config-table-'.$idTable.'-max-per-page';
        $controller = $this->getController();
        $c = $controller->getRequest()->getCookie($cookie);
        if ($c && is_numeric($c)) {
            $controller->paginate['limit'] = (int)$c;
        } elseif (property_exists($controller, 'cookies')) {
            $c = Hash::get($controller->cookies, $cookie);
            if ($c && is_numeric($c)) {
                $controller->paginate['limit'] = (int)$c;
            }
        }
    }

    /**
     * Sauvegarde en base de données les cookies de l'utilisateur
     */
    public function saveCookies()
    {
        /** @var Controller $controller */
        $controller = $this->getController();
        $request = $controller->getRequest();
        /** @var Identity $identity */
        $identity = $request->getAttribute('identity');
        $user = $identity ? $identity->getOriginalData() : null;
        $user_id = Hash::get($user ?: [], 'id');
        if ($user_id) {
            /** @var CookieComponent $CookieComponent */
            $CookieComponent = $controller->loadComponent(
                'AsalaeCore.Cookie',
                ['encryption' => false]
            );
            $cookies = $request->getCookieParams();
            unset($cookies['PHPSESSID'], $cookies['XDEBUG_SESSION']);

            $Users = TableRegistry::getTableLocator()->get('Users');
            $entity = $Users->find()->where(['id' => $user_id])->first();
            $oldCookies = $entity ? (array)json_decode($entity->get('cookies'), true) : [];
            $newCookies = array_filter(
                array_merge($oldCookies, $cookies),
                function ($v) {
                    if (in_array(strtolower($v), ['null', 'false', 'undefined', '0', '0n', 'nan'])) {
                        return false;
                    }
                    return $v;
                }
            );

            $json = json_encode($newCookies);
            if ($newCookies && $json === false) {
                dforcelog($oldCookies, $cookies);
                if (Configure::read('Cookies.enable_errors')) {
                    return;
                }
                throw new GenericException(__("Erreur lors de la sauvegarde des cookies"));
            }
            if ($entity && $entity->get('cookies') !== $json) {
                $Users->patchEntity($entity, ['cookies' => $json]);
                $Users->save($entity);
            }
            $controller->cookies = $newCookies;
            if ($request->getHeaderLine('Accept') !== 'application/json') {
                $controller->set('cookies', $json);
            }
            foreach (array_keys($cookies) as $cookie) {
                $CookieComponent->delete($cookie);
            }
            if ($xdebug = $request->getQuery('XDEBUG_SESSION_START')) {
                $CookieComponent->write('XDEBUG_SESSION', $xdebug);
            }
        }
    }

    /**
     * Permet d'utiliser la vue ajax_{$templateName}.ctp plutôt que de recharger
     * toute la page lors d'un appel ajax
     */
    public function ajaxPagination()
    {
        $request = $this->getController()->getRequest();
        $viewBuilder = $this->getController()->viewBuilder();
        if ($request->is('ajax') && $viewBuilder->getLayout() === null) {
            $template = Inflector::underscore($request->getParam('action'));
            $viewBuilder->setTemplate('ajax_'.$template);
        }
    }
}
