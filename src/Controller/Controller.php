<?php
/**
 * AsalaeCore\Controller\Controller
 */

namespace AsalaeCore\Controller;

use AsalaeCore\Controller\Component\AjaxPaginatorComponent;
use AsalaeCore\Controller\Component\AuthorizationComponent;
use AsalaeCore\Controller\Component\ConditionComponent;
use AsalaeCore\Controller\Component\CookieComponent;
use AsalaeCore\Controller\Component\FilterComponent;
use AsalaeCore\Controller\Component\IndexComponent;
use AsalaeCore\Controller\Component\JstableComponent;
use AsalaeCore\Controller\Component\ModalComponent;
use AsalaeCore\Controller\Component\ReaderComponent;
use AsalaeCore\Controller\Component\RestComponent;
use AsalaeCore\Datasource\Paginator;
use AsalaeCore\Error\AppErrorHandler;
use AsalaeCore\Http\AppServerRequest;
use AsalaeCore\Http\Response as CoreResponse;
use AsalaeCore\Model\Table\FiltersTable;
use AsalaeCore\Model\Table\SavedFiltersTable;
use Authentication\Controller\Component\AuthenticationComponent;
use Cake\Controller\ComponentRegistry;
use Cake\Controller\Controller as CakeController;
use Cake\Core\Configure;
use Cake\Datasource\ResultSetInterface;
use Cake\Error\Debugger;
use Cake\Error\ErrorTrap;
use Cake\Error\PhpError;
use Cake\Event\EventManagerInterface;
use Cake\Http\Response as CakeResponse;
use Cake\Http\ServerRequest;
use Cake\Log\Log;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Exception;
use Psr\Http\Message\ResponseInterface;
use RuntimeException;

/**
 * CoreController
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property AjaxPaginatorComponent AjaxPaginator
 * @property AuthenticationComponent Authentication
 * @property AuthorizationComponent Authorization
 * @property AppServerRequest request
 * @property ConditionComponent Condition
 * @property CookieComponent Cookie
 * @property FilterComponent $Filter
 * @property FiltersTable $Filters
 * @property IndexComponent Index
 * @property JstableComponent Jstable
 * @property ModalComponent $Modal
 * @property Paginator Paginator
 * @property ReaderComponent Reader
 * @property RestComponent Rest
 * @property SavedFiltersTable $SavedFilters
 */
class Controller extends CakeController
{
    /**
     * @var array
     */
    public $cookies = [];

    /**
     * Constructor.
     *
     * Sets a number of properties based on conventions if they are empty. To override the
     * conventions CakePHP uses you can define properties in your class declaration.
     *
     * @param ServerRequest|null         $request      Request object for this controller.
     *                                                 Can be null for testing, but expect
     *                                                 that features that use the request
     *                                                 parameters will not work.
     * @param CakeResponse|null          $response     Response object for this controller.
     * @param string|null                $name         Override the name useful in
     *                                                 testing when using mocks.
     * @param EventManagerInterface|null $eventManager The event manager. Defaults to a new instance.
     * @param ComponentRegistry|null     $components   The component registry. Defaults to a new instance.
     */
    public function __construct(
        ?ServerRequest $request = null,
        ?CakeResponse $response = null,
        ?string $name = null,
        ?EventManagerInterface $eventManager = null,
        ?ComponentRegistry $components = null
    ) {
        $response = $response ?: new CoreResponse;
        parent::__construct($request, $response, $name, $eventManager, $components);
    }

    /**
     * Donne les informations sur l'api du controller (actions => accès)
     * @return array
     */
    public static function getApiActions(): array
    {
        return ['default'];
    }

    /**
     * Handles pagination of records in Table objects.
     *
     * Will load the referenced Table object, and have the PaginatorComponent
     * paginate the query using the request date and settings defined in `$this->paginate`.
     *
     * This method will also make the PaginatorHelper available in the view.
     *
     * @param Table|string|Query|null $object   Table to paginate (e.g: Table
     *                                          instance, 'TableName' or a
     *                                          Query object)
     * @param array                   $settings The settings/configuration used for pagination.
     * @return ResultSetInterface Query results
     * @throws Exception When no compatible table object can be found.
     * @link https://book.cakephp.org/3/en/controllers.html#paginating-a-model
     */
    public function paginate($object = null, array $settings = [])
    {
        if (is_object($object)) {
            $table = $object;
        }

        if (is_string($object) || $object === null) {
            $try = [$object, $this->defaultTable];
            foreach ($try as $tableName) {
                if (empty($tableName)) {
                    continue;
                }
                $table = $this->fetchTable($tableName);
                break;
            }
        }

        if (empty($table)) {
            throw new RuntimeException('Unable to locate an object compatible with paginate.');
        }
        $settings += $this->paginate;
        $settings += [
            'request' => $this->getRequest(),
        ];

        $paginator = new Paginator;
        $results = $paginator->paginate($table, $this->request->getQueryParams(), $settings);
        $paging = $paginator->getPagingParams() + (array)$this->request->getAttribute('paging', []);
        $this->request = $this->request->withAttribute('paging', $paging);
        
        return $results;
    }

    /**
     * Perform the various shutdown processes for this controller.
     * Fire the Components and Controller callbacks in the correct order.
     *
     * - triggers the component `shutdown` callback.
     * - calls the Controller's `afterFilter` method.
     *
     * @return \Psr\Http\Message\ResponseInterface|null
     */
    public function shutdownProcess(): ?ResponseInterface
    {
        $request = $this->getRequest();
        if ($request->is('json') && ($errors = AppErrorHandler::getErrors())) {
            // supprime le contexte qui peut causer des problèmes de mémoire
            foreach ($errors as $key => $values) {
                $values['context'] = null;
                $errors[$key] = $values;
            }
            AppErrorHandler::enableErrorOutput();
            $response = $this->getResponse();
            $body = $response->getBody();
            $body->rewind();
            $content = $body->getContents();
            $json = json_decode($content, true);
            if ($json && is_array($json)) {
                $json['php_error'] = $errors;
                $body->rewind();
                $body->write(json_encode($json));
                return $response;
            } elseif (!$json) {
                $debug = Configure::read('debug');
                $errorTrap = new ErrorTrap;
                $renderer = $errorTrap->renderer();
                foreach ($errors as $error) {
                    $error = new PhpError(
                        $error['code'],
                        $error['description'],
                        $error['file'] ?? null,
                        $error['line'] ?? null,
                        $error['trace'] ?? []
                    );
                    $renderer->write($renderer->render($error, $debug));
                }
            } else {
                Log::error(Debugger::exportVar($errors));
            }
        }
        return parent::shutdownProcess();
    }
}
