<?php
/**
 * AsalaeCore\Controller\ApiTrait
 * @noinspection PhpDeprecationInspection - AuthComponent
 */

namespace AsalaeCore\Controller;

use AsalaeCore\Controller\Component\RestComponent;
use AsalaeCore\Http\AppServerRequest;
use Cake\Controller\Component\AuthComponent;
use Cake\Datasource\EntityInterface;
use Cake\Datasource\ResultSetInterface;
use Cake\Http\Response;
use Cake\Http\Exception\BadRequestException;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\MethodNotAllowedException;
use Cake\Http\Exception\NotAcceptableException;
use Cake\Http\Exception\NotFoundException;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Exception;

/**
 * Trait d'un controller api
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property AppServerRequest $request
 * @property Response $response
 * @property RestComponent $Rest
 * @property AuthComponent $Auth
 * @mixin Controller
 */
trait ApiTrait
{
    /**
     * Permet d'accèder à l'api rest du controller
     * @param string $id
     * @return Response|null
     * @throws Exception
     * @todo 405 Method Not Allowed - si l'action existe mais la méthode n'a pas de chemin
     */
    public function api(string $id = '')
    {
        $request = $this->getRequest();
        $this->loadComponent('AsalaeCore.Rest');
        $method = strtolower($request->getEnv('REQUEST_METHOD'));
        $action = $id ?: 'index';
        if (is_numeric($action)) {
            $action = 'index';
        }
        $route = $this->Rest->route();
        $seal = $route['seal'] ?? null;
        if (isset($route['callback'][1]) && $route['callback'][1] === 'default') {
            $route = null;
        } elseif ($route) {
            $this->viewBuilder()->setTemplate($action);
            $response = call_user_func_array($route['callback'], $route['args']);
            if ($response) {
                return $response;
            }
            return $this->render();
        }

        // Vérifications d'usage
        $accepts = $this->apiCheck($method, $id);

        // Récupération des données
        $controller = $request->getParam('controller');
        $Model = $this->fetchTable($controller);
        $results = [];
        if ($request->is(['get', 'put', 'delete'])) {
            if ($seal && isset($this->Seal) && method_exists($this->Seal, $seal)) {
                $query = $this->Seal->$seal()->$controller->find('seal');
            } else {
                $query = $Model->find();
            }
            $filters = $request->getQuery('filters');
            if ($filters) {
                $this->applyFilters($filters, $query);
            }
            if ($id) {
                $query->where([$Model->getAlias() . '.id' => $id]);
                $results = $query->first();
            } else {
                $page = (int)$request->getQuery('page', 1);
                $limit = min(
                    (int)$request->getQuery('limit', $this->paginate['limit'] ?? 100),
                    1000
                );
                $count = $query->count();
                $offset = ($page -1) * $limit;
                $sort = $request->getQuery('sort', $Model->getAlias() . '.id');
                $direction = $request->getQuery('direction', 'asc');
                $results = [
                    'pagination' => compact(
                        'page',
                        'limit',
                        'count',
                        'sort',
                        'direction'
                    )
                ];
                if (filter_var($request->getQuery('raw'), FILTER_VALIDATE_BOOL)) {
                    $results = [];
                }
                $results += $query->order([$sort => $direction])
                    ->limit($limit)
                    ->offset($offset)
                    ->toArray();
            }
        }
        $this->apiCheckData($method, $results, $id);

        // Actions et résultats
        $functionName = 'api'.ucfirst($method).'Action';
        $output = $this->$functionName($Model, $results, $id);

        // Rendu
        return RenderDataTrait::staticRenderByAccepts($this, $accepts, $output);
    }

    /**
     * Vérifications du formatage de la requête
     * @param string $method
     * @param string $id
     * @return array
     * @throws MethodNotAllowedException
     * @throws BadRequestException
     * @throws NotAcceptableException
     */
    protected function apiCheck(string $method, string $id): array
    {
        if (!in_array($method, ['get', 'put', 'post', 'delete'])) {
            throw new MethodNotAllowedException(__("405 Method Not Allowed"));
        }
        if ((in_array($method, ['put', 'delete']) && empty($id))
            || ($method === 'post' && !empty($id))
        ) {
            throw new BadRequestException(__("400 Bad Request"));
        }
        $acceptable = ['json', 'xml', 'html', 'xhtml'];
        $accepts = current($this->getResponse()->mapType($this->getRequest()->parseAccept()));
        $intersect = array_intersect($accepts ?: [], $acceptable);
        if ($accepts && !$intersect) {
            throw new NotAcceptableException(__("406 Not Acceptable"));
        }
        return $intersect;
    }

    /**
     * Vérifications lié à la présence de data
     * @param string                          $method
     * @param array|ResultSetInterface|Entity $results
     * @param string                          $id
     * @throws NotFoundException
     * @throws BadRequestException
     */
    protected function apiCheckData(string $method, $results, $id)
    {
        if (empty($results)
            && (in_array($method, ['put', 'delete'])
            || ($method === 'get' && !empty($id)))
        ) {
            throw new NotFoundException(__("404 Not Found"));
        }
        if (empty($this->getRequest()->getData()) && in_array($method, ['put', 'post'])) {
            throw new BadRequestException(__("400 Bad Request"));
        }
    }

    /**
     * Action get
     * @param Table                           $Model
     * @param array|ResultSetInterface|Entity $results
     * @param string                          $id
     * @return array
     */
    protected function apiGetAction(/** @noinspection PhpUnusedParameterInspection */
        Table $Model,
        $results,
        $id = ''
    ): array {
        return $results instanceof EntityInterface ? $results->toArray() : $results;
    }

    /**
     * Action put
     * @param Table                           $Model
     * @param array|ResultSetInterface|Entity $results
     * @param string                          $id
     * @return array
     */
    protected function apiPutAction(/** @noinspection PhpUnusedParameterInspection */
        Table $Model,
        $results,
        $id = ''
    ): array {
        return $this->apiSave($Model, $results);
    }

    /**
     * Logique de sauvegarde commune entre put et post
     * @param Table  $Model
     * @param Entity $entity
     * @return array
     */
    protected function apiSave(Table $Model, Entity $entity): array
    {
        $Model->patchEntity($entity, $this->getRequest()->getData());
        if ($Model->save($entity)) {
            $output['success'] = true;
        } else {
            $output['success'] = false;
            $output['errors'] = $entity->getErrors();
        }
        $output['id'] = $entity->get('id');
        return $output;
    }

    /**
     * Action post
     * @param Table                           $Model
     * @param array|ResultSetInterface|Entity $results
     * @param string                          $id
     * @return array
     */
    protected function apiPostAction(/** @noinspection PhpUnusedParameterInspection */
        Table $Model,
        $results,
        $id = ''
    ): array {
        /* @var Entity $entity */
        $entity = $Model->newEntity([], ['validate' => false]);
        return $this->apiSave($Model, $entity);
    }

    /**
     * Action delete
     * @param Table                           $Model
     * @param array|ResultSetInterface|Entity $results
     * @param string                          $id
     * @return array
     */
    protected function apiDeleteAction(/** @noinspection PhpUnusedParameterInspection */
        Table $Model,
        $results,
        $id = ''
    ): array {
        $output['id'] = $results->get('id');
        if ($Model->delete($results)) {
            $output['success'] = true;
        } else {
            $output['success'] = false;
            $output['errors'] = $results->getErrors();
        }
        return $output;
    }

    /**
     * Définit les actions possibles via l'api
     *
     * exemple :
     * Le controller Users permet d'obtenir les infos d'un utilisateur et
     * possède également une méthode pour réinitialiser son mot de passe :
     *  $this->setApiActions([
     *      'default' => [          // pour https://mon.url/users[/:id]
     *          'get' => [
     *              'params' => ['id' => ['numeric', 'optional']],
     *              'function' => 'apiGet',
     *          ]
     *      ],
     *      'resetPassword' => [    // pour https://mon.url/users/reset-password/15
     *          'put' => [
     *              'function' => 'resetPassword'
     *          ]
     *      ]
     *  ])
     *
     * @param array $actions
     */
    protected function setApiActions(array $actions)
    {
        $this->Rest->setApiActions($actions);
    }

    /** @noinspection PhpUnusedPrivateMethodInspection */
    /**
     * Donne un callback pour interdire une action
     * Utile pour les fonctions non implémentées pour éviter d'accéder à des
     * informations confidentielles (ex: mot de passe), en effet, par défaut, un
     * get permet d'obtenir tous les champs d'une table si les droits le
     * permettent
     *
     * ex:
     *  $this->setApiActions([
     *      'default' => ['*' => ['function' => 'forbidden']]
     * ])
     */
    private function forbidden()
    {
        throw new ForbiddenException(__("403 Forbidden"));
    }

    /**
     * Ajoute les filtres saisie dans le query
     * example: code >= "404"; foo = "tes; \"bar\"; "; msg LIKE "%test%"
     *          donnera ['code >=' => 404, 'foo' => 'tes; "bar"; ', 'msg LIKE' => '%test%']
     * @param string $filters
     * @param Query  $query
     */
    private function applyFilters(string $filters, Query $query)
    {
        foreach ($this->parseFilters($filters) as $field => $value) {
            $query->where([$field => $value]);
        }
    }

    /**
     * Transforme une chaine sous la forme "field operateur valeur ; ..."
     * en conditions pour l'orm ['field operateur' => 'valeur', ...]
     * @param string $filters
     * @return array
     */
    public static function parseFilters(string $filters): array
    {
        $tokens = token_get_all('<?php '.$filters.';');
        $whiteList = [
            ';', '=', '>', '<', T_STRING, T_IS_GREATER_OR_EQUAL, T_IS_NOT_EQUAL,
            T_IS_SMALLER_OR_EQUAL, T_CONSTANT_ENCAPSED_STRING, T_DNUMBER, T_LNUMBER
        ];
        $field = null;
        $operator = null;
        $value = null;
        $conditions = [];
        $prevIsField = false;
        $prevIsDot = false;
        foreach ($tokens as $token) {
            // gestion des noms de champs composés ex: Users.name --------------
            if ($field) {
                if ($token === '.' && $prevIsField) {
                    $field .= '.';
                    $prevIsDot = true;
                    $prevIsField = false;
                    continue;
                } elseif (is_array($token) && $prevIsDot) {
                    $field .= $token[1];
                    $prevIsDot = false;
                    continue;
                }
            }
            $prevIsField = false;
            $prevIsDot = false;
            // -----------------------------------------------------------------

            if ((!is_array($token) && !in_array($token, $whiteList)) || !in_array($token[0], $whiteList)) {
                continue;
            }
            if ($token === ';') {
                if ($field && $operator) {
                    $conditions[$field.' '.$operator] = $value ?: null;
                }
                $field = $operator = null;
                continue;
            }
            if (!$field && is_array($token)) {
                $field = $token[1];
                $prevIsField = true;
            } elseif (!$operator) {
                $operator = is_array($token) ? $token[1] : $token;
            } elseif (is_array($token) && strtoupper($operator) === 'IS' && strtoupper($token[1]) === 'NOT') {
                $operator = 'IS NOT';
            } else {
                if (is_array($token)) {
                    if ($token[0] === T_CONSTANT_ENCAPSED_STRING || in_array($token[1], ['null', 'true', 'false'])) {
                        $v = $token[1];
                        eval('$value = '.$v.';'); // NOSONAR $v vaut forcement null, true ou false (pas de risques)
                    } elseif ($token[0] === T_LNUMBER) {
                        $value = (int)$token[1];
                    } elseif ($token[0] === T_DNUMBER) {
                        $value = (float)$token[1];
                    } else {
                        $value = $token[1];
                    }
                } else {
                    $value = $token;
                }
            }
        }
        return $conditions;
    }
}
