<?php
/**
 * AsalaeCore\Controller\RenderDataTrait
 */

namespace AsalaeCore\Controller;

use AsalaeCore\Http\AppServerRequest;
use AsalaeCore\Http\Response;
use AsalaeCore\Utility\XmlUtility;
use Cake\Collection\CollectionInterface;
use Cake\Controller\Controller;
use Cake\Datasource\EntityInterface;
use Cake\Http\ContentTypeNegotiation;
use Cake\Http\Exception\NotAcceptableException;
use Cake\ORM\Entity;
use Cake\ORM\ResultSet;
use Cake\Http\Response as CakeResponse;
use Cake\Utility\Hash;
use Cake\Utility\Inflector;
use Cake\View\ViewBuilder;
use DateTime;

/**
 * Permet le rendu d'une variable en divers formats (ex: json, xml)
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin Controller
 * @property AppServerRequest $request
 * @property Response $response
 */
trait RenderDataTrait
{
    /**
     * Effectue un rendu de fichier json
     * @param string                     $json
     * @param CakeResponse|Response|null $response
     * @return Response
     */
    protected function renderJson(string $json, CakeResponse $response = null): CakeResponse
    {
        return self::staticRenderJson($this, $json, $response);
    }

    /**
     * Effectue un json_encode sur le data avant de lancer le rendu json
     * @param mixed                      $data
     * @param CakeResponse|Response|null $response
     * @return Response
     */
    protected function renderDataToJson($data, CakeResponse $response = null): CakeResponse
    {
        return $this->renderJson(json_encode($data), $response);
    }

    /**
     * Effectue un rendu de fichier xml
     * @param string                     $xml
     * @param CakeResponse|Response|null $response
     * @return Response
     */
    protected function renderXml(string $xml, CakeResponse $response = null): CakeResponse
    {
        return self::staticRenderXml($this, $xml, $response);
    }

    /**
     * Effectue un rendu de fichier html
     * @param mixed                      $data
     * @param CakeResponse|Response|null $response
     * @return CakeResponse|Response|null
     */
    protected function renderHtml($data, CakeResponse $response = null)
    {
        return self::staticRenderHtml($this, $data, $response);
    }

    /**
     * Effectue le rendu des données selon le(s) accept(s) de la requête
     * @param array                      $accepts
     * @param mixed                      $output
     * @param CakeResponse|Response|null $response
     * @return Response
     */
    protected function renderByAccepts(array $accepts, $output, CakeResponse $response = null): CakeResponse
    {
        return self::staticRenderByAccepts($this, $accepts, $output, $response);
    }

    /**
     * Effectue un rendu selon le accept
     * @param mixed                      $output
     * @param CakeResponse|Response|null $response
     * @return Response
     */
    protected function renderData($output, CakeResponse $response = null): CakeResponse
    {
        return self::staticRenderData($this, $output, $response);
    }

    /**
     * Effectue le rendu des données selon le(s) accept(s) de la requête
     * @param Controller                 $controller
     * @param array                      $accepts
     * @param mixed                      $output
     * @param CakeResponse|Response|null $response
     * @return Response
     */
    public static function staticRenderByAccepts(
        Controller $controller,
        array $accepts,
        $output,
        CakeResponse $response = null
    ): CakeResponse {
        if ($output instanceof ResultSet) {
            $output = $output->toArray();
        }
        if (is_array($output)) {
            $output = array_map(
                function ($v) {
                    if ($v instanceof Entity) {
                        $v = $v->toArray();
                    }
                    return $v;
                },
                $output
            );
        }
        switch (current($accepts)) {
            case 'json':
                return self::staticRenderJson($controller, json_encode($output, JSON_PRETTY_PRINT), $response);
            case 'csv':
                return self::staticRenderCsv(
                    $controller,
                    $output,
                    $response
                );
            case 'xml':
                $output = self::prepareOutputForRenderXml($controller, $output);
                return self::staticRenderXml($controller, XmlUtility::arrayToXmlString($output), $response);
            default:
                return self::staticRenderHtml($controller, $output, $response);
        }
    }

    /**
     * Modifi output pour être compatible avec le rendu xml
     * @param Controller                       $controller
     * @param array|CollectionInterface|string $output
     * @return array
     */
    private static function prepareOutputForRenderXml(Controller $controller, $output): array
    {
        $controllerName = $controller->getRequest()->getParam('controller');
        if ($output instanceof CollectionInterface) {
            $rootForCollection = [];
            /** @var EntityInterface $element */
            foreach ($output as $element) {
                $entityName = Inflector::singularize($element->getSource());
                $rootForCollection[$entityName][] = $element;
            }
            $output = [$controllerName => $rootForCollection];
        }

        if (is_string($output)) {
            $output = ['message' => $output];
        }

        $model = $controller->fetchTable();
        $entity = trim(
            substr($model->getEntityClass(), strrpos($model->getEntityClass(), '\\')),
            '\\'
        );

        if (count($output) > 1) { // créé un noeud "root" si besoin
            if (isset($output[0])) {
                $newOutput = [$controllerName => [$entity => []]];
                foreach ($output as $key => $value) {
                    if (is_numeric($key)) {
                        $newOutput[$controllerName][$entity][] = $value;
                    } else {
                        $newOutput[$controllerName][$key] = $value;
                    }
                }
                $output = $newOutput;
            } else {
                $output = [$controllerName => $output];
            }
        }

        $rootName = array_keys($output)[0];
        $root =& $output[$rootName];
        if (is_array($root) && isset($root[0]) && isset($root[1])) {
            $root = [$entity => $root];
        }
        $output = self::prepareArrayXml($output);
        if (array_keys($output)[0] === 0) {
            $output = [$controllerName => $output];
        }
        return $output;
    }

    /**
     * Effectue un rendu de fichier json
     * @param Controller                 $controller
     * @param string                     $json
     * @param CakeResponse|Response|null $response
     * @return Response
     */
    public static function staticRenderJson(
        Controller $controller,
        string $json,
        CakeResponse $response = null
    ): CakeResponse {
        if ($response === null) {
            $response = $controller->getResponse();
        }
        $body = $response->getBody();
        $body->write($json);
        return $response
            ->withBody($body)
            ->withType('json');
    }

    /**
     * Effectue un rendu de fichier csv
     * @param Controller                 $controller
     * @param array                      $data
     * @param CakeResponse|Response|null $response
     * @return Response
     */
    public static function staticRenderCsv(
        Controller $controller,
        $data,
        CakeResponse $response = null
    ): CakeResponse {
        if ($response === null) {
            $response = $controller->getResponse();
        }
        // Rend $data bidimentionnel ['key1' => ['val1'], 'key2' => ['val2']]
        $nData = [];
        foreach (Hash::flatten(self::prepareArrayXml((array)$data)) as $key => $value) {
            $nData[] = [$key, (string)$value];
        }
        $viewBuilder = new ViewBuilder;
        $csv = $viewBuilder
            ->setClassName('CsvView.Csv')
            ->setVar('data', $nData)
            ->setOptions(['serialize' => 'data'])
            ->build()
            ->render();

        $body = $response->getBody();
        $body->write($csv);
        return $response
            ->withBody($body)
            ->withType('csv');
    }

    /**
     * Effectue un rendu de fichier html
     * @param Controller                 $controller
     * @param mixed                      $data
     * @param CakeResponse|Response|null $response
     * @return CakeResponse|Response|null
     */
    public static function staticRenderHtml(Controller $controller, $data, CakeResponse $response = null)
    {
        if ($data instanceof CollectionInterface) {
            $data = array_map(fn(EntityInterface $e) => $e->toArray(), $data->toArray());
        }
        if ($response === null) {
            $response = $controller->getResponse();
        }
        $View = $controller->viewBuilder();
        $View->setLayout('empty')->setTemplate('/element/export');
        $controller->set(compact('data'));
        $response = $response->withType('html')->withStringBody($controller->render());
        $controller->setResponse($response);
        return $response;
    }

    /**
     * Effectue un rendu de fichier xml
     * @param Controller                 $controller
     * @param string                     $xml
     * @param CakeResponse|Response|null $response
     * @return Response
     */
    public static function staticRenderXml(
        Controller $controller,
        string $xml,
        CakeResponse $response = null
    ): CakeResponse {
        if ($response === null) {
            $response = $controller->getResponse();
        }
        $body = $response->getBody();
        $body->write($xml);
        return $response
            ->withBody($body)
            ->withType('xml');
    }

    /**
     * Effectue un rendu selon le accept
     * @param Controller                 $controller
     * @param mixed                      $output
     * @param CakeResponse|Response|null $response
     * @return Response
     */
    public static function staticRenderData(
        Controller $controller,
        $output,
        CakeResponse $response = null
    ): CakeResponse {
        if ($response === null) {
            $response = $controller->getResponse();
        }
        $acceptable = ['json', 'xml', 'html', 'xhtml', 'csv'];
        /** @var AppServerRequest $request */
        $request = $controller->getRequest();
        $accepts = $response->mapType((new ContentTypeNegotiation())->parseAccept($request));
        $accepts = $accepts ? current($accepts) : [];
        if (count($accepts) === 0 || (count($accepts) === 1 && current($accepts) === 'html') && $request->is('ajax')) {
            $accepts = ['json'];
        }
        $intersect = array_intersect($accepts, $acceptable);
        if (!$intersect) {
            throw new NotAcceptableException(__("406 Not Acceptable"));
        }
        return self::staticRenderByAccepts($controller, $intersect, $output, $response);
    }

    /**
     * Evite les erreurs Craur car il n'arrive pas à parser en string
     * @param array $data
     * @return array
     */
    private static function prepareArrayXml(array $data): array
    {
        $output = [];
        foreach ($data as $key => $value) {
            if (is_array($value)) {
                $output[$key] = self::prepareArrayXml($value);
            } elseif (is_resource($value)) {
                $output[$key] = '';
                while (!feof($value)) {
                    $output[$key] .= fread($value, 4096);
                }
            } elseif ($value instanceof EntityInterface) {
                $output[$key] = self::prepareArrayXml($value->toArray());
            } elseif (is_object($value)) {
                if (method_exists($value, '__toString')) {
                    $output[$key] = (string)$value;
                } elseif ($value instanceof DateTime) {
                    $output[$key] = json_decode(json_encode($value), true);
                } else {
                    $output[$key] = serialize($value);
                }
            } else {
                $output[$key] = $value;
            }
        }
        return $output;
    }
}
