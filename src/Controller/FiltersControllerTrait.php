<?php /** @noinspection PhpDeprecationInspection */
/**
 * AsalaeCore\Controller\FiltersControllerTrait
 */

namespace AsalaeCore\Controller;

use AsalaeCore\Controller\Component\FilterComponent;
use AsalaeCore\Controller\Component\ModalComponent;
use AsalaeCore\Model\Entity\Filter;
use AsalaeCore\Model\Table\FiltersTable;
use AsalaeCore\Model\Table\SavedFiltersTable;
use AsalaeCore\Utility\FormatError;
use Cake\Controller\Component\AuthComponent;
use Cake\Http\Response;
use Cake\Utility\Hash;
use Exception;
use Cake\View\Helper\UrlHelper;
use Cake\View\View;

/**
 * Filtres de rechercher
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property SavedFiltersTable $SavedFilters
 * @property FiltersTable $Filters
 * @property AuthComponent $Auth
 * @property ModalComponent $Modal
 * @property FilterComponent $Filter
 */
trait FiltersControllerTrait
{
    /**
     * Permet de rediriger vers l'url construite à partir des filtres sauvegardés
     * @param string $id
     * @return Response|null
     * @throws Exception
     */
    public function ajaxRedirectUrl(string $id)
    {
        $this->getRequest()->allowMethod('ajax');
        return $this->redirect($this->getUrlById($id));
    }

    /**
     * Permet de récupérer une url à partir des filtres sauvegardé
     * @param string $id
     * @return string
     * @throws Exception
     */
    private function getUrlById(string $id): string
    {
        $this->SavedFilters = $this->fetchTable('SavedFilters');
        $save = $this->SavedFilters->find()
            ->where(
                [
                    'SavedFilters.id' => $id,
                    'user_id' => $this->Authentication->getIdentityData('id')
                ]
            )
            ->contain(['Filters'])
            ->first();

        if (!$save) {
            throw new Exception(__("Valeur non trouvée"));
        }

        $UrlHelper = new UrlHelper(new View());
        $params = [
            'controller' => $save->get('controller'),
            'action' => $save->get('action'),
            '?' => ["SaveFilterSelect" => $id]
        ];
        /** @var Filter $filter */
        foreach ($save->get('filters') as $filter) {
            $params['?'][$filter->get('key')] = $filter->get('value');
        }

        return $UrlHelper->build($params, ['escape' => false, 'fullBase' => true]);
    }

    /**
     * Permet de supprimer une sauvegarde de filtre de recherche
     * @param string $id
     * @throws Exception
     */
    public function ajaxDeleteSave(string $id)
    {
        $this->getRequest()->allowMethod('ajax');
        $this->SavedFilters = $this->fetchTable('SavedFilters');

        $viewBuild = $this->viewBuilder();
        $viewBuild->setClassName('Json');

        $save = $this->SavedFilters->find()
            ->where(
                [
                    'SavedFilters.id' => $id,
                    'user_id' => $this->Authentication->getIdentityData('id')
                ]
            )
            ->contain(['Filters'])
            ->first();
        if (empty($save)) {
            throw new Exception(__("La sauvegarde du filtre n'a pas été trouvée"));
        }
        $success = $this->SavedFilters->delete($save);
        $this->loadComponent('AsalaeCore.Modal');
        if ($success) {
            $this->Modal->success();
        } else {
            $this->Modal->fail();
            FormatError::logEntityErrors($save);
        }

        $this->set('success', $success);
        $this->set('_serialize', ['success']);
    }

    /**
     * @param string $id
     * @throws Exception
     */
    public function ajaxGetFilters(string $id)
    {
        $this->getRequest()->allowMethod('ajax');
        $this->SavedFilters = $this->fetchTable('SavedFilters');
        $save = $this->SavedFilters->find()
            ->where(
                [
                    'SavedFilters.id' => $id,
                    'user_id' => $this->Authentication->getIdentityData('id')
                ]
            )
            ->contain(['Filters'])
            ->first();

        if (!$save) {
            throw new Exception("Valeur non trouvée");
        }

        $this->set('savedFilters', $save);
        $this->set('helperConfig', json_decode($this->getRequest()->getData('helper'), true));
    }

    /**
     * Enregistre un jeu de filtres
     */
    public function ajaxNewSave()
    {
        $this->getRequest()->allowMethod('ajax');
        $this->getRequest()->allowMethod('post');
        $this->SavedFilters = $this->fetchTable('SavedFilters');

        $this->loadComponent('AsalaeCore.Modal');
        $this->loadComponent('AsalaeCore.Filter');
        $data = $this->getRequest()->getData();
        $name = $data['savename'];
        $controller = $data['controller'];
        $action = $data['action'];
        $filters = $this->Filter->dataToFilters(array_filter($data, 'is_array'));
        if (empty($filters)) {
            $this->Modal->fail();
            $report = "Empty filters";
        } else {
            $entity = $this->SavedFilters->newEntity(
                compact('name', 'controller', 'action') + [
                    'user_id' => $this->Authentication->getIdentityData('id'),
                    'filters' => $filters
                ],
                ['associated' => 'Filters']
            );
            if ($this->SavedFilters->save($entity)) {
                $this->Modal->success();
                $report = $entity->toArray();
            } else {
                $this->Modal->fail();
                $report = $entity->getErrors();
                FormatError::logEntityErrors($entity);
            }
        }
        return RenderDataTrait::staticRenderData($this, $report);
    }

    /**
     * Remplace les filtres d'une sauvegarde
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function ajaxOverwrite(string $id)
    {
        $this->getRequest()->allowMethod('ajax');
        $this->getRequest()->allowMethod('post');
        $this->SavedFilters = $this->fetchTable('SavedFilters');
        $this->Filters = $this->fetchTable('Filters');

        $this->loadComponent('AsalaeCore.Modal');
        $this->loadComponent('AsalaeCore.Filter');
        $data = $this->getRequest()->getData();
        $filters = $this->Filter->dataToFilters(array_filter($data, 'is_array'));

        if (empty($filters)) {
            $this->Modal->fail();
            $report = "Empty filters";
        } else {
            $entity = $this->SavedFilters->find()
                ->where(['id' => $id, 'user_id' => $this->Authentication->getIdentityData('id')])
                ->contain(['Filters'])
                ->firstOrFail();
            $this->Filters->deleteAll(
                [
                    'id IN' => Hash::extract($entity->get('filters'), '{n}.id')
                ]
            );
            $this->SavedFilters->patchEntity($entity, compact('filters'));
            if ($this->SavedFilters->save($entity, ['associated' => 'Filters'])) {
                $this->Modal->success();
                $report = $entity->toArray();
            } else {
                $this->Modal->fail();
                $report = $entity->getErrors();
                FormatError::logEntityErrors($entity);
            }
        }
        return RenderDataTrait::staticRenderData($this, $report);
    }
}
