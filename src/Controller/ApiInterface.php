<?php
/**
 * AsalaeCore\Controller\ApiInterface
 */

namespace AsalaeCore\Controller;

use Cake\Http\Response;

/**
 * Interface d'un controller api
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
interface ApiInterface
{
    /**
     * Permet d'accèder à l'api rest du controller
     * @param string $id
     * @return Response|null
     */
    public function api(string $id = '');

    /**
     * Donne la liste des actions API
     * @return array
     */
    public static function getApiActions();
}
