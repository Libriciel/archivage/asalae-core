<?php
/**
 * AsalaeCore\MinkSuite\SetupMinkInterface
 */

namespace AsalaeCore\MinkSuite;

/**
 * Ajoute la possibilité d'ajouter un setUp() et tearDown()
 *
 * @category TestSuite
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2022, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
interface SetupMinkInterface
{
    /**
     * Lancé avant un test
     */
    public function setUp(): void;

    /**
     * Lancé après un test
     */
    public function tearDown(): void;

    /**
     * Lancé avant la serie de tests
     */
    public function build(): void;

    /**
     * Lancé après la serie de tests
     */
    public function destroy(): void;
}
