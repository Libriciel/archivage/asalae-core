<?php
/**
 * AsalaeCore\MinkSuite\MinkCaseException
 */

namespace AsalaeCore\MinkSuite;

use Exception;

/**
 * Exception lors d'un test fonctionnel
 *
 * @category TestSuite
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2022, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class MinkCaseException extends Exception
{
}
