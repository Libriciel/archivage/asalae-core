<?php
/**
 * AsalaeCore\MinkSuite\MinkBeanstalkTrait
 */

namespace AsalaeCore\MinkSuite;

use Beanstalk\Model\Table\BeanstalkJobsTable;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use StateMachine\Model\Behavior\StateMachineBehavior;

/**
 * Initialise les tests mink avec des workers
 *
 * @category TestSuite
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2022, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
trait MinkBeanstalkTrait
{
    /**
     * @var bool
     */
    public bool $beanstalkInitialized = false;

    /**
     * Lancé avant la serie de tests
     */
    public function build(): void
    {
        MinkBeanstalk::init($this->workers ?? []);
        $this->beanstalkInitialized = true;
    }

    /**
     * Lancé après la serie de tests
     */
    public function destroy(): void
    {
        TableRegistry::getTableLocator()->get('Notifications')->deleteAll([]);
        MinkBeanstalk::destroy();
    }

    /**
     * Attends que le job se termine
     * @param string $tube
     * @param string $subject
     * @param int    $foreignKey
     * @param float  $duration
     * @return bool
     */
    public function waitJobFinish(string $tube, string $subject, int $foreignKey, float $duration = 5.0): bool
    {
        $BeanstalkJobs = TableRegistry::getTableLocator()->get('BeanstalkJobs');
        $time = 0;
        do {
            $job = $BeanstalkJobs->find()
                ->where(['tube' => $tube, 'object_model' => $subject, 'object_foreign_key' => $foreignKey])
                ->first();
            if (!$job) {
                return true;
            } elseif ($job->get('job_state') === BeanstalkJobsTable::S_FAILED) {
                return false;
            }
            $time += 0.01;
            usleep(10000);
        } while ($time < $duration);
        trigger_error(
            sprintf(
                'waitJobFinish timeout occured: tube %s subject %s (id=%d) state: %s',
                $tube,
                $subject,
                $foreignKey,
                $job->get('job_state')
            )
        );
        return false;
    }

    /**
     * Equivalent du waitJobFinish mais adapté à Beanstalk v4
     * @param Table  $Model
     * @param int    $id
     * @param string $state
     * @param float  $duration
     * @return bool
     */
    public function waitState(Table $Model, int $id, string $state, float $duration = 5.0): bool
    {
        if (!$Model->hasBehavior('StateMachine')) {
            return false;
        }
        /** @var StateMachineBehavior $stateMachine */
        $stateMachine = $Model->getBehavior('StateMachine');
        $field = $stateMachine->getConfig('fields.state');
        $time = 0;
        do {
            $current = $Model->find()
                ->select([$field])
                ->where([$Model->getPrimaryKey() => $id])
                ->disableHydration()
                ->first();
            if ($current && $current[$field] === $state) {
                return true;
            }
            $time += 0.01;
            usleep(10000);
        } while ($time < $duration);
        if ($current) {
            trigger_error(
                sprintf(
                    'waitState timeout occured: Model %s (id=%d) current state: %s ; target state: %s',
                    $Model->getAlias(),
                    $id,
                    $current[$field],
                    $state
                )
            );
        } else {
            trigger_error(
                sprintf(
                    'waitState timeout occured: Model %s (id=%d) was not found',
                    $Model->getAlias(),
                    $id
                )
            );
        }
        return false;
    }
}
