<?php

/**
 * AsalaeCore\MinkSuite\MinkCaseDomTrait
 */

namespace AsalaeCore\MinkSuite;

use AsalaeCore\Utility\DOMUtility;
use DOMElement;
use Exception;

/**
 * Manipulations de fichiers xml
 *
 * @category TestSuite
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2023, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin MinkCase
 */
trait MinkCaseDomTrait
{
    /**
     * @var DOMUtility instance active
     */
    protected DOMUtility $domInstance;

    /**
     * Charge un fichier xml à partir d'un URI
     * @param string $path
     * @return DOMUtility
     * @throws Exception
     */
    protected function domLoad(string $path): DOMUtility
    {
        $this->domInstance = DOMUtility::load($path);
        return $this->domInstance;
    }

    /**
     * Charge un fichier xml à partir d'un string
     * @param string $xml
     * @return DOMUtility
     * @throws Exception
     */
    protected function domLoadXML(string $xml): DOMUtility
    {
        $this->domInstance = DOMUtility::loadXML($xml);
        return $this->domInstance;
    }

    /**
     * Donne la 1ère valeur dans $xpath
     * @param string $xpath
     * @return string|null
     */
    protected function domGetValue(string $xpath): ?string
    {
        return $this->domInstance->nodeValue($xpath);
    }

    /**
     * Donne toutes les valeurs dans $xpath
     * @param string $xpath
     * @return string[]
     */
    protected function domGetValues(string $xpath): array
    {
        $nodes = $this->domInstance->xpath->query($xpath);
        $result = [];
        /** @var DOMElement $node */
        foreach ($nodes as $node) {
            $result[] = $node->nodeValue;
        }
        return $result;
    }
}
