<?php
/**
 * AsalaeCore\MinkSuite\MinkCaseInterface
 */

namespace AsalaeCore\MinkSuite;

use Behat\Mink\Driver\DriverInterface;
use Behat\Mink\Session;
use DMore\ChromeDriver\ChromePage;
use Exception;
use Psy\Shell as PsyShell;
use ReflectionClass;
use ReflectionException;
use WebSocket\Client;

/**
 * Permet l'accès aux méthodes protected d'une instance MinkCase pour la console
 *
 * @category TestSuite
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2022, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @method void doLogin(string $username = 'admin', string $password = 'admin')
 */
class MinkConsole
{
    /**
     * @var MinkCase
     */
    public MinkCase $mink;
    /**
     * @var DriverInterface
     */
    public DriverInterface $driver;
    /**
     * @var ChromeBrowser
     */
    public ChromeBrowser $browser;
    /**
     * @var ChromePage
     */
    public ChromePage $page;
    /**
     * @var Client
     */
    public Client $client;
    /**
     * @var Session
     */
    public Session $session;
    /**
     * @var ReflectionClass
     */
    private ReflectionClass $reflection;

    /**
     * Constructor
     * @param MinkCase $mink
     * @throws ReflectionException
     */
    public function __construct(MinkCase $mink)
    {
        $this->mink = $mink;
        $this->reflection = new ReflectionClass(get_class($mink));
        $driver = $this->reflection->getProperty('driver');
        $driver->setAccessible(true);
        $this->driver = $driver->getValue($mink);
        $session = $this->reflection->getProperty('session');
        $session->setAccessible(true);
        $this->session = $session->getValue($mink);
    }

    /**
     * Appel les fonctions protégés de MinkCase
     * @param string $name
     * @param array  $args
     * @return mixed
     * @throws ReflectionException
     */
    public function __call(string $name, array $args)
    {
        $method = $this->reflection->getMethod($name);
        $method->setAccessible(true); // NOSONAR pas le choix

        return $method->invokeArgs($this->mink, $args);
    }

    /**
     * Lance une console PsyShell
     * @return void
     * @throws Exception
     */
    public function console()
    {
        $psy = new PsyShell;
        $psy->setScopeVariables(['mink' => $this]);
        $psy->run();
    }
}
