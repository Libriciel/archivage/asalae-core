<?php

/**
 * AsalaeCore\MinkSuite\ChromeDriver
 * @noinspection PhpUnusedLocalVariableInspection
 */

namespace AsalaeCore\MinkSuite;

use Behat\Mink\Driver\DriverInterface;
use Behat\Mink\Element\NodeElement;
use Behat\Mink\Exception\DriverException;
use Behat\Mink\Exception\ElementNotFoundException;
use Behat\Mink\Exception\UnsupportedDriverActionException;
use Behat\Mink\Session;
use DMore\ChromeDriver\ChromeDriver as DMoreChromeDriver;
use DMore\ChromeDriver\ChromePage;
use DMore\ChromeDriver\HttpClient;
use ReflectionClass;
use ReflectionException;

/**
 * Wrapper pour donner accès à client
 *
 * @category TestSuite
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2022, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin DMoreChromeDriver
 */
class ChromeDriver implements DriverInterface
{
    /**
     * @var DMoreChromeDriver
     */
    private DMoreChromeDriver $driver;
    /**
     * @var ReflectionClass
     */
    private ReflectionClass $reflection;

    /**
     * ChromeDriver constructor.
     * @param string          $api_url
     * @param HttpClient|null $http_client
     * @param null            $base_url
     * @param array           $options
     */
    public function __construct(
        $api_url = 'http://localhost:9222',
        HttpClient $http_client = null,
        $base_url = null,
        $options = []
    ) {
        $this->driver = new DMoreChromeDriver($api_url, $http_client, $base_url, $options);
        $this->reflection = new ReflectionClass(get_class($this->driver));
    }

    /**
     * Appel les fonctions de ChromeBrowser
     * @param string $name
     * @param array  $args
     * @return mixed
     * @throws ReflectionException
     */
    public function __call(string $name, array $args)
    {
        $method = $this->reflection->getMethod($name);
        return $method->invokeArgs($this->driver, $args);
    }

    /**
     * Getter
     * @param string $name
     * @return mixed
     * @throws ReflectionException
     */
    public function __get(string $name)
    {
        $property = $this->reflection->getProperty($name);
        return $property->getValue($this->driver);
    }

    /**
     * Setter
     * @param string $name
     * @param mixed  $value
     * @throws ReflectionException
     */
    public function __set($name, $value)
    {
        $property = $this->reflection->getProperty($name);
        $property->setValue($this->driver, $value);
    }

    /**
     * Getter pour page (private property)
     * @return ChromePage
     * @throws ReflectionException
     */
    public function getPage(): ChromePage
    {
        $property = $this->reflection->getProperty('page');
        $property->setAccessible(true); // NOSONAR
        return $property->getValue($this->driver);
    }

    /**
     * call
     * @param string $xpath
     * @param string $script
     * @param null   $type
     * @return array
     * @throws ElementNotFoundException|ReflectionException
     * @noinspection PhpDocRedundantThrowsInspection
     */
    protected function runScriptOnXpathElement($xpath, $script, $type = null)
    {
        $method = $this->reflection->getMethod(__FUNCTION__);
        return $method->invokeArgs($this->driver, [$xpath, $script, $type]);
    }

    /**
     * Ajoute $value à un input plutôt que de remplacer son texte
     * @param string $xpath
     * @param string $value
     * @return void
     * @throws DriverException
     * @throws ElementNotFoundException
     * @throws ReflectionException
     */
    public function appendToTextTypeValue($xpath, $value)
    {
        if (is_array($value) || is_bool($value)) {
            throw new DriverException('Textual and file form fields don\'t support array or boolean values');
        }

        $this->focusElement($xpath);
        $this->caretAtEnd($xpath);

        for ($i = 0; $i < mb_strlen($value); $i++) {
            $char = mb_substr($value, $i, 1);
            $text = $key = $char;
            if ($char === "\n") {
                $text = chr(13);
                $key = 'Enter';
            }
            $this->getPage()->send('Input.dispatchKeyEvent', ['type' => 'keyDown', 'text' => $text, 'key' => $key]);
            $this->getPage()->send('Input.dispatchKeyEvent', ['type' => 'keyUp', 'key' => $key]);
        }
        usleep(5000);

        try {
            $this->blurElement($xpath);
        } catch (ElementNotFoundException $e) {
        }
    }

    /**
     * Effectu un element.focus();
     * @param string $xpath
     * @return void
     * @throws DriverException
     * @throws ElementNotFoundException
     * @throws ReflectionException
     */
    public function focusElement($xpath)
    {
        $script = 'if (element.offsetParent !== null)  { element.focus(); return true; } else { return false;  }';
        if (!$this->runScriptOnXpathElement($xpath, $script)) {
            throw new DriverException('Element is not visible and can not be focused');
        }
    }

    /**
     * Effectu un element.blur();
     * @param string $xpath
     * @return void
     * @throws ElementNotFoundException
     * @throws ReflectionException
     */
    public function blurElement($xpath)
    {
        $script = 'if (element === document.activeElement) {element.blur();}';
        $this->runScriptOnXpathElement($xpath, $script);
    }

    /**
     * Place le curseur à la fin de l'input
     * @param string $xpath
     * @return void
     * @throws ElementNotFoundException
     * @throws ReflectionException
     */
    public function caretAtEnd($xpath)
    {
        $script = 'if (element.value) { let end = element.value.length; element.setSelectionRange(end, end); }';
        $this->runScriptOnXpathElement($xpath, $script);
    }

    /**
     * Sets driver's current session.
     *
     * @param Session $session
     */
    public function setSession(Session $session)
    {
        $this->driver->setSession($session);
    }

    /**
     * Starts driver.
     *
     * Once started, the driver should be ready to visit a page.
     *
     * Calling any action before visiting a page is an undefined behavior.
     * The only supported method calls on a fresh driver are
     * - visit()
     * - setRequestHeader()
     * - setBasicAuth()
     * - reset()
     * - stop()
     *
     * Calling start on a started driver is an undefined behavior. Driver
     * implementations are free to handle it silently or to fail with an
     * exception.
     *
     * @throws DriverException When the driver cannot be started
     */
    public function start()
    {
        $this->driver->start();
    }

    /**
     * Checks whether driver is started.
     *
     * @return boolean
     */
    public function isStarted()
    {
        return $this->driver->isStarted();
    }

    /**
     * Stops driver.
     *
     * Once stopped, the driver should be started again before using it again.
     *
     * Calling any action on a stopped driver is an undefined behavior.
     * The only supported method call after stopping a driver is starting it again.
     *
     * Calling stop on a stopped driver is an undefined behavior. Driver
     * implementations are free to handle it silently or to fail with an
     * exception.
     *
     * @throws DriverException When the driver cannot be closed
     */
    public function stop()
    {
        $this->driver->stop();
    }

    /**
     * Resets driver state.
     *
     * This should reset cookies, request headers and basic authentication.
     * When possible, the history should be reset as well, but this is not enforced
     * as some implementations may not be able to reset it without restarting the
     * driver entirely. Consumers requiring a clean history should restart the driver
     * to enforce it.
     *
     * Once reset, the driver should be ready to visit a page.
     * Calling any action before visiting a page is an undefined behavior.
     * The only supported method calls on a fresh driver are
     * - visit()
     * - setRequestHeader()
     * - setBasicAuth()
     * - reset()
     * - stop()
     *
     * Calling reset on a stopped driver is an undefined behavior.
     */
    public function reset()
    {
        $this->driver->reset();
    }

    /**
     * Visit specified URL.
     *
     * @param string $url url of the page
     *
     * @throws UnsupportedDriverActionException When operation not supported by the driver
     * @throws DriverException                  When the operation cannot be done
     */
    public function visit($url)
    {
        $this->driver->visit($url);
    }

    /**
     * Returns current URL address.
     *
     * @return string
     *
     * @throws UnsupportedDriverActionException When operation not supported by the driver
     * @throws DriverException                  When the operation cannot be done
     */
    public function getCurrentUrl()
    {
        return $this->driver->getCurrentUrl();
    }

    /**
     * Reloads current page.
     *
     * @throws UnsupportedDriverActionException When operation not supported by the driver
     * @throws DriverException                  When the operation cannot be done
     */
    public function reload()
    {
        $this->driver->reload();
    }

    /**
     * Moves browser forward 1 page.
     *
     * @throws UnsupportedDriverActionException When operation not supported by the driver
     * @throws DriverException                  When the operation cannot be done
     */
    public function forward()
    {
        $this->driver->forward();
    }

    /**
     * Moves browser backward 1 page.
     *
     * @throws UnsupportedDriverActionException When operation not supported by the driver
     * @throws DriverException                  When the operation cannot be done
     */
    public function back()
    {
        $this->driver->back();
    }

    /**
     * Sets HTTP Basic authentication parameters.
     *
     * @param string|boolean $user     user name or false to disable authentication
     * @param string         $password password
     *
     * @throws UnsupportedDriverActionException When operation not supported by the driver
     * @throws DriverException                  When the operation cannot be done
     */
    public function setBasicAuth($user, $password)
    {
        $this->driver->setBasicAuth($user, $password);
    }

    /**
     * Switches to specific browser window.
     *
     * @param string|null $name window name (null for switching back to main window)
     *
     * @throws UnsupportedDriverActionException When operation not supported by the driver
     * @throws DriverException                  When the operation cannot be done
     */
    public function switchToWindow($name = null)
    {
        $this->driver->switchToWindow($name);
    }

    /**
     * Switches to specific iFrame.
     *
     * @param string|null $name iframe name (null for switching back)
     *
     * @throws UnsupportedDriverActionException When operation not supported by the driver
     * @throws DriverException                  When the operation cannot be done
     */
    public function switchToIFrame($name = null)
    {
        $this->driver->switchToIFrame($name);
    }

    /**
     * Sets specific request header on client.
     *
     * @param string $name
     * @param string $value
     *
     * @throws UnsupportedDriverActionException When operation not supported by the driver
     * @throws DriverException                  When the operation cannot be done
     */
    public function setRequestHeader($name, $value)
    {
        $this->driver->setRequestHeader($name, $value);
    }

    /**
     * Returns last response headers.
     *
     * @return array<string, string|string[]>
     *
     * @throws UnsupportedDriverActionException When operation not supported by the driver
     * @throws DriverException                  When the operation cannot be done
     */
    public function getResponseHeaders()
    {
        return $this->driver->getResponseHeaders();
    }

    /**
     * Sets cookie.
     *
     * Passing null as value will delete the cookie.
     *
     * @param string      $name
     * @param string|null $value
     *
     * @throws UnsupportedDriverActionException When operation not supported by the driver
     * @throws DriverException                  When the operation cannot be done
     */
    public function setCookie($name, $value = null)
    {
        $this->driver->setCookie($name, $value);
    }

    /**
     * Returns cookie by name.
     *
     * @param string $name
     *
     * @return string|null
     *
     * @throws UnsupportedDriverActionException When operation not supported by the driver
     * @throws DriverException                  When the operation cannot be done
     */
    public function getCookie($name)
    {
        return $this->driver->getCookie($name);
    }

    /**
     * Returns last response status code.
     *
     * @return int
     *
     * @throws UnsupportedDriverActionException When operation not supported by the driver
     * @throws DriverException                  When the operation cannot be done
     */
    public function getStatusCode()
    {
        return $this->driver->getStatusCode();
    }

    /**
     * Returns last response content.
     *
     * @return string
     *
     * @throws UnsupportedDriverActionException When operation not supported by the driver
     * @throws DriverException                  When the operation cannot be done
     */
    public function getContent()
    {
        return $this->driver->getContent();
    }

    /**
     * Capture a screenshot of the current window.
     *
     * @return string screenshot of MIME type image/* depending
     *                on driver (e.g., image/png, image/jpeg)
     *
     * @throws UnsupportedDriverActionException When operation not supported by the driver
     * @throws DriverException                  When the operation cannot be done
     */
    public function getScreenshot()
    {
        return $this->driver->getScreenshot();
    }

    /**
     * Return the names of all open windows.
     *
     * @return string[] array of all open windows
     *
     * @throws UnsupportedDriverActionException When operation not supported by the driver
     * @throws DriverException                  When the operation cannot be done
     */
    public function getWindowNames()
    {
        return $this->driver->getWindowNames();
    }

    /**
     * Return the name of the currently active window.
     *
     * @return string the name of the current window
     *
     * @throws UnsupportedDriverActionException When operation not supported by the driver
     * @throws DriverException                  When the operation cannot be done
     */
    public function getWindowName()
    {
        return $this->driver->getWindowName();
    }

    /**
     * Finds elements with specified XPath query.
     *
     * @param string $xpath
     *
     * @return NodeElement[]
     *
     * @throws UnsupportedDriverActionException When operation not supported by the driver
     * @throws DriverException                  When the operation cannot be done
     */
    public function find($xpath)
    {
        return $this->driver->find($xpath);
    }

    /**
     * Returns element's tag name by its XPath query.
     *
     * @param string $xpath
     *
     * @return string
     *
     * @throws ElementNotFoundException When the operation cannot be done
     */
    public function getTagName($xpath)
    {
        return $this->driver->getTagName($xpath);
    }

    /**
     * Returns element's text by its XPath query.
     *
     * @param string $xpath
     *
     * @return string
     *
     * @throws ElementNotFoundException
     */
    public function getText($xpath)
    {
        return $this->driver->getText($xpath);
    }

    /**
     * Returns element's inner html by its XPath query.
     *
     * @param string $xpath
     *
     * @return string
     *
     * @throws UnsupportedDriverActionException When operation not supported by the driver
     * @throws DriverException                  When the operation cannot be done
     */
    public function getHtml($xpath)
    {
        return $this->driver->getHtml($xpath);
    }

    /**
     * Returns element's outer html by its XPath query.
     *
     * @param string $xpath
     *
     * @return string
     *
     * @throws UnsupportedDriverActionException When operation not supported by the driver
     * @throws DriverException                  When the operation cannot be done
     */
    public function getOuterHtml($xpath)
    {
        return $this->driver->getOuterHtml($xpath);
    }

    /**
     * Returns element's attribute by its XPath query.
     *
     * @param string $xpath
     * @param string $name
     *
     * @return string|null
     *
     * @throws UnsupportedDriverActionException When operation not supported by the driver
     * @throws DriverException                  When the operation cannot be done
     */
    public function getAttribute($xpath, $name)
    {
        return $this->driver->getAttribute($xpath, $name);
    }

    /**
     * Returns element's value by its XPath query.
     *
     * @param string $xpath
     *
     * @return string|bool|array|null
     *
     * @throws UnsupportedDriverActionException When operation not supported by the driver
     * @throws DriverException                  When the operation cannot be done
     *
     * @see \Behat\Mink\Element\NodeElement::getValue
     */
    public function getValue($xpath)
    {
        return $this->driver->getValue($xpath);
    }

    /**
     * Sets element's value by its XPath query.
     *
     * @param string            $xpath
     * @param string|bool|array $value
     *
     * @throws UnsupportedDriverActionException When operation not supported by the driver
     * @throws DriverException                  When the operation cannot be done
     *
     * @see \Behat\Mink\Element\NodeElement::setValue
     */
    public function setValue($xpath, $value)
    {
        $this->driver->setValue($xpath, $value);
    }

    /**
     * Checks checkbox by its XPath query.
     *
     * @param string $xpath
     *
     * @throws UnsupportedDriverActionException When operation not supported by the driver
     * @throws DriverException                  When the operation cannot be done
     *
     * @see \Behat\Mink\Element\NodeElement::check
     */
    public function check($xpath)
    {
        $this->driver->check($xpath);
    }

    /**
     * Unchecks checkbox by its XPath query.
     *
     * @param string $xpath
     *
     * @throws UnsupportedDriverActionException When operation not supported by the driver
     * @throws DriverException                  When the operation cannot be done
     *
     * @see \Behat\Mink\Element\NodeElement::uncheck
     */
    public function uncheck($xpath)
    {
        $this->driver->uncheck($xpath);
    }

    /**
     * Checks whether checkbox or radio button located by its XPath query is checked.
     *
     * @param string $xpath
     *
     * @return boolean
     *
     * @throws UnsupportedDriverActionException When operation not supported by the driver
     * @throws DriverException                  When the operation cannot be done
     *
     * @see \Behat\Mink\Element\NodeElement::isChecked
     */
    public function isChecked($xpath)
    {
        return $this->driver->isChecked($xpath);
    }

    /**
     * Selects option from select field or value in radio group located by its XPath query.
     *
     * @param string  $xpath
     * @param string  $value
     * @param boolean $multiple
     *
     * @throws UnsupportedDriverActionException When operation not supported by the driver
     * @throws DriverException                  When the operation cannot be done
     *
     * @see \Behat\Mink\Element\NodeElement::selectOption
     */
    public function selectOption($xpath, $value, $multiple = false)
    {
        $this->driver->selectOption($xpath, $value, $multiple = false);
    }

    /**
     * Checks whether select option, located by its XPath query, is selected.
     *
     * @param string $xpath
     *
     * @return boolean
     *
     * @throws UnsupportedDriverActionException When operation not supported by the driver
     * @throws DriverException                  When the operation cannot be done
     *
     * @see \Behat\Mink\Element\NodeElement::isSelected
     */
    public function isSelected($xpath)
    {
        return $this->driver->isSelected($xpath);
    }

    /**
     * Clicks button or link located by its XPath query.
     *
     * @param string $xpath
     *
     * @throws UnsupportedDriverActionException When operation not supported by the driver
     * @throws DriverException                  When the operation cannot be done
     */
    public function click($xpath)
    {
        $this->driver->click($xpath);
    }

    /**
     * Double-clicks button or link located by its XPath query.
     *
     * @param string $xpath
     *
     * @throws UnsupportedDriverActionException When operation not supported by the driver
     * @throws DriverException                  When the operation cannot be done
     */
    public function doubleClick($xpath)
    {
        $this->driver->doubleClick($xpath);
    }

    /**
     * Right-clicks button or link located by its XPath query.
     *
     * @param string $xpath
     *
     * @throws UnsupportedDriverActionException When operation not supported by the driver
     * @throws DriverException                  When the operation cannot be done
     */
    public function rightClick($xpath)
    {
        $this->driver->rightClick($xpath);
    }

    /**
     * Attaches file path to file field located by its XPath query.
     *
     * @param string $xpath
     * @param string $path
     *
     * @throws DriverException When the operation cannot be done
     * @throws ElementNotFoundException
     * @throws UnsupportedDriverActionException When operation not supported by the driver
     * @see \Behat\Mink\Element\NodeElement::attachFile
     */
    public function attachFile($xpath, $path)
    {
        $this->driver->attachFile($xpath, $path);
    }

    /**
     * Checks whether element visible located by its XPath query.
     *
     * @param string $xpath
     *
     * @return boolean
     *
     * @throws UnsupportedDriverActionException When operation not supported by the driver
     * @throws DriverException                  When the operation cannot be done
     */
    public function isVisible($xpath)
    {
        return $this->driver->isVisible($xpath);
    }

    /**
     * Simulates a mouse over on the element.
     *
     * @param string $xpath
     *
     * @throws UnsupportedDriverActionException When operation not supported by the driver
     * @throws DriverException                  When the operation cannot be done
     */
    public function mouseOver($xpath)
    {
        $this->driver->mouseOver($xpath);
    }

    /**
     * Brings focus to element.
     *
     * @param string $xpath
     *
     * @throws UnsupportedDriverActionException When operation not supported by the driver
     * @throws DriverException                  When the operation cannot be done
     */
    public function focus($xpath)
    {
        $this->driver->focus($xpath);
    }

    /**
     * Removes focus from element.
     *
     * @param string $xpath
     *
     * @throws UnsupportedDriverActionException When operation not supported by the driver
     * @throws DriverException                  When the operation cannot be done
     */
    public function blur($xpath)
    {
        $this->driver->blur($xpath);
    }

    /**
     * Presses specific keyboard key.
     *
     * @param string     $xpath
     * @param string|int $char     could be either char ('b') or char-code (98)
     * @param string     $modifier keyboard modifier (could be 'ctrl', 'alt', 'shift' or 'meta')
     *
     * @throws UnsupportedDriverActionException When operation not supported by the driver
     * @throws DriverException                  When the operation cannot be done
     */
    public function keyPress($xpath, $char, $modifier = null)
    {
        $this->driver->keyPress($xpath, $char, $modifier = null);
    }

    /**
     * Pressed down specific keyboard key.
     *
     * @param string      $xpath
     * @param string|int  $char     could be either char ('b') or char-code (98)
     * @param string|null $modifier keyboard modifier (could be 'ctrl', 'alt', 'shift' or 'meta')
     *
     * @throws UnsupportedDriverActionException When operation not supported by the driver
     * @throws DriverException                  When the operation cannot be done
     */
    public function keyDown($xpath, $char, $modifier = null)
    {
        $this->driver->keyDown($xpath, $char, $modifier = null);
    }

    /**
     * Pressed up specific keyboard key.
     *
     * @param string      $xpath
     * @param string|int  $char     could be either char ('b') or char-code (98)
     * @param string|null $modifier keyboard modifier (could be 'ctrl', 'alt', 'shift' or 'meta')
     *
     * @throws UnsupportedDriverActionException When operation not supported by the driver
     * @throws DriverException                  When the operation cannot be done
     */
    public function keyUp($xpath, $char, $modifier = null)
    {
        $this->driver->keyUp($xpath, $char, $modifier = null);
    }

    /**
     * Drag one element onto another.
     *
     * @param string $sourceXpath
     * @param string $destinationXpath
     *
     * @throws UnsupportedDriverActionException When operation not supported by the driver
     * @throws DriverException                  When the operation cannot be done
     */
    public function dragTo($sourceXpath, $destinationXpath)
    {
        $this->driver->dragTo($sourceXpath, $destinationXpath);
    }

    /**
     * Executes JS script.
     *
     * @param string $script
     *
     * @throws UnsupportedDriverActionException When operation not supported by the driver
     * @throws DriverException                  When the operation cannot be done
     */
    public function executeScript($script)
    {
        $this->driver->executeScript($script);
    }

    /**
     * Evaluates JS script.
     *
     * The "return" keyword is optional in the script passed as argument. Driver implementations
     * must accept the expression both with and without the keyword.
     *
     * @param string $script
     *
     * @return mixed
     *
     * @throws UnsupportedDriverActionException When operation not supported by the driver
     * @throws DriverException                  When the operation cannot be done
     */
    public function evaluateScript($script)
    {
        return $this->driver->evaluateScript($script);
    }

    /**
     * Waits some time or until JS condition turns true.
     *
     * @param int    $timeout   timeout in milliseconds
     * @param string $condition JS condition
     *
     * @return bool
     *
     * @throws UnsupportedDriverActionException When operation not supported by the driver
     * @throws DriverException                  When the operation cannot be done
     */
    public function wait($timeout, $condition)
    {
        return $this->driver->wait($timeout, $condition);
    }

    /**
     * Set the dimensions of the window.
     *
     * @param int    $width  set the window width, measured in pixels
     * @param int    $height set the window height, measured in pixels
     * @param string $name   window name (null for the main window)
     *
     * @throws UnsupportedDriverActionException When operation not supported by the driver
     * @throws DriverException                  When the operation cannot be done
     */
    public function resizeWindow($width, $height, $name = null)
    {
        $this->driver->resizeWindow($width, $height, $name = null);
    }

    /**
     * Maximizes the window if it is not maximized already.
     *
     * @param string $name window name (null for the main window)
     *
     * @throws UnsupportedDriverActionException When operation not supported by the driver
     * @throws DriverException                  When the operation cannot be done
     */
    public function maximizeWindow($name = null)
    {
        $this->driver->maximizeWindow($name = null);
    }

    /**
     * Submits the form.
     *
     * @param string $xpath Xpath.
     *
     * @throws UnsupportedDriverActionException When operation not supported by the driver
     * @throws DriverException                  When the operation cannot be done
     *
     * @see \Behat\Mink\Element\NodeElement::submitForm
     */
    public function submitForm($xpath)
    {
        $this->driver->submitForm($xpath);
    }
}
