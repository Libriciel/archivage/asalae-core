<?php
/**
 * AsalaeCore\MinkSuite\MinkAssertionFailedException
 */

namespace AsalaeCore\MinkSuite;

/**
 * Exception lors d'un test fonctionnel
 *
 * @category TestSuite
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2022, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class MinkAssertionFailedException extends MinkCaseException
{
}
