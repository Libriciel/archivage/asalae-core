<?php
/**
 * AsalaeCore\MinkSuite\MinkBeanstalk
 */

namespace AsalaeCore\MinkSuite;

use AsalaeCore\Factory\Utility;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;
use Exception;
use Pheanstalk\Job;

/**
 * Manipulations de beanstalk pour les tests mink
 *
 * @category    Mink
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2022, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
abstract class MinkBeanstalk
{
    /**
     * Tue tous les workers
     * @return void
     * @throws Exception
     */
    public static function killAll()
    {
        $BeanstalkWorkers = TableRegistry::getTableLocator()->get('BeanstalkWorkers');
        foreach ($BeanstalkWorkers->find() as $worker) {
            if ($worker->get('running')) {
                $config = Configure::read('Beanstalk.workers.' . $worker->get('name'));
                $kill = str_replace('{{pid}}', $worker->get('pid'), $config['kill']);
                $kill = str_replace('{{hostname}}', $worker->get('hostname'), $kill);
                exec($kill);
            }
            $BeanstalkWorkers->delete($worker);
        }
    }

    /**
     * Lance les workers sités dans la liste $workers
     * @param array $workers
     * @return void
     * @throws Exception
     */
    public static function run(array $workers)
    {
        $BeanstalkWorkers = TableRegistry::getTableLocator()->get('BeanstalkWorkers');
        $config = Configure::read('Beanstalk.workers');
        foreach ($config as $name => $args) {
            if (!$args['active'] || !in_array($name, $workers)) {
                continue;
            }
            $count = $BeanstalkWorkers->find()
                ->where(['name' => $name])
                ->count();
            if ($count < 1) {
                self::runSingle($name);
            }
        }
    }

    /**
     * Lancement d'un worker
     * @param string $tube
     * @return EntityInterface
     * @throws Exception
     */
    public static function runSingle(string $tube): EntityInterface
    {
        $config = Configure::read("Beanstalk.workers.$tube.run");
        $command = preg_replace(
            '/^bin\/cake(\.bat|\.php)?/',
            CAKE_SHELL,
            $config
        );
        $Exec = Utility::get('Exec');
        $Exec->async($command);
        $i = 0;
        do {
            $i++;
            usleep(10000);
            $worker = TableRegistry::getTableLocator()
                ->get('BeanstalkWorkers')
                ->find()
                ->where(['tube' => $tube])
                ->first();
        } while ($i < 1000 && !$worker);
        return $worker;
    }

    /**
     * Supprime tous les jobs
     * @return void
     * @throws Exception
     */
    public static function deleteAllJobs()
    {
        $BeanstalkJobs = TableRegistry::getTableLocator()->get('BeanstalkJobs');
        $tubes = $BeanstalkJobs->find()
            ->select(['tube'])
            ->group(['tube'])
            ->all()
            ->map(fn (EntityInterface $e) => $e->get('tube'))
            ->toArray();
        $Beanstalk = Utility::get('Beanstalk');
        $Beanstalk->params['disable_check_ttr'] = true;
        // beanstalk v3
        if (method_exists($Beanstalk, 'getPheanstalk')) {
            $pheanstalk = $Beanstalk->getPheanstalk();
            foreach ($tubes as $tube) {
                /** @var Job $job */
                while ($pheanstalk->statsTube($tube)->{'current-jobs-buried'}) {
                    $job = $pheanstalk->peekBuried($tube);
                    $pheanstalk->delete($job);
                }
                while ($pheanstalk->statsTube($tube)->{'current-jobs-delayed'}) {
                    $job = $pheanstalk->peekDelayed($tube);
                    $pheanstalk->delete($job);
                }
                while ($pheanstalk->statsTube($tube)->{'current-jobs-ready'}) {
                    $job = $pheanstalk->peekReady($tube);
                    $pheanstalk->delete($job);
                }
            }
        } else {
            $BeanstalkJobs->deleteAll([]);
        }
    }

    /**
     * Prépare les tests mink
     * @param array $workers
     * @return void
     * @throws Exception
     */
    public static function init(array $workers)
    {
        self::killAll();
        self::deleteAllJobs();
        usleep(10000);
        self::run($workers);
        usleep(10000);
    }

    /**
     * Nettoyage
     * @return void
     * @throws Exception
     */
    public static function destroy()
    {
        self::killAll();
        self::deleteAllJobs();
    }
}
