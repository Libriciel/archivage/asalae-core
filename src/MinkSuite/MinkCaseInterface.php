<?php
/**
 * AsalaeCore\MinkSuite\MinkCaseInterface
 */

namespace AsalaeCore\MinkSuite;

use AsalaeCore\Console\ConsoleIo;
use Behat\Mink\Mink;
use Behat\Mink\Session;
use ErrorException;
use Exception;
use WebSocket\TimeoutException;

/**
 * Tests fonctionnels mink
 *
 * @category TestSuite
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2022, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
interface MinkCaseInterface
{
    /**
     * Contructeur
     * @param string         $name
     * @param Mink           $mink
     * @param array          $options
     * @param ConsoleIo|null $io
     */
    public function __construct(string $name, Mink $mink, array $options = [], ConsoleIo $io = null);

    /**
     * Execute un des tests de la class
     * @param string $method
     * @return void
     * @throws MinkAssertionFailedException|ErrorException|TimeoutException
     */
    public function test(string $method): void;

    /**
     * Fait un screenshot
     * @param string $path
     * @return void
     * @throws Exception
     */
    public function screenshot(string $path): void;

    /**
     * Donne le chemin du screenshot d'erreur pour MinkSuite
     * @return string
     */
    public static function getScreenshotPath(): string;

    /**
     * Récupère le status ajax de la page actuelle (dernière requête)
     * @return array|null
     */
    public function debugAjax(): ?array;

    /**
     * Donne la session lié au MinkCase
     * @return Session
     */
    public function getSession(): Session;
}
