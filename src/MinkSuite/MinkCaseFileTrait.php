<?php

/**
 * AsalaeCore\MinkSuite\MinkCaseFileTrait
 */

namespace AsalaeCore\MinkSuite;

use Datacompressor\Exception\DataCompressorException;
use Datacompressor\Utility\DataCompressor;

/**
 * Methodes de manipulation de fichiers
 *
 * @category TestSuite
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2023, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin MinkCase
 */
trait MinkCaseFileTrait
{
    /**
     * dump un fichier
     * @param string|null $filename
     * @param string|null $content
     * @return string $path
     */
    protected function fileCreate(string $filename = null, string $content = null): string
    {
        if (empty($filename)) {
            $filename = sprintf('mink-%s.txt', uuid_create(UUID_TYPE_TIME));
        }
        if (empty($content)) {
            $content = uuid_create(UUID_TYPE_TIME);
        }
        $path = MinkCase::getTmpDir() . DS . $filename;
        $dir = dirname($path);
        if (!is_dir($dir)) {
            mkdir($dir, 0777, true);
        }
        file_put_contents($path, $content);
        return $path;
    }

    /**
     * Créer un zip
     * @param string|null $filename          nom du fichier zip
     * @param array       $files             liste des fichiers (optionnel)
     * @param int         $addGeneratedFiles ajoute x fichiers à $files
     * @return string
     * @throws DataCompressorException
     */
    protected function fileCreateZip(
        string $filename = null,
        array $files = [],
        int $addGeneratedFiles = 0
    ): string {
        if (empty($filename)) {
            $filename = sprintf('mink-%s.zip', uuid_create(UUID_TYPE_TIME));
        }
        if (empty($files) && $addGeneratedFiles === 0) {
            $files[] = $this->fileCreate();
        }
        for ($i = 0; $i < $addGeneratedFiles; $i++) {
            $files[] = $this->fileCreate();
        }
        $path = MinkCase::getTmpDir() . DS . $filename;
        $dir = dirname($path);
        if (!is_dir($dir)) {
            mkdir($dir, 0777, true);
        }
        DataCompressor::compress($files, $path);
        return $path;
    }
}
