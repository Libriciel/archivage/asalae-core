<?php
/**
 * AsalaeCore\MinkSuite\MinkCaseAssertTrait
 */

namespace AsalaeCore\MinkSuite;

use Behat\Mink\Exception\DriverException;
use Behat\Mink\Exception\UnsupportedDriverActionException;
use Cake\Error\Debugger;
use Cake\ORM\TableRegistry;

/**
 * Assertions
 *
 * @category TestSuite
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2022, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin MinkCase
 */
trait MinkCaseAssertTrait
{
    /**
     * Vérifi qu'un champ de formulaire visible, possède bien une valeur
     * @param string $label
     * @param string $content
     * @return void
     * @throws MinkAssertionFailedException
     * @throws DriverException
     * @throws UnsupportedDriverActionException
     */
    protected function assertFormFieldContains(string $label, string $content): void
    {
        $element = $this->findVisibleFormElement($label);
        if (!$element) {
            throw new MinkAssertionFailedException(
                sprintf(
                    "%s failed : %s\nat %s",
                    __FUNCTION__,
                    "the form field does not exists or is not visible",
                    $this->getAssertLocation()
                )
            );
        }
        switch ($element->getTagName()) {
            case 'input':
                $value = $element->getValue();
                break;
            default:
                $value = $element->getText();
                break;
        }
        if (mb_strpos($value, $content) === false) {
            throw new MinkAssertionFailedException(
                sprintf(
                    "%s failed : %s\nat %s",
                    __FUNCTION__,
                    "the form field does not content the string",
                    $this->getAssertLocation()
                )
            );
        }
        $this->outputRunningFunction();
    }

    /**
     * Donne l'emplacement de l'assertion en échec dans le test
     * @return string
     */
    private function getAssertLocation(): string
    {
        $trace = Debugger::trace(['start' => 2, 'depth' => 3, 'format' => 'array']);
        $this->outputRunningFunction();
        return sprintf(
            '%s:%d',
            $trace[0]['file'] ?? '',
            $trace[0]['line'] ?? ''
        );
    }

    /**
     * Attend la réponse ajax et renvoi une exception si elle ne contiens pas un
     * X-Asalae-Success = 'true'
     * @param int $wait
     * @return void
     * @throws MinkAssertionFailedException
     */
    protected function assertResponseSuccess(int $wait = 5000): void
    {
        $begin = microtime(true);
        $this->session->wait(
            $wait,
            "minkAjaxRunning === false"
        );
        if (microtime(true) - $begin > ($wait / 1000)) {
            trigger_error('wait timeout occured');
        }
        $xAsalaeSuccess = $this->session->evaluateScript(
            "minkLastAjaxResponse.response?.getResponseHeader('X-Asalae-Success')"
        );
        if ($xAsalaeSuccess !== 'true') {
            throw new MinkAssertionFailedException(
                sprintf(
                    "%s failed : %s\nat %s",
                    __FUNCTION__,
                    "failed to assert that the Response is sent and contains the header X-Asalae-Success = 'true'",
                    $this->getAssertLocation()
                )
            );
        }
        $this->outputRunningFunction();
    }

    /**
     * Attend la réponse ajax et renvoi une exception si elle ne contiens pas un
     * X-Asalae-Success = 'false'
     * @param int $wait
     * @return void
     * @throws MinkAssertionFailedException
     */
    protected function assertResponseFailure(int $wait = 5000): void
    {
        $begin = microtime(true);
        $this->session->wait(
            $wait,
            "minkAjaxRunning === false"
        );
        if (microtime(true) - $begin > ($wait / 1000)) {
            trigger_error('wait timeout occured');
        }
        $xAsalaeSuccess = $this->session->evaluateScript(
            "minkLastAjaxResponse.response?.getResponseHeader('X-Asalae-Success')"
        );
        if ($xAsalaeSuccess !== 'false') {
            throw new MinkAssertionFailedException(
                sprintf(
                    "%s failed : %s\nat %s",
                    __FUNCTION__,
                    "failed to assert that the Response is sent and contains the header X-Asalae-Success = 'false'",
                    $this->getAssertLocation()
                )
            );
        }
        $this->outputRunningFunction();
    }

    /**
     * Assure que 2 valeurs soit égales
     * @param mixed $expected
     * @param mixed $value
     * @return void
     * @throws MinkAssertionFailedException
     */
    protected function assertEquals($expected, $value): void
    {
        if ($expected !== $value) {
            throw new MinkAssertionFailedException(
                sprintf(
                    "%s failed : %s\nat %s",
                    __FUNCTION__,
                    "failed to assert that the expected value match the value",
                    $this->getAssertLocation()
                )
            );
        }
        $this->outputRunningFunction();
    }

    /**
     * Assure que la valeur soit vrai
     * @param mixed $value
     * @return void
     * @throws MinkAssertionFailedException
     */
    protected function assertTrue($value): void
    {
        if ($value !== true) {
            throw new MinkAssertionFailedException(
                sprintf(
                    "%s failed : %s\nat %s",
                    __FUNCTION__,
                    "failed to assert that the value is true",
                    $this->getAssertLocation()
                )
            );
        }
        $this->outputRunningFunction();
    }

    /**
     * Assure que la valeur soit fausse
     * @param mixed $value
     * @return void
     * @throws MinkAssertionFailedException
     */
    protected function assertFalse($value): void
    {
        if ($value !== false) {
            throw new MinkAssertionFailedException(
                sprintf(
                    "%s failed : %s\nat %s",
                    __FUNCTION__,
                    "failed to assert that the value is false",
                    $this->getAssertLocation()
                )
            );
        }
        $this->outputRunningFunction();
    }

    /**
     * Assure que la valeur soit nulle
     * @param mixed $value
     * @return void
     * @throws MinkAssertionFailedException
     */
    protected function assertNull($value): void
    {
        if ($value !== null) {
            throw new MinkAssertionFailedException(
                sprintf(
                    "%s failed : %s\nat %s",
                    __FUNCTION__,
                    "failed to assert that the value is null",
                    $this->getAssertLocation()
                )
            );
        }
        $this->outputRunningFunction();
    }

    /**
     * Assure que $needle existe dans $haystack
     * @param mixed $needle
     * @param mixed $haystack
     * @return void
     * @throws MinkAssertionFailedException
     */
    protected function assertStringContainsString(string $needle, string $haystack): void
    {
        if (strpos($haystack, $needle) === false) {
            throw new MinkAssertionFailedException(
                sprintf(
                    "%s failed : %s\nat %s",
                    __FUNCTION__,
                    "failed to assert that the haystack contain the needle",
                    $this->getAssertLocation()
                )
            );
        }
        $this->outputRunningFunction();
    }

    /**
     * Assure que $needle existe dans $haystack
     * @param string $needle
     * @param string $haystack
     * @return void
     * @throws MinkAssertionFailedException
     */
    protected function assertStringNotContainsString(string $needle, string $haystack): void
    {
        if (strpos($haystack, $needle) !== false) {
            throw new MinkAssertionFailedException(
                sprintf(
                    "%s failed : %s\nat %s",
                    __FUNCTION__,
                    "failed to assert that the haystack does not contain the needle",
                    $this->getAssertLocation()
                )
            );
        }
        $this->outputRunningFunction();
    }

    /**
     * Vérifi qu'un tableau de résultats, contienne bien une valeur
     * @param string $css
     * @param string $text
     * @return void
     * @throws DriverException
     * @throws MinkAssertionFailedException
     * @throws UnsupportedDriverActionException
     */
    protected function assertTableContains(string $css, string $text): void
    {
        $xpath = $this->cssToXpath($css);
        $table = $this->findFirst($xpath);
        if (!$table) {
            throw new MinkAssertionFailedException(
                sprintf(
                    "%s failed : %s\nat %s",
                    __FUNCTION__,
                    "failed to assert that the table exists",
                    $this->getAssertLocation()
                )
            );
        }
        $tds = $table->findAll('xpath', '//td');
        foreach ($tds as $td) {
            if ($td->getText() === $text) {
                $this->outputRunningFunction();
                return;
            }
        }
        $tds = $table->findAll('css', 'td .td-group-fieldvalue');
        foreach ($tds as $td) {
            if ($td->getText() === $text) {
                $this->outputRunningFunction();
                return;
            }
        }
        throw new MinkAssertionFailedException(
            sprintf(
                "%s failed : %s\nat %s",
                __FUNCTION__,
                "failed to assert that the expected value exists in the table",
                $this->getAssertLocation()
            )
        );
    }

    /**
     * Vérifi qu'un tableau de résultats ne contient pas une valeur
     * @param string $css
     * @param string $text
     * @return void
     * @throws DriverException
     * @throws MinkAssertionFailedException
     * @throws UnsupportedDriverActionException
     */
    protected function assertTableDoesNotContains(string $css, string $text): void
    {
        $xpath = $this->cssToXpath($css);
        $table = $this->findFirst($xpath);
        if (!$table) {
            throw new MinkAssertionFailedException(
                sprintf(
                    "%s failed : %s\nat %s",
                    __FUNCTION__,
                    "failed to assert that the table exists",
                    $this->getAssertLocation()
                )
            );
        }
        $tds = $table->findAll('xpath', '//td');
        foreach ($tds as $td) {
            if ($td->getText() === $text) {
                throw new MinkAssertionFailedException(
                    sprintf(
                        "%s failed : %s\nat %s",
                        __FUNCTION__,
                        "failed to assert that the expected value does not exists in the table",
                        $this->getAssertLocation()
                    )
                );
            }
        }
        $this->outputRunningFunction();
    }

    /**
     * Vérifi le titre de la page
     * @param string $h1
     * @return void
     * @throws DriverException
     * @throws MinkAssertionFailedException
     * @throws UnsupportedDriverActionException
     */
    protected function assertPageTitleIs(string $h1)
    {
        $h1s = $this->driver->find('//h1');
        foreach ($h1s as $h1Element) {
            if ($h1Element->getText() === $h1) {
                $this->outputRunningFunction();
                return;
            }
        }
        throw new MinkAssertionFailedException(
            sprintf(
                "%s failed : %s\nat %s",
                __FUNCTION__,
                "failed to assert that the page title is correct",
                $this->getAssertLocation()
            )
        );
    }

    /**
     * Vérifi qu'une modale est bien ouverte
     * @return void
     * @throws DriverException
     * @throws MinkAssertionFailedException
     * @throws UnsupportedDriverActionException
     */
    protected function assertModalIsOpenned()
    {
        $modals = $this->driver->find($this->cssToXpath('.modal'));
        foreach ($modals as $modal) {
            if ($modal->isVisible()) {
                $this->outputRunningFunction();
                return;
            }
        }
        throw new MinkAssertionFailedException(
            sprintf(
                "%s failed : %s\nat %s",
                __FUNCTION__,
                "failed to assert that the modal is openned",
                $this->getAssertLocation()
            )
        );
    }

    /**
     * Vérifi qu'une modale est bien ouverte
     * @return void
     * @throws DriverException
     * @throws MinkAssertionFailedException
     * @throws UnsupportedDriverActionException
     */
    protected function assertModalIsClosed()
    {
        $modals = $this->driver->find($this->cssToXpath('.modal'));
        foreach ($modals as $modal) {
            if ($modal->isVisible()) {
                throw new MinkAssertionFailedException(
                    sprintf(
                        "%s failed : %s\nat %s",
                        __FUNCTION__,
                        "failed to assert that the modal is closed",
                        $this->getAssertLocation()
                    )
                );
            }
        }
        $this->outputRunningFunction();
    }

    /**
     * Vérifi l'étape de la modale
     * @param int $num
     * @return void
     * @throws DriverException
     * @throws MinkAssertionFailedException
     * @throws UnsupportedDriverActionException
     */
    protected function assertModalStep(int $num)
    {
        $lis = $this->driver->find($this->cssToXpath('.modal ol > li'));
        $i = 0;
        foreach ($lis as $li) {
            if (!$li->isVisible()) {
                continue;
            }
            $i++;
            if ($li->hasClass('active')) {
                if ($i === $num) {
                    $this->outputRunningFunction();
                    return;
                }
                throw new MinkAssertionFailedException(
                    sprintf(
                        "%s failed : %s\nat %s",
                        __FUNCTION__,
                        "failed to assert that the modal step is $num, current step is $i",
                        $this->getAssertLocation()
                    )
                );
            }
        }
        throw new MinkAssertionFailedException(
            sprintf(
                "%s failed : %s\nat %s",
                __FUNCTION__,
                "failed to assert that the modal step is $num",
                $this->getAssertLocation()
            )
        );
    }

    /**
     * Vérifi le contenu d'un tableau de visualisation
     * @param string $thText
     * @param string $tdContains
     * @return void
     * @throws DriverException
     * @throws MinkAssertionFailedException
     * @throws UnsupportedDriverActionException
     */
    protected function assertViewTableValueContains(string $thText, string $tdContains): void
    {
        $xpath = $this->cssToXpath('table.fixed-left-th tbody th');
        $ths = $this->driver->find($xpath);
        foreach ($ths as $th) {
            if (!$th->isVisible() || $th->getText() !== $thText) {
                continue;
            }
            $td = $th->getParent()->find('xpath', 'td');
            if ($td) {
                if (mb_strpos($td->getText(), $tdContains) === false) {
                    throw new MinkAssertionFailedException(
                        sprintf(
                            "%s failed : %s\nat %s",
                            __FUNCTION__,
                            "the view table field does not content the string",
                            $this->getAssertLocation()
                        )
                    );
                }
                $this->outputRunningFunction();
                return;
            }
        }
        throw new MinkAssertionFailedException(
            sprintf(
                "%s failed : %s\nat %s",
                __FUNCTION__,
                "the view table field does not exists or is not visible",
                $this->getAssertLocation()
            )
        );
    }

    /**
     * Vérifi qu'un des elements donnés par $css, contien $text
     * @param string $css
     * @param string $text
     * @return void
     * @throws DriverException
     * @throws MinkAssertionFailedException
     * @throws UnsupportedDriverActionException
     */
    protected function assertElementContains(string $css, string $text): void
    {
        $xpath = $this->cssToXpath($css);
        $elements = $this->driver->find($xpath);
        if (count($elements) === 0) {
            throw new MinkAssertionFailedException(
                sprintf(
                    "%s failed : %s\nat %s",
                    __FUNCTION__,
                    "the element does not exists",
                    $this->getAssertLocation()
                )
            );
        }
        foreach ($elements as $element) {
            if (mb_strpos($element->getText(), $text) !== false) {
                return;
            }
        }
        throw new MinkAssertionFailedException(
            sprintf(
                "%s failed : %s\nat %s",
                __FUNCTION__,
                "failed to assert that the element contains text",
                $this->getAssertLocation()
            )
        );
    }

    /**
     * vérifi l'existance d'un job
     * @param string $tube
     * @param string $model
     * @param int    $foreignKey
     * @throws MinkAssertionFailedException
     */
    protected function assertJobExists(string $tube, string $model, int $foreignKey)
    {
        $exists = TableRegistry::getTableLocator()
            ->get('BeanstalkJobs')
            ->exists(
                [
                    'tube' => $tube,
                    'object_model' => $model,
                    'object_foreign_key' => $foreignKey
                ]
            );
        if ($exists !== true) {
            throw new MinkAssertionFailedException(
                sprintf(
                    "%s failed : %s\nat %s",
                    __FUNCTION__,
                    "failed to assert that the job exists",
                    $this->getAssertLocation()
                )
            );
        }
        $this->outputRunningFunction();
    }

    /**
     * Vérifi si un element donné par $css possède la classe css $class
     * @param string $css
     * @param string $class
     * @return void
     * @throws DriverException
     * @throws MinkAssertionFailedException
     * @throws UnsupportedDriverActionException
     */
    protected function assertElementHasClass(string $css, string $class)
    {
        $xpath = $this->cssToXpath($css);
        $elements = $this->driver->find($xpath);
        if (count($elements) === 0) {
            throw new MinkAssertionFailedException(
                sprintf(
                    "%s failed : %s\nat %s",
                    __FUNCTION__,
                    "the element does not exists",
                    $this->getAssertLocation()
                )
            );
        }
        foreach ($elements as $element) {
            $classes = explode(' ', $element->getAttribute('class') ?: []);
            if (in_array($class, $classes)) {
                return;
            }
        }
        throw new MinkAssertionFailedException(
            sprintf(
                "%s failed : %s\nat %s",
                __FUNCTION__,
                "failed to assert that the element has class",
                $this->getAssertLocation()
            )
        );
    }
}
