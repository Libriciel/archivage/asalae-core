<?php

/**
 * AsalaeCore\MinkSuite\MinkCaseDatabaseTrait
 */

namespace AsalaeCore\MinkSuite;

use Cake\Datasource\EntityInterface;
use Cake\ORM\Locator\TableLocator;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;

/**
 * Methodes de manipulation de base de données
 *
 * @category TestSuite
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2023, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin MinkCase
 */
trait MinkCaseDatabaseTrait
{
    /**
     * TableRegistry::getTableLocator()->get($modelName)
     * @param string $modelName
     * @return Table
     */
    protected function databaseGetModel(string $modelName): Table
    {
        /** @var TableLocator $loc */
        $loc = TableRegistry::getTableLocator();
        return $loc->get($modelName);
    }

    /**
     * Effectu un find sur $modelName
     * @param string $modelName
     * @param array  $conditions
     * @return Query
     */
    protected function databaseFind(string $modelName, array $conditions = []): Query
    {
        $query = $this->databaseGetModel($modelName)->find();
        if ($conditions) {
            $query->where($conditions);
        }
        return $query;
    }

    /**
     * Effectu un find sur $modelName
     * @param string $modelName
     * @param array  $conditions
     * @return EntityInterface|null
     */
    protected function databaseFindFirst(string $modelName, array $conditions = []): ?EntityInterface
    {
        return $this->databaseFind($modelName, $conditions)->first();
    }

    /**
     * Effectu un find sur $modelName
     * @param string $modelName
     * @param array  $conditions
     * @return EntityInterface
     * @throw RecordNotFoundException
     */
    protected function databaseFindFirstOrFail(string $modelName, array $conditions = []): EntityInterface
    {
        return $this->databaseFind($modelName, $conditions)->firstOrFail();
    }

    /**
     * delete all $modelName where $conditions
     * @param string $modelName
     * @param array  $conditions
     * @return int delete count
     * @throw RecordNotFoundException
     */
    protected function databaseDeleteIfExists(string $modelName, array $conditions = []): int
    {
        $model = $this->databaseGetModel($modelName);
        $entities = $model->find()->where($conditions);
        $count = 0;
        foreach ($entities as $entity) {
            if ($model->delete($entity)) {
                $count++;
            }
        }
        return $count;
    }

    /**
     * delete all $modelName where $conditions
     * @param string $modelName
     * @param array  $conditions
     * @return int delete count
     * @throw RecordNotFoundException
     */
    protected function databaseDeleteAll(string $modelName, array $conditions = []): int
    {
        $model = $this->databaseGetModel($modelName);
        return $model->deleteAll($conditions);
    }
}
