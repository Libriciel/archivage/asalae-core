<?php

/**
 * AsalaeCore\MinkSuite\ChromeBrowser
 */

namespace AsalaeCore\MinkSuite;

use DMore\ChromeDriver\ChromeBrowser as DMoreChromeBrowser;
use ReflectionClass;
use ReflectionException;

/**
 * Wrapper pour donner accès à client
 *
 * @category TestSuite
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2022, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin MinkCase
 */
class ChromeBrowser
{
    /**
     * @var DMoreChromeBrowser
     */
    private DMoreChromeBrowser $browser;
    /**
     * @var ReflectionClass
     */
    private ReflectionClass $reflection;

    /**
     * Constructeur
     * @param string $url
     * @param null   $socket_timeout
     */
    public function __construct($url, $socket_timeout = null)
    {
        $this->browser = new DMoreChromeBrowser($url, $socket_timeout);
        $this->reflection = new ReflectionClass(get_class($this->browser));
    }

    /**
     * Appel les fonctions privés de ChromeBrowser
     * @param string $name
     * @param array  $args
     * @return mixed
     * @throws ReflectionException
     */
    public function __call(string $name, array $args)
    {
        $method = $this->reflection->getMethod($name);
        $method->setAccessible(true); // NOSONAR

        return $method->invokeArgs($this->browser, $args);
    }

    /**
     * Getter
     * @param string $name
     * @return mixed
     * @throws ReflectionException
     */
    public function __get(string $name)
    {
        $property = $this->reflection->getProperty($name);
        $property->setAccessible(true); // NOSONAR
        return $property->getValue($this->browser);
    }
}
