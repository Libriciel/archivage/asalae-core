<?php

/**
 * AsalaeCore\MinkSuite\MinkSuite
 */

namespace AsalaeCore\MinkSuite;

use AsalaeCore\Console\ConsoleIo;
use AsalaeCore\Exception\GenericException;
use Behat\Mink\Mink;
use Behat\Mink\Session;
use Cake\Core\Configure;
use Cake\Error\Debugger;
use DMore\ChromeDriver\HttpClient;
use ErrorException;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;
use Throwable;
use WebSocket\TimeoutException;

/**
 * Lance une suite de tests fonctionnels mink
 *
 * @category TestSuite
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2022, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class MinkSuite
{
    const ATTEMPT_LIMIT = 3;
    /**
     * @var Mink instance de Mink
     */
    private Mink $mink;
    /**
     * @var ConsoleIo
     */
    private ConsoleIo $io;
    /**
     * @var array
     */
    private array $options;
    /**
     * @var string
     */
    private string $chromeUrl;
    /**
     * @var bool
     */
    private bool $testDone = false;

    /**
     * Contructeur
     * @param string         $chromeUrl
     * @param array          $options
     * @param ConsoleIo|null $io
     */
    public function __construct(string $chromeUrl, array $options = [], ConsoleIo $io = null)
    {
        $this->mink = self::newMink($chromeUrl);
        $this->chromeUrl = $chromeUrl;
        $this->options = $options + ['pause' => 0];
        $this->io = $io ?: new ConsoleIo();
    }

    /**
     * Destructeur
     */
    public function __destruct()
    {
        $this->unloadAppLocal();
    }

    /**
     * Getter
     * @return Mink
     */
    public function getMink(): Mink
    {
        return $this->mink;
    }

    /**
     * Créé un app_local.json avec le datasource mink comme default
     * path_to_local.php est backupé en path_to_local.php.bak
     * @return void
     */
    public function loadAppLocal(): void
    {
        $pathToLocal = Configure::read('App.paths.path_to_local_config');
        if (is_file($pathToLocal . '.bak')) {
            trigger_error("Path to local.bak already exists");
            return;
        }
        if (!$pathToLocal || !is_file($pathToLocal)) {
            throw new GenericException("Path to local not found");
        }
        $localJson = include $pathToLocal;
        $json = json_decode(file_get_contents($localJson), true);
        rename($pathToLocal, $pathToLocal . '.bak');

        register_shutdown_function([$this, 'unloadAppLocal']);
        if (function_exists('pcntl_signal')) { // intercept CTRL + C
            pcntl_signal(2, [$this, 'unloadAppLocal']);
        }

        $json['Datasources_old'] = $json['Datasources'];
        $json['Datasources'] = [
            'default' => $json['Datasources']['mink'],
            'mink' => $json['Datasources']['mink'],
        ];
        $json['App']['paths']['data_old'] = $json['App']['paths']['data'];
        $json['App']['paths']['data'] = ROOT . '/data/mink';
        Configure::write('App.paths.data', ROOT . '/data/mink');
        $jsonPath = CONFIG . 'app_local_mink.json';
        file_put_contents($jsonPath, json_encode($json, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));
        file_put_contents($pathToLocal, "<?php return CONFIG . 'app_local_mink.json';?>");
    }

    /**
     * Remet le fichier path_to_local.php comme à l'origine
     * @return void
     */
    public function unloadAppLocal(): void
    {
        $pathToLocal = Configure::read('App.paths.path_to_local_config');
        if (!is_file($pathToLocal . '.bak')) {
            return;
        }
        if (is_file(include $pathToLocal)) {
            unlink(include $pathToLocal);
        }
        if (is_file($pathToLocal)) {
            unlink($pathToLocal);
        }
        if (is_file(TMP . 'app_local_mink.json')) {
            unlink(TMP . 'app_local_mink.json');
        }
        rename($pathToLocal . '.bak', $pathToLocal);
    }

    /**
     * Lance les tests liés à un fichier de test Mink
     * @param string|null $filename
     * @return bool
     * @throws Exception|Throwable
     */
    public function run(string $filename = null): bool
    {
        // on ne fait pas plusieurs essais si on a spécifié un nom de fichier
        $attempt = $filename ? self::ATTEMPT_LIMIT : 1;
        if (!$filename) {
            $filename = TESTS . 'MinkCase';
        }
        if (is_dir($filename)) {
            $minkCases = Filesystem::listFiles($filename);
        } else {
            $minkCases = [$filename];
        }

        if (!is_dir(TMP . 'mink')) {
            mkdir(TMP . 'mink');
        }

        $this->testDone = false;
        $success = true;
        foreach ($minkCases as $minkCase) {
            if (!preg_match('/Mink.php$/', $minkCase)) {
                continue;
            }
            $success = $success && $this->runTest($minkCase, $attempt);
        }
        if (!$this->testDone) {
            trigger_error('no mink test found');
        }

        return $success;
    }

    /**
     * Affiche le message d'erreur dans la console avec pointage sur le test mink
     * @param int    $num
     * @param string $message
     * @param string $file
     * @param int    $line
     * @return void
     * @noinspection PhpUnusedParameterInspection
     * @throws ErrorException
     */
    public function errorHandler($num, $message, $file, $line)
    {
        $trace = Debugger::trace(['start' => 3, 'depth' => 999, 'format' => 'array']);
        $trace = array_reverse($trace);
        $trace = array_splice($trace, 3);
        array_unshift(
            $trace,
            ['file' => $file, 'line' => $line]
        );
        foreach ($trace as $tr) {
            if (isset($tr['file'])
                && strpos($tr['file'], 'MinkCase.php') === false
                && strpos($tr['file'], 'MinkSuite.php') === false
            ) {
                $this->io->warning(
                    sprintf(
                        "%s\nat %s:%d",
                        $message,
                        $tr['file'],
                        $tr['line'] ?? ''
                    )
                );
                break;
            }
        }
        throw new ErrorException($message, 0, E_ERROR, $file, $line);
    }

    /**
     * Affiche le log navigateur si les options le permettent
     * @return void
     */
    private function debugLog()
    {
        if (empty($this->options['log_filename']) || !is_readable($this->options['log_filename'])) {
            return;
        }
        $this->io->out(file_get_contents($this->options['log_filename']));
    }

    /**
     * Execute le test
     * @param string $minkCase chemin du test mink
     * @param int    $attempt
     * @return bool vrai si au moins 1 test est passé
     */
    private function runTest(string $minkCase, int $attempt = 1): bool
    {
        $len = strlen(TESTS . 'MinkCase' . DS);
        $relativePath = substr(dirname($minkCase), $len);
        $relativeClassname = str_replace(DS, '\\', $relativePath)
            . '\\' . basename($minkCase, '.php');
        $classname = '\\' . Configure::read('App.namespace') . '\Test\MinkCase\\' . $relativeClassname;

        if (class_exists($classname) && in_array(MinkCaseInterface::class, class_implements($classname))) {
            /** @var MinkCaseInterface $instance */
            $instance = new $classname($relativeClassname, $this->mink, $this->options, $this->io);
            try {
                if ($instance->getSession()->isStarted()) {
                    $instance->getSession()->stop();
                }
                $instance->getSession()->start();
            } catch (Throwable $e) {
            }

            set_error_handler([$this, 'errorHandler']);
            if ($instance instanceof SetupMinkInterface) {
                $instance->build();
            }
            try {
                foreach (get_class_methods($instance) as $method) {
                    if (substr($method, 0, 4) !== 'mink') {
                        continue;
                    }
                    $this->testDone = true;
                    if (!$this->runCase($instance, $method)) {
                        return $this->handleTestFailed(
                            $minkCase,
                            sprintf(
                                "Test %s %s failed, retrying...",
                                $relativeClassname,
                                $method
                            ),
                            $attempt
                        );
                    }
                }
            } finally {
                if ($instance instanceof SetupMinkInterface) {
                    $instance->destroy();
                }
                restore_error_handler();
            }
        }
        return true;
    }

    /**
     * Effectue un des tests de la classe MinkCase
     * @param MinkCaseInterface $instance
     * @param string            $method
     * @return bool
     */
    private function runCase(MinkCaseInterface $instance, string $method): bool
    {
        $instance->pause = $this->options['pause'] * 1000000;
        try {
            $this->runMethod($instance, $method);
        } catch (ErrorException $e) {
            $this->showException('Error', $e);
            $this->extractDataFromBrowser($instance);
            return false;
        } catch (MinkAssertionFailedException $e) {
            $this->showException('Assertion failed', $e);
            $this->extractDataFromBrowser($instance);
            return false;
        } catch (Exception $e) {
            $this->showException('Exception', $e);
            $this->extractDataFromBrowser($instance);
            return false;
        }
        return true;
    }

    /**
     * Affiche une exception sous la forme :
     * $message: $e->getMessage() at $file:$line
     * @param string    $message
     * @param Exception $e
     * @return void
     */
    private function showException(string $message, Exception $e)
    {
        $this->io->err(
            sprintf(
                "%s: %s\nat %s:%d",
                $message,
                $e->getMessage(),
                $e->getFile(),
                $e->getLine()
            )
        );
    }

    /**
     * Sauvegarde un screenshot, la page HTML et affiche la réponse ajax
     * @param MinkCaseInterface $instance
     * @return void
     */
    private function extractDataFromBrowser(MinkCaseInterface $instance)
    {
        $path = $instance::getScreenshotPath();
        $this->debugLog();
        set_error_handler(fn() => null);
        $browserFailed = false;
        try {
            $instance->screenshot($path);
            $this->io->success('error screenshot: ' . $path);
        } catch (Throwable $f) {
            $this->io->error('failed to take a screenshot');
            $browserFailed = true;
        }

        if (!$browserFailed) {
            try {
                $html = $instance->getSession()->getPage()->getHtml();
                if ($html) {
                    $htmlPath = substr($path, 0, strlen($path) - 4) . '.html';
                    file_put_contents($htmlPath, $html);
                    $this->io->success('html file: ' . $htmlPath);
                }
            } catch (Throwable $f) {
                $this->io->error('failed to save the html file');
                $browserFailed = true;
            }
        }

        $this->io->warning('last ajax response: ', 0);
        try {
            if (!$browserFailed) {
                $debugAjax = $instance->debugAjax() ?: $instance->lastRequest;
            } else {
                $debugAjax = $instance->lastRequest;
            }
        } catch (Throwable $f) {
            $debugAjax = $instance->lastRequest;
        }
        $this->io->info(Debugger::exportVarAsPlainText($debugAjax));
        restore_error_handler();
    }

    /**
     * Plusieurs tentatives, en cas de plantage du navigateur
     * @param MinkCaseInterface $instance
     * @param string            $method
     * @return void
     * @throws ErrorException
     * @throws MinkAssertionFailedException
     * @throws TimeoutException
     */
    private function runMethod(MinkCaseInterface $instance, string $method)
    {
        $attempt = 1;
        do {
            try {
                $instance->test($method);
                break;
            } catch (TimeoutException $e) {
                if ($attempt <= self::ATTEMPT_LIMIT) {
                    $this->io->err('Browser crashed, retry in 1s...');
                    sleep(1); // NOSONAR
                    $attempt++;
                    continue;
                }
                throw $e;
            }
        } while (true);
    }

    /**
     * Nouvelle instance de mink avec une nouvelle session
     * @param string $chromeUrl
     * @return Mink
     */
    public static function newMink(string $chromeUrl): Mink
    {
        $driver = new ChromeDriver(
            $chromeUrl,
            new HttpClient(),
            Configure::read('App.fullBaseUrl'),
            ['socketTimeout' => 1]
        );
        $session = new Session($driver);
        $session->restart();
        return new Mink([BROWSER_SESSION => $session]);
    }

    /**
     * En cas d'échec d'un test, on peut réessayer
     * @param string $minkCase chemin du test mink
     * @param string $errorMsg
     * @param int    $attempt
     * @return bool
     */
    private function handleTestFailed(string $minkCase, string $errorMsg, int $attempt)
    {
        if ($attempt >= self::ATTEMPT_LIMIT) {
            return false;
        } else {
            $this->io->out($errorMsg);
            $this->mink = self::newMink($this->chromeUrl);
            return $this->runTest($minkCase, $attempt + 1);
        }
    }
}
