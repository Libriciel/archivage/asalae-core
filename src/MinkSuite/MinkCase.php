<?php

/**
 * AsalaeCore\MinkSuite\MinkCase
 */

namespace AsalaeCore\MinkSuite;

use AsalaeCore\Console\ConsoleIo as CoreConsoleIo;
use AsalaeCore\Error\ContextDebugger;
use Behat\Mink\Driver\DriverInterface;
use Behat\Mink\Element\NodeElement;
use Behat\Mink\Exception\DriverException;
use Behat\Mink\Exception\UnsupportedDriverActionException;
use Behat\Mink\Mink;
use Behat\Mink\Selector\CssSelector;
use Behat\Mink\Selector\SelectorInterface;
use Behat\Mink\Selector\SelectorsHandler;
use Behat\Mink\Session;
use Cake\Console\ConsoleIo as CakeConsoleIo;
use Cake\Error\Debugger;
use Cake\Http\Client;
use Cake\Http\Client\Response;
use DateTime;
use Error;
use ErrorException;
use Exception;
use ReflectionClass;

/**
 * Lance une suite de tests fonctionnels mink
 *
 * @category TestSuite
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2022, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class MinkCase implements MinkCaseInterface, SetupMinkInterface
{
    use MinkBeanstalkTrait;
    use MinkCaseAssertTrait;
    use MinkCaseDatabaseTrait;
    use MinkCaseDoTrait;
    use MinkCaseDomTrait;
    use MinkCaseFileTrait;
    
    const UPLOAD_URL = 'https://curl.libriciel.fr';

    /**
     * @var int
     */
    public int $pause = 0;
    /**
     * @var string
     */
    public static string $tmpDir;
    /**
     * @var array
     */
    public array $workers = [];
    /**
     * @var array
     */
    public array $lastRequest = [];
    /**
     * @var DriverInterface|ChromeDriver
     */
    protected DriverInterface $driver;
    /**
     * @var string nom relatif de la classe
     */
    protected string $name;
    /**
     * @var Mink instance de Mink
     */
    protected Mink $mink;
    /**
     * @var Session
     */
    protected Session $session;
    /**
     * @var CakeConsoleIo
     */
    protected CakeConsoleIo $io;
    /**
     * @var array
     */
    protected array $options;
    /**
     * @var string
     */
    private string $lastOutput = '';

    /**
     * Contructeur
     * @param string             $name
     * @param Mink               $mink
     * @param array              $options
     * @param CakeConsoleIo|null $io
     */
    public function __construct(string $name, Mink $mink, array $options = [], CakeConsoleIo $io = null)
    {
        $this->name = $name;
        $this->mink = $mink;
        $this->options = $options;
        $this->io = $io ?: new CoreConsoleIo();
        $this->session = $mink->getSession(BROWSER_SESSION);
        $this->driver = $this->session->getDriver();
    }

    /**
     * Donne le chemin vers le dossier temporaire (dossier de téléchargement)
     * @return string
     */
    public static function getTmpDir(): string
    {
        if (empty(self::$tmpDir)) {
            self::$tmpDir = sys_get_temp_dir() . DS . 'mink';
        }
        return self::$tmpDir;
    }

    /**
     * Donne le chemin du screenshot d'erreur pour MinkSuite
     * @return string
     */
    public static function getScreenshotPath(): string
    {
        return TMP . 'mink' . DS . (new DateTime())->format('Ymdhis_')
            . str_replace('\\', '_', static::class) . '.png';
    }

    /**
     * Execute un des tests de la class
     * @param string $method
     * @return void
     * @throws MinkAssertionFailedException|Exception
     */
    public function test(string $method): void
    {
        $refl = new ReflectionClass(get_called_class());
        $relativeFilename = substr($refl->getFileName(), strlen(ROOT . DS));
        $msg = sprintf('testing %s %s', $relativeFilename, $method);
        // si on a pas quiet, on affiche des points, il faut revenir à la ligne
        $this->io->out($msg, ($this->options['quiet'] ?? false) ? 0 : 1);
        try {
            $this->setUp();
            $this->$method();
            $this->tearDown();
        } catch (Exception $e) {
            $this->io->err(
                sprintf(
                    ' <error>failed: %s</error>',
                    $this->getLastLine($e, $method)
                )
            );
            throw $e;
        }
        $this->io->out(' <success>success</success>');
    }

    /**
     * Fait un screenshot
     * @param string|null $path
     * @return void
     * @throws Exception
     */
    public function screenshot(string $path = null): void
    {
        $path = $path ?: self::getScreenshotPath();
        if (!is_dir(dirname($path))) {
            mkdir(dirname($path), 0777, true);
        }
        $this->driver->captureScreenshot($path);
        if (!env('LINES')) {
            exec("echo \"feh $path\" | xargs echo -n | xclip -selection clipboard >/dev/null 2>/dev/null");
        }
        $this->io->success('screenshot file: ' . $path);
    }

    /**
     * Fait un screenshot et l'envoi sur un serveur
     * @param string|null $path
     * @param string      $url
     * @return void
     * @throws Exception
     */
    public function screenshotUpload(
        string $path = null,
        string $url = self::UPLOAD_URL
    ): void {
        $path = $path ?: self::getScreenshotPath();
        $this->screenshot($path);

        $response = $this->upload($path, $url);
        $status = $response->getStatusCode();
        $response = $response->getStringBody();
        if ($status === 200) {
            $this->io->success('screenshot url: ' . trim($response));
        } else {
            $this->io->err(sprintf('error %d: %s', $status, $response));
        }
    }

    /**
     * Sauvegarde le html dans un fichier
     * @param string|null $path
     * @return void
     */
    public function exportHtml(string $path = null): void
    {
        $path = $path ?: (basename(self::getScreenshotPath(), '.png') . '.html');
        $html = $this->getSession()->getPage()->getHtml();
        if (!$html) {
            $this->io->err('failed to export html');
        }
        file_put_contents($path, $html);
        $this->io->success('html file: ' . $path);
    }

    /**
     * Upload le html
     * @param string|null $path
     * @param string      $url
     * @return void
     */
    public function htmlUpload(
        string $path = null,
        string $url = self::UPLOAD_URL
    ): void {
        $path = $path ?: (basename(self::getScreenshotPath(), '.png') . '.html');
        $this->exportHtml($path);

        $response = $this->upload($path, $url);
        $status = $response->getStatusCode();
        $response = $response->getStringBody();
        if ($status === 200) {
            $this->io->success('html url: ' . trim($response));
        } else {
            $this->io->err(sprintf('error %d: %s', $status, $response));
        }
    }

    /**
     * Upload d'un fichier ($filename) vers $url
     * @param string $filename
     * @param string $url
     * @return Response|null
     */
    public function upload(string $filename, string $url = self::UPLOAD_URL): ?Response
    {
        $client = new Client();
        $file = fopen($filename, 'r');
        if (!$file) {
            return null;
        }
        $response = $client->post(
            $url,
            ['file' => $file]
        );
        fclose($file);
        return $response;
    }

    /**
     * Récupère le status ajax de la page actuelle (dernière requête)
     * @return array|null
     */
    public function debugAjax(): ?array
    {
        $ajax = $this->session->evaluateScript(
            <<<js
            if (typeof minkLastAjaxResponse === 'undefined') {
                return;
            }
            var contentType = minkLastAjaxResponse?.response?.getResponseHeader('Content-Type');
            JSON.stringify({
                status: minkAjaxRunning || customAjaxRunning ? 'a request has not yet received a response' : 'ok',
                method: minkLastAjaxResponse?.request?.method,
                url: minkLastAjaxResponse?.request?.url,
                code: minkLastAjaxResponse?.response?.status,
                success: minkLastAjaxResponse?.response?.getResponseHeader('X-Asalae-Success'),
                step: minkLastAjaxResponse?.response?.getResponseHeader('X-Asalae-Step'),
                message: contentType?.indexOf('application/json') > -1
                    ? (minkLastAjaxResponse?.response?.responseJSON)
                    : minkLastAjaxResponse?.response?.responseText?.substr(0, 300)
            });
js
        );
        return json_decode((string)$ajax, true);
    }

    /**
     * Donne la session lié au MinkCase
     * @return Session
     */
    public function getSession(): Session
    {
        return $this->session;
    }

    /**
     * Ajout de javascript pour capturer les evenements ajax et de modale
     * @return void
     */
    protected function mountAjaxEvents(): void
    {
        $this->session->executeScript(
            "var minkAjaxRunning = false;
            var customAjaxRunning = false;
            var minkFileUploading = false;
            var minkLastAjaxResponse = {};
            $(document).off('.mink')
                .on('ajaxStart.mink', function(event, response) {
                    minkAjaxRunning = true;
                    minkLastAjaxResponse = {complete: false};
                })
                .on('ajaxStop.mink', function(event, response) {
                    setTimeout(() => minkAjaxRunning = false, 100);
                })
                .on('ajaxSuccess.mink', function(event, response, request) {
                    minkLastAjaxResponse = {
                        success: true,
                        complete: false
                    };
                })
                .on('ajaxError.mink', function(event, response, request) {
                    minkLastAjaxResponse = {
                        success: false,
                        complete: false
                    };
                })
                .on('ajaxComplete.mink', function(event, response, request) {
                    setTimeout(
                        function() {
                            minkLastAjaxResponse.complete = true;
                            minkLastAjaxResponse.response = response;
                            minkLastAjaxResponse.request = request;
                        },
                        0
                    );
                })
                .on('ajax.begin.mink', function() {
                    customAjaxRunning = true;
                })
                .on('ajax.complete.mink', function() {
                    customAjaxRunning = false;
                })
                .on('filesSubmitted.mink', function() {
                    minkFileUploading = true;
                })
                .on('complete.mink', function() {
                    minkFileUploading = false;
                });

            function minkModalOpenned() {
                return minkAjaxRunning === false && $('.modal:visible').length > 0;
            }"
        );
    }

    /**
     * Returns selectors handler.
     * @param string $selector
     * @return CssSelector|SelectorInterface
     */
    protected function getSelector(string $selector = 'css'): CssSelector
    {
        $selectorHandler = new SelectorsHandler();
        return $selectorHandler->getSelector($selector);
    }

    /**
     * Donne le 1er élement d'un xpath ou null si il n'existe pas
     * @param string $xpath
     * @return NodeElement|null
     * @throws DriverException
     * @throws UnsupportedDriverActionException
     */
    protected function findFirst(string $xpath): ?NodeElement
    {
        return current($this->driver->find($xpath)) ?: null;
    }

    /**
     * Converti un chemin css en xpath
     * @param string $css
     * @return string
     */
    protected function cssToXpath(string $css): string
    {
        return $this->getSelector()->translateToXPath($css);
    }

    /**
     * Donne le dernier element de formulaire lié au premier label visible qui
     * a un contenu qui correspond à $label
     * @param string $label
     * @param bool   $multiple
     * @return NodeElement|NodeElement[]|null
     * @throws DriverException
     * @throws UnsupportedDriverActionException
     */
    protected function findVisibleFormElement(string $label, bool $multiple = false)
    {
        $labels = $this->driver->find(
            "descendant-or-self::*[@class and contains(concat(' ', normalize-space(@class), ' '), ' fake-label ')]"
        );
        foreach ($labels as $labelNode) {
            if (!$labelNode->isVisible() || $labelNode->getText() !== $label) {
                continue;
            }
            $inputs = $labelNode->getParent()->findAll('xpath', '//*[@type="file"]');
            if ($inputs && $multiple) {
                return $inputs;
            }
            $input = end($inputs);
            if ($input) {
                return $input;
            }
        }
        $labels = $this->driver->find("//label");
        foreach ($labels as $labelNode) {
            if (!$labelNode->isVisible() || $labelNode->getText() !== $label) {
                continue;
            }
            $inputs = $labelNode->getParent()->findAll('xpath', '//*[@name]');
            if ($inputs && $multiple) {
                return $inputs;
            }
            $input = end($inputs);
            if ($input) {
                return $input;
            }
        }

        return null;
    }

    /**
     * Donne le dernier element de formulaire lié au premier label visible qui
     * a un contenu qui correspond à $label
     * @param string $label
     * @return NodeElement|null
     * @throws DriverException
     * @throws UnsupportedDriverActionException
     */
    protected function findNamelessFormElement(string $label): ?NodeElement
    {
        $labels = $this->driver->find(
            "//label|"
            . "descendant-or-self::*[@class and contains(concat(' ', normalize-space(@class), ' '), ' fake-label ')]"
        );
        foreach ($labels as $labelNode) {
            if (!$labelNode->isVisible() || $labelNode->getText() !== $label) {
                continue;
            }
            $inputs = $labelNode->getParent()->findAll('xpath', '//input|//select|//textarea');
            $input = end($inputs);
            if ($input) {
                return $input;
            }
        }
        return null;
    }

    /**
     * Affiche la fonction executée
     * @return void
     */
    protected function outputRunningFunction(): void
    {
        if (!empty($this->options['quiet'])) {
            return;
        }
        $caller = Debugger::trace(['start' => 2, 'depth' => 3, 'format' => 'array']);
        if (strpos($caller[0]['file'], '/TestSuite/Mink/') !== false) {
            return;
        }
        if (!empty($this->options['verbose'])) {
            $context = $this->extractContext($caller[0]['file'], $caller[0]['line']);
            $this->io->out(sprintf('    %s', $context));
        } else {
            $this->lastOutput .= '.';
            $this->io->out('.', 0);
            if (strlen($this->lastOutput) >= 100) {
                $this->io->out();
                $this->lastOutput = '';
            }
        }
    }

    /**
     * Lancé avant un test
     */
    public function setUp(): void
    {
    }

    /**
     * Lancé après un test
     */
    public function tearDown(): void
    {
    }

    /**
     * Donne la ligne qui a provoqué l'exception
     * @param Exception $e
     * @param string    $method
     * @return string
     */
    private function getLastLine(Exception $e, string $method): string
    {
        $traces = $e->getTrace();
        array_unshift(
            $traces,
            ['file' => $e->getFile(), 'line' => $e->getLine()]
        );
        foreach ($traces as $trace) {
            if (!isset($trace['file']) || !isset($trace['line'])) {
                continue;
            }
            $realTrace = ContextDebugger::extractMethod($trace['file'], $trace['line']);
            if (($realTrace['class'] ?? null) === get_class($this)
                && ($realTrace['function'] ?? null) === $method
            ) {
                return $this->extractContext($realTrace['file'], $realTrace['line']);
            }
        }
        return '';
    }

    /**
     * Donne la ligne $line dans le fichier $filename
     * @param string $filename
     * @param int    $line
     * @return string
     */
    private function extractContext(string $filename, int $line)
    {
        $ctx = htmlspecialchars_decode(
            ContextDebugger::getContext($filename, $line, 1),
            ENT_QUOTES | ENT_SUBSTITUTE
        );
        $context = substr($ctx, strpos($ctx, '<b>') + 3);
        return trim(substr($context, 0, strrpos($context, '</b>')));
    }

    /**
     * Affiche un message en mode verbose
     * @param string $message
     * @return void
     */
    protected function out(string $message): void
    {
        if (empty($this->options['quiet']) && !empty($this->options['verbose'])) {
            $this->io->out(sprintf('    %s', $message));
        }
    }

    /**
     * Effectue un eval des lignes de classname jusqu'à $to ($from optionnel)
     *
     * Il est possible de remplacer le $from par un commentaire: "// @from runLines"
     * Il est possible de remplacer le $to par un commentaire: "// @to runLines"
     *
     * @param string $classname ou uri du fichier
     * @param mixed  ...$args   array de variables, ou int (bornes from et to)
     * @return array
     * @throws ErrorException
     */
    public function runLines(string $classname, ...$args): array
    {
        // extraction des vars dans les arguments
        $vars = [];
        foreach ($args as $value) {
            if (is_array($value)) {
                $vars = $value;
                break;
            }
        }
        unset($vars['success'], $vars['error']);

        // classname ou filename
        if (is_file($classname)) {
            $filename = $classname;
        } elseif ($path = self::findFilenameByClassname($classname)) {
            $filename = $path;
        } else {
            $reflector = new ReflectionClass(static::class);
            $filename = $reflector->getFileName();
        }
        $lines = file($filename);
        $to = 0;

        // Extraction du from et du to depuis les commentaires
        foreach ($lines as $line => $content) {
            if (strpos($content, '@from runLines') !== false) {
                $from = $line + 1;
            } elseif (strpos($content, '@to runLines') !== false) {
                $to = $line + 1;
                break;
            }
        }

        // extraction du from et du to depuis les arguments
        foreach ($args as $value) {
            if (is_int($value)) {
                if (empty($to)) {
                    $to = $value;
                } elseif (empty($from)) {
                    $from = $value;
                }
            }
        }
        unset($args);
        if (empty($from)) {
            $from = 0;
        }

        // on s'assure que to soit supérieur à from
        if ($to < $from) {
            $invert = $to;
            $to = $from;
            $from = $invert;
        }

        $multiLineStatement = '';
        $returned = [];

        // on parse le fichier
        set_error_handler(
            function ($errno, $errstr, $errfile, $errline) {
                throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
            }
        );
        $currentLine = 0;
        try {
            $this->evalLines(
                $lines,
                $from,
                $to,
                $vars,
                $multiLineStatement,
                $returned,
                $currentLine
            );
        } /** @noinspection PhpRedundantCatchClauseInspection */ catch (ErrorException | Error $e) {
            throw new ErrorException(
                sprintf(
                    "An error occurred while evaluating the content: %s\n%s",
                    trim($multiLineStatement ?? ''),
                    $e->getMessage()
                ),
                0,
                E_ERROR,
                $filename,
                $currentLine,
                $e
            );
        } catch (Exception $e) {
            $returned['error'] = sprintf(
                "An exception occurred while evaluating the content: %s\n%s\n  at %s:%d",
                trim($multiLineStatement ?? ''),
                $e->getMessage(),
                $filename,
                $currentLine
            );
            restore_error_handler();
            trigger_error($returned['error']);
            set_error_handler(fn() => null);
        } finally {
            restore_error_handler();
        }
        return $this->cleanReturnVars($returned);
    }

    /**
     * Renvoi les variables calculés durant l'eval
     * @param array $returned
     * @return array
     */
    private function cleanReturnVars(array $returned): array
    {
        $unsetList = [
            'classname',
            'to',
            'from',
            'vars',
            'filename',
            'invert',
            'lines',
            'mink',
            'includeLineOn',
            'insideMinkTest',
            'skipNextLine',
            'multiLineStatement',
            'usesList',
            'returnVars',
            'returned',
            'content',
            'line',
            'currentLine',
        ];
        foreach ($unsetList as $varname) {
            unset($returned[$varname]);
        }
        $returned['success'] = empty($returned['error']);
        return $returned;
    }

    /**
     * Evalue les $lines de $from à $to
     * @param array  $lines              fichier mink sous forme d'array
     * @param int    $from               ligne de départ
     * @param int    $to                 ligne d'arrivé
     * @param array  $vars               variables
     * @param string $multiLineStatement permet de debug la dernière ligne
     * @param array  $returned           variables calculés par le eval
     * @param int    $currentLine
     * @return void
     * @throw ErrorException
     * @throw Exception
     */
    private function evalLines(
        array $lines,
        int $from,
        int $to,
        array &$vars,
        string &$multiLineStatement,
        array &$returned,
        int &$currentLine
    ) {
        extract($vars);
        /** @noinspection PhpUnusedLocalVariableInspection pour le eval */
        $mink = $this;
        $usesList = '';
        $returnVars = 'return compact(array_keys(get_defined_vars()));';
        $includeLineOn = false;
        $insideMinkTest = false;
        $skipNextLine = false;
        foreach ($lines as $line => $content) {
            if ($skipNextLine) {
                $skipNextLine = false;
                continue;
            }
            $currentLine = $line + 1;
            if (preg_match('/^namespace .*;$/', $content)) {
                $includeLineOn = true;
            } elseif ($includeLineOn && preg_match('/^use .*\\\\.*;$/', $content)) {
                $usesList .= $content;
            } elseif (preg_match('/^class .*$/', $content)) {
                $includeLineOn = false;
            } elseif (preg_match('/public function mink.*\(\)/', $content)) {
                $insideMinkTest = true;
                $skipNextLine = true;
            } elseif ($insideMinkTest && preg_match('/^ {4}}$/', $content)) {
                $insideMinkTest = false;
            } elseif ($insideMinkTest && $currentLine >= $from) {
                $multiLineStatement .= $content;
                if (preg_match('/.*;$/', $content)) {
                    $returned = eval($usesList . $multiLineStatement . $returnVars); // NOSONAR eval obligatoire
                    $multiLineStatement = '';
                }
            }
            if ($currentLine >= $to) {
                break;
            }
        }
    }

    /**
     * Trouve un filename à partir d'un classname
     * ex: "Cake\Core\Configure" ou "Configure" = chemin vers Configure.php
     * @param string $classname
     * @return string
     */
    private static function findFilenameByClassname(string $classname): string
    {
        foreach (self::listAllClasses() as $class => $path) {
            if (preg_match("/(^|.+\\\\)$classname\$/", $class)) {
                return realpath($path);
            }
        }
        return '';
    }

    /**
     * @var array cache la liste des classes
     */
    private static $classes = [];

    /**
     * Donne la liste de toutes les classes données par l'autoloader de composer
     * @return array
     */
    private static function listAllClasses(): array
    {
        if (empty(self::$classes)) {
            $res = get_declared_classes();
            $autoloaderClassName = '';
            foreach ($res as $className) {
                if (strpos($className, 'ComposerAutoloaderInit') === 0) {
                    $autoloaderClassName = $className; // ComposerAutoloaderInit<token>
                    break;
                }
            }
            /** @noinspection PhpUndefinedMethodInspection */
            $classLoader = $autoloaderClassName::getLoader();
            self::$classes = $classLoader->getClassMap();
        }
        return self::$classes;
    }
}
