<?php

/**
 * AsalaeCore\MinkSuite\MinkCaseDoTrait
 */

namespace AsalaeCore\MinkSuite;

use AsalaeCore\Exception\GenericException;
use AsalaeCore\Factory\Utility;
use AsalaeCore\Utility\DOMUtility;
use Behat\Mink\Exception\DriverException;
use Behat\Mink\Exception\ElementNotFoundException;
use Behat\Mink\Exception\UnsupportedDriverActionException;
use Cake\Core\Configure;
use Cake\Http\Client;
use Cake\Http\Cookie\Cookie;
use Cake\Routing\Router;
use Cake\Utility\Inflector;
use DateTime;
use ReflectionException;

/**
 * Exception lors d'un test fonctionnel
 *
 * @category TestSuite
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2022, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin MinkCase
 */
trait MinkCaseDoTrait
{
    /**
     * @var string selecteur css pour le user menu
     */
    protected $menuUserSelector = '#user-menu > button';
    /**
     * @var string selecteur xpath pour les éléments du user menu
     */
    protected $menuUserElementSelector = '//*[@id="user-menu"]/ul/li/*[self::button or self::a]';
    /**
     * @var string selecteur xpath pour le menu
     */
    protected $menuSelector = '//*[@id="header-menu-collapsable"]/ul/li/a';
    /**
     * @var string selecteur xpath pour les sous-menus
     */
    protected $submenuSelector = 'ul/li/a';

    /**
     * Se connecte à l'application
     * @param string $username
     * @param string $password
     * @return void
     * @throws DriverException
     * @throws UnsupportedDriverActionException
     */
    protected function doLogin(string $username = 'admin', string $password = 'admin'): void
    {
        $fullBaseUrl = Configure::read('App.fullBaseUrl');
        $len = mb_strlen($fullBaseUrl);
        $this->driver->visit($fullBaseUrl);
        $relativeUrl = trim(substr($this->driver->getCurrentUrl(), $len), '/');
        $currentUrl = implode(
            '/',
            array_map(
                fn($v) => Inflector::variable($v),
                explode('/', $relativeUrl)
            )
        );
        if (strpos($currentUrl, 'users/login') === false) {
            $this->mountAjaxEvents();
            return;
        }
        $usernameElement = $this->findFirst('//input[@name="username"]');
        $passwordElement = $this->findFirst('//input[@name="password"]');
        $buttonElement = $this->findFirst('//button[@type="submit"]');
        if (!$usernameElement) {
            throw new GenericException('username or password input not found');
        }
        $usernameElement->setValue($username);
        $passwordElement->setValue($password);
        $buttonElement->click();

        $this->mountAjaxEvents();
        $this->outputRunningFunction();
    }

    /**
     * Effectue un click sur le 1er élement indiqué par le css
     * @param string $css
     * @return void
     * @throws DriverException
     * @throws UnsupportedDriverActionException
     */
    protected function doClick(string $css): void
    {
        $xpath = $this->cssToXpath($css);
        $element = $this->findFirst($xpath);
        $element->click();
        $this->outputRunningFunction();
    }

    /**
     * Attend qu'une modale s'ouvre
     * @param int $wait timeout in milliseconds
     * @return void
     */
    protected function waitModalOpen(int $wait = 5000): void
    {
        $begin = microtime(true);
        $this->session->wait(
            $wait,
            "minkModalOpenned()"
        );
        if (microtime(true) - $begin > ($wait / 1000)) {
            trigger_error('waitModalOpen timeout occured');
        }
        $this->session->wait(400); // animation
        $this->outputRunningFunction();
    }

    /**
     * Attend qu'un upload se termine
     * @param int $wait timeout in milliseconds
     * @return void
     */
    protected function waitUploadComplete(int $wait = 5000): void
    {
        $this->session->wait(10);
        $begin = microtime(true);
        $this->session->wait(
            $wait,
            "minkFileUploading === false"
        );
        if (microtime(true) - $begin > ($wait / 1000)) {
            trigger_error('waitUploadComplete timeout occured');
        }
        $this->session->wait(400); // animation
        $this->outputRunningFunction();
    }

    /**
     * Attend qu'une requete ajax soit fini
     * @param int  $wait  timeout in milliseconds
     * @param bool $quiet
     * @return void
     */
    protected function waitAjaxComplete(int $wait = 5000, bool $quiet = false): void
    {
        $begin = microtime(true);
        $this->session->wait(
            $wait,
            "minkAjaxRunning === false && customAjaxRunning === false"
        );
        if (microtime(true) - $begin > ($wait / 1000)) {
            trigger_error('waitAjaxComplete timeout occured');
        }
        $this->session->wait(500); // laisse le temps de garnir la modale
        if (!$quiet) {
            $this->outputRunningFunction();
        }
        $this->lastRequest = $this->debugAjax() ?: [];
    }

    /**
     * Attend qu'une modale se ferme
     * @param int $wait timeout in milliseconds
     * @return void
     */
    protected function waitModalClose(int $wait = 5000): void
    {
        $begin = microtime(true);
        $this->session->wait(
            $wait,
            "$('.modal:visible').length === 0"
        );
        if (microtime(true) - $begin > ($wait / 1000)) {
            trigger_error('waitModalClose timeout occured');
        }
        $this->outputRunningFunction();
    }

    /**
     * Rempli un champ de formulaire
     * @param string $label
     * @param string $value
     * @return void
     * @throws DriverException
     * @throws UnsupportedDriverActionException
     */
    protected function setFormFieldValue(string $label, string $value): void
    {
        $element = $this->findVisibleFormElement($label);
        if ($element && $element->getAttribute('type') === 'file') {
            $this->requiredIdName('input[type="file"]');
            $element->attachFile($value);
            $this->removeRequiredIdName();
            $this->outputRunningFunction();
            return;
        } elseif ($element) {
            $element->setValue($value);
            if ($id = $element->getAttribute('id')) {
                $this->session->executeScript("$('#$id').trigger('chosen:updated');");
            }
            $this->pausable();
            $this->outputRunningFunction();
            return;
        }
        trigger_error(__FUNCTION__ . ' failed');
    }

    /**
     * Ajoute une valeur à un champ de formulaire
     * @param string $label
     * @param string $value
     * @return void
     * @throws DriverException
     * @throws UnsupportedDriverActionException
     * @throws ElementNotFoundException|ReflectionException
     */
    protected function appendToFieldValue(string $label, string $value): void
    {
        $element = $this->findVisibleFormElement($label);
        if (!$element) {
            trigger_error(__FUNCTION__ . ' failed');
            return;
        }
        $this->driver->appendToTextTypeValue($element->getXpath(), $value);
        $this->pausable();
        $this->outputRunningFunction();
    }

    /**
     * Rempli un champ de formulaire
     * @param string $label
     * @param string $value
     * @return void
     * @throws DriverException
     * @throws UnsupportedDriverActionException
     */
    protected function setNamelessFormFieldValue(string $label, string $value): void
    {
        $element = $this->findNamelessFormElement($label);
        if ($element && $element->getAttribute('type') === 'file') {
            $this->requiredIdName('input[type="file"]');
            $element->attachFile($value);
            $this->removeRequiredIdName();
            $this->outputRunningFunction();
            return;
        } elseif ($element) {
            $element->setValue($value);
            if ($id = $element->getAttribute('id')) {
                $this->session->executeScript("$('#$id').trigger('chosen:updated');");
            }
            $this->pausable();
            $this->outputRunningFunction();
            return;
        }
        trigger_error(__FUNCTION__ . ' failed');
    }

    /**
     * Rempli un champ de formulaire select en se basant sur le texte affiché de la valeur
     * @param string $label
     * @param string $text
     * @return void
     * @throws DriverException
     * @throws UnsupportedDriverActionException
     */
    protected function setFormSelectValueByText(string $label, string $text): void
    {
        $element = $this->findVisibleFormElement($label);

        if ($element) {
            $values = $element->findAll('xpath', '//option');
            foreach ($values as $value) {
                if ($value->getText() === $text) {
                    $element->setValue($value->getValue());
                    if ($id = $element->getAttribute('id')) {
                        $this->session->executeScript("$('#$id').trigger('chosen:updated');");
                    }
                    $this->pausable();
                    $this->outputRunningFunction();
                    return;
                }
            }
        }
        trigger_error(__FUNCTION__ . ' failed');
    }

    /**
     * Rempli un champs de formulaire multiple
     * @param string $label
     * @param array  $values
     * @return void
     * @throws DriverException
     * @throws UnsupportedDriverActionException
     */
    protected function setFormFieldValues(string $label, array $values): void
    {
        $elements = $this->findVisibleFormElement($label, true) ?: [];
        $element = end($elements);
        if (!$element) {
            trigger_error(__FUNCTION__ . ' failed');
        } elseif ($element->hasClass('chosenified')) {
            $id = $element->getAttribute('id');
            $chosenId = str_replace('-', '_', $id) . '_chosen';

            $closes = $this->driver->find('//*[@id="' . $chosenId . '" and @class="search-choice-close"]');
            foreach ($closes as $close) {
                $close->click();
                $element->getParent()->click(); // permet de revenir à l'état initial
            }

            foreach ($values as $value) {
                $this->session->executeScript(
                    <<<JS
$('#$chosenId .chosen-search-input').mousedown();
$('#$chosenId .active-result[data-option-array-index="' + $('#$id option[value="$value"]').index() + '"]').mouseup();
JS
                );
            }
            $this->pausable();
            $this->outputRunningFunction();
            return;
        } elseif ($element->hasClass('select2-hidden-accessible')) {
            $id = $element->getAttribute('id');
            $chosenId = 'select2-' . $id . '-results';

            $closes = $element->findAll(
                'xpath',
                '../span/span/span/ul/li/span[@class="select2-selection__choice__remove"]'
            );
            foreach ($closes as $close) {
                $close->click();
                $element->getParent()->click(); // permet de revenir à l'état initial
            }

            foreach ($values as $value) {
                $element->find('xpath', '../span')->click(); // ouvre les choix
                $choicesPath = '//*[@id="' . $chosenId
                    . '"]//li[contains(@class, "select2-results__option") and @aria-selected="false"]';
                $choices = $this->driver->find($choicesPath);
                foreach ($choices as $choice) {
                    $id = explode('-', $choice->getAttribute('id'));
                    if (end($id) === (string)$value) {
                        $choice->click();
                        continue 2;
                    }
                }
            }
            $this->pausable();
            $this->outputRunningFunction();
            return;
        }
        // checkbox multiple
        $setted = 0;
        foreach ($elements as $element) {
            if ($element->hasAttribute('type')
                && $element->getAttribute('type') === 'checkbox'
            ) {
                $inValues = in_array($element->getAttribute('value'), $values);
                if ($inValues && $element->isChecked() === false) {
                    $element->check();
                    $setted++;
                } elseif (!$inValues && $element->isChecked()) {
                    $element->uncheck();
                }
            }
        }
        if ($setted === count($values)) {
            return;
        }

        trigger_error(__FUNCTION__ . ' failed');
    }

    /**
     * Rempli un champ de formulaire en se basant sur l'id du champ
     * @param string $id
     * @param string $value
     * @return void
     * @throws DriverException
     * @throws UnsupportedDriverActionException
     */
    protected function setFormFieldValueById(string $id, string $value): void
    {
        $elements = $this->driver->find('//*[@id="' . $id . '"]');

        if (count($elements) === 1) {
            $element = end($elements);
            $element->setValue($value);
            $this->pausable();
            $this->outputRunningFunction();
            return;
        }

        trigger_error(__FUNCTION__ . ' failed');
    }


    /**
     * Rempli un champ de formulaire
     * @param string $selectorEngine css | xpath | named_partial | named_exact
     * @param string $locator
     * @param string $value
     * @return void
     * @throws DriverException
     * @throws UnsupportedDriverActionException
     */
    protected function setInputValue(string $selectorEngine, string $locator, string $value): void
    {
        $xpath = $selectorEngine !== 'xpath'
            ? $this->getSelector($selectorEngine)->translateToXPath($locator)
            : $locator;
        $element = $this->findFirst($xpath);
        if ($element) {
            $element->setValue($value);
            $this->pausable();
            return;
        }
        trigger_error(__FUNCTION__ . ' failed');
    }

    /**
     * Ajoute des ids et names aux élements choisis par $css
     * @param string $css
     * @return void
     */
    protected function requiredIdName(string $css): void
    {
        $css = addcslashes($css, "'");
        $this->session->executeScript(
            <<<JS
            $('$css').each(
                function() {
                    var id = 'mink-' + uuid.v4();
                    if (!$(this).attr('id')) {
                        $(this).attr('data-id-less', 'true');
                        $(this).attr('id', id);
                    }
                    if (!$(this).attr('name')) {
                        $(this).attr('data-name-less', 'true');
                        $(this).attr('name', id);
                    }
                }
            );
JS
        );
        $this->wait(10);
    }

    /**
     * Retire des ids et names des élements
     * @return void
     */
    protected function removeRequiredIdName(): void
    {
        $this->session->executeScript(
            <<<JS
            $('[data-id-less]').attr('id', '').removeAttr('id');
            $('[data-name-less]').attr('name', '').removeAttr('name');
JS
        );
    }

    /**
     * Click sur le bouton .accept de la modale active
     * @param int $wait
     * @return void
     * @throws DriverException
     * @throws UnsupportedDriverActionException
     */
    protected function submitModal(int $wait = 5000): void
    {
        $xpath = $this->cssToXpath('.modal-footer button.accept');
        $accepts = $this->driver->find($xpath);
        foreach (array_reverse($accepts) as $accept) {
            if ($accept->isVisible()) {
                $accept->click();
                $this->waitAjaxComplete($wait, true);
                $this->outputRunningFunction();
                return;
            }
        }
        trigger_error(__FUNCTION__ . ' failed');
    }

    /**
     * Click sur le bouton .close de la modale active
     * @param int $wait
     * @return void
     * @throws DriverException
     * @throws UnsupportedDriverActionException
     */
    protected function closeModal(int $wait = 5000): void
    {
        $xpath = $this->cssToXpath('.modal-footer button.cancel');
        $cancels = $this->driver->find($xpath);
        foreach (array_reverse($cancels) as $cancel) {
            if ($cancel->isVisible()) {
                $cancel->click();
                $this->waitAjaxComplete($wait, true);
                $this->outputRunningFunction();
                return;
            }
        }
        trigger_error(__FUNCTION__ . ' failed');
    }

    /**
     * Click sur le 1er élement du menu utilisateur qui correspond à $text
     * @param string $text
     * @return void
     * @throws DriverException
     * @throws UnsupportedDriverActionException
     */
    protected function doClickUserMenu(string $text): void
    {
        $this->doClick($this->menuUserSelector);
        $menuElements = $this->driver->find($this->menuUserElementSelector);
        foreach ($menuElements as $menuElement) {
            if ($menuElement->getText() === $text) {
                $menuElement->click();
                $this->outputRunningFunction();
                return;
            }
        }
        trigger_error(__FUNCTION__ . ' failed');
    }

    /**
     * Click sur le 1er element du menu qui correspond à $text1 puis clic sur
     * le 1er element qui est dans le sous menu lié à $text1 qui correspond à $text2
     * @param string $text1
     * @param string $text2
     * @return void
     * @throws DriverException
     * @throws UnsupportedDriverActionException
     */
    protected function doClickMenu(string $text1, string $text2): void
    {
        $menuItems = $this->driver->find($this->menuSelector);
        foreach ($menuItems as $menuItem) {
            if ($menuItem->getText() === $text1) {
                $menu = $menuItem->getParent();
                $menuItem->click();
                break;
            }
        }
        if (!isset($menu)) {
            $this->outputRunningFunction();
            return;
        }
        $submenuItems = $menu->findAll('xpath', $this->submenuSelector);
        foreach ($submenuItems as $submenuItem) {
            if ($submenuItem->getText() === $text2) {
                $submenuItem->click();
                $this->waitAjaxComplete(5000, true);
                $this->outputRunningFunction();
                return;
            }
        }
        trigger_error(__FUNCTION__ . ' failed');
    }

    /**
     * Click sur le 1er element button qui correspond à $text
     * @param string $text
     * @return void
     * @throws DriverException
     * @throws UnsupportedDriverActionException
     */
    protected function doClickButton(string $text): void
    {
        $xpath = $this->cssToXpath('a.btn');
        $buttons = $this->driver->find('//button|' . $xpath);
        foreach ($buttons as $button) {
            if ($button->getText() === $text && $button->isVisible()) {
                $button->click();
                $this->session->wait(400); // animation
                $this->outputRunningFunction();
                return;
            }
        }
        trigger_error(__FUNCTION__ . ' failed');
    }

    /**
     * Effectu un téléchargement depuis $url vers $targetDir, return le chemin
     * vers le fichier
     * @param string      $url
     * @param string|null $targetDir
     * @param string|null $download
     * @return string|null chemin vers le fichier
     * @throws DriverException
     */
    protected function doDownload(string $url, string $targetDir = null, string $download = null): ?string
    {
        $targetDir = $targetDir ?: MinkCase::getTmpDir();
        /** @var Client $client */
        $client = get_class(Utility::get(Client::class));
        /** @var Client $client */
        $client = new $client(
            [
                'ssl_verify_peer' => false,
                'ssl_verify_host' => false,
                'redirect' => true,
            ]
        );
        foreach ($this->driver->getCookies() as $cookie) {
            $expires = $cookie['expires'];
            if ($expires === -1) {
                $expires = null;
            } else {
                $expires = new DateTime("@$expires");
            }
            $client->addCookie(
                new Cookie(
                    $cookie['name'],
                    $cookie['value'],
                    $expires,
                    $cookie['path'],
                    $cookie['domain'],
                    $cookie['secure'],
                    $cookie['httpOnly']
                )
            );
        }
        if (strpos($url, 'http') === false) {
            $url = Router::fullBaseUrl() . $url;
        }
        $response = $client->get($url);
        if (($code = $response->getStatusCode()) !== 200) {
            trigger_error('Download response code=' . $code);
        }

        $contentDisposition = $response->getHeaderLine('Content-Disposition');
        if (!preg_match('/filename="(.*)"|filename=(.*)/', $contentDisposition, $m) && !$download) {
            trigger_error('Content-Disposition not found');
            return null;
        }
        $filename = $m ? end($m) : $download;
        $body = $response->getBody();
        $body->rewind();
        $filePath = rtrim($targetDir, DS) . DS . $filename;
        $file = fopen($filePath, 'w');
        while (!$body->eof()) {
            fwrite($file, $body->read(4096));
        }
        fclose($file);
        $this->outputRunningFunction();
        return $filePath;
    }

    /**
     * Télécharge le fichier lié à un href du 1er lien qui contient $text
     * @param string      $text
     * @param string|null $targetDir
     * @return string|null
     * @throws DriverException
     * @throws UnsupportedDriverActionException
     */
    protected function doDownloadFromLink(string $text, string $targetDir = null): ?string
    {
        $buttons = $this->driver->find('//a[@download]');
        foreach ($buttons as $button) {
            if ($button->isVisible()
                && $button->getText() === $text
                || $button->getAttribute('aria-label') === $text
            ) {
                $url = $button->getAttribute('href');
                $download = $button->getAttribute('download');
                $this->outputRunningFunction();
                return $this->doDownload($url, $targetDir, $download);
            }
        }
        return null;
    }

    /**
     * Effectue un clic sur un bouton action d'un tableau de résultats selon le title
     * @param string $title
     * @return void
     * @throws DriverException
     * @throws UnsupportedDriverActionException
     */
    protected function doClickAction(string $title): void
    {
        $buttons = $this->driver->find('//*[@class="td-group-fieldvalue"]/*[self::button or self::a]');
        foreach ($buttons as $button) {
            if ($button->isVisible() && $button->getAttribute('title') === $title) {
                $button->click();
                $this->outputRunningFunction();
                return;
            }
        }

        // placard à balais
        $buttons = $this->driver->find('//div[@class="subactions"]//*[self::button or self::a]');
        foreach ($buttons as $button) {
            if ($button->getAttribute('title') === $title) {
                // cliquer sur le placard à balais puis sur le bouton
                $button->find('xpath', '../../../a[@class="dropdown-toggle"]')->click();
                if (!$button->isVisible()) {
                    break;
                }
                $button->click();
                $this->outputRunningFunction();
                return;
            }
        }
        trigger_error(__FUNCTION__ . ' failed');
    }

    /**
     * Effectue un clic sur un élément du breadcrumb
     * @param string $title
     * @return void
     * @throws DriverException
     * @throws UnsupportedDriverActionException
     */
    protected function doClickBreadcrumb(string $title): void
    {
        $xpath = $this->cssToXpath('.breadcrumb > li');
        $elements = $this->driver->find($xpath);
        foreach ($elements as $element) {
            if ($element->getText() === $title) {
                $element->click();
                $this->outputRunningFunction();
                return;
            }
        }
        trigger_error(__FUNCTION__ . ' failed');
    }

    /**
     * Effectu un clic sur les 3 petits points (en fonction du data-id de la ligne)
     * @param int|string $id
     * @return void
     * @throws DriverException
     * @throws UnsupportedDriverActionException
     */
    protected function doClickActionList($id): void
    {
        $xpath = $this->cssToXpath('[data-id="' . $id . '"] > td.action div.subactions > a[data-toggle]');
        $buttons = $this->driver->find($xpath);
        foreach ($buttons as $button) {
            if ($button->isVisible()) {
                $button->click();
                $this->outputRunningFunction();
                return;
            }
        }
        trigger_error(__FUNCTION__ . ' failed');
    }

    /**
     * Effectu un clic un onglet
     * @param string $text
     * @return void
     * @throws DriverException
     * @throws UnsupportedDriverActionException
     */
    protected function doClickTab(string $text): void
    {
        $xpath = $this->cssToXpath('li.ui-tabs-tab > a.ui-tabs-anchor');
        $buttons = $this->driver->find($xpath);
        foreach ($buttons as $button) {
            if ($button->isVisible() && $button->getText() === $text) {
                $button->click();
                $this->outputRunningFunction();
                return;
            }
        }
        trigger_error(__FUNCTION__ . ' failed');
    }

    /**
     * Valide les alertes javascript
     * @return void
     */
    protected function doAcceptAlert(): void
    {
        $this->driver->acceptAlert();
        $this->outputRunningFunction();
    }

    /**
     * Valide les alertes javascript
     * @return void
     */
    protected function doCancelAlert(): void
    {
        $this->driver->dismissAlert();
        $this->outputRunningFunction();
    }

    /**
     * Attend un petit peu
     * @param int $wait timeout in milliseconds
     * @return void
     */
    protected function wait(int $wait = 400): void
    {
        $this->session->wait($wait);
    }

    /**
     * Pause configurable entre actions
     * @return void
     */
    protected function pausable(): void
    {
        if ($this->pause) {
            usleep($this->pause);
        }
    }

    /**
     * Ouvre le noeud qui correspond au chemin indiqué par ...$nodeNames
     * @param string ...$nodeNames ex: ArchiveTransfer, Archive, ContentDescription
     * @return void
     * @throws DriverException
     * @throws UnsupportedDriverActionException
     */
    protected function doJstreeOpen(...$nodeNames): void
    {
        $xpath = $this->cssToXpath('div.jstree');
        $jstree = $this->findFirst($xpath);
        $lastnode = $jstree->find('css', 'ul.jstree-container-ul');
        foreach ($nodeNames as $nodeName) {
            $text = DOMUtility::xpathQuote($nodeName);
            $lastnode = $lastnode->find(
                'xpath',
                "../ul/li/a[contains(@class,'jstree-anchor') and . = $text]"
            );
            if (!$lastnode) {
                trigger_error('jstree node not found: ' . $nodeName);
                return;
            }
        }
        $lastnode->getParent()->find('xpath', 'i')->click();
        $this->outputRunningFunction();
    }

    /**
     * Ouvre le noeud qui correspond au chemin indiqué par ...$nodeNames
     * @param string ...$nodeNames ex: ArchiveTransfer, Archive, ContentDescription
     * @return void
     * @throws DriverException
     * @throws UnsupportedDriverActionException
     */
    protected function doJstreeClick(...$nodeNames): void
    {
        $xpath = $this->cssToXpath('div.jstree');
        $jstree = $this->findFirst($xpath);
        $lastnode = $jstree->find('css', 'ul.jstree-container-ul');
        foreach ($nodeNames as $nodeName) {
            $text = DOMUtility::xpathQuote($nodeName);
            $lastnode = $lastnode->find(
                'xpath',
                "../ul/li/a[contains(@class,'jstree-anchor') and . = $text]"
            );
            if (!$lastnode) {
                trigger_error('jstree node not found: ' . $nodeName);
                return;
            }
        }
        $lastnode->click();
        $this->outputRunningFunction();
    }

    /**
     * Extraction des données js d'un tableau de résultat
     * @param string|null     $css  1ère table visible si null
     * @param int|string|null $tdId id de la ligne (optionnel) renvoi toute la table si null
     * @return array|null
     * @throws DriverException
     * @throws UnsupportedDriverActionException
     */
    protected function extractDataFromTable(string $css = null, $tdId = null): ?array
    {
        $xpath = $css === null ? "//table[@data-table-uid]" : $this->cssToXpath($css);
        $tables = $this->driver->find($xpath);
        foreach ($tables as $table) {
            if (!$table->isVisible()) {
                continue;
            }
            $instance = $table->getAttribute('data-table-uid');
            $tableObject = "TableGenerator.instance[$instance]";
            $script = $tdId ? "$tableObject.getDataId('$tdId')" : "$tableObject.data";
            $script = "JSON.stringify($script)";
            return json_decode($this->session->evaluateScript($script), true);
        }
        trigger_error(__FUNCTION__ . ' failed');
    }

    /**
     * Attend le chargement de la page
     * @return void
     * @throws DriverException
     * @throws UnsupportedDriverActionException
     */
    protected function waitForDom(): void
    {
        $this->driver->getCurrentUrl();
    }

    /**
     * Attend le chargement de la page
     * @return void
     */
    protected function prepareRequestChange(): void
    {
        $this->session->evaluateScript("minkLastAjaxResponse.response = null;");
        $this->outputRunningFunction();
    }

    /**
     * Attend le chargement de la page
     * @param int $wait
     * @return void
     */
    protected function waitRequestChange(int $wait = 5000): void
    {
        $begin = microtime(true);
        $this->session->wait(
            $wait,
            "minkLastAjaxResponse.response"
        );
        if (microtime(true) - $begin > ($wait / 1000)) {
            trigger_error('waitRequestChange timeout occured');
        }
        $this->session->wait(400); // animation
        $this->outputRunningFunction();
    }

    /**
     * Rempli le textarea TinyMCE lié à $id avec le $text
     * @param string $id
     * @param string $text
     * @return void
     */
    protected function doTinyMceFill(string $id, string $text)
    {
        $id = addcslashes($id, "'");
        $text = addcslashes($text, "'");
        $this->session->executeScript("tinymce.get('$id').setContent('$text');");
        $this->session->executeScript(
            <<<JS
tinymce.get('$id').setContent('$text');
$('#$id').val('$text').change();
JS
        );
    }
}
