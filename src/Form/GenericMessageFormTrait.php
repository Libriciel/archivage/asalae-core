<?php
/**
 * AsalaeCore\Form\GenericMessageFormTrait
 */

namespace AsalaeCore\Form;

use AsalaeCore\Form\MessageSchema\MessageSchema;
use AsalaeCore\Form\MessageSchema\Seda02Schema;
use AsalaeCore\Utility\DOMUtility;
use AsalaeCore\Utility\PreControlMessages;
use AsalaeCore\Utility\ValidatorXml;
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Database\Expression\IdentifierExpression;
use Cake\Datasource\EntityInterface;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Form\Schema;
use Cake\I18n\FrozenTime as Time;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Cake\Utility\Xml;
use Cake\Validation\Validator;
use DateTimeInterface;
use DOMDocument;
use DOMElement;
use DOMNode;
use DOMText;
use DOMXPath;
use Exception;

/**
 * Génère un formulaire à partir d'un schéma de type MessageFormSchemaInterface
 *
 * @category Form
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin MessageFormInterface
 */
trait GenericMessageFormTrait
{
    /**
     * @var array liste les schemas (lorsque Cache::disabled)
     */
    public static $schemas;

    /**
     * @var array schema du xml
     */
    public $schema;

    /**
     * @var DOMElement|null data
     */
    public $element;

    /**
     * @var DOMDocument|null data
     */
    public $dom;

    /**
     * @var string|int id de l'entité de l'utilisateur
     */
    public $orgEntityId;

    /**
     * @var string path sur lequel est construit le formulaire
     */
    public $path;

    /**
     * Initialise le formulaire pour un chemin xml donné
     * @param int|string       $orgEntityId id de l'entité de l'utilisateur
     * @param string           $path
     * @param DOMDocument|null $dom
     * @param DOMElement|null  $element
     * @return MessageFormInterface|GenericMessageFormTrait
     */
    public static function buildForm(
        $orgEntityId,
        string $path = '',
        DOMDocument $dom = null,
        DOMElement $element = null
    ): MessageFormInterface {
        $base = ($pos = strpos($path, '.')) ? substr($path, 0, $pos) : substr($path, 0);
        $instance = new self;
        $schema = MessageSchema::getCachedSchema($base, static::getVersion(), $orgEntityId);
        $paths = $path ? explode('.', str_replace('.', '.children.', $path)) : [];
        $ref =& $schema;
        foreach ($paths as $k) {
            $ref =& $ref[$k];
        }
        $instance->schema = $ref;
        $instance->dom = $dom;
        $instance->element = $element;
        $instance->orgEntityId = $orgEntityId;
        $instance->path = $path;

        return $instance;
    }

    /**
     * Donne l'uri du dossier "data" d'un transfert
     * @param string|int $transfer_id
     * @return string
     */
    public static function getDataDirectory($transfer_id): string
    {
        return Configure::read('App.paths.data') . DS . 'transfers' . DS . $transfer_id;
    }

    /**
     * Ajoute un element en respectant l'ordre dans le schema
     * @param string|int  $orgEntityId
     * @param string      $append      tagName de l'element à
     *                                 ajouter
     * @param string      $path
     * @param DOMDocument $dom
     * @param DOMElement  $parentNode  element sur lequel ajouter le nouveau noeud
     * @return DOMElement
     * @throws Exception
     */
    public static function appendNode(
        $orgEntityId,
        string $append,
        string $path,
        DOMDocument $dom,
        DOMElement $parentNode
    ): DOMElement {
        // récupère "Archive" dans "Archive.ArchiveObject.Document"
        $base = ($pos = strpos($path, '.')) ? substr($path, 0, $pos) : $path;
        $schema = MessageSchema::getCachedSchema($base, static::getVersion(), $orgEntityId);
        $ref =& $schema;
        $paths = $path ? explode('.', str_replace('.', '.children.', $path)) : [];
        foreach ($paths as $k) {
            $ref =& $ref[$k];
        }
        if (empty($ref)) {
            throw new Exception(
                __("Schema non trouvé pour le chemin {0}", $path)
            );
        }
        $appendBefore = [];
        $found = false;
        foreach (array_keys((array)$ref['children']) as $child) {
            if ($child === $append) {
                $found = true;
            } elseif ($found) {
                $appendBefore[] = $child;
            }
        }
        $newNode = $dom->createElementNS($parentNode->namespaceURI, $append);
        $inserted = false;
        if (!empty($ref['complex'])) {
            return static::insertComplex($newNode, $parentNode, $ref['complex']);
        }
        foreach ($parentNode->childNodes as $node) {
            if ($node instanceof DOMElement && in_array($node->tagName, $appendBefore)) {
                $newNode = $parentNode->insertBefore($newNode, $node);
                $inserted = true;
                break;
            }
        }
        if (!$inserted) {
            $newNode = $parentNode->appendChild($newNode);
        }
        return $newNode;
    }

    /**
     * Insertion d'un element avec structure complexe
     * @param DOMElement $newNode
     * @param DOMElement $parentNode
     * @param array      $complex
     * @return DOMElement|DOMNode
     */
    public static function insertComplex(DOMElement $newNode, DOMElement $parentNode, array $complex)
    {
        $flatten = Hash::flatten($complex);

        $key = preg_replace('/\.\d+$/', '', array_search($newNode->tagName, $flatten));
        $block = Hash::extract($complex, $key);
        $type = substr($key, ($pos = strrpos($key, '.')) ? $pos +1 : 0);
        if (is_numeric($type)) {
            $type = 'sequence';
        }

        $beginAfterNode = false;
        $beginAfterBlock = false;
        $afterNode = [];
        $afterBlock = [];
        $target = end($block);
        foreach ($flatten as $value) {
            if ($beginAfterBlock) {
                $afterBlock[] = $value;
            } elseif ($value === $target) {
                $beginAfterBlock = true;
            }
            if ($beginAfterNode) {
                $afterNode[] = $value;
            } elseif ($value === $newNode->tagName) {
                $beginAfterNode = true;
            }
        }

        if ($type === 'sequence') {
            $sequences = [[]];
            $index = 0;
            // on liste les répétitions de la séquences
            foreach ($parentNode->childNodes as $node) {
                if (!$node instanceof DOMElement || !in_array($node->tagName, $block)) {
                    continue;
                }
                if (isset($sequences[$index][$node->tagName])) {
                    $index++;
                }
                $sequences[$index][$node->tagName] = $node;
            }
            // on trouve dans quelle séquence il faut insérer $newNode
            $targetIndex = null;
            foreach ($sequences as $index => $nodes) {
                if (!isset($nodes[$newNode->tagName])) {
                    $targetIndex = $index;
                    break;
                }
            }
            // on ajoute l'élement dans la sequence choisie
            if ($targetIndex !== null && isset($sequences[$targetIndex])) {
                foreach ($sequences[$targetIndex] as $tagName => $node) {
                    if (in_array($tagName, $afterNode)) {
                        return $parentNode->insertBefore($newNode, $node);
                    }
                }
                // si on arrive là, c'est qu'il faut insérer à la fin de la séquence
                // donc avant le 1er element de la sequence suivante
                if (isset($sequences[$targetIndex +1])) {
                    return $parentNode->insertBefore($newNode, current($sequences[$targetIndex +1]));
                }
                // l'élement choisi est en derniere position de la derniere sequence
            }
            // sinon c'est comme pour un choice (insertBefore le prochain element après le bloc)
        }

        // Si on arrive ici, c'est qu'il faut insérer sans tenir compte de la séquence
        foreach ($parentNode->childNodes as $node) {
            if ($node instanceof DOMElement && in_array($node->tagName, $afterBlock)) {
                return $parentNode->insertBefore($newNode, $node);
            }
        }

        // Si on arrive ici c'est que le bloc à insérer est position après ceux présent
        return $parentNode->appendChild($newNode);
    }

    /**
     * Permet d'appeler plus facilement appendNode() en calculant certains attributs
     * @param DOMElement    $node
     * @param string        $append
     * @param null|bool|int $orgEntityId si valeur à true, la valeur est définie automatiquement
     *                                   par rapport à l'archive identifier dans le dom
     * @return DOMElement
     * @throws Exception
     */
    public static function smartAppendNode(DOMElement $node, string $append, $orgEntityId = null): DOMElement
    {
        if (is_bool($orgEntityId) && $orgEntityId) {
            $orgEntityId = static::searchOrgEntityId($node);
        }
        $path = [$node->tagName];
        $parent = $node->parentNode;
        while ($parent instanceof DOMElement) {
            array_unshift($path, $parent->tagName);
            $parent = $parent->parentNode;
        }
        return static::appendNode(
            $orgEntityId,
            $append,
            implode('.', $path),
            $node->ownerDocument,
            $node
        );
    }

    /**
     * Donne le org_entity_id à partir d'un DOMElement
     * @param DOMElement $node
     * @return int|null
     */
    private static function searchOrgEntityId(DOMElement $node)
    {
        $dom = $node->ownerDocument;
        $xpath = new DOMXPath($dom);
        $namespace = $dom->documentElement->getAttributeNode('xmlns')->nodeValue;
        $xpath->registerNamespace('ns', $namespace);
        $query = $xpath->query('ns:ArchivalAgency/ns:Identification')->item(0);
        $identifier = $query ? $query->nodeValue : null;
        $OrgEntities = TableRegistry::getTableLocator()->get('OrgEntities');
        $orgEntity = $OrgEntities->find()
            ->select(['id'])
            ->where(
                [
                    'identifier' => $identifier,
                    'archival_agency_id' => new IdentifierExpression('id'),
                ]
            )
            ->first();
        return $orgEntity ? $orgEntity->id : null;
    }

    /**
     * A hook method intended to be implemented by subclasses.
     *
     * You can use this method to define the schema using
     * the methods on Cake\Form\Schema, or loads a pre-defined
     * schema from a concrete class.
     *
     * @param Schema $schema The schema to customize.
     * @return Schema The schema to use.
     */
    protected function _buildSchema(Schema $schema): Schema
    {
        $localSchema = $this->schema;
        if (isset($localSchema['children'])) {
            $localSchema = $localSchema['children'];
        }
        foreach ($localSchema as $field => $params) {
            if (isset($params['children'])) {
                continue;
            }
            $this->addFieldToSchema($schema, $field, $params);
        }
        return $schema;
    }

    /**
     * Ajoute un element au schema
     * @param Schema $schema
     * @param string $field
     * @param array  $params
     * @return void
     */
    private function addFieldToSchema(Schema $schema, string $field, array $params)
    {
        if ($this->element) {
            $i = 0;
            /** @var DOMElement|DOMText $element */
            foreach ($this->element->childNodes as $element) {
                if ($element->nodeName !== $field) {
                    continue;
                }
                $key = $field . '_#' . $i;
                $schema->addField($key, ['type' => $params['data-type'] ?? 'string']);
                foreach ($params['attributes'] ?? [] as $attr => $values) {
                    $schema->addField($key.'_@'.$attr, $values['data-type'] ?? 'string');
                }
                $i++;
            }
            if ($i === 0) {
                $key = $field . '_#0';
                $schema->addField($key, ['type' => $params['data-type'] ?? 'string']);
                foreach ($params['attributes'] ?? [] as $attr => $values) {
                    $schema->addField($key.'_@'.$attr, $values['data-type'] ?? 'string');
                }
            }
            if ($field === 'custom') {
                $schema->addField('custom_#0', ['type' => 'string']);
            }
        } else {
            $schema->addField($field, ['type' => $params['data-type'] ?? 'string']);
            foreach ($params['attributes'] ?? [] as $attr => $values) {
                $schema->addField($field.'_@'.$attr, $values['data-type'] ?? 'string');
            }
        }
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        if (!$this->element) {
            return $validator;
        }
        $localSchema = $this->schema;
        if (isset($localSchema['children'])) {
            $localSchema = $localSchema['children'];
        }
        foreach ($localSchema as $field => $params) {
            if (isset($params['children']) || is_bool($params)) {
                continue;
            }
            $i = 0;
            /** @var DOMElement|DOMText $element */
            foreach ($this->element->childNodes as $element) {
                if ($element->nodeName !== $field) {
                    continue;
                }
                $key = $field . '_#' . $i;
                $this->appendValidations($params, $field, $key, $validator);
                $i++;
            }
            if ($i === 0) {
                $key = $field . '_#0';
                $this->appendValidations($params, $field, $key, $validator);
            }
        }
        return $validator;
    }

    /**
     * Ajoute les règles de validation d'un champ selon $params['cardinality'] et
     * $params['validations']
     * @param array     $params
     * @param string    $field
     * @param string    $key
     * @param Validator $validator
     */
    private function appendValidations(array $params, string $field, string $key, Validator $validator)
    {
        [$min, $max] = explode('..', $params['cardinality'] ?? '0..1');
        $validator->allowEmptyString(
            $key,
            null,
            function ($context) use ($key, $params, $min) {
                if ($min === '0' || !empty($params['allowEmpty'])) {
                    return true;
                }
                $notEmpties = array_keys(array_filter($context['data']));
                $keyField = explode('_#', $key)[0];
                foreach ($notEmpties as $k) {
                    $m = explode('_', $k);
                    if (!isset($m[2]) && $keyField === $m[0]) {
                        return true;
                    }
                }
                return false;
            }
        );
        if (is_numeric($max)) {
            $validator->add(
                $key,
                'toomuch',
                [
                    'rule' => function (
                        $value,
                        $context
                    ) use (
                        $field,
                        $max
                    ) {
                        $count = 0;
                        foreach (array_keys($context['data']) as $dataField) {
                            if (preg_match("/^{$field}_#\d+$/", $dataField)) {
                                $count++;
                            }
                        }
                        return $count <= $max;
                    },
                    'message' => __("Le nombre de {0} est limité à {1} dans ce contexte", $field, $max),
                ]
            );
        }
        if (isset($params['validations'])) {
            $validator->add($key, $params['validations']);
        }
    }

    /**
     * @var array data envoyé à execute
     */
    private $data;

    /**
     * Execute the form if it is valid.
     *
     * First validates the form, then calls the `_execute()` hook method.
     * This hook method can be implemented in subclasses to perform
     * the action of the form. This may be sending email, interacting
     * with a remote API, or anything else you may need.
     *
     * @param array $data    Form data.
     * @param array $options
     * @return bool False on validation failure, otherwise returns the
     *   result of the `_execute()` method.
     */
    public function execute(array $data, array $options = []): bool
    {
        $this->data = $data;
        $formatedData = [];
        if (isset($data['custom'][0]['@'])) {
            unset($this->data['custom']);
            $custom = json_decode($data['custom'][0]['@'], true);
            call_user_func_array([$this, $custom['action']], $custom['args'] ?? []);
        }
        foreach ($this->data as $field => $values) {
            foreach ($values as $i => $attrs) {
                $formatedData[$field.'_#'.$i] = $attrs['@'] ?? null;
                unset($attrs['@']);
                foreach ($attrs as $attr => $value) {
                    $formatedData[$field.'_#'.$i.'_@'.$attr] = $value;
                }
            }
        }
        return parent::execute($formatedData);
    }

    /**
     * Hook method to be implemented in subclasses.
     *
     * Used by `execute()` to execute the form's action.
     *
     * @param array $data Form data.
     * @return bool
     * @throws Exception
     */
    protected function _execute(array $data): bool
    {
        $localSchema = $this->schema;
        if (isset($localSchema['children'])) {
            $localSchema = $localSchema['children'];
        }
        // Transforme 'KeywordReference_#0_@schemeID' => '' en ['KeywordReference' => ['0_@schemeID' => '']]
        foreach (Hash::expand($data, '_#') as $element => $values) {
            // cas champs spéciaux
            if (empty($localSchema[$element]['cardinality'])) {
                continue;
            }

            /** @var DOMElement[] $refnodes liste les noeuds existant (même tagName) */
            $refnodes = [];
            foreach ($this->element->childNodes as $node) {
                if ($node instanceof DOMElement && $node->tagName === $element) {
                    $refnodes[] = $node;
                }
            }

            // boucle sur $index et attributs: [0 => '', '0_@schemeID' => '']
            foreach ($values as $key => $value) {
                $ex = explode('_', $key, 2);
                $index = $ex[0];

                // Si le noeud n'existe pas, on l'ajoute au dom (sauf balise sans valeurs ni attributs)
                if (!isset($refnodes[$index])) {
                    if (!$value) {
                        continue;
                    }
                    $refnodes[$index] = static::appendNode(
                        $this->orgEntityId,
                        $element,
                        $this->path,
                        $this->dom,
                        $this->element
                    );
                }

                // Si c'est un attribut
                if (count($ex) > 1) {
                    $attr = substr($ex[1], 1); // @attribut -> attribut
                    $this->convertValueDataType($value, $localSchema[$element]['attributes'][$attr]['data-type'] ?? '');
                    if ($value) {
                        $refnodes[$index]->setAttribute($attr, $value);
                    } else {
                        $refnodes[$index]->removeAttribute($attr);
                    }
                } else {
                    $this->convertValueDataType($value, $localSchema[$element]['data-type'] ?? '');
                    if ($value) {
                        DOMUtility::setValue($refnodes[$index], $value);
                    } else {
                        $refnodes[$index]->parentNode->removeChild($refnodes[$index]);
                        unset($refnodes[$index]);
                    }
                }
            }
        }
        return true;
    }

    /**
     * Converti $value selon le data-type
     * @param mixed  $value
     * @param string $dataType
     */
    private function convertValueDataType(&$value, string $dataType)
    {
        if (!$value) {
            return;
        }
        switch ($dataType) {
            case 'date':
                $value = $this->dateStringToDateObject($value)->format('Y-m-d');
                break;
            case 'datetime':
                $value = $this->dateStringToDateObject($value)->format('Y-m-d\TH:i:s');
                break;
        }
    }

    /**
     * Permet d'obtenir l'arbre pour l'importer dans jstree
     * @param string $xmlUri
     * @return string
     */
    public function getDataJsTree(string $xmlUri): string
    {
        $data = Xml::toArray(Xml::build($xmlUri, ['readFile' => true]));
        return $this->recursiveTreeBuilder($data);
    }

    /**
     * Renvoi les données jstree de façon récursive
     * @param array  $data
     * @param array  $path
     * @param string $parentKey
     * @return string
     */
    private function recursiveTreeBuilder(array $data, $path = [], string $parentKey = ''): string
    {
        $output = '[';
        $isFirst = true;
        foreach ($this->completeData($data, $path) as $key => $values) {
            if (is_string($values) || isset($values['@'])) {// Balise de valeur, on n'affiche pas dans l'arbre
                continue;
            }
            // On récupère le schema de l'element
            $localPath = $path;
            if (is_string($key)) {
                $localPath[] = $key;
            }
            $ref = $this->getMessageSchema($localPath);

            // On défini la sorti à partir du schema et de/des valeur(s)
            if (empty($ref['text']) || (isset($ref['display']) && !$ref['display'])) {
                continue; // seul les "dossiers" portent un "text"
            }
            [, $max] = explode('..', $ref['cardinality'] ?? '0..1'); // 0..n

            // is multiple
            if ($max !== '1' && isset($values[0]) && isset($values[1])) {
                foreach ($values as $i => $value) {
                    if (!$isFirst) {
                        $output .= ',';
                    }
                    $isFirst = false;
                    $childKey = ($parentKey ? $parentKey.'/' : '').$key.'['.($i+1).']';
                    $output .= $this->appendTreeBranch($value, $localPath, $childKey, $ref, $path);
                }
                continue;
            }
            if (!$isFirst) {
                $output .= ',';
            }
            $isFirst = false;

            $childKey = ($parentKey ? $parentKey.'/' : '').$key;
            $output .= $this->appendTreeBranch($values, $localPath, $childKey, $ref, $path);
        }
        $output .= ']';
        return $output;
    }

    /**
     * Ajoute des elements au data issue du schema seda
     * @param array $data
     * @param array $path
     * @return array
     */
    private function completeData(array $data, array $path): array
    {
        $completedData = [];
        $next = $this->getMessageSchema($path);
        if (isset($next['children'])) {
            $next = $next['children'];
        }
        foreach (array_keys($next) as $key) {
            if ($key === 'text' || $key[0] === '@') {
                continue;
            }
            if (!isset($data[$key])) {
                $completedData[$key] = '';
            } else {
                $completedData[$key] = $data[$key];
            }
        }
        return $completedData;
    }

    /**
     * Ajout d'une branche à l'arbre
     * @param array  $value
     * @param array  $localPath
     * @param string $childKey
     * @param array  $ref
     * @param array  $path
     * @return string
     */
    private function appendTreeBranch($value, $localPath, $childKey, $ref, $path): string
    {
        // texte additionnel selon valeur des enfants
        $appendChildValue = '';
        if (isset($ref['text_appendChildValue'])) {
            $appendChildValue = $this->extractAppendChildValue($ref);
        }
        [$min] = explode('..', $ref['cardinality'] ?? '0..1');
        $addable = $this->extractAddable($ref);

        [$required, $max] = explode('..', $ref['cardinality'] ?? '0..1'); // 0..n
        $icon = $this->extractIcon($ref, (bool)$required);
        unset($ref['children']);
        $output = json_encode(
            [
                'element' => $ref['text'],
                'text' => $ref['text'].$appendChildValue,
                'path' => $childKey,
                'icon' => $icon,
                'min' => $min,
                'max' => $max,
                'addable' => $addable,
                'li_attr' => $ref['li_attr'] ?? [],
                'state' => [
                    'opened' => count($path) < 2
                ],
            ] + $ref
        );
        $children = $this->recursiveTreeBuilder((array)$value, $localPath, $childKey);
        if ($children) {
            return substr($output, 0, -1)
                .',"children":'
                .$this->recursiveTreeBuilder((array)$value, $localPath, $childKey)
                .'}';
        } else {
            return $output;
        }
    }

    /**
     * Donne le appendChildValue pour une $ref
     * @param array $ref
     * @return string
     */
    private function extractAppendChildValue(array $ref)
    {
        $text = $ref['text_appendChildValue'];
        while (preg_match('/{{([^{]+)}}/', $text, $match)) {
            $field = $match[1];
            $attr = '@';
            if (strpos($field, '.')) {
                [$field, $attr] = explode('.', $field);
                $attr = '@'.$attr;
            }
            $val = '<null>';
            // Converti la valeur du child dans la traduction "option" de son select
            $schemaField = $attr === '@'
                ? $ref['children'][$field]
                : $ref['children'][$field]['attributes'][substr($attr, 1)];
            if (isset($schemaField['options'])
                && (!isset($schemaField['options'][$val])
                || is_array($schemaField['options'][$val]))
            ) {
                foreach ($schemaField['options'] as $option) {
                    if (!is_array($option) || !isset($option['value']) || $option['value'] !== $val) {
                        continue;
                    }
                    $val = $option['text'] ?? '<null>';
                    break;
                }
            } elseif (isset($schemaField['options'][$val])) {
                $val = $schemaField['options'][$val];
            }
            $text = str_replace($match[0], $val, $text);
        }
        return $text ?? '';
    }

    /**
     * Donne le addable d'une $ref
     * @param array $ref
     * @return array
     */
    private function extractAddable(array $ref): array
    {
        $addable = [];
        foreach ($ref['children'] as $child => $params) {
            if (empty($params['children'])) {
                continue;
            }
            $addable[] = $child;
        }
        return $addable;
    }

    /**
     * Donne l'icone pour une $ref
     * @param array $ref
     * @param bool  $required
     * @return string
     */
    private function extractIcon(array $ref, bool $required = false): string
    {
        if (!empty($ref['icon'])) {
            if (is_string($ref['icon'])) {
                $icon = $ref['icon'];
            } else {
                $icon = ($ref['icon']['complete'] ?? '');
            }
        } elseif ($required) {
            $icon = 'fa fa-exclamation-circle text-danger';
        } else {
            $icon = 'fa fa-folder';
        }
        return $icon;
    }

    /**
     * Getter type Hash::get pour le schema
     * @param string|array $path
     * @return array
     */
    public function getMessageSchema($path = []): array
    {
        $paths = is_string($path)
            ? ($path ? explode('.', $path) : [])
            : $path;
        $ref =& $this->schema;
        foreach ($paths as $k) {
            if (isset($ref['children'][$k])) {
                $ref =& $ref['children'][$k];
            } elseif (isset($ref[$k])) {
                $ref =& $ref[$k];
            } else {
                return [];
            }
        }
        return (array)$ref;
    }

    /**
     * Supprime le caché lié à un org_entity et à ses parents
     * @param string|int $orgEntityId
     */
    public static function clearCache($orgEntityId)
    {
        $OrgEntities = TableRegistry::getTableLocator()->get('OrgEntities');
        try {
            $entity = $OrgEntities->get($orgEntityId);
        } catch (RecordNotFoundException $e) {
            return;
        }
        $query = $OrgEntities->find()
            ->select(['id'])
            ->where(
                [
                    'lft <=' => $entity->get('lft'),
                    'rght >=' => $entity->get('rght'),
                ]
            );
        /** @var EntityInterface $entity */
        foreach ($query as $entity) {
            Cache::delete(static::SCHEMA_CACHE_KEY.'_'.$entity->get('id'));
        }
    }

    /**
     * Permet de créer des dates à partir du format français
     * @param mixed $value
     * @return Time
     */
    private function dateStringToDateObject($value)
    {
        $object = $value;
        if (is_string($value)) {
            $object = Time::parseDateTime($value);
            if (empty($object)) {
                $object = Time::parseDate($value);
            }
            if (empty($object)) {
                $object = Time::parseTime($value);
            }
        } elseif ($value instanceof DateTimeInterface) {
            $object = new Time($value);
        }
        return $object;
    }

    /**
     * Effectue la validation d'un message xml et renvoi un array d'erreurs
     * @param string $path
     * @return array
     * @throws Exception
     */
    public function validateFile(string $path): array
    {
        $size = filesize($path);
        $max_execution_time = (int)($size / 100000); // 46Mo = 460 seconds
        $memory_limit = (int)($size * 10); // 46Mo = 460Mo
        if ($memory_limit > 134217728) { // > 128Mo
            ini_set('memory_limit', $memory_limit);
        }
        if ($max_execution_time > 30) {
            ini_set('max_execution_time', $max_execution_time);
            set_time_limit($max_execution_time);
        }

        $validator = new ValidatorXml(static::SCHEMA_VALIDATION);
        $PreControl = new PreControlMessages($path);
        $archivalAgency = $PreControl->getArchivalAgency();

        if ($archivalAgency && $this->orgEntityId !== $archivalAgency->get('id')) {
            return [
                __(
                    "Le service d'archives indiqué dans le bordereau"
                    ." ne correspond pas avec celui de l'utilisateur courant"
                )
            ];
        }

        if ($validator->validate($path)) {
            return [];
        }

        $errors = $this->searchForMissingNodes($path);
        if ($errors) {
            return $errors;
        }

        $lines = [];
        $file = fopen($path, 'r');
        $count = 0;
        while (!feof($file)) {
            $line = fgets($file);
            $count++;
            if (preg_match('/^(\s*)<(\w+)/', $line, $matches)) {
                $lines[$count] = [
                    'indent' => strlen($matches[1]),
                    'element' => $matches[2],
                ];
            } else {
                $lines[$count] = [];
            }
        }
        fclose($file);

        $ns = implode('|', [NAMESPACE_SEDA_02, NAMESPACE_SEDA_10, NAMESPACE_SEDA_21, NAMESPACE_SEDA_22]);
        $namespaces = "\{(?:$ns)}";
        $formatedErrors = [];
        foreach ($validator->errors as $error) {
            if (preg_match(
                "/^Error (\d+)\D*(\d+),.*Element '$namespaces(\w+)', attribute '(\w+)': (.*)$/",
                $error,
                $matches
            )
            ) {
                /** @noinspection PhpUnusedLocalVariableInspection */
                [, $code, $line, $element, $attribute, $message] = $matches;
                $message = preg_replace('/{[^}]+}/', '', $message);
                $formatedErrors[] = __(
                    "<b>{2} {1}</b>:<br>{3}<br>",
                    $code,
                    $attribute,
                    static::lineToPath($line, $lines),
                    $message
                );
            } elseif (preg_match(
                "/^Error (\d+)\D*(\d+),.*Element '$namespaces(\w+(?:', attribute '\w+)?)': (.*)$/",
                $error,
                $matches
            )
            ) {
                /** @noinspection PhpUnusedLocalVariableInspection */
                [, $code, $line, $element, $message] = $matches;
                $message = preg_replace('/{[^}]+}/', '', $message);
                $formatedErrors[] = __("<b>{1}</b>:<br>{2}<br>", $code, static::lineToPath($line, $lines), $message);
            } else {
                $formatedErrors[] = $error;
            }
        }

        return $formatedErrors;
    }

    /**
     * Donne un chemin xpath à partir d'un numéro de ligne
     * @param int   $line
     * @param array $lines
     * @return string chemin/vers/element
     */
    private static function lineToPath($line, array $lines): string
    {
        $output = $lines[$line]['element'];
        $indent = $lines[$line]['indent'];
        for ($i = (int)$line; $i > 0; $i--) {
            if (isset($lines[$i]['indent']) && $lines[$i]['indent'] < $indent) {
                $indent = $lines[$i]['indent'];
                $output = $lines[$i]['element'].' / '.$output;
            }
        }
        return '[ '.$output.' ]';
    }

    /** @noinspection PhpUnusedPrivateMethodInspection */
    /**
     * Callback d'insertion des données liés à une entité à partir d'un id
     * @see Seda02Schema::getTransferringAgency()
     * @param string $initialField
     * @param array  $replacement
     */
    private function insertOrgEntityData(string $initialField, array $replacement)
    {
        $identifier = $this->data[$initialField][0]['@'];
        $OrgEntities = TableRegistry::getTableLocator()->get('OrgEntities');
        $subquery = $OrgEntities->query()
            ->select(['a.id'])
            ->from(['a' => 'org_entities'])
            ->where(
                [
                    'a.lft >=' => new IdentifierExpression('OrgEntities.lft'),
                    'a.rght <=' => new IdentifierExpression('OrgEntities.rght'),
                ]
            );
        $entity = $OrgEntities->find()
            ->where(
                [
                    'OrgEntities.identifier' => $identifier,
                    'OrgEntities.id IN' => $subquery,
                ]
            )
            ->firstOrFail();
        foreach ($replacement as $field => $dom) {
            $this->data[$dom][0]['@'] = $entity->get($field);
        }
    }

    /** @noinspection PhpUnusedPrivateMethodInspection */
    /**
     * Retire une valeur à element
     * @see Seda02Schema::getDocument()
     * @param string $element
     */
    private function unsetValue(string $element)
    {
        foreach ($this->data[$element] as $i => $attrs) {
            unset($this->data[$element][$i]['@']);
        }
    }

    /** @noinspection PhpUnusedPrivateMethodInspection */
    /**
     * Permet de lancer plusieurs callback
     * @see Seda02Schema::getDocument()
     * @param array ...$actions
     */
    private function multiAction(array ...$actions)
    {
        foreach ($actions as $action) {
            call_user_func_array([$this, $action['action']], $action['args'] ?? []);
        }
    }

    /**
     * @var EntityInterface entité traité (pour callback)
     */
    private $entity;

    /**
     * Permet de passer l'entité à la classe de formulaire pour certains callbacks
     * @param EntityInterface $entity
     * @return MessageFormInterface|self
     */
    public function setEntity(EntityInterface $entity): MessageFormInterface
    {
        $this->entity = $entity;
        return $this;
    }

    /**
     * Retire le noeud courant du dom (donné par static::buildForm($id, $path))
     * @return MessageFormInterface|self
     */
    public function removeNode(): MessageFormInterface
    {
        if (isset($this->schema['delete'])) {
            call_user_func_array([$this, $this->schema['delete']['action']], $this->schema['delete']['args'] ?? []);
        }
        $this->element->parentNode->removeChild($this->element);
        return $this;
    }

    /**
     * Recherche les elements manquant (cardinalité < min)
     * @param string $path
     * @return array
     * @throws Exception
     */
    public function searchForMissingNodes(string $path): array
    {
        $util = DOMUtility::load($path);
        $element = $util->dom->documentElement;
        $schema =& $this->schema[$element->tagName]['children'];
        $errors = [];
        $this->recursiveSearchForMissingNodes($util, $element, $schema, $errors);
        return $errors;
    }

    /**
     * Recherche récursive des élements manquants
     * @param DOMUtility $util
     * @param DOMElement $element
     * @param array      $schema
     * @param array      $errors
     */
    private function recursiveSearchForMissingNodes(
        DOMUtility $util,
        DOMElement $element,
        array $schema,
        array &$errors
    ) {
        $required = [];
        foreach ($schema as $el => $params) {
            if (isset($params['cardinality']) && $params['cardinality'][0] !== '0') {
                $required[$el] = $el;
            }
        }
        foreach ($element->childNodes as $node) {
            if (!$node instanceof DOMElement) {
                continue;
            }
            if (isset($required[$node->nodeName])) {
                unset($required[$node->nodeName]);
            }
            if (isset($schema[$node->nodeName]['children'])) {
                $this->recursiveSearchForMissingNodes($util, $node, $schema[$node->nodeName]['children'], $errors);
            }
        }
        if (empty($required)) {
            return;
        }
        $path = [];
        $ref =& $element;
        while ($ref->parentNode && $ref->parentNode->nodeName[0] !== '#') {
            $name = null;
            foreach ($ref->childNodes as $snode) {
                if ($snode instanceof DOMElement && $snode->tagName === 'Name') {
                    $name = $snode->nodeValue;
                    break;
                }
            }
            $el = $ref->nodeName . ($name ? "($name)" : '');
            array_unshift($path, $el);
            $ref =& $ref->parentNode;
        }
        $path = implode(' / ', $path);
        $errors[] = __(
            "Element obligatoire manquant dans {0}: {1}",
            $path,
            implode(', ', $required)
        );
    }
}
