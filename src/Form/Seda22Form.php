<?php
/**
 * AsalaeCore\Form\Seda22Form
 */

namespace AsalaeCore\Form;

use AsalaeCore\Form\MessageSchema\Seda22Schema;
use AsalaeCore\Utility\DOMUtility;
use Cake\Datasource\EntityInterface;
use Cake\Form\Form;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Cake\Utility\Text;
use DateTime;
use DateTimeInterface;
use DOMDocument;
use DOMElement;
use DOMXPath;
use Exception;

/**
 * Formulaire d'édition d'un message seda v2.2
 *
 * @category Form
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2022, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Seda22Form extends Form implements MessageFormInterface
{
    const SCHEMA_CLASS = Seda22Schema::class;
    const SCHEMA_CACHE_KEY = 'Seda22_schema';
    const SCHEMA_VALIDATION = SEDA_ARCHIVE_V22_XSD;

    /**
     * Traits
     */
    use GenericMessageFormTrait {
        GenericMessageFormTrait::_execute as _executeTrait;
    }

    /**
     * Donne la liste des fichiers utilisés dans le xml ($path)
     * @param string $path
     * @return array
     */
    public static function listFiles(string $path): array
    {
        $dom = new DOMDocument;
        $dom->load($path);
        $namespace = $dom->documentElement->getAttributeNode('xmlns')->nodeValue;
        $xpath = new DOMXPath($dom);
        $xpath->registerNamespace('ns', $namespace);
        $nodes = $xpath->query('//ns:Filename');
        $files = [];
        foreach ($nodes as $node) {
            if ($node instanceof DOMElement) {
                $files[] = $node->nodeValue;
            }
        }
        return $files;
    }

    /**
     * Applique des modifications à un xml afin de corriger certaines erreurs
     * comme l'absence de bloc MessageDigest dans les balises "BinaryDataObject"
     * @param EntityInterface $entity
     * @param string|null     $xml
     * @throws Exception
     */
    public static function autoRepair(EntityInterface $entity, string $xml = null)
    {
        if (!$xml) {
            $xml = $entity->get('xml');
        }
        $save = false;
        $dom = new DOMDocument;
        $dom->load($xml);
        $namespace = $dom->documentElement->getAttributeNode('xmlns')->nodeValue;
        $xpath = new DOMXPath($dom);
        $xpath->registerNamespace('ns', $namespace);
        $nodes = $xpath->query('//ns:BinaryDataObject');
        $TransferAttachments = TableRegistry::getTableLocator()->get('TransferAttachments');
        /** @var DOMElement $binaryDataObject */
        foreach ($nodes as $binaryDataObject) {
            $Filename = $binaryDataObject->getElementsByTagName('Filename')->item(0);
            if (!$Filename) {
                continue;
            }
            $filename = $Filename->nodeValue;
            if ($filename[0] === '/') {
                $filename = ltrim($filename, '/');
                DOMUtility::setValue($Filename, $filename);
                $save = true;
            }
            $attachment = $TransferAttachments->find()
                ->where(
                    [
                        'transfer_id' => $entity->get('id'),
                        'filename' => $filename,
                    ]
                )
                ->first();
            $integrity = $binaryDataObject->getElementsByTagName('MessageDigest')->item(0);
            if (!$attachment || $integrity) {
                continue;
            }
            $save = true;
            $integrity = static::appendNode(
                $entity->get('archival_agency_id'),
                'MessageDigest',
                implode('.', DOMUtility::getElementPath($binaryDataObject)),
                $dom,
                $binaryDataObject
            );
            $integrity->setAttribute('algorithm', $attachment->get('hash_algo'));
            DOMUtility::setValue($integrity, $attachment->get('hash'));
        }
        if ($save) {
            $dom->save($xml);
        }
    }

    /**
     * Ajoute un ensemble de mots-clés
     * @param array $ids keyword_id[]
     * @return bool success
     * @throws Exception
     */
    public function addMultipleKeywords(array $ids): bool
    {
        $Keywords = TableRegistry::getTableLocator()->get('Keywords');
        // préserve l'ordre choisi
        foreach ($ids as $id) {
            $keywordData = $Keywords->find()
                ->where(['Keywords.id' => $id])
                ->contain(['KeywordLists'])
                ->firstOrFail();
            $keyword = static::appendNode($this->orgEntityId, 'Keyword', $this->path, $this->dom, $this->element);
            $keyword->nodeValue = null; // supprime les elements obligatoire automatiquement créé
            $content = $this->dom->createElement('KeywordContent');
            $keyword->appendChild($content);
            $reference = $this->dom->createElement('KeywordReference');
            $keyword->appendChild($reference);
            DOMUtility::setValue($content, $keywordData->get('name'));
            DOMUtility::setValue($reference, $keywordData->get('code'));
            $reference->setAttribute('schemeID', $keywordData->get('keyword_list')->get('identifier'));
            $reference->setAttribute('schemeName', $keywordData->get('keyword_list')->get('name'));
            $reference->setAttribute('schemeVersionID', $keywordData->get('keyword_list')->get('version'));
        }
        return true;
    }

    /**
     * Ajoute un ensemble de mots-clés en saisie libre
     * @param array $keywords [['name' => ..., 'code' => ...]]
     * @return bool success
     * @throws Exception
     */
    public function addMultipleCustomKeywords(array $keywords): bool
    {
        foreach ($keywords as $keywordData) {
            if (empty($keywordData['name'])) {
                continue;
            }
            $keyword = static::appendNode($this->orgEntityId, 'Keyword', $this->path, $this->dom, $this->element);
            $keyword->nodeValue = null;
            $content = $this->dom->createElement('KeywordContent');
            $keyword->appendChild($content);
            DOMUtility::setValue($content, $keywordData['name']);
            if (!empty($keywordData['code'])) {
                $reference = $this->dom->createElement('KeywordReference');
                $keyword->appendChild($reference);
                DOMUtility::setValue($reference, $keywordData['code']);
            }
        }
        return true;
    }

    /**
     * @inheritDoc
     */
    public static function getVersion(): string
    {
        return '2.2';
    }

    /** @noinspection PhpUnusedPrivateMethodInspection */
    /**
     * Ajoute un identifiant unique au binary
     */
    private function addBinaryDataObject()
    {
        if (!$this->element->getAttribute('id')) {
            $this->element->setAttribute('id', 'UUID-'.Text::uuid());
        }
        /** @var DOMElement $parent */
        $parent = $this->element->parentNode;

        // si le parent n'est pas un DataObjectGroup, on ajoute un DataObjectGroupId pour identifier le BinaryDataObject
        if ($parent->tagName !== 'DataObjectGroup'
            && $this->element->getElementsByTagName('DataObjectGroupId')->count() === 0
        ) {
            $newNode = $this->element->ownerDocument->createElementNS(
                NAMESPACE_SEDA_22,
                'DataObjectGroupId'
            );
            $newNode->appendChild(DOMUtility::createDomTextNode($this->element->ownerDocument, 'UUID-'.Text::uuid()));
            $inserted = false;
            foreach ($this->element->childNodes as $node) {
                if ($node instanceof DOMElement) {
                    $this->element->insertBefore($newNode, $node);
                    $inserted = true;
                    break;
                }
            }
            if (!$inserted) {
                $this->element->appendChild($newNode);
            }
        }

        // on récupère l'attachment lié
        $filename = Hash::get($this->data, 'Attachment.0.filename');
        if (!$filename) {
            return;
        }
        $TransferAttachments = TableRegistry::getTableLocator()->get('TransferAttachments');
        $attachment = $TransferAttachments->find()
            ->where(
                [
                    'transfer_id' => $this->entity->id,
                    'filename' => $filename,
                ]
            )
            ->contain(['Pronoms'])
            ->first();
        if (!$attachment) {
            return;
        }

        // ajout de MessageDigest
        $messageDigest = $this->element->getElementsByTagName('MessageDigest')->item(0);
        if (!$messageDigest) {
            $messageDigest = static::appendNode(
                $this->orgEntityId,
                'MessageDigest',
                $this->path,
                $this->dom,
                $this->element
            );
        }
        $messageDigest->setAttribute('algorithm', $attachment->get('hash_algo'));
        DOMUtility::setValue($messageDigest, $attachment->get('hash'));

        // ajout du Size
        $size = $this->element->getElementsByTagName('Size')->item(0);
        $attachmentSize = $attachment->get('size');
        if ($attachmentSize) {
            if (!$size) { // ajout du noeud manquant
                $size = static::appendNode(
                    $this->orgEntityId,
                    'Size',
                    $this->path,
                    $this->dom,
                    $this->element
                );
            }
            DOMUtility::setValue($size, $attachment->get('size'));
        } elseif ($size) { // retrait car size à 0 interdit en 2.2
            $size->parentNode->removeChild($size);
        }

        // ajout du FileInfo
        $fileinfo = $this->element->getElementsByTagName('FileInfo')->item(0);
        if (!$fileinfo) {
            $fileinfo = static::appendNode(
                $this->orgEntityId,
                'FileInfo',
                $this->path,
                $this->dom,
                $this->element
            );
        }

        // ajout du FileInfo.Filename
        $file = $fileinfo->getElementsByTagName('Filename')->item(0);
        if (!$file) {
            $file = static::appendNode(
                $this->orgEntityId,
                'Filename',
                $this->path.'.FileInfo',
                $this->dom,
                $fileinfo
            );
        }
        DOMUtility::setValue($file, $filename);

        // ajout du FileInfo.LastModified
        $modified = $fileinfo->getElementsByTagName('LastModified')->item(0);
        if (!$modified) {
            $modified = static::appendNode(
                $this->orgEntityId,
                'LastModified',
                $this->path.'.FileInfo',
                $this->dom,
                $fileinfo
            );
        }
        DOMUtility::setValue(
            $modified,
            (new DateTime)->setTimestamp(filemtime($attachment->get('path')))
                ->format(DateTimeInterface::RFC3339)
        );

        // ajout du FormatIdentification
        $format = $this->element->getElementsByTagName('FormatIdentification')->item(0);
        if (!$format) {
            $format = static::appendNode(
                $this->orgEntityId,
                'FormatIdentification',
                $this->path,
                $this->dom,
                $this->element
            );
        }

        /** @var EntityInterface $pronom */
        $pronom = Hash::get($attachment, 'pronom');
        if (!$pronom) {
            $format->parentNode->removeChild($format);
            return;
        }

        // ajout du FormatIdentification.FormatLitteral
        $litteral = $format->getElementsByTagName('FormatLitteral')->item(0);
        if (!$litteral) {
            $litteral = static::appendNode(
                $this->orgEntityId,
                'FormatLitteral',
                $this->path.'.FormatIdentification',
                $this->dom,
                $format
            );
        }
        DOMUtility::setValue($litteral, $pronom->get('name'));

        // ajout du FormatIdentification.MimeType
        $mime = $format->getElementsByTagName('MimeType')->item(0);
        if (!$mime) {
            $mime = static::appendNode(
                $this->orgEntityId,
                'MimeType',
                $this->path.'.FormatIdentification',
                $this->dom,
                $format
            );
        }
        DOMUtility::setValue($mime, $attachment->get('mime'));

        // ajout du FormatIdentification.FormatId
        $formatId = $format->getElementsByTagName('FormatId')->item(0);
        if (!$formatId) {
            $formatId = static::appendNode(
                $this->orgEntityId,
                'FormatId',
                $this->path.'.FormatIdentification',
                $this->dom,
                $format
            );
        }
        DOMUtility::setValue($formatId, $pronom->get('puid'));
    }

    /** @noinspection PhpUnusedPrivateMethodInspection */
    /**
     * Ajoute l'attribut id obligatoire
     */
    private function addUUID()
    {
        if (!$this->element->getAttribute('id')) {
            $this->element->setAttribute('id', 'UUID-'.Text::uuid());
        }
    }

    /**
     * Hook method to be implemented in subclasses.
     *
     * Used by `execute()` to execute the form's action.
     *
     * @param array $data Form data.
     * @return bool
     * @throws Exception
     */
    protected function _execute(array $data): bool
    {
        $orgTypes = ['ArchivalAgency', 'TransferringAgency', 'OriginatingAgency', 'SubmissionAgency'];
        if (in_array($this->element->nodeName, $orgTypes)) {
            // supprime l'éventuel metadata existant
            foreach ($this->element->childNodes as $child) {
                if ($child instanceof DOMElement && $child->nodeName === 'OrganizationDescriptiveMetadata') {
                    $child->parentNode->removeChild($child);
                    break;
                }
            }
            // nouveau metadata
            $metadata = $this->element->ownerDocument->createElementNS(
                $this->element->namespaceURI,
                'OrganizationDescriptiveMetadata'
            );
            $this->element->appendChild($metadata);
            if (!empty($data['Description_#0'])) {
                $description = $this->element->ownerDocument->createElementNS(
                    NAMESPACE_SEDA_10,
                    'Description'
                );
                $description->appendChild(
                    DOMUtility::createDomTextNode($this->element->ownerDocument, $data['Description_#0'])
                );
                $metadata->appendChild($description);
            }
            unset($data['Description_#0']);
            if (!empty($data['Name_#0'])) {
                $name = $this->element->ownerDocument->createElementNS(
                    NAMESPACE_SEDA_10,
                    'Name'
                );
                $name->appendChild(DOMUtility::createDomTextNode($this->element->ownerDocument, $data['Name_#0']));
                $metadata->appendChild($name);
            }
            unset($data['Name_#0']);
        }
        return $this->_executeTrait($data);
    }
}
