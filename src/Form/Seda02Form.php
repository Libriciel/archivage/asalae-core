<?php
/**
 * AsalaeCore\Form\Seda02Form
 */

namespace AsalaeCore\Form;

use AsalaeCore\Form\MessageSchema\Seda02Schema;
use AsalaeCore\Utility\DOMUtility;
use Cake\Datasource\EntityInterface;
use Cake\Form\Form;
use Cake\ORM\TableRegistry;
use DOMDocument;
use DOMElement;
use DOMXPath;
use Exception;

/**
 * Formulaire d'édition d'un message seda v0.2
 *
 * @category Form
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Seda02Form extends Form implements MessageFormInterface
{
    const SCHEMA_CLASS = Seda02Schema::class;
    const SCHEMA_CACHE_KEY = 'Seda02_schema';
    const SCHEMA_VALIDATION = SEDA_V02_XSD;

    /**
     * Traits
     */
    use GenericMessageFormTrait;

    /**
     * Donne la liste des fichiers utilisés dans le xml ($path)
     * @param string $path
     * @return array
     */
    public static function listFiles(string $path): array
    {
        $dom = new DOMDocument;
        $dom->load($path);
        $namespace = $dom->documentElement->getAttributeNode('xmlns')->nodeValue;
        $xpath = new DOMXPath($dom);
        $xpath->registerNamespace('ns', $namespace);
        $nodes = $xpath->query('//ns:Attachment');
        $files = [];
        foreach ($nodes as $node) {
            if ($node instanceof \DOMElement) {
                $files[] = $node->getAttribute('filename');
            }
        }
        return $files;
    }

    /**
     * Applique des modifications à un xml afin de corriger certaines erreurs
     * comme l'absence de bloc Integrity dans les balises "Document"
     * @param EntityInterface $entity
     * @param string|null     $xml
     * @throws Exception
     */
    public static function autoRepair(EntityInterface $entity, string $xml = null)
    {
        if (!$xml) {
            $xml = $entity->get('xml');
        }
        $save = false;
        $dom = new DOMDocument;
        $dom->load($xml);
        $namespace = $dom->documentElement->getAttributeNode('xmlns')->nodeValue;
        $xpath = new DOMXPath($dom);
        $xpath->registerNamespace('ns', $namespace);
        $nodes = $xpath->query('//ns:Attachment');
        $TransferAttachments = TableRegistry::getTableLocator()->get('TransferAttachments');
        /** @var \DOMElement $attachmentNode */
        foreach ($nodes as $attachmentNode) {
            $filename = $attachmentNode->getAttribute('filename');
            if ($filename[0] === '/') {
                $filename = ltrim($filename, '/');
                $attachmentNode->setAttribute('filename', $filename);
                $save = true;
            }
            $attachment = $TransferAttachments->find()
                ->where(
                    [
                        'transfer_id' => $entity->get('id'),
                        'filename' => $filename,
                    ]
                )
                ->first();

            $efilename = addcslashes($filename, "'");
            $integrity = $xpath->query(
                "/ns:ArchiveTransfer/ns:Integrity/ns:UnitIdentifier/text()[contains(.,'$efilename')]/../../ns:Contains"
            )->item(0);
            if (!$attachment || $integrity) {
                continue;
            }
            $save = true;
            $integrity = static::appendNode(
                $entity->get('archival_agency_id'),
                'Integrity',
                'ArchiveTransfer',
                $dom,
                $dom->documentElement
            );
            $contains = $dom->createElement('Contains');
            $contains->setAttribute('algorithme', $attachment->get('hash_algo'));
            DOMUtility::setValue($contains, $attachment->get('hash'));
            $unitIdentifier = $dom->createElement('UnitIdentifier');
            DOMUtility::setValue($unitIdentifier, $attachment->get('filename'));
            $integrity->appendChild($contains);
            $integrity->appendChild($unitIdentifier);
        }
        if ($save) {
            $dom->save($xml);
        }
    }

    /**
     * Ajoute un ensemble de mots-clés
     * @param array $ids keyword_id[]
     * @return bool success
     * @throws Exception
     */
    public function addMultipleKeywords(array $ids): bool
    {
        $Keywords = TableRegistry::getTableLocator()->get('Keywords');
        // préserve l'ordre choisi
        foreach ($ids as $id) {
            $keywordData = $Keywords->find()
                ->where(['Keywords.id' => $id])
                ->contain(['KeywordLists'])
                ->firstOrFail();
            $keyword = self::appendNode(
                $this->orgEntityId,
                'ContentDescriptive',
                $this->path,
                $this->dom,
                $this->element
            );
            $keyword->nodeValue = null; // supprime les elements obligatoire automatiquement créé
            $content = $this->dom->createElement('KeywordContent');
            $keyword->appendChild($content);
            $reference = $this->dom->createElement('KeywordReference');
            $keyword->appendChild($reference);
            DOMUtility::setValue($content, $keywordData->get('name'));
            DOMUtility::setValue($reference, $keywordData->get('code'));
            $reference->setAttribute('schemeID', $keywordData->get('keyword_list')->get('identifier'));
            $reference->setAttribute('schemeName', $keywordData->get('keyword_list')->get('name'));
            $reference->setAttribute('schemeVersionID', $keywordData->get('keyword_list')->get('version'));
        }
        return true;
    }

    /**
     * Ajoute un ensemble de mots-clés en saisie libre
     * @param array $keywords [['name' => ..., 'code' => ...]]
     * @return bool success
     * @throws Exception
     */
    public function addMultipleCustomKeywords(array $keywords): bool
    {
        foreach ($keywords as $keywordData) {
            if (empty($keywordData['name'])) {
                continue;
            }
            $keyword = self::appendNode(
                $this->orgEntityId,
                'ContentDescriptive',
                $this->path,
                $this->dom,
                $this->element
            );
            $keyword->nodeValue = null;
            $content = $this->dom->createElement('KeywordContent');
            $keyword->appendChild($content);
            DOMUtility::setValue($content, $keywordData['name']);
            if (!empty($keywordData['code'])) {
                $reference = $this->dom->createElement('KeywordReference');
                $keyword->appendChild($reference);
                DOMUtility::setValue($reference, $keywordData['code']);
            }
        }
        return true;
    }

    /** @noinspection PhpUnusedPrivateMethodInspection */
    /**
     * Ajoute un HashCode lors de l'ajout d'un fichier
     * @see Seda02Schema::getDocument()
     */
    private function addFileHashCode()
    {
        $xpath = new DOMXPath($this->dom);
        $namespace = $this->dom->documentElement->getAttributeNode('xmlns')->nodeValue;
        $xpath->registerNamespace('ns', $namespace);

        $filename = $this->data['Attachment'][0]['filename'];
        $integrityElement = null;
        $contains = null;
        foreach ($xpath->query('/ns:ArchiveTransfer/ns:Integrity/ns:UnitIdentifier') as $element) {
            if ($element instanceof DOMElement && $element->nodeValue === $filename) {
                $integrityElement = $element->parentNode;
                $contains = $xpath->query('ns:Contains', $integrityElement)->item(0);
                break;
            }
        }
        if (!$integrityElement) {
            $contains = $this->dom->createElementNS($namespace, 'Contains');
            $uid = $this->dom->createElementNS($namespace, 'UnitIdentifier');
            DOMUtility::setValue($uid, $filename);
            $integrityElement = self::appendNode(
                $this->orgEntityId,
                'Integrity',
                'ArchiveTransfer',
                $this->dom,
                $this->dom->documentElement
            );
            $integrityElement->appendChild($contains);
            $integrityElement->appendChild($uid);
        }

        $uri = self::getDataDirectory($this->entity->get('id')).DS.'attachments'.DS.$filename;
        if (!is_file($uri)) {
            return;
        }
        $TransferAttachments = TableRegistry::getTableLocator()->get('TransferAttachments');
        $attachment = $TransferAttachments->find()
            ->where(
                [
                    'transfer_id' => $this->entity->get('id'),
                    'filename' => $filename,
                ]
            )
            ->first();
        if ($attachment) {
            $contains->setAttribute('algorithme', $attachment->get('hash_algo'));
            DOMUtility::setValue($contains, $attachment->get('hash'));
            return;
        }

        $neededTime = (int)(filesize($uri) / 10000000); // 1s / 10Mo
        if ($neededTime > 30) {
            set_time_limit($neededTime);
        }
        $contains->setAttribute('algorithme', 'sha256');
        DOMUtility::setValue($contains, hash_file('sha256', $uri));
    }

    /** @noinspection PhpUnusedPrivateMethodInspection */
    /**
     * Ajoute un HashCode lors de l'ajout d'un fichier
     * @see Seda02Schema::getDocument()
     */
    private function deleteDocument()
    {
        $xpath = new DOMXPath($this->dom);
        $namespace = $this->dom->documentElement->getAttributeNode('xmlns')->nodeValue;
        $xpath->registerNamespace('ns', $namespace);

        /** @var DOMElement $element */
        $element = $xpath->query('ns:Attachment', $this->element)->item(0);
        $filename = $element->getAttribute('filename');
        foreach ($xpath->query('/ns:ArchiveTransfer/ns:Integrity/ns:UnitIdentifier') as $element) {
            if ($element instanceof DOMElement && $element->nodeValue === $filename) {
                $integrityElement = $element->parentNode;
                $integrityElement->parentNode->removeChild($integrityElement);
                break;
            }
        }
    }

    /**
     * @inheritDoc
     */
    public static function getVersion(): string
    {
        return '0.2';
    }
}
