<?php
/**
 * AsalaeCore\Form\Seda10Form
 */

namespace AsalaeCore\Form;

use AsalaeCore\Form\MessageSchema\Seda10Schema;
use AsalaeCore\Utility\DOMUtility;
use Cake\Datasource\EntityInterface;
use Cake\Form\Form;
use Cake\ORM\TableRegistry;
use DOMDocument;
use DOMElement;
use DOMXPath;
use Exception;

/**
 * Formulaire d'édition d'un message seda v1.0
 *
 * @category Form
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Seda10Form extends Form implements MessageFormInterface
{
    const SCHEMA_CLASS = Seda10Schema::class;
    const SCHEMA_CACHE_KEY = 'Seda10_schema';
    const SCHEMA_VALIDATION = SEDA_V10_XSD;

    /**
     * Traits
     */
    use GenericMessageFormTrait;

    /**
     * Donne la liste des fichiers utilisés dans le xml ($path)
     * @param string $path
     * @return array
     */
    public static function listFiles(string $path): array
    {
        $dom = new DOMDocument;
        $dom->load($path);
        $namespace = $dom->documentElement->getAttributeNode('xmlns')->nodeValue;
        $xpath = new DOMXPath($dom);
        $xpath->registerNamespace('ns', $namespace);
        $nodes = $xpath->query('//ns:Attachment');
        $files = [];
        foreach ($nodes as $node) {
            if ($node instanceof DOMElement) {
                $files[] = $node->getAttribute('filename');
            }
        }
        return $files;
    }

    /**
     * Applique des modifications à un xml afin de corriger certaines erreurs
     * comme l'absence de bloc Integrity dans les balises "Document"
     * @param EntityInterface $entity
     * @param string|null     $xml
     * @throws Exception
     */
    public static function autoRepair(EntityInterface $entity, string $xml = null)
    {
        if (!$xml) {
            $xml = $entity->get('xml');
        }
        $save = false;
        $dom = new DOMDocument;
        $dom->load($xml);
        $namespace = $dom->documentElement->getAttributeNode('xmlns')->nodeValue;
        $xpath = new DOMXPath($dom);
        $xpath->registerNamespace('ns', $namespace);
        $nodes = $xpath->query('//ns:Attachment');
        $TransferAttachments = TableRegistry::getTableLocator()->get('TransferAttachments');
        /** @var DOMElement $attachmentNode */
        foreach ($nodes as $attachmentNode) {
            /** @var DOMElement $document */
            $document = $attachmentNode->parentNode;
            $filename = $attachmentNode->getAttribute('filename');
            if ($filename[0] === '/') {
                $filename = ltrim($filename, '/');
                $attachmentNode->setAttribute('filename', $filename);
                $save = true;
            }
            $attachment = $TransferAttachments->find()
                ->where(
                    [
                        'transfer_id' => $entity->get('id'),
                        'filename' => $filename,
                    ]
                )
                ->first();
            $integrity = $document->getElementsByTagName('Integrity')->item(0);
            if (!$attachment || $integrity) {
                continue;
            }
            $save = true;
            $integrity = static::appendNode(
                $entity->get('archival_agency_id'),
                'Integrity',
                implode('.', DOMUtility::getElementPath($document)),
                $dom,
                $document
            );
            $integrity->setAttribute('algorithme', $attachment->get('hash_algo'));
            DOMUtility::setValue($integrity, $attachment->get('hash'));
        }
        if ($save) {
            $dom->save($xml);
        }
    }

    /**
     * Ajoute un ensemble de mots-clés
     * @param array $ids keyword_id[]
     * @return bool success
     * @throws Exception
     */
    public function addMultipleKeywords(array $ids): bool
    {
        $Keywords = TableRegistry::getTableLocator()->get('Keywords');
        // préserve l'ordre choisi
        foreach ($ids as $id) {
            $keywordData = $Keywords->find()
                ->where(['Keywords.id' => $id])
                ->contain(['KeywordLists'])
                ->firstOrFail();
            $keyword = self::appendNode($this->orgEntityId, 'Keyword', $this->path, $this->dom, $this->element);
            $keyword->nodeValue = null; // supprime les elements obligatoire automatiquement créé
            $content = $this->dom->createElement('KeywordContent');
            $keyword->appendChild($content);
            $reference = $this->dom->createElement('KeywordReference');
            $keyword->appendChild($reference);
            DOMUtility::setValue($content, $keywordData->get('name'));
            DOMUtility::setValue($reference, $keywordData->get('code'));
            $reference->setAttribute('schemeID', $keywordData->get('keyword_list')->get('identifier'));
            $reference->setAttribute('schemeName', $keywordData->get('keyword_list')->get('name'));
            $reference->setAttribute('schemeVersionID', $keywordData->get('keyword_list')->get('version'));
        }
        return true;
    }

    /**
     * Ajoute un ensemble de mots-clés en saisie libre
     * @param array $keywords [['name' => ..., 'code' => ...]]
     * @return bool success
     * @throws Exception
     */
    public function addMultipleCustomKeywords(array $keywords): bool
    {
        foreach ($keywords as $keywordData) {
            if (empty($keywordData['name'])) {
                continue;
            }
            $keyword = self::appendNode($this->orgEntityId, 'Keyword', $this->path, $this->dom, $this->element);
            $keyword->nodeValue = null;
            $content = $this->dom->createElement('KeywordContent');
            $keyword->appendChild($content);
            DOMUtility::setValue($content, $keywordData['name']);
            if (!empty($keywordData['code'])) {
                $reference = $this->dom->createElement('KeywordReference');
                $keyword->appendChild($reference);
                DOMUtility::setValue($reference, $keywordData['code']);
            }
        }
        return true;
    }

    /** @noinspection PhpUnusedPrivateMethodInspection */
    /**
     * Ajoute un HashCode lors de l'ajout d'un fichier
     * @see Seda10Schema::getDocument()
     */
    private function addFileIntegrity()
    {
        $xpath = new DOMXPath($this->dom);
        $namespace = $this->dom->documentElement->getAttributeNode('xmlns')->nodeValue;
        $xpath->registerNamespace('ns', $namespace);

        $filename = $this->data['Attachment'][0]['filename'];
        $integrityElement = $xpath->query('ns:Integrity', $this->element)->item(0);
        if (!$integrityElement) {
            $integrityElement = self::appendNode(
                $this->orgEntityId,
                'Integrity',
                $this->path,
                $this->dom,
                $this->element
            );
        }
        $sizeElement = $xpath->query('ns:Size', $this->element)->item(0);
        if (!$sizeElement) {
            $sizeElement = self::appendNode(
                $this->orgEntityId,
                'Size',
                $this->path,
                $this->dom,
                $this->element
            );
        }

        $uri = self::getDataDirectory($this->entity->get('id')).DS.'attachments'.DS.$filename;
        if (!is_file($uri)) {
            return;
        }
        $TransferAttachments = TableRegistry::getTableLocator()->get('TransferAttachments');
        $attachment = $TransferAttachments->find()
            ->where(
                [
                    'transfer_id' => $this->entity->get('id'),
                    'filename' => $filename,
                ]
            )
            ->first();
        if ($attachment) {
            $integrityElement->setAttribute('algorithme', $attachment->get('hash_algo'));
            DOMUtility::setValue($integrityElement, $attachment->get('hash'));
            $sizeElement->setAttribute('unitCode', 'Q12');
            DOMUtility::setValue($sizeElement, $attachment->get('size'));
            return;
        }

        $neededTime = (int)(filesize($uri) / 10000000); // 1s / 10Mo
        if ($neededTime > 30) {
            set_time_limit($neededTime);
        }
        $integrityElement->setAttribute('algorithme', 'sha256');
        DOMUtility::setValue($integrityElement, hash_file('sha256', $uri));

        $sizeElement->setAttribute('unitCode', 'Q12');
        DOMUtility::setValue($sizeElement, filesize($uri));
    }

    /**
     * @inheritDoc
     */
    public static function getVersion(): string
    {
        return '1.0';
    }
}
