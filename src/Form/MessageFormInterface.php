<?php
/**
 * AsalaeCore\Form\MessageFormInterface
 */

namespace AsalaeCore\Form;

use Cake\Datasource\EntityInterface;
use DOMDocument;
use DOMElement;

/**
 * Interface des formulaires de type Message (SEDA)
 *
 * @category Form
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
interface MessageFormInterface
{
    /**
     * Initialise le formulaire pour un chemin xml donné
     * @param int|string       $orgEntityId id de l'entité de l'utilisateur
     * @param string           $path
     * @param DOMDocument|null $dom
     * @param DOMElement|null  $element
     * @return MessageFormInterface|GenericMessageFormTrait
     */
    public static function buildForm(
        $orgEntityId,
        string $path = '',
        DOMDocument $dom = null,
        DOMElement $element = null
    ): MessageFormInterface;

    /**
     * Permet d'obtenir l'arbre pour l'importer dans jstree
     * @param string $xmlUri
     * @return string
     */
    public function getDataJsTree(string $xmlUri): string;

    /**
     * Execute the form if it is valid.
     *
     * First validates the form, then calls the `_execute()` hook method.
     * This hook method can be implemented in subclasses to perform
     * the action of the form. This may be sending email, interacting
     * with a remote API, or anything else you may need.
     *
     * @param array $data Form data.
     * @return bool False on validation failure, otherwise returns the
     *   result of the `_execute()` method.
     */
    public function execute(array $data);

    /**
     * Effectue la validation d'un message xml et renvoi un array d'erreurs
     * @param string $path
     * @return array
     */
    public function validateFile(string $path): array;

    /**
     * Ajoute un element en respectant l'ordre dans le schema
     * @param string|int  $orgEntityId
     * @param string      $append      tagName de l'element à ajouter
     * @param string      $path
     * @param DOMDocument $dom
     * @param DOMElement  $parentNode  element sur lequel ajouter le nouveau noeud
     * @return DOMElement
     */
    public static function appendNode(
        $orgEntityId,
        string $append,
        string $path,
        DOMDocument $dom,
        DOMElement $parentNode
    ): DOMElement;

    /**
     * Donne la liste des fichiers utilisés dans le xml ($path)
     * @param string $path
     * @return array
     */
    public static function listFiles(string $path): array;

    /**
     * Ajoute un ensemble de mots-clés
     * @param array $ids keyword_id[]
     * @return bool success
     */
    public function addMultipleKeywords(array $ids): bool;

    /**
     * Ajoute un ensemble de mots-clés en saisie libre
     * @param array $keywords [['name' => ..., 'code' => ...]]
     * @return bool success
     */
    public function addMultipleCustomKeywords(array $keywords): bool;

    /**
     * Permet de passer l'entité à la classe de formulaire pour certains callbacks
     * @param EntityInterface $entity
     * @return MessageFormInterface
     */
    public function setEntity(EntityInterface $entity): MessageFormInterface;

    /**
     * Retire le noeud courant du dom (donné par self::buildForm($id, $path))
     * @return MessageFormInterface
     */
    public function removeNode(): MessageFormInterface;

    /**
     * Donne la version du schema utilisé
     * @return string
     */
    public static function getVersion(): string;
}
