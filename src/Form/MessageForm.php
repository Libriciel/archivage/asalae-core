<?php
/**
 * AsalaeCore\Form\MessageForm
 */

namespace AsalaeCore\Form;

use AsalaeCore\Form\MessageSchema\MessageSchema;
use Cake\Http\Exception\NotImplementedException;
use DOMDocument;
use DOMElement;

/**
 * Formulaire d'édition d'un message (seda)
 *
 * @category Form
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class MessageForm
{
    /**
     * Donne le formulaire pour une version spécifique
     * @param string           $version
     * @param null|int         $orgEntityId
     * @param string           $path
     * @param DOMDocument|null $dom
     * @param DOMElement|null  $element
     * @return MessageFormInterface
     */
    public static function buildForm(
        string $version = NAMESPACE_SEDA_10,
        $orgEntityId = null,
        string $path = '',
        DOMDocument $dom = null,
        DOMElement $element = null
    ): MessageFormInterface {
        /** @var string|MessageFormInterface $classname */
        $classname = self::getFormClassname($version);
        return $classname::buildForm(
            $orgEntityId,
            $path,
            $dom,
            $element
        );
    }

    /**
     * Donne le formulaire lié à une version (seda)
     * @param string $version
     * @return string
     */
    public static function getFormClassname(string $version = NAMESPACE_SEDA_10): string
    {
        switch (MessageSchema::getNamespace($version)) {
            case NAMESPACE_SEDA_02:
                return Seda02Form::class;
            case NAMESPACE_SEDA_10:
                return Seda10Form::class;
            case NAMESPACE_SEDA_21:
                return Seda21Form::class;
            case NAMESPACE_SEDA_22:
                return Seda22Form::class;
            default:
                throw new NotImplementedException;
        }
    }
}
