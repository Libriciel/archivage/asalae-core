<?php
/**
 * AsalaeCore\Form\ConfigurationForm
 * @noinspection PhpConditionAlreadyCheckedInspection faux positif
 */

namespace AsalaeCore\Form;

use AsalaeCore\Utility\Config;
use AsalaeCore\Utility\Ip;
use Cake\Core\Configure;
use Cake\Database\Connection;
use Cake\Database\DriverInterface;
use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\Validation\Validator;
use DateTimeImmutable;
use Libriciel\Filesystem\Utility\Filesystem;
use ReflectionClass;
use Symfony\Component\Filesystem\Exception\IOException;
use Exception;
use FileValidator\Utility\FileValidator;
use Cake\Datasource\ConnectionManager;
use Cake\Utility\Hash;
use AsalaeCore\Validation\ValidationRule;

/**
 * Formulaire de modification de la configuration
 *
 * @category Form
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ConfigurationForm extends Form
{
    const CONFIG_MAP = [
        'app_paths_data' => 'App.paths.data',
        'ignore_invalid_fullbaseurl' => 'App.ignore_invalid_fullbaseurl',
        'ignore_invalid_datasource' => 'App.ignore_invalid_datasource',
        'app_fullbaseurl' => 'App.fullBaseUrl',
        'hash_algo' => 'hash_algo',
        'password_admin_complexity' => 'Password.admin.complexity',
        'password_complexity' => 'Password.complexity',
        'ratchet_connect' => 'Ratchet.connect',
        'datacompressor_encodefilename' => 'DataCompressorUtility.encodeFilename',
        'filesystem_useshred' => 'FilesystemUtility.useShred',
        'tokens-duration_code' => 'Webservices.tokens-duration.code',
        'tokens-duration_access-token' => 'Webservices.tokens-duration.access-token',
        'tokens-duration_refresh-token' => 'Webservices.tokens-duration.refresh-token',
        'beanstalk_tests_timeout' => 'Beanstalk.tests.timeout',
        'downloads_asyncfilecreationtimelimit' => 'Downloads.asyncFileCreationTimeLimit',
        'downloads_tempfileconservationtimelimit' => 'Downloads.tempFileConservationTimeLimit',
        'proxy_host' => 'Proxy.host',
        'proxy_port' => 'Proxy.port',
        'paginator_limit' => 'Pagination.limit',
        'ajaxpaginator_limit' => 'AjaxPaginator.limit',
        'proxy_username' => 'Proxy.username',
        'proxy_password' => 'Proxy.password',
        'ip_enable_whitelist' => 'Ip.enable_whitelist',
        'ip_whitelist' => 'Ip.whitelist',
        'libriciel_login_logo' => 'Libriciel.login.logo-client.image',
        'libriciel_login_background' => 'Libriciel.login.logo-client.style',
        'emails_from' => 'Email.default.from',
        'emails_class' => 'EmailTransport.default.className',
        'emails_host' => 'EmailTransport.default.host',
        'emails_port' => 'EmailTransport.default.port',
        'emails_username' => 'EmailTransport.default.username',
        'emails_password' => 'EmailTransport.default.password',
        'attestations_destruction_delay' => 'Attestations.destruction_delay',
    ];

    /**
     * A hook method intended to be implemented by subclasses.
     *
     * You can use this method to define the schema using
     * the methods on Cake\Form\Schema, or loads a pre-defined
     * schema from a concrete class.
     *
     * @param Schema $schema The schema to customize.
     * @return Schema The schema to use.
     */
    protected function _buildSchema(Schema $schema): Schema
    {
        $schema->addField('config', ['type' => 'text']);
        $schema->addField('hash_algo', ['type' => 'string']);
        $schema->addField('app_fullbaseurl', ['type' => 'string']);
        $schema->addField('datacompressor_encodefilename', ['type' => 'string']);
        $schema->addField('password_complexity', ['type' => 'integer']);
        $schema->addField('password_admin_complexity', ['type' => 'integer']);
        $schema->addField('filesystem_useshred', ['type' => 'boolean']);
        $schema->addField('libriciel_login_logo', ['type' => 'string']);
        $schema->addField('libriciel_login_background', ['type' => 'string']);
        $schema->addField('assets_global_background', ['type' => 'string']);
        $schema->addField('assets_global_background_position', ['type' => 'string']);
        $schema->addField('emails_from', ['type' => 'string']);
        $schema->addField('emails_class', ['type' => 'string']);
        $schema->addField('emails_host', ['type' => 'string']);
        $schema->addField('emails_port', ['type' => 'integer']);
        $schema->addField('emails_username', ['type' => 'string']);
        $schema->addField('emails_password', ['type' => 'string']);
        $schema->addField('datasources_default_driver', ['type' => 'string']);
        $schema->addField('datasources_default_host', ['type' => 'string']);
        $schema->addField('datasources_default_username', ['type' => 'string']);
        $schema->addField('datasources_default_password', ['type' => 'string']);
        $schema->addField('datasources_default_database', ['type' => 'string']);
        $schema->addField('datasources_default_log', ['type' => 'boolean']);
        $schema->addField('datasources_default_invalid', ['type' => 'boolean']);
        $schema->addField('datasources_debug_kit_driver', ['type' => 'string']);
        $schema->addField('datasources_debug_kit_host', ['type' => 'string']);
        $schema->addField('datasources_debug_kit_username', ['type' => 'string']);
        $schema->addField('datasources_debug_kit_password', ['type' => 'string']);
        $schema->addField('datasources_debug_kit_database', ['type' => 'string']);
        $schema->addField('datasources_test_driver', ['type' => 'string']);
        $schema->addField('datasources_test_host', ['type' => 'string']);
        $schema->addField('datasources_test_username', ['type' => 'string']);
        $schema->addField('datasources_test_password', ['type' => 'string']);
        $schema->addField('datasources_test_database', ['type' => 'string']);
        $schema->addField('tokens-duration_code', ['type' => 'string']);
        $schema->addField('tokens-duration_access-token', ['type' => 'string']);
        $schema->addField('tokens-duration_refresh-token', ['type' => 'string']);
        $schema->addField('beanstalk_tests_timeout', ['type' => 'integer']);
        $schema->addField('downloads_asyncfilecreationtimelimit', ['type' => 'integer']);
        $schema->addField('downloads_tempfileconservationtimelimit', ['type' => 'integer']);
        $schema->addField('proxy_host', ['type' => 'string']);
        $schema->addField('proxy_port', ['type' => 'integer']);
        $schema->addField('proxy_username', ['type' => 'string']);
        $schema->addField('proxy_password', ['type' => 'string']);
        $schema->addField('paginator_limit', ['type' => 'integer']);
        $schema->addField('ajaxpaginator_limit', ['type' => 'integer']);
        $schema->addField('ip_enable_whitelist', ['type' => 'boolean']);
        $schema->addField('ip_whitelist', ['type' => 'array']);
        $schema->addField('attestations_destruction_delay', ['type' => 'integer', 'default' => 0]);

        return $schema;
    }

    /**
     * Valide l'onglet Utilitaires
     *
     * @param Validator $validator
     * @return self
     */
    protected function validateUtilities(Validator $validator): self
    {
        $dateStringIntervalCheck = function ($value) {
            $dateInterval = date_interval_create_from_date_string($value);
            $reference = new DateTimeImmutable;
            $endTime = $reference->add($dateInterval);
            return (bool)($endTime->getTimestamp() - $reference->getTimestamp());
        };
        $validator
            ->add(
                'app_paths_data',
                'custom',
                [
                    'rule' => function ($value) {
                        return is_writable($value)
                            && is_dir($value)
                            && preg_match('/[^\/]$/', $value);
                    },
                    'message' => __("Erreur sur la configuration du dossier data")
                ]
            )
            ->add(
                'config',
                'custom',
                [
                    'rule' => function ($value) {
                        json_decode($value);
                        $error = json_last_error();
                        return $error === JSON_ERROR_NONE;
                    },
                    'message' => __("N'est pas un json valide")
                ]
            )
            ->add(
                'hash_algo',
                'inList',
                [
                    'rule' => ['inList', array_keys($this->options('hash_algo'))]
                ]
            )
            ->allowEmptyString('hash_algo')
            ->add(
                'password_complexity',
                'isInteger',
                [
                    'rule' => 'isInteger'
                ]
            )
            ->allowEmptyString('password_complexity')
            ->add(
                'password_admin_complexity',
                'isInteger',
                [
                    'rule' => 'isInteger'
                ]
            )
            ->allowEmptyString('password_admin_complexity');
        $validator
            ->add(
                'ratchet_connect',
                'custom',
                [
                    'rule' => ['custom', '/^wss?:\/\/[\w\.\/\-]+(?::\d+)?(?:\/[\w\.\/\-]+)?$/']
                ]
            )
            ->add(
                'app_fullbaseurl',
                'custom',
                [
                    'rule' => function ($value, $context) {
                        if ($context['data']['ignore_invalid_fullbaseurl'] ?? false) {
                            return true;
                        }
                        stream_context_set_default(
                            [
                                'ssl' => [
                                    'verify_peer' => false,
                                    'verify_peer_name' => false,
                                ],
                            ]
                        );
                        $headers = @get_headers($value);
                        return $headers && $headers[0] !== 'HTTP/1.1 404 Not Found';
                    },
                    'message' => __("N'est pas un dns valide")
                ]
            )
            ->add(
                'datacompressor_encodefilename',
                'inList',
                [
                    'rule' => [
                        'inList',
                        array_merge(
                            array_keys($this->options('datacompressor_encodefilename')),
                            [false]
                        ),
                    ],
                ]
            )
            ->allowEmptyString('datacompressor_encodefilename')
            ->add(
                'filesystem_useshred',
                'inList',
                [
                    'rule' => [
                        'inList',
                        array_merge(
                            array_keys($this->options('filesystem_useshred')),
                            [false]
                        ),
                    ],
                ]
            )
            ->allowEmptyString('filesystem_useshred')
            ->allowEmptyString('tokens-duration_code')
            ->add(
                'tokens-duration_code',
                'interval',
                [
                    'rule' => $dateStringIntervalCheck,
                    'message' => __("Doit être un interval valide")
                ]
            )
            ->allowEmptyString('tokens-duration_access-token')
            ->add(
                'tokens-duration_access-token',
                'interval',
                [
                    'rule' => $dateStringIntervalCheck,
                    'message' => __("Doit être un interval valide")
                ]
            )
            ->allowEmptyString('tokens-duration_refresh-token')
            ->add(
                'tokens-duration_refresh-token',
                'interval',
                [
                    'rule' => $dateStringIntervalCheck,
                    'message' => __("Doit être un interval valide")
                ]
            )
            ->allowEmptyString('beanstalk_tests_timeout')
            ->nonNegativeInteger('beanstalk_tests_timeout')
            ->allowEmptyString('downloads_asyncfilecreationtimelimit')
            ->nonNegativeInteger('downloads_asyncfilecreationtimelimit')
            ->allowEmptyString('downloads_tempfileconservationtimelimit')
            ->nonNegativeInteger('downloads_tempfileconservationtimelimit')
            ->nonNegativeInteger('proxy_port')
            ->allowEmptyString('proxy_port')
            ->greaterThanOrEqual('paginator_limit', 1)
            ->greaterThanOrEqual('ajaxpaginator_limit', 1);
        $validator->add(
            'ip_whitelist',
            'admin_in_whitelist',
            [
                'rule' => function ($value, $context) {
                    if (!Hash::get($context, 'data.ip_enable_whitelist')) {
                        return true;
                    }
                    $ip = Hash::get($context, 'data.client_ip');
                    return Ip::inWhitelist($ip, array_merge($value, ['::1', '127.0.0.1']));
                },
                'message' => __("L'IP de l'utilisateur actuel doit faire parti de la liste"),
            ]
        );
        $validator->nonNegativeInteger('attestations_destruction_delay');

        return $this;
    }

    /**
     * Valide l'onglet Apparence
     *
     * @param Validator $validator
     * @return self
     */
    protected function validateAppearance(Validator $validator): self
    {
        $validator
            ->add(
                'libriciel_login_logo',
                'is_file',
                [
                    'rule' => function ($value) {
                        return is_file(WWW_ROOT.$value);
                    },
                    'message' => __("N'est pas un fichier")
                ]
            )
            ->add(
                'libriciel_login_logo',
                'readable',
                [
                    'rule' => function ($value) {
                        return !is_file(WWW_ROOT.$value) || is_readable(WWW_ROOT.$value);
                    },
                    'message' => __("Ne peut pas être lu, vérifiez les autorisations")
                ]
            )
            ->add(
                'libriciel_login_logo',
                'is_image',
                [
                    'rule' => function ($value) {
                        $basedir = Configure::read('App.paths.data');
                        return !is_readable($basedir.$value)
                        || (preg_match('/^image\//', mime_content_type($basedir.$value))
                        && FileValidator::check($basedir.$value));
                    },
                    'message' => __("N'est pas une image valide")
                ]
            )
            ->allowEmptyString('libriciel_login_logo')
            ->allowEmptyString('libriciel_login_background')
            ->add(
                'assets_global_background',
                'is_file',
                [
                    'rule' => function ($value) {
                        return is_file($value);
                    },
                    'message' => __("N'est pas un fichier")
                ]
            )
            ->add(
                'assets_global_background',
                'readable',
                [
                    'rule' => function ($value) {
                        return !is_file($value) || is_readable($value);
                    },
                    'message' => __("Ne peut pas être lu, vérifiez les autorisations")
                ]
            )
            ->add(
                'assets_global_background',
                'is_image',
                [
                    'rule' => function ($value) {
                        return !is_readable($value)
                        || (preg_match('/^image\//', mime_content_type($value))
                        && FileValidator::check($value));
                    },
                    'message' => __("N'est pas une image valide")
                ]
            )
            ->allowEmptyString('assets_global_background')
            ->allowEmptyString('assets_global_background_position');
        return $this;
    }

    /**
     * Valide l'onglet E-mails
     *
     * @param Validator $validator
     * @return self
     */
    protected function validateEmails(Validator $validator): self
    {
        $validator
            ->add('emails_from', 'email', ['rule' => 'email'])
            ->allowEmptyString('emails_from')
            ->allowEmptyString('emails_host')
            ->add(
                'emails_port',
                'isInteger',
                [
                    'rule' => 'isInteger'
                ]
            )
            ->add(
                'emails_port',
                'is_port',
                [
                    'rule' => function ($value) {
                        return (int)$value > 0 && (int)$value <= 65535;
                    },
                    'message' => __("N'est pas une image valide")
                ]
            )
            ->allowEmptyString('emails_port')
            ->allowEmptyString('emails_username')
            ->allowEmptyString('emails_password');
        return $this;
    }

    /**
     * Valide l'onglet Datasources
     *
     * @param Validator $validator
     * @return self
     */
    protected function validateDatasources(Validator $validator): self
    {
        $default = Config::readDefault();
        foreach (['default', 'debug_kit', 'test'] as $datasource) {
            $rule = new ValidationRule(
                [
                    'rule' => function (
                        $value,
                        $context
                    ) use (
                        $datasource,
                        $default
                    ) {
                        if ($context['data']["ignore_invalid_datasource"] ?? false) {
                            return true;
                        }
                        try {
                            $json = json_decode($context['data']['config'], true);
                        } catch (Exception $e) {
                            $json = [];
                        }
                        if (isset($json['Datasources'][$datasource])) {
                            $default['Datasources'][$datasource] = array_merge(
                                $default['Datasources'][$datasource],
                                $json['Datasources'][$datasource]
                            );
                        }
                        foreach (['driver', 'host', 'username', 'password', 'database'] as $k) {
                            $v = $context['data']['datasources_'.$datasource.'_'.$k] ?? null;
                            if (!empty($v)) {
                                $default['Datasources'][$datasource][$k] = $v;
                            }
                        }

                        try {
                            ConnectionManager::drop('validate_datasource');
                            ConnectionManager::setConfig(
                                'validate_datasource',
                                Hash::get($default, 'Datasources.'.$datasource)
                            );
                            $conn = ConnectionManager::get('validate_datasource');
                            if ($conn instanceof Connection) {
                                $conn->query('select 1');
                            }
                            return true;
                        } catch (Exception $e) {
                            return $e->getMessage();
                        }
                    },
                    'message' => __("La connexion à la base de données a échoué")
                ]
            );
            $validator
                ->add(
                    'datasources_'.$datasource.'_driver',
                    'isDriver',
                    [
                        'rule' => function ($value) {
                            return class_exists($value)
                            && (new ReflectionClass($value))
                                ->implementsInterface(DriverInterface::class);
                        },
                        'message' => __("Le driver doit être une classe qui implémente {0}", DriverInterface::class)
                    ]
                )
                ->allowEmptyString('datasources_'.$datasource.'_driver')
                ->allowEmptyString('datasources_'.$datasource.'_host')
                ->allowEmptyString('datasources_'.$datasource.'_username')
                ->allowEmptyString('datasources_'.$datasource.'_password')
                ->allowEmptyString('datasources_'.$datasource.'_database')
                ->add('datasources_'.$datasource.'_database', 'connectable', $rule);
        }
        return $this;
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $this->validateUtilities($validator)
            ->validateAppearance($validator)
            ->validateEmails($validator)
            ->validateDatasources($validator);
        return $validator;
    }

    /**
     * Sauvegarde l'onglet Utilitaires
     *
     * @param array $data
     * @param array $config
     * @return self
     */
    protected function saveUtilities(array $data, array &$config): self
    {
        $booleans = [
            'datacompressor_encodefilename',
            'filesystem_useshred',
            'ip_enable_whitelist',
            'ignore_invalid_fullbaseurl',
            'ignore_invalid_datasource',
        ];
        foreach (self::CONFIG_MAP as $dataPath => $configPath) {
            if (in_array($dataPath, $booleans) && isset($data[$dataPath]) && $data[$dataPath] !== '') {
                $data[$dataPath] = $data[$dataPath] === '1';
            }
            $config = Hash::insert($config, $configPath, $data[$dataPath] ?? null);
        }
        foreach ($data as $dataPath => $value) {
            if (preg_match('/^datasources_(default|[\w-]+)_([\w-]+)$/', $dataPath, $m)) {
                $configPath = sprintf('Datasources.%s.%s', $m[1], $m[2]);
                $config = Hash::insert($config, $configPath, $value);
            } elseif (preg_match('/^workers_([\w-]+)_([\w-]+)$/', $dataPath, $m)) {
                $configPath = sprintf('Beanstalk.workers.%s.%s', $m[1], $m[2]);
                $config = Hash::insert($config, $configPath, $value);
            }
        }
        $config = Hash::filter(
            $config,
            function ($var) {
                return $var === 0 || $var === 0.0 || $var === '0' || !empty($var) || $var === false;
            }
        );
        return $this;
    }

    /**
     * Sauvegarde l'onglet Apparence
     *
     * @param array $data
     * @param array $config
     * @return self
     */
    protected function saveAppearance(array $data, array &$config): self
    {
        if (!empty($data['libriciel_login_logo'])) {
            $config['Libriciel']['login']['logo-client']['image'] = $data['libriciel_login_logo'];
        } else {
            unset($config['Libriciel']['login']['logo-client']['image']);
        }
        if (!empty($data['libriciel_login_background'])) {
            $config['Libriciel']['login']['logo-client']['style'] = 'background: '.$data['libriciel_login_background'];
        } else {
            unset($config['Libriciel']['login']['logo-client']['style']);
        }
        if (empty($config['Libriciel']['login']['logo-client'])) {
            unset($config['Libriciel']['login']['logo-client']);
        }
        if (empty($config['Libriciel']['login'])) {
            unset($config['Libriciel']['login']);
        }
        if (empty($config['Libriciel'])) {
            unset($config['Libriciel']);
        }
        return $this;
    }

    /**
     * Sauvegarde l'onglet E-mails
     *
     * @param array $data
     * @param array $config
     * @return self
     */
    protected function saveEmails(array $data, array &$config): self
    {
        if (!empty($data['emails_from'])) {
            $config['Email']['default']['from'] = $data['emails_from'];
        } else {
            unset($config['Email']['default']['from']);
            if (empty($config['Email']['default'])) {
                unset($config['Email']['default']);
            }
            if (empty($config['Email'])) {
                unset($config['Email']);
            }
        }
        if (!empty($data['emails_class'])) {
            $config['EmailTransport']['default']['className'] = $data['emails_class'];
        } else {
            unset($config['EmailTransport']['default']['className']);
        }
        if (!empty($data['emails_host'])) {
            $config['EmailTransport']['default']['host'] = $data['emails_host'];
        } else {
            unset($config['EmailTransport']['default']['host']);
        }
        if (!empty($data['emails_port'])) {
            $config['EmailTransport']['default']['port'] = $data['emails_port'];
        } else {
            unset($config['EmailTransport']['default']['port']);
        }
        if (!empty($data['emails_username'])) {
            $config['EmailTransport']['default']['username'] = $data['emails_username'];
        } else {
            unset($config['EmailTransport']['default']['username']);
        }
        if (!empty($data['emails_password'])) {
            $config['EmailTransport']['default']['password'] = $data['emails_password'];
        } else {
            unset($config['EmailTransport']['default']['password']);
        }
        if (empty($config['EmailTransport']['default'])) {
            unset($config['EmailTransport']['default']);
        }
        if (empty($config['EmailTransport'])) {
            unset($config['EmailTransport']);
        }
        return $this;
    }

    /**
     * Hook method to be implemented in subclasses.
     *
     * Used by `execute()` to execute the form's action.
     *
     * @param array $data Form data.
     * @return bool
     * @throws Exception
     */
    protected function _execute(array $data): bool
    {
        $config = json_decode($data['config'], true);
        $this->appendConfigToData($data, $config);

        $this->saveUtilities($data, $config)
            ->saveAppearance($data, $config)
            ->saveEmails($data, $config);

        ksort($config);
        $json = json_encode($config, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);

        try {
            Filesystem::begin('edit_config');
            Filesystem::dumpFile(
                include Config::read('App.paths.path_to_local_config'),
                $json
            );
            Filesystem::commit('edit_config');
            return true;
        } catch (IOException $e) {
            Filesystem::rollback('edit_config');
            trigger_error($e->getMessage());
            return false;
        }
    }

    /**
     * Permet d'obtenir les options possible pour un $field
     *
     * @param string $field
     * @return array
     */
    public static function options(string $field): array
    {
        switch ($field) {
            case 'hash_algo':
                return [
                    'sha256' => __x('hash_algo', "sha256 - Lourd"),
                    'sha512' => __x('hash_algo', "sha512 - Très lourd"),
                    'whirlpool' => __x('hash_algo', "whirlpool - Ultra lourd (+ secure)"),
                ];
            case 'password_admin_complexity':
            case 'password_complexity':
                return [
                    -1 =>  __x('password_complexity', "-- Valeur custom --"),
                    PASSWORD_ULTRA_WEAK => __x(
                        'password_complexity',
                        "Très faible (49) - (~8 caractères avec chiffres lettres majuscules et spéciaux)"
                    ),
                    PASSWORD_WEAK => __x(
                        'password_complexity',
                        "Faible (78) - Taille minimale recommandée par l'ANSSI (~12)"
                    ),
                    PASSWORD_MEDIUM => __x(
                        'password_complexity',
                        "Moyen (82) - Taille recommandée par l'ANSSI (~16)"
                    ),
                    PASSWORD_STRONG => __x(
                        'password_complexity',
                        "Fort (130) - Equivalent clef 128bits (~20)"
                    ),
                ];
            case 'datacompressor_encodefilename':
                return [
                    '0' => __x('datacompressor_encodefilename', "Ne pas ré-encoder les noms des fichiers"),
                    'IBM850' => __x('datacompressor_encodefilename', "IBM850 - Encodage Windows"),
                    'ASCII//TRANSLIT' => __x(
                        'datacompressor_encodefilename',
                        "ASCII//TRANSLIT - Retire les caractères spéciaux (compatibilité max)"
                    ),
                ];
            case 'filesystem_useshred':
                return [
                    '0' => __("Non"),
                    '1' => __("Oui"),
                ];
            default:
                return [];
        }
    }

    /**
     * Ajoute à data, les valeurs ajoutés dans config
     * @param array      $data
     * @param array|null $config
     * @return void
     */
    private function appendConfigToData(array &$data, $config)
    {
        foreach (self::CONFIG_MAP as $dataKey => $configPath) {
            if (!isset($data[$dataKey])) {
                $value = Hash::get($config, $configPath);
                if ($value !== null) {
                    $data[$dataKey] = $value;
                }
            }
        }
    }
}
