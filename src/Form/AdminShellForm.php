<?php
/**
 * AsalaeCore\Form\AdminShellForm
 */

namespace AsalaeCore\Form;

use Cake\Auth\DefaultPasswordHasher;
use Cake\Core\Configure;
use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\Utility\Hash;
use Cake\Validation\Validator;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;
use Symfony\Component\Filesystem\Exception\IOException;

/**
 * Formulaire d'ajout d'un admin (shell)
 *
 * @category Form
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class AdminShellForm extends Form
{
    /**
     * Permet d'obtenir une instance du formulaire
     * @return AdminShellForm
     */
    public static function buildForm(): AdminShellForm
    {
        return new self;
    }

    /**
     * A hook method intended to be implemented by subclasses.
     *
     * You can use this method to define the schema using
     * the methods on Cake\Form\Schema, or loads a pre-defined
     * schema from a concrete class.
     *
     * @param Schema $schema The schema to customize.
     * @return Schema The schema to use.
     */
    protected function _buildSchema(Schema $schema): Schema
    {
        $schema->addField('username', ['type' => 'string']);
        $schema->addField('name', ['type' => 'string']);
        $schema->addField('email', ['type' => 'string']);
        $schema->addField('password', ['type' => 'string']);
        $schema->addField('confirm-password', ['type' => 'string']);
        $schema->addField('notify', ['type' => 'boolean', 'default' => true]);
        return $schema;
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator->add(
            'username',
            'unique',
            [
                'rule' => function ($value, $context) {
                    if (Hash::get($context, 'data.action') !== 'add') {
                        return true;
                    }
                    $administrators = Configure::read(
                        'App.paths.administrators_json',
                        Configure::read('App.paths.data') . DS . 'administrateurs.json'
                    );
                    $json = is_file($administrators) ? json_decode(file_get_contents($administrators), true) : [];
                    return empty(
                        array_filter(
                            $json,
                            function ($v) use ($value) {
                                return ($v['username'] ?? '') === $value;
                            }
                        )
                    );
                },
                'message' => __("Cet identifiant est déjà utilisé")
            ]
        );

        $validator->allowEmptyString('name');

        $validator->allowEmptyString('email')
            ->email('email', false, __("N'est pas un email valide"));

        $validator->allowEmptyString('password')
            ->add(
                'password',
                'score',
                [
                    'rule' => function ($value, $context) {
                        if (!empty($context['data']['ignorePasswordComplexity'])) {
                            return true;
                        }
                        $minScore = Configure::read('Password.admin.complexity', PASSWORD_WEAK);
                        $score = AdminShellForm::scorePassword($value);
                        return $score >= $minScore;
                    },
                    'message' => __("Le mot de passe n'est pas assez complexe")
                ]
            )
            ->sameAs('password', 'confirm-password');

        $validator->allowEmptyString('confirm-password');

        return $validator;
    }


    /**
     * Permet d'obtenir un score à partir d'un password
     * @see https://www.ssi.gouv.fr/administration/precautions-elementaires/calculer-la-force-dun-mot-de-passe/
     * @param string $password
     * @return float
     */
    public static function scorePassword(string $password): float
    {
        $charLists = [
            '0123456789',
            'abcdefghijklmnopqrstuvwxyz',
            'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
            'éèçàù',
            'âêîôûŷäëïöüÿ',
            'ÉÈÇÀÙÂÊÎÔÛŶÄËÏÖÜŸ',
            preg_quote('^$,;:!/*-+.<&"\'(_)=°>¨£%µ?./§~#{[|`\@]}€¤', '/'),
        ];
        $chars = 0;
        foreach ($charLists as $list) {
            if (preg_match("/[$list]/", $password)) {
                $chars += strlen($list);
            }
        }
        // Si un char n'est pas listé, on ajoute 20
        if (preg_match('/[^'.implode('', $charLists).']/', $password)) {
            $chars += 20;
        }
        return strlen($password) * log($chars, 2);
    }

    /**
     * Hook method to be implemented in subclasses.
     *
     * Used by `execute()` to execute the form's action.
     *
     * @param array $data Form data.
     * @return bool
     * @throws Exception
     */
    protected function _execute(array $data): bool
    {
        $path = Configure::read(
            'App.paths.administrators_json',
            Configure::read('App.paths.data') . DS . 'administrateurs.json'
        );
        $admins = is_file($path) ? json_decode(file_get_contents($path), true) : [];
        if (!empty($data['password'])) {
            $data['password'] = (new DefaultPasswordHasher)->hash($data['password']);
        }
        $action = $data['action'] ?? '';
        unset(
            $data['action'],
            $data['encrypted'],
            $data['confirm-password'],
            $data['ignorePasswordComplexity']
        );
        switch ($action) {
            case 'add':
                $admins[] = $data;
                break;
            case 'edit':
                $this->formatForEdit($admins, $data);
                break;
            case 'delete':
                $admins = array_filter(
                    $admins,
                    function ($v) use ($data) {
                        return $v['username'] !== $data['username'];
                    }
                );
                break;
        }
        try {
            Filesystem::dumpFile($path, json_encode($admins, JSON_PRETTY_PRINT));
            return true;
        } catch (IOException $e) {
            return false;
        }
    }

    /**
     * Gestion des mots de passes et des cookies pour l'édition
     * @param array $admins
     * @param array $data
     * @return void
     */
    private function formatForEdit(array &$admins, array $data)
    {
        foreach ($admins as $key => $value) {
            if ($value['username'] === $data['username']) {
                $password = $value['password'];
                $admins[$key] = $data;
                if (!$data['password']) {
                    $admins[$key]['password'] = $password;
                }
                if ($value['cookies'] ?? null) {
                    $admins[$key]['cookies'] = $value['cookies'];
                }
                break;
            }
        }
    }
}
