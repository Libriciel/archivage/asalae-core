<?php
/**
 * AsalaeCore\Form\ForgottenPasswordForm
 */

namespace AsalaeCore\Form;

use AsalaeCore\Factory\Utility;
use Cake\Datasource\EntityInterface;
use Cake\Core\Configure;
use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\Mailer\Mailer;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Exception;

/**
 * Formulaire de récupération du mot de passe
 *
 * @category Form
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ForgottenPasswordForm extends Form
{
    /**
     * @var EntityInterface si execute produit un AccessCode, il se retrouve ici
     */
    public $code;
    /**
     * @var Mailer object Mailer construit près à envoyer
     */
    public $mailer;

    /**
     * A hook method intended to be implemented by subclasses.
     *
     * You can use this method to define the schema using
     * the methods on Cake\Form\Schema, or loads a pre-defined
     * schema from a concrete class.
     *
     * @param Schema $schema The schema to customize.
     * @return Schema The schema to use.
     */
    protected function _buildSchema(Schema $schema): Schema
    {
        $schema->addField('username', ['type' => 'text']);
        $schema->addField('email', ['type' => 'text']);
        return $schema;
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator->notEmptyString(
            'username',
            __("Veuillez renseigner un nom d'utilisateur")
        );
        $validator->notEmptyString(
            'email',
            __("Veuillez renseigner une adresse mail")
        );
        return $validator;
    }

    /**
     * Envoi un mot de passe à l'utilisateur si il existe
     * @param array $data
     * @return bool
     * @throws Exception
     */
    public function _execute(array $data): bool
    {
        $Users = TableRegistry::getTableLocator()->get('Users');
        $user = $Users->find()
            ->where(
                [
                    'username' => $data['username'],
                    'email' => $data['email'],
                    'agent_type' => 'person',
                    'active' => true,
                ]
            )
            ->first();
        if (!$user || !$user->get('email')) {
            $this->setErrors(
                [
                    'user' => ['notFound' => __("L'utilisateur n'a pas été trouvé ou est désactivé")]
                ]
            );
            return false;
        }
        $AuthUrls = TableRegistry::getTableLocator()->get('AuthUrls');
        $code = $AuthUrls->newEntity(
            [
                'url' => '/users/forgotten-password-mail/'.$user->get('id'),
                'expire' => date('Y-m-d H:i:s', strtotime('+10 minutes'))
            ],
            ['validate' => false]
        );
        $AuthUrls->saveOrFail($code);
        if (explode('@', $user->get('email'))[1] === 'test.fr') {
            return true;
        }
        $prefix = $data['prefix'] ?? Configure::read('App.name', '');

        /** @var Mailer $email */
        $email = clone Utility::get(Mailer::class);
        $email->viewBuilder()
            ->setTemplate('AsalaeCore.forgotten_password')
            ->setLayout('default')
            ->addHelpers(['Html', 'Url']);
        $email->setEmailFormat('both')
            ->setSubject($prefix.'  '.__("Changement du mot de passe"))
            ->setViewVars(
                [
                    "title" => __("Changement du mot de passe"),
                    "user" => $user,
                    "code" => $code->get('code'),
                ]
            )
            ->setTo($user->get('email'));
        $this->mailer = $email;
        return true;
    }

    /**
     * Execute the form if it is valid.
     *
     * First validates the form, then calls the `_execute()` hook method.
     * This hook method can be implemented in subclasses to perform
     * the action of the form. This may be sending email, interacting
     * with a remote API, or anything else you may need.
     *
     * @param array $data    Form data.
     * @param array $options
     * @return bool False on validation failure, otherwise returns the
     *   result of the `_execute()` method.
     */
    public function execute(array $data, array $options = []): bool
    {
        return parent::execute($data);
    }
}
