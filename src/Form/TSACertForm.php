<?php
/**
 * AsalaeCore\Form\TSACertForm
 */

namespace AsalaeCore\Form;

use AsalaeCore\Utility\Exec;
use Cake\Datasource\EntityInterface;
use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Exception;

/**
 * Formulaire d'ajout d'un certificat TSA
 *
 * @category Form
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class TSACertForm extends Form
{
    const DEFAULT_TIMESTAMPER_TSA_EXTFILE = <<<eot
extensions = extend
[extend]
basicConstraints=CA:FALSE
subjectKeyIdentifier=hash
authorityKeyIdentifier=keyid,issuer
extendedKeyUsage=critical,timeStamping
eot;

    /**
     * @var null|EntityInterface
     */
    public $entity = null;

    /**
     * A hook method intended to be implemented by subclasses.
     *
     * You can use this method to define the schema using
     * the methods on Cake\Form\Schema, or loads a pre-defined
     * schema from a concrete class.
     *
     * @param Schema $schema The schema to customize.
     * @return Schema The schema to use.
     */
    protected function _buildSchema(Schema $schema): Schema
    {
        $schema->addField('countryName', ['type' => 'string']);
        $schema->addField('stateOrProvinceName', ['type' => 'string']);
        $schema->addField('localityName', ['type' => 'string']);
        $schema->addField('organizationName', ['type' => 'string']);
        $schema->addField('organizationalUnitName', ['type' => 'string']);
        $schema->addField('commonName', ['type' => 'string']);
        $schema->addField('emailAddress', ['type' => 'string']);
        $schema->addField('extfile', ['type' => 'text']);
        $schema->addField('directory', ['type' => 'string']);
        $schema->addField('cnf', ['type' => 'text']);
        $schema->addField('name', ['type' => 'string']);
        return $schema;
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator->notEmptyString('countryName');
        $validator->notEmptyString('stateOrProvinceName');
        $validator->notEmptyString('localityName');
        $validator->notEmptyString('organizationName');
        $validator->notEmptyString('organizationalUnitName');
        $validator->notEmptyString('commonName');
        $validator->notEmptyString('emailAddress');
        $validator->notEmptyString('extfile');
        $validator->notEmptyString('directory');
        $validator->add(
            'directory',
            'writable',
            [
                'rule' => function ($value) {
                    return is_writable($value);
                },
                'message' => __("Le dossier indiqué n'est pas inscriptible"),
            ]
        );
        $validator->notEmptyString('cnf');
        $validator->notEmptyString('name');
        return $validator;
    }

    /**
     * Hook method to be implemented in subclasses.
     *
     * Used by `execute()` to execute the form's action.
     *
     * @param array $data Form data.
     * @return bool
     * @throws Exception
     */
    protected function _execute(array $data): bool
    {
        if (empty($data['directory'])) {
            return false;
        }
        $dir = $data['directory'];
        $cnf = str_replace('{{dir}}', $dir, $data['cnf']);
        file_put_contents($dir.DS.'asalae-tsa.cnf', $cnf);

        $tsaKey = openssl_pkey_new(
            [
                'private_key_bits' => 2048,
                'private_key_type' => OPENSSL_KEYTYPE_RSA,
            ]
        );
        $csr = openssl_csr_new(
            [
                "countryName" => $data['countryName'],
                "stateOrProvinceName" => $data['stateOrProvinceName'],
                "localityName" => $data['localityName'],
                "organizationName" => $data['organizationName'],
                "organizationalUnitName" => $data['organizationalUnitName'],
                "commonName" => $data['commonName'],
                "emailAddress" => $data['emailAddress']
            ],
            $tsaKey
        );
        openssl_pkey_export_to_file($tsaKey, $dir.DS.'tsacert.pem');
        openssl_csr_export_to_file($csr, $dir.DS.'tsacert.csr');

        file_put_contents($dir.DS.'extfile', $data['extfile']);
        $in = Exec::escapeshellarg($dir.DS.'tsacert.csr');
        $signkey = Exec::escapeshellarg($dir.DS.'tsacert.pem');
        $out = Exec::escapeshellarg($dir.DS.'tsacert.crt');
        $extfile = Exec::escapeshellarg($dir.DS.'extfile');
        $cmd = "openssl x509 -req -days 3650 -in $in -signkey $signkey -out $out -extfile $extfile 2>&1";
        exec($cmd, $output, $code);
        if ($code !== 0) {
            throw new Exception(implode(PHP_EOL, $output));
        }

        // Ajout de l'horodateur en base
        file_put_contents($dir.DS.'serial', '00000001');
        $Timestampers = TableRegistry::getTableLocator()->get('Timestampers');
        $this->entity = $Timestampers->newEntity(
            [
                'name' => $data['name'],
                'fields' => json_encode(
                    [
                        'name' => $data['name'],

                        'driver' => 'OPENSSL',
                        'ca' => $dir.DS.'tsacert.crt',
                        'crt' => $dir.DS.'tsacert.crt',
                        'pem' => $dir.DS.'tsacert.pem',
                        'cnf' => $dir.DS.'asalae-tsa.cnf',
                    ],
                    JSON_UNESCAPED_SLASHES
                ),
            ]
        );
        return (bool)$Timestampers->save($this->entity);
    }
}
