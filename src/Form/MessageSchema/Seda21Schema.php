<?php
/**
 * AsalaeCore\Form\MessageSchema\Seda21Schema
 */

namespace AsalaeCore\Form\MessageSchema;

use Cake\Cache\Cache;
use Cake\Utility\Hash;

/**
 * Schema d'un message seda v2.1
 *
 * @category Form\MessageSchema
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Seda21Schema implements MessageFormSchemaInterface
{
    /**
     * Traits
     */
    use SchemaTrait;

    /**
     * Contenu de l'objet
     * @param string $context
     * @return array
     */
    public function data(string $context): array
    {
        switch ($context) {
            case 'Archive':
                return [
                    'Archive' => $this->getDataObjectPackage()
                ];
            case 'ArchiveTransfer':
            default:
                return $this->getArchiveTransfer();
        }
    }

    /**
     * ArchiveTransfer
     * @return array
     */
    public function getArchiveTransfer()
    {
        return [
            'ArchiveTransfer' => [
                'text' => __("Transfert d'archives"),
                'element' => 'ArchiveTransfer',
                'cardinality' => '1..1',
                'li_attr' => [
                    'title' => __("Transfert d'archives."),
                ],
                'icon' => [
                    'complete' => 'fa fa-check text-success',
                    'incomplete' => 'fa fa-exclamation-circle text-danger',
                ],
                'children' => $this->list(
                    $this->refBusinessRequestMessageType(),
                    'RelatedTransferReference',
                    'TransferRequestReplyIdentifier',
                    'ArchivalAgency',
                    'TransferringAgency'
                )
            ]
        ];
    }

    /**
     * Donne les attributs pour un type donné
     * @param string $string
     * @return array
     */
    protected function attrs(string $string): array
    {
        switch ($string) {
            case 'TextType':
                return [
                    'xml:lang' => [
                        'placeholder' => 'fr-FR',
                        'help' => __("Code language RFC 3066"),
                    ]
                ];
            case 'IdentifierType':
                return [
                    'schemeID' => [
                        'help' => __("The identification of the identification scheme."),
                    ],
                    'schemeName' => [
                        'help' => __("The name of the identification scheme."),
                    ],
                    'schemeAgencyID' => [
                        'help' => __("The identification of the agency that maintains the identification scheme."),
                    ],
                    'schemeAgencyName' => [
                        'help' => __("The name of the agency that maintains the identification scheme."),
                    ],
                    'schemeVersionID' => [
                        'help' => __("The version of the identification scheme."),
                    ],
                    'schemeDataURI' => [
                        'help' => __(
                            "The Uniform Resource Identifier that identifies"
                            ." where the identification scheme data is located."
                        ),
                    ],
                    'schemeURI' => [
                        'help' => __(
                            "The Uniform Resource Identifier that identifies"
                            ." where the identification scheme is located."
                        ),
                    ],
                ];
            case 'BinaryObjectType':
                return [
                    'filename' => [
                        'help' => __("The filename of the binary object."),
                        'class' => 'filename',
                        'type' => 'hidden',
                    ],
                    'uri' => [
                        'help' => __(
                            "The Uniform Resource Identifier that identifies where the binary object is located."
                        ),
                        'type' => 'hidden',
                    ],
                ];
            case 'CodeType':
                return [
                    'listID' => [
                        'help' => __("The identification of a list of codes."),
                    ],
                    'listAgencyID' => [
                        'help' => __("An agency that maintains one or more code lists."),
                        'options' => self::getXsdOptions(
                            'agencyid',
                            'ccts:Name',
                            'ccts:Definition'
                        ),
                        'empty' => true,
                        'data-placeholder' => __("-- Choisir une identification --"),
                    ],
                    'listAgencyName' => [
                        'help' => __("The name of the agency that maintains the code list."),
                    ],
                    'listName' => [
                        'help' => __("The name of a list of codes."),
                    ],
                    'listVersionID' => [
                        'help' => __("The version of the code list."),
                    ],
                    'name' => [
                        'help' => __("The textual equivalent of the code content."),
                    ],
                    'languageID' => [
                        'help' => __("The identifier of the language used in the corresponding text string."),
                        'pattern' => '[a-zA-Z]{1,8}(-[a-zA-Z0-9]{1,8})*',
                        'validations' => [
                            'islang' => $this->validationIsLang(),
                        ]
                    ],
                    'listURI' => [
                        'help' => __("The Uniform Resource Identifier that identifies where the code list is located."),
                    ],
                    'listSchemeURI' => [
                        'help' => __(
                            "The Uniform Resource Identifier that identifies where the code list scheme is located."
                        ),
                    ],
                ];
            default:
                return [];
        }
    }

    /**
     * ArchiveTransfer.Comment
     * @return array
     */
    public function getComment()
    {
        return [
            'label' => 'Comment : '.__("Commentaires"),
            'type' => 'textarea',
            'data-type' => 'text',
            'cardinality' => '0..n',
            'help' => __("Optionnel"),
            'attributes' => $this->attrs('TextType'),
        ];
    }

    /**
     * ArchiveTransfer.Date
     * @return array
     */
    protected function getDate(): array
    {
        return [
            'label' => "Date : ".__("Date du message."),
            'cardinality' => '1..1',
            'data-type' => 'datetime',
            'validations' => $this->validations('DateTimeType'),
            'class' => 'datepicker',
            'placeholder' => $this->datetimePlaceholder(),
        ];
    }

    /**
     * Donne le nom de l'élément à partir d'un nom généric
     * ex: Archive -> Contains pour du seda 0.2
     * @param string $genericName
     * @param string $context
     * @return string si false, ne contient pas $genericName
     */
    public static function getTagname(string $genericName, string $context)
    {
        switch ($genericName) {
            case 'Archive':
                return $context === 'ArchiveTransfer' ? 'ArchiveUnit' : 'Archive';
            case 'ArchiveObject':
                return 'ArchiveUnit';
            case 'Document':
                return 'BinaryDataObject';
            case 'ArchivalAgencyArchiveIdentifier':
            case 'ArchivalAgencyObjectIdentifier':
                return 'ArchivalAgencyArchiveUnitIdentifier';
            case 'ArchivalAgencyDocumentIdentifier':
                return 'DataObjectSystemId';
            case 'ContentDescription':
                return 'Content';
            default:
                return $genericName;
        }
    }

    /**
     * Donne le fichier xsd de liste de codes
     * @param string $keypath
     * @return string
     */
    public static function getXsdCodePath(string $keypath): string
    {
        return SEDA_V10_CODES.DS.SEDA_V10_CODES_FILES[$keypath];
    }

    /**
     * ref BusinessRequestMessageType
     * @return array
     */
    protected function refBusinessRequestMessageType(): array
    {
        return $this->list(
            $this->refMessageType(),
            'ArchivalAgreement',
            'CodeListVersions',
            'DataObjectPackage'
        );
    }

    /**
     * ref MessageType
     * @return array
     */
    protected function refMessageType(): array
    {
        return $this->list(
            'Comment',
            'Date',
            'MessageIdentifier',
            'Signature'
        );
    }

    /**
     * ArchiveTransfer.MessageIdentifier
     * @return array
     */
    protected function getMessageIdentifier(): array
    {
        return [
            'label' => "MessageIdentifier : ".__("Identifiant du message."),
            'cardinality' => '1..1',
            'attributes' => $this->attrs('IdentifierType'),
            'default' => '#A_CALCULER_LORS_DU_VERROUILLAGE#',
        ];
    }

    /**
     * ArchiveTransfer.ArchivalAgreement
     * @return array
     */
    protected function getArchivalAgreement(): array
    {
        return [
            'label' => "ArchivalAgreement : ".__("Accord de versement."),
            'cardinality' => '0..1',
            'attributes' => $this->attrs('IdentifierType'),
            'options' => $this->optionsArchivalAgreement(),
            'empty' => true,
            'data-placeholder' => __("-- Choisir un accord de versement --"),
        ];
    }

    /**
     * ArchiveTransfer.DataObjectPackage
     * @return array
     */
    protected function getDataObjectPackage(): array
    {
        return [
            'text' => 'DataObjectPackage',
            'cardinality' => '0..1',
            'li_attr' => [
                'title' => __("Objets-données échangés dans le message."),
            ],
            'children' => $this->list(
                'DataObjectGroup',
                'BinaryDataObject',
                'PhysicalDataObject',
                'DescriptiveMetadata',
                'ManagementMetadata'
            ),
            'complex' => [
                [
                    'choice' => [
                        'DataObjectGroup',
                        'BinaryDataObject',
                        'PhysicalDataObject',
                    ]
                ],
                'DescriptiveMetadata',
                'ManagementMetadata',
            ]
        ];
    }

    /**
     * ArchiveTransfer.DataObjectPackage.DataObjectGroup
     * @return array
     */
    protected function getDataObjectGroup(): array
    {
        return [
            'text' => 'DataObjectGroup',
            'cardinality' => '0..n',
            'true_cardinality' => '1..1',
            'children' => $this->list(
                'BinaryDataObject',
                'PhysicalDataObject',
                'LogBook',
                [
                    'custom' => [
                        'type' => 'hidden',
                        'default' => json_encode(
                            [
                                'action' => 'addUUID',
                                'args' => []
                            ]
                        )
                    ],
                ]
            )
        ];
    }

    /**
     * ArchiveTransfer.DataObjectPackage.BinaryDataObject
     * @return array
     */
    protected function getBinaryDataObject(): array
    {
        return [
            'text' => 'BinaryDataObject',
            'cardinality' => '0..n',
            'true_cardinality' => '1..1',
            'li_attr' => [
                'title' => __("Objet-données numérique."),
            ],
            'attr' => [
                'id' => [
                    'required' => true,
                    'label' => __("Identifiant de l'objet-données associé."),
                ]
            ],
            'children' => $this->list(
                $this->extMinimalDataObjectType(),
                'Attachment',
                'Uri',
                'MessageDigest',
                'Size',
                'Compressed',
                'FormatIdentification',
                'FileInfo',
                'Metadata',
                'OtherMetadata',
                [
                    'custom' => [
                        'type' => 'hidden',
                        'default' => json_encode(
                            [
                                'action' => 'multiAction',
                                'args' => [
                                    [
                                        'action' => 'unsetValue',
                                        'args' => ['Attachment']
                                    ],
                                    [
                                        'action' => 'addBinaryDataObject',
                                    ],
                                ]
                            ]
                        )
                    ]
                ]
            ),
            'complex' => [
                'DataObjectSystemId',
                'DataObjectGroupSystemId',
                'Relationship',
                'DataObjectGroupReferenceId',
                'DataObjectGroupId',
                'DataObjectVersion',
                [
                    'choice' => [
                        'Attachment',
                        'Uri',
                    ]
                ],
                'MessageDigest',
                'Size',
                'Compressed',
                'FormatIdentification',
                'FileInfo',
                'Metadata',
                'OtherMetadata',
            ],
            'choice' => [
                'Attachment',
                'Uri',
            ],
        ];
    }

    /**
     * ref MinimalDataObjectType
     * @return array
     */
    protected function extMinimalDataObjectType()
    {
        return $this->list(
            'DataObjectSystemId',
            'DataObjectGroupSystemId',
            'Relationship',
            'DataObjectGroupReferenceId',
            'DataObjectGroupId',
            'DataObjectVersion'
        );
    }

    /**
     * ArchiveTransfer.DataObjectPackage.BinaryDataObject.DataObjectVersion
     * @return array
     */
    protected function getDataObjectVersion(): array
    {
        return [
            'label' => "DataObjectVersion",
            'info' => __(
                "Version d’un objet-données (par exemple : original papier, conservation, diffusion, vignette, txt, …)."
            ),
            'cardinality' => '0..1',
            'attributes' => $this->attrs('IdentifierType'),
        ];
    }

    /**
     * ArchiveTransfer.DataObjectPackage.BinaryDataObject.Attachment
     * @return array
     */
    protected function getAttachment(): array
    {
        return [
            'label' => "Attachment : ".__("Objet-données"),
            'cardinality' => '0..1',
            'true_cardinality' => '1..1',
            'attributes' => $this->attrs('BinaryObjectType'),
            'class' => 'attachments-files-select',
            'onchange' => 'loadAttachment(this)',
            'afterload' => 'initializeAttachment',
            'chosen' => false,
            'options' => [],
            'data-placeholder' => __("-- Sélectionner un fichier --"),
            'allowEmpty' => true, // force la validation à se désactiver car la valeur est set uniquement pour le js
        ];
    }

    /**
     * ArchiveTransfer.DataObjectPackage.BinaryDataObject.MessageDigest
     * @return array
     */
    protected function getMessageDigest(): array
    {
        return [
            'label' => "MessageDigest",
            'cardinality' => '0..1',
            'true_cardinality' => '1..1',
            'disabled' => true,
        ];
    }

    /**
     * ArchiveTransfer.DataObjectPackage.BinaryDataObject.Size
     * @return array
     */
    protected function getSize(): array
    {
        return [
            'label' => "Size",
            'cardinality' => '0..1',
            'disabled' => true,
        ];
    }

    /**
     * ArchiveTransfer.DataObjectPackage.BinaryDataObject.FormatIdentification
     * @return array
     */
    protected function getFormatIdentification(): array
    {
        return [
            'text' => 'FormatIdentification',
            'cardinality' => '0..1',
            'li_attr' => [
                'title' => __("Identification du format de l'objet-données."),
            ],
            'disabled' => true,
            'children' => $this->list(
                'FormatLitteral',
                'MimeType',
                'FormatId',
                'Encoding'
            )
        ];
    }

    /**
     * ArchiveTransfer.DataObjectPackage.BinaryDataObject.FormatLitteral
     * @return array
     */
    protected function getFormatLitteral(): array
    {
        return [
            'label' => "FormatLitteral",
            'cardinality' => '0..1',
            'disabled' => true,
        ];
    }

    /**
     * ArchiveTransfer.DataObjectPackage.BinaryDataObject.MimeType
     * @return array
     */
    protected function getMimeType(): array
    {
        return [
            'label' => "MimeType",
            'cardinality' => '0..1',
            'disabled' => true,
        ];
    }

    /**
     * ArchiveTransfer.DataObjectPackage.BinaryDataObject.FormatId
     * @return array
     */
    protected function getFormatId(): array
    {
        return [
            'label' => "FormatId",
            'cardinality' => '0..1',
            'disabled' => true,
        ];
    }

    /**
     * ArchiveTransfer.DataObjectPackage.BinaryDataObject.FileInfo
     * @return array
     */
    protected function getFileInfo(): array
    {
        return [
            'text' => 'FileInfo',
            'cardinality' => '0..1',
            'li_attr' => [
                'title' => __("Informations sur le fichier lui-même (d'un point de vue technique)."),
            ],
            'disabled' => true,
            'children' => $this->list(
                'Filename',
                'CreatingApplicationName',
                'CreatingApplicationVersion',
                'DateCreatedByApplication',
                'CreatingOs',
                'CreatingOsVersion',
                'LastModified'
            )
        ];
    }

    /**
     * ArchiveTransfer.DataObjectPackage.BinaryDataObject.FileInfo.Filename
     * @return array
     */
    protected function getFilename(): array
    {
        return [
            'label' => "Filename",
            'cardinality' => '1..1',
            'disabled' => true,
        ];
    }

    /**
     * ArchiveTransfer.DataObjectPackage.BinaryDataObject.FileInfo.CreatingApplicationName
     * @return array
     */
    protected function getCreatingApplicationName(): array
    {
        return [
            'label' => "CreatingApplicationName",
            'cardinality' => '0..1',
            'disabled' => true,
        ];
    }

    /**
     * ArchiveTransfer.DataObjectPackage.BinaryDataObject.FileInfo.CreatingApplicationVersion
     * @return array
     */
    protected function getCreatingApplicationVersion(): array
    {
        return [
            'label' => "CreatingApplicationVersion",
            'cardinality' => '0..1',
            'disabled' => true,
        ];
    }

    /**
     * ArchiveTransfer.DataObjectPackage.BinaryDataObject.FileInfo.DateCreatedByApplication
     * @return array
     */
    protected function getDateCreatedByApplication(): array
    {
        return [
            'label' => "DateCreatedByApplication",
            'cardinality' => '0..1',
            'disabled' => true,
            'data-type' => 'datetime',
            'validations' => $this->validations('DateTimeType'),
            'class' => 'datepicker',
            'placeholder' => $this->datePlaceholder(),
        ];
    }

    /**
     * ArchiveTransfer.DataObjectPackage.BinaryDataObject.FileInfo.CreatingOs
     * @return array
     */
    protected function getCreatingOs(): array
    {
        return [
            'label' => "CreatingOs",
            'cardinality' => '0..1',
            'disabled' => true,
        ];
    }

    /**
     * ArchiveTransfer.DataObjectPackage.BinaryDataObject.FileInfo.LastModified
     * @return array
     */
    protected function getLastModified(): array
    {
        return [
            'label' => "LastModified",
            'cardinality' => '0..1',
            'disabled' => true,
        ];
    }

    /**
     * ArchiveTransfer.DataObjectPackage.BinaryDataObject.FileInfo.CreatingOsVersion
     * @return array
     */
    protected function getCreatingOsVersion(): array
    {
        return [
            'label' => "CreatingOsVersion",
            'cardinality' => '0..1',
            'disabled' => true,
        ];
    }

    /**
     * ArchiveTransfer.DataObjectPackage.DescriptiveMetadata
     * @return array
     */
    protected function getDescriptiveMetadata(): array
    {
        return [
            'text' => 'DescriptiveMetadata',
            'cardinality' => '1..1',
            'li_attr' => [
                'title' => __(
                    "Bloc de métadonnées descriptives des objets-données."
                ),
            ],
            'children' => $this->list(
                'ArchiveUnit'
            )
        ];
    }

    /**
     * ArchiveTransfer.DataObjectPackage.DescriptiveMetadata.ArchiveUnit
     * @return array
     */
    protected function getArchiveUnit(): array
    {
        return [
            'text' => 'ArchiveUnit',
            'cardinality' => '0..n',
            'true_cardinality' => '1..1',
            'li_attr' => [
                'title' => __(
                    "Correspond à la notion de composant en ISAD(G). ArchiveUnit "
                    . "permet à la fois de gérer la hiérarchie intellectuelle, tout en contenant "
                    . "les métadonnées de description et de gestion propres à chaque niveau de "
                    . "description archivistique."
                ),
            ],
            'attr' => [
                'id' => [
                    'required' => true,
                    'label' => __("Identifiant de l'objet-données associé."),
                ]
            ],
            'children' => $this->list(
                'ArchiveUnitRefId',
                'ArchiveUnitProfile',
                'Management',
                'Content',
                'ArchiveUnit',
                'DataObjectReference',
                [
                    'custom' => [
                        'type' => 'hidden',
                        'default' => json_encode(
                            [
                                'action' => 'addUUID',
                                'args' => []
                            ]
                        )
                    ],
                ]
            ),
            'complex' => [
                [
                    'choice' => [
                        'ArchiveUnitRefId',
                        [
                            'sequence' => [
                                'ArchiveUnitProfile',
                                'Management',
                                'Content',
                                'ArchiveUnit',
                                'DataObjectReference',
                            ]
                        ]
                    ]
                ]
            ],
        ];
    }

    /**
     * ArchiveTransfer.DataObjectPackage.DescriptiveMetadata.ArchiveUnit.ArchiveUnitRefId
     * @return array
     */
    protected function getArchiveUnitRefId(): array
    {
        return [
            'label' => 'ArchiveUnitRefId',
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'info' => __("Permet de faire une référence à d'autres ArchiveUnit dans la même transaction."),
        ];
    }

    /**
     * ArchiveTransfer.DataObjectPackage.DescriptiveMetadata.ArchiveUnit.ArchiveUnitProfile
     * @return array
     */
    protected function getArchiveUnitProfile(): array
    {
        return [
            'label' => 'ArchiveUnitProfile',
            'cardinality' => '0..1',
            'info' => __(
                "Référence à une partie d'un profil d’archivage applicable à un"
                ." ArchiveUnit en particulier. Permet par exemple de faire référence à "
                ."une typologie documentaire dans un profil d'archivage."
            ),
            'attributes' => $this->attrs('IdentifierType'),
        ];
    }

    /**
     * ArchiveTransfer.DataObjectPackage.DescriptiveMetadata.ArchiveUnit.Management
     * @return array
     */
    protected function getManagement(): array
    {
        return [
            'text' => 'Management',
            'cardinality' => '0..1',
            'li_attr' => [
                'title' => __("Métadonnées de gestion applicables à l’ArchiveUnit concernée et à ses héritiers."),
            ],
            'children' => $this->list(
                $this->refManagementType()
            )
        ];
    }

    /**
     * ref ManagementType
     * @return array
     */
    protected function refManagementType(): array
    {
        return $this->list(
            'StorageRule',
            'AppraisalRule',
            'AccessRule',
            'DisseminationRule',
            'ReuseRule',
            'ClassificationRule',
            'LogBook',
            'NeedAuthorization'
        );
    }

    /**
     * ManagementType.StorageRule
     * @return array
     */
    protected function getStorageRule(): array
    {
        return [
            'text' => 'StorageRule',
            'cardinality' => '0..1',
            'li_attr' => [
                'title' => __("Gestion de la durée d’utilité courante."),
            ],
            'children' => $this->list(
                [
                    'Rule' => [
                        'label' => "Rule : ".__("Règle de durée d'utilité courante."),
                        'cardinality' => '0..n',
                        'true_cardinality' => '1..1',
                        'help' => __("Optionnel"),
                        'info' => __("Référence à la règle de durée d'utilité courante."),
                        'attributes' => [
                            'id' => []
                        ],
                    ],
                ],
                'StartDate',
                'PreventInheritance',
                'RefNonRuleId',
                [
                    'FinalAction' => $this->getFinalAction() + [
                        'options' => [
                            'RestrictAccess' => 'RestrictAccess',
                            'Transfer' => 'Transfer',
                            'Copy' => 'Copy',
                        ],
                    ]
                ]
            ),
            'complex' => [
                [
                    'sequence' => [
                        'Rule',
                        'StartDate',
                    ],
                ],
                [
                    'choice' => [
                        'PreventInheritance',
                        'RefNonRuleId',
                    ],
                ],
                'FinalAction'
            ],
        ];
    }

    /**
     * ManagementType.AppraisalRule
     * @return array
     */
    protected function getAppraisalRule(): array
    {
        $rules = [];
        for ($i = 0; $i <= 100; $i++) {
            $rules['APP'.$i.'Y'] = __n("{0} an", "{0} ans", $i, $i);
        }
        return [
            'text' => 'AppraisalRule',
            'cardinality' => '0..1',
            'li_attr' => [
                'title' => __("Gestion de la durée d’utilité administrative."),
            ],
            'children' => $this->list(
                [
                    'Rule' => [
                        'label' => "Rule : ".__("Durée d'utilité administrative."),
                        'cardinality' => '0..n',
                        'true_cardinality' => '1..1',
                        'help' => __("Optionnel"),
                        'info' => __("Référence à la règle de durée d'utilité administrative."),
                        'attributes' => [
                            'id' => []
                        ],
                        'options' => $rules,
                        'afterload' => 'ajaxSearchAppraisalRuleCode',
                        'chosen' => false,
                        'empty' => true,
                        'data-placeholder' => __("-- Sélectionner une DUA --"),
                    ],
                ],
                'StartDate',
                'PreventInheritance',
                'RefNonRuleId',
                [
                    'FinalAction' => $this->getFinalAction() + [
                        'options' => [
                            'Keep' => 'Keep - '.__("Conserver"),
                            'Destroy' => 'Destroy - '.__("Détruire"),
                        ],
                    ]
                ]
            ),
            'complex' => [
                [
                    'sequence' => [
                        'Rule',
                        'StartDate',
                    ],
                ],
                [
                    'choice' => [
                        'PreventInheritance',
                        'RefNonRuleId',
                    ],
                ],
                'FinalAction'
            ],
        ];
    }

    /**
     * ManagementType.AccessRule
     * @return array
     */
    protected function getAccessRule(): array
    {
        return [
            'text' => 'AccessRule',
            'cardinality' => '0..1',
            'li_attr' => [
                'title' => __("Gestion de la communicabilité."),
            ],
            'children' => $this->list(
                [
                    'Rule' => [
                        'label' => "Rule : ".__("Règle de communicabilité."),
                        'cardinality' => '0..n',
                        'true_cardinality' => '1..1',
                        'help' => __("Optionnel"),
                        'info' => __("Référence à la règle de communicabilité."),
                        'attributes' => [
                            'id' => []
                        ],
                        'options' => self::getXsdOptions('access_code'),
                        'afterload' => 'ajaxSearchAccessRuleCode',
                        'chosen' => false,
                        'empty' => true,
                        'data-placeholder' => __("-- Choisir un code --"),
                    ],
                ],
                'StartDate',
                'PreventInheritance',
                'RefNonRuleId'
            ),
            'complex' => [
                [
                    'sequence' => [
                        'Rule',
                        'StartDate',
                    ],
                ],
                [
                    'choice' => [
                        'PreventInheritance',
                        'RefNonRuleId',
                    ],
                ],
            ],
        ];
    }

    /**
     * ManagementType.DisseminationRule
     * @return array
     */
    protected function getDisseminationRule(): array
    {
        return [
            'text' => 'DisseminationRule',
            'cardinality' => '0..1',
            'li_attr' => [
                'title' => __("Gestion de la diffusion."),
            ],
            'children' => $this->list(
                [
                    'Rule' => [
                        'label' => "Rule : ".__("Règle de diffusion."),
                        'cardinality' => '0..n',
                        'true_cardinality' => '1..1',
                        'help' => __("Optionnel"),
                        'info' => __("Référence à la règle de diffusion."),
                        'attributes' => [
                            'id' => []
                        ],
                    ],
                ],
                'StartDate',
                'PreventInheritance',
                'RefNonRuleId'
            ),
            'complex' => [
                [
                    'sequence' => [
                        'Rule',
                        'StartDate',
                    ],
                ],
                [
                    'choice' => [
                        'PreventInheritance',
                        'RefNonRuleId',
                    ],
                ],
            ],
        ];
    }

    /**
     * ManagementType.ReuseRule
     * @return array
     */
    protected function getReuseRule(): array
    {
        return [
            'text' => 'ReuseRule',
            'cardinality' => '0..1',
            'li_attr' => [
                'title' => __("Gestion de la réutilisation."),
            ],
            'children' => $this->list(
                [
                    'Rule' => [
                        'label' => "Rule : ".__("Règle de réutilisation."),
                        'cardinality' => '0..n',
                        'true_cardinality' => '1..1',
                        'help' => __("Optionnel"),
                        'info' => __("Référence à la règle de réutilisation."),
                        'attributes' => [
                            'id' => []
                        ],
                    ],
                ],
                'StartDate',
                'PreventInheritance',
                'RefNonRuleId'
            ),
            'complex' => [
                [
                    'sequence' => [
                        'Rule',
                        'StartDate',
                    ],
                ],
                [
                    'choice' => [
                        'PreventInheritance',
                        'RefNonRuleId',
                    ],
                ],
            ],
        ];
    }

    /**
     * ManagementType.ClassificationRule
     * @return array
     */
    protected function getClassificationRule(): array
    {
        return [
            'text' => 'ClassificationRule',
            'cardinality' => '0..1',
            'li_attr' => [
                'title' => __("Gestion de la classification."),
            ],
            'children' => $this->list(
                [
                    'Rule' => [
                        'label' => "Rule : ".__("Règle de classification."),
                        'cardinality' => '0..n',
                        'true_cardinality' => '1..1',
                        'help' => __("Optionnel"),
                        'info' => __("Référence à la règle de classification."),
                        'attributes' => [
                            'id' => []
                        ],
                    ],
                ],
                'StartDate',
                'ClassificationAudience',
                'PreventInheritance',
                'RefNonRuleId',
                'ClassificationLevel',
                'ClassificationOwner',
                'ClassificationReassessingDate',
                'NeedReassessingAuthorization'
            ),
            'complex' => [
                [
                    'sequence' => [
                        'Rule',
                        'StartDate',
                    ],
                ],
                'ClassificationAudience',
                [
                    'choice' => [
                        'PreventInheritance',
                        'RefNonRuleId',
                    ],
                ],
                'ClassificationLevel',
                'ClassificationOwner',
                'ClassificationReassessingDate',
                'NeedReassessingAuthorization'
            ],
        ];
    }

    /**
     * ManagementType.*.Rule
     * @return array
     * @deprecated Rule doit être défini selon le contexte
     */
    protected function getRule(): array
    {
        return [
            'label' => "Rule",
            'cardinality' => '0..n',
            'true_cardinality' => '1..1',
            'help' => __("Optionnel"),
            'info' => __("Référence à la règle."),
            'attributes' => [
                'id' => []
            ],
        ];
    }

    /**
     * ManagementType.*.StartDate
     * @return array
     */
    protected function getStartDate(): array
    {
        return [
            'label' => "StartDate",
            'cardinality' => '0..n',
            'true_cardinality' => '0..1',
            'help' => __("Optionnel"),
            'info' => __("Date de départ de calcul de la règle."),
            'data-type' => 'date',
            'validations' => $this->validations('DateType'),
            'class' => 'datepicker',
            'placeholder' => $this->datePlaceholder(),
        ];
    }

    /**
     * ManagementType.ClassificationRule.ClassificationAudience
     */
    protected function getClassificationAudience(): array
    {
        return [
            'label' => "ClassificationAudience",
            'cardinality' => '0..1',
            'info' => __(
                "Permet de gérer les questions de \"diffusion restreinte\","
                ." de \"spécial France\" et de \"Confidentiel Industrie\"."
            ),
            'help' => __("Optionnel"),
        ];
    }

    /**
     * ManagementType.ClassificationRule.ClassificationLevel
     */
    protected function getClassificationLevel(): array
    {
        return [
            'label' => "ClassificationLevel",
            'cardinality' => '1..1',
            'info' => __("Référence au niveau de classification."),
        ];
    }

    /**
     * ManagementType.ClassificationRule.ClassificationOwner
     */
    protected function getClassificationOwner(): array
    {
        return [
            'label' => "ClassificationOwner",
            'cardinality' => '1..1',
            'info' => __("Propriétaire de la classification. Service émetteur au sens de l’IGI 1300."),
        ];
    }

    /**
     * ManagementType.ClassificationRule.ClassificationReassessingDate
     */
    protected function getClassificationReassessingDate(): array
    {
        return [
            'label' => "ClassificationReassessingDate",
            'cardinality' => '0..1',
            'info' => __("Date de réévaluation de la classification."),
            'help' => __("Optionnel"),
            'data-type' => 'date',
            'validations' => $this->validations('DateType'),
            'class' => 'datepicker',
            'placeholder' => $this->datePlaceholder(),
        ];
    }

    /**
     * ManagementType.ClassificationRule.NeedReassessingAuthorization
     */
    protected function getNeedReassessingAuthorization(): array
    {
        return [
            'label' => "NeedReassessingAuthorization",
            'cardinality' => '0..1',
            'info' => __(
                "Indique si une autorisation humaine"
                ." est nécessaire pour réévaluer la classification."
            ),
            'help' => __("Optionnel"),
            'empty' => __("-- Autorisation humaine nécéssaire --"),
            'options' => [
                'true' => __("Oui"),
                'false' => __("Non"),
            ]
        ];
    }

    /**
     * ManagementType.*.FinalAction
     * @return array
     */
    protected function getFinalAction(): array
    {
        return [
            'label' => "FinalAction",
            'cardinality' => '1..1',
            'info' => __(
                "Code correspondant à l’action à entreprendre au terme de la durée."
            ),
            'empty' => true,
            'data-placeholder' => __("-- Choisir un sort final --"),
        ];
    }

    /**
     * ArchiveTransfer.DataObjectPackage.DescriptiveMetadata.ArchiveUnit.Content
     * @return array
     */
    protected function getContent(): array
    {
        return [
            'text' => 'Content',
            'cardinality' => '1..1',
            'li_attr' => [
                'title' => __("Métadonnées de description associées à un ArchiveUnit."),
            ],
            'children' => $this->list(
                'DescriptionLevel',
                'Title',
                'FilePlanPosition',
                'SystemId',
                'OriginatingSystemId',
                'ArchivalAgencyArchiveUnitIdentifier',
                'OriginatingAgencyArchiveUnitIdentifier',
                'TransferringAgencyArchiveUnitIdentifier',
                'Description',
                'CustodialHistory',
                'Type',
                'DocumentType',
                'Language',
                'DescriptionLanguage',
                'Status',
                'Version',
                'Tag',
                'Keyword',
                'Coverage',
                'OriginatingAgency',
                'SubmissionAgency',
                'AuthorizedAgent',
                'Writer',
                'Addressee',
                'Recipient',
                'Transmitter',
                'Sender',
                'Source',
                'RelatedObjectReference',
                'CreatedDate',
                'TransactedDate',
                'AcquiredDate',
                'SentDate',
                'ReceivedDate',
                'RegisteredDate',
                [
                    'StartDate' => [
                        'label' => "StartDate : ".__("Date d'ouverture / date de début."),
                        'cardinality' => '0..1',
                        'help' => __("Optionnel"),
                        'data-type' => 'datetime',
                        'validations' => $this->validations('DateTimeType'),
                        'class' => 'datepicker',
                        'placeholder' => $this->datetimePlaceholder(),
                    ],
                ],
                'EndDate',
                'Event',
                'Signature',
                'Gps'
            )
        ];
    }

    /**
     * Content.DescriptionLevel
     * @return array
     */
    protected function getDescriptionLevel(): array
    {
        return [
            'label' => 'DescriptionLevel',
            'cardinality' => '0..1',
            'info' => __(
                "Niveau de description au sens de la norme ISAD (G). Indique"
                ." si l’ArchiveUnit correspond à un fonds, à un sous-fonds, à une classe, à une série organique,"
                ." à une sous-série organique, à un dossier, à un sous-dossier ou à une pièce."
            ),
            'help' => __("Optionnel"),
            'options' => [
                'Fonds' => 'Fonds',
                'Subfonds' => 'Subfonds',
                'Class' => 'Class',
                'Collection' => 'Collection',
                'Series' => 'Series',
                'Subseries' => 'Subseries',
                'RecordGrp' => 'RecordGrp',
                'SubGrp' => 'SubGrp',
                'File' => 'File',
                'Item' => 'Item',
                'OtherLevel' => 'OtherLevel',
            ],
            'empty' => true,
            'data-placeholder' => __("-- Sélectionner un niveau de description -- "),
        ];
    }

    /**
     * Content.Title
     * @return array
     */
    protected function getTitle(): array
    {
        return [
            'label' => "Title : ".__("Intitulé de l'ArchiveUnit."),
            'cardinality' => '0..n',
            'help' => __("Optionnel"),
            'attributes' => $this->attrs('TextType'),
        ];
    }

    /**
     * Content.FilePlanPosition
     * @return array
     */
    protected function getFilePlanPosition(): array
    {
        return [
            'label' => 'FilePlanPosition',
            'cardinality' => '0..n',
            'help' => __("Optionnel"),
            'info' => __("Position de l’ArchiveUnit dans le plan de classement du service producteur."),
        ];
    }

    /**
     * Content.SystemId
     * @return array
     */
    protected function getSystemId(): array
    {
        return [
            'label' => 'SystemId',
            'cardinality' => '0..n',
            'help' => __("Optionnel"),
            'info' => __(
                "Identifiant attribué aux objets. Il est attribué par le SAE et correspond à un identifiant interne."
            ),
        ];
    }

    /**
     * Content.OriginatingSystemId
     * @return array
     */
    protected function getOriginatingSystemId(): array
    {
        return [
            'label' => 'OriginatingSystemId',
            'cardinality' => '0..n',
            'help' => __("Optionnel"),
            'info' => __("Identifiant système attribué à l’ArchiveUnit par l’application du service producteur."),
        ];
    }

    /**
     * Content.OriginatingAgencyArchiveUnitIdentifier
     * @return array
     */
    protected function getOriginatingAgencyArchiveUnitIdentifier(): array
    {
        return [
            'label' => 'OriginatingAgencyArchiveUnitIdentifier',
            'cardinality' => '0..n',
            'help' => __("Optionnel"),
            'info' => __("Identifiant métier attribué à l’ArchiveUnit par le service producteur."),
        ];
    }

    /**
     * Content.TransferringAgencyArchiveUnitIdentifier
     * @return array
     */
    protected function getTransferringAgencyArchiveUnitIdentifier(): array
    {
        return [
            'label' => 'TransferringAgencyArchiveUnitIdentifier',
            'cardinality' => '0..n',
            'help' => __("Optionnel"),
            'info' => __("Identifiant attribué à l'ArchiveUnit par le service versant."),
        ];
    }

    /**
     * Content.Description
     * @return array
     */
    protected function getDescription(): array
    {
        return [
            'label' => 'Description',
            'cardinality' => '0..n',
            'help' => __("Optionnel"),
            'info' => __(
                "Description détaillée de l’ArchiveUnit. Correspond à la"
                ." présentation du contenu au sens de la norme ISAD(G)."
            ),
            'attributes' => $this->attrs('TextType'),
        ];
    }

    /**
     * Content.CustodialHistory
     * @return array
     */
    protected function getCustodialHistory(): array
    {
        return [
            'text' => 'CustodialHistory',
            'cardinality' => '0..1',
            'li_attr' => [
                'title' => __(
                    "Énumère les changements successifs de propriété, de responsabilité"
                    ." et de conservation des ArchiveUnit avant leur entrée dans le lieu de conservation."
                    ." On peut notamment y indiquer comment s'est effectué le passage de l'application "
                    ."d'origine au fichier archivable. Correspond à l'historique de la conservation en ISAD(G)."
                ),
            ],
            'children' => $this->list(
                'CustodialHistoryItem',
                'CustodialHistoryFile'
            )
        ];
    }

    /**
     * Content.CustodialHistory.CustodialHistoryItem
     * @return array
     */
    protected function getCustodialHistoryItem(): array
    {
        return [
            'label' => 'CustodialHistoryItem',
            'cardinality' => '1..n',
            'info' => __(
                "Description détaillée de l’ArchiveUnit. Correspond à la"
                ." présentation du contenu au sens de la norme ISAD(G)."
            ),
            'attributes' => $this->attrs('TextType') + [
                'when' => [
                    'data-type' => 'date',
                    'validations' => $this->validations('DateType'),
                    'class' => 'datepicker',
                    'placeholder' => $this->datePlaceholder(),
                ]
            ],
            'type' => 'textarea',
        ];
    }

    /**
     * Content.Type
     * @return array
     */
    protected function getType(): array
    {
        return [
            'label' => 'Type',
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'info' => __(
                "Type d’information au sens de l’OAIS (information de représentation,"
                ." information de pérennisation, etc.)."
            ),
            'attributes' => $this->attrs('TextType'),
        ];
    }

    /**
     * Content.DocumentType
     * @return array
     */
    protected function getDocumentType(): array
    {
        return [
            'label' => 'DocumentType',
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'info' => __(
                "Type de document au sens diplomatique du terme (ex. compte-rendu"
                ." de réunion, note, correspondance, etc.). Ne pas confondre avec Type."
            ),
            'attributes' => $this->attrs('TextType'),
        ];
    }

    /**
     * Content.Language
     * @return array
     */
    protected function getLanguage(): array
    {
        return [
            'label' => 'Language',
            'cardinality' => '0..n',
            'help' => __("Optionnel"),
            'options' => $this->optionsLang(),
            'data-placeholder' => __("-- Sélectionner un language --"),
            'pattern' => '[a-zA-Z]{1,8}(-[a-zA-Z0-9]{1,8})*',
            'validations' => [
                'islang' => $this->validationIsLang(),
            ],
            'info' => __("Langue du contenu des objets-données."),
        ];
    }

    /**
     * Content.DescriptionLanguage
     * @return array
     */
    protected function getDescriptionLanguage(): array
    {
        return [
            'label' => 'DescriptionLanguage',
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'pattern' => '[a-zA-Z]{1,8}(-[a-zA-Z0-9]{1,8})*',
            'options' => $this->optionsLang(),
            'data-placeholder' => __("-- Sélectionner un language --"),
            'validations' => [
                'islang' => $this->validationIsLang(),
            ],
            'info' => __("Langue utilisée pour les informations de représentation et de pérennisation."),
        ];
    }

    /**
     * Content.Status
     * @return array
     */
    protected function getStatus(): array
    {
        return [
            'label' => 'Status',
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'info' => __(
                "Etat de l'objet-données (par rapport avec son cycle de vie). "
                ."Permet par exemple d'indiquer si la signature du fichier a été"
                ." vérifiée avant le transfert aux archives."
            ),
            'attributes' => $this->attrs('TextType'),
        ];
    }

    /**
     * Content.Version
     * @return array
     */
    protected function getVersion(): array
    {
        return [
            'label' => 'Version',
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'info' => __("Permet d'indiquer quelle est la version du document."),
        ];
    }

    /**
     * Content.Tag
     * @return array
     */
    protected function getTag(): array
    {
        return [
            'label' => 'Tag',
            'cardinality' => '0..n',
            'help' => __("Optionnel"),
            'info' => __(
                "Mots-clés ou liste de mots-clés génériques. En ce qui concerne"
                ." l'indexation, on pourra utiliser Tag ou Keyword en fonction de ce que l'on souhaite décrire."
            ),
        ];
    }

    /**
     * Content.Keyword
     * @return array
     */
    protected function getKeyword(): array
    {
        return [
            'text' => 'Keyword',
            'cardinality' => '0..n',
            'text_appendChildValue' => '{{KeywordContent}}',
            'li_attr' => [
                'title' => __(
                    "Mots-clef avec contexte inspiré du SEDA 1.0. En ce qui concerne"
                    ." l'indexation, on pourra utiliser Tag ou Keyword en fonction de ce que l'on souhaite décrire."
                ),
            ],
            'children' => $this->list(
                [
                    'keyword' => [
                        'label' => __("Rechercher un mot clé"),
                        'options' => [],
                        'empty' => true,
                        'data-placeholder' => __("-- Sélectionner un mot clé -- "),
                        'chosen' => false,
                        'afterload' => 'ajaxSearchKeyword',
                        'help' => '<button type="button" onclick="loadKeywordData(this)" class="btn btn-success">'
                            .'<i class="fa fa-check-square-o" aria-hidden="true"></i> '
                            .__("Sélectionner")
                            .'</button>'
                    ],
                ],
                'KeywordContent',
                'KeywordReference',
                'KeywordType'
            )
        ];
    }

    /**
     * Content.Keyword.KeywordContent
     * @return array
     */
    protected function getKeywordContent(): array
    {
        return [
            'label' => 'KeywordContent',
            'cardinality' => '1..1',
            'info' => __("Valeur du mot-clé. A utiliser avec Keyword."),
            'attributes' => $this->attrs('TextType'),
            'class' => 'keyword-name',
        ];
    }

    /**
     * Content.Keyword.KeywordReference
     * @return array
     */
    protected function getKeywordReference(): array
    {
        return [
            'label' => 'KeywordReference',
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'info' => __(
                "Identifiant du mot clé dans un référentiel donné. Par exemple,"
                ." pour un lieu, il pourrait s'agir de son code officiel géographique selon l'INSEE."
            ),
            'attributes' => Hash::merge(
                $this->attrs('IdentifierType'),
                [
                    'schemeID' => ['class' => 'keyword-list-id'],
                    'schemeName' => ['class' => 'keyword-list-name'],
                    'schemeVersionID' => ['class' => 'keyword-list-version'],
                ]
            ),
            'class' => 'keyword-code',
        ];
    }

    /**
     * Content.Keyword.KeywordType
     * @return array
     */
    protected function getKeywordType(): array
    {
        return [
            'label' => 'KeywordType : '.__("Type de mot clé."),
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'options' => self::getXsdOptions('keywordtype'),
            'empty' => true,
            'data-placeholder' => __("-- Sélectionner un type -- "),
        ];
    }

    /**
     * Content.Coverage
     * @return array
     */
    protected function getCoverage(): array
    {
        return [
            'text' => 'Coverage',
            'cardinality' => '0..1',
            'li_attr' => [
                'title' => __("Couverture spatiale, temporelle ou juridictionnelle de l’ArchiveUnit"),
            ],
            'children' => $this->list(
                'Spatial',
                'Temporal',
                'Juridictional'
            )
        ];
    }

    /**
     * Content.Coverage.Spatial
     * @return array
     */
    protected function getSpatial(): array
    {
        return [
            'label' => 'Spatial',
            'cardinality' => '0..n',
            'help' => __("Optionnel"),
            'info' => __("Couverture spatiale ou couverture géographique."),
            'attributes' => $this->attrs('TextType'),
        ];
    }

    /**
     * Content.Coverage.Temporal
     * @return array
     */
    protected function getTemporal(): array
    {
        return [
            'label' => 'Temporal',
            'cardinality' => '0..n',
            'help' => __("Optionnel"),
            'info' => __("Couverture temporelle."),
            'attributes' => $this->attrs('TextType'),
        ];
    }

    /**
     * Content.Coverage.Juridictional
     * @return array
     */
    protected function getJuridictional(): array
    {
        return [
            'label' => 'Juridictional',
            'cardinality' => '0..n',
            'help' => __("Optionnel"),
            'info' => __("Juridiction administrative ou ressort administratif."),
            'attributes' => $this->attrs('TextType'),
        ];
    }

    /**
     * Content.OriginatingAgency
     * @return array
     */
    protected function getOriginatingAgency(): array
    {
        return [
            'text' => 'OriginatingAgency',
            'text_appendChildValue' => '{{Identifier}}',
            'cardinality' => '0..1',
            'li_attr' => [
                'title' => __(
                    "Service producteur. \"Personne physique ou morale, publique ou privée,"
                    ." qui a produit, reçu et conservé des archives  dans l'exercice de son activité\","
                    ." Dictionnaire de terminologie archivistique, direction des archives de France, 2002."
                ),
            ],
            'children' => $this->typeOrganizationType('OriginatingAgency'),
        ];
    }

    /**
     * Content.OriginatingAgency.Identifier
     * @return array
     */
    protected function getIdentifier(): array
    {
        return [
            'label' => 'Identifier : '.__("Identifiant de l'organisation."),
            'cardinality' => '1..1',
            'attributes' => $this->attrs('IdentifierType'),
        ];
    }

    /**
     * Content.OriginatingAgency.OrganizationDescriptiveMetadata
     * @return array
     */
    protected function getOrganizationDescriptiveMetadata(): array
    {
        return [
            'text' => 'OrganizationDescriptiveMetadata',
            'cardinality' => '0..1',
            'disabled' => true,
            'children' => [],
        ];
    }

    /**
     * Content.SubmissionAgency
     * @return array
     */
    protected function getSubmissionAgency(): array
    {
        return [
            'text' => 'SubmissionAgency',
            'cardinality' => '0..1',
            'li_attr' => [
                'title' => __("Service versant responsable du transfert des données."),
            ],
            'children' => $this->typeOrganizationType('SubmissionAgency'),
        ];
    }

    /**
     * Content.AuthorizedAgent
     * @return array
     */
    protected function getAuthorizedAgent(): array
    {
        return [
            'text' => 'AuthorizedAgent',
            'cardinality' => '0..n',
            'li_attr' => [
                'title' => __("Titulaire des droits de propriété intellectuelle."),
            ],
            'children' => $this->refAgentType()
        ];
    }

    /**
     * ref AgentType
     * @return array
     */
    protected function refAgentType(): array
    {
        return $this->list(
            $this->refPersonOrEntityGroup(),
            $this->refBusinessGroup()
        );
    }

    /**
     * ref PersonOrEntityGroup
     * @return array
     */
    protected function refPersonOrEntityGroup(): array
    {
        return $this->list(
            [
                'choice' => [
                    'type' => 'hidden',
                    'afterload' => 'elementsChoice',
                    'data-choice' => json_encode(
                        [
                            [
                                'FirstName',
                                'BirthName',
                                'FullName',
                                'GivenName',
                                'Gender',
                                'BirthDate',
                                'BirthPlace',
                                'DeathDate',
                                'DeathPlace',
                                'Nationality',
                            ],
                            [
                                'Corpname',
                            ]
                        ]
                    )
                ],
            ],
            'FirstName',
            'BirthName',
            'FullName',
            'GivenName',
            'Gender',
            'BirthDate',
            'BirthPlace',
            'DeathDate',
            'DeathPlace',
            'Nationality',
            'Corpname'
        );
    }

    /**
     * ref BusinessGroup
     * @return array
     */
    protected function refBusinessGroup(): array
    {
        return $this->list(
            'Function',
            'Activity',
            'Position',
            'Role',
            'Mandate'
        );
    }

    /**
     * Content.Writer
     * @return array
     */
    protected function getWriter(): array
    {
        return [
            'text' => 'Writer',
            'cardinality' => '0..n',
            'li_attr' => [
                'title' => __("Rédacteur Rédacteur de l’ArchiveUnit."),
            ],
            'children' => $this->refAgentType()
        ];
    }

    /**
     * Content.Addressee
     * @return array
     */
    protected function getAddressee(): array
    {
        return [
            'text' => 'Addressee',
            'cardinality' => '0..n',
            'li_attr' => [
                'title' => __(
                    "Destinataire pour action. Utilisé pour indiquer le nom"
                    ." du destinatire par exemple dans un courrier électronique."
                ),
            ],
            'children' => $this->refAgentType()
        ];
    }

    /**
     * Content.Recipient
     * @return array
     */
    protected function getRecipient(): array
    {
        return [
            'text' => 'Recipient',
            'cardinality' => '0..n',
            'li_attr' => [
                'title' => __(
                    "Destinataire pour information. Utilisé pour indiquer le nom"
                    ." du destinatire en copie, pour information, par exemple dans un courrier électronique."
                ),
            ],
            'children' => $this->refAgentType()
        ];
    }

    /**
     * Content.Transmitter
     * @return array
     */
    protected function getTransmitter(): array
    {
        return [
            'text' => 'Transmitter',
            'cardinality' => '0..n',
            'li_attr' => [
                'title' => __("Emetteur du message."),
            ],
            'children' => $this->refAgentType()
        ];
    }

    /**
     * Content.Sender
     * @return array
     */
    protected function getSender(): array
    {
        return [
            'text' => 'Sender',
            'cardinality' => '0..n',
            'li_attr' => [
                'title' => __("Expéditeur du message."),
            ],
            'children' => $this->refAgentType()
        ];
    }

    /**
     * PersonOrEntityGroup.FirstName
     * @return array
     */
    protected function getFirstName(): array
    {
        return [
            'label' => 'FirstName : '.__("Prénom d'une personne."),
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
        ];
    }

    /**
     * PersonOrEntityGroup.BirthName
     * @return array
     */
    protected function getBirthName(): array
    {
        return [
            'label' => 'BirthName : '.__("Nom de naissance d'une personne."),
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
        ];
    }

    /**
     * PersonOrEntityGroup.FullName
     * @return array
     */
    protected function getFullName(): array
    {
        return [
            'label' => 'FullName : '.__("Nom complet d'une personne."),
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
        ];
    }

    /**
     * PersonOrEntityGroup.GivenName
     * @return array
     */
    protected function getGivenName(): array
    {
        return [
            'label' => 'GivenName : '.__("Nom d'usage d'une personne."),
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
        ];
    }

    /**
     * PersonOrEntityGroup.Gender
     * @return array
     */
    protected function getGender(): array
    {
        return [
            'label' => 'Gender : '.__("Sexe de la personne."),
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
        ];
    }

    /**
     * PersonOrEntityGroup.BirthDate
     * @return array
     */
    protected function getBirthDate(): array
    {
        return [
            'label' => 'BirthDate : '.__("Date de naissance de la personne."),
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'data-type' => 'date',
            'validations' => $this->validations('DateType'),
            'class' => 'datepicker',
            'placeholder' => $this->datePlaceholder(),
        ];
    }

    /**
     * PersonOrEntityGroup.BirthPlace
     * @return array
     */
    protected function getBirthPlace(): array
    {
        return [
            'text' => 'BirthPlace',
            'cardinality' => '0..1',
            'li_attr' => [
                'title' => __("Lieu de naissance de la personne."),
            ],
            'children' => $this->refLocationGroup()
        ];
    }

    /**
     * PersonOrEntityGroup.DeathDate
     * @return array
     */
    protected function getDeathDate(): array
    {
        return [
            'label' => 'DeathDate : '.__("Date de décès d'une personne."),
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'data-type' => 'date',
            'validations' => $this->validations('DateType'),
            'class' => 'datepicker',
            'placeholder' => $this->datePlaceholder(),
        ];
    }

    /**
     * PersonOrEntityGroup.DeathPlace
     * @return array
     */
    protected function getDeathPlace(): array
    {
        return [
            'text' => 'DeathPlace',
            'cardinality' => '0..1',
            'li_attr' => [
                'title' => __("Lieu de décès d'une personne."),
            ],
            'children' => $this->refLocationGroup()
        ];
    }

    /**
     * PersonOrEntityGroup.Nationality
     * @return array
     */
    protected function getNationality(): array
    {
        return [
            'label' => 'Nationality : '.__("Nationalité d'une personne."),
            'cardinality' => '0..n',
            'help' => __("Optionnel"),
        ];
    }

    /**
     * PersonOrEntityGroup.Corpname
     * @return array
     */
    protected function getCorpname(): array
    {
        return [
            'label' => 'Corpname : '.__("Nom d'une entité."),
            'cardinality' => '0..1',
            'true_cardinality' => '1..1',
            'help' => __("Optionnel"),
        ];
    }

    /**
     * BusinessGroup.Function
     * @return array
     */
    protected function getFunction(): array
    {
        return [
            'label' => 'Function',
            'cardinality' => '0..n',
            'help' => __("Optionnel"),
            'attributes' => $this->attrs('TextType'),
        ];
    }

    /**
     * BusinessGroup.Activity
     * @return array
     */
    protected function getActivity(): array
    {
        return [
            'label' => 'Activity',
            'cardinality' => '0..n',
            'help' => __("Optionnel"),
            'attributes' => $this->attrs('TextType'),
        ];
    }

    /**
     * BusinessGroup.Position
     * @return array
     */
    protected function getPosition(): array
    {
        return [
            'label' => 'Position',
            'cardinality' => '0..n',
            'help' => __("Optionnel"),
            'info' => __("Intitulé du poste de travail occupé par la personne."),
            'attributes' => $this->attrs('TextType'),
        ];
    }

    /**
     * BusinessGroup.Role
     * @return array
     */
    protected function getRole(): array
    {
        return [
            'label' => 'Role',
            'cardinality' => '0..n',
            'help' => __("Optionnel"),
            'info' => __(
                "Droits avec lesquels un utilisateur a réalisé une opération, notamment dans une application."
            ),
            'attributes' => $this->attrs('TextType'),
        ];
    }

    /**
     * BusinessGroup.Mandate
     * @return array
     */
    protected function getMandate(): array
    {
        return [
            'label' => 'Mandate',
            'cardinality' => '0..n',
            'help' => __("Optionnel"),
            'info' => __("Définit la propriété intellectuelle et artistique."),
            'attributes' => $this->attrs('TextType'),
        ];
    }

    /**
     * ref LocationGroup
     * @return array
     */
    protected function refLocationGroup(): array
    {
        return $this->list(
            'Geogname',
            'Address',
            'PostalCode',
            'City',
            'Region',
            'Country'
        );
    }

    /**
     * LocationGroup.Geogname
     * @return array
     */
    protected function getGeogname(): array
    {
        return [
            'label' => 'Geogname',
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
        ];
    }

    /**
     * LocationGroup.Address
     * @return array
     */
    protected function getAddress(): array
    {
        return [
            'label' => 'Address',
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
        ];
    }

    /**
     * LocationGroup.PostalCode
     * @return array
     */
    protected function getPostalCode(): array
    {
        return [
            'label' => 'PostalCode',
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
        ];
    }

    /**
     * LocationGroup.City
     * @return array
     */
    protected function getCity(): array
    {
        return [
            'label' => 'City',
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
        ];
    }

    /**
     * LocationGroup.Region
     * @return array
     */
    protected function getRegion(): array
    {
        return [
            'label' => 'Region',
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
        ];
    }

    /**
     * LocationGroup.Country
     * @return array
     */
    protected function getCountry(): array
    {
        return [
            'label' => 'Country',
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
        ];
    }

    /**
     * Content.Source
     * @return array
     */
    protected function getSource(): array
    {
        return [
            'label' => 'Source',
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'info' => __("En cas de substitution numérique, permet de faire référence au papier."),
        ];
    }

    /**
     * Content.RelatedObjectReference
     * @return array
     */
    protected function getRelatedObjectReference(): array
    {
        return [
            'text' => 'RelatedObjectReference',
            'cardinality' => '0..1',
            'li_attr' => [
                'title' => __("Référence à un objet faisant ou ne faisant pas partie du présent paquet d'information."),
            ],
            'children' => $this->list(
                'IsVersionOf',
                'Replaces',
                'Requires',
                'IsPartOf',
                'References'
            )
        ];
    }

    /**
     * ref DataObjectOrArchiveUnitReferenceType
     * @return array
     */
    protected function refDataObjectOrArchiveUnitReferenceType(): array
    {
        return $this->list(
            'ArchiveUnitRefId',
            'DataObjectReference',
            'RepositoryArchiveUnitPID',
            'RepositoryObjectPID',
            'ExternalReference'
        );
    }

    /**
     * Content.RelatedObjectReference.IsVersionOf
     * @return array
     */
    protected function getIsVersionOf(): array
    {
        return [
            'text' => 'IsVersionOf',
            'cardinality' => '0..n',
            'li_attr' => [
                'title' => __(
                    "Est une version de. Edition, adaptation, traduction."
                    ." Cette relation permet d'indiquer les modifications dans le contenu."
                ),
            ],
            'children' => $this->refDataObjectOrArchiveUnitReferenceType()
        ];
    }

    /**
     * Content.RelatedObjectReference.Replaces
     * @return array
     */
    protected function getReplaces(): array
    {
        return [
            'text' => 'Replaces',
            'cardinality' => '0..n',
            'li_attr' => [
                'title' => __(
                    "Remplace. Cette relation permet d'indiquer les objets remplacés"
                    ." par le niveau courant de description."
                ),
            ],
            'children' => $this->refDataObjectOrArchiveUnitReferenceType()
        ];
    }

    /**
     * Content.RelatedObjectReference.Requires
     * @return array
     */
    protected function getRequires(): array
    {
        return [
            'text' => 'Requires',
            'cardinality' => '0..n',
            'li_attr' => [
                'title' => __(
                    "Requiert. Cette relation permet d'indiquer les objets nécessaire"
                    ." à la compréhension du niveau courant de description."
                ),
            ],
            'children' => $this->refDataObjectOrArchiveUnitReferenceType()
        ];
    }

    /**
     * Content.RelatedObjectReference.IsPartOf
     * @return array
     */
    protected function getIsPartOf(): array
    {
        return [
            'text' => 'IsPartOf',
            'cardinality' => '0..n',
            'li_attr' => [
                'title' => __(
                    "Est une partie de. Cette relation permet d'indique qu'un objet est une partie d'un autre."
                ),
            ],
            'children' => $this->refDataObjectOrArchiveUnitReferenceType()
        ];
    }

    /**
     * Content.RelatedObjectReference.References
     * @return array
     */
    protected function getReferences(): array
    {
        return [
            'text' => 'References',
            'cardinality' => '0..n',
            'li_attr' => [
                'title' => __("Référence. Cette relation permet d'indiquer qu'un objet en référence un autre."),
            ],
            'children' => $this->refDataObjectOrArchiveUnitReferenceType(),
            'complex' => [
                [
                    'choice' => [
                        'ArchiveUnitRefId',
                        'DataObjectReference',
                        'RepositoryArchiveUnitPID',
                        'RepositoryObjectPID',
                        'ExternalReference'
                    ]
                ],
            ],
            'choice' => [
                'ArchiveUnitRefId',
                'DataObjectReference',
                'RepositoryArchiveUnitPID',
                'RepositoryObjectPID',
                'ExternalReference'
            ],
        ];
    }

    /**
     * Content.CreatedDate
     * @return array
     */
    protected function getCreatedDate(): array
    {
        return [
            'label' => "CreatedDate : ".__("Date de création."),
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'data-type' => 'datetime',
            'validations' => $this->validations('DateTimeType'),
            'class' => 'datepicker',
            'placeholder' => $this->datetimePlaceholder(),
        ];
    }

    /**
     * Content.TransactedDate
     * @return array
     */
    protected function getTransactedDate(): array
    {
        return [
            'label' => "TransactedDate : ".__("Date de la transaction."),
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'data-type' => 'datetime',
            'validations' => $this->validations('DateTimeType'),
            'class' => 'datepicker',
            'placeholder' => $this->datetimePlaceholder(),
        ];
    }

    /**
     * Content.AcquiredDate
     * @return array
     */
    protected function getAcquiredDate(): array
    {
        return [
            'label' => "AcquiredDate : ".__("Date de numérisation."),
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'data-type' => 'datetime',
            'validations' => $this->validations('DateTimeType'),
            'class' => 'datepicker',
            'placeholder' => $this->datetimePlaceholder(),
        ];
    }

    /**
     * Content.SentDate
     * @return array
     */
    protected function getSentDate(): array
    {
        return [
            'label' => "SentDate : ".__("Date d'envoi."),
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'data-type' => 'datetime',
            'validations' => $this->validations('DateTimeType'),
            'class' => 'datepicker',
            'placeholder' => $this->datetimePlaceholder(),
        ];
    }

    /**
     * Content.ReceivedDate
     * @return array
     */
    protected function getReceivedDate(): array
    {
        return [
            'label' => "ReceivedDate : ".__("Date de réception."),
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'data-type' => 'datetime',
            'validations' => $this->validations('DateTimeType'),
            'class' => 'datepicker',
            'placeholder' => $this->datetimePlaceholder(),
        ];
    }

    /**
     * Content.RegisteredDate
     * @return array
     */
    protected function getRegisteredDate(): array
    {
        return [
            'label' => "RegisteredDate : ".__("Date d'enregistrement."),
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'data-type' => 'datetime',
            'validations' => $this->validations('DateTimeType'),
            'class' => 'datepicker',
            'placeholder' => $this->datetimePlaceholder(),
        ];
    }

    /**
     * Content.EndDate
     * @return array
     */
    protected function getEndDate(): array
    {
        return [
            'label' => "EndDate : ".__("Date de fermeture / Date de fin."),
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'data-type' => 'datetime',
            'validations' => $this->validations('DateTimeType'),
            'class' => 'datepicker',
            'placeholder' => $this->datetimePlaceholder(),
        ];
    }

    /**
     * Content.Event
     * @return array
     */
    protected function getEvent(): array
    {
        return [
            'text' => 'Event',
            'cardinality' => '0..n',
            'li_attr' => [
                'title' => __(
                    "Informations décrivant un événement survenu au cours"
                    ." d’une procédure (ex. publication d’un marché, notification"
                    ." d’un marché, recueil d’un avis administratif, etc.)."
                ),
            ],
            'children' => $this->refEventType()
        ];
    }

    /**
     * ref EventType
     * @return array
     */
    protected function refEventType(): array
    {
        return $this->list(
            'EventIdentifier',
            'EventTypeCode',
            'EventType',
            'EventDateTime',
            'EventDetail',
            'Outcome',
            'OutcomeDetail',
            'OutcomeDetailMessage',
            'EventDetailData',
            'EventAbstract'
        );
    }

    /**
     * EventType.EventIdentifier
     * @return array
     */
    protected function getEventIdentifier(): array
    {
        return [
            'label' => 'EventIdentifier : '.__("Identifiant de l'événement."),
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
        ];
    }

    /**
     * EventType.EventTypeCode
     * @return array
     */
    protected function getEventTypeCode(): array
    {
        return [
            'label' => 'EventTypeCode : '.__("Code du type d'événement."),
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
        ];
    }

    /**
     * EventType.EventType
     * @return array
     */
    protected function getEventType(): array
    {
        return [
            'label' => 'EventType : '.__("Type d'événement."),
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
        ];
    }

    /**
     * EventType.EventDateTime
     * @return array
     */
    protected function getEventDateTime(): array
    {
        return [
            'label' => 'EventDateTime : '.__("Date et heure de l'événement."),
            'cardinality' => '1..1',
            'data-type' => 'datetime',
            'validations' => $this->validations('DateTimeType'),
            'class' => 'datepicker',
            'placeholder' => $this->datetimePlaceholder(),
        ];
    }

    /**
     * EventType.EventDetail
     * @return array
     */
    protected function getEventDetail(): array
    {
        return [
            'label' => 'EventDetail : '.__("Détail sur l'événement."),
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'attributes' => $this->attrs('TextType'),
        ];
    }

    /**
     * EventType.Outcome
     * @return array
     */
    protected function getOutcome(): array
    {
        return [
            'label' => 'Outcome : '.__("Résultat du traitement."),
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
        ];
    }

    /**
     * EventType.OutcomeDetail
     * @return array
     */
    protected function getOutcomeDetail(): array
    {
        return [
            'label' => 'OutcomeDetail : '.__("Détail sur le résultat du traitement."),
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
        ];
    }

    /**
     * EventType.OutcomeDetailMessage
     * @return array
     */
    protected function getOutcomeDetailMessage(): array
    {
        return [
            'label' => 'OutcomeDetailMessage : '.__("Message détaillé sur le résultat du traitement."),
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
        ];
    }

    /**
     * EventType.EventDetailData
     * @return array
     */
    protected function getEventDetailData(): array
    {
        return [
            'label' => 'EventDetailData : '.__("Message technique détaillant l'erreur."),
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
        ];
    }

    /**
     * Content.Signer
     * @return array
     */
    protected function getSigner(): array
    {
        return [
            'text' => 'Signer',
            'cardinality' => '0..n',
            'true_cardinality' => '1..n',
            'li_attr' => [
                'title' => __("Signataire(s) de la transaction ou de l'objet."),
            ],
            'children' => $this->list(
                $this->refPersonOrEntityGroup(),
                'SigningTime',
                $this->refBusinessGroup()
            )
        ];
    }

    /**
     * Content.Signer.SigningTime
     * @return array
     */
    protected function getSigningTime(): array
    {
        return [
            'label' => 'SigningTime : '.__("Date de signature."),
            'cardinality' => '1..1',
            'data-type' => 'datetime',
            'validations' => $this->validations('DateTimeType'),
            'class' => 'datepicker',
            'placeholder' => $this->datetimePlaceholder(),
        ];
    }

    /**
     * Content.Validator
     * @return array
     */
    protected function getValidator(): array
    {
        return [
            'text' => 'Validator',
            'cardinality' => '1..1',
            'li_attr' => [
                'title' => __("Validateur de la signature."),
            ],
            'children' => $this->list(
                $this->refPersonOrEntityGroup(),
                'ValidationTime',
                $this->refBusinessGroup()
            )
        ];
    }

    /**
     * Content.Validator.ValidationTime
     * @return array
     */
    protected function getValidationTime(): array
    {
        return [
            'label' => 'ValidationTime : '.__("Date de la validation de la signature."),
            'cardinality' => '1..1',
            'data-type' => 'datetime',
            'validations' => $this->validations('DateTimeType'),
            'class' => 'datepicker',
            'placeholder' => $this->datetimePlaceholder(),
        ];
    }

    /**
     * Content.Masterdata
     * @return array
     */
    protected function getMasterdata(): array
    {
        return [
            'label' => 'Masterdata',
            'cardinality' => '0..1',
            'info' => __(
                "Référentiel des personnes et des organisations au moment"
                ." de la vérification de la signature et de sa validation."
            ),
            'help' => __("Optionnel"),
            'attributes' => $this->attrs('CodeType'),
        ];
    }

    /**
     * Content.ReferencedObject
     * @return array
     */
    protected function getReferencedObject(): array
    {
        return [
            'text' => 'ReferencedObject',
            'cardinality' => '0..1',
            'li_attr' => [
                'title' => __("Référence à l'objet signé."),
            ],
            'children' => $this->list(
                'SignedObjectId',
                'SignedObjectDigest'
            )
        ];
    }

    /**
     * Content.ReferencedObject.SignedObjectId
     * @return array
     */
    protected function getSignedObjectId(): array
    {
        return [
            'label' => 'SignedObjectId : '.__("Identifiant de l'objet-données signé."),
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
        ];
    }

    /**
     * Content.ReferencedObject.SignedObjectDigest
     * @return array
     */
    protected function getSignedObjectDigest(): array
    {
        return [
            'label' => 'SignedObjectDigest',
            'cardinality' => '0..1',
            'info' => __(
                "Empreinte obligatoire jusqu'au processus de versement pour assurer"
                ." la portabilité de la valeur probante. Le SAE peut ne pas la conserver"
                ." si l'on considère que l'identifiant de l'objet correspondant suffit. "
                ."Ce procédé permet de résister au temps lorsque les informations binaires"
                ." du paquet seront converties au gré des opérations de préservation de la"
                ." lisibilité des formats. Au cours de ces opérations, l'identifiant ne"
                ." changera pas, contrairement au format dufichier et donc à son empreinte."
            ),
            'help' => __("Optionnel"),
            'attributes' => [
                'algorithm' => [
                    'help' => __("Algorithme de hachage spécifié dans DigestAlgorithmCodeList.")
                ]
            ]
        ];
    }

    /**
     * Content.Gps
     * @return array
     */
    protected function getGps(): array
    {
        return [
            'text' => 'Gps',
            'cardinality' => '0..1',
            'li_attr' => [
                'title' => __(
                    "Coordonnées gps complétées ou vérifiées par un utilisateur."
                    ." Fait référence à des coordonnées traitées par un utilisateur et non à des coordonnées captées."
                ),
            ],
            'children' => $this->list(
                'GpsVersionID',
                'GpsAltitude',
                'GpsAltitudeRef',
                'GpsLatitude',
                'GpsLatitudeRef',
                'GpsLongitude',
                'GpsLongitudeRef',
                'GpsDateStamp'
            )
        ];
    }

    /**
     * Content.Gps.GpsVersionID
     * @return array
     */
    protected function getGpsVersionID(): array
    {
        return [
            'label' => 'GpsVersionID : '.__("Identifiant de la version du GPS."),
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
        ];
    }

    /**
     * Content.Gps.GpsAltitude
     * @return array
     */
    protected function getGpsAltitude(): array
    {
        return [
            'label' => 'GpsAltitude',
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'info' => __(
                "Indique l'altitude basée sur la référence dans GPSAltitudeRef. L'altitude est exprimée en mètres."
            ),
        ];
    }

    /**
     * Content.Gps.GpsAltitudeRef
     * @return array
     */
    protected function getGpsAltitudeRef(): array
    {
        return [
            'label' => 'GpsAltitudeRef',
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'info' => __("Indique l'altitude utilisée comme altitude de référence.").PHP_EOL
                .__("Si l'altitude est au dessus du niveau de la mer, la valeur 0 est normalement donnée.").PHP_EOL
                .__("Si l'altitude est au-dessous du niveau de la mer, la veleur 1 est normalement donnée.")
        ];
    }

    /**
     * Content.Gps.GpsLatitude
     * @return array
     */
    protected function getGpsLatitude(): array
    {
        return [
            'label' => 'GpsLatitude',
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'info' => __(
                "La latitude peut être exprimée de deux manières différentes"
                ." : 1)degrés, décimaux ou 2)degrés, minutes et secondes."
            ).PHP_EOL
                .__(
                    "1)Si la latitude est exprimée en degrés, décimaux,"
                    ." le format type est dd, dd. Par ex: \"45.3130339\"."
                ).PHP_EOL
                .__(
                    "2)Si la latitude est exprimée en degrés, minutes et"
                    ." secondes, le format type est dd, mm, ss. Par ex: \"45 18 46.922\"."
                )
        ];
    }

    /**
     * Content.Gps.GpsLatitudeRef
     * @return array
     */
    protected function getGpsLatitudeRef(): array
    {
        return [
            'label' => 'GpsLatitudeRef',
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'info' => __(
                "Indique si la latitude est nord ou sud. La valeur 'N' "
                ."indique la latitude nord, et 'S' indique la latitude sud.."
            )
        ];
    }

    /**
     * Content.Gps.GpsLongitude
     * @return array
     */
    protected function getGpsLongitude(): array
    {
        return [
            'label' => 'GpsLongitude',
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'info' => __(
                "La longitude peut être exprimée de deux manières différentes"
                ." : 1)degrés, décimaux ou 2)degrés, minutes et secondes."
            )
            .PHP_EOL
            .__(
                "1)Si la longitude est exprimée en degrés, décimaux,"
                ." le format type est dd, dd. Par ex: \"5.392285833333334\"."
            )
            .PHP_EOL
            .__(
                "2)Si la longitude est exprimée en degrés, minutes et"
                ." secondes, le format type est dd, mm, ss. Par ex: \"5 23 32.229\"."
            )
        ];
    }

    /**
     * Content.Gps.GpsLongitudeRef
     * @return array
     */
    protected function getGpsLongitudeRef(): array
    {
        return [
            'label' => 'GpsLongitudeRef',
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'info' => __(
                "Indique si la longitude est est ou ouest. La valeur 'E' "
                ."indique la longitude est, et 'W' indique la longitude Ouest."
            )
        ];
    }

    /**
     * Content.Gps.GpsDateStamp
     * @return array
     */
    protected function getGpsDateStamp(): array
    {
        return [
            'label' => 'GpsDateStamp : '.__("Heure et Date de la position GPS."),
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
        ];
    }

    /**
     * ArchiveTransfer.DataObjectPackage.DescriptiveMetadata.ArchiveUnit.DataObjectReference
     * @return array
     */
    protected function getDataObjectReference(): array
    {
        return [
            'text' => 'DataObjectReference',
            'cardinality' => '0..n',
            'true_cardinality' => '1..1',
            'li_attr' => [
                'title' => __(
                    "Référence à un objet-données ou à un groupe d'objets-données interne(s)."
                ),
            ],
            'children' => $this->list(
                $this->refDataObjectRefType()
            )
        ];
    }

    /**
     * ArchiveTransfer.DataObjectPackage.DescriptiveMetadata
     * .ArchiveUnit.Content.RelatedObjectReference.RepositoryArchiveUnitPID
     * @return array
     */
    protected function getRepositoryArchiveUnitPID(): array
    {
        return [
            'label' => 'RepositoryArchiveUnitPID',
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'info' => __(
                "Référence à un ArchiveUnit déjà conservé dans un système d'archivage."
            ),
        ];
    }

    /**
     * ArchiveTransfer.DataObjectPackage.DescriptiveMetadata
     * .ArchiveUnit.Content.RelatedObjectReference.RepositoryObjectPID
     * @return array
     */
    protected function getRepositoryObjectPID(): array
    {
        return [
            'label' => 'RepositoryObjectPID',
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'info' => __(
                "Référence à un un objet-données ou à un groupe d'objets-données"
                ." déjà conservé(s) dans un système d'archivage."
            ),
        ];
    }

    /**
     * ArchiveTransfer.DataObjectPackage.DescriptiveMetadata
     * .ArchiveUnit.Content.RelatedObjectReference.ExternalReference
     * @return array
     */
    protected function getExternalReference(): array
    {
        return [
            'label' => 'ExternalReference : ' . __("Référence externe."),
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
        ];
    }

    /**
     * ref DataObjectRefType
     * @return array
     */
    protected function refDataObjectRefType(): array
    {
        return $this->list(
            [
                'reference' => [
                    'label' => __("Rechercher un binary"),
                    'options' => [],
                    'empty' => true,
                    'data-placeholder' => __("-- Sélectionner un binary -- "),
                    'chosen' => false,
                    'afterload' => 'ajaxSearchBinary',
                    'help' => '<button type="button" onclick="loadBinaryData(this)" class="btn btn-success">'
                        .'<i class="fa fa-check-square-o" aria-hidden="true"></i> '
                        .__("Sélectionner")
                        .'</button>'
                ],
            ],
            'DataObjectReferenceId',
            [
                'reference_group' => [
                    'label' => __("Rechercher un groupe"),
                    'options' => [],
                    'empty' => true,
                    'data-placeholder' => __("-- Sélectionner un groupe -- "),
                    'chosen' => false,
                    'afterload' => 'ajaxSearchGroup',
                    'help' => '<button type="button" onclick="loadGroupData(this)" class="btn btn-success">'
                        .'<i class="fa fa-check-square-o" aria-hidden="true"></i> '
                        .__("Sélectionner")
                        .'</button>'
                ],
            ],
            'DataObjectGroupReferenceId'
        );
    }

    /**
     * DataObjectRefType.DataObjectReferenceId
     * @return array
     */
    protected function getDataObjectReferenceId(): array
    {
        return [
            'label' => 'DataObjectReferenceId',
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'readonly' => true,
            'info' => __(
                "Référence à un objet-données listé dans les métadonnées de transport."
            ),
        ];
    }

    /**
     * DataObjectRefType.DataObjectGroupReferenceId
     * @return array
     */
    protected function getDataObjectGroupReferenceId(): array
    {
        return [
            'label' => 'DataObjectGroupReferenceId',
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'readonly' => true,
            'info' => __(
                "Référence à un groupe d'objets-données listé dans les métadonnées de transport."
            ),
        ];
    }

    /**
     * ArchiveTransfer.DataObjectPackage.ManagementMetadata
     * @return array
     */
    protected function getManagementMetadata(): array
    {
        return [
            'text' => 'ManagementMetadata',
            'cardinality' => '0..1',
            'li_attr' => [
                'title' => __(
                    "Bloc des métadonnées de gestion par défaut des objets-données."
                ),
            ],
            'children' => $this->list(
                'ArchivalProfile',
                'ServiceLevel',
                'AcquisitionInformation',
                'LegalStatus',
                'OriginatingAgencyIdentifier',
                'SubmissionAgencyIdentifier',
                $this->refManagementType()
            )
        ];
    }

    /**
     * ArchiveTransfer.DataObjectPackage.ManagementMetadata.ArchivalProfile
     * @return array
     */
    protected function getArchivalProfile(): array
    {
        return [
            'label' => 'ArchivalProfile',
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'info' => __(
                "Profil d’archivage applicable aux ArchiveUnit."
            ),
            'attributes' => $this->attrs('IdentifierType'),
            'options' => $this->optionsArchivalProfile(),
            'empty' => true,
            'data-placeholder' => __("-- Choisir un profil d'archives --"),
        ];
    }

    /**
     * ArchiveTransfer.DataObjectPackage.ManagementMetadata.ServiceLevel
     * @return array
     */
    protected function getServiceLevel(): array
    {
        return [
            'label' => 'ServiceLevel',
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'info' => __(
                "Niveau de service applicable aux unités d’archives."
            ),
            'attributes' => $this->attrs('IdentifierType'),
            'options' => $this->optionsServiceLevels(),
            'empty' => true,
            'data-placeholder' => __("-- Choisir un niveau de service --"),
        ];
    }

    /**
     * ArchiveTransfer.DataObjectPackage.ManagementMetadata.AcquisitionInformation
     * @return array
     */
    protected function getAcquisitionInformation(): array
    {
        return [
            'label' => 'AcquisitionInformation : '.__("Modalités d'entrée des archives."),
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
        ];
    }

    /**
     * ArchiveTransfer.DataObjectPackage.ManagementMetadata.LegalStatus
     * @return array
     */
    protected function getLegalStatus(): array
    {
        return [
            'label' => 'LegalStatus : '.__("Statut des archives échangées."),
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'options' => [
                'Public Archive' => 'Public Archive',
                'Private Archive' => 'Private Archive',
                'Public and Private Archive' => 'Public and Private Archive',
            ],
            'empty' => __("-- Veuillez sélectionner un statut --")
        ];
    }

    /**
     * ArchiveTransfer.DataObjectPackage.ManagementMetadata.OriginatingAgencyIdentifier
     * @return array
     */
    protected function getOriginatingAgencyIdentifier(): array
    {
        return [
            'label' => 'OriginatingAgencyIdentifier',
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'info' => __(
                "Identifiant du service producteur - information de "
                . "gestion à ne pas confondre avec OriginatingAgency dans les "
                . "métadonnées de description."
            ),
            'attributes' => $this->attrs('IdentifierType'),
        ];
    }

    /**
     * ArchiveTransfer.DataObjectPackage.ManagementMetadata.SubmissionAgencyIdentifier
     * @return array
     */
    protected function getSubmissionAgencyIdentifier(): array
    {
        return [
            'label' => 'SubmissionAgencyIdentifier',
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'info' => __(
                "Identifiant du service versant - information de gestion à ne"
                . "pas confondre avec SubmissionAgency dans les métadonnées de"
                . "description."
            ),
            'attributes' => $this->attrs('IdentifierType'),
        ];
    }

    /**
     * ArchiveTransfer.RelatedTransferReference
     * @return array
     */
    protected function getRelatedTransferReference(): array
    {
        return [
            'label' => 'RelatedTransferReference : '.__("Identifiant d’un transfert associé."),
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'attributes' => $this->attrs('IdentifierType'),
        ];
    }

    /**
     * ArchiveTransfer.TransferRequestReplyIdentifier
     * @return array
     */
    protected function getTransferRequestReplyIdentifier(): array
    {
        return [
            'label' => 'TransferRequestReplyIdentifier : '.__("Identifiant de la réponse à une demande de transfert."),
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'attributes' => $this->attrs('IdentifierType'),
        ];
    }

    /**
     * Content.ArchivalAgency
     * @return array
     */
    protected function getArchivalAgency(): array
    {
        return [
            'text' => 'ArchivalAgency',
            'text_appendChildValue' => '{{Identifier}}',
            'cardinality' => '1..1',
            'li_attr' => [
                'title' => __("Service d'archives responsable du transfert."),
            ],
            'children' => $this->typeOrganizationType('ArchivalAgency'),
        ];
    }

    /**
     * Content.TransferringAgency
     * @return array
     */
    protected function getTransferringAgency(): array
    {
        return [
            'text' => 'TransferringAgency',
            'text_appendChildValue' => '{{Identifier}}',
            'cardinality' => '1..1',
            'li_attr' => [
                'title' => __("Service versant chargé de réaliser le transport."),
            ],
            'children' => $this->typeOrganizationType('TransferringAgency'),
        ];
    }

    /**
     * OrganizationType
     * @param string $type
     * @return array
     */
    protected function typeOrganizationType(string $type): array
    {
        $identification = [];
        switch ($type) {
            case 'ArchivalAgency':
                $identification = [
                    'label' => __("Service d'archives"),
                    'cardinality' => '1..1',
                    'options' => $this->optionsIdArchivalAgency(),
                    'empty' => true,
                    'data-placeholder' => __("-- Sélectionner un service d'archives --"),
                ];
                break;
            case 'TransferringAgency':
                $identification = [
                    'label' => __("Service versant"),
                    'cardinality' => '1..1',
                    'options' => $this->optionsIdTransferringAgency(),
                    'empty' => true,
                    'data-placeholder' => __("-- Sélectionner un service versant --"),
                ];
                break;
            case 'OriginatingAgency':
                $identification = [
                    'label' => __("Service producteur"),
                    'cardinality' => '1..1',
                    'info' => __(
                        "Service producteur (\"Personne physique ou morale, publique ou privée, \n"
                        . "qui a produit, reçu et conservé des archives  dans l'exercice de son activité\", \n"
                        . "Dictionnaire de terminologie archivistique, direction des archives de France, 2002)."
                    ),
                    'options' => $this->optionsIdOriginatingAgency(),
                    'empty' => true,
                    'data-placeholder' => __("-- Sélectionner un producteur --"),
                ];
                break;
            case 'SubmissionAgency':
                $identification = [
                    'label' => __("Opérateur de versement"),
                    'cardinality' => '1..1',
                    'info' => __("Service versant responsable du transfert des données."),
                    'options' => $this->optionsIdSubmissionAgency(),
                    'empty' => true,
                    'data-placeholder' => __("-- Sélectionner un opérateur de versement --"),
                ];
                break;
        }
        return $this->list(
            [
                'Description' => ['type' => 'hidden', 'cardinality' => '0..1'],
                'Name' => ['type' => 'hidden', 'cardinality' => '0..1'],
                'Identifier' => $identification,
                'custom' => [
                    'type' => 'hidden',
                    'default' => json_encode(
                        [
                            'action' => 'insertOrgEntityData',
                            'args' => [
                                'Identifier',
                                ['description' => 'Description', 'name' => 'Name']
                            ]
                        ]
                    )
                ],
            ],
            'OrganizationDescriptiveMetadata'
        );
    }

    /**
     * Donne les options pour les codes lang
     * @return array
     */
    protected function optionsLang(): array
    {
        $options = Cache::read($key = 'Seda10Schema.options.lang');
        if (empty($options)) {
            $fr = [];
            $majors = [];
            $others = [];
            $langs = self::getXsdOptions('lang');
            usort(
                $langs,
                function ($a, $b) {
                    return strcmp($a['value'], $b['value']);
                }
            );
            foreach ($langs as $lang) {
                switch ($lang['value']) {
                    case 'fra':
                        $fr[] = $lang;
                        break;
                    case 'eng':
                    case 'spa':
                    case 'zho':
                    case 'hin':
                    case 'ara':
                        $majors[] = $lang;
                        break;
                    default:
                        $others[] = $lang;
                }
            }
            Cache::write($key, $options = array_merge($fr, $majors, $others));
        }
        return $options;
    }

    /**
     * Règles pour un contenu lang ex:
     * fr
     * fra
     * fr-FR
     * @return array
     */
    protected function validationIsLang()
    {
        return [
            'rule' => [
                'custom',
                '/[a-zA-Z]{1,8}(-[a-zA-Z0-9]{1,8})*/'
            ]
        ];
    }
}
