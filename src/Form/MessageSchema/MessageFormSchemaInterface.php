<?php
/**
 * AsalaeCore\Form\MessageSchema\MessageFormSchemaInterface
 */

namespace AsalaeCore\Form\MessageSchema;

/**
 * Interface d'un Schema de formulaire d'un message
 *
 * @category Form\MessageSchema
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
interface MessageFormSchemaInterface
{
    /**
     * Contenu de l'objet
     * @param string $context
     * @return array
     */
    public function data(string $context): array;

    /**
     * Donne le nom de l'élément à partir d'un nom généric
     * ex: Archive -> Contains pour du seda 0.2
     * @param string $genericName
     * @param string $context
     * @return string|false si false, ne contient pas $genericName
     */
    public static function getTagname(string $genericName, string $context);

    /**
     * Donne les options contenu dans un fichier xsd
     * @param string $keypath
     * @param string $enumNamePath
     * @param string $descriptionPath
     * @return array
     */
    public static function getXsdOptions(
        string $keypath,
        string $enumNamePath = 'ccts:Name',
        string $descriptionPath = 'ccts:Description'
    ): array;

    /**
     * Donne le fichier xsd de liste de codes
     * @param string $keypath
     * @return string
     */
    public static function getXsdCodePath(string $keypath): string;
}
