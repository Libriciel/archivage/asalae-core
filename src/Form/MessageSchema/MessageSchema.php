<?php
/**
 * AsalaeCore\Form\MessageSchema\MessageSchema
 */

namespace AsalaeCore\Form\MessageSchema;

use AsalaeCore\Utility\ValidatorXml;
use Cake\Cache\Cache;
use Cake\Http\Exception\NotImplementedException;
use DOMDocument;

/**
 * Schema d'un message seda
 *
 * @category Form\MessageSchema
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class MessageSchema
{
    /**
     * @var MessageFormSchemaInterface[] liste les schemas (lorsque Cache::disabled)
     */
    public static $schemas;

    /**
     * Donne la classe de schema selon la version du message
     * @param string $version
     * @return string|MessageFormSchemaInterface
     */
    public static function getSchemaClassname(string $version = NAMESPACE_SEDA_10): string
    {
        switch (self::getNamespace($version)) {
            case NAMESPACE_SEDA_02:
                return Seda02Schema::class;
            case NAMESPACE_SEDA_10:
                return Seda10Schema::class;
            case NAMESPACE_SEDA_21:
                return Seda21Schema::class;
            case NAMESPACE_SEDA_22:
                return Seda22Schema::class;
            default:
                throw new NotImplementedException;
        }
    }

    /**
     * Donne le schema selon la version/service d'archives
     * @param string   $version     du seda
     * @param null|int $orgEntityId
     * @return MessageFormSchemaInterface
     */
    public static function getSchema(
        string $version = NAMESPACE_SEDA_10,
        $orgEntityId = null
    ): MessageFormSchemaInterface {
        $classname = self::getSchemaClassname($version);
        return new $classname($orgEntityId);
    }

    /**
     * Récupère le schema selon orgEntityId
     * @param string   $base
     * @param string   $version
     * @param null|int $orgEntityId
     * @return array|MessageFormSchemaInterface|mixed|null
     */
    public static function getCachedSchema(
        string $base = 'ArchiveTransfer',
        string $version = NAMESPACE_SEDA_10,
        $orgEntityId = null
    ) {
        $key = self::getCacheKey($base, $version, $orgEntityId);
        if (!empty(self::$schemas[$key])) {
            return self::$schemas[$key];
        }
        if (Cache::enabled()) {
            self::$schemas[$key] = Cache::read($key);
        }
        if (empty(self::$schemas[$key])) {
            $schemaObject = self::getSchema($version, $orgEntityId);
            self::$schemas[$key] = $schemaObject->data($base);
            if (Cache::enabled()) {
                Cache::write($key, self::$schemas[$key]);
            }
        }
        return self::$schemas[$key];
    }

    /**
     * Renvoi le namespace lié à une version
     * @param string $version
     * @return string
     */
    public static function getNamespace(string $version): string
    {
        switch ($version) {
            case NAMESPACE_SEDA_02:
            case 'seda0.2':
            case '0.2':
                return NAMESPACE_SEDA_02;
            case NAMESPACE_SEDA_10:
            case 'seda1.0':
            case '1.0':
                return NAMESPACE_SEDA_10;
            case NAMESPACE_SEDA_21:
            case 'seda2.1':
            case '2.1':
                return NAMESPACE_SEDA_21;
            case NAMESPACE_SEDA_22:
            case 'seda2.2':
            case '2.2':
                return NAMESPACE_SEDA_22;
            default:
                throw new NotImplementedException;
        }
    }

    /**
     * Donne une clef de cache
     * @param string   $base
     * @param string   $version
     * @param null|int $orgEntityId
     * @return string
     */
    private static function getCacheKey(
        string $base = 'ArchiveTransfer',
        string $version = NAMESPACE_SEDA_10,
        $orgEntityId = null
    ): string {
        switch (self::getNamespace($version)) {
            case NAMESPACE_SEDA_02:
                $baseKey = 'Seda02_schema';
                break;
            case NAMESPACE_SEDA_10:
                $baseKey = 'Seda10_schema';
                break;
            case NAMESPACE_SEDA_21:
                $baseKey = 'Seda21_schema';
                break;
            case NAMESPACE_SEDA_22:
                $baseKey = 'Seda22_schema';
                break;
            default:
                throw new NotImplementedException;
        }
        return $baseKey.'_'.$orgEntityId.($base ? '_'.$base : '');
    }

    /**
     * Supprime le cache
     * @param int|null $orgEntityId
     */
    public static function clearCache($orgEntityId = null)
    {
        $versions = [NAMESPACE_SEDA_02, NAMESPACE_SEDA_10, NAMESPACE_SEDA_21, NAMESPACE_SEDA_22];
        $bases = ['', 'ArchiveTransfer', 'Archive'];
        $entities = $orgEntityId ? [null, $orgEntityId] : [null];
        foreach ($versions as $version) {
            foreach ($bases as $base) {
                foreach ($entities as $entity) {
                    Cache::delete(self::getCacheKey($base, $version, $entity));
                }
            }
        }
    }

    /**
     * schemaValidate d'un seda
     * @param string|DOMDocument $mixed chemin de fichier, (string)xml ou DOMDocument
     * @return bool
     */
    public static function validate($mixed): bool
    {
        if (is_string($mixed)) {
            $dom = new DOMDocument;
            if (is_file($mixed)) {
                $dom->load($mixed);
            } else {
                $dom->loadXML($mixed);
            }
        } else {
            $dom = $mixed;
        }
        $xmlns = $dom->documentElement->getAttributeNode('xmlns');
        $namespace = $xmlns ? $xmlns->nodeValue : null;
        $path = tempnam(sys_get_temp_dir(), "ValidatorXml-");
        $dom->save($path);

        switch ($namespace) {
            case NAMESPACE_SEDA_02:
                $validator = new ValidatorXml(SEDA_V02_XSD);
                break;
            case NAMESPACE_SEDA_10:
                $validator = new ValidatorXml(SEDA_V10_XSD);
                break;
            case NAMESPACE_SEDA_21:
                $validator = new ValidatorXml(SEDA_ARCHIVE_V21_XSD);
                break;
            case NAMESPACE_SEDA_22:
                $validator = new ValidatorXml(SEDA_ARCHIVE_V22_XSD);
                break;
            default:
                return false;
        }
        $valid = $validator->validate($path);
        foreach ($validator->errors as $error) {
            $error = str_replace(['{'.$namespace.'}', '<br/>'], ['', "\n"], $error);
            trigger_error($error, E_USER_WARNING);
        }
        return $valid;
    }
}
