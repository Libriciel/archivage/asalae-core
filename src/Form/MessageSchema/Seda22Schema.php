<?php
/**
 * AsalaeCore\Form\MessageSchema\Seda22Schema
 */

namespace AsalaeCore\Form\MessageSchema;

/**
 * Schema d'un message seda v2.2
 *
 * @category Form\MessageSchema
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2022, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Seda22Schema extends Seda21Schema implements MessageFormSchemaInterface
{
    /**
     * Traits
     */
    use SchemaTrait;

    /**
     * ref MinimalDataObjectType
     * @return array
     */
    protected function extMinimalDataObjectType()
    {
        return $this->list(
            'DataObjectProfile',
            'DataObjectSystemId',
            'DataObjectGroupSystemId',
            'Relationship',
            'DataObjectGroupReferenceId',
            'DataObjectGroupId',
            'DataObjectVersion'
        );
    }

    /**
     * ArchiveTransfer.DataObjectPackage.BinaryDataObject
     * @return array
     */
    protected function getBinaryDataObject(): array
    {
        $data = parent::getBinaryDataObject();
        $data['complex'] = array_merge(['DataObjectProfile'], $data['complex']);
        return $data;
    }

    /**
     * MinimalDataObjectType.DataObjectProfile
     * @return array
     */
    protected function getDataObjectProfile(): array
    {
        return [
            'label' => 'DataObjectProfile',
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'info' => __(
                "Référence à une partie d'un profil d’archivage applicable à un objet technique en particulier"
            ),
            'attributes' => $this->attrs('IdentifierType'),
        ];
    }

    /**
     * ref ManagementType
     * @return array
     */
    protected function refManagementType(): array
    {
        return $this->list(
            'StorageRule',
            'AppraisalRule',
            'AccessRule',
            'DisseminationRule',
            'ReuseRule',
            'ClassificationRule',
            'LogBook',
            'NeedAuthorization',
            'HoldRule'
        );
    }

    /**
     * ManagementType.HoldRule
     * @return array
     */
    protected function getHoldRule(): array
    {
        return [
            'text' => 'HoldRule',
            'cardinality' => '0..1',
            'li_attr' => [
                'title' => __("Gestion de la durée de gel des ArchiveUnits."),
            ],
            'children' => $this->list(
                'Rule',
                'StartDate',
                'HoldEndDate',
                'HoldOwner',
                'HoldReassessingDate',
                'HoldReason',
                'PreventRearrangement',
                'PreventInheritance',
                'RefNonRuleId',
            ),
            'complex' => [
                [
                    'sequence' => [
                        'Rule',
                        'StartDate',
                        'HoldEndDate',
                        'HoldOwner',
                        'HoldReassessingDate',
                        'HoldReason',
                        'PreventRearrangement',
                    ],
                ],
                [
                    'choice' => [
                        'PreventInheritance',
                        'RefNonRuleId',
                    ],
                ],
            ],
        ];
    }

    /**
     * ArchiveTransfer.DataObjectPackage.DescriptiveMetadata.ArchiveUnit.Content
     * @return array
     */
    protected function getContent(): array
    {
        return [
            'text' => 'Content',
            'cardinality' => '1..1',
            'li_attr' => [
                'title' => __("Métadonnées de description associées à un ArchiveUnit."),
            ],
            'children' => $this->list(
                'DescriptionLevel',
                'Title',
                'FilePlanPosition',
                'SystemId',
                'OriginatingSystemId',
                'ArchivalAgencyArchiveUnitIdentifier',
                'OriginatingAgencyArchiveUnitIdentifier',
                'TransferringAgencyArchiveUnitIdentifier',
                'Description',
                'CustodialHistory',
                'Type',
                'DocumentType',
                'Language',
                'DescriptionLanguage',
                'Status',
                'Version',
                'Tag',
                'Keyword',
                'Coverage',
                'OriginatingAgency',
                'SubmissionAgency',
                'Agent',
                'AuthorizedAgent',
                'Writer',
                'Addressee',
                'Recipient',
                'Transmitter',
                'Sender',
                'Source',
                'RelatedObjectReference',
                'CreatedDate',
                'TransactedDate',
                'AcquiredDate',
                'SentDate',
                'ReceivedDate',
                'RegisteredDate',
                [
                    'StartDate' => [
                        'label' => "StartDate : ".__("Date d'ouverture / date de début."),
                        'cardinality' => '0..1',
                        'help' => __("Optionnel"),
                        'data-type' => 'datetime',
                        'validations' => $this->validations('DateTimeType'),
                        'class' => 'datepicker',
                        'placeholder' => $this->datetimePlaceholder(),
                    ],
                ],
                'EndDate',
                'DateLitteral',
                'Event',
                'Signature',
                'Gps',
                'OriginatingSystemIdReplyTo',
                'TextContent'
            )
        ];
    }

    /**
     * Content.OriginatingSystemIdReplyTo
     * @return array
     */
    protected function getOriginatingSystemIdReplyTo(): array
    {
        return [
            'label' => "OriginatingSystemIdReplyTo : ".__("Référence du message auquel on répond."),
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
        ];
    }

    /**
     * Content.TextContent
     * @return array
     */
    protected function getTextContent(): array
    {
        return [
            'label' => "TextContent : ".__("Contenu du message électronique."),
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
        ];
    }

    /**
     * Content.Agent
     * @return array
     */
    protected function getAgent(): array
    {
        return [
            'text' => 'Agent',
            'cardinality' => '0..n',
            'li_attr' => [
                'title' => __("Agent générique."),
            ],
            'children' => $this->refAgentType()
        ];
    }

    /**
     * Content.DateLitteral
     * @return array
     */
    protected function getDateLitteral(): array
    {
        return [
            'label' => "DateLitteral : ".__("Champ date en texte libre."),
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
        ];
    }

    /**
     * ref EventType
     * @return array
     */
    protected function refEventType(): array
    {
        return $this->list(
            'EventIdentifier',
            'EventTypeCode',
            'EventType',
            'EventDateTime',
            'EventDetail',
            'Outcome',
            'OutcomeDetail',
            'OutcomeDetailMessage',
            'EventDetailData',
            'LinkingAgentIdentifier',
            'EventAbstract'
        );
    }

    /**
     * EventType.LinkingAgentIdentifier
     * @return array
     */
    protected function getLinkingAgentIdentifier(): array
    {
        return [
            'text' => 'LinkingAgentIdentifier',
            'cardinality' => '0..n',
            'li_attr' => [
                'title' => __("Permet de renseigner des agents répertoriés dans des évènements."),
            ],
            'children' => $this->list(
                'LinkingAgentIdentifierType',
                'LinkingAgentIdentifierValue',
                'LinkingAgentRole'
            )
        ];
    }

    /**
     * EventType.LinkingAgentIdentifier.LinkingAgentIdentifierType
     * @return array
     */
    protected function getLinkingAgentIdentifierType(): array
    {
        return [
            'label' => 'LinkingAgentIdentifierType : '
                .__("Identifiant d'un agent répertorié dans des évènements."),
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
        ];
    }

    /**
     * EventType.LinkingAgentIdentifier.LinkingAgentIdentifierValue
     * @return array
     */
    protected function getLinkingAgentIdentifierValue(): array
    {
        return [
            'label' => 'LinkingAgentIdentifierValue : '
                .__("Mention d'un agent répertorié dans des évènements."),
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
        ];
    }

    /**
     * EventType.LinkingAgentIdentifier.LinkingAgentRole
     * @return array
     */
    protected function getLinkingAgentRole(): array
    {
        return [
            'label' => 'LinkingAgentRole : '
                .__("Fonction d'un agent répertorié dans des évènements."),
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
        ];
    }

    /**
     * ManagementType.HoldRule.HoldEndDate
     * @return array
     */
    protected function getHoldEndDate(): array
    {
        return [
            'label' => 'HoldEndDate : '.__("Date de fin de gel explicite."),
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'data-type' => 'date',
            'validations' => $this->validations('DateType'),
            'class' => 'datepicker',
            'placeholder' => $this->datePlaceholder(),
        ];
    }

    /**
     * ManagementType.HoldRule.HoldOwner
     * @return array
     */
    protected function getHoldOwner(): array
    {
        return [
            'label' => 'HoldOwner : '.__("Propriétaire de la demande de gel."),
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
        ];
    }

    /**
     * ManagementType.HoldRule.HoldReassessingDate
     * @return array
     */
    protected function getHoldReassessingDate(): array
    {
        /**
        'HoldReason',
        'PreventRearrangement',
         */
        return [
            'label' => 'HoldReassessingDate : '.__("Date de réévaluation du gel."),
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'data-type' => 'date',
            'validations' => $this->validations('DateType'),
            'class' => 'datepicker',
            'placeholder' => $this->datePlaceholder(),
        ];
    }

    /**
     * ManagementType.HoldRule.HoldReason
     * @return array
     */
    protected function getHoldReason(): array
    {
        return [
            'label' => 'HoldReason : '.__("Motif de gel."),
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
        ];
    }

    /**
     * ManagementType.HoldRule.PreventRearrangement
     * @return array
     */
    protected function getPreventRearrangement(): array
    {
        return [
            'label' => 'PreventRearrangement',
            'cardinality' => '0..1',
            'info' => __(
                "Blocage de la reclassification de l'ArchiveUnit lorsque la restriction de gel est effective"
            ),
            'help' => __("Optionnel"),
            'empty' => __("-- Blocage de la reclassification --"),
            'options' => [
                'true' => __("Oui"),
                'false' => __("Non"),
            ],
        ];
    }
}
