<?php
/**
 * AsalaeCore\Form\MessageSchema\Seda10Schema
 *
 * @noinspection PhpUnusedPrivateMethodInspection
 */

namespace AsalaeCore\Form\MessageSchema;

use Cake\Cache\Cache;
use Cake\Utility\Hash;

/**
 * Schema d'un message seda v1.0
 *
 * @category Form\MessageSchema
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Seda10Schema implements MessageFormSchemaInterface
{
    /**
     * Traits
     */
    use SchemaTrait;

    /**
     * Contenu de l'objet
     * @param string $context
     * @return array
     */
    public function data(string $context): array
    {
        switch ($context) {
            case 'Archive':
                return [
                    'Archive' => [
                        'cardinality' => '1..1',
                    ] + $this->getArchive()
                ];
            case 'ArchiveTransfer':
            default:
                return $this->getArchiveTransfer();
        }
    }

    /**
     * Donne le nom de l'élément à partir d'un nom généric
     * ex: Archive -> Contains pour du seda 0.2
     * @param string $genericName
     * @param string $context
     * @return string si false, ne contient pas $genericName
     */
    public static function getTagname(string $genericName, string $context)
    {
        return $genericName;
    }

    /**
     * Donne le fichier xsd de liste de codes
     * @param string $keypath
     * @return string
     */
    public static function getXsdCodePath(string $keypath): string
    {
        return SEDA_V10_CODES.DS.SEDA_V10_CODES_FILES[$keypath];
    }

    /**
     * ArchiveTransfer
     * @return array
     */
    public function getArchiveTransfer()
    {
        return [
            'ArchiveTransfer' => [
                'text' => __("Transfert d'archives"),
                'element' => 'ArchiveTransfer',
                'cardinality' => '1..1',
                'li_attr' => [
                    'title' => __("Transfert d'archives."),
                ],
                'icon' => [
                    'complete' => 'fa fa-check text-success',
                    'incomplete' => 'fa fa-exclamation-circle text-danger',
                    'fields' => ['Date', 'TransferIdentifier', 'ArchivalAgency', 'TransferringAgency', 'Archive'],
                ],
                'children' => $this->list(
                    'Comment',
                    'Date',
                    'RelatedTransferReference',
                    'TransferIdentifier',
                    'TransferRequestReplyIdentifier',
                    'ArchivalAgency',
                    'TransferringAgency',
                    'Integrity',
                    'Archive',
                    'NonRepudiation'
                )
            ]
        ];
    }

    /**
     * Donne les attributs pour un type donné
     * @param string $string
     * @return array
     */
    private function attrs(string $string): array
    {
        $agencyIdentificationCodeContentType = self::getXsdOptions(
            'agencyid',
            'ccts:Name',
            'ccts:Definition'
        );
        $schemeAgencyId = [
            'help' => __("The identification of the agency that maintains the identification scheme."),
            'options' => $agencyIdentificationCodeContentType,
            'empty' => true,
            'data-placeholder' => __("-- Choisir une identification --"),
        ];
        $languageId = [
            'help' => __("The identifier of the language used in the corresponding text string."),
            'pattern' => '[a-zA-Z]{1,8}(-[a-zA-Z0-9]{1,8})*',
            'validations' => [
                'islang' => [
                    'rule' => [
                        'custom',
                        '/[a-zA-Z]{1,8}(-[a-zA-Z0-9]{1,8})*/'
                    ]
                ]
            ]
        ];
        $listAgencyID = [
            'help' => __("An agency that maintains one or more code lists."),
            'options' => $agencyIdentificationCodeContentType,
            'empty' => true,
            'data-placeholder' => __("-- Choisir une identification --"),
        ];
        switch ($string) {
            case 'TextType':
                return [
                    'languageID' => $languageId
                ];
            /** @noinspection PhpMissingBreakStatementInspection */
            case 'ArchivesIDType':
                $schemeAgencyId = ['type' => 'hidden'];// use="prohibited"
                // fall-through
                // no break
            case 'IDType':
                return [
                    'schemeID' => [
                        'help' => __("The identification of the identification scheme."),
                    ],
                    'schemeName' => [
                        'help' => __("The name of the identification scheme."),
                    ],
                    'schemeAgencyID' => $schemeAgencyId,
                    'schemeAgencyName' => [
                        'help' => __("The name of the agency that maintains the identification scheme."),
                    ],
                    'schemeVersionID' => [
                        'help' => __("The version of the identification scheme."),
                    ],
                    'schemeDataURI' => [
                        'help' => __(
                            "The Uniform Resource Identifier that identifies"
                            ." where the identification scheme data is located."
                        ),
                    ],
                    'schemeURI' => [
                        'help' => __(
                            "The Uniform Resource Identifier that identifies"
                            ." where the identification scheme is located."
                        ),
                    ],
                ];
            /** @noinspection PhpMissingBreakStatementInspection */
            case 'ArchivesCodeType':
                $listAgencyID = ['type' => 'hidden'];
                // fall-through
                // no break
            case 'CodeType':
                return [
                    'listID' => [
                        'help' => __("The identification of a list of codes."),
                    ],
                    'listAgencyID' => $listAgencyID,
                    'listAgencyName' => [
                        'help' => __("The name of the agency that maintains the code list."),
                    ],
                    'listName' => [
                        'help' => __("The name of a list of codes."),
                    ],
                    'listVersionID' => [
                        'help' => __("The version of the code list."),
                    ],
                    'name' => [
                        'help' => __("The textual equivalent of the code content."),
                    ],
                    'languageID' => $languageId,
                    'listURI' => [
                        'help' => __("The Uniform Resource Identifier that identifies where the code list is located."),
                    ],
                    'listSchemeURI' => [
                        'help' => __(
                            "The Uniform Resource Identifier that identifies where the code list scheme is located."
                        ),
                    ],
                ];
            case 'CodeAccessRestrictionType':
            case 'CodeAppraisalType':
            case 'CodeKeywordType':
            case 'AppraisalCodeType':
                return [
                    'listVersionID' => [
                        'placeholder' => 'edition 2009',
                        'help' => __("The version of the code list."),
                    ],
                ];
            case 'CodeLanguageType':
                return [
                    'listVersionID' => [
                        'placeholder' => 'edition 2011',
                        'help' => __("The version of the code list."),
                    ],
                ];
            case 'ArchivesBinaryObjectType':
                $formats = self::getXsdOptions('filetype');
                $mimes = self::getXsdOptions('mime');
                $encoding = self::getXsdOptions(
                    'encoding',
                    'ccts:name'
                );
                $charset = self::getXsdOptions('charset');
                return [
                    'format' => [
                        'help' => __("The format of the binary content."),
                        'options' => $formats,
                        'data-placeholder' => __("-- Choisir un format --"),
                        'empty' => true,
                    ],
                    'mimeCode' => [
                        'help' => __("The mime type of the binary object."),
                        'options' => $mimes,
                        'data-placeholder' => __("-- Choisir un type mime --"),
                        'empty' => true,
                    ],
                    'encodingCode' => [
                        'help' => __("Specifies the decoding algorithm of the binary object."),
                        'options' => $encoding,
                        'data-placeholder' => __("-- Choisir un encodage --"),
                        'empty' => true,
                    ],
                    'characterSetCode' => [
                        'help' => __("The character set of the binary object if the mime type is text."),
                        'options' => $charset,
                        'data-placeholder' => __("-- Choisir un charset --"),
                        'empty' => true,
                    ],
                    'uri' => [
                        'help' => __(
                            "The Uniform Resource Identifier that identifies where the binary object is located."
                        ),
                        'type' => 'hidden',
                    ],
                    'filename' => [
                        'help' => __("The filename of the binary object."),
                        'class' => 'filename',
                        'type' => 'hidden',
                    ],
                ];
            default:
                return [];
        }
    }

    /**
     * ArchiveTransfer.Comment
     * @return array
     */
    public function getComment()
    {
        return [
            'label' => 'Comment : '.__("Commentaires"),
            'type' => 'textarea',
            'data-type' => 'text',
            'cardinality' => '0..n',
            'help' => __("Optionnel"),
            'attributes' => $this->attrs('TextType'),
        ];
    }

    /**
     * ArchiveTransfer.Date
     * @return array
     */
    private function getDate(): array
    {
        return [
            'label' => "Date : ".__("Date de l'émission du message"),
            'cardinality' => '1..1',
            'data-type' => 'datetime',
            'validations' => $this->validations('DateTimeType'),
            'class' => 'datepicker',
            'placeholder' => $this->datetimePlaceholder(),
        ];
    }

    /**
     * ArchiveTransfer.RelatedTransferReference
     * @return array
     */
    private function getRelatedTransferReference(): array
    {
        return [
            'label' => "RelatedTransferReference : ".__("Référence à un autre transfert"),
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'attributes' => $this->attrs('ArchivesIDType'),
        ];
    }

    /**
     * ArchiveTransfer.TransferIdentifier
     * @return array
     */
    private function getTransferIdentifier(): array
    {
        return [
            'label' => "TransferIdentifier : ".__("Identifiant du transfert"),
            'cardinality' => '1..1',
            'attributes' => $this->attrs('ArchivesIDType'),
            'default' => '#A_CALCULER_LORS_DU_VERROUILLAGE#',
        ];
    }

    /**
     * ArchiveTransfer.TransferRequestReplyIdentifier
     * @return array
     */
    private function getTransferRequestReplyIdentifier(): array
    {
        return [
            'label' => "TransferRequestReplyIdentifier",
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'attributes' => $this->attrs('ArchivesIDType'),
        ];
    }

    /**
     * ArchiveTransfer.ArchivalAgency
     * @return array
     */
    private function getArchivalAgency(): array
    {
        return [
            'text' => "ArchivalAgency",
            'text_appendChildValue' => '{{Identification}}',
            'cardinality' => '1..1',
            'children' => $this->typeOrganizationType('ArchivalAgency'),
            'icon' => [
                'complete' => 'fa fa-check-square-o',
                'incomplete' => 'fa fa-square-o',
                'fields' => ['Identification'],
            ],
        ];
    }

    /**
     * ArchiveTransfer.TransferringAgency
     * @return array
     */
    private function getTransferringAgency(): array
    {
        return [
            'text' => "TransferringAgency",
            'text_appendChildValue' => '{{Identification}}',
            'cardinality' => '1..1',
            'children' => $this->typeOrganizationType('TransferringAgency'),
            'icon' => [
                'complete' => 'fa fa-check-square-o',
                'incomplete' => 'fa fa-square-o',
                'fields' => ['Identification'],
            ],
        ];
    }

    /**
     * ArchiveTransfer.Archive
     * @return array
     */
    private function getArchive(): array
    {
        return [
            'text' => "Archive",
            'text_appendChildValue' => '{{Name}}',
            'cardinality' => '1..n',
            'children' => $this->typeArchiveType('Archive'),
            'icon' => [
                'complete' => 'fa fa-archive',
                'incomplete' => 'fa fa-warning',
                'fields' => ['DescriptionLanguage', 'Name'],
            ],
        ];
    }

    /**
     * OrganizationType
     * @param string $type
     * @return array
     */
    private function typeOrganizationType(string $type): array
    {
        $identification = [];
        switch ($type) {
            case 'ArchivalAgency':
                $identification = [
                    'label' => __("Service d'archives"),
                    'cardinality' => '1..1',
                    'options' => $this->optionsIdArchivalAgency(),
                    'empty' => true,
                    'data-placeholder' => __("-- Sélectionner un service d'archives --"),
                ];
                break;
            case 'TransferringAgency':
                $identification = [
                    'label' => __("Service versant"),
                    'cardinality' => '1..1',
                    'options' => $this->optionsIdTransferringAgency(),
                    'empty' => true,
                    'data-placeholder' => __("-- Sélectionner un service d'archives --"),
                ];
                break;
            case 'OriginatingAgency':
                $identification = [
                    'label' => __("Service producteur"),
                    'cardinality' => '1..1',
                    'info' => __(
                        "Service producteur (\"Personne physique ou morale, "
                        ."publique ou privée, \nqui a produit, reçu et conservé des archives"
                        ." dans l'exercice de son activité\", \nDictionnaire de terminologie"
                        ." archivistique, direction des archives de France, 2002)."
                    ),
                    'options' => $this->optionsIdOriginatingAgency(),
                    'empty' => true,
                    'data-placeholder' => __("-- Sélectionner un producteur --"),
                ];
                break;
        }
        return $this->list(
            'BusinessType',
            [
                'Description' => ['type' => 'hidden', 'cardinality' => '0..1'],
                'Identification' => $identification,
                'custom' => [
                    'type' => 'hidden',
                    'default' => json_encode(
                        [
                            'action' => 'insertOrgEntityData',
                            'args' => [
                                'Identification',
                                ['description' => 'Description', 'name' => 'Name']
                            ]
                        ]
                    )
                ],
            ],
            'LegalClassification',
            ['Name' => ['type' => 'hidden', 'cardinality' => '0..1']],
            'Address',
            'Communication',
            'Contact'
        );
    }

    /**
     * ArchiveType
     * @param string $name
     * @return array
     */
    private function typeArchiveType(string $name)
    {
        return $this->list(
            'ArchivalAgencyArchiveIdentifier',
            'ArchivalAgreement',
            'ArchivalProfile',
            'DescriptionLanguage',
            [
                'Name' => [
                    'label' => "Name : ".__("Intitulé de l'<{0}>.", $name),
                    'cardinality' => '1..1',
                    'attributes' => $this->attrs('TextType'),
                ],
            ],
            'OriginatingAgencyArchiveIdentifier',
            'ServiceLevel',
            'TransferringAgencyArchiveIdentifier',
            'ContentDescription',
            'AccessRestrictionRule',
            'AppraisalRule',
            'ArchiveObject',
            'Document'
        );
    }

    /**
     * ArchiveTransfer.Archive.ArchivalAgreement
     * @return array
     */
    private function getArchivalAgreement(): array
    {
        return [
            'label' => "ArchivalAgreement",
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'attributes' => $this->attrs('ArchivesIDType'),
            'info' => __(
                "Convention de services : porte sur les conditions qui pourront"
                ." faire l'objet d'un contrôle par le SAE pour une catégorie d'archives"
                ." (périodicité de la transaction, volumétries, formats des données...)."
            ),
            'options' => $this->optionsArchivalAgreement(),
            'empty' => true,
            'data-placeholder' => __("-- Choisir un accord de versement --"),
        ];
    }

    /**
     * ArchiveTransfer.Archive.ArchivalProfile
     * @return array
     */
    private function getArchivalProfile(): array
    {
        return [
            'label' => "ArchivalProfile",
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'attributes' => $this->attrs('ArchivesIDType'),
            'info' => __("Modèle de description/profil d'archivage pour une catégorie d'archives donnée."),
            'options' => $this->optionsArchivalProfile(),
            'empty' => true,
            'data-placeholder' => __("-- Choisir un profil d'archives --"),
        ];
    }

    /**
     * ArchiveTransfer.Archive.DescriptionLanguage
     * @return array
     */
    private function getDescriptionLanguage(): array
    {
        return [
            'label' => "DescriptionLanguage",
            'cardinality' => '1..n',
            'attributes' => $this->attrs('CodeLanguageType'),
            'info' => __("Langue utilisée pour les informations de représentation et de pérennisation."),
            'options' => $this->optionsLang(),
            'empty' => true,
            'data-placeholder' => __("-- Sélectionner une langue --"),
            'default' => 'fra',
        ];
    }

    /**
     * ArchiveTransfer.Archive.OriginatingAgencyArchiveIdentifier
     * @return array
     */
    private function getOriginatingAgencyArchiveIdentifier(): array
    {
        return [
            'label' => "OriginatingAgencyArchiveIdentifier : "
                .__("Identifiant attribué à l'<Archive> par le service producteur."),
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'attributes' => $this->attrs('ArchivesIDType'),
        ];
    }

    /**
     * ArchiveTransfer.Archive.ServiceLevel
     * @return array
     */
    private function getServiceLevel(): array
    {
        return [
            'label' => "ServiceLevel : ".__("Niveau de service demandé (disponibilité, sécurité...)."),
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'attributes' => $this->attrs('ArchivesCodeType'),
            'options' => $this->optionsServiceLevels(),
            'empty' => true,
            'data-placeholder' => __("-- Choisir un niveau de service --"),
        ];
    }

    /**
     * ArchiveTransfer.Archive.TransferringAgencyArchiveIdentifier
     * @return array
     */
    private function getTransferringAgencyArchiveIdentifier(): array
    {
        return [
            'label' => "TransferringAgencyArchiveIdentifier : "
                .__("Identifiant attribué à l'<Archive> par le service versant."),
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'attributes' => $this->attrs('ArchivesIDType'),
        ];
    }

    /**
     * Donne les options pour les codes lang
     * @return array
     */
    private function optionsLang(): array
    {
        $options = Cache::read($key = 'Seda10Schema.options.lang');
        if (empty($options)) {
            $fr = [];
            $majors = [];
            $others = [];
            $langs = self::getXsdOptions('lang');
            usort(
                $langs,
                function ($a, $b) {
                    return strcmp($a['value'], $b['value']);
                }
            );
            foreach ($langs as $lang) {
                switch ($lang['value']) {
                    case 'fra':
                        $fr[] = $lang;
                        break;
                    case 'eng':
                    case 'spa':
                    case 'zho':
                    case 'hin':
                    case 'ara':
                        $majors[] = $lang;
                        break;
                    default:
                        $others[] = $lang;
                }
            }
            Cache::write($key, $options = array_merge($fr, $majors, $others));
        }
        return $options;
    }

    /**
     * ArchiveTransfer.Archive.ContentDescription
     * @return array
     */
    private function getContentDescription(): array
    {
        return [
            'text' => "ContentDescription",
            'cardinality' => '1..1',
            'li_attr' => [
                'title' => __("Informations de pérennisation de l'<Archive>."),
            ],
            'action' => 'add-multiple-keyword',
            'children' => $this->list(
                [
                    'Description' => [
                        'label' => "Description : ".__("Description"),
                        'cardinality' => '0..1',
                        'help' => __("Optionnel"),
                        'info' => __("Permet de donner des précisions sur le contenu de l'objet."),
                        'attributes' => $this->attrs('TextType'),
                        'type' => 'textarea',
                        'data-type' => 'text',
                    ]
                ],
                'DescriptionLevel',
                'FilePlanPosition',
                [
                    'Language' => [
                        'label' => "Language : ".__("Langue du contenu de l'objet."),
                        'cardinality' => '1..n',
                        'attributes' => $this->attrs('CodeLanguageType'),
                        'options' => $this->optionsLang(),
                        'data-placeholder' => __("-- Sélectionner une langue --"),
                        'empty' => true,
                        'default' => 'fra',
                    ],
                ],
                'LatestDate',
                'OldestDate',
                'OtherDescriptiveData',
                [
                    'AccessRestrictionRule' => [
                        'text' => 'AccessRestrictionRule',
                        'cardinality' => '0..1',
                        'li_attr' => [
                            'title' => __(
                                "Règle à appliquer en matière d'accès aux"
                                ." informations de pérennisation (communicabilité)"
                            )
                        ]
                    ] + $this->getAccessRestrictionRule(),
                ],
                'CustodialHistory',
                'Keyword',
                'OriginatingAgency',
                'OtherMetadata',
                'RelatedObjectReference',
                'Repository'
            ),
        ];
    }

    /**
     * ArchiveTransfer.Archive.ContentDescription.FilePlanPosition
     * @return array
     */
    private function getFilePlanPosition(): array
    {
        return [
            'label' => "FilePlanPosition",
            'cardinality' => '0..n',
            'info' => __("Position du contenu d'information dans le plan de classement du producteur."),
        ];
    }

    /**
     * ArchiveTransfer.Archive.AccessRestrictionRule.LatestDate
     * @return array
     */
    private function getLatestDate(): array
    {
        return [
            'label' => 'LatestDate : '.__("Date de production la plus récente des objets-données."),
            'cardinality' => '0..1',
            'data-type' => 'date',
            'validations' => $this->validations('DateType'),
            'class' => 'datepicker',
            'data-validationtype' => 'latestdate',
            'placeholder' => $this->datePlaceholder(),
        ];
    }

    /**
     * ArchiveTransfer.Archive.AccessRestrictionRule.OldestDate
     * @return array
     */
    private function getOldestDate(): array
    {
        return [
            'label' => 'OldestDate : '.__("Date de production la plus ancienne des objets-données."),
            'cardinality' => '0..1',
            'data-type' => 'date',
            'validations' => $this->validations('DateType'),
            'class' => 'datepicker',
            'data-validationtype' => 'oldestdate',
            'placeholder' => $this->datePlaceholder(),
        ];
    }

    /**
     * ArchiveTransfer.Archive.ContentDescription.OtherDescriptiveData
     * @return array
     */
    private function getOtherDescriptiveData(): array
    {
        return [
            'label' => "OtherDescriptiveData : ".__("Autres informations sur l'objet-données."),
            'cardinality' => '0..1',
            'type' => 'textarea',
            'data-type' => 'text',
            'attributes' => $this->attrs('TextType'),
        ];
    }

    /**
     * ArchiveTransfer.Archive.ContentDescription.DescriptionLevel
     * @return array
     */
    private function getDescriptionLevel(): array
    {
        return [
            'label' => "DescriptionLevel",
            'cardinality' => '1..1',
            'info' => __(
                "Niveau de description (Isad(g)) indiquant si l'objet <Archive>"
                ." ou <ArchiveObject>. décrit est un groupe de documents, un sous-groupe"
                ." de documents, un dossier, ou une pièce."
            ),
            'options' => self::getXsdOptions('level'),
            'empty' => true,
            'data-placeholder' => __("-- Sélectionner un niveau de description -- "),
        ];
    }

    /**
     * ArchiveTransfer.Archive.AccessRestrictionRule
     * @return array
     */
    private function getAccessRestrictionRule(): array
    {
        return [
            'text' => "AccessRestrictionRule",
            'cardinality' => '1..1',
            'li_attr' => [
                'title' => __("Règle à appliquer en matière d'accès aux objets-données (communicabilité)."),
            ],
            'children' => $this->list(
                [
                    'Code' => [
                        'label' => 'Code',
                        'cardinality' => '1..1',
                        'info' => __(
                            "Code correspondant à la catégorie de restriction"
                            ." d'accès au délai de communicabilité à appliquer"
                        ),
                        'attributes' => $this->attrs('CodeAccessRestrictionType'),
                        'options' => self::getXsdOptions('access_code'),
                        'empty' => true,
                        'data-placeholder' => __("-- Choisir un code --"),
                    ]
                ],
                'StartDate'
            ),
        ];
    }

    /**
     * ArchiveTransfer.Archive.AccessRestrictionRule.StartDate
     * @return array
     */
    private function getStartDate(): array
    {
        return [
            'label' => 'StartDate',
            'cardinality' => '1..1',
            'info' => __("Date de départ du calcul de la règle."),
            'data-type' => 'date',
            'validations' => $this->validations('DateType'),
            'class' => 'datepicker',
            'placeholder' => $this->datePlaceholder(),
        ];
    }


    /**
     * ArchiveTransfer.Archive.AppraisalRule
     * @return array
     */
    private function getAppraisalRule(): array
    {
        $durations = [];
        for ($i = 0; $i <= 100; $i++) {
            $durations['P'.$i.'Y'] = __n("{0} an", "{0} ans", $i, $i);
        }
        return [
            'text' => "AppraisalRule",
            'cardinality' => '0..1',
            'li_attr' => [
                'title' => __("Règle relative au cycle de vie du contenu d'information."),
            ],
            'children' => $this->list(
                [
                    'Code' => [
                        'label' => 'Code : '.__("Code correspondant au sort final à appliquer."),
                        'cardinality' => '1..1',
                        'attributes' => $this->attrs('AppraisalCodeType'),
                        'options' => self::getXsdOptions('appraisal_code'),
                        'empty' => true,
                        'data-placeholder' => __("-- Choisir un sort final --"),
                    ],
                    'Duration' => [
                        'label' => 'Duration : '.__("Durée d'utilité administrative."),
                        'cardinality' => '1..1',
                        'options' => $durations,
                        'empty' => true,
                        'data-placeholder' => __("-- Sélectionner une DUA -- "),
                    ]
                ],
                'StartDate'
            ),
        ];
    }

    /**
     * ArchiveTransfer.Archive.ArchiveObject
     * @return array
     */
    private function getArchiveObject(): array
    {
        $contentDescription = $this->getContentDescription();
        $contentDescription['cardinality'] = '0..1';
        return [
            'text' => "ArchiveObject",
            'text_appendChildValue' => '{{Name}}',
            'cardinality' => '0..n',
            'li_attr' => [
                'title' => __("Objet correspondant à une subdivision de l'<Archive>."),
            ],
            'children' => $this->list(
                'ArchivalAgencyObjectIdentifier',
                [
                    'Name' => [
                        'label' => "Name : ".__("Intitulé de l'<{0}>.", 'ArchiveObject'),
                        'cardinality' => '1..1',
                        'attributes' => $this->attrs('TextType'),
                    ],
                ],
                'OriginatingAgencyObjectIdentifier',
                'TransferringAgencyObjectIdentifier',
                [
                    'ContentDescription' => $contentDescription,
                    'AccessRestrictionRule' => [
                        'text' => 'AccessRestrictionRule',
                        'cardinality' => '0..1',
                        'li_attr' => [
                            'title' => __(
                                "Règle à appliquer en matière d'accès aux"
                                ." informations de pérennisation (communicabilité)"
                            )
                        ]
                    ] + $this->getAccessRestrictionRule(),
                ],
                'AppraisalRule',
                'ArchiveObject',
                'Document'
            ),
        ];
    }

    /**
     * ArchiveTransfer.Archive.ArchiveObject.OriginatingAgencyObjectIdentifier
     * @return array
     */
    private function getOriginatingAgencyObjectIdentifier(): array
    {
        return [
            'label' => "OriginatingAgencyObjectIdentifier : "
                .__("Identifiant attribué à l'<ArchiveObject> par le service producteur."),
            'cardinality' => '0..1',
            'attributes' => $this->attrs('ArchivesIDType'),
        ];
    }

    /**
     * ArchiveTransfer.Archive.ArchiveObject.TransferringAgencyObjectIdentifier
     * @return array
     */
    private function getTransferringAgencyObjectIdentifier(): array
    {
        return [
            'label' => "TransferringAgencyObjectIdentifier : "
                .__("Identifiant attribué à l'<ArchiveObject> par le service versant."),
            'cardinality' => '0..1',
            'attributes' => $this->attrs('ArchivesIDType'),
        ];
    }

    /**
     * ArchiveTransfer.Archive.ContentDescription.CustodialHistory
     * @return array
     */
    private function getCustodialHistory(): array
    {
        return [
            'text' => "CustodialHistory",
            'cardinality' => '0..1',
            'li_attr' => [
                'title' => __(
                    "Énumère les changements successifs de propriété, de "
                    ."responsabilité et de conservation des objets-données avant "
                    ."leur entrée dans le lieu de conservation. On peut indiquer "
                    ."notamment comment s'est effectué le passage de l'application"
                    ." d'origine au fichier archivable.."
                ),
            ],
            'children' => $this->list(
                'CustodialHistoryItem'
            ),
        ];
    }

    /**
     * ArchiveTransfer.Archive.ContentDescription.CustodialHistory.CustodialHistoryItem
     * @return array
     */
    private function getCustodialHistoryItem(): array
    {
        return [
            'label' => "CustodialHistoryItem",
            'cardinality' => '1..n',
            'info' => __("Description d'une période ou d'un événement dans l'historique."),
            'attributes' => $this->attrs('TextType') + [
                'when' => [
                    'data-type' => 'date',
                    'validations' => $this->validations('DateType'),
                    'class' => 'datepicker',
                    'placeholder' => $this->datePlaceholder(),
                ]
            ],
            'type' => 'textarea',
        ];
    }

    /**
     * ArchiveTransfer.Archive.ContentDescription.Keyword
     * @return array
     */
    private function getKeyword(): array
    {
        return [
            'text' => "Keyword",
            'cardinality' => '0..n',
            'text_appendChildValue' => '{{KeywordContent}}',
            'children' => $this->list(
                [
                    'keyword' => [
                        'label' => __("Rechercher un mot clé"),
                        'options' => [],
                        'empty' => true,
                        'data-placeholder' => __("-- Sélectionner un mot clé -- "),
                        'chosen' => false,
                        'afterload' => 'ajaxSearchKeyword',
                        'help' => '<button type="button" onclick="loadKeywordData(this)" class="btn btn-success">'
                            .'<i class="fa fa-check-square-o" aria-hidden="true"></i> '
                            .__("Sélectionner")
                            .'</button>'
                    ],
                ],
                'KeywordContent',
                'KeywordReference',
                'KeywordType',
                [
                    'AccessRestrictionRule' => [
                        'text' => 'AccessRestrictionRule',
                        'cardinality' => '0..1',
                        'li_attr' => [
                            'title' => __(
                                "Règle à appliquer en matière d'accès au mot-clé (communicabilité)."
                                ." \nPermet d'indiquer son caractère confidentiel."
                            )
                        ]
                    ] + $this->getAccessRestrictionRule(),
                ]
            ),
        ];
    }

    /**
     * ArchiveTransfer.Archive.ContentDescription.Keyword.KeywordContent
     * @return array
     */
    private function getKeywordContent()
    {
        return [
            'label' => 'KeywordContent',
            'cardinality' => '1..1',
            'attributes' => $this->attrs('TextType') + [
                'role' => [
                    'help' => __("Rôle du mot-clé.")
                ]
            ],
            'class' => 'keyword-name',
        ];
    }

    /**
     * ArchiveTransfer.Archive.ContentDescription.Keyword.KeywordReference
     * @return array
     */
    private function getKeywordReference()
    {
        return [
            'label' => 'KeywordReference',
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'attributes' => Hash::merge(
                $this->attrs('ArchivesIDType'),
                [
                    'schemeID' => ['class' => 'keyword-list-id'],
                    'schemeName' => ['class' => 'keyword-list-name'],
                    'schemeVersionID' => ['class' => 'keyword-list-version'],
                ]
            ),
            'info' => __(
                "Identifiant du mot clé dans un référentiel, par exemple "
                ."pour un lieu son Code Officiel Géographique selon l'INSEE."
            ),
            'class' => 'keyword-code',
        ];
    }

    /**
     * ArchiveTransfer.Archive.ContentDescription.Keyword.KeywordType
     * @return array
     */
    private function getKeywordType()
    {
        return [
            'label' => 'KeywordType : '.__("Type de mot clé."),
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'attributes' => $this->attrs('CodeKeywordType'),
            'options' => self::getXsdOptions('keywordtype'),
            'empty' => true,
            'data-placeholder' => __("-- Sélectionner un type -- "),
        ];
    }

    /**
     * ArchiveTransfer.Archive.ContentDescription.OriginatingAgency
     * @return array
     */
    private function getOriginatingAgency(): array
    {
        return [
            'text' => "OriginatingAgency",
            'text_appendChildValue' => '{{Identification}}',
            'cardinality' => '0..n',
            'li_attr' => [
                'title' => __(
                    "Service producteur (\"Personne physique ou morale, publique"
                    ." ou privée, qui a produit, reçu et conservé des archives dans "
                    ."l'exercice de son activité\", Dictionnaire de terminologie "
                    ."archivistique, direction des archives de France, 2002)."
                ),
            ],
            'children' => $this->typeOrganizationType('OriginatingAgency'),
        ];
    }

    /**
     * ArchiveTransfer.Archive.Document
     * @return array
     */
    private function getDocument(): array
    {
        return [
            'text' => "Document",
            'text_appendChildValue' => '{{Attachment.filename}}',
            'cardinality' => '0..n',
            'icon' => [
                'complete' => 'fa fa-file-o',
                'incomplete' => 'fa fa-warning text-danger',
                'fields' => ['Attachment.filename', 'Type'],
            ],
            'li_attr' => [
                'title' => __(
                    "Ensemble de données relatives à une pièce physique ou"
                    ." électronique qui fournit des informations ou des preuves."
                )
            ],
            'children' => $this->list(
                'ArchivalAgencyDocumentIdentifier',
                'Attachment',
                'Control',
                'Copy',
                'Creation',
                [
                    'Description' => [
                        'label' => "Description : ".__("Description du document."),
                        'cardinality' => '0..1',
                        'help' => __("Optionnel"),
                        'attributes' => $this->attrs('TextType'),
                        'type' => 'textarea',
                        'data-type' => 'text',
                    ],
                    'Integrity' => [
                        'label' => "Integrity",
                        'disabled' => true,
                    ]
                ],
                'Issue',
                'Language',
                'OriginatingAgencyDocumentIdentifier',
                'Purpose',
                'Receipt',
                'Response',
                'Size',
                'Status',
                'Submission',
                'TransferringAgencyDocumentIdentifier',
                'Type',
                'OtherMetadata',
                'RelatedData',
                [
                    'custom' => [
                        'type' => 'hidden',
                        'default' => json_encode(
                            [
                                'action' => 'multiAction',
                                'args' => [
                                    [
                                        'action' => 'unsetValue',
                                        'args' => ['Attachment']
                                    ],
                                    [
                                        'action' => 'addFileIntegrity',
                                    ],
                                ]
                            ]
                        )
                    ]
                ]
            )
        ];
    }

    /**
     * ArchiveTransfer.Archive.Document.Attachment
     * @return array
     */
    private function getAttachment(): array
    {
        return [
            'label' => "Attachment : ".__("Pièce jointe ou annexée"),
            'cardinality' => '1..1',
            'info' => __(
                "Pièce jointe ou annexée. Dans une période transitoire, "
                ."le même format peut aussi servir à indiquer une pièce \"papier\" et sa localisation."
            ),
            'attributes' => $this->attrs('ArchivesBinaryObjectType'),
            'class' => 'attachments-files-select',
            'onchange' => 'loadAttachment(this)',
            'afterload' => 'initializeAttachment',
            'chosen' => false,
            'options' => [],
            'data-placeholder' => __("-- Sélectionner un fichier --"),
            'allowEmpty' => true, // force la validation à se désactiver car la valeur est set uniquement pour le js
        ];
    }

    /**
     * ArchiveTransfer.Archive.Document.Control
     * @return array
     */
    private function getControl(): array
    {
        return [
            'label' => "Control : ".__("Indique si des exigences de contrôle portent sur le document."),
            'cardinality' => '0..n',
            'help' => __("Optionnel"),
            'attributes' => $this->attrs('ArchivesCodeType')
        ];
    }

    /**
     * ArchiveTransfer.Archive.Document.Copy
     * @return array
     */
    private function getCopy(): array
    {
        return [
            'label' => "Copy : ".__("Indique s'il s'agit d'un original ou d'une copie."),
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'options' => [
                'false' => 'false - '.__("Non"),
                'true' => 'true - '.__("Oui"),
            ],
            'data-placeholder' => __("-- Est une copie --"),
        ];
    }

    /**
     * ArchiveTransfer.Archive.Document.Creation
     * @return array
     */
    private function getCreation(): array
    {
        return [
            'label' => "Creation : ".__("Date de création."),
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'data-type' => 'datetime',
            'validations' => $this->validations('DateTimeType'),
            'class' => 'datepicker',
            'placeholder' => $this->datetimePlaceholder(),
        ];
    }

    /**
     * ArchiveTransfer.Archive.Document.Issue
     * @return array
     */
    private function getIssue(): array
    {
        return [
            'label' => "Issue : ".__("Date d'émission du document."),
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'data-type' => 'datetime',
            'validations' => $this->validations('DateTimeType'),
            'class' => 'datepicker',
            'placeholder' => $this->datetimePlaceholder(),
        ];
    }

    /**
     * ArchiveTransfer.Archive.Document.Language
     * @return array
     */
    private function getLanguage(): array
    {
        return [
            'label' => "Language : ".__("Langue du contenu de l'objet-données."),
            'cardinality' => '0..n',
            'help' => __("Optionnel"),
            'attributes' => $this->attrs('CodeLanguageType'),
            'options' => $this->optionsLang(),
            'data-placeholder' => __("-- Sélectionner une langue --"),
            'empty' => true,
        ];
    }

    /**
     * ArchiveTransfer.Archive.Document.OriginatingAgencyDocumentIdentifier
     * @return array
     */
    private function getOriginatingAgencyDocumentIdentifier(): array
    {
        return [
            'label' => "OriginatingAgencyDocumentIdentifier : "
                .__("Identifiant attribué au <Document> par le service producteur."),
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'attributes' => $this->attrs('ArchivesIDType'),
        ];
    }

    /**
     * ArchiveTransfer.Archive.Document.Purpose
     * @return array
     */
    private function getPurpose(): array
    {
        return [
            'label' => "Purpose : ".__("Objet ou objectif du document."),
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'attributes' => $this->attrs('TextType'),
            'type' => 'textarea',
            'data-type' => 'text',
        ];
    }

    /**
     * ArchiveTransfer.Archive.Document.Receipt
     * @return array
     */
    private function getReceipt(): array
    {
        return [
            'label' => "Receipt : ".__("Date de réception de l'objet-données"),
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'data-type' => 'datetime',
            'validations' => $this->validations('DateTimeType'),
            'class' => 'datepicker',
            'placeholder' => $this->datetimePlaceholder(),
        ];
    }

    /**
     * ArchiveTransfer.Archive.Document.Response
     * @return array
     */
    private function getResponse(): array
    {
        return [
            'label' => "Response : ".__("Date de réponse de l'objet-données."),
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'data-type' => 'datetime',
            'validations' => $this->validations('DateTimeType'),
            'class' => 'datepicker',
            'placeholder' => $this->datetimePlaceholder(),
        ];
    }

    /**
     * ArchiveTransfer.Archive.Document.Status
     * @return array
     */
    private function getStatus(): array
    {
        return [
            'label' => "Status",
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'attributes' => $this->attrs('ArchivesCodeType'),
            'info' => __(
                "Etat de l'objet-données (par rapport avec son cycle de vie)."
                ." Permet par exemple d'indiquer si la signature du contenu binaire "
                ."ou fichier a été vérifiée avant transfert aux archives."
            )
        ];
    }

    /**
     * ArchiveTransfer.Archive.Document.Submission
     * @return array
     */
    private function getSubmission(): array
    {
        return [
            'label' => "Submission",
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'data-type' => 'datetime',
            'validations' => $this->validations('DateTimeType'),
            'class' => 'datepicker',
            'info' => __("Date de soumission de l'objet-données par un émetteur à un destinataire."),
            'placeholder' => $this->datetimePlaceholder(),
        ];
    }

    /**
     * ArchiveTransfer.Archive.Document.Type
     * @return array
     */
    private function getType(): array
    {
        return [
            'label' => "Type",
            'cardinality' => '1..n',
            'options' => self::getXsdOptions('type'),
            'data-placeholder' => __("-- Choisir un type --"),
            'default' => 'CDO',
            'empty' => true,
            'attributes' => $this->attrs('DocumentTypeCodeType'),
        ];
    }

    /**
     * ArchiveTransfer.Archive.Document.TransferringAgencyDocumentIdentifier
     * @return array
     */
    private function getTransferringAgencyDocumentIdentifier(): array
    {
        return [
            'label' => "TransferringAgencyDocumentIdentifier",
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'attributes' => $this->attrs('ArchivesIDType'),
            'info' => __("Identifiant attribué au <Document> par le service versant.")
        ];
    }
}
