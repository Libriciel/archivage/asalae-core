<?php
/**
 * AsalaeCore\Form\MessageSchema\Seda02Schema
 *
 * @noinspection PhpUnusedPrivateMethodInspection
 */

namespace AsalaeCore\Form\MessageSchema;

use Cake\Cache\Cache;

/**
 * Schema d'un message seda v0.2
 *
 * @category Form\MessageSchema
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Seda02Schema implements MessageFormSchemaInterface
{
    /**
     * Traits
     */
    use SchemaTrait;

    /**
     * Contenu de l'objet
     * @param string $context
     * @return array
     */
    public function data(string $context): array
    {
        switch ($context) {
            case 'Archive':
                return [
                    'Archive' => [
                        'cardinality' => '1..1',
                    ] + $this->getFirstContains()
                ];
            case 'ArchiveTransfer':
            default:
                return $this->getArchiveTransfer();
        }
    }

    /**
     * Donne le nom de l'élément à partir d'un nom généric
     * ex: Archive -> Contains pour du seda 0.2
     * @param string $genericName
     * @param string $context
     * @return string si false, ne contient pas $genericName
     */
    public static function getTagname(string $genericName, string $context)
    {
        switch ($genericName) {
            /** @noinspection PhpMissingBreakStatementInspection */
            case 'Archive':
                if ($context === 'Archive') {
                    return 'Archive';
                }
                // fallthrough no-break
            case 'ArchiveObject':
                return 'Contains';
            case 'ArchivalAgencyDocumentIdentifier':
                return 'Identification';
            case 'Keyword':
                return 'ContentDescriptive';
            default:
                return $genericName;
        }
    }

    /**
     * Donne le fichier xsd de liste de codes
     * @param string $keypath
     * @return string
     */
    public static function getXsdCodePath(string $keypath): string
    {
        return SEDA_V02_CODES.DS.SEDA_V02_CODES_FILES[$keypath];
    }

    /**
     * Donne le 1er niveau de Contains (
     * @return array
     */
    private function getFirstContains(): array
    {
        return [
            'text' => "Contains (Archive)",
            'element' => 'Contains',
            'text_appendChildValue' => '{{Name}}',
            'cardinality' => '1..n',
            'icon' => [
                'complete' => 'fa fa-archive',
                'incomplete' => '',
                'fields' => [],
            ],
            'children' => $this->list(
                'ArchivalAgencyArchiveIdentifier',
                'ArchivalAgreement',
                'ArchivalProfile',
                'DescriptionLanguage',
                'DescriptionLevel',
                'Name',
                'ServiceLevel',
                'TransferringAgencyArchiveIdentifier',
                'ContentDescription',
                'Appraisal',
                'AccessRestriction',
                'Document',
                'Contains'
            )
        ];
    }

    /**
     * ArchiveTransfer
     * @return array
     */
    private function getArchiveTransfer()
    {
        return [
            'ArchiveTransfer' => [
                'text' => __("Transfert d'archives"),
                'element' => 'ArchiveTransfer',
                'cardinality' => '1..1',
                'li_attr' => [
                    'title' => __("Transfert d'archives."),
                ],
                'icon' => [
                    'complete' => 'fa fa-check text-success',
                    'incomplete' => 'fa fa-exclamation-circle text-danger',
                    'fields' => ['Date', 'TransferIdentifier', 'TransferringAgency', 'ArchivalAgency', 'Contains'],
                ],
                'children' => $this->list(
                    'Comment',
                    'Date',
                    'TransferIdentifier',
                    'TransferringAgency',
                    'ArchivalAgency',
                    'Integrity',
                    [
                        'Contains' => $this->getFirstContains(),
                    ]
                )
            ]
        ];
    }


    /**
     * Donne les attributs pour un type donné
     * @param string $string
     * @return array
     */
    private function attrs(string $string): array
    {
        switch ($string) {
            case 'TextType':
                return [
                    'languageID' => [],
                ];
            case 'ArchivesIDType':
                return [
                    'schemeID' => ['help' => __('Identification Scheme. Identifier')],
                    'schemeName' => ['help' => __('Identification Scheme. Name. Text')],
                    'schemeAgencyName' => ['help' => __('Identification Scheme. Agency Name. Text')],
                    'schemeVersionID' => ['help' => __('Identification Scheme. Version. Identifier')],
                    'schemeDataURI' => ['help' => __('Identification Scheme Data. Uniform Resource. Identifier')],
                    'schemeURI' => ['help' => __('Identification Scheme. Uniform Resource. Identifier')],
                ];
            case 'ArchivesCodeType':
                return [
                    'listID' => ['help' => __("The identification of a list of codes.")],
                    'listAgencyName' => ['help' => __("The name of the agency that maintains the code list.")],
                    'listName' => ['help' => __("The name of a list of codes.")],
                    'listVersionID' => ['help' => __("The version of the code list.")],
                    'name' => ['help' => __("The textual equivalent of the code content.")],
                    'languageID' => [
                        'help' => __("The identifier of the language used in the corresponding text string.")
                    ],
                    'listURI' => [
                        'help' => __("The Uniform Resource Identifier that identifies where the code list is located.")
                    ],
                    'listSchemeURI' => [
                        'help' => __(
                            "The Uniform Resource Identifier that identifies where the code list scheme is located."
                        )
                    ],
                ];
            case 'MeasureType':
                return [
                    'unitCode' => [
                        'help' => __("Measure. Unit. Code"),
                        'options' => self::getXsdOptions('unit'),
                        'data-placeholder' => __("-- Choisir un unitCode --"),
                        'empty' => true,
                    ],
                ];
            case 'CodeDescriptionLevelType':
            case 'CodeLanguageType':
            case 'DocumentTypeCodeType':
                return [
                    'listVersionID' => [
                        'help' => __("The version of the code list."),
                        'default' => 'edition 2009',
                    ],
                ];
            case 'ArchivesBinaryObjectType':
                $formats = self::getXsdOptions('filetype');
                $mimes = self::getXsdOptions('mime');
                $encoding = self::getXsdOptions(
                    'encoding',
                    'ccts:name'
                );
                $charset = self::getXsdOptions('charset');
                return [
                    'format' => [
                        'help' => __("The format of the binary content."),
                        'options' => $formats,
                        'data-placeholder' => __("-- Choisir un format --"),
                        'empty' => true,
                    ],
                    'mimeCode' => [
                        'help' => __("The mime type of the binary object."),
                        'options' => $mimes,
                        'data-placeholder' => __("-- Choisir un type mime --"),
                        'empty' => true,
                    ],
                    'encodingCode' => [
                        'help' => __("Specifies the decoding algorithm of the binary object."),
                        'options' => $encoding,
                        'data-placeholder' => __("-- Choisir un encodage --"),
                        'empty' => true,
                    ],
                    'characterSetCode' => [
                        'help' => __("The character set of the binary object if the mime type is text."),
                        'options' => $charset,
                        'data-placeholder' => __("-- Choisir un charset --"),
                        'empty' => true,
                    ],
                    'uri' => [
                        'help' => __(
                            "The Uniform Resource Identifier that identifies where the binary object is located."
                        ),
                        'type' => 'hidden',
                    ],
                    'filename' => [
                        'help' => __("The filename of the binary object."),
                        'class' => 'filename',
                        'type' => 'hidden',
                    ],
                ];
            default:
                return [];
        }
    }

    /**
     * ArchiveTransfer.Comment
     * @return array
     */
    private function getComment(): array
    {
        return [
            'label' => "Comment : ".__("Commentaires"),
            'type' => 'textarea',
            'data-type' => 'text',
            'help' => __("Optionnel"),
            'cardinality' => '0..1',
            'attributes' => $this->attrs('TextType'),
        ];
    }

    /**
     * ArchiveTransfer.Date
     * @return array
     */
    private function getDate(): array
    {
        return [
            'label' => "Date : ".__("Date du transfert"),
            'cardinality' => '1..1',
            'data-type' => 'datetime',
            'validations' => $this->validations('DateTimeType'),
            'class' => 'datepicker',
            'placeholder' => $this->datePlaceholder(),
        ];
    }

    /**
     * ArchiveTransfer.TransferIdentifier
     * @return array
     */
    private function getTransferIdentifier(): array
    {
        return [
            'label' => "TransferIdentifier : ".__("Identifiant du transfert"),
            'cardinality' => '1..1',
            'attributes' => $this->attrs('ArchivesIDType'),
            'default' => '#A_CALCULER_LORS_DU_VERROUILLAGE#',
        ];
    }

    /**
     * ArchiveTransfer.TransferRequestReplyIdentifier
     * @return array
     */
    private function getTransferRequestReplyIdentifier(): array
    {
        return [
            'label' => "TransferRequestReplyIdentifier",
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'attributes' => $this->attrs('ArchivesIDType'),
            'info' => __(
                "Identifiant de la réponse à la demande de transfert "
                ."(permet par exemple de rappeler l'accord donné par le service d'archives)"
            ),
        ];
    }

    /**
     * ArchiveTransfer.TransferringAgency
     * @return array
     */
    private function getTransferringAgency(): array
    {
        return [
            'text' => "TransferringAgency",
            'text_appendChildValue' => '{{Identification}}',
            'cardinality' => '1..1',
            'icon' => [
                'complete' => 'fa fa-check-square-o',
                'incomplete' => 'fa fa-square-o',
                'fields' => ['Identification'],
            ],
            'children' => [
                'Description' => ['type' => 'hidden', 'cardinality' => '0..1'],
                'Identification' => [
                    'label' => __("Service versant"),
                    'cardinality' => '1..1',
                    'options' => $this->optionsIdTransferringAgency(),
                    'empty' => true,
                    'data-placeholder' => __("-- Sélectionner un service d'archives --"),
                ],
                'custom' => [
                    'type' => 'hidden',
                    'default' => json_encode(
                        [
                            'action' => 'insertOrgEntityData',
                            'args' => [
                                'Identification',
                                ['description' => 'Description', 'name' => 'Name']
                            ]
                        ]
                    )
                ],
                'Name' => ['type' => 'hidden', 'cardinality' => '0..1'],
            ],
        ];
    }

    /**
     * ArchiveTransfer.ArchivalAgency
     * @return array
     */
    private function getArchivalAgency(): array
    {
        return [
            'text' => "ArchivalAgency",
            'text_appendChildValue' => '{{Identification}}',
            'cardinality' => '1..1',
            'icon' => [
                'complete' => 'fa fa-check-square-o',
                'incomplete' => 'fa fa-square-o',
                'fields' => ['Identification'],
            ],
            'children' => [
                'Description' => ['type' => 'hidden', 'cardinality' => '0..1'],
                'Identification' => [
                    'cardinality' => '1..1',
                    'options' => $this->optionsIdArchivalAgency(),
                    'empty' => true,
                    'data-placeholder' => __("-- Sélectionner un service d'archives --"),
                ],
                'custom' => [
                    'type' => 'hidden',
                    'default' => json_encode(
                        [
                            'action' => 'insertOrgEntityData',
                            'args' => [
                                'Identification',
                                ['description' => 'Description', 'name' => 'Name']
                            ]
                        ]
                    )
                ],
                'Name' => ['type' => 'hidden', 'cardinality' => '0..1'],
            ],
            'options' => $this->optionsIdArchivalAgency(),
            'empty' => true,
            'data-placeholder' => __("-- Sélectionner un service d'archives --"),
        ];
    }

    /**
     * ArchiveTransfer.Integrity
     * @return array
     */
    private function getIntegrity(): array
    {
        return [
            'text' => "Integrity",
            'cardinality' => '0..n',
            'display' => false,
            'children' => [],// HashCodeType
        ];
    }

    /**
     * ArchiveTransfer.NonRepudiation
     * @return array
     */
    private function getNonRepudiation(): array
    {
        return [
            'text' => "NonRepudiation",
            'cardinality' => '0..1',
            'display' => false,
            'children' => [],// SignatureType
        ];
    }

    /**
     * ArchiveTransfer.Contains.Contains
     * @return array
     */
    private function getContains(): array
    {
        $contentDescription = $this->getContentDescription();
        $contentDescription['cardinality'] = '0..1';
        return [
            'text' => "Contains (ArchiveObject)",
            'element' => 'Contains',
            'text_appendChildValue' => '{{Name}}',
            'cardinality' => '0..n',
            'li_attr' => [
                'title' => __("Ensemble constitué d'un contenu d'information et de son information de pérennisation."),
            ],
            'children' => $this->list(
                'ArchivalAgencyObjectIdentifier',
                'DescriptionLevel',
                'Name',
                'TransferringAgencyObjectIdentifier',
                ['ContentDescription' => $contentDescription],
                'Appraisal',
                'AccessRestriction',
                'Document',
                'Contains'
            )
        ];
    }

    /**
     * children de ArchiveType
     * @return array
     */
    public function typeArchiveType(): array
    {
        return $this->list(
            'ArchivalAgencyArchiveIdentifier',
            'ArchivalAgreement',
            'ArchivalProfile',
            'DescriptionLanguage',
            'DescriptionLevel',
            'Name',
            'ServiceLevel',
            'TransferringAgencyArchiveIdentifier',
            'ContentDescription',
            'Appraisal',
            'AccessRestriction',
            'Document',
            'Contains'
        );
    }

    /**
     * Contain.DescriptionLevel
     */
    private function getDescriptionLevel(): array
    {
        return [
            'label' => "DescriptionLevel",
            'cardinality' => '1..1',
            'info' => __(
                "Indique si l'objet décrit est un groupe de documents, "
                ."un sous-groupe de documents, un dossier, ou une pièce."
            ),
            'empty' => true,
            'data-placeholder' => __("-- Sélectionner un DescriptionLevel --"),
            'attributes' => $this->attrs('CodeDescriptionLevelType'),
            'options' => [
                [
                    'value' => 'class',
                    'text' => 'class - '.__("Classe"),
                    'title' => __(
                        "Cette valeur, issue de la tradition archivistique allemande,"
                        ." ne correspond pas pour l'instant aux pratiques archivistiques françaises"
                    ),
                ],
                [
                    'value' => 'collection',
                    'text' => 'collection - '.__("Collection"),
                    'title' => __(
                        "Réunion artificielle de documents en fonction de critères"
                        ." communs liés à leur contenu ou à leur support, sans considération"
                        ." de leur provenance, par opposition au fonds d'archives constitué de façon organique"
                    ),
                ],
                [
                    'value' => 'file',
                    'text' => 'file - '.__("Dossier"),
                    'title' => __(
                        "ensemble de documents regroupés, soit par"
                        ." le producteur pour son usage courant, soit dans le processus"
                        ." du classement d'archives, parce qu'ils concernent un même sujet"
                        ." ou une même affaire; le dossier est ordinairement "
                        ."l'unité de base à l'intérieur d'une série organique"
                    ),
                ],
                [
                    'value' => 'fonds',
                    'text' => 'fonds - '.__("Fonds"),
                    'title' => __(
                        "ensemble de documents quels que soit leur type et "
                        ."leur support, créé ou reçu de manière organique et utilisé"
                        ." par une personne physique ou morale dans l'exercice de ses activités"
                    ),
                ],
                [
                    'value' => 'item',
                    'text' => 'item - '.__("Item"),
                    'title' => __(
                        "plus petite unité documentaire, par exemple une "
                        ."lettre, un mémoire, un rapport, une photographie, un enregistrement sonore"
                    ),
                ],
                [
                    'value' => 'recordgrp',
                    'text' => 'recordgrp - '.__("Groupe de documents"),
                    'title' => __(
                        "niveau de description intermédiaire qui ne correspond"
                        ." pas à une division organique (sous-fonds, série ou sous-série organique);"
                        ." parties au sein d'une collection, versements, épaves d'un fonds,"
                        ." subdivisions de fonds dont on ne connait pas la nature exacte,"
                        ." sous-ensemble classés thématiquement"
                    ),
                ],
                [
                    'value' => 'series',
                    'text' => 'series - '.__("Serie organique"),
                    'title' => __(
                        "division organique d'un fonds, correspondant à un "
                        ."ensemble de dossiers maintenus groupés parce qu'ils résultent"
                        ." d'une même activité, se rapportent à une même fonction"
                        ." ou à un même sujet ou revêtent une même forme"
                    ),
                ],
                [
                    'value' => 'subfonds',
                    'text' => 'subfonds - '.__("Sous fonds"),
                    'title' => __(
                        "division organique d'un fonds correspondant aux "
                        ."divisions administratives de l'institution ou de l'organisme"
                        ." producteur, ou, à défaut, à un regroupement géographique,"
                        ." chronologique, fonctionnel ou autre des documents; quand le"
                        ." producteur a une structure hiérarchique complexe , chaque sous-fonds"
                        ." est lui-même subdivisé, autant que nécessaire pour refléter les niveaux hiérarchiques"
                    ),
                ],
                [
                    'value' => 'subgrp',
                    'text' => 'subgrp - '.__("Sous-groupe de documents"),
                    'title' => __("subdivision du groupe de documents"),
                ],
                [
                    'value' => 'subseries',
                    'text' => 'subseries - '.__("Sous-série organique"),
                    'title' => __("subdivision de la série organique"),
                ],
            ]
        ];
    }

    /**
     * Donne les options pour les codes lang
     * @return array
     */
    private function optionsLang(): array
    {
        $options = Cache::read($key = 'Seda02Schema.options.lang');
        if (empty($options)) {
            Cache::write($key, $options = self::getXsdOptions('lang'));
        }
        return $options;
    }

    /**
     * ArchiveTransfer.Contains.DescriptionLanguage
     * @return array
     */
    private function getDescriptionLanguage(): array
    {
        return [
            'label' => "DescriptionLanguage: ".__("Langue des descriptions"),
            'cardinality' => '1..n',
            'attributes' => $this->attrs('CodeLanguageType'),
            'options' => $this->optionsLang(),
            'data-placeholder' => __("-- Sélectionner une langue --"),
            'empty' => true,
            'default' => 'fr',
        ];
    }

    /**
     * ArchiveTransfer.Contains.Name
     * @return array
     */
    private function getName()
    {
        return [
            'label' => "Name: ".__("Intitulé du contenu d'information."),
            'cardinality' => '1..1',
            'attributes' => [
                'languageID' => [
                    'help' => __("The identifier of the language used in the corresponding text string.")
                ]
            ],
        ];
    }

    /**
     * ArchiveTransfer.Contains.ArchivalAgreement
     * @return array
     */
    private function getArchivalAgreement()
    {
        return [
            'label' => "ArchivalAgreement: ".__("Indique la convention d'archivage à appliquer pour l'archive."),
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'attributes' => $this->attrs('ArchivesIDType'),
            'options' => $this->optionsArchivalAgreement(),
            'empty' => true,
            'data-placeholder' => __("-- Choisir un accord de versement --"),
        ];
    }

    /**
     * ArchiveTransfer.Contains.ArchivalProfile
     * @return array
     */
    private function getArchivalProfile()
    {
        return [
            'label' => "ArchivalProfile",
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'info' => __(
                "Indique la méthodologie de production de l'archive appliquée"
                ." (structure adaptée au domaine particulier traité, par exemple"
                ." archivage d'un dossier de marché public)."
            ),
            'attributes' => $this->attrs('ArchivesIDType'),
            'options' => $this->optionsArchivalProfile(),
            'empty' => true,
            'data-placeholder' => __("-- Choisir un profil d'archives --"),
        ];
    }

    /**
     * ArchiveTransfer.Contains.ServiceLevel
     * @return array
     */
    private function getServiceLevel()
    {
        return [
            'label' => "ServiceLevel",
            'cardinality' => '0..n',
            'help' => __("Optionnel"),
            'info' => __(
                "Niveau de service demandé (disponibilité, sécurité...), "
                ."en référence aux différents niveaux prévus par le contrat ou la"
                ." convention passée entre le service versant et le service d'archives."
            ),
            'attributes' => $this->attrs('ArchivesCodeType'),
            'options' => $this->optionsServiceLevels(),
            'empty' => true,
            'data-placeholder' => __("-- Choisir un niveau de service --"),
        ];
    }

    /**
     * ArchiveTransfer.Contains.TransferringAgencyArchiveIdentifier
     * @return array
     */
    private function getTransferringAgencyArchiveIdentifier()
    {
        return [
            'label' => "TransferringAgencyArchiveIdentifier",
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'info' => __(
                "Identifiant d'archive fourni par le service versant. "
                ."L'identifiant pour l'émetteur du message est recommandé, si ce n'est obligatoire.."
            ),
            'attributes' => $this->attrs('ArchivesIDType'),
        ];
    }

    /**
     * ArchiveTransfer.Contains.ContentDescription
     * @return array
     */
    private function getContentDescription(): array
    {
        return [
            'text' => "ContentDescription",
            'cardinality' => '1..1',
            'li_attr' => [
                'title' => __("Informations sur le contenu de l'archive ou l'objet."),
            ],
            'action' => 'add-multiple-keyword',
            'children' => $this->list(
                [
                    'CustodialHistory' => $this->getCustodialHistory(),
                    'Description' => [
                        'label' => "Description",
                        'cardinality' => '0..1',
                        'help' => __("Optionnel"),
                        'info' => __(
                            "Permet de donner des précisions sur le contenu "
                            ."de l'objet. Il permet aussi de donner des précisions "
                            ."réservées aux professionnels et auxquelles le public ne doit pas avoir accès."
                        ),
                        'attributes' => $this->attrs('TextType'),
                        'type' => 'textarea',
                    ],
                ],
                'FilePlanPosition',
                'Format',
                'Language',
                'LatestDate',
                'OldestDate',
                'OtherDescriptiveData',
                'RelatedObjectReference',
                'Size',
                [
                    'OriginatingAgency' => [
                        'text' => "OriginatingAgency",
                        'cardinality' => '0..n',
                        'help' => __("Optionnel"),
                        'children' => [
                            'Description' => ['type' => 'hidden', 'cardinality' => '0..1'],
                            'Identification' => [
                                'label' => __("Service producteur"),
                                'cardinality' => '1..1',
                                'info' => __(
                                    "Identifiant unique de l'organisation. UN00000053"
                                    ." Organisation.Identification.Identifier.Par exemple dans"
                                    ." le Répertoire SIRENE: SIREN ou SIRET suivant le niveau."
                                ),
                                'options' => $this->optionsIdOriginatingAgency(),
                                'empty' => true,
                                'data-placeholder' => __("-- Sélectionner un producteur --"),
                            ],
                            'custom' => [
                                'type' => 'hidden',
                                'default' => json_encode(
                                    [
                                        'action' => 'insertOrgEntityData',
                                        'args' => [
                                            'Identification',
                                            ['description' => 'Description', 'name' => 'Name']
                                        ]
                                    ]
                                )
                            ],
                            'Name' => ['type' => 'hidden', 'cardinality' => '0..1'],
                        ],
                    ]
                ],
                'Repository',
                'ContentDescriptive',
                'AccessRestriction',
                'OtherMetadata'
            ),
        ];
    }

    /**
     * ArchiveTransfer.Contains.ContentDescription.CustodialHistory
     * @return array
     */
    private function getCustodialHistory(): array
    {
        return [
            'label' => "CustodialHistory",
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'info' => __(
                "Enumère les changements successifs de propriété, de responsabilité"
                ." et de conservation de l'objet avant son entrée dans le lieu de conservation."
                ." On peut indiquer notamment comment s'est effectué le passage de"
                ." l'application d'origine au fichier archivable."
            ),
            'attributes' => $this->attrs('TextType'),
            'type' => 'textarea',
        ];
    }

    /**
     * ArchiveTransfer.Contains.ContentDescription.FilePlanPosition
     * @return array
     */
    private function getFilePlanPosition(): array
    {
        return [
            'label' => "FilePlanPosition",
            'cardinality' => '0..n',
            'help' => __("Optionnel"),
            'info' => __(
                "Classement de l'objet transféré dans le (ou les) plan(s) de classement du (ou des) producteur."
            ),
            'attributes' => $this->attrs('ArchivesIDType'),
        ];
    }

    /**
     * ArchiveTransfer.Contains.ContentDescription.Format
     * @return array
     */
    private function getFormat(): array
    {
        return [
            'label' => "Format",
            'cardinality' => '0..n',
            'help' => __("Optionnel"),
            'info' => __(
                "Permet d'indiquer d'autres formats respectés par l'objet"
                ." que ceux qui sont mentionnés dans les éléments BinaryObject "
                ."(par exemple: le fichier pdf contient du texte avec des balises XML)."
            ),
            'attributes' => $this->attrs('TextType'),
        ];
    }

    /**
     * ArchiveTransfer.Contains.ContentDescription.Language
     * @return array
     */
    private function getLanguage(): array
    {
        return [
            'label' => "Language : ".__("Langue du contenu de l'objet."),
            'cardinality' => '1..n',
            'attributes' => $this->attrs('CodeLanguageType'),
            'options' => $this->optionsLang(),
            'data-placeholder' => __("-- Sélectionner une langue --"),
            'empty' => true,
        ];
    }

    /**
     * ArchiveTransfer.Contains.ContentDescription.LatestDate
     * @return array
     */
    private function getLatestDate(): array
    {
        return [
            'label' => "LatestDate : ".__("Date de fin du contenu."),
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'data-type' => 'date',
            'validations' => $this->validations('DateType'),
            'class' => 'datepicker',
            'data-validationtype' => 'latestdate',
            'placeholder' => $this->datePlaceholder(),
        ];
    }

    /**
     * ArchiveTransfer.Contains.ContentDescription.OldestDate
     * @return array
     */
    private function getOldestDate(): array
    {
        return [
            'label' => "OldestDate : ".__("Date et heure de début du contenu d'information."),
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'data-type' => 'date',
            'validations' => $this->validations('DateType'),
            'class' => 'datepicker',
            'data-validationtype' => 'oldestdate',
            'placeholder' => $this->datePlaceholder(),
        ];
    }

    /**
     * ArchiveTransfer.Contains.ContentDescription.OtherDescriptiveData
     * @return array
     */
    private function getOtherDescriptiveData(): array
    {
        return [
            'label' => "OtherDescriptiveData : ".__("Autres informations sur l'objet."),
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'attributes' => $this->attrs('TextType'),
            'type' => 'textarea',
        ];
    }

    /**
     * ArchiveTransfer.Contains.ContentDescription.RelatedObjectReference
     * @return array
     */
    private function getRelatedObjectReference(): array
    {
        return [
            'label' => "RelatedObjectReference",
            'cardinality' => '0..n',
            'help' => __("Optionnel"),
            'attributes' => $this->attrs('ArchivesIDType'),
            'info' => __("Permet d'indiquer la référence d'un autre objet et la nature du lien avec ce dernier."),
        ];
    }

    /**
     * ArchiveTransfer.Contains.ContentDescription.ContentDescriptive
     * @return array
     */
    private function getContentDescriptive(): array
    {
        return [
            'text' => "ContentDescriptive",
            'text_appendChildValue' => '{{KeywordContent}}',
            'cardinality' => '0..n',
            'help' => __("Optionnel"),
            'children' => $this->list(
                [
                    'keyword' => [
                        'label' => __("Rechercher un mot clé"),
                        'options' => [],
                        'empty' => true,
                        'data-placeholder' => __("-- Sélectionner un mot clé -- "),
                        'chosen' => false,
                        'afterload' => 'ajaxSearchKeyword',
                        'help' => '<button type="button" onclick="loadKeywordData(this)" class="btn btn-success">'
                            .'<i class="fa fa-check-square-o" aria-hidden="true"></i> '
                            .__("Sélectionner")
                            .'</button>'
                    ],
                ],
                'AccessRestriction',
                'KeywordContent',
                'KeywordReference',
                'KeywordType'
            )
        ];
    }

    /**
     * ArchiveTransfer.Contains.ContentDescription.ContentDescriptive.KeywordContent
     * @return array
     */
    private function getKeywordContent()
    {
        return [
            'label' => 'KeywordContent',
            'cardinality' => '1..1',
            'attributes' => $this->attrs('TextType'),
            'class' => 'keyword-name',
        ];
    }

    /**
     * ArchiveTransfer.Contains.ContentDescription.ContentDescriptive.KeywordReference
     * @return array
     */
    private function getKeywordReference()
    {
        return [
            'label' => 'KeywordReference',
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'attributes' => $this->attrs('ArchivesIDType'),
            'info' => __(
                "Indique, s'il en a, l'identifiant du mot clé dans une liste"
                ." déposée, par exemple pour un lieu son Code Officiel Géographique selon l'INSEE."
            ),
            'class' => 'keyword-code',
        ];
    }

    /**
     * ArchiveTransfer.Contains.ContentDescription.ContentDescriptive.KeywordType
     * @return array
     */
    private function getKeywordType()
    {
        return [
            'label' => 'KeywordType : '.__("Type de mot clé."),
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'attributes' => $this->attrs('CodeKeywordType'),
            'options' => self::getXsdOptions('keywordtype'),
            'empty' => true,
            'data-placeholder' => __("-- Sélectionner un type -- "),
        ];
    }

    /**
     * ArchiveTransfer.Contains.ContentDescription.Size
     * @return array
     */
    private function getSize(): array
    {
        return [
            'label' => "Size : ".__("Taille de l'objet en octets, nombre d'entregistrements..."),
            'cardinality' => '0..n',
            'help' => __("Optionnel"),
            'attributes' => $this->attrs('MeasureType'),
        ];
    }

    /**
     * ArchiveTransfer.Contains.Appraisal
     * @return array
     */
    private function getAppraisal(): array
    {
        return [
            'text' => "Appraisal",
            'text_appendChildValue' => '{{Code}} - {{Duration}}',
            'cardinality' => '0..1',
            'children' => $this->list(
                'Code',
                'Duration',
                'StartDate'
            ),
            'icon' => [
                'complete' => 'fa fa-check-square-o',
                'incomplete' => 'fa fa-square-o',
                'fields' => ['Code', 'Duration', 'StartDate'],
            ]
        ];
    }

    /**
     * ArchiveTransfer.Contains.Appraisal.Code
     * @return array
     */
    private function getCode(): array
    {
        return [
            'label' => "Code : ".__("Indique la règle à appliquer"),
            'cardinality' => '1..1',
            'attributes' => $this->attrs('CodeLanguageType'),
            'options' => [
                'conserver' => __("Conserver"),
                'detruire' => __("Détruire"),
            ]
        ];
    }

    /**
     * ArchiveTransfer.Contains.Appraisal.Duration
     * @return array
     */
    private function getDuration(): array
    {
        $options = [];
        for ($i = 0; $i <= 100; $i++) {
            $options['P'.$i.'Y'] = __n("{0} an", "{0} ans", $i, $i);
        }
        return [
            'label' => "Duration : ".__("Indique la durée à appliquer"),
            'cardinality' => '1..1',
            'validations' => $this->validations('duration'),
            'empty' => true,
            'options' => $options,
            'data-placeholder' => __("-- Sélectionner une durée --"),
        ];
    }

    /**
     * ArchiveTransfer.Contains.Appraisal.StartDate
     * @return array
     */
    private function getStartDate(): array
    {
        return [
            'label' => "StartDate : ".__("Date de départ du calcul"),
            'cardinality' => '1..1',
            'data-type' => 'date',
            'validations' => $this->validations('DateType'),
            'class' => 'datepicker',
            'placeholder' => $this->datePlaceholder(),
        ];
    }

    /**
     * ArchiveTransfer.Contains.AccessRestriction
     * @return array
     */
    private function getAccessRestriction(): array
    {
        return [
            'text' => "AccessRestriction",
            'text_appendChildValue' => '{{Code}}',
            'cardinality' => '0..1',
            'children' => [
                'Code' => [
                    'label' => "Code : ".__("Indique la règle à appliquer"),
                    'cardinality' => '1..1',
                    'attributes' => $this->attrs('CodeLanguageType'),
                    'options' => self::getXsdOptions('access_code'),
                    'empty' => __("-- Choisir une règle --"),
                ],
                'StartDate' => $this->getStartDate(),
            ],
            'icon' => [
                'complete' => 'fa fa-check-square-o',
                'incomplete' => 'fa fa-square-o',
                'fields' => ['Code', 'StartDate'],
            ]
        ];
    }

    /**
     * ArchiveTransfer.Contains.Document
     * @return array
     */
    private function getDocument(): array
    {
        return [
            'text' => "Document",
            'text_appendChildValue' => '{{Attachment.filename}}',
            'cardinality' => '0..n',
            'delete' => [
                'action' => 'deleteDocument',
            ],
            'icon' => [
                'complete' => 'fa fa-file-o',
                'incomplete' => 'fa fa-warning text-danger',
                'fields' => ['Attachment.filename', 'Type'],
            ],
            'li_attr' => [
                'title' => __(
                    "Ensemble de données relatives à une pièce physique ou"
                    ." électronique qui fournit des informations ou des preuves."
                )
            ],
            'children' => $this->list(
                'Attachment',
                'Control',
                'Copy',
                'Creation',
                'Description',
                'Identification',
                'Issue',
                'ItemIdentifier',
                'Purpose',
                'Receipt',
                'Response',
                'Status',
                'Submission',
                'Type',
                'OtherMetadata',
                [
                    'custom' => [
                        'type' => 'hidden',
                        'default' => json_encode(
                            [
                                'action' => 'multiAction',
                                'args' => [
                                    [
                                        'action' => 'unsetValue',
                                        'args' => ['Attachment']
                                    ],
                                    [
                                        'action' => 'addFileHashCode',
                                    ],
                                ]
                            ]
                        )
                    ]
                ]
            )
        ];
    }

    /**
     * ArchiveTransfer.Contains.Document.Attachment
     * @return array
     */
    private function getAttachment(): array
    {
        return [
            'label' => "Attachment : ".__("Pièce jointe ou annexée"),
            'cardinality' => '1..1',
            'info' => __(
                "Pièce jointe ou annexée. Dans une période transitoire, "
                ."le même format peut aussi servir à indiquer une pièce \"papier\" et sa localisation."
            ),
            'attributes' => $this->attrs('ArchivesBinaryObjectType'),
            'class' => 'attachments-files-select',
            'onchange' => 'loadAttachment(this)',
            'afterload' => 'initializeAttachment',
            'chosen' => false,
            'options' => [],
            'data-placeholder' => __("-- Sélectionner un fichier --"),
            'allowEmpty' => true, // force la validation à se désactiver car la valeur est set uniquement pour le js
        ];
    }

    /**
     * ArchiveTransfer.Contains.Document.Control
     * @return array
     */
    private function getControl(): array
    {
        return [
            'label' => "Control : ".__("Indique si des exigences de contrôle portent sur le document."),
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'options' => [
                'false' => 'false - '.__("Non"),
                'true' => 'true - '.__("Oui"),
            ],
            'data-placeholder' => __("-- Exigences de contrôle --"),
        ];
    }

    /**
     * ArchiveTransfer.Contains.Document.Copy
     * @return array
     */
    private function getCopy(): array
    {
        return [
            'label' => "Copy : ".__("Indique s'il s'agit d'un original ou d'une copie."),
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'options' => [
                'false' => 'false - '.__("Non"),
                'true' => 'true - '.__("Oui"),
            ],
            'data-placeholder' => __("-- Est une copie --"),
        ];
    }

    /**
     * ArchiveTransfer.Contains.Document.Creation
     * @return array
     */
    private function getCreation(): array
    {
        return [
            'label' => "Creation : ".__("Date de création."),
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'data-type' => 'datetime',
            'validations' => $this->validations('DateTimeType'),
            'class' => 'datepicker',
            'placeholder' => $this->datetimePlaceholder(),
        ];
    }

    /**
     * ArchiveTransfer.Contains.Document.Description
     * @return array
     */
    private function getDescription(): array
    {
        return [
            'label' => "Description : ".__("Description du document."),
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'attributes' => $this->attrs('TextType'),
            'type' => 'textarea',
            'data-type' => 'text',
        ];
    }

    /**
     * ArchiveTransfer.Contains.Document.Issue
     * @return array
     */
    private function getIssue(): array
    {
        return [
            'label' => "Issue : ".__("Date d'émission du document."),
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'data-type' => 'datetime',
            'validations' => $this->validations('DateTimeType'),
            'class' => 'datepicker',
            'placeholder' => $this->datetimePlaceholder(),
        ];
    }

    /**
     * ArchiveTransfer.Contains.Document.ItemIdentifier
     * @return array
     */
    private function getItemIdentifier(): array
    {
        return [
            'label' => "ItemIdentifier : ".__("Identifiant unique d'un élément particulier dans le document."),
            'cardinality' => '0..n',
            'help' => __("Optionnel"),
            'attributes' => $this->attrs('ArchivesIDType'),
        ];
    }

    /**
     * ArchiveTransfer.Contains.Document.Purpose
     * @return array
     */
    private function getPurpose(): array
    {
        return [
            'label' => "Purpose : ".__("Objet ou objectif du document."),
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'attributes' => $this->attrs('TextType'),
            'type' => 'textarea',
            'data-type' => 'text',
        ];
    }

    /**
     * ArchiveTransfer.Contains.Document.Receipt
     * @return array
     */
    private function getReceipt(): array
    {
        return [
            'label' => "Receipt : ".__("Date de réception."),
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'data-type' => 'datetime',
            'validations' => $this->validations('DateTimeType'),
            'class' => 'datepicker',
            'placeholder' => $this->datetimePlaceholder(),
        ];
    }

    /**
     * ArchiveTransfer.Contains.Document.Response
     * @return array
     */
    private function getResponse(): array
    {
        return [
            'label' => "Response : ".__("Date de réponse."),
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'data-type' => 'datetime',
            'validations' => $this->validations('DateTimeType'),
            'class' => 'datepicker',
            'placeholder' => $this->datetimePlaceholder(),
        ];
    }

    /**
     * ArchiveTransfer.Contains.Document.Status
     * @return array
     */
    private function getStatus(): array
    {
        return [
            'label' => "Status",
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'attributes' => $this->attrs('ArchivesCodeType'),
        ];
    }

    /**
     * ArchiveTransfer.Contains.Document.Submission
     * @return array
     */
    private function getSubmission(): array
    {
        return [
            'label' => "Submission",
            'cardinality' => '0..1',
            'help' => __("Optionnel"),
            'data-type' => 'datetime',
            'validations' => $this->validations('DateTimeType'),
            'class' => 'datepicker',
            'placeholder' => $this->datetimePlaceholder(),
        ];
    }

    /**
     * ArchiveTransfer.Contains.Document.Type
     * @return array
     */
    private function getType(): array
    {
        return [
            'label' => "Type",
            'cardinality' => '1..n',
            'options' => self::getXsdOptions('type'),
            'data-placeholder' => __("-- Choisir un type --"),
            'empty' => true,
            'attributes' => $this->attrs('DocumentTypeCodeType'),
        ];
    }
}
