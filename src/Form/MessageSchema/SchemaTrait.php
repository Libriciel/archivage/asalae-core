<?php
/**
 * AsalaeCore\Form\MessageSchema\SchemaTrait
 */

namespace AsalaeCore\Form\MessageSchema;

use AsalaeCore\Model\Table\OrgEntitiesTable;
use Cake\Datasource\EntityInterface;
use Cake\I18n\I18n;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Cake\Utility\Xml;
use IntlDateFormatter;

/**
 * Code commun entre les schémas de messages
 *
 * @category Form\MessageSchema
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin MessageFormSchemaInterface
 */
trait SchemaTrait
{
    /**
     * @var int id de l'entité
     */
    protected $orgEntityId;

    /**
     * @var EntityInterface entité
     */
    protected $orgEntity;

    /**
     * @var array reférences des sous-objects déjà calculés
     */
    protected $references = [];

    /**
     * constructor.
     * @param int|string $orgEntityId
     */
    public function __construct($orgEntityId)
    {
        $this->orgEntityId = $orgEntityId;
        $this->orgEntity = $orgEntityId
            ? TableRegistry::getTableLocator()->get('OrgEntities')->get($orgEntityId)
            : null;
    }

    /**
     * Renvoi les elements enfant
     * @param mixed ...$elements
     * @return array
     */
    private function list(...$elements): array
    {
        $output = [];
        foreach ($elements as $key) {
            if (is_array($key)) {
                foreach ($key as $field => $params) {
                    $output[$field] = $params;
                }
                continue;
            }
            if (isset($this->references[$key])) {
                $output[$key] =& $this->references[$key];
                continue;
            }
            $func = 'get'.$key;
            if (method_exists($this, $func)) {
                $this->references[$key] = []; // set la valeur pour éviter les boucles infinies
                $this->references[$key] = $this->$func();
                $output[$key] =& $this->references[$key];
            } else {
                $output[$key] = [
                    'display' => false,
                ];
            }
        }
        return $output;
    }

    /**
     * @var string locale
     */
    private static $lang;

    /**
     * Donne "fr" si "fr_FR"
     * @return string
     */
    private static function localeLang(): string
    {
        if (empty(self::$lang)) {
            $local = I18n::getLocale();
            if (strpos($local, '_')) {
                /** @noinspection PhpUnusedLocalVariableInspection */
                [self::$lang, $territory] = explode('_', I18n::getLocale());
            } else {
                self::$lang = $local;
            }
        }
        return self::$lang;
    }

    /**
     * @var string format date local
     */
    private $localFormat;

    /**
     * Donne le format date local
     * @return string
     */
    private function getLocalFormat(): string
    {
        if (empty($this->localFormat)) {
            $formatter = new IntlDateFormatter(
                I18n::getDefaultLocale(),
                IntlDateFormatter::SHORT,
                IntlDateFormatter::NONE
            );
            $this->localFormat = preg_replace(
                '/\W/',
                '',
                preg_replace('/(\w)\w+/', '$1', strtolower($formatter->getPattern()))
            );
        }
        return $this->localFormat;
    }

    /**
     * Donne les validations pour un type de données particulier
     * @param string $string
     * @return array
     */
    private function validations(string $string): array
    {
        switch ($string) {
            case 'DateType':
                return [
                    'dateTime' => [
                        'rule' => ['date', $this->getLocalFormat()]
                    ]
                ];
            case 'TimeType':
                return [
                    'time' => [
                        'rule' => ['time', $this->getLocalFormat()]
                    ]
                ];
            case 'DateTimeType':
                return [
                    'dateTime' => [
                        'rule' => ['dateTime', $this->getLocalFormat()]
                    ]
                ];
            case 'duration':
                return [
                    'duration' => [
                        'rule' => [
                            'custom',
                            '/^(-?)P(?=\d|T\d)(?:(\d+)Y)?(?:(\d+)M)?(?:(\d+)([DW]))?'
                            .'(?:T(?:(\d+)H)?(?:(\d+)M)?(?:(\d+(?:\.\d+)?)S)?)?$/'
                        ]
                    ]
                ];
            default:
                return [];
        }
    }

    /**
     * @var array liste des versants
     */
    private $optionsIdTransferringAgency = [];

    /**
     * Liste des versants
     * @return array
     */
    private function optionsIdTransferringAgency(): array
    {
        if (empty($this->optionsIdTransferringAgency) && $this->orgEntity) {
            /** @var OrgEntitiesTable $OrgEntities */
            $OrgEntities = TableRegistry::getTableLocator()->get('OrgEntities');
            $this->optionsIdTransferringAgency = $OrgEntities->listOptionsWithoutSeal(['keyField' => 'identifier'])
                ->where(
                    [
                        'TypeEntities.code IN' => OrgEntitiesTable::CODE_TRANSFERRING_AGENCIES,
                        'OrgEntities.lft >=' => $this->orgEntity->get('lft'),
                        'OrgEntities.rght <=' => $this->orgEntity->get('rght'),
                        'OrgEntities.active' => true,
                    ]
                )
                ->all()
                ->toArray();
        }
        return $this->optionsIdTransferringAgency;
    }

    /**
     * @var array Liste des services d'archives
     */
    private $optionsIdArchivalAgency = [];

    /**
     * Liste des services d'archives
     * @return array
     */
    private function optionsIdArchivalAgency(): array
    {
        if (empty($this->optionsIdArchivalAgency) && $this->orgEntity) {
            /** @var OrgEntitiesTable $OrgEntities */
            $OrgEntities = TableRegistry::getTableLocator()->get('OrgEntities');
            $this->optionsIdArchivalAgency = $OrgEntities->listOptionsWithoutSeal(['keyField' => 'identifier'])
                ->where(
                    [
                        'TypeEntities.code IN' => OrgEntitiesTable::CODE_ARCHIVAL_AGENCIES,
                        'OrgEntities.lft >=' => $this->orgEntity->get('lft'),
                        'OrgEntities.rght <=' => $this->orgEntity->get('rght'),
                        'OrgEntities.active' => true,
                    ]
                )
                ->all()
                ->toArray();
        }
        return $this->optionsIdArchivalAgency;
    }

    /**
     * @var array liste des services producteur
     */
    private $optionsIdOriginatingAgency = [];

    /**
     * liste des services producteur
     * @return array
     */
    private function optionsIdOriginatingAgency(): array
    {
        if (empty($this->optionsIdOriginatingAgency) && $this->orgEntity) {
            /** @var OrgEntitiesTable $OrgEntities */
            $OrgEntities = TableRegistry::getTableLocator()->get('OrgEntities');
            $this->optionsIdOriginatingAgency = $OrgEntities->listOptionsWithoutSeal(['keyField' => 'identifier'])
                ->where(
                    [
                        'TypeEntities.code IN' => OrgEntitiesTable::CODE_ORIGINATING_AGENCIES,
                        'OrgEntities.lft >=' => $this->orgEntity->get('lft'),
                        'OrgEntities.rght <=' => $this->orgEntity->get('rght'),
                        'OrgEntities.active' => true,
                    ]
                )
                ->all()
                ->toArray();
        }
        return $this->optionsIdOriginatingAgency;
    }

    /**
     * @var array liste des Accords de versement
     */
    private $optionsIdSubmissionAgency = [];

    /**
     * liste des services producteur
     * @return array
     */
    private function optionsIdSubmissionAgency(): array
    {
        if (empty($this->optionsIdSubmissionAgency) && $this->orgEntity) {
            /** @var OrgEntitiesTable $OrgEntities */
            $OrgEntities = TableRegistry::getTableLocator()->get('OrgEntities');
            $this->optionsIdSubmissionAgency = $OrgEntities->listOptionsWithoutSeal(['keyField' => 'identifier'])
                ->where(
                    [
                        'TypeEntities.code IN' => OrgEntitiesTable::CODE_SUBMISSION_AGENCIES,
                        'OrgEntities.lft >=' => $this->orgEntity->get('lft'),
                        'OrgEntities.rght <=' => $this->orgEntity->get('rght'),
                    ]
                )
                ->all()
                ->toArray();
        }
        return $this->optionsIdSubmissionAgency;
    }

    /**
     * @var array liste des Accords de versement
     */
    private $optionsArchivalAgreement = [];

    /**
     * liste des Accords de versement
     * @return array
     */
    private function optionsArchivalAgreement(): array
    {
        if (empty($this->optionsArchivalAgreement) && $this->orgEntity) {
            $Agreements = TableRegistry::getTableLocator()->get('Agreements');
            $this->optionsArchivalAgreement = $Agreements->find(
                'list',
                [
                    'keyField' => 'identifier',
                    'valueField' => function (EntityInterface $entity) {
                        return $entity->get('identifier') . ' - ' . $entity->get('name');
                    },
                ]
            )
                ->select(
                    [
                        'Agreements.identifier',
                        'Agreements.name',
                    ]
                )
                ->innerJoinWith('OrgEntities')
                ->where(
                    [
                        'Agreements.active' => true,
                        'OrgEntities.lft <=' => $this->orgEntity->get('lft'),
                        'OrgEntities.rght >=' => $this->orgEntity->get('rght'),
                    ]
                )
                ->order(['Agreements.name' => 'asc'])
                ->all()
                ->toArray();
        }
        return $this->optionsArchivalAgreement;
    }

    /**
     * @var array liste des profils d'archives
     */
    private $optionsArchivalProfile = [];

    /**
     * liste des profils d'archives
     * @return array
     */
    private function optionsArchivalProfile(): array
    {
        if (empty($this->optionsArchivalProfile) && $this->orgEntity) {
            $Profiles = TableRegistry::getTableLocator()->get('Profiles');
            $this->optionsArchivalProfile = $Profiles->find(
                'list',
                [
                    'keyField' => 'identifier',
                    'valueField' => function (EntityInterface $entity) {
                        return $entity->get('identifier') . ' - ' . $entity->get('name');
                    },
                ]
            )
                ->select(
                    [
                        'Profiles.identifier',
                        'Profiles.name',
                    ]
                )
                ->innerJoinWith('OrgEntities')
                ->where(
                    [
                        'Profiles.active' => true,
                        'OrgEntities.lft <=' => $this->orgEntity->get('lft'),
                        'OrgEntities.rght >=' => $this->orgEntity->get('rght'),
                    ]
                )
                ->order(['Profiles.name' => 'asc'])
                ->all()
                ->toArray();
        }
        return $this->optionsArchivalProfile;
    }

    /**
     * @var array liste des niveaux de service
     */
    private $optionsServiceLevels = [];

    /**
     * liste des des niveaux de service
     * @return array
     */
    private function optionsServiceLevels(): array
    {
        if (empty($this->optionsServiceLevels) && $this->orgEntity) {
            $ServiceLevels = TableRegistry::getTableLocator()->get('ServiceLevels');
            $this->optionsServiceLevels = $ServiceLevels->find(
                'list',
                [
                    'keyField' => 'identifier',
                    'valueField' => function (EntityInterface $entity) {
                        return $entity->get('identifier') . ' - ' . $entity->get('name');
                    },
                ]
            )
                ->select(
                    [
                        'ServiceLevels.identifier',
                        'ServiceLevels.name',
                    ]
                )
                ->innerJoinWith('OrgEntities')
                ->where(
                    [
                        'ServiceLevels.active' => true,
                        'OrgEntities.lft <=' => $this->orgEntity->get('lft'),
                        'OrgEntities.rght >=' => $this->orgEntity->get('rght'),
                    ]
                )
                ->order(['ServiceLevels.name' => 'asc'])
                ->all()
                ->toArray();
        }
        return $this->optionsServiceLevels;
    }

    /**
     * Donne le format de date pour le placeholder
     * @return string The pattern string being used to format/parse.
     */
    private function datePlaceholder(): string
    {
        $locale = I18n::getDefaultLocale();
        if ($locale === 'fr_FR') {
            return 'jj/mm/aaaa';
        }
        $formatter = new IntlDateFormatter(
            $locale,
            IntlDateFormatter::SHORT,
            IntlDateFormatter::NONE
        );
        return $formatter->getPattern();
    }

    /**
     * Donne le format de date pour le placeholder
     * @return string The pattern string being used to format/parse.
     */
    private function datetimePlaceholder(): string
    {
        $locale = I18n::getDefaultLocale();
        if ($locale === 'fr_FR') {
            return 'jj/mm/aaaa hh:mm';
        }
        $formatter = new IntlDateFormatter(
            $locale,
            IntlDateFormatter::SHORT,
            IntlDateFormatter::NONE
        );
        return $formatter->getPattern();
    }

    /**
     * Donne les options contenu dans un fichier xsd
     * @param string $keypath
     * @param string $enumNamePath
     * @param string $descriptionPath
     * @return array
     */
    public static function getXsdOptions(
        string $keypath,
        string $enumNamePath = 'ccts:Name',
        string $descriptionPath = 'ccts:Description'
    ): array {
        $raw = Xml::toArray(Xml::build(static::getXsdCodePath($keypath), ['readFile' => true]));
        $enums = $raw['schema']['xsd:simpleType']['xsd:restriction']['xsd:enumeration'] ?? [];
        $output = [];
        foreach ($enums as $enum) {
            $output[$enum['@value']] = ['value' => $enum['@value']];
            $docs = Hash::get($enum, 'xsd:annotation.xsd:documentation', []);
            $title = preg_replace('/\t+/', '', Hash::get($docs, $descriptionPath));
            if ($title) {
                $output[$enum['@value']]['title'] = $title;
            }
            if (!isset($docs[0])) {
                $output[$enum['@value']]['text'] = $enum['@value'].' - '.Hash::get($docs, $enumNamePath);
                continue;
            }
            $default = [];
            foreach ($docs as $doc) {
                $lang = $doc['@xml:lang'] ?? '';
                if ($lang === 'en') {
                    $default = $doc;
                }
                if ($lang !== self::localeLang()) {
                    continue;
                }
                $output[$enum['@value']]['text'] = $enum['@value'].' - '.Hash::get($doc, $enumNamePath);
                break;
            }
            if (empty($output[$enum['@value']])) {
                $output[$enum['@value']]['text'] = $enum['@value'].' - '.Hash::get($default, $enumNamePath);
            }
        }
        return array_values($output);
    }
}
