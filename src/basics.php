<?php
/**
 * Fonctions de base de l'application
 * Améliorant/remplaçant les fonctions basics de cakephp
 * @noinspection PhpCSValidationInspection
 * @noinspection RedundantSuppression - bug sur PhpCSValidationInspection
 * @noinspection PhpInternalEntityUsedInspection
 * phpcs:ignoreFile - A file should declare new symbols and cause no other side effects...
 */

use AsalaeCore\Error\ContextDebugger;
use Cake\Core\Configure;
use Cake\Error\Debug\TextFormatter;
use Cake\Error\Debugger;
use Cake\Log\Log;

if (!function_exists('d')) {
    /**
     * Fonction de debug, permet de chainer les arguments
     * ex: dd('foo', 'bar') affichera un array['foo', 'bar']
     * Ajoute la ligne executée à l'entête
     *
     * @see debug()
     *
     * @param mixed ...$args
     * @return mixed
     */
    function d(...$args)
    {
        if (!Configure::read('debug')) {
            return;
        }
        return call_user_func_array('\AsalaeCore\Error\ContextDebugger::debug', $args);
    }
    ContextDebugger::registerDebugFunction('d');
}

if (!function_exists('debugl')) {
    /**
     * Fonction de debug light
     *
     * @see debug()
     *
     * @param mixed ...$args
     * @return mixed
     */
    function debugl(...$args)
    {
        if (!Configure::read('debug')) {
            return false;
        }
        $trace = Debugger::trace(['start' => 1, 'depth' => 2, 'format' => 'array']);
        /** @psalm-suppress PossiblyInvalidArrayOffset */
        $location = [
            'line' => $trace[0]['line'],
            'file' => $trace[0]['file'],
        ];
        $debugger = Debugger::getInstance();
        $formatter = $debugger->getExportFormatter();
        foreach ($args as $arg) {
            echo $formatter->formatWrapper(
                Debugger::exportVar($arg),
                $location
            );
        }
        return count($args) === 1 ? $args[0] : true;
    }

    /**
     * Fonction de debug light avec retour du debug au lieu de l'afficher
     *
     * @see debug()
     *
     * @param mixed ...$args
     * @return string
     */
    function debuglr(...$args): string
    {
        if (!Configure::read('debug')) {
            return '';
        }
        $trace = Debugger::trace(['start' => 1, 'depth' => 2, 'format' => 'array']);
        $location = [
            'line' => $trace[0]['line'] ?? '',
            'file' => $trace[0]['file'] ?? '',
        ];
        $debugger = Debugger::getInstance();
        $formatter = $debugger->getExportFormatter();
        $str = '';
        foreach ($args as $arg) {
            $str .= $formatter->formatWrapper(
                Debugger::exportVar($arg),
                $location
            );
        }
        return $str;
    }
}

if (!function_exists('djson')) {
    /**
     * Permet d'effectuer le debug au format json
     * @param array ...$args
     */
    function djson(...$args)
    {
        if (!Configure::read('debug')) {
            return;
        }
        if (!headers_sent()) {
            header('Content-Type', 'application/json');
        }
        echo json_encode($args, JSON_PRETTY_PRINT).PHP_EOL;
    }
}

if (!function_exists('dtrace')) {
    /**
     * Effectue un backtrace formaté
     * @param bool  $show    retour de fonction dans un debug
     * @param array $options
     * @return array|string
     */
    function dtrace(bool $show = true, array $options = [])
    {
        $backtrace = debug_backtrace();
        $trace = Debugger::formatTrace($backtrace, $options);
        if ($show && Configure::read('debug')) {
            $debug = Debugger::formatTrace($backtrace, ['start' => 0, 'depth' => 2, 'format' => 'array']);
            /** @psalm-suppress PossiblyInvalidArrayOffset */
            $location = [
                'line' => $debug[0]['line'],
                'file' => $debug[0]['file'],
            ];
            Debugger::printVar($trace, $location);
        }
        return $trace;
    }
}

if (!function_exists('dlogtrace')) {
    /**
     * Log un backtrace formaté
     * @param array $options
     * @return array|string
     */
    function dlogtrace(array $options = [])
    {
        $traceLocation = Debugger::trace(['start' => 1, 'depth' => 2, 'format' => 'array']);
        $location = [
            'line' => $traceLocation[0]['line'] ?? '',
            'file' => $traceLocation[0]['file'] ?? '',
        ];

        $backtrace = debug_backtrace();
        $trace = Debugger::formatTrace($backtrace, $options);

        $formatter = new TextFormatter;
        $str = $formatter->formatWrapper(
            Debugger::exportVarAsPlainText($trace),
            $location
        );
        Log::debug($str);
        return $trace;
    }
}

if (!function_exists('dstart')) {
    $chronoStart = null;

    /**
     * Démare le compteur
     * @var bool $stopLast arrete le précédent compteur
     */
    function dstart(bool $stopLast = true, bool $printVar = true)
    {
        global $chronoStart;

        if ($stopLast && $chronoStart) {
            $msg = dstop(true, $printVar);
        } elseif ($chronoStart) {
            $backtrace = debug_backtrace();
            $debug = Debugger::formatTrace(
                $backtrace,
                ['start' => 0, 'depth' => 2, 'format' => 'array']
            );
            $duration = microtime(true) - $chronoStart['microtime'];
            $msg = sprintf(
                "%s seconds since dstart was called in %s:%s",
                round($duration, 4),
                Debugger::trimPath((string)$chronoStart['file']),
                $chronoStart['line']
            );
            /** @psalm-suppress PossiblyInvalidArrayOffset */
            $location = [
                'line' => $debug[0]['line'],
                'file' => $debug[0]['file'],
            ];
            if ($printVar && Configure::read('debug')) {
                Debugger::printVar($msg, $location);
            }
            return $msg;
        }

        $backtrace = debug_backtrace();
        $debug = Debugger::formatTrace($backtrace, ['start' => 0, 'depth' => 2, 'format' => 'array']);
        $chronoStart = [
            'microtime' => microtime(true),
            'line' => $debug[0]['line'],
            'file' => $debug[0]['file'],
        ];
        return $msg ?? 'initialising dstart()';
    }

    /**
     * Arrète le compteur et affiche la durée
     */
    function dstop(bool $stoppedByStart = false, bool $printVar = true)
    {
        global $chronoStart;
        $backtrace = debug_backtrace();
        $debug = Debugger::formatTrace(
            $backtrace,
            ['start' => $stoppedByStart ? 1 : 0, 'depth' => 2, 'format' => 'array']
        );
        if (!$chronoStart) {
            $msg = 'chrono was not started (you must use dstart() before call dstop())';
        } else {
            $duration = microtime(true) - $chronoStart['microtime'];
            $msg = sprintf(
                "%s seconds since dstart was called in %s:%s",
                round($duration, 4),
                Debugger::trimPath((string)$chronoStart['file']),
                $chronoStart['line']
            );
        }
        $chronoStart = null;
        /** @psalm-suppress PossiblyInvalidArrayOffset */
        $location = [
            'line' => $debug[0]['line'],
            'file' => $debug[0]['file'],
        ];
        if ($printVar && Configure::read('debug')) {
            Debugger::printVar($msg, $location);
        }
        return $msg;
    }
}

if (!function_exists('dlog')) {
    /**
     * Exporte le debug dans le logs/debug.log (fonctionne même sans debug => true)
     * @param mixed ...$args debug les arguments
     * @return mixed 1er argument
     */
    function dlog(...$args)
    {
        $trace = Debugger::trace(['start' => 1, 'depth' => 2, 'format' => 'array']);
        $location = [
            'line' => $trace[0]['line'] ?? '',
            'file' => $trace[0]['file'] ?? '',
        ];
        $formatter = new TextFormatter;
        $str = '';
        foreach ($args as $arg) {
            $str .= $formatter->formatWrapper(
                Debugger::exportVarAsPlainText($arg),
                $location
            );
        }
        Log::debug($str);
        return current($args);
    }
}

if (!function_exists('dforcelog')) {
    /**
     * Exporte le debug dans le logs/debug.log (fonctionne même sans debug => true)
     * @param mixed ...$args debug les arguments
     * @return mixed 1er argument
     */
    function dforcelog(...$args)
    {
        $trace = Debugger::trace(['start' => 1, 'depth' => 2, 'format' => 'array']);
        $location = [
            'line' => $trace[0]['line'] ?? '',
            'file' => $trace[0]['file'] ?? '',
        ];
        $formatter = new TextFormatter;
        $str = '';
        foreach ($args as $arg) {
            $str .= $formatter->formatWrapper(
                Debugger::exportVarAsPlainText($arg),
                $location
            );
        }
        file_put_contents(LOGS . 'debug.log', $str, FILE_APPEND);
        return current($args);
    }
}

if (!function_exists('dlast')) {
    /**
     * Affiche la méthode appelante (trace de 1 niveau)
     * @param int  $amount quantité de trace à remonter
     * @param null $var debug une variable (optionnel)
     * @param bool $show
     * @return string|mixed $var ou str du debug
     */
    function dlast(int $amount = 1, $var = null, bool $show = true)
    {
        $trace = Debugger::trace(['start' => 1, 'depth' => 2 + $amount, 'format' => 'array']);
        $location = [
            'line' => $trace[0]['line'] ?? '',
            'file' => $trace[0]['file'] ?? '',
        ];
        $formatter = new TextFormatter;
        do {
            $selectedTrace = $trace[$amount] ?? null;
        } while ($selectedTrace === null && $amount-- > 0);
        $prev = $selectedTrace
            ? sprintf(
                '%s:%d%s',
                $selectedTrace['file'],
                $selectedTrace['line'],
                $var !== null ? " \n" . Debugger::exportVarAsPlainText($var) : ''
            )
            : '';
        $prev = str_replace(APP, 'APP', $prev);
        $prev = str_replace(WWW_ROOT, 'WWW_ROOT', $prev);
        $prev = str_replace(ROOT, 'ROOT', $prev);
        $export = substr(Debugger::exportVarAsPlainText($prev), 1, -1);
        if ($selectedTrace && is_int($selectedTrace['line'])) {
            $extractLine = exec(
                sprintf('sed -n %dp %s', $selectedTrace['line'], escapeshellarg($selectedTrace['file']))
            );
            $export .= "\n" . trim($extractLine);
        }
        $str = $formatter->formatWrapper($export, $location);
        if ($show && Configure::read('debug')) {
            echo $str;
        }
        return $var !== null ? $var : $export;
    }
}

if (!function_exists('dlastlocal')) {
    /**
     * Affiche la méthode locale appelante
     * @param null $var debug une variable (optionnel)
     * @param bool $show
     * @return string|mixed $var ou str du debug
     */
    function dlastlocal($var = null, bool $show = true)
    {
        $backtrace = debug_backtrace();
        $trace = Debugger::formatTrace($backtrace, ['format' => 'array']);
        $localDirs = [APP, CONFIG];
        if (defined('ASALAE_CORE')) {
            $localDirs[] = ROOT . DS . 'vendor' . DS . 'libriciel';
        }
        $currentLocation = $trace[0];
        $location = null;
        foreach ($trace as $details) {
            foreach ($localDirs as $dir) {
                $len = strlen($dir);
                if (substr($details['file'] ?? '', 0, $len) === $dir) {
                    $location = [
                        'line' => $details['line'] ?? '',
                        'file' => $details['file'] ?? '',
                    ];
                    break 2;
                }
            }
        }
        if (!$location) {
            return null;
        }
        $formatter = new TextFormatter;
        $prev = sprintf(
            '%s:%d%s',
            $location['file'],
            $location['line'],
            $var !== null ? " \n" . Debugger::exportVarAsPlainText($var) : ''
        );
        $prev = str_replace(APP, 'APP', $prev);
        $prev = str_replace(WWW_ROOT, 'WWW_ROOT', $prev);
        $prev = str_replace(ROOT, 'ROOT', $prev);
        $export = substr(Debugger::exportVarAsPlainText($prev), 1, -1);
        $extractLine = exec(
            sprintf('sed -n %dp %s', $location['line'], escapeshellarg($location['file']))
        );
        $export .= "\n" . trim($extractLine);
        $str = $formatter->formatWrapper($export, $currentLocation);
        if ($show && Configure::read('debug')) {
            echo $str;
        }
        return $var !== null ? $var : $export;
    }
}

if (!function_exists('forcedebug')) {
    /**
     * Prints out debug information about given variable and returns the
     * variable that was passed.
     *
     * Only runs if debug mode is enabled.
     *
     * @param mixed $var Variable to show debug information for.
     * @param bool|null $showHtml If set to true, the method prints the debug data in a browser-friendly way.
     * @param bool $showFrom If set to true, the method prints from where the function was called.
     * @return mixed The same $var that was passed
     * @link https://book.cakephp.org/4/en/development/debugging.html#basic-debugging
     * @link https://book.cakephp.org/4/en/core-libraries/global-constants-and-functions.html#debug
     */
    function forcedebug($var, $showHtml = null, $showFrom = true)
    {
        $location = [];
        if ($showFrom) {
            $trace = Debugger::trace(['start' => 1, 'depth' => 2, 'format' => 'array']);
            /** @psalm-suppress PossiblyInvalidArrayOffset */
            $location = [
                'line' => $trace[0]['line'],
                'file' => $trace[0]['file'],
            ];
        }

        Debugger::printVar($var, $location, $showHtml);

        return $var;
    }
}


if (!function_exists('dtext')) {
    /**
     * Effectue un debug sans mise en page HTML
     * @param mixed $var Variable to show debug information for.
     * @return mixed The same $var that was passed
     * @link https://book.cakephp.org/4/en/development/debugging.html#basic-debugging
     * @link https://book.cakephp.org/4/en/core-libraries/global-constants-and-functions.html#debug
     */
    function dtext($var)
    {
        $trace = Debugger::trace(['start' => 1, 'depth' => 2, 'format' => 'array']);
        $location = [
            'line' => $trace[0]['line'] ?? '',
            'file' => $trace[0]['file'] ?? '',
        ];
        $formatter = new TextFormatter;
        echo $formatter->formatWrapper(
            Debugger::exportVarAsPlainText($var),
            $location
        );

        return $var;
    }
}
