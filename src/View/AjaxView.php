<?php
/**
 * AsalaeCore\View\AjaxView
 */

namespace AsalaeCore\View;

/**
 * Gestion du rendu de la vue lors d'une requête ajax
 *
 * @category View
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class AjaxView extends AppView
{

    /**
     * The name of the layout file to render the view inside of. The name
     * specified is the filename of the layout in /src/Template/Layout without
     * the .ctp extension.
     *
     * @var string
     */
    public $layout = 'ajax';

    /**
     * Initialization hook method.
     *
     * @return void
     */
    public function initialize(): void
    {
        parent::initialize();

        $this->setResponse($this->getResponse()->withType('ajax'));
    }
}
