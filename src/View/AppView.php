<?php
/**
 * AsalaeCore\View\AppView
 */

namespace AsalaeCore\View;

use AsalaeCore\View\Helper\AclHelper;
use AsalaeCore\View\Helper\AjaxPaginatorHelper;
use AsalaeCore\View\Helper\DateHelper;
use AsalaeCore\View\Helper\FaHelper;
use AsalaeCore\View\Helper\FilterHelper;
use AsalaeCore\View\Helper\FormHelper;
use AsalaeCore\View\Helper\LiberSignHelper;
use AsalaeCore\View\Helper\ModalFormHelper;
use AsalaeCore\View\Helper\ModalViewHelper;
use AsalaeCore\View\Helper\MultiStepFormHelper;
use AsalaeCore\View\Helper\PaginatorHelper;
use AsalaeCore\View\Helper\PrevHelper;
use AsalaeCore\View\Helper\TableHelper;
use AsalaeCore\View\Helper\TabsHelper;
use AsalaeCore\View\Helper\TranslateHelper;
use AsalaeCore\View\Helper\UploadHelper;
use AsalaeCore\View\Helper\ViewTableHelper;
use Bootstrap\View\Helper\BreadcrumbsHelper;
use Bootstrap\View\Helper\ModalHelper;
use Cake\View\View;

/**
 * Gestion du rendu de la vue de l'application
 *
 * @category View
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @link http://book.cakephp.org/3.0/en/views.html#the-app-view
 * @property AclHelper Acl
 * @property AjaxPaginatorHelper AjaxPaginator
 * @property DateHelper Date
 * @property FaHelper Fa
 * @property FilterHelper Filter
 * @property FormHelper Form
 * @property LiberSignHelper LiberSign
 * @property ModalFormHelper ModalForm
 * @property ModalViewHelper ModalView
 * @property MultiStepFormHelper MultiStepForm
 * @property PaginatorHelper Paginator
 * @property PrevHelper Prev
 * @property TableHelper Table
 * @property TabsHelper Tabs
 * @property TranslateHelper Translate
 * @property UploadHelper Upload
 * @property ViewTableHelper ViewTable
 * @property BreadcrumbsHelper Breadcrumbs
 * @property ModalHelper Modal
 */
class AppView extends View
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading helpers.
     *
     * e.g. `$this->loadHelper('Html');`
     *
     * @return void
     */
    public function initialize(): void
    {
    }
}
