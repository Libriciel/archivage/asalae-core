<?php
/**
 * AsalaeCore\View\Helper\TranslateHelper
 */

namespace AsalaeCore\View\Helper;

use Cake\I18n\I18n;
use Cake\View\Helper;

/**
 * Centralise certaines traductions
 *
 * @category Helper
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property \Cake\View\Helper\HtmlHelper $Html
 */
class TranslateHelper extends Helper
{
    /**
     * Helpers utilisés
     *
     * @var array
     */
    public $helpers = ['Html'];

    /**
     * Permet d'obtenir la traduction pour un controller / action dans un
     * contexte de droits
     *
     * @param string      $controller
     * @param string|null $action
     * @return string
     */
    public function permission(string $controller, string $action = null): string
    {
        if ($action === null) {
            if ($controller == 'Agreements') {
                return __d('permission', "Accords de versement");
            }
            return I18n::getTranslator('permission')->translate($controller);
        }
        if ($controller === 'Download') {
            switch ($action) {
                case 'file':
                    return __dx('permission', 'Download', 'file');
                case 'open':
                    return __dx('permission', 'Download', 'open');
                case 'thumbnailImage':
                    return __dx('permission', 'Download', 'thumbnailImage');
                case 'zip':
                    return __dx('permission', 'Download', 'zip');
            }
        }

        // Actions générique
        switch ($action) {
            case 'add':
            case 'add1':
                return __d('permission', "Ajouter");
            case 'delete':
                return __d('permission', "Supprimer");
            case 'edit':
                return __d('permission', "Modifier");
            case 'index':
                return __d('permission', "Lister");
            case 'view':
                return __d('permission', "Visualiser");
            case 'api.create':
                return __d('permission', "Création via Web service");
            case 'api.read':
                return __d('permission', "Lecture via Web service");
            case 'api.update':
                return __d('permission', "Mise à jour via Web service");
            case 'api.delete':
                return __d('permission', "Suppression via Web service");
        }
        return I18n::getTranslator('permission')->translate($action, ['_context' => $controller]);
    }

    /**
     * Traduction des options de permission
     * @param string $name
     * @return string
     */
    public function optionsPerms(string $name): string
    {
        switch ($name) {
            case 'versant':
                return __("Versant");
            case 'producteur':
                return __("Producteur");
            case 'admin':
                return __("Administrateur");
        }
        return I18n::getTranslator()->translate($name);
    }
}
