<?php
/**
 * AsalaeCore\View\Helper\FormHelper
 */

namespace AsalaeCore\View\Helper;

use AsalaeCore\DataType\FormDataInterface;
use Bootstrap\View\Helper\FormHelper as BootstrapFormHelper;
use Cake\Datasource\EntityInterface;
use Cake\Form\Form;
use Cake\Utility\Hash;
use Cake\View\Helper\FormHelper as CakeFormHelper;
use Cake\View\Helper\UrlHelper;
use Cake\View\View;
use Traversable;

/**
 * Surcharge du Bootstrap\View\Helper\FormHelper
 *
 * @category Helper
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property UrlHelper $Url
 * @property \Cake\View\Helper\HtmlHelper $Html
 */
class FormHelper extends BootstrapFormHelper
{
    /**
     * @var EntityInterface Entité utilisé dans le formulaire
     */
    private $entity;

    /**
     * @var Form Formulaire utilisé
     */
    private $form;

    /**
     * @var array Liste des erreurs affichés
     */
    private $displayedErrors = [];

    /**
     * Default configuration for the helper.
     *
     * - `idPrefix` See CakePHP `FormHelper`.
     * - `errorClass` See CakePHP `FormHelper`. Overriden by `'has-error'`.
     * - `typeMap` See CakePHP `FormHelper`.
     * - `templates` Templates for the various form elements.
     * - `templateClass` Class used to format the various template. Do not override!
     * - `buttons` Default options for buttons.
     * - `columns` Default column sizes for horizontal forms.
     * - `useCustomFileInput` Set to `true` to use the custom file input. Default is `false`.
     *
     * @var array
     */
    protected $_defaultConfig = [];

    /**
     * Construct the widgets and binds the default context providers
     *
     * @param View                 $View
     * @param array<string, mixed> $config Configuration settings for the helper.
     */
    public function __construct(View $View, array $config = [])
    {
        $this->_defaultConfig = [
            'idPrefix' => null,
            'errorClass' => 'has-error',
            'typeMap' => [
                'string' => 'text', 'datetime' => 'datetime', 'boolean' => 'checkbox',
                'timestamp' => 'datetime', 'text' => 'textarea', 'time' => 'time',
                'date' => 'date', 'float' => 'number', 'integer' => 'number',
                'decimal' => 'number', 'binary' => 'file', 'uuid' => 'string'
            ],
            'templates' => [
                'button' => '<button{{attrs}}>{{text}}</button>',
                'checkbox' => '<input type="checkbox" name="{{name}}" value="{{value}}"{{attrs}}>',
                'checkboxFormGroup' => '{{label}}',
                'checkboxWrapper' => '<div class="checkbox">{{label}}</div>',
                'checkboxContainer' => '<div class="form-group checkbox {{required}}">{{content}}</div>',
                'checkboxContainerHorizontal'
                => '<div class="form-group"><div class="{{inputColumnOffsetClass}} {{inputColumnClass}}">'
                    .'<div class="checkbox {{required}}">{{content}}</div></div></div>',
                'confirmJs' => '{{confirm}}',
                'dateWidget'
                => '<div class="row">{{year}}{{month}}{{day}}{{hour}}{{minute}}{{second}}{{meridian}}</div>',
                'error' => '<span class="help-block error-message">{{content}}</span>',
                'errorHorizontal' => '<span class="help-block error-message {{errorColumnClass}}">{{content}}</span>',
                'errorList' => '<ul>{{content}}</ul>',
                'errorItem' => '<li>{{text}}</li>',
                'file' => '<input type="file" name="{{name}}" {{attrs}}>',
                'fieldset' => '<fieldset{{attrs}}>{{content}}</fieldset>',
                'formStart' => '<form{{attrs}}>',
                'formEnd' => '</form>',
                'formGroup' => '{{label}}<div class="form-group-wrapper">{{prepend}}{{input}}{{append}}</div>',
                'formGroupHorizontal'
                => '{{label}}<div class="{{inputColumnClass}}">{{prepend}}{{input}}{{append}}</div>',
                'hiddenBlock' => '<div style="display:none;">{{content}}</div>',
                'input' => '<input type="{{type}}" name="{{name}}" class="form-control{{attrs.class}}" {{attrs}} />',
                'inputSubmit' => '<input type="{{type}}"{{attrs}}>',
                'inputContainer' => '<div class="form-group {{type}}{{required}}">{{content}}</div>',
                'inputContainerError'
                => '<div class="form-group has-error {{type}}{{required}}">{{content}}{{error}}</div>',
                'label' => '<label class="control-label{{attrs.class}}"{{attrs}}>{{text}}</label>',
                'labelHorizontal'
                => '<label class="control-label {{labelColumnClass}}{{attrs.class}}"{{attrs}}>{{text}}</label>',
                'labelInline' => '<label class="sr-only{{attrs.class}}"{{attrs}}>{{text}}</label>',
                'nestingLabel' => '{{hidden}}<label{{attrs}}>{{input}}{{text}}</label>',
                'legend' => '<legend>{{text}}</legend>',
                'option' => '<option value="{{value}}"{{attrs}}>{{text}}</option>',
                'optgroup' => '<optgroup label="{{label}}"{{attrs}}>{{content}}</optgroup>',
                'select'
                => '<select name="{{name}}" class="form-control{{attrs.class}}" {{attrs}}>{{content}}</select>',
                'selectColumn'
                => '<div class="col-md-{{columnSize}}">'
                    .'<select name="{{name}}" class="form-control{{attrs.class}}" {{attrs}}>{{content}}</select></div>',
                'selectMultiple'
                => '<select name="{{name}}[]" multiple="multiple" '
                    .'class="form-control{{attrs.class}}" {{attrs}}>{{content}}</select>',
                'selectedClass' => 'selected',
                'radio' => '<input type="radio" name="{{name}}" value="{{value}}"{{attrs}}>',
                'radioWrapper' => '<div class="radio">{{label}}</div>',
                'radioContainer' => '<div class="form-group {{required}}">{{content}}</div>',
                'inlineRadio' => '<input type="radio" name="{{name}}" value="{{value}}"{{attrs}}>',
                'inlineRadioWrapper' => '{{label}}',
                'inlineRadioNestingLabel' => '{{hidden}}<label{{attrs}} class="radio-inline">{{input}}{{text}}</label>',
                'textarea'
                => '<textarea name="{{name}}" class="form-control{{attrs.class}}" {{attrs}}>{{value}}</textarea>',
                'submitContainer'
                => '<div class="form-group">'
                    .'{{submitContainerHorizontalStart}}{{content}}{{submitContainerHorizontalEnd}}</div>',
                'submitContainerHorizontal'
                => '<div class="form-group">'
                    .'<div class="{{inputColumnOffsetClass}} {{inputColumnClass}}">{{content}}</div></div>',
                'inputGroup' => '{{inputGroupStart}}{{input}}{{inputGroupEnd}}',
                'inputGroupStart' => '<div class="input-group">{{prepend}}',
                'inputGroupEnd' => '{{append}}</div>',
                'inputGroupAddons' => '<span class="input-group-addon">{{content}}</span>',
                'inputGroupButtons' => '<span class="input-group-btn">{{content}}</span>',
                'helpBlock' => '<p class="help-block">{{content}}</p>',
                'buttonGroup' => '<div class="btn-group{{attrs.class}}"{{attrs}}>{{content}}</div>',
                'buttonToolbar' => '<div class="btn-toolbar{{attrs.class}}"{{attrs}}>{{content}}</div>',
                'fancyFileInput'
                => '{{fileInput}}<div class="input-group"><div class="input-group-btn">{{button}}</div>{{input}}</div>'
            ],
            'buttons' => [
                'type' => 'default'
            ],
            'columns' => [
                'md' => [
                    'label' => 2,
                    'input' => 10,
                    'error' => 0
                ]
            ],
            'useCustomFileInput' => false
        ];
        parent::__construct($View, $config);
    }

    /**
     * Returns an HTML form element.
     *
     * ### Options
     *
     * - `context` Additional options for the context class. For example the
     * EntityContext accepts a 'table' option that allows you to set the specific Table
     * class the form should be based on.
     * - `encoding` Set the accept-charset encoding for the form. Defaults to
     * `Configure::read('App.encoding')`.
     * - `enctype` Set the form encoding explicitly. By default `type => file` will set
     * `enctype` to `multipart/form-data`.
     * - `horizontal` Boolean specifying if the form should be horizontal.
     * - `idPrefix` Prefix for generated ID attributes.
     * - `inline` Boolean specifying if the form should be inlined.
     * - `method` Set the form's method attribute explicitly.
     * - `templates` The templates you want to use for this form. Any templates will be
     * merged on top of the already loaded templates. This option can either be a filename
     * in /config that contains the templates you want to load, or an array of templates
     * to use.
     * - `templateVars` Provide template variables for the formStart template.
     * - `type` Form method defaults to autodetecting based on the form context. If
     *   the form context's isCreate() method returns false, a PUT request will be done.
     * - `url` The URL the form submits to. Can be a string or a URL array. If you use 'url'
     *    you should leave 'action' undefined.
     *
     * @override Bootstrap::create() - retrait du 'role' => 'form' forcé incompatible W3C
     *
     * @param mixed $context The context for which the form is being defined. Can
     *                       be an ORM entity, ORM resultset, or an array of meta
     *                       data. You can use false or null to make a model-less
     *                       form.
     * @param array $options An array of html attributes and options.
     *
     * @return string An formatted opening FORM tag.
     */
    public function create($context = null, array $options = array()): string
    {
        $options += [
            'horizontal' => false,
            'inline' => false
        ];
        $this->horizontal = $options['horizontal'];
        $this->inline = $options['inline'];
        unset($options['horizontal'], $options['inline']);
        if ($this->horizontal) {
            $options = $this->addClass($options, 'form-horizontal');
        } elseif ($this->inline) {
            $options = $this->addClass($options, 'form-inline');
        }
        if ($context instanceof EntityInterface) {
            $this->entity = $context;
            $this->displayedErrors = [];
        }
        if ($context instanceof Form) {
            $this->form = $context;
            $this->displayedErrors = [];
        }

        return CakeFormHelper::create($context, $options);
    }

    /**
     * Generates a form input element complete with label and wrapper div.
     *
     * ### Options
     *
     * See each field type method for more information. Any options that are part of
     * `$attributes` or `$options` for the different **type** methods can be included
     * in `$options` for input().
     * Additionally, any unknown keys that are not in the list below, or part of the
     * selected type's options will be treated as a regular HTML attribute for the
     * generated input.
     *
     * - `append` Content to append to the input, may be a string or an array of buttons.
     * - `empty` String or boolean to enable empty select box options.
     * - `error` Control the error message that is produced. Set to `false` to disable
     * any kind of error reporting (field error and error messages).
     * - `help` Help message to add below the input.
     * - `label` Either a string label, or an array of options for the label.
     * See FormHelper::label().
     * - `labelOptions` - Either `false` to disable label around nestedWidgets e.g. radio, multicheckbox or an array
     *   of attributes for the label tag. `selected` will be added to any classes e.g. `class => 'myclass'` where
     *   widget is checked.
     * - `nestedInput` Used with checkbox and radio inputs. Set to false to render
     * inputs outside of label elements. Can be set to true on any input to force the
     * input inside the label. If you enable this option for radio buttons you will also
     * need to modify the default `radioWrapper` template.
     * - `inline` Only used with radio inputs, set to `true` to have inlined radio buttons.
     * - `options` For widgets that take options e.g. radio, select.
     * - `templates` The templates you want to use for this input. Any templates will be
     * merged on top of the already loaded templates. This option can either be a filename
     * in /config that contains the templates you want to load, or an array of templates
     * to use.
     * - `prepend` Content to prepend to the input, may be a string or an array of buttons.
     * - `templateVars` Array of template variables.
     * - `type` Force the type of widget you want. e.g. `type => 'select'`
     *
     * @overide évite une erreur W3C en cas de name à vide
     *
     * @param string $fieldName This should be "modelname.fieldname"
     * @param array  $options   Each type of input takes different options.
     *
     * @return string Completed form widget.
     */
    public function control($fieldName, array $options = array()): string
    {
        $options += [
            'type' => null,
            'label' => null,
            'error' => null,
            'required' => null,
            'options' => null,
            'templates' => [],
            'templateVars' => [],
            'labelOptions' => true
        ];

        $templater = $this->templater();
        $newTemplates = $options['templates'];
        if ($newTemplates) {
            $templater->push();
            $templateMethod = is_string($options['templates']) ? 'load' : 'add';
            $templater->{$templateMethod}($options['templates']);
        }
        unset($options['templates']);

        if (!isset($options['val'])) {
            $valOptions = [
                'default' => $options['default'] ?? null,
                'schemaDefault' => $options['schemaDefault'] ?? true,
            ];
            $options['val'] = $this->getSourceValue($fieldName, $valOptions);
        }
        if ($options['val'] instanceof FormDataInterface) {
            $options['val'] = $options['val']->toFormData();
        }
        if ($options['val'] instanceof Traversable) {
            $options['val'] = iterator_to_array($options['val']);
        }

        $input = parent::control($fieldName, $options);
        if ($fieldName
            && !empty($options['options'])
            && !empty($options['val'])
            && !strpos($input, 'selected="selected"')
            && !strpos($input, 'checked="checked"')
        ) {
            $opts = $options['options'];
            if ($opts instanceof Traversable) {
                $opts = iterator_to_array($opts);
            }
            $options['options'] = $opts;
            if (is_array($options['val'])) {
                foreach ($options['val'] as $opt) {
                    if (!is_array($opt) && !is_object($opt)) {
                        $options['options'][$opt] = __("** option retirée ** - {0}", $opt);
                    }
                }
            } else {
                $options['options'][$options['val']] = __("** option retirée ** - {0}", $options['val']);
            }
            $input = parent::control($fieldName, $options);
        }
        if ($this->entity
            && $fieldName
            && $this->entity->getError($fieldName)
            && empty($options['hidden'])
        ) {
            $this->displayedErrors[] = $fieldName;
        }
        if ($this->form && $fieldName && empty($options['hidden'])) {
            $errs = $this->form->getErrors();
            if (isset($errs[$fieldName])) {
                $this->displayedErrors[] = $fieldName;
            }
        }

        if ($newTemplates) {
            $templater->pop();
        }

        return str_replace(' name=""', '', $input);
    }

    /**
     * Closes an HTML form, cleans up values set by FormHelper::create(), and writes hidden
     * input fields where appropriate.
     *
     * Resets some parts of the state, shared among multiple FormHelper::create() calls, to defaults.
     *
     * @param array $secureAttributes Secure attributes which will be passed as HTML attributes
     *                                into the hidden input elements generated for the Security Component.
     * @return string A closing FORM tag.
     * @link https://book.cakephp.org/3.0/en/views/helpers/form.html#closing-the-form
     */
    public function end(array $secureAttributes = []): string
    {
        $alert = '';
        if ($this->entity) {
            $errs = $this->entity->getErrors();
        } elseif ($this->form) {
            $errs = $this->form->getErrors();
        }
        if (!empty($errs)) {
            $toDisplay = [];
            foreach ($this->flattenErrors($errs) as $field => $errors) {
                if (in_array($field, $this->displayedErrors)) {
                    continue;
                }
                $escapedErrors = array_map('h', Hash::flatten($errors));
                $ul = $this->Html->tag('ul');
                $ul .= '<li>'.implode('</li><li>', $escapedErrors).'</li>';
                $ul .= $this->Html->tag('/ul');

                $toDisplay[] = $this->Html->tag('li', $field.$ul).PHP_EOL;
            }
            if (!empty($toDisplay)) {
                $alert = $this->Html->tag(
                    'ul.alert.alert-danger',
                    $this->Html->tag('h4', __("Erreur détectée dans le formulaire"))
                    .implode(PHP_EOL, $toDisplay)
                );
            }
        }
        return $alert.parent::end($secureAttributes);
    }

    /**
     * Permet l'affichage d'erreurs sur des entités liés
     * @param array $errs
     * @return array
     */
    private function flattenErrors(array $errs): array
    {
        $flatten = [];
        foreach (Hash::flatten($errs) as $key => $error) {
            $fieldname = substr($key, 0, strrpos($key, '.'));
            if (!isset($flatten[$fieldname])) {
                $flatten[$fieldname] = [];
            }
            $flatten[$fieldname][] = $error;
        }
        return $flatten;
    }

    /**
     * Creates a `<button>` tag.
     *
     * ### Options:
     *
     * - `type` - Value for "type" attribute of button. Defaults to "submit".
     * - `escapeTitle` - HTML entity encode the title of the button. Defaults to true.
     * - `escape` - HTML entity encode the attributes of button tag. Defaults to true.
     * - `confirm` - Confirm message to show. Form execution will only continue if confirmed then.
     *
     * @param string $title   The button's caption. Not automatically HTML encoded
     * @param array  $options Array of options and HTML attributes.
     * @return string A HTML button tag.
     * @link https://book.cakephp.org/4/en/views/helpers/form.html#creating-button-elements
     */
    public function button(string $title, array $options = []): string
    {
        $options += [
            'escapeTitle' => false,
        ];
        return parent::button($title, $options);
    }
}
