<?php
/**
 * AsalaeCore\View\Helper\Object\ObjectInterface
 */

namespace AsalaeCore\View\Helper\Object;

/**
 * Interface des objets issue des Helpers
 *
 * @category Helper
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
interface ObjectInterface
{
    /**
     * Récupère le HTML de l'objet
     * @param array $params
     * @param mixed ...$args
     * @return string
     */
    public function generate(array $params = [], ...$args): string;

    /**
     * Appel self::generate() sans spécifier de paramètres
     * @return string
     */
    public function __toString(): string;
}
