<?php
/**
 * AsalaeCore\View\Helper\Object\AjaxPaginator
 */

namespace AsalaeCore\View\Helper\Object;

use AsalaeCore\View\Helper\AjaxPaginatorHelper;
use Cake\View\Helper;

/**
 * Objet crée par AsalaeCore\View\Helper\AjaxPaginatorHelper
 *
 * @category Helper
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class AjaxPaginator implements ObjectInterface
{
    /**
     * @var string identifiant DOM de la section de pagination
     */
    private $id;

    /**
     * @var AjaxPaginatorHelper parent
     */
    private $Helper;

    /**
     * @var ObjectInterface|Table tableau
     */
    private $Table;

    /**
     * @var string nom de la variable javascript qui porte le Tableau
     */
    private $table;

    /**
     * Initialisation d'un paginateur
     * @param Helper $helper
     * @param string $id
     */
    public function __construct(Helper $helper, string $id)
    {
        $this->Helper = clone $helper;
        $this->id = $id;
    }

    /**
     * Défini l'url de base
     * @param array $url
     * @return AjaxPaginator
     */
    public function url(array $url)
    {
        $this->Helper->setUrl($url);
        return $this;
    }

    /**
     * Défini le tableau js sur lequel il faudra remplacer les données
     * @param string|Table $table nom de l'object javascript du générateur de tableau
     * @return AjaxPaginator
     */
    public function table($table)
    {
        if ($table instanceof Table) {
            $this->Table = $table;
            $table = $table->tableObject;
        }
        $this->table = $table;
        return $this;
    }

    /**
     * Défini le nombre de résultats
     * @param int $count
     * @return $this
     */
    public function count(int $count)
    {
        $this->Helper->setCount($count);
        return $this;
    }

    /**
     * Appel self::generate() sans spécifier de paramètres
     * @return string
     */
    public function __toString(): string
    {
        return $this->generate();
    }

    /**
     * Récupère le HTML de l'objet
     * @param array $params
     * @param mixed ...$args
     * @return string
     */
    public function generate(array $params = [], ...$args): string
    {
        return $this->Helper->Html->tag(
            'div',
            $this->Helper->blocPagination(
                $this->id,
                $params + ['url' => $this->Helper->getUrl()]
            ),
            ['id' => $this->id, 'class' => 'd-flex']
        ) . $this->generateJavascript();
    }

    /**
     * Génère le javascript pour un bloc de pagination ajax
     * @param string|null $id
     * @return string
     */
    public function generateJavascript(string $id = null): string
    {
        $id = $id ?: $this->id;
        $tableVar = $this->table;
        $params = json_encode(
            $this->Helper->params() + [
                'table' => $tableVar
            ]
        );
        /** @noinspection JSUnnecessarySemicolon BadExpressionStatementJS */
        return <<<EOT
<script>
$(function() {
new AsalaePaginator('#$id', $params);
});
</script>
EOT;
    }

    /**
     * Bouton scroll down
     * @param array $paramsContainer
     * @return string
     */
    public function scrollBottom(array $paramsContainer = []): string
    {
        return $this->Helper->scrollBottom($paramsContainer);
    }

    /**
     * Bouton scroll up
     * @param array $paramsContainer
     * @return string
     */
    public function scrollTop(array $paramsContainer = []): string
    {
        return $this->Helper->scrollTop($paramsContainer);
    }

    /**
     * Génère un tableau, avec les boutons de scrolls et la pagination
     * @return string
     */
    public function generateTable(): string
    {
        return $this->scrollBottom() . $this->Table . $this . $this->scrollTop();
    }
}
