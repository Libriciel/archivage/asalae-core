<?php

/**
 * AsalaeCore\View\Helper\Object\Upload
 */

namespace AsalaeCore\View\Helper\Object;

use AsalaeCore\View\Helper\UploadHelper;
use Cake\Utility\Inflector;
use Cake\View\Helper;
use Exception;

/**
 * Objet crée par AsalaeCore\View\Helper\UploadHelper
 *
 * @category Helper
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Upload implements ObjectInterface
{

    /**
     * @var UploadHelper parent
     */
    private $Helper;

    /**
     * @var array Paramètres du Upload
     */
    public $params = [];

    /**
     * @var string div de la dropbox
     */
    public $dropbox;

    /**
     * @var string id de la dropbox
     */
    public $dropboxId;

    /**
     * @var Table object
     */
    public $table;

    /**
     * @var string nom de la variable AsalaeUploader
     */
    public $jsObject;

    /**
     * Initialise la création d'une section de tabs
     * @param  Helper $helper
     * @param  string $domId
     * @param  array  $params
     * @throws Exception
     */
    public function __construct(Helper $helper, string $domId, array $params = [])
    {
        $this->Helper = $helper;
        $params += [
            'iClass' => 'fa fa-upload fa-3x text-primary',
            'pText' => __('Glissez-déposez vos fichiers ici'),
            'inputName' => 'fileupload_id',
            'inputRequired' => false,
        ];
        $i = $this->Helper->Html->tag(
            'i',
            '',
            ['class' => $params['iClass']]
        );
        unset($params['iClass']);
        $p = $this->Helper->Html->tag(
            'p',
            $params['pText']
        );
        $button = $this->Helper->Html->tag(
            'div',
            $this->Helper->Fa->i('fa-folder-open', __("Parcourir...")),
            ['class' => 'btn btn-success', 'tabindex' => 0]
        );

        $this->dropbox = $this->Helper->Html->tag(
            'div',
            $i . $p . $button,
            ['class' => 'dropbox', 'id' => $domId]
        );

        $this->dropboxId = $domId;
        $this->table = new Table($this->Helper->Table, $this->Helper->getTableId($domId), $params);
        $this->jsObject = Inflector::camelize(
            'uploader_' . preg_replace('/\W/', '_', $this->dropboxId)
        );
        $this->params = $params;
    }

    /**
     * Renseigne les champs utilisable
     * @param array $fields
     * @return self
     */
    public function fields(array $fields): self
    {
        $this->table->fields($fields);
        return $this;
    }

    /**
     * Renseigne les champs utilisable
     * @param  mixed $data
     * @return self
     */
    public function data($data): self
    {
        $this->table->data($data);
        return $this;
    }

    /**
     * Paramètres du tableau
     * @param  array $params
     * @return self
     * @throws Exception
     */
    public function params(array $params): self
    {
        $this->table->params($params);
        return $this;
    }

    /**
     * Permet d'insérer des actions sur le tableau de résultat
     * @param  array $actions
     * @return self
     */
    public function actions(array $actions)
    {
        $this->table->actions($actions);
        return $this;
    }

    /**
     * Permet d'obtenir le lien pour la configuration du tableau
     * @param array $params Pour configurer le tag "A"
     * @return string
     */
    public function getConfigureLink(array $params = []): string
    {
        return $this->table->getConfigureLink($params);
    }

    /**
     * Permet d'obtenir le json de paramètre à passer au javascript
     * @return string json
     */
    public function getParamsJson(): string
    {
        return $this->table->getParamsJson();
    }

    /**
     * Permet d'obtenir une dropbox et son javascript
     *
     * @param array $params  Configuration supplémentaire à envoyer à
     *                       AsalaeUploader
     * @param array ...$args
     * @return string
     */
    public function generate(array $params = [], ...$args): string
    {
        $params += [
            'id' => $this->dropboxId,
            'table' => $this->table->tableObject,
            'target' => $this->Helper->config['target'],
            'chunkSize' => $this->Helper->config['chunkSize'],
            'simultaneousUploads' => $this->Helper->config['simultaneousUploads'],
            'thumbnail_path' => '/download/thumbnail-image?path=',
            'download_path' => '/download/file?path=',
            'inputRequired' => false,
        ];

        $obj = $this->jsObject;
        $json = json_encode($params);
        $script = $this->Helper->Html->tag(
            'script',
            <<<eot
$obj = new AsalaeUploader($json);
new GenericUploader($obj);

eot
        );
        if ($params['inputRequired']) {
            $required = $this->Helper->Html->tag(
                'input',
                '',
                [
                    'name' => $params['inputName'],
                    'class' => 'required-anchor',
                    'type' => 'radio',
                    'style' => 'transform : scale(0,0);',
                    'tabindex' => '-1',
                    'aria-hidden' => true,
                    'required' => true,
                ]
            );
        } else {
            $required = '';
        }

        return $this->dropbox . $this->table . $required . $script;
    }

    /**
     * Appel self::generate() sans spécifier de paramètres
     * @return string
     * @throws Exception
     */
    public function __toString(): string
    {
        return $this->generate();
    }
}
