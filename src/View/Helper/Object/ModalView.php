<?php
/**
 * AsalaeCore\View\Helper\Object\ModalView
 */

namespace AsalaeCore\View\Helper\Object;

use AsalaeCore\View\Helper\ModalViewHelper;
use Cake\View\Helper;
use Exception;

/**
 * Objet crée par AsalaeCore\View\Helper\ModalViewHelper
 *
 * @category Helper
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ModalView implements ObjectInterface
{
    /**
     * @var string Identifiant de la modal
     */
    public $id;

    /**
     * @var array id, class etc...
     */
    public $params;

    /**
     * @var string Ce que contient la modal
     */
    public $content = '';

    /**
     * @var string Callback javascript, appelé après l'envoi du formulaire
     */
    public $callback = '';

    /**
     * @var string Titre de la modal
     */
    public $title = '';

    /**
     * @var array Options pour le rendu du bouton d'activation de la modale
     */
    public $output = [];

    /**
     * @var array Options pour le Modal->body()
     */
    public $bodyParams = [];

    /**
     * @var ModalViewHelper parent
     */
    private $Helper;

    /**
     * @var string bouton retour
     */
    public $returnButton;

    /**
     * ModalView constructor.
     * @param Helper $helper
     * @param string $id
     * @param array  $params
     */
    public function __construct(Helper $helper, string $id, array $params)
    {
        $this->Helper = $helper;
        $params += [
            'returnButton' => $this->Helper->Form->button(
                $this->Helper->Fa->i('fa-arrow-left', __("Retour")),
                [
                    'data-dismiss' => 'modal',
                    'class' => 'cancel',
                    'type' => 'button',
                ]
            )
        ];
        $this->id = $id;
        $this->returnButton = $params['returnButton'];
        unset($params['returnButton']);
        $this->params = $params + [
            'id' => $id,
            'class' => 'ui-draggable-handle'
        ];
    }

    /**
     * Défini le titre et le contenu de la modal
     * @param string $title
     * @param string $content
     * @param array  $bodyParams
     * @return self
     */
    public function modal(string $title, string $content = '', array $bodyParams = []): self
    {
        $this->content = $content ?: $this->Helper->Html->tag(
            'div',
            $this->Helper->Html->image('ajax-loader.gif', ['alt' => __("Chargement...")]),
            ['class' => 'text-center']
        );
        $this->title = $title;
        $this->bodyParams = $bodyParams;

        return $this;
    }

    /**
     * Défini le callback javascript lié à la modal
     * Envoi les attributs javascript suivant :
     * (content, textStatus, jqXHR)
     * @param string $funcName
     * @return self
     * @throws Exception
     */
    public function javascriptCallback(string $funcName): self
    {
        if (strpos($funcName, '(')) {
            throw new Exception(
                __(
                    "Le callback sera appelé avec les arguments suivants : 
                    (content, textStatus, jqXHR), ne pas indiquer de parentèses"
                )
            );
        }
        $this->callback = $funcName . '(content, textStatus, jqXHR);';
        return $this;
    }

    /**
     * Génère un fonction javacript qui pourra être appelé pour ouvrir la modal
     * @param string       $function
     * @param string|array $url
     * @return string
     */
    public function generateFunction(string $function, $url): string
    {
        $modalId = $this->params['id'];
        $callback = $this->callback ?: '';

        $script = <<<EOT
<!--suppress JSConstantReassignment -->
<script>
    $function = function() {
        var id = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
        AsalaeModal.append($('#$modalId').modal('show'));
        {$this->javascriptFunctionContent($this->Helper->Url->build($url), $modalId, $callback, '+id')}
    };
    $('#$modalId').find('div.modal-header button.refresh').on('click.refresh-modal', function() {
        $function($('#$modalId').attr('data-id'));
    });
</script>
EOT;
        return $this->getModal().$script;
    }

    /**
     * Javascript de la modal
     * @param string $url
     * @param string $modalId
     * @param string $callback
     * @param string $id
     * @return string
     */
    private function javascriptFunctionContent(string $url, string $modalId, string $callback, string $id = ''): string
    {
        $content = addcslashes($this->content, "'");
        return <<<EOT
        $('#$modalId').attr('data-id', ''$id);
        var body = $('#$modalId').find('div.modal-body');
        var html = '$content';
        body.find('*').off().remove();
        $(html).each(function() {
            if (this.id) {
                $('[id="'+this.id+'"]').remove();
            }
            $(this).find('[id]').each(function() {
                $('[id="'+this.id+'"]').remove();
            });
        });
        body.html(html);
        var chrono = function() {
            return (Date.now ? Date.now() : new Date().getTime());
        };
        var duration = chrono() + 400;
        var xhr = $.ajax({
            url: '$url/'$id,
            success: function(content, textStatus, jqXHR) {
                setTimeout(function() {
                    body.find('*').off().remove();
                    try {
                        $(content).each(function() {
                            if (this.id) {
                                $('[id="'+this.id+'"]').remove();
                            }
                            $(this).find('[id]').each(function() {
                                $('[id="'+this.id+'"]').remove();
                            });
                        });
                    } catch (e) {
                    }
                    body.html(content);
                    $callback
                }, Math.max(0, duration - chrono()));
            },
            error: function(error) {
                setTimeout(function() {
                    AsalaeModal.error(error, $('#$modalId'));
                }, Math.max(0, duration - chrono()));
            },
            complete: function() {
                setTimeout(function() {
                    AsalaeGlobal.removeWaitingAjaxResponse('#$modalId');
                    AsalaeGlobal.collapseDebug();
                    AsalaeDownloads.getInstance().handleDownloads();
                }, Math.max(0, duration - chrono()));
            }
        });
        $('#$modalId').one('hide.bs.modal', function () {
            xhr.abort();
        });
EOT;
    }

    /**
     * Permet de définir de quel type sera la sortie HTML
     * ex: un lien, un bouton, une fonction...
     * @param string       $type
     * @param string       $text
     * @param string|array $url
     * @return self
     */
    public function output(string $type, string $text, $url): self
    {
        $this->output = compact('type', 'text', 'url');
        return $this;
    }

    /**
     * Génère la modal à partir des options défini précedement
     * @param array $params
     * @param mixed ...$args
     * @return string
     * @throws Exception
     */
    public function generate(array $params = [], ...$args): string
    {
        if (empty($this->output)) {
            throw new Exception(
                __(
                    "Vous devez faire appel à la méthode ModalView::output()
                     avant d'utiliser ModalView::generate()."
                )
            );
        }
        if ($this->output['type'] === 'function') {
            return $this->generateFunction($this->output['text'], $this->output['url']);
        }

        return $this->getModal().$this->getButton($params);
    }

    /**
     * Génère le boutton, à utiliser avec $this->getModal()
     * @param array $params
     * @return string
     * @throws Exception
     */
    public function getButton(array $params = []): string
    {
        if (empty($this->output)) {
            throw new Exception(
                __(
                    "Vous devez faire appel à la méthode ModalView::output()
                     avant d'utiliser ModalView::getButton()."
                )
            );
        }
        $modalId = $this->params['id'];
        $btnId = $this->output['type'].'-'.$this->id;
        $btn = $this->Helper->Html->tag(
            $this->output['type'],
            $this->output['text'],
            $params + [
                'id' => $btnId,
                'class' => 'btn btn-primary',
            ]
        );
        $callback = $this->callback ?: '';
        $jsfunc = $this->javascriptFunctionContent(
            $this->Helper->Url->build($this->output['url']),
            $modalId,
            $callback,
            '+id'
        );

        /** @noinspection JSJQueryEfficiency */
        $script = <<<EOT
<script>
    (function() {
        var fn = function() {
            var id = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
            AsalaeModal.append($('#$modalId').modal('show'));
            $jsfunc
        };
        $('#$btnId').on('click.open-modal', function() {
            fn($('#$modalId').attr('data-id'));
        });
        $('#$modalId').find('div.modal-header button.refresh').on('click.refresh-modal', function() {
            fn($('#$modalId').attr('data-id'));
        });
    })();
</script>
EOT;
        return $btn.$script;
    }

    /**
     * Construit la modal
     * @param array $params
     * @return string
     */
    public function getModal($params = []): string
    {
        $params += $this->params;
        return $this->Helper->Modal->create($params)
            . $this->Helper->Modal->header($this->title)
            . $this->Helper->Modal->body($this->content, $this->bodyParams)
            . $this->Helper->Modal->end(
                $this->returnButton
            )
            . $this->Helper->modalScript($params['id']);
    }

    /**
     * Appel self::generate() sans spécifier de paramètres
     * @return string
     * @throws Exception
     */
    public function __toString(): string
    {
        return $this->generate();
    }
}
