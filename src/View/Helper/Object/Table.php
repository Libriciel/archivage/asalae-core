<?php
/**
 * AsalaeCore\View\Helper\Object\Table
 */

namespace AsalaeCore\View\Helper\Object;

use AsalaeCore\View\Helper\AjaxPaginatorHelper;
use AsalaeCore\View\Helper\TableHelper;
use Cake\Datasource\EntityInterface;
use Cake\I18n\I18n;
use Cake\ORM\ResultSet;
use Cake\View\Helper;
use Cake\ORM\Entity;
use Cake\Utility\Hash;
use Exception;
use Traversable;

/**
 * Objet crée par AsalaeCore\View\Helper\TableHelper
 *
 * @category Helper
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Table implements ObjectInterface
{
    /**
     * @var string Contient le HTML de la table en construction
     */
    private $html;

    /**
     * @var string Contient l'id de la table
     */
    private $id;

    /**
     * @var array Contient les champs
     */
    private $fields = [];

    /**
     * @var string Contient les données
     */
    private $data = '[]';

    /**
     * @var array Contient les actions
     */
    private $actions = [];

    /**
     * @var string nom de l'objet javascript
     */
    public $tableObject = '';

    /**
     * @var array Paramètres du tableau
     */
    public $params = [];

    /**
     * @var TableHelper parent
     */
    private $Helper;

    /**
     * @var AjaxPaginatorHelper
     */
    private $AjaxPaginatorHelper;

    /**
     * @var bool
     */
    private $useAjax = false;

    /**
     * @var array
     */
    private $url;

    /**
     * Initialise la création d'un tableau HTML
     * @param  Helper $helper
     * @param  string $id
     * @param  array  $params
     * @throws Exception
     */
    public function __construct(Helper $helper, string $id, array $params = [])
    {
        if (empty($id)) {
            throw new Exception(__('Vous devez saisir un identifiant pour le tableau'));
        }

        $this->Helper = $helper;
        $tableVar = $this->Helper->getJsTableObject($id);
        $this->tableObject = $tableVar;
        $params['id'] = $id;
        $params += [
            'class' => $this->Helper->defaultCssClass,
            'data-for' => $tableVar
        ];

        $this->id = $id;
        $this->html = $this->Helper->Html->tag('table', '', $params);
        $this->AjaxPaginatorHelper = new AjaxPaginatorHelper($this->Helper->getView());
    }

    /**
     * Défini l'url de base
     * @param array $url
     * @return Table
     */
    public function url(array $url)
    {
        $this->url = $url;
        $this->AjaxPaginatorHelper->setUrl($url);
        $this->useAjax = true;
        return $this;
    }

    /**
     * Permet d'obtenir le Helper (parent de l'objet)
     * @return TableHelper
     */
    public function getHelper(): TableHelper
    {
        return $this->Helper;
    }

    /**
     * Récupère le HTML de l'objet
     * @param array $params
     * @param mixed ...$args
     * @return string
     * @throws Exception
     */
    public function generate(array $params = [], ...$args): string
    {
        $this->isCompleteOrDie();
        $paramsJson = $this->getParamsJson();
        $tableVar = $this->tableObject;

        $actions = $this->actions
            ? json_encode($this->formatActions(), $this->Helper->jsonStyle)
            : 'null';

        $messages = json_encode(
            [
                'favorite' => __('Ajouter/supprimer des favoris'),
                'confirmSwitchView' => __('Voulez vous enregistrer vos modifications ?'),
                'confirmChangeMaxPerPage' => __(
                    "Vous devez recharger la page pour prendre en compte "
                    ."toutes les modifications. Voulez-vous le faire maintenant ?"
                ),
                'selectCheckbox' => __("Select"),
                'selectAllCheckboxes' => __("Select all")
            ],
            $this->Helper->jsonStyle
        );


        $local = I18n::getLocale();
        if (strpos($local, '_')) {
            /** @noinspection PhpUnusedLocalVariableInspection */
            [$lang, $territory] = explode('_', I18n::getLocale());
        } else {
            $lang = $local;
        }

        $formAction = $this->url
            ? "'".$this->Helper->Url->build($this->url, ['escape' => false])."'"
            : '("" + window.location)';

        $id = $this->id;
        $data = $this->data;
        /** @noinspection CommaExpressionJS, JSUnusedAssignment */
        $javascript = <<<EOD
<!--suppress JSConstantReassignment, JSUnresolvedFunction, UnreachableCodeJS, UnusedAssignment, JSValidateTypes -->
<script>
    TableHelper.locale('$lang');
    var $tableVar = new TableGenerator($('#$id'));
    $tableVar.messages = $messages;
    $tableVar.data = $data;
    $tableVar.actions = $actions;

    if ($tableVar.actions) {
        $tableVar.data = TableGenerator.appendActions($tableVar.data, $tableVar.actions);
    }
    $tableVar.params($paramsJson);
    $tableVar.applyCookies();
    $tableVar.generateAll();
    $tableVar.insertConfiguration('#modal-$id');
    
    $tableVar.table.off('created.form').on('created.form', function(e, data) {
        var container = $(data.form).on('submit', function(e) {
            e.preventDefault();
            e.stopPropagation();
            var url = $formAction;
            url += (url.indexOf('?') !== -1 ? '&' : '?');
            var formdata = AsalaeFilter.getFiltersData(this);
            AsalaeGlobal.interceptedLinkToAjax(url + formdata);
            $(document).off('click.outside.form');
            container.toggle('drop', function() {
                $(this).remove();
            });
        });
        setTimeout(function() {
            $(document).off('click.outside.form').on('click.outside.form keydown.outside.form', function(e) {
                if (e.keyCode === 27
                    || ((!container.is(e.target) && container.has(e.target).length === 0)
                    && $(document).has(e.target).length > 0)
                ) {
                    $(document).off('click.outside.form');
                    container.toggle('drop', function() {
                        $(this).remove();
                    });
                }
            });
            container.find('input').on('keydown', function(e) {
                if (e.keyCode === 13) {
                    e.preventDefault();
                    e.stopPropagation();
                    $(this).closest('form').submit();
                }
            });
        }, 0);
    });
</script>
EOD;

        return $this->html . $javascript;
    }


    /**
     * Renseigne les champs utilisable
     * @param array $fields
     * @return self
     */
    public function fields(array $fields): self
    {
        $this->fields = Hash::normalize($fields);

        foreach ($this->fields as $fieldname => $params) {
            $field = $this->fieldParams($fieldname, (array)$params);
            if (!empty($field['order'])) {
                if ($field['order'] === true) {
                    $field['order'] = $fieldname;
                }
                $label = $field['label'];
                $sr = $this->Helper->Html->tag('span', __("Trier par").' '.$label, ['class' => 'sr-only']);
                $field['label'] = [
                    'label' => '<span class="field">' . $label . '&nbsp;</span>',
                    'sort' => $this->useAjax
                        ? $this->AjaxPaginatorHelper->sort(
                            $field['order'],
                            $sr,
                            [
                                'asc' => ['title' => __("Trier par ''{0}'' par ordre croissant", $label)],
                                'desc' => ['title' => __("Trier par ''{0}'' par ordre décroissant", $label)],
                                'escape' => false,
                                'url' => $this->AjaxPaginatorHelper->getUrl() ?: [],
                            ]
                        )
                        : $this->Helper->Paginator->sort(
                            $field['order'],
                            $sr,
                            [
                                'asc' => ['title' => __("Trier par ''{0}'' par ordre croissant", $label)],
                                'desc' => ['title' => __("Trier par ''{0}'' par ordre décroissant", $label)],
                                'escape' => false,
                                'url' => $this->AjaxPaginatorHelper->getUrl() ?: [],
                            ]
                        )
                ];
            }
            $this->fields[$fieldname] = $field;
        }

        return $this;
    }

    /**
     * Permet d'obtenir les paramètres d'un field
     * @param string $fieldname
     * @param array  $params
     * @return array
     */
    private function fieldParams(string $fieldname, array $params): array
    {
        if (!isset($params['label'])) {
            $params['label'] = $this->translate($fieldname);
        }
        if (isset($params['type'])) {
            switch ($params['type']) {
                case 'date':
                    unset($params['type']);
                    $params['callback'] = 'TableHelper.date("LL")';
                    break;
                case 'datetime':
                    unset($params['type']);
                    $params['callback'] = 'TableHelper.date("LLL")';
                    break;
                case 'bool':
                case 'boolean':
                    unset($params['type']);
                    $params['callback'] = 'TableHelper.boolean("'.__("Oui").'", "'.__('Non').'")';
                    break;
            }
        }
        if (isset($params['thead'])) {
            $params['thead'] = Hash::normalize($params['thead']);
            foreach ($params['thead'] as $subFieldname => $subParams) {
                $params['thead'][$subFieldname]
                    = $this->fieldValue($subFieldname, (array)$subParams);
            }
        }

        return $params;
    }

    /**
     * Assure une utilisation de la méthode toArray() sur toutes les profondeurs
     * sans modification de la structure de données
     * @param mixed $data
     */
    private function dataToArray(&$data)
    {
        if ($data instanceof ResultSet) {
            $newData = [];
            foreach ($data as $value) {
                if ($value instanceof Entity) {
                    $newData[][$value->getSource()] = $value->toArray();
                }
            }
            $data = $newData;
        }
        if ($data instanceof Entity) {
            $data = $data->toArray();
        }
        foreach ($data as $key => $value) {
            if ($value instanceof Entity) {
                $source = $value->getSource();
                $value = $value->toArray();
                if (is_numeric($key)) {
                    $data[$key][$source] = $value;
                } else {
                    $data[$key] = $value;
                }
                $this->dataToArray($data[$key]);
            } elseif (is_array($value) || $value instanceof Traversable) {
                $this->dataToArray($data[$key]);
            } elseif (is_resource($value)) {
                rewind($value);
                $data[$key] = stream_get_contents($value);
            }
        }
    }

    /**
     * Applique un h() sur les données
     * @param mixed $data
     * @param array $path
     * @param bool  $escape
     * @deprecated depuis le 13/01/2021 - l'échapement se fait dans ls js désormais
     * @todo retirer la fonction après s'être assuré qu'il n'y ai pas de problèmes
     * @noinspection PhpUnusedPrivateMethodInspection
     */
    private function sanitizeData(&$data, $path = [], bool $escape = true)
    {
        foreach ($data as $key => $value) {
            $spath = $path;
            if ($value instanceof EntityInterface) {
                $data[$key] = $value->toArray();
                $value = $data[$key];
            }
            if (is_array($value)
                || $value instanceof Traversable
            ) {
                $skey = '';
                if ($value instanceof EntityInterface) {
                    $skey = $value->getSource();
                }
                if (is_numeric($key)) {
                    if ($skey) {
                        $spath[] = $skey;
                    }
                } else {
                    $spath[] = $key;
                }
                $nEscape = $escape;
                $pkey = implode('.', $spath);
                if ($escape && isset($this->fields[$pkey]['escape']) && !$this->fields[$pkey]['escape']) {
                    $nEscape = false;
                }
                /** @noinspection PhpDeprecationInspection */
                $this->sanitizeData($data[$key], $spath, $nEscape);
            } elseif (is_string($value)) {
                $spath[] = $key;
                $pkey = implode('.', $spath);
                if (!$escape || (isset($this->fields[$pkey]['escape']) && !$this->fields[$pkey]['escape'])) {
                    $data[$key] = $value;
                } else {
                    $data[$key] = h($value);
                }
            }
        }
    }

    /**
     * Renseigne les champs utilisable
     * @param  mixed $data
     * @return self
     */
    public function data($data): self
    {
        $this->dataToArray($data);
        $this->data = json_encode($data, $this->Helper->jsonStyle);

        return $this;
    }

    /**
     * Paramètres du tableau
     * @param  array $params
     * @return self
     * @throws Exception
     */
    public function params(array $params): self
    {
        if (!Hash::get($params, 'identifier')) {
            throw new Exception(
                __("Impossible de trouver la paramètre 'identifier'.")
            );
        }
        $this->params = $params;
        if (!empty($this->params['favorites'])
            && is_bool($this->params['favorites'])
        ) {
            $sr = $this->Helper->Html->tag(
                'span',
                __("Filtrer par favoris"),
                ['class' => 'sr-only']
            );
            $url = $this->Helper->Paginator->generateUrlParams(
                ['favoris' => ['1']],
                $params['model'] ?? $this->Helper->Paginator->defaultModel()
            );
            $this->params['favorites'] = $this->Helper->Html->tag(
                'a',
                $this->Helper->Fa->i('fa-filter', $sr),
                [
                    'class' => 'btn btn-link btn-filter',
                    'href' => $this->Helper->Url->build($url, ['escape' => false]),
                ]
            );
        }

        return $this;
    }

    /**
     * Permet d'insérer des actions sur le tableau de résultat
     * @param array|callable $actions
     * @return self
     */
    public function actions($actions)
    {
        if (is_callable($actions)) {
            $actions = call_user_func($actions, $this, $this->Helper->getView());
        }
        foreach ($actions as $action) {
            if (is_array($action)) {
                $this->actions[] = $action;
            } elseif (is_callable($action)) {
                $this->actions[] = call_user_func($action, $this, $this->Helper->getView());
            }
        }
        return $this;
    }

    /**
     * Permet d'obtenir le lien pour la configuration du tableau
     * @param array $params Pour configurer le tag "A"
     * @return string
     */
    public function getConfigureLink(array $params = []): string
    {
        $params += [
            'class' => 'float-right btn-link',
            'data-toggle' => 'modal',
            'data-target' => '#modal-'.$this->id,
            'content' => $this->Helper->Fa->i(
                'fa-sliders',
                '',
                ['title' => __("Paramétrage du tableau de résultat")]
            )
            .$this->Helper->Html->tag(
                'span',
                __("Paramétrage du tableau de résultat"),
                ['class' => 'sr-only']
            )
        ];
        $content = $params['content'];
        unset($params['content']);
        return $this->Helper->Html->tag('button', $content, $params);
    }

    /**
     * Permet d'obtenir le json de paramètre à passer au javascript
     * @return string json
     */
    public function getParamsJson(): string
    {
        $paramsJs = [];

        foreach ($this->fields as $fieldname => $params) {
            $params = (array)$params;
            $paramsJs['thead'][0][] = $this->fieldValue($fieldname, $params);
        }

        if ($this->actions) {
            $paramsJs['thead'][0][] = [
                '_id' => 'actions',
                'label' => __('Actions'),
                'title' => '',
                'link' => false,
                'type' => 'th',
                'data-view' => 'column',
                'display' => true,
                'colspan' => 1,
                'target' => 'actions',
                'thead' => $this->actionsToThead(),
                'callback' => null,
                'class' => 'action'
            ];
        }

        $paramsJs['tbody'] = $this->params;
        return json_encode($paramsJs, $this->Helper->jsonStyle);
    }

    /**
     * Donne un champ avec tout ses paramètres
     *
     * '_id' => identifiant unique de l'objet
     * 'label' => Nom qui sera affiché à l'utilisateur
     * 'title' => attribut title de l'objet
     * 'link' => url qui sera inséré dans le 'th' pour le tri sur colonne
     * 'type' => 'th' ou 'td' pour la colonne dans le thead
     * 'data-view' => default, div ou column pour le basculement d'une vue à
     *                l'autre
     * 'display' => colonne affiché ou non par défaut
     * 'colspan' => attribut colspan de la colonne
     * 'target' => Pointeur vers la donnée dans "$results[index]"
     *              ex: Monmodel.field
     * 'thead' => Si la colonne en contient d'autres, les informations seront
     *            contenues ici
     * 'callback' => function javascript qui reçois la valeur de la colonne en
     *               paramètre et renvoi la valeur modifié
     * @param string $fieldname
     * @param array  $params
     * @return array
     */
    private function fieldValue(string $fieldname, array $params): array
    {
        $display = !(Hash::get($params, 'display') === false
            || Hash::get($params, 'display') === 'none');
        $label = !isset($params['label']) ? $fieldname : $params['label'];

        $value = [
            '_id' => preg_replace('/\W/', '_', $fieldname),
            'label' => $label,
            'title' => Hash::get($params, 'title', ''),
            'link' => Hash::get($params, 'link', false),
            'type' => Hash::get($params, 'type', 'th'),
            'data-view' => Hash::get($params, 'data-view', 'default'),
            'display' => $display,
            'displayEval' => Hash::get($params, 'displayEval'),
            'colspan' => Hash::get($params, 'colspan', 1),
            'target' => $params['target'] ?? $fieldname,
            'thead' => Hash::get($params, 'thead'),
            'callback' => Hash::get($params, 'callback'),
            'style' => Hash::get($params, 'style'),
            'class' => Hash::get($params, 'class'),
            'filter' => Hash::get($params, 'filter'),
        ] + $params;

        if ($value['thead']) {
            $value['thead'] = Hash::normalize((array)$value['thead']);
            foreach ($value['thead'] as $subfield => $subparams) {
                $value['thead'][$subfield]
                    = $this->fieldParams($subfield, (array)$subparams);
            }
        }
        if ($value['filter']) {
            $value['filter'] = Hash::normalize((array)$value['filter']);
            foreach ($value['filter'] as $fieldname => $params) {
                $value['filter'][$fieldname]
                    = $this->Helper->Form->control($fieldname, $params);
            }
        }

        return $value;
    }

    /**
     * Vérifi que toutes les méthodes obligatoire ont été appelés
     * @throws Exception
     */
    private function isCompleteOrDie()
    {
        if (empty($this->fields)) {
            throw new Exception(__('Il faut appeler la méthode {0}() du helper.', 'field'));
        } elseif (empty($this->data)) {
            throw new Exception(__('Il faut appeler la méthode {0}() du helper.', 'data'));
        } elseif (empty($this->params)) {
            throw new Exception(__('Il faut appeler la méthode {0}() du helper.', 'params'));
        } elseif (empty($this->html)) {
            throw new Exception(__('Il faut appeler la méthode {0}() du helper.', 'create'));
        }
    }

    /**
     * Transforme les actions en json exploitable par le javascript
     * @return array
     */
    private function actionsToThead(): array
    {
        $thead = [];
        foreach ($this->formatActions() as $actionName => $action) {
            $display = !(Hash::get($action, 'display') === false
                || Hash::get($action, 'display') === 'none');

            $thead[$actionName] = [
                '_id' => $actionName,
                'label' => false,
                'title' => Hash::get($action, 'title') ?: $actionName,
                'link' => false,
                'type' => 'th',
                'data-view' => 'default',
                'display' => $display,
                'colspan' => 1,
                'target' => $actionName,
                'thead' => null,
                'callback' => Hash::get($action, 'callback'),
            ];
        }
        $thead['subactions'] = [
            '_id' => 'subactions',
            'type' => 'th',
            'data-view' => 'default',
            'target' => 'subactions',
        ];

        return $thead;
    }

    /**
     * Assure que les actions ont un identifiant
     * @return array
     */
    private function formatActions(): array
    {
        $actions = [];
        foreach ($this->actions as $key => $action) {
            $actionName = is_numeric($key) ? 'action_'.$key : $key;
            $actions[$actionName] = $action;
        }
        return $actions;
    }

    /**
     * Fourni la traduction
     * @param string $name
     * @return string
     */
    private function translate(string $name): string
    {
        if ($name == '') {
            return __('');
        }
        return I18n::getTranslator()->translate($name);
    }

    /**
     * Appel self::generate() sans spécifier de paramètres
     * @return string
     * @throws Exception
     */
    public function __toString(): string
    {
        return $this->generate();
    }
}
