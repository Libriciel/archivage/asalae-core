<?php
/**
 * AsalaeCore\View\Helper\Object\Tabs
 */

namespace AsalaeCore\View\Helper\Object;

use AsalaeCore\View\Helper\TabsHelper;
use Cake\View\Helper;
use Exception;

/**
 * Objet crée par AsalaeCore\View\Helper\TabsHelper
 *
 * @category Helper
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Tabs implements ObjectInterface
{
    /**
     * @var string Contient l'id de la section
     */
    private $id;

    /**
     * @var array Paramètres du tabs
     */
    public $params = [];

    /**
     * @var TabsHelper parent
     */
    private $Helper;

    /**
     * @var array liste d'onglets
     */
    private $ul = [];

    /**
     * @var array liste de content d'onglet
     */
    private $sections = [];

    /**
     * Initialise la création d'une section de tabs
     * @param  Helper $helper
     * @param  string $id
     * @param  array  $params
     * @throws Exception
     */
    public function __construct(Helper $helper, string $id, array $params = [])
    {
        $this->Helper = $helper;
        $this->id = $id;
        $this->params = $params;
    }

    /**
     * Récupère le HTML de l'objet
     * @param array $params
     * @param mixed ...$args
     * @return string
     * @throws Exception
     */
    public function generate(array $params = [], ...$args): string
    {
        $id = $this->id;
        $hidden = '<input type="hidden" name="_tab" class="selected-tab">';
        $ul = $this->Helper->Html->tag('ul', implode(PHP_EOL, $this->ul));
        $content = implode(PHP_EOL, $this->sections);
        $div = $this->Helper->Html->tag(
            'div',
            $hidden.PHP_EOL.$ul.PHP_EOL.$content,
            ['id' => $this->id] + $params + $this->params
        );
        $script = <<<EOT
    var tabs = $('#$id').tabs();
    tabs.find('li.ui-tabs-tab a.ui-tabs-anchor').click(function() {
        tabs.find('input.selected-tab').val($(this).attr('href').substr(1));
    });
    tabs.find('input.selected-tab').val(
        tabs.find('li.ui-tabs-active a.ui-tabs-anchor').attr('href').substr(1)
    );
EOT;
        return $div.PHP_EOL.$this->Helper->Html->tag('script', $script);
    }

    /**
     * Ajoute un onglet au tabs
     * @param string $id
     * @param string $button
     * @param string $content
     * @param array  $params
     * @return Tabs
     */
    public function add(string $id, string $button, string $content, array $params = []): self
    {
        $a = $this->Helper->Html->tag('a', $button, ['href' => '#'.$id] + $params);
        $this->ul[] = $this->Helper->Html->tag('li', $a, $params['li'] ?? []);
        $this->sections[] = $this->Helper->Html->tag(
            'div',
            $content,
            ['id' => $id] + ($params['div'] ?? [])
        );
        return $this;
    }

    /**
     * Appel self::generate() sans spécifier de paramètres
     * @return string
     * @throws Exception
     */
    public function __toString(): string
    {
        return $this->generate();
    }
}
