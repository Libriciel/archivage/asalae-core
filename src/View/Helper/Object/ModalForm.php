<?php
/**
 * AsalaeCore\View\Helper\Object\ModalForm
 */

namespace AsalaeCore\View\Helper\Object;

use AsalaeCore\View\Helper\ModalFormHelper;
use Cake\View\Helper;
use Exception;
use Cake\Core\Configure;

/**
 * Objet crée par AsalaeCore\View\Helper\ModalFormHelper
 *
 * @category Helper
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ModalForm implements ObjectInterface
{
    /**
     * @var string Identifiant de la modal
     */
    public $id;

    /**
     * @var array id, class etc...
     */
    public $params;

    /**
     * @var string Ce que contient la modal
     */
    public $content = '';

    /**
     * @var bool Recharge la page lors de l'envoi du formulaire sans erreurs
     */
    public $refresh = false;

    /**
     * @var string Callback javascript, appelé après l'envoi du formulaire
     */
    public $callback = '';

    /**
     * @var string Titre de la modal
     */
    public $title = '';

    /**
     * @var array Options pour le rendu du bouton d'activation de la modale
     */
    public $output = [];

    /**
     * @var ModalFormHelper parent
     */
    private $Helper;

    /**
     * @var string Bouton annuler
     */
    public $cancelButton;

    /**
     * @var string Bouton Enregistrer
     */
    public $acceptButton;

    /**
     * @var array Options pour le Modal->body()
     */
    public $bodyParams = [];

    /**
     * @var array Etapes
     */
    public $steps = [];

    /**
     * @var array Boutons dans le footer de la modale
     */
    public $buttons = [];

    /**
     * ModalForm constructor.
     * @param Helper $helper
     * @param string $id
     * @param array  $params
     */
    public function __construct(Helper $helper, string $id, array $params)
    {
        $this->Helper = $helper;
        $this->id = $id;
        $this->cancelButton = $params['cancelButton']
            ?? $this->Helper->getDefaultCancelButton();
        $this->acceptButton = $params['acceptButton']
            ?? $this->Helper->getDefaultAcceptButton();
        $this->buttons = $params['buttons'] ?? [
            &$this->cancelButton,
            &$this->acceptButton,
        ];
        unset($params['cancelButton'], $params['acceptButton'], $params['buttons']);
        $this->params = $params + [
            'id' => $id,
            'data-backdrop' => 'static',
            'data-keyboard' => "false",
            'class' => 'ui-draggable-handle',
        ];
    }

    /**
     * Défini le titre et le contenu de la modal
     * @param string $title
     * @param string $content
     * @param array  $bodyParams
     * @return self
     */
    public function modal(string $title, string $content = '', array $bodyParams = []): self
    {
        $this->content = $content ?: $this->Helper->Html->tag(
            'div',
            $this->Helper->Fa->i('fa-4x fa-spinner faa-spin animated')
            . $this->Helper->Html->tag('span.sr-only', 'Loading...'),
            ['class' => 'text-center loading-container']
        );
        $this->title = $title;
        $this->bodyParams = $bodyParams;

        return $this;
    }

    /**
     * Recharge la page en cas de succès
     * @param bool $refresh
     * @return self
     */
    public function refreshOnSuccess(bool $refresh = true): self
    {
        $this->refresh = $refresh;
        return $this;
    }

    /**
     * Défini le callback javascript lié à la modal
     * Envoi les attributs javascript suivant :
     * (content, textStatus, jqXHR)
     * @param string $funcName
     * @return self
     * @throws Exception
     */
    public function javascriptCallback(string $funcName): self
    {
        $this->callback = $funcName . '(content, textStatus, jqXHR);';
        return $this;
    }

    /**
     * Donne le js nécéssaire pour un location.refresh
     * @return string
     */
    private function getRefreshScript()
    {
        return $this->refresh ? "window.onbeforeunload = null;\nlocation.reload();" : '';
    }

    /**
     * Génère un fonction javacript qui pourra être appelé pour ouvrir la modal
     * @param string       $function
     * @param string|array $url
     * @param array        $params
     * @return string
     */
    public function generateFunction(string $function, $url, array $params = []): string
    {
        $params += $this->params;
        $modalId = $params['id'];
        $refresh = $this->getRefreshScript();
        if (empty($params['callback'])) {
            $callback = $this->callback ?: '';
        } else {
            $callback = $params['callback'];
        }

        $javascriptFunctionContent = $this->javascriptFunctionContent(
            $this->Helper->Url->build($url, ['escape' => false]),
            $modalId,
            $refresh,
            $callback,
            '+id',
            $params['ajaxParams'] ?? []
        );
        /** @noinspection ReservedWordAsName */
        $script = <<<EOT
<!--suppress JSValidateTypes -->
<script>
    function $function() {
        var id = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
        var modal = $('#$modalId');
        AsalaeModal.append(modal);
        modal.modal('show');
        $javascriptFunctionContent
    }
</script>
EOT;
        return $this->getModal($params).$script;
    }

    /**
     * Javascript de la modal
     * @param string $url
     * @param string $modalId
     * @param string $refresh
     * @param string $callback
     * @param string $id
     * @param array  $ajaxParams
     * @return string
     */
    private function javascriptFunctionContent(
        string $url,
        string $modalId,
        string $refresh,
        string $callback,
        string $id = '',
        array $ajaxParams = []
    ): string {
        $ajaxParams += [
            'method' => '"GET"',
            'headers' => '{}',
            'data' => '{}',
        ];
        $method = $ajaxParams['method'];
        $headers = $ajaxParams['headers'];
        if (is_array($headers)) {
            $headers = json_encode($headers);
        }
        $data = $ajaxParams['data'];
        unset($ajaxParams['method'], $ajaxParams['headers'], $ajaxParams['data']);
        $othersParams = [];
        foreach ($ajaxParams as $key => $value) {
            $othersParams[] = "\"$key\": $value,";
        }
        $othersParams = implode(' ', $othersParams);
        if (is_array($data)) {
            $data = json_encode($data);
        }
        $debug = '';
        if (Configure::read('debug')) {
            $debug = "default: $('#$modalId div.modal-body').find('.debug-output').remove();".PHP_EOL
                . "$('#$modalId div.modal-body')"
                .".append('<div class=\"debug-output danger\"><h2>Debug output</h2>'+content+'</div>');";
        }
        $content = addcslashes($this->content, "'");
        $message = __("CONFIRM_EXIT_PAGE");
        $confirmCancel = __("Souhaitez-vous fermer le formulaire ? (vous perdrez les informations saisies)");
        $errorEntity = __("Une ou plusieurs erreurs ont été détectés");
        $uFunction = 'modalFormSubmit'.uniqid();
        $queryParams = '';
        if ($pos = mb_strpos($url, '?')) {
            $queryParams = addcslashes(mb_substr($url, $pos), "'");
            $url = mb_substr($url, 0, $pos);
        }
        if ($id) {
            $url .= '/';
        }
        return <<<EOT
window.onbeforeunload = function(e) {
    return e.returnValue = "$message";
};
var queryParams = '$queryParams';
var chrono = function() {
    return (Date.now ? Date.now() : new Date().getTime());
};
var complete = function() {
    var hiddenSubmit = $('<button type="submit" class="hide">Hidden submit</button>');
    AsalaeGlobal.removeWaitingAjaxResponse('#$modalId');
    $('#$modalId')
        .trigger('ajax.complete')
        .find('form')
        .find('button[type="submit"], input[type="submit"]')
        .remove().end()
        .append(hiddenSubmit)
        .find('button:disabled').each(function() {
            if ($(this).data('do-not-enable-after-submit')) {
                $(this).data('do-not-enable-after-submit', false);
            } else {
                $(this).enable();
            }
        })
    ;
};
var $uFunction = function(event) {
    if (event.isDefaultPrevented()) {
        return false;
    }
    var form = $(this).get(0);
    var action = $(this).attr('action');
    if (!action) {
        action = '$url'$id+queryParams;
    }
    event.preventDefault();
    if (form.checkValidity()) {
        $(form).removeClass('display-validation');
        AsalaeGlobal.addWaitingAjaxResponse('#$modalId');
        $(this).find('button').each(function() {
            var isDisabled = $(this).prop('disabled');
            if ($(this).prop('disabled')) {
                $(this).data('do-not-enable-after-submit', true);
            } else {
                $(this).disable();            
            }
        });
        var duration = chrono() + 400;
        $(document).trigger('ajax.begin');
        $.ajax({
            url: action,
            type: 'POST',
            data: $(form).serialize(),
            success: function(content, textStatus, jqXHR) {
                setTimeout(function() {
                    $('#$modalId')
                        .trigger('ajax.success', jqXHR)
                        .find('div.modal-body')
                        .removeClass('alert')
                        .removeClass('alert-danger')
                    switch (jqXHR.getResponseHeader('X-Asalae-Success')) {
                        case 'true':
                            var btns = [];
                            $('#$modalId').find('button:disabled').each(function() {
                                $(this).data('do-not-enable-after-submit', true);
                                btns.push(this);
                            });
                            $('#$modalId').one('hidden.bs.modal', function () {
                                for (var i = 0; i < btns.length; i++) {
                                    $(btns[i]).enable();
                                }
                            }).modal("hide");
                            $refresh
                            break;
                        case 'false':
                            if (typeof content === 'object') {
                                if (typeof content.errors === 'object') {
                                    var message = "$errorEntity\\n\\n";
                                    for (var field in content.errors) {
                                        message += '\\t' + field + ':\\n'
                                        for (var name in content.errors[field]) {
                                            message += '\\t\\t• '+content.errors[field][name]+'\\n';
                                        }
                                    }
                                    alert(message);
                                } else {
                                    this.error(jqXHR);
                                }
                            } else {
                                $(content).each(function () {
                                    if ($(this).attr('id') && $(this).hasClass('modal')) {
                                         $('.modal#'+$(this).attr('id')).remove();
                                    }
                                    $(this).find('.modal[id]').each(function () {
                                        $('.modal#'+$(this).attr('id')).remove();
                                    });
                                });
                                $('#$modalId').find('div.modal-body')
                                    .find('*').off().remove().end()
                                    .html(content);
                                $('#$modalId').find('form')
                                    .off('submit.modalForm')
                                    .on('submit.modalForm', $uFunction);
                                $('#$modalId').find(':invalid')
                                    .filter(
                                        'input:visible, textarea:visible, select:visible, select.chosenified'
                                    )
                                    .first()
                                    .focus();
                                setTimeout(
                                    function() {
                                        $('div.dropbox').off('.draganimation').on(
                                            'dragover.draganimation',
                                             function() {
                                                $(this).addClass('dragover');
                                            }
                                        ).on(
                                            'dragleave.draganimation dragend.draganimation drop.draganimation', 
                                            function() {
                                                $(this).removeClass('dragover');
                                            }
                                        );
                                    },
                                    0
                                );
                            }
                            break;
                        $debug
                    }
                    $callback
                    complete(jqXHR.getResponseHeader('X-Asalae-Success') === 'true');
                }, Math.max(400, duration - chrono()));
            },
            error: function(jqXHR, textStatus, errorThrown) {
                setTimeout(function() {
                    AsalaeModal.error(jqXHR, $('#$modalId'));
                    alert(PHP.messages.genericError);
                    complete(false);
                }, Math.max(400, duration - chrono()));
            }
        });
    } else {
        var invalid = $(form).find('input:invalid, select:invalid, textarea:invalid').first(); 
        if (invalid.is('visible') === false) {
            $('[href="#'+invalid.closest('.ui-tabs-panel').attr('id')+'"]').click();
        }
        form.reportValidity();
        $(form).addClass('display-validation');
    }
};
var body = $('#$modalId').find('div.modal-body');
var html = '$content';
body.find('*').off().remove();
$(html).each(function() {
    if (this.id) {
        $('[id="'+this.id+'"]').remove();
    }
});
body.html(html);
var start = new Date;
var duration = chrono() + 400;
complete = function() {
    var success = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
    var hiddenSubmit = $('<button type="submit" class="hide">Hidden submit</button>');
    $('#$modalId')
        .find('form')
        .off('submit.modalForm')
        .on('submit.modalForm', $uFunction);
    if (!success) {
        AsalaeGlobal.removeWaitingAjaxResponse('#$modalId:visible');
    }
    $('#$modalId:visible').find('form')
        .find('button[type="submit"], input[type="submit"]')
        .remove().end()
        .append(hiddenSubmit)
    ;
    AsalaeGlobal.collapseDebug();
    AsalaeDownloads.getInstance().handleDownloads();
    $('#$modalId:visible').trigger('ajax.complete');
    $(document).trigger('ajax.complete');
};
$(document).trigger('ajax.begin');
var xhr = $.ajax({
    url: '$url'$id+queryParams,
    method: $method,
    data: $data,
    headers: $headers,
    $othersParams
    success: function(content, textStatus, jqXHR) {
        setTimeout(function() {
            var modal = $('#$modalId:visible');
            var body = modal.find('div.modal-body');
            body.find('*').off().remove();
            $(content).each(function() {
                if (this.id) {
                    $('[id="'+this.id+'"]').remove();
                }
                $(this).find('[id]').each(function() {
                    $('[id="'+this.id+'"]').remove();
                });
            });
            body.removeClass('alert')
                .removeClass('alert-danger')
                .html(content);
            modal.trigger('ajax.success', jqXHR);
            body.find('input:visible, textarea:visible, select:visible, select.chosenified')
                .first()
                .not('.hasDatepicker')
                .focus();
            complete(false);
            $('div.dropbox').off('.draganimation').on('dragover.draganimation', function() {
                $(this).addClass('dragover');
            }).on('dragleave.draganimation dragend.draganimation drop.draganimation', function() {
                $(this).removeClass('dragover');
            });
        }, Math.max(400, duration - chrono()));
    },
    error: function(error) {
        setTimeout(function() {
            AsalaeModal.error(error, $('#$modalId')); 
            $('#$modalId').trigger('ajax.error');
            complete(false);
        }, Math.max(400, duration - chrono()));
    }
});

$('#$modalId').find('div.modal-footer button.cancel').off('keydown.modalForm').on('keydown.modalForm', function(event) {
    if (event.keyCode === 13) {
        return confirm('$confirmCancel');
    }
});

$('#$modalId').find('div.modal-footer button.accept').off('click.modalForm').on('click.modalForm', function(event) {
    event.preventDefault();
    var form = $('#$modalId').find('form:visible');
    if (form.length === 0) {
        form = $('#$modalId').find('form');
    }
    form = form.first();
    form.trigger('beforeSubmit.modalForm', event);
    setTimeout(
        function() {
            form.trigger('submit')
                .trigger('afterSubmit.modalForm', event);
        },
        0
    );
});

$('#$modalId').one('hide.bs.modal', function () {
    xhr.abort();
});
EOT;
    }

    /**
     * Permet de définir de quel type sera la sortie HTML
     * ex: un lien, un bouton, une fonction...
     * @param string       $type
     * @param string       $text
     * @param string|array $url
     * @return self
     */
    public function output(string $type, string $text = '', $url = ''): self
    {
        $this->output = compact('type', 'text', 'url');
        return $this;
    }

    /**
     * Génère la modal à partir des options défini précedement
     * @param array $params
     * @param mixed ...$args
     * @return string
     * @throws Exception
     */
    public function generate(array $params = [], ...$args): string
    {
        if (empty($this->output)) {
            throw new Exception(
                __(
                    "Vous devez faire appel à la méthode ModalForm::output()
                     avant d'utiliser ModalForm::generate()."
                )
            );
        }
        if (empty($this->steps)) {
            if ($this->output['type'] === 'function') {
                return $this->generateFunction(
                    $this->output['text'],
                    $this->output['url'],
                    ['ajaxParams' => $params['ajaxParams'] ?? []]
                );
            }

            return $this->getModal().$this->getButton($params);
        }
        $output = '';
        $firstId = null;
        $firstFunc = null;
        foreach ($this->steps as $num => $step) {
            $id = $this->params['id'].'-'.($num +1);
            $firstId = $firstId ?: $id;
            $firstFunc = $firstFunc ?: $step['fn'];
            $options = ['id' => $id, 'ajaxParams' => $params['ajaxParams'] ?? []];
            if ($step['callback']) {
                $options['callback'] = $step['callback'].'(content, textStatus, jqXHR);';
            }

            $output .= $this->generateFunction($step['fn'], $step['url'], $options);
        }
        if ($this->output['type'] !== 'function') {
            $output .= $this->getButton($params, ['id' => $firstId], $firstFunc.'()');
        }
        return $output;
    }

    /**
     * Génère le boutton, à utiliser avec $this->getModal()
     * @param array  $paramsBtn
     * @param array  $params
     * @param string $js
     * @return string
     * @throws Exception
     */
    public function getButton(array $paramsBtn = [], array $params = [], string $js = ''): string
    {
        if (empty($this->output)) {
            throw new Exception(
                __(
                    "Vous devez faire appel à la méthode ModalForm::output() 
                    avant d'utiliser ModalForm::getButton()."
                )
            );
        }
        $params += $this->params;
        $modalId = $params['id'];
        $btnId = $this->output['type'].'-'.$this->id;
        $btn = $this->Helper->Html->tag(
            $this->output['type'],
            $this->output['text'],
            $paramsBtn + [
                'id' => $btnId,
                'class' => 'btn btn-primary',
            ]
        );
        $refresh = $this->getRefreshScript();
        $callback = $this->callback ?: '';
        if (empty($js)) {
            $js = $this->javascriptFunctionContent(
                $this->Helper->Url->build($params['url'] ?? $this->output['url'], ['escape' => false]),
                $modalId,
                $refresh,
                $callback,
                '',
                $params['ajaxParams'] ?? []
            );
        }

        $script = <<<EOT
<script>
    $('#$btnId').on('click.modalform', function() {
        AsalaeModal.append($('#$modalId').modal('show'));
        $js;
    });
</script>
EOT;
        return $btn.$script;
    }

    /**
     * Construit la modal
     * @param array $params
     * @return string
     */
    public function getModal($params = []): string
    {
        $params += $this->params;
        return $this->Helper->Modal->create($params)
            . $this->Helper->Modal->header($this->title, ['close' => false])
            . $this->Helper->Modal->body($this->content, $this->bodyParams)
            . $this->Helper->Modal->end($this->buttons)
            . $this->Helper->modalScript($params['id']);
    }

    /**
     * Appel self::generate() sans spécifier de paramètres
     * @return string
     * @throws Exception
     */
    public function __toString(): string
    {
        return $this->generate();
    }

    /**
     * Ajoute une étape pour une modal en multiSteps
     * @param string $url
     * @param string $fn       fonction javascript à appeler pour poper la
     *                         modale
     * @param string $callback fonction javascript utilisé à la fin de l'étape
     * @return ModalForm
     */
    public function step($url, $fn, $callback = ''): self
    {
        $this->steps[] = compact('url', 'fn', 'callback');
        return $this;
    }
}
