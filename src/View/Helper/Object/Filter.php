<?php
/**
 * AsalaeCore\View\Helper\Object\Filter
 */

namespace AsalaeCore\View\Helper\Object;

use AsalaeCore\Model\Entity\SavedFilter;
use AsalaeCore\View\Helper\FilterHelper;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\Http\ServerRequest;
use Cake\Routing\Router;
use Cake\Utility\Hash;
use Cake\Utility\Inflector;
use Cake\Utility\Text;
use Cake\View\Helper;
use Exception;

/**
 * Objet crée par AsalaeCore\View\Helper\FilterHelper
 *
 * @category Helper
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017-2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Filter implements ObjectInterface
{
    /**
     * @var string Identifiant HTML
     */
    public $id;

    /**
     * @var array Liste des filtres
     */
    public $filters = [];

    /**
     * @var array Liste des filtres permanents
     */
    public $permanentFilters = [];

    /**
     * @var array Save
     */
    public $saves = [];

    /**
     * @var array Liste [id => name] des sauvegardes
     */
    public $saveList = [];

    /**
     * @var array Filtres chargés
     */
    public $loadedFilter = [];

    /**
     * @var int identifiant de la sauvegarde selectionnée
     */
    public $selectedSave = 0;

    /**
     * @var array liste des urls pour les requêtes ajax
     */
    public $urls;

    /**
     * @var FilterHelper parent
     */
    private $Helper;

    /**
     * @var string id unique, fait le lien entre les modales d'un même Filter
     */
    private $uid;

    /**
     * @var ServerRequest
     */
    private $request;

    /**
     * Initialisation d'un bloc de filtres
     * @param Helper $helper
     * @param string $id
     */
    public function __construct(Helper $helper, string $id)
    {
        $this->Helper = $helper;
        $this->id = Inflector::camelize(
            preg_replace('/\W/', '_', $id)
        );
        $this->urls = array_map(
            function ($v) {
                return Router::url('/'.trim($v, "/"));
            },
            Configure::read(
                'Filters.urls',
                [
                    'delete' => 'filters/ajax-delete-save',
                    'overwrite' => 'filters/ajax-overwrite',
                    'new' => 'filters/ajax-new-save',
                    'load' => 'filters/ajax-get-filters',
                ]
            )
        );
        $this->uid = uniqid();
        $this->request = $this->Helper->getView()->getRequest();
    }

    /**
     * Returns a formatted block tag, i.e DIV, SPAN, P.
     *
     * ### Options
     *
     * - `escape` Whether or not the contents should be html_entity escaped.
     *
     * @param string      $name    Tag name.
     * @param string|null $text    String content that will appear inside the div element.
     *                             If null, only a start tag will be printed
     * @param array       $options Additional HTML attributes of the DIV tag, see above.
     * @return string The formatted tag element
     */
    private function tag($name, $text = null, array $options = []): string
    {
        return $this->Helper->Html->tag($name, $text, $options);
    }

    /**
     * Filtres sauvegardés
     * @param array $saves
     * @return self
     */
    public function saves(array $saves): self
    {
        $this->saves = $saves;
        $this->saveList = [];
        foreach ($saves as $savedFilter) {
            $this->saveList[$savedFilter->get('id')] = $savedFilter->get('name');
        }
        return $this;
    }

    /**
     * Ajout d'un filtre de recherche
     * @param string $name
     * @param array  $params
     * @return self
     */
    public function filter(string $name, array $params = []): self
    {
        if (empty($params['label'])) {
            $params['label'] = $name;
        }
        if (!empty($params['type']) && $params['type'] === 'date') {
            $this->appendDatepicker($name, $params);
        }
        if ((array_key_exists('wildcard', $params)
            || in_array('wildcard', $params, true) !== false)
            && empty($params['help'])
        ) {
            $params['help'] = $this->wildcard();
            unset($params['wildcard']);
        }
        $this->filters[$name] = $params;
        return $this;
    }

    /**
     * Ajoute à un champ de type date, un datepicker
     * et une selection d'operateur
     * @param string $name
     * @param array  $params
     */
    private function appendDatepicker(string $name, array &$params)
    {
        $options = empty($params['options'])
            ? [
                '=' => __x('date', '='),
                '<=' => __x('date', '<='),
                '>=' => __x('date', '>='),
                '<' => __x('date', '<'),
                '>' => __x('date', '>'),
                ['value' => 'null', 'text' => '────────────────────', 'disabled' => true],
                [
                    'value' => 'today',
                    'text'  => __x('date', 'today'),
                    'class' => 'disable-linked-date',
                ],
                [
                    'value' => 'week',
                    'text'  => __x('date', 'week'),
                    'class' => 'disable-linked-date',
                ],
                [
                    'value' => 'month',
                    'text'  => __x('date', 'month'),
                    'class' => 'disable-linked-date',
                ],
                [
                    'value' => 'year',
                    'text'  => __x('date', 'year'),
                    'class' => 'disable-linked-date',
                ],
                ['value' => 'null', 'text' => '────────────────────', 'disabled' => true],
                [
                    'value' => 'P7D',
                    'text'  => __x('date', 'P7D'),
                    'class' => 'disable-linked-date',
                ],
                [
                    'value' => 'P1M',
                    'text'  => __x('date', 'P1M'),
                    'class' => 'disable-linked-date',
                ],
                [
                    'value' => 'P1Y',
                    'text'  => __x('date', 'P1Y'),
                    'class' => 'disable-linked-date',
                ],
            ]
            : $params['options'];

        $explodedName = explode('.', $name);
        $explodedName[] = 'dateoperator_'.array_pop($explodedName);
        $operatorName = implode('.', $explodedName);
        $operatorId = mb_strtolower(Text::slug($operatorName, '-'));

        $endsWithBrackets = '';
        if (substr($operatorName, -2) === '[]') {
            $operatorName = substr($operatorName, 0, -2);
            $endsWithBrackets = '[]';
        }
        $parts = explode('.', $operatorName);
        $first = array_shift($parts);
        $operatorName = $first . (!empty($parts) ? '[' . implode('][', $parts) . ']' : '') . $endsWithBrackets;

        $select = $this->tag(
            'label',
            __("Opérateur de comparaison pour la date"),
            ['class' => 'sr-only', 'for' => $operatorId, 'escape' => false]
        );
        $select .= $this->tag(
            'input',
            '',
            [
                'type' => 'hidden',
                'value' => 'NOW',
                'name' => $name,
            ]
        );
        $select .= $this->tag(
            'select',
            null,
            [
                'name' => $operatorName,
                'id' => $operatorId,
                'onchange' => 'AsalaeFilter.dateOperatorChanged(this)'
            ]
        );
        foreach ($options as $k => $v) {
            if (is_array($v)) {
                $label = $v['text'];
                unset($v['text']);
                $select .= $this->tag('option', $label, $v);
            } else {
                $select .= $this->tag('option', $v, ['value' => $k]);
            }
        }
        $select .= $this->tag('/select');

        $params['class'] = empty($params['class'])
            ? 'datepicker with-select'
            : $params['class'] . ' datepicker with-select';

        $params += [
            'prepend' => $select,
            'data-datepicker' => $uuid = Text::uuid(),
            'append' => $this->Helper->Date->picker("[data-datepicker=$uuid]")
        ];
        unset($params['type'], $params['options']);
    }

    /**
     * Génère le code html et javascript pour les filtres de recherches
     * @return string
     */
    public function generateFilters(): string
    {
        $meta = $this->generateMetaInputs();
        $saveSection = $this->generateSaveSection();
        $select = $this->generateSelectFilterContainer();
        $container = $this->generateFilterContainer();

        // Boutons du modal-footer
        $this->generateLoadedFilterContent(); // génère $this->loadedContent
        $deleteBtn = !empty($this->loadedContent)
            ? $this->Helper->Form->button(
                '<i class="fa fa-undo" aria-hidden="true"></i> '.__('Réinitialiser les filtres actifs'),
                [
                    'type' => 'button',
                    'onclick' => 'AsalaeGlobal.confirmRemoveFilters()',
                    'class' => 'btn btn-default'
                ]
            )
            : '';
        $newSaveButton = $this->tag(
            'button',
            '<i class="fa fa-plus-circle" aria-hidden="true"></i> '
            .__("Nouvelle sauvegarde"),
            [
                'type' => 'button',
                'onclick' => 'AsalaeGlobal.saveFilters.call(this)',
                'class' => 'btn btn-success save'
            ]
        );
        $submit = !empty($this->loadedContent)
            ? $this->tag(
                'div',
                $deleteBtn
                . $newSaveButton
                . $this->Helper->Form->button(
                    '<i class="fa fa-filter" aria-hidden="true"></i> ' . __('Filtrer'),
                    ['class' => 'btn btn-primary']
                ),
                ['class' => 'submit-btn-filter-container modal-footer']
            )
            : '';

        $script = $this->generateScript();

        return $meta . $saveSection . $select . $container . $submit . $script;
    }

    /**
     * Génère le code html et javascript pour les filtres de recherches
     * @param array $params
     * @param mixed ...$args
     * @return string
     */
    public function generate(array $params = [], ...$args): string
    {
        if (isset($params['id'])) {
            $id = $params['id'];
            unset($params['id']);
        } else {
            $id = 'modal-filters';
        }
        $form = $this->tag(
            'form',
            $this->generateFilters(),
            $params + [
                'novalidate',
                'class' => 'ModalFilters minifiable-target no-padding',
                'method' => 'get',
                'id' => $this->id.'-form',
                'action' => $this->Helper->Url->build(
                    [
                        'controller' => $this->request->getParam('controller'),
                        'action' => $this->request->getParam('action'),
                    ] + $this->request->getParam('pass')
                )
            ]
        );

        return $this->getModals($id, $form);
    }

    /**
     * Génère les modales
     * @param string $id
     * @param string $form
     * @return string
     */
    private function getModals(string $id, string $form)
    {
        $main = $this->Helper->Modal->create(['id' => $id, 'data-uid' => $this->uid])
            . $this->Helper->Modal->header(__('Filtres de recherche'))
            . $this->Helper->Modal->body($form)
            . $this->Helper->Modal->end();
        $save = $this->Helper->Modal->create(
            ['id' => $this->id."NewSave", 'class' => 'filter-save-modal', 'data-uid' => $this->uid]
        )
            . $this->Helper->Modal->header(__('Nouvelle sauvegarde de recherche'))
            . $this->Helper->Modal->body($this->newSaveModalContent())
            . $this->Helper->Modal->end();
        return $main . $save;
    }

    /**
     * Formulaire de sauvegarde des filtres
     * @return string
     */
    private function newSaveModalContent()
    {
        $saveName = $this->tag(
            'div',
            $this->Helper->Form->control(
                'FilterName',
                [
                    'label' => __('Nom de la nouvelle sauvegarde'),
                    'class' => 'filterNewSave',
                    'id' => $this->id.'-new-save',
                ]
            ),
            ['class' => 'select-filter-save-container']
        ).PHP_EOL;
        $cancelButton = $this->tag(
            'button',
            '<i class="fa fa-times-circle" aria-hidden="true"></i> '
            .__("Annuler"),
            [
                'type' => 'button',
                'class' => 'btn btn-default cancel',
                'data-toggle' => 'modal',
                'data-target' => '#'.$this->id.'NewSave',
            ]
        );
        $newSaveButton = $this->tag(
            'button',
            '<i class="fa fa-floppy-o" aria-hidden="true"></i> '
            .__("Enregistrer"),
            [
                'type' => 'button',
                'data-toggle' => 'modal',
                'data-target' => '#'.$this->id.'Save',
                'class' => 'btn btn-primary save'
            ]
        );
        $script = $this->tag(
            'script',
            "AsalaeFilter.modalAddSaveButtons('".$this->urls['new']."', '"
                    .$this->request->getParam('controller')."', '"
                    .$this->request->getParam('action')
                ."');"
        );
        $saveDiv = $this->tag(
            'div',
            $cancelButton . $newSaveButton . $script,
            ['class' => 'submit-btn-filter-container modal-footer modal-save-buttons']
        );
        return $saveName . $saveDiv;
    }

    /**
     * Genère un filtre à partir des valeurs dans le request->params['?']
     * @param string $name
     * @param int    $index
     * @param array  $params
     * @return string
     */
    private function addInput(string $name, int $index, array $params): string
    {
        if ($s = strpos($name, '[value]')) {
            if (!isset($params['value']['value'])) {
                return '';
            }
            $name = substr($name, 0, $s). ".$index.value";
            $params['value'] = $params['value']['value'];
        } elseif ($s = strpos($name, '[')) {
            $prefix = substr($name, 0, $s);
            $suffix = substr($name, $s);
            $name = implode('.', array_filter([$prefix, $index, $suffix]));
        } else {
            $name = $name . '.' . $index;
        }
        $close = $this->tag(
            'button',
            $this->tag('span', '×'),
            [
                'class' => 'close',
                'type' => 'button',
                'aria-label' => __('Retirer le filtre de recherche'),
                'onclick' => 'var p = $(this).parent(), f = p.closest("form");'
                    .' p.slideUp(400, function() { p.remove(); f.change(); })'
            ]
        );

        foreach (['prepend', 'append'] as $key) {
            if (!empty($params[$key])
                && preg_match_all('/name=[\'"]([^\'"]+)[\'"]/', $params[$key], $match)
            ) {
                foreach ($match[1] as $k => $subname) {
                    // ['foo', '[bar]'] capturés dans "foo[bar]"
                    if (preg_match('/([^\[]+)(\[.*])/', $subname, $m)) {
                        $subname = $m[1].'['.$index.']'.$m[2]; // "foo[bar]" devient "foo[0][bar]"
                        $path = str_replace(['[', ']'], ['.', ''], $subname);
                    } else {
                        $path = $subname.'.'.$index;
                        $subname = $subname . '[' . $index . ']';
                    }
                    $subid = Text::slug($path);
                    $insert = 'name="' . $subname . '"';
                    $value = $this->loadedFilter[$subname][$index]
                        ?? $this->request->getQuery($path);
                    // Insert la valeur
                    $evalue = addcslashes((string)$value, "'");
                    $postScript = $value
                        ? "<script>
                            setTimeout(function() {
                                $('[name=\"$subname\"]').last().val('" . $evalue . "').change()
                            }, 0);
                            </script>"
                        : '';

                    // Remplace l'id
                    $params[$key] = preg_replace(
                        '/(for|id)="([^"]+)"/',
                        '$1="' . $subid . '"',
                        $params[$key]
                    );

                    $params[$key] = str_replace($match[0][$k], $insert, $params[$key]) . $postScript;
                }
            }
        }

        return $this->tag(
            'div',
            $close . $this->Helper->Form->control($name, $params)
        );
    }

    /**
     * Permet de charger les filtres sauvegardés en base
     * @param SavedFilter $save
     * @return self
     */
    public function loadFilters(SavedFilter $save): self
    {
        $this->selectedSave = $save->get('id');
        /** @var EntityInterface $filter */
        foreach ($save->get('filters') as $filter) {
            $key = $filter->get('key');
            /** @var \AsalaeCore\Model\Entity\Filter $filter */
            if (preg_match('/\[(\d+)](?:\[(\d+)])*$/', $key, $match)) {
                $index = $match[1];
                $multiVal = $match[2] ?? null;
                $key = substr($key, 0, strrpos($key, $match[0]));
                $baseName = str_replace('[', '.', str_replace(']', '', $key));
                if ($multiVal !== null) {
                    $this->loadedFilter[$baseName][$index][$multiVal] = $filter->get('value');
                } else {
                    $this->loadedFilter[$baseName][$index] = $filter->get('value');
                }
            }
        }
        return $this;
    }

    /**
     * Appel self::generate() sans spécifier de paramètres
     * @return string
     */
    public function __toString(): string
    {
        return $this->generate();
    }

    /**
     * Tri les filtres de recherches par label
     */
    private function sortFilters()
    {
        uasort(
            $this->filters,
            function ($a, $b) {
                $a = Hash::get($a, 'label');
                $b = Hash::get($b, 'label');
                return strcasecmp(
                    is_array($a) ? $a['text'] : $a,
                    is_array($b) ? $b['text'] : $b
                );
            }
        );
    }

    /**
     * Donne un input caché qui permet de reconstruire le helper
     * @return string
     */
    private function getHiddenConfig(): string
    {
        return $this->Helper->Form->control(
            'ConfigFilter',
            [
                'class' => 'configuration-filter',
                'type' => 'hidden',
                'id' => $this->id.'-config',
                'value' => base64_encode(
                    json_encode(
                        [
                            'id' => $this->id,
                            'filters' => $this->filters,
                            'permanent_filters' => array_keys($this->permanentFilters),
                            'saves' => $this->saveList,
                        ]
                    )
                )
            ]
        );
    }

    /**
     * Génère les inputs caché
     * - les filtres sauvegardés en json base64
     * - la sauvegarde sélectionnée
     * @return string
     */
    public function generateMetaInputs(): string
    {
        $this->sortFilters();
        $serializedHelper = $this->getHiddenConfig().PHP_EOL;
        $save = $this->Helper->Form->control(
            'SaveFilterSelect',
            [
                'value' => $this->selectedSave ?: $this->request->getParam('?.SaveFilterSelect'),
                'type' => 'hidden',
                'id' => $this->id.'-savefilterselect',
            ]
        );
        return $save . $serializedHelper;
    }

    /**
     * Bloc "Sélectionner la sauvegarde" avec ses boutons et javascript
     * @param string $callbackJs
     * @return string
     */
    public function generateSaveSection(string $callbackJs = 'AsalaeFilter.modalListButtons'): string
    {
        $selectSave = $this->tag(
            'div',
            $this->Helper->Form->control(
                null,
                [
                    'label' => __('Sélectionner la sauvegarde'),
                    'val' => $this->selectedSave ?: $this->request->getParam('?.SaveFilterSelect'),
                    'type' => 'select',
                    'class' => 'filterSelect',
                    'id' => $this->id.'-select-save',
                    'options' => ['-1' => __('-- Sélectionner une sauvegarde --')] + $this->saveList
                ]
            ),
            ['class' => 'select-filter-container']
        ).PHP_EOL;
        $deleteButton = $this->tag(
            'button',
            '<i class="fa fa-trash" aria-hidden="true"></i> '
            .__("Supprimer la sauvegarde"),
            [
                'type' => 'button',
                'class' => 'btn btn-danger delete',
                'disabled' => 'disabled',
            ]
        );
        $overwriteButton = $this->tag(
            'button',
            '<i class="fa fa-floppy-o" aria-hidden="true"></i> '
            .__("Enregistrer les modifications"),
            [
                'type' => 'button',
                'class' => 'btn btn-warning overwrite',
                'disabled' => 'disabled',
            ]
        );
        $loadButton = $this->tag(
            'button',
            '<i class="fa fa-caret-square-o-down" aria-hidden="true"></i> '
            .__("Charger"),
            [
                'type' => 'button',
                'class' => 'btn btn-primary load',
                'disabled' => 'disabled',
            ]
        );
        $script = $this->tag(
            'script',
            sprintf(
                "%s(%s, '%s', '%s');",
                $callbackJs,
                json_encode($this->urls),
                $this->request->getParam('controller'),
                $this->request->getParam('action')
            )
        );
        $buttons = $this->tag(
            'div',
            $deleteButton . $overwriteButton . $loadButton . $script,
            ['class' => 'submit-btn-filter-container modal-footer modal-list-buttons']
        );
        return $this->tag(
            'div',
            $selectSave . $buttons . '<hr>',
            ['class' => 'save-section']
        );
    }

    /**
     * Bloc de sélection pour ajout de filtre de recherche
     * @return string
     */
    public function generateSelectFilterContainer(): string
    {
        $this->sortFilters();
        $options = [];
        foreach ($this->filters as $name => $params) {
            if (isset($this->permanentFilters[$name]) || !empty($params['hidden'])) {
                continue;
            }
            if (is_array($params['label'])) {
                $options[] = $params['label'] + ['value' => $name];
            } else {
                if (isset($params['data-group'])) {
                    $options[$params['data-group']][$name] = $params['label'];
                } else {
                    $options[$name] = $params['label'];
                }
            }
        }
        return $options
            ? $this->tag(
                'div',
                $this->Helper->Form->control(
                    '',
                    [
                        'label' => __('Ajouter un filtre'),
                        'value' => 'null',
                        'empty' => __('--- Sélectionner un filtre ---'),
                        'type' => 'select',
                        'class' => 'filter',
                        'escape' => false,
                        'id' => $this->id.'-select',
                        'options' => $options
                    ]
                ),
                ['class' => 'select-filter-container']
            ).PHP_EOL
            : '';
    }

    /**
     * @var string Garde en mémoire les filtres de recherche (formulaire)
     */
    private $loadedContent = '';

    /**
     * Construit le formulaire à partir des filtres sauvegardés
     * Conserve le rendu dans $this->loadedContent
     * @return string
     */
    public function generateLoadedFilterContent(): string
    {
        if ($this->loadedContent) {
            return $this->loadedContent;
        }
        $savedFilters = array_merge(
            $this->request->getQueryParams(),
            $this->loadedFilter
        );
        if (!empty($savedFilters)) {
            $this->sortFilters();
            foreach ($savedFilters as $key => $values) {
                if (isset($this->filters[$key.'[value]'])) {
                    $key .= '[value]';
                }
                if (isset($this->permanentFilters[$key])) {
                    $this->permanentFilters[$key]['val'] = $values;
                } elseif (isset($this->filters[$key])) {
                    $params = $this->filters[$key];
                    foreach ((array)$values as $index => $value) {
                        $params['value'] = $value;
                        $this->loadedContent .= $this->addInput($key, $index, $params).PHP_EOL;
                    }
                }
            }
        }
        return $this->loadedContent;
    }

    /**
     * Génère le bloc des filtres de recherches chargés par la sauvegarde
     * @return string
     */
    public function generateFilterContainer(): string
    {
        $this->Helper->Form->setTemplates(
            ['dateWidget' => '{{day}}{{month}}{{year}}']
        );
        return $this->tag(
            'div',
            $this->generateLoadedFilterContent(),
            [
                'id' => $this->id.'-container',
                'class' => 'filter-container'
            ]
        ).PHP_EOL;
    }

    /**
     * Bloc javascript lié à un selectFiltre
     * @return string
     */
    public function generateScript(): string
    {
        $json = $this->getFiltersTemplates();
        $id = $this->id;
        return $this->tag(
            'script',
            "AsalaeFilter.selectHandler('#$id-select', '#$id-container', $json);
            $('#$id-form').on('change.filter', AsalaeFilter.formChangedHandler);
            "
        );
    }

    /**
     * Bloc javascript de section
     * @return string
     */
    public function generateScriptSection(): string
    {
        $idForm = $this->id.'-form';
        $json = $this->getFiltersTemplates();
        $script = [
            "var form = $('#$idForm');",
            "var searchForm = new AsalaeFilter(form, $json);",
            "AsalaeGlobal.select2(form.find('.select-filter-container select.filter'));",
        ];
        return $this->tag('script', implode(PHP_EOL, $script));
    }

    /**
     * Donne les filtres ajoutés par add() sous forme de json
     * @return string
     */
    public function getFiltersTemplates(): string
    {
        $this->sortFilters();
        $formElements = [];
        foreach ($this->filters as $name => $params) {
            $formElements[$name] = $this->Helper->Form->control($name, $params);
        }
        return json_encode($formElements);
    }

    /**
     * Ajout d'un filtre de recherche
     * @param string $name
     * @param array  $params
     * @return self
     */
    public function permanentFilter(string $name, array $params = []): self
    {
        $this->filter($name, $params);
        $this->permanentFilters[$name] = $this->filters[$name];
        return $this;
    }

    /**
     * Bloc section Filtres
     * @param array $params
     * @return string
     * @throws Exception
     */
    public function generateSection(array $params = []): string
    {
        $title = $this->tag('h2', __("Filtres"), ['class' => 'filter-title']);
        $toggleBar = $this->tag(
            'div',
            $this->Helper->Fa->button(
                'fa-angle-double-down text-muted fa-2x',
                __("Déplier les filtres"),
                [
                    'onclick' => 'AsalaeFilter.toggleFilters(this)',
                    'tabindex' => null, // evite un tabindex -1
                ]
            ),
            ['class' => 'text-center toggle-button']
        );
        $script = $this->generateScriptSection();

        $buttons = $this->tag(
            'div',
            $this->tag(
                'button',
                $this->Helper->Fa->charte('Filtrer', __("Filtrer")),
                ['class' => 'btn btn-primary']
            )
            .$this->tag(
                'button',
                $this->Helper->Fa->i('fa-list', __("Sauvegardes")),
                [
                    'class' => 'btn btn-default',
                    'type' => 'button',
                    'data-toggle' => 'modal',
                    'data-target' => '#'.$this->id.'-modal',
                ]
            ),
            ['class' => 'btn-group']
        );

        $modalSaves = $this->modalSaves();

        // doit être généré avant generatePermanentFilterContainer()
        $filterContainer = $this->generateFilterContainer();

        $permanentFilters = $this->generatePermanentFilterContainer();
        $addableFilters = $this->generateSelectFilterContainer();
        if ($permanentFilters && $addableFilters) {
            $permanentFilters .= '<hr>';
        }
        $showFilters = $this->Helper->getView()->getRequest()->getQuery('show-filters');
        $filters = $this->tag(
            'div',
            $this->getHiddenConfig()
            .$permanentFilters
            .$addableFilters
            .$filterContainer
            .$buttons
            .$script,
            ['class' => 'filters hidden-section', 'style' => $showFilters ? 'display: block' : 'display: none']
        );
        $section = $this->tag(
            'section',
            $title
            .$toggleBar
            .$filters,
            ['class' => 'bg-white container filters']
        );
        return $modalSaves . $this->tag(
            'form',
            $section,
            $params + [
                'novalidate',
                'method' => 'get',
                'id' => $this->id.'-form',
                'class' => 'filters-section-form',
                'action' => $this->Helper->Url->build(
                    [
                        'controller' => $this->request->getParam('controller'),
                        'action' => $this->request->getParam('action'),
                    ] + $this->request->getParam('pass')
                )
            ]
        );
    }

    /**
     * Message à insérer pour indiquer l'utilisation possible d'un wildcard
     * @return string
     */
    public function wildcard(): string
    {
        return __("? = n'importe quel caractère ; * = n'importe quelle chaîne de caractères");
    }

    /**
     * Filtre toujours affichés (valeur unique)
     * @return string
     */
    private function generatePermanentFilterContainer(): string
    {
        $filters = [];
        foreach ($this->permanentFilters as $name => $params) {
            $filters[] = $this->Helper->Form->control($name.'[0]', $params);
        }
        return $filters
            ? $this->tag('div', implode('', $filters), ['class' => 'permanent-filters'])
            : '';
    }

    /**
     * Donne la modal de gestion des sauvegardes
     * @return string
     * @throws Exception
     */
    private function modalSaves(): string
    {
        $Modal = $this->Helper->Modal;
        $modalContent = $this->tag('section', null, ['class' => 'bg-white']);

        // sous modale de sauvegarde (click sur bouton "Créer une nouvelle sauvegarde")
        $subformId = 'save-'.$this->id.'-form';
        $form = $this->Helper->Form->create(
            null,
            [
                'id' => $subformId,
                'url' => $this->urls['new'],
                'data-link' => '#'.$this->id.'-form',
                'data-table' => '#'.$this->id.'-table',
                'idPrefix' => 'save-'.$this->id,
            ]
        ).PHP_EOL;
        $form .= $this->Helper->Form->control(
            'savename',
            ['label' => __("Nom de la sauvegarde"), 'required']
        ).PHP_EOL;
        $form .= $this->Helper->Form->control(
            'controller',
            ['type' => 'hidden', 'value' => $this->request->getParam('controller')]
        ).PHP_EOL;
        $form .= $this->Helper->Form->control(
            'action',
            ['type' => 'hidden', 'value' => $this->request->getParam('action')]
        ).PHP_EOL;
        $form .= $this->Helper->Form->button(
            'Hidden submit',
            ['class' => 'hide']
        ).PHP_EOL;
        $form .= $this->tag('script', "AsalaeFilter.saveFormHandler($('#$subformId'));");
        $form .= $this->Helper->Form->end().PHP_EOL;
        $modalId = 'save-'.$this->id.'-modal';
        $cancelButton = $this->Helper->Form->button(
            $this->Helper->Fa->i('fa-times-circle-o', __('Annuler')),
            ['data-dismiss' => 'modal', 'class' => 'cancel', 'type' => 'button']
        );
        $submitButton = $this->Helper->Form->button(
            $this->Helper->Fa->charte('Enregistrer', __('Enregistrer')),
            ['bootstrap-type' => 'primary', 'class' => 'accept']
        );
        $modalContent .= $Modal->create(
            [
                'id' => $modalId,
                'class' => 'filter-save-modal small-modal',
            ]
        )
            . $Modal->header(__("Nouvelle sauvegarde de recherche"))
            . $Modal->body($form)
            . $Modal->end([$cancelButton, $submitButton])
            . $this->Helper->ModalForm->modalScript('save-'.$this->id.'-modal')
            . PHP_EOL;

        // bouton "Créer une nouvelle sauvegarde"
        $modalContent .= $this->tag(
            'div',
            $this->tag(
                'button',
                $this->Helper->Fa->charte('Ajouter', __("Créer une nouvelle sauvegarde")),
                [
                    'type' => 'button',
                    'class' => 'btn btn-success save',
                    'onclick' => "AsalaeModal.append($('#$modalId').modal('show'))"
                ]
            ),
            ['class' => 'separator']
        ).PHP_EOL;

        // Titre de modale
        $modalContent .= $this->tag(
            'header',
            $this->tag('h2', __("Liste des sauvegardes"), ['class' => 'h4'])
        ).PHP_EOL;

        // Liste des sauvegardes (table)
        $formId = $this->id.'-form';
        $tableId = $this->id.'-table';
        $urlDelete = $this->urls['delete'];
        $modalContent .= $this->Helper->Table->create($tableId)
            ->fields(
                [
                    'name' => ['label' => __("Nom de la sauvegarde")],
                    'filters' => ['display' => false, 'escape' => false],
                ]
            )
            ->data($this->saves)
            ->params(
                [
                    'identifier' => 'id',
                ]
            )
            ->actions(
                [
                    [
                        'onclick' => "AsalaeFilter.actionFilter({0}, '$formId')",
                        'type' => "button",
                        'class' => 'btn-link',
                        'label' => $this->Helper->Fa->charte('Filtrer'),
                        'title' => $title = __("Utiliser la sauvegarde {0}", '{1}'),
                        'aria-label' => $title,
                        'params' => ['filters', 'name']
                    ],
                    [
                        'data-callback' => "AsalaeFilter.actionDelete({0}, '$urlDelete', '$tableId')",
                        'type' => 'button',
                        'class' => 'btn-link',
                        'label' => $this->Helper->Fa->charte('Supprimer', '', 'text-danger'),
                        'title' => $title = __("Supprimer la sauvegarde {0}", '{1}'),
                        'aria-label' => $title,
                        'confirm' => __("Supprimer la sauvegarde ?"),
                        'params' => ['id', 'name']
                    ],
                ]
            )
            ->generate().PHP_EOL;
        $modalContent .= $this->tag('/section').PHP_EOL;
        return $Modal->create(['id' => $this->id.'-modal', 'class' => 'small-modal'])
            . $Modal->header(__("Sauvegardes de filtres de recherche"))
            . $Modal->body($modalContent, ['class' => 'no-padding'])
            . $Modal->end()
            . $this->Helper->ModalForm->modalScript($this->id.'-modal');
    }
}
