<?php
/**
 * AsalaeCore\View\Helper\ModalViewHelper
 */

namespace AsalaeCore\View\Helper;

use AsalaeCore\View\Helper\Object\ModalView;
use AsalaeCore\View\Helper\Object\ObjectInterface;
use Bootstrap\View\Helper\ModalHelper;
use Cake\View\Helper;
use Cake\View\Helper\UrlHelper;

/**
 * Initialise une modal et crée le bouton/lien pour l'afficher
 *
 * @category Helper
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017-2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property \AsalaeCore\View\Helper\FaHelper Fa
 * @property \AsalaeCore\View\Helper\FormHelper Form
 * @property ModalHelper Modal
 * @property \Cake\View\Helper\HtmlHelper Html
 * @property UrlHelper Url
 */
class ModalViewHelper extends Helper implements ObjectHelperInterface
{
    /**
     * Helpers utilisés
     *
     * @var array
     */
    public $helpers = [
        'Fa',
        'Form',
        'Html',
        'Modal' => ['className' => 'Bootstrap.Modal'],
        'Url'
    ];

    /**
     * Initialise une nouvelle modale
     * @param string $id
     * @param array  $params
     * @return ObjectInterface|ModalView
     */
    public function create(string $id, array $params = []): ModalView
    {
        return new ModalView($this, $id, $params);
    }

    /**
     * Permet d'obtenir le javascript additionnel qui va avec la modale
     * @param string $id
     * @return string
     */
    public function modalScript(string $id): string
    {
        return "<script> AsalaeModal.append($('#$id')); </script>";
    }
}
