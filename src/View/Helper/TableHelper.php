<?php
/**
 * AsalaeCore\View\Helper\TableHelper
 */

namespace AsalaeCore\View\Helper;

use AsalaeCore\View\Helper\Object\ObjectInterface;
use AsalaeCore\View\Helper\Object\Table;
use Cake\Core\Configure;
use Cake\Utility\Inflector;
use Cake\View\Helper;
use Cake\View\Helper\UrlHelper;
use Exception;

/**
 * Permet d'utiliser le générateur de tableau javascript
 *
 * @category Helper
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property \Cake\View\Helper\HtmlHelper Html
 * @property UrlHelper Url
 * @property FaHelper Fa
 * @property PaginatorHelper Paginator
 * @property FormHelper Form
 */
class TableHelper extends Helper implements ObjectHelperInterface
{
    /**
     * Classes css par défaut
     *
     * @var array
     */
    public $defaultCssClass = ['table', 'table-striped', 'table-hover'];

    /**
     * Helpers utilisés
     *
     * @var array
     */
    public $helpers = ['Html', 'Paginator', 'Fa', 'Form', 'Url'];

    /**
     * Aparance du json
     *
     * @var integer
     */
    public $jsonStyle;

    /**
     * Initialisation du helper
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->jsonStyle = Configure::read('debug')
            ? JSON_PRETTY_PRINT
            : 0;
    }

    /**
     * Permet de créer et de récupérer l'objet
     *
     * @param string $id
     * @param array  $params
     * @return ObjectInterface|Table
     * @throws Exception
     */
    public function create(string $id, array $params = []): Table
    {
        return new Table($this, $id, $params);
    }

    /**
     * permet d'obtenir le nom de l'objet javascript selon l'id de la table
     * @param string $id
     * @return string
     */
    public function getJsTableObject(string $id): string
    {
        return Inflector::camelize(
            'generated_'.preg_replace('/\W/', '_', $id)
        );
    }
}
