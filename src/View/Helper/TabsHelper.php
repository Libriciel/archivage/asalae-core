<?php
/**
 * AsalaeCore\View\Helper\TabsHelper
 */

namespace AsalaeCore\View\Helper;

use AsalaeCore\View\Helper\Object\ObjectInterface;
use AsalaeCore\View\Helper\Object\Tabs;
use Cake\View\Helper;
use Exception;

/**
 * Permet de générer une vue avec tabulation
 *
 * @category Helper
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property \Cake\View\Helper\HtmlHelper $Html
 * @property PaginatorHelper $Paginator
 * @property FaHelper $Fa
 */
class TabsHelper extends Helper implements ObjectHelperInterface
{

    /**
     * @var array Helpers utilisés
     */
    public $helpers = ['Html'];

    /**
     * Permet de créer et de récupérer l'objet
     * @param string $id
     * @param array  $params
     * @return ObjectInterface|Tabs
     * @throws Exception
     */
    public function create(string $id, array $params = []): Tabs
    {
        return new Tabs($this, $id, $params);
    }
}
