<?php
/**
 * AsalaeCore\View\Helper\FaHelper
 */

namespace AsalaeCore\View\Helper;

use Cake\View\Helper;

/**
 * Permet de générer des icônes, intègre la charte graphique Libriciel
 *
 * @category Helper
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property \Cake\View\Helper\HtmlHelper $Html
 */
class FaHelper extends Helper
{
    /**
     * @var array Helpers utilisés
     */
    public $helpers = [
        'Html',
    ];

    /**
     * Liste les règles de la charte
     * @link https://gitlab.libriciel.fr/outils/charte-graphique
     */
    const CHARTE = [
        // Actions
        'Activer' => ['fa-toggle-off'],
        'Actualiser' => ['fa-refresh'],
        'Ajouter' => ['fa-plus-circle'],
        'Annuler' => ['fa-times-circle', 'fa-times-circle-o'],
        'Archiver' => ['fa-archive'],
        'Clore' => ['fa-lock'],
        'Créer' => ['fa-plus'],
        'Commenter' => ['fa-commenting', 'fa-commenting-o'],
        'Annoter' => ['fa-commenting', 'fa-commenting-o'],
        'Désactiver' => ['fa-toggle-on'],
        'Déverrouiller' => ['fa-unlock-alt'],
        'Dupliquer' => ['fa-files-o'],
        'Enregistrer' => ['fa-floppy-o'],
        'Envoyer' => ['fa-paper-plane', 'fa-paper-plane-o'],
        'Exécuter' => ['fa-cog', 'fa-cogs'],
        'Filtrer' => ['fa-filter'],
        'Générer' => ['fa-cog', 'fa-cogs'],
        'Importer' => ['fa-upload'],
        'Parcourir' => ['fa-folder-open', 'fa-folder-open-o'],
        'Imprimer' => ['fa-print'],
        'Modifier' => ['fa-pencil'],
        'Rechercher' => ['fa-search'],
        'Refuser' => ['fa-times'],
        'Réinitialiser' => ['fa-undo'],
        'Supprimer' => ['fa-trash', 'fa-trash-o'],
        'Se connecter' => ['fa-sign-in'],
        'Se déconnecter' => ['fa-sign-out'],
        'Télécharger' => ['fa-download'],
        'Valider' => ['fa-check'],
        'Verrouiller' => ['fa-lock'],
        'Visualiser' => ['fa-eye'],
        'Voter' => ['fa-gavel'],

        // Navigation
        'Accueil' => ['fa-home'],
        'Aide' => ['fa-question'],
        'Certificat' => ['fa-certificate'],
        'Circuit' => ['fa-road'],
        'Collectivité' => ['fa-building', 'fa-building-o'],
        'Connecteurs' => ['fa-plug'],
        'Dossier' => ['fa-folder', 'fa-folder-o'],
        'Fermer' => ['fa-times'],
        'Fichier' => ['fa-file-text', 'fa-file-text-o'],
        'Fichier PDF' => ['fa-file-pdf'],
        'Historique' => ['fa-history'],
        'Informations' => ['fa-info'],
        'Journal des évènements' => ['fa-list-alt'],
        'Liste' => ['fa-list-ul'],
        'Menu' => ['fa-bars'],
        'Mot de passe' => ['fa-lock'],
        'Notification' => ['fa-bell', 'fa-bell-o'],
        'Paramètres' => ['fa-sliders'],
        'Préférences' => ['fa-cogs'],
        'Retour' => ['fa-arrow-left'],
        'Services' => ['fa-sitemap'],
        'Tableau de bord' => ['fa-tachometer'],
        'Utilisateur' => ['fa-user', 'fa-user-o'],
        'Utilisateurs' => ['fa-user', 'fa-user-o'],
    ];

    /**
     * Certaines icones ne sont pas compatible avec fontawesome 5 (fas => solid), elles sont
     * listées ici
     *
     * @var array classe blacklisté => alternative
     */
    const V5_BLACKLIST = [
        'fa-globe' => 'fa-globe-europe',
        'fa-pencil' => 'fa-pencil-alt',
        'fa-sign-in' => 'fa-sign-in-alt',
        'fa-sign-out' => 'fa-sign-out-alt',
        'fa-twitter-square' => 'fab fa-twitter-square',
        'fa-facebook-square' => 'fab fa-facebook-square',
        'fa-tachometer' => 'fa-tachometer-alt',
        'fa-dashboard' => 'fa-tachometer-alt',
        'fa-exchange' => 'fa-exchange-alt',
        'fa-arrows' => 'fa-arrows-alt',
        'fa-arrows-v' => 'fa-arrows-alt-v',
        'fa-arrows-h' => 'fa-arrows-alt-h',
        'fa-trash-o' => 'far fa-trash-alt',
        'fa-arrow-circle-o-down' => 'far fa-arrow-alt-circle-down',
        'fa-arrow-circle-o-up' => 'far fa-arrow-alt-circle-up',
        'fa-picture-o' => 'far fa-image',
        'fa-pencil-square-o' => 'far fa-edit',
        'fa-linkedin-square' => 'fab fa-linkedin-square',
        'fa-external-link' => 'fa-external-link-alt',
        'fa-github-square' => 'fab fa-github-square',
        'fa-twitter' => 'fab fa-twitter',
        'fa-github' => 'fab fa-github',
        'fa-facebook' => 'fab fa-facebook',
        'fa-facebook-f' => 'fab fa-facebook-f',
        'fa-hand-o-right' => 'far fa-hand-point-right',
        'fa-hand-o-left' => 'far fa-hand-point-left',
        'fa-hand-o-up' => 'far fa-hand-point-up',
        'fa-hand-o-down' => 'far fa-hand-point-down',
        'fa-files-o' => 'far fa-copy',
        'fa-floppy-o' => 'far fa-save',
        'fa-pinterest' => 'fab fa-pinterest',
        'fa-pinterest-square' => 'fab fa-pinterest-square',
        'fa-google-plus' => 'fab fa-google-plus',
        'fa-google-plus-square' => 'fab fa-google-plus-square',
        'fa-linkedin' => 'fab fa-linkedin',
        'fa-cloud-download' => 'fa-cloud-download-alt',
        'fa-cloud-upload' => 'fa-cloud-upload-alt',
        'fa-cutlery' => 'fa-utensils',
        'fa-github-alt' => 'fab fa-github-alt',
        'fa-mail-reply' => false, // étrangement, "fas fa-reply" ne fonctionne pas
        'fa-reply' => false, // étrangement, "fas fa-reply" ne fonctionne pas
        'fa-star-half-empty' => 'fa-star-half-alt',
        'fa-star-half-full' => 'fa-star-half-alt',
        'fa-shield' => 'fa-shield-alt',
        'fa-maxcdn' => 'fab fa-maxcdn',
        'fa-html5' => 'fab fa-html5',
        'fa-css3' => 'fab fa-css3',
        'fa-bitcoin' => 'fab fa-bitcoin',
        'fa-btc' => 'fab fa-bitcoin',
        'fa-dropbox' => 'fab fa-dropbox',
        'fa-flickr' => 'fab fa-flickr',
        'fa-adn' => 'fab fa-adn',
        'fa-bitbucket' => 'fab fa-bitbucket',
        'fa-tumblr' => 'fab fa-tumblr',
        'fa-tumblr-square' => 'fab fa-tumblr-square',
        'fa-bitbucket-square' => false,
        'fa-youtube' => 'fab fa-youtube',
        'fa-instagram' => 'fab fa-instagram',
        'fa-stack-overflow' => 'fab fa-stack-overflow',
        'fa-youtube-play' => 'fab fa-youtube',
        'fa-youtube-square' => false, // étrangement, "fab fa-youtube-square" ne fonctionne pas
        'fa-xing' => 'fab fa-xing',
        'fa-xing-square' => 'fab fa-xing-square',
        'fa-ticket' => false, // fa-ticket-alt est different
        'fa-level-up' => 'fa-level-up-alt',
        'fa-level-down' => 'fa-level-down-alt',
        'fa-external-link-square' => 'fa-external-link-square-alt',
        'fa-long-arrow-down' => 'fa-long-arrow-alt-down',
        'fa-long-arrow-up' => 'fa-long-arrow-alt-up',
        'fa-long-arrow-left' => 'fa-long-arrow-alt-left',
        'fa-long-arrow-right' => 'fa-long-arrow-alt-right',
        'fa-apple' => 'fab fa-apple',
        'fa-windows' => 'fab fa-windows',
        'fa-android' => 'fab fa-android',
        'fa-linux' => 'fab fa-linux',
        'fa-dribbble' => 'fab fa-dribbble',
        'fa-skype' => 'fab fa-skype',
        'fa-foursquare' => 'fab fa-foursquare',
        'fa-trello' => 'fab fa-trello',
        'fa-gittip' => 'fab fa-gittip',
        'fa-gratipay' => 'fab fa-gratipay',
        'fa-vk' => 'fab fa-vk',
        'fa-weibo' => 'fab fa-weibo',
        'fa-renren' => 'fab fa-renren',
        'fa-pagelines' => 'fab fa-pagelines',
        'fa-stack-exchange' => 'fab fa-stack-exchange',
        'fa-vimeo-square' => 'fab fa-vimeo-square',
        'fa-slack' => 'fab fa-slack',
        'fa-wordpress' => 'fab fa-wordpress',
        'fa-openid' => 'fab fa-openid',
        'fa-yahoo' => 'fab fa-yahoo',
        'fa-google' => 'fab fa-google',
        'fa-reddit' => 'fab fa-reddit',
        'fa-reddit-square' => 'fab fa-reddit-square',
        'fa-stumbleupon-circle' => 'fab fa-stumbleupon-circle',
        'fa-stumbleupon' => 'fab fa-stumbleupon',
        'fa-delicious' => 'fab fa-delicious',
        'fa-digg' => 'fab fa-digg',
        'fa-pied-piper-pp' => 'fab fa-pied-piper-pp',
        'fa-pied-piper-alt' => 'fab fa-pied-piper-alt',
        'fa-drupal' => 'fab fa-drupal',
        'fa-joomla' => 'fab fa-joomla',
        'fa-behance' => 'fab fa-behance',
        'fa-behance-square' => 'fab fa-behance-square',
        'fa-steam' => 'fab fa-steam',
        'fa-steam-square' => 'fab fa-steam-square',
        'fa-spotify' => 'fab fa-spotify',
        'fa-deviantart' => 'fab fa-deviantart',
        'fa-soundcloud' => 'fab fa-soundcloud',
        'fa-vine' => 'fab fa-vine',
        'fa-codepen' => 'fab fa-codepen',
        'fa-jsfiddle' => 'fab fa-jsfiddle',
        'fa-ra' => 'fab fa-ra',
        'fa-resistance' => 'fab fa-resistance',
        'fa-rebel' => 'fab fa-rebel',
        'fa-ge' => 'fab fa-ge',
        'fa-empire' => 'fab fa-empire',
        'fa-git-square' => 'fab fa-git-square',
        'fa-git' => 'fab fa-git',
        'fa-y-combinator-square' => 'fab fa-y-combinator-square',
        'fa-yc-square' => 'fab fa-yc-square',
        'fa-hacker-news' => 'fab fa-hacker-news',
        'fa-tencent-weibo' => 'fab fa-tencent-weibo',
        'fa-qq' => 'fab fa-qq',
        'fa-wechat' => 'fab fa-wechat',
        'fa-weixin' => 'fab fa-weixin',
        'fa-slideshare' => 'fab fa-slideshare',
        'fa-twitch' => 'fab fa-twitch',
        'fa-yelp' => 'fab fa-yelp',
        'fa-paypal' => 'fab fa-paypal',
        'fa-google-wallet' => 'fab fa-google-wallet',
        'fa-cc-visa' => 'fab fa-cc-visa',
        'fa-cc-mastercard' => 'fab fa-cc-mastercard',
        'fa-cc-discover' => 'fab fa-cc-discover',
        'fa-cc-amex' => 'fab fa-cc-amex',
        'fa-cc-paypal' => 'fab fa-cc-paypal',
        'fa-cc-stripe' => 'fab fa-cc-stripe',
        'fa-lastfm' => 'fab fa-lastfm',
        'fa-lastfm-square' => 'fab fa-lastfm-square',
        'fa-ioxhost' => 'fab fa-ioxhost',
        'fa-angellist' => 'fab fa-angellist',
        'fa-meanpath' => 'fab fa-meanpath',
        'fa-buysellads' => 'fab fa-buysellads',
        'fa-connectdevelop' => 'fab fa-connectdevelop',
        'fa-dashcube' => 'fab fa-dashcube',
        'fa-forumbee' => 'fab fa-forumbee',
        'fa-leanpub' => 'fab fa-leanpub',
        'fa-sellsy' => 'fab fa-sellsy',
        'fa-shirtsinbulk' => 'fab fa-shirtsinbulk',
        'fa-simplybuilt' => 'fab fa-simplybuilt',
        'fa-skyatlas' => 'fab fa-skyatlas',
        'fa-pinterest-p' => 'fab fa-pinterest-p',
        'fa-whatsapp' => 'fab fa-whatsapp',
        'fa-viacoin' => 'fab fa-viacoin',
        'fa-medium' => 'fab fa-medium',
        'fa-yc' => 'fab fa-yc',
        'fa-y-combinator' => 'fab fa-y-combinator',
        'fa-optin-monster' => 'fab fa-optin-monster',
        'fa-opencart' => 'fab fa-opencart',
        'fa-expeditedssl' => 'fab fa-expeditedssl',
        'fa-cc-jcb' => 'fab fa-cc-jcb',
        'fa-cc-diners-club' => 'fab fa-cc-diners-club',
        'fa-creative-commons' => 'fab fa-creative-commons',
        'fa-gg' => 'fab fa-gg',
        'fa-gg-circle' => 'fab fa-gg-circle',
        'fa-tripadvisor' => 'fab fa-tripadvisor',
        'fa-odnoklassniki' => 'fab fa-odnoklassniki',
        'fa-odnoklassniki-square' => 'fab fa-odnoklassniki-square',
        'fa-get-pocket' => 'fab fa-get-pocket',
        'fa-wikipedia-w' => 'fab fa-wikipedia-w',
        'fa-safari' => 'fab fa-safari',
        'fa-chrome' => 'fab fa-chrome',
        'fa-firefox' => 'fab fa-firefox',
        'fa-opera' => 'fab fa-opera',
        'fa-internet-explorer' => 'fab fa-internet-explorer',
        'fa-contao' => 'fab fa-contao',
        'fa-500px' => 'fab fa-500px',
        'fa-amazon' => 'fab fa-amazon',
        'fa-houzz' => 'fab fa-houzz',
        'fa-vimeo' => 'fab fa-vimeo',
        'fa-black-tie' => 'fab fa-black-tie',
        'fa-fonticons' => 'fab fa-fonticons',
        'fa-reddit-alien' => 'fab fa-reddit-alien',
        'fa-edge' => 'fab fa-edge',
        'fa-codiepie' => 'fab fa-codiepie',
        'fa-modx' => 'fab fa-modx',
        'fa-fort-awesome' => 'fab fa-fort-awesome',
        'fa-usb' => 'fab fa-usb',
        'fa-product-hunt' => 'fab fa-product-hunt',
        'fa-mixcloud' => 'fab fa-mixcloud',
        'fa-scribd' => 'fab fa-scribd',
        'fa-gitlab' => 'fab fa-gitlab',
        'fa-wpbeginner' => 'fab fa-wpbeginner',
        'fa-wpforms' => 'fab fa-wpforms',
        'fa-envira' => 'fab fa-envira',
        'fa-glide' => 'fab fa-glide',
        'fa-glide-g' => 'fab fa-glide-g',
        'fa-viadeo' => 'fab fa-viadeo',
        'fa-viadeo-square' => 'fab fa-viadeo-square',
        'fa-snapchat' => 'fab fa-snapchat',
        'fa-snapchat-ghost' => 'fab fa-snapchat-ghost',
        'fa-snapchat-square' => 'fab fa-snapchat-square',
        'fa-pied-piper' => 'fab fa-pied-piper',
        'fa-first-order' => 'fab fa-first-order',
        'fa-yoast' => 'fab fa-yoast',
        'fa-themeisle' => 'fab fa-themeisle',
        'fa-google-plus-circle' => 'fab fa-google-plus-circle',
        'fa-google-plus-official' => 'fab fa-google-plus-official',
        'fa-fa' => 'fab fa-fa',
        'fa-font-awesome' => 'fab fa-font-awesome',
        'fa-linode' => 'fab fa-linode',
        'fa-quora' => 'fab fa-quora',
        'fa-free-code-camp' => 'fab fa-free-code-camp',
        'fa-telegram' => 'fab fa-telegram',
        'fa-bandcamp' => 'fab fa-bandcamp',
        'fa-grav' => 'fab fa-grav',
        'fa-etsy' => 'fab fa-etsy',
        'fa-imdb' => 'fab fa-imdb',
        'fa-ravelry' => 'fab fa-ravelry',
        'fa-eercast' => 'fab fa-eercast',
        'fa-superpowers' => 'fab fa-superpowers',
        'fa-wpexplorer' => 'fab fa-wpexplorer',
        'fa-meetup' => 'fab fa-meetup',
        'fa-photo' => 'far fa-image',
        'fa-image' => 'far fa-image',
        'fa-file-photo-o' => 'far fa-file-image',
        'fa-file-picture-o' => 'far fa-file-image',
        'fa-file-zip-o' => 'far fa-file-archive',
        'fa-file-sound-o' => 'far fa-file-audio',
        'fa-file-movie-o' => 'far fa-file-video',
        'fa-circle-o-notch' => 'fa-circle-notch',
        'fa-circle-thin' => 'far fa-circle',
        'fa-diamond' => 'far fa-gem',
        'fa-facebook-official' => 'fab fa-facebook-square',
        'fa-hand-grab-o' => 'far fa-hand-rock',
        'fa-hand-stop-o' => 'far fa-hand-paper',
        'fa-credit-card-alt' => 'fa-credit-card',
        'fa-bluetooth' => 'fab fa-bluetooth',
        'fa-bluetooth-b' => 'fab fa-bluetooth-b',
        'fa-wheelchair-alt' => 'fab fa-accessible-icon',
        'fa-soccer-ball-o' => 'far fa-futbol',
        'fa-spoon' => false, // different de fa-utensil-spoon
        'fa-times-rectangle' => false, // devrai fonctionner - fa-window-close
        'fa-window-close' => false, // devrai fonctionner
        'fa-times-rectangle-o' => false, // devrai fonctionner - far fa-window-close
        'fa-window-close-o' => false, // devrai fonctionner - far fa-window-close
        'fa-arrow-circle-o-right' => 'far fa-arrow-alt-circle-right',
        'fa-arrow-circle-o-left' => 'far fa-arrow-alt-circle-left',
        'fa-toggle-right' => 'far fa-toggle-right',
        'fa-toggle-up' => 'far fa-toggle-up',
        'fa-toggle-down' => 'far fa-toggle-down',
    ];

    /**
     * Permet de générer une icône très facilement :
     *
     * exemple:
     * $this->Fa->i('fa-times') === '<i class="fa fa-times" aria-hidden="true"></i>'
     *
     * @param string $class
     * @param string $text
     * @param array  $options
     * @return string
     */
    public function i(string $class, string $text = '', array $options = []): string
    {
        if (isset($options['class'])) {
            trigger_error(__("\$options['class'] ne doit pas être défini."));
            unset($options['class']);
        }
        $classes = explode(' ', $class);
        $baseclass = 'fa';
        $useV5 = $options['v5'] ?? $this->getConfig('v5', false);

        // cas 1 : v5 hors blacklist => $baseclass = fas
        // cas 2 : v5 dans blacklist sans alterative => $baseclass = fa
        // cas 3 : v5 dans blacklist avec alternative => $baseclass = fas && $classes[0] = alternative
        // cas 4 : v5 hors blacklist mais fait parti des "-o" $baseclass = far
        // cas 5 : v5 dans blacklist - avec prefixe comme baseclass
        if ($useV5) {
            if (isset(self::V5_BLACKLIST[$classes[0]])) {
                if ($alt = self::V5_BLACKLIST[$classes[0]]) {
                    if (strpos($alt, ' ') !== false) {
                        [$baseclass, $alt] = explode(' ', $alt);
                    } else {
                        $baseclass = 'fas';
                    }
                    $classes[0] = $alt;
                }
            } elseif (preg_match('/^fa-(.*)-o($|-.*)/', $classes[0], $m)) {
                $baseclass = 'far';
                $classes[0] = 'fa-'.$m[1].$m[2];
            } else {
                $baseclass = 'fas';
            }
        }
        $options += [
            'class' => $baseclass.' '.implode(' ', $classes),
            'aria-hidden' => 'true'
        ];
        if ($text && $text[0] !== ' ') {
            $options['class'] .= ' fa-space';
        }
        return $this->Html->tag('i', '', $options).$text;
    }

    /**
     * Renvoi l'icône définie par la charte graphique Libriciel
     * @param string           $keyword
     * @param int|array|string $index   si integer, $text si string, $options si array
     * @param int|array|string $text    si string, $options si array
     * @param array|string     $options , si string, equivalent à ['class' =>
     *                                  $options]
     * @return string
     */
    public function charte(string $keyword, $index = 0, $text = '', $options = []): string
    {
        if (!isset(self::CHARTE[$keyword])) {
            trigger_error(__("{0} ne fait pas partie des valeurs autorisées", $keyword));
            debug(array_keys(self::CHARTE));
        }
        if (is_array($index)) {
            $options = $index;
            $index = 0;
            $text = '';
        } elseif (is_numeric($index)) {
            if (is_array($text)) {
                $options = $text;
                $text = '';
            }
        } elseif (is_string($index)) {
            $options = $text;
            $text = $index;
            $index = 0;
        }
        if (is_string($options)) {
            $options = ['class' => $options];
        }
        if (isset($options['icon'])) {
            $options['class'] = trim(($options['class'] ?? '').' '.$options['icon']);
            unset($options['icon']);
        }
        $class = trim(self::CHARTE[$keyword][$index].' '.($options['class'] ?? ''));
        unset($options['class']);
        return $this->i($class, $text, $options);
    }

    /**
     * Permet d'obtenir un bouton
     * @param string $class   classes css de l'icône font
     *                        awesome
     * @param string $title   texte descriptif de l'icône en title et
     *                        sr-only
     * @param array  $options à passer au bouton (ex: id, onclick
     *                        etc...)
     * @return string
     */
    public function button(string $class, string $title = '', array $options = []): string
    {
        $options += [
            'type' => 'button',
            'class' => 'btn-link',
            'escape' => false,
            'title' => $title, // Note: provoque un warning Redundant title (on ne peut pas se passer du sr-only)
        ];
        $text = '';
        $srOnly = $this->Html->tag('span', $title, ['class' => 'sr-only']);
        if (isset($options['srOnly'])) {
            $srOnly = $options['srOnly'];
            unset($options['srOnly']);
        }
        if (isset($options['text'])) {
            $text = $options['text'];
            unset($options['text']);
        }
        return $this->Html->tag('button', $this->i($class) . $srOnly . $text, $options);
    }

    /**
     * Donne un bouton avec icône de charte
     * @param string $keyword
     * @param string $title
     * @param int    $index
     * @param array  $options
     * @return string
     */
    public function charteBtn(string $keyword, string $title = '', $index = 0, $options = []): string
    {
        if (!isset(self::CHARTE[$keyword])) {
            trigger_error(__("{0} ne fait pas partie des valeurs autorisées", $keyword));
            debug(array_keys(self::CHARTE));
        }
        if (!is_numeric($index)) {
            $options = $index;
            $index = 0;
        }
        if (is_string($options)) {
            $options = ['icon' => $options];
        }
        $class = trim(self::CHARTE[$keyword][$index].' '.($options['icon'] ?? ''));
        unset($options['icon']);

        $options += [
            'type' => 'button',
            'class' => 'btn-link',
            'escape' => false,
            'title' => $title,
        ];
        $text = '';
        $srOnly = $this->Html->tag('span', $title, ['class' => 'sr-only']);
        if (isset($options['srOnly'])) {
            $srOnly = $options['srOnly'];
            unset($options['srOnly']);
        }
        if (isset($options['text'])) {
            $text = $options['text'];
            unset($options['text']);
        }
        return $this->Html->tag('button', $this->i($class) . $srOnly . $text, $options);
    }
}
