<?php
/**
 * AsalaeCore\View\Helper\AjaxPaginatorHelper
 */

namespace AsalaeCore\View\Helper;

use AsalaeCore\View\Helper\Object\AjaxPaginator;
use AsalaeCore\View\Helper\Object\ObjectInterface;
use Cake\Core\Configure;

/**
 * Pour la pagination de tableau de résultat par ajax
 *
 * @category Helper
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property \Cake\View\Helper\HtmlHelper $Html
 * @property PaginatorHelper $Paginator
 * @property FaHelper $Fa
 */
class AjaxPaginatorHelper extends PaginatorHelper implements ObjectHelperInterface
{
    /**
     * Helpers utilisés
     *
     * @var array
     */
    public $helpers = ['Url', 'Number', 'Html', 'Form', 'Fa'];

    /**
     * Default config for this class
     *
     * Options: Holds the default options for pagination links
     *
     * The values that may be specified are:
     *
     * - `url` Url of the action. See Router::url()
     * - `url['sort']` the key that the recordset is sorted.
     * - `url['direction']` Direction of the sorting (default: 'asc').
     * - `url['page']` Page number to use in links.
     * - `model` The name of the model.
     * - `escape` Defines if the title field for the link should be escaped (default: true).
     *
     * Templates: the templates used by this class
     *
     * @var array
     */
    protected $_defaultConfig = [
        'options' => [],
        'numbers' => [
            'first' => 1,
            'last' => 1,
            'modulus' => 2,
        ],
        'limit' => 100,
    ];

    /**
     * @var array url cible sous forme d'array
     */
    private $url;

    /**
     * @var string|null url plugin
     */
    private $plugin;

    /**
     * @var string url controller
     */
    private $controller;

    /**
     * @var string url action
     */
    private $action;

    /**
     * @var array partie "?" de l'url. ex: ?param=1
     */
    private $urlParams;

    /**
     * @var array les ids passés à l'url
     */
    private $passed;

    /**
     * Constructor hook method.
     *
     * Implement this method to avoid having to overwrite the constructor and call parent.
     *
     * @param array $config The configuration settings provided to this helper.
     * @return void
     */
    public function initialize(array $config): void
    {
        if (empty($config['limit'])) {
            $this->setConfig('limit', Configure::read('AjaxPaginator.limit', 100));
        }
    }

    /**
     * Permet de créer et de récupérer l'objet
     *
     * @param string $id
     * @param array  $params
     * @return ObjectInterface|AjaxPaginator
     */
    public function create(string $id, array $params = []): AjaxPaginator
    {
        return new AjaxPaginator($this, $id);
    }

    /**
     * Défini l'url à partir d'un array
     * @param array $url [controller => 'foo', action => 'bar']
     */
    public function setUrl(array $url)
    {
        $request = $this->_View->getRequest();
        $this->url = $url;
        $this->plugin = $url['plugin'] ?? null;
        $this->controller = $url['controller'] ?? $request->getParam('controller');
        $this->action = $url['action'] ?? $request->getParam('action');
        $this->urlParams = $url['?'] ?? [];
        $this->passed = [];
        foreach (array_filter(array_keys($url), 'is_integer') as $passed) {
            $this->passed[] = $url[$passed];
        }
    }

    /**
     * Getter de l'url
     * @return array
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Défini le nombre de résultats
     * @param int $count
     * @return $this
     */
    public function setCount(int $count)
    {
        $this->count = $count;
        return $this;
    }

    /**
     * Merges passed URL options with current pagination state to generate a pagination URL.
     *
     * @param array       $options Pagination/URL options array
     * @param string|null $model   Which model to paginate on
     * @param array       $url     URL.
     * @return array An array of URL parameters
     */
    public function generateUrlParams(array $options = [], ?string $model = null, array $url = []): array
    {
        $p = $this->urlParams;
        $options += $p;
        $newUrl = parent::generateUrlParams($options, $model) + [
            'plugin' => $this->plugin,
            'controller' => $this->controller,
            'action' => $this->action,
            '?' => $p
        ];
        foreach (array_keys($newUrl) as $key) {
            if (is_integer($key)) {
                unset($newUrl[$key]);
            }
        }
        return array_merge($this->passed, $newUrl);
    }

    /**
     * Gets the current paging parameters from the resultset for the given model
     *
     * @param string|null $model Optional model name. Uses the default if none is specified.
     * @return array The array of paging parameters for the paginated resultset.
     */
    public function params(?string $model = null): array
    {
        $params = parent::params($model);
        $count = $this->count;
        $page = $this->url['page'] ?? 1;
        $limit = $this->getConfig('limit');
        $pageCount = max((int)ceil($this->count / $limit), 1);
        $page = min($page, $pageCount);

        $start = 0;
        if ($count >= 1) {
            $start = (($page - 1) * $limit) + 1;
        }
        $end = $start + $limit - 1;
        if ($count < $end) {
            $end = $count;
        }

        return [
            'url' => $this->Url->build($this->url),
            'finder' => 'all',
            'page' => $page,
            'current' => $limit,
            'count' => $count,
            'perPage' => $limit,
            'start' => $start,
            'end' => $end,
            'prevPage' => $page > 1,
            'nextPage' => $count > ($page * $limit),
            'pageCount' => $pageCount,
            'limit' => $limit,
        ] + $params;
    }

    /**
     * Bouton scroll down
     * @param array $paramsContainer
     * @return string
     */
    public function scrollBottom(array $paramsContainer = []): string
    {
        $paramsContainer += [
            'class' => 'float-right'
        ];
        $button = $this->Fa->button(
            'fa fa-arrow-down',
            __("Scroller vers le bas"),
            [
                'class' => 'btn btn-primary',
                'onclick' => 'AsalaeGlobal.scrollBottom(this)'
            ]
        );
        return $this->Html->tag('div', $button, $paramsContainer);
    }

    /**
     * Bouton scroll up
     * @param array $paramsContainer
     * @return string
     */
    public function scrollTop(array $paramsContainer = []): string
    {
        $paramsContainer += [
            'class' => 'float-right'
        ];
        $button = $this->Fa->button(
            'fa fa-arrow-up',
            __("Scroller vers le haut"),
            [
                'class' => 'btn btn-primary',
                'onclick' => 'AsalaeGlobal.scrollTop(this)'
            ]
        );
        return $this->Html->tag('div', $button, $paramsContainer);
    }
}
