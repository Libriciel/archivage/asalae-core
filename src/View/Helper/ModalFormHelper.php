<?php
/**
 * AsalaeCore\View\Helper\ModalFormHelper
 */

namespace AsalaeCore\View\Helper;

use AsalaeCore\View\Helper\Object\ModalForm;
use AsalaeCore\View\Helper\Object\ObjectInterface;
use Bootstrap\View\Helper\ModalHelper;
use Cake\View\Helper;
use Cake\View\Helper\UrlHelper;

/**
 * Initialise une modal de formulaire et crée le bouton/lien pour l'afficher
 *
 * @category Helper
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property \AsalaeCore\View\Helper\FormHelper Form
 * @property \AsalaeCore\View\Helper\FaHelper Fa
 * @property \Cake\View\Helper\HtmlHelper Html
 * @property ModalHelper Modal
 * @property UrlHelper Url
 */
class ModalFormHelper extends Helper implements ObjectHelperInterface
{
    /**
     * Helpers utilisés
     *
     * @var array
     */
    public $helpers = [
        'Form',
        'Html',
        'Modal' => ['className' => 'Bootstrap.Modal'],
        'Url',
        'Fa'
    ];

    /**
     * Initialise une nouvelle modale
     * @param string $id
     * @param array  $params
     * @return ObjectInterface|ModalForm
     */
    public function create(string $id, array $params = []): ModalForm
    {
        return new ModalForm($this, $id, $params);
    }

    /**
     * Permet d'obtenir le javascript additionnel qui va avec la modale
     * @param string $id
     * @return string
     */
    public function modalScript(string $id): string
    {
        return "<script> AsalaeModal.append($('#$id')); </script>";
    }

    /**
     * Donne le bouton annuler
     * @return string
     */
    public function getDefaultCancelButton(): string
    {
        return $this->Form->button(
            '<i class="fa fa-times-circle-o" aria-hidden="true"></i> ' . __('Annuler'),
            [
                'data-dismiss' => 'modal',
                'class' => 'cancel',
                'type' => 'button',
            ]
        );
    }

    /**
     * Donne le bouton enregistrer
     * @return string
     */
    public function getDefaultAcceptButton(): string
    {
        return $this->Form->button(
            '<i class="fa fa-floppy-o" aria-hidden="true"></i> ' . __('Enregistrer'),
            ['bootstrap-type' => 'primary', 'class' => 'accept']
        );
    }
}
