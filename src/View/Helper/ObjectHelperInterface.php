<?php
/**
 * AsalaeCore\View\Helper\ObjectHelperInterface
 */

namespace AsalaeCore\View\Helper;

use AsalaeCore\View\Helper\Object\ObjectInterface;

/**
 * Interface des helpers créateur d'objets
 *
 * @category Helper
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
interface ObjectHelperInterface
{
    /**
     * Permet de créer et de récupérer l'objet
     *
     * @param string $id
     * @param array  $params
     * @return ObjectInterface
     */
    public function create(string $id, array $params = []): ObjectInterface;
}
