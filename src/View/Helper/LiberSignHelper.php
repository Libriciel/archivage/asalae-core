<?php
/**
 * AsalaeCore\View\Helper\LiberSignHelper
 */

namespace AsalaeCore\View\Helper;

use Cake\I18n\I18n;
use Cake\View\Helper;
use Cake\View\Helper\HtmlHelper;

/**
 * Permet d'afficher le tableau de signature
 *
 * @category Helper
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property HtmlHelper $Html
 */
class LiberSignHelper extends Helper
{

    /**
     * List of helpers used by this helper
     *
     * @var array
     */
    public $helpers = ['Html', 'Form'];

    /**
     * Insert dans $selector, un tableau de certificats utilisateur suivi d'un
     * bouton de signature (signe le $hash selon le certificat selectionné)
     * @param string            $selector
     * @param string|array|null $hash
     * @return string
     */
    public function insertSignForm(string $selector, $hash = null)
    {
        $local = I18n::getLocale();
        if (strpos($local, '_')) {
            /** @noinspection PhpUnusedLocalVariableInspection */
            list($lang, $territory) = explode('_', I18n::getLocale());
        } else {
            $lang = $local;
        }
        $hash = $hash ? json_encode($hash) : null;
        $callback = <<<EOT
    $(libersign['$selector']).one('AsalaeLiberSign.loaded', function() {
        $('$selector').append(libersign['$selector'].signButton($hash));
    });
EOT;
        $callback = $hash ? $callback : '';
        $script = <<<EOT
if (typeof libersign === 'undefined') {
    var libersign = {};
}
(function() {
    TableHelper.locale('$lang');
    libersign['$selector'] = new AsalaeLiberSign;
    libersign['$selector'].buildTable('$selector');
    $callback
})();
EOT;
        return $this->Html->tag('script', $script);
    }
}
