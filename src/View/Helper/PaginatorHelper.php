<?php
/**
 * AsalaeCore\View\Helper\PaginatorHelper
 */

namespace AsalaeCore\View\Helper;

use Cake\I18n\I18n;
use Cake\Utility\Inflector;
use Cake\View\Helper\NumberHelper;
use Cake\View\Helper\PaginatorHelper as CakePaginatorHelper;
use Cake\View\Helper\UrlHelper;
use Cake\View\View;

/**
 * Permet de générer le bloc de pagination
 *
 * @category Helper
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property UrlHelper $Url
 * @property NumberHelper $Number
 * @property \Cake\View\Helper\HtmlHelper $Html
 */
class PaginatorHelper extends CakePaginatorHelper
{
    /**
     * Default config for this class
     *
     * Options: Holds the default options for pagination links
     *
     * The values that may be specified are:
     *
     * - `url` Url of the action. See Router::url()
     * - `url['sort']` the key that the recordset is sorted.
     * - `url['direction']` Direction of the sorting (default: 'asc').
     * - `url['page']` Page number to use in links.
     * - `model` The name of the model.
     * - `escape` Defines if the title field for the link should be escaped (default: true).
     *
     * Templates: the templates used by this class
     *
     * @var array
     */
    protected $_defaultConfig = [
        'options' => [],
        'numbers' => [
            'first' => 2,
            'last' => 2,
            'modulus' => 4,
        ]
    ];

    /**
     * Constructor. Overridden to merge passed args with URL options.
     *
     * @param \Cake\View\View $View   The View this helper is being attached to.
     * @param array           $config Configuration settings for the helper.
     */
    public function __construct(View $View, array $config = [])
    {
        $this->_defaultConfig['templates'] = [
            'nextActive' => '<li class="next"><a rel="next" href="{{url}}"'
                .' data-mode="section-ajax" title="'.__('Next').'">{{text}}</a></li>',
            'nextDisabled' => '<li class="next disabled"><a href="#" onclick="return false;">{{text}}</a></li>',
            'prevActive' => '<li class="prev"><a rel="prev" href="{{url}}"'
                .' data-mode="section-ajax" title="'.__('Previous').'">{{text}}</a></li>',
            'prevDisabled' => '<li class="prev disabled"><a href="#" onclick="return false;">{{text}}</a></li>',
            'counterRange' => '<i class="start">{{start}}</i> - {{end}} of {{count}}',
            'counterPages' => '{{page}} of {{pages}}',
            'first' => '<li class="first"><a href="{{url}}" data-mode="section-ajax"'
                .' title="'.__('Page: {0}', '{{text}}').'">{{text}}</a></li>',
            'last' => '<li class="last"><a href="{{url}}" data-mode="section-ajax"'
                .' title="'.__('Page: {0}', '{{text}}').'">{{text}}</a></li>',
            'number' => '<li><a href="{{url}}" data-mode="section-ajax" '
                .'title="'.__('Page: {0}', '{{text}}').'">{{text}}</a></li>',
            'current' => '<li class="active"><a href="#" title="'.__('Page: {0}', '{{text}}').'">{{text}}</a></li>',
            'ellipsis' => '<li class="ellipsis"><button type="button" class="btn-link">&hellip;</button></li>',
            'sort' => '<a class="sortable" href="{{url}}" title="{{title}}" data-mode="section-ajax">{{text}}</a>',
            'sortAsc' => '<a class="sort-asc" href="{{url}}" title="{{title}}" data-mode="section-ajax">{{text}}</a>',
            'sortDesc' => '<a class="sort-desc" href="{{url}}" title="{{title}}" data-mode="section-ajax">{{text}}</a>',
            'sortAscLocked' => '<a class="sort-asc locked" href="{{url}}" '
                .'title="{{title}}" data-mode="section-ajax">{{text}}</a>',
            'sortDescLocked' => '<a class="sort-desc locked" href="{{url}}" '
                .'title="{{title}}" data-mode="section-ajax">{{text}}</a>',
        ];
        parent::__construct($View, $config);
    }

    /**
     * Permet de générer le bloc de pagination
     *
     * @param string $id      'select-'.$id pour le select et 'btn-'.$id pour le bouton
     * @param array  $options options du select
     * @return string
     */
    public function blocPagination(string $id = 'default-bloc-pagination', array $options = []): string
    {
        $url = $options['url'] ?? [];
        unset($options['url']);
        if (empty($options['model'])) {
            if (is_array($url) && !isset($url['controller'])) {
                $url['controller'] = $this->getView()->getRequest()->getParam('controller');
            }
            $model = is_array($url)
                ? $url['controller']
                : substr(trim($url, '/'), 0, strpos(trim($url, '/'), '/'));
            $model = Inflector::camelize($model, '-');
        } else {
            $model = $options['model'];
        }
        unset($options['model']);
        if (empty($options)) {
            $first = '';
            $second = '';
        } else {
            $first = $this->Html->tag('div', null, ['class' => 'form-group form-group-sm'])
                . $this->Html->tag(
                    'label',
                    __("Action sur les cases à cocher"),
                    ['class' => 'sr-only', 'for' => 'select-' . $id]
                )
                . $this->Html->tag('select', null, ['class' => 'form-control w-sm inline', 'id' => 'select-'.$id]);
            foreach ($options['actions'] ?? [] as $value => $message) {
                $first .= $this->Html->tag('option', $message, compact('value'));
            }
            $first .= $this->Html->tag('/select') . $this->Html->tag('/div');
            $second = $this->Html->tag(
                'button',
                '<i class="fa fa-cog" aria-hidden="true"></i> ' . __("Executer"),
                [
                    'type' => 'button', 'class' => 'btn btn-primary',
                    'id' => 'btn-' . $id
                ]
            );
        }
        $third = $this->Html->tag(
            'small',
            $this->counter(
                __(
                    'Affichage des résultats de {0} à {1} sur les<br>{2} résultats',
                    '<i class="start">{{start}}</i>',
                    '<i class="end">{{end}}</i>',
                    '<i class="count">{{count}}</i>'
                ),
                ['model' => $model]
            ),
            ['class' => 'text-muted pagination-counters']
        );
        $fourth = $this->Html->tag(
            'ul',
            $this->prev(
                '<i class="fa fa-chevron-left" aria-hidden="true"></i><span class="sr-only">'
                . __("Previous")
                . '</span>',
                ['escape' => false, 'url' => $url]
            )
            . $this->numbers(['url' => $url] + $this->getConfig('numbers'))
            . $this->next(
                '<i class="fa fa-chevron-right" aria-hidden="true"></i><span class="sr-only">' . __("Next") . '</span>',
                ['escape' => false, 'url' => $url]
            ),
            ['class' => 'pagination pagination-sm m-0']
        );

        return $this->Html->tag('div', $first, ['class' => 'col-md-2', 'id' => $id.'-bloc1'])
            . $this->Html->tag('div', $second, ['class' => 'col-md-2', 'id' => $id.'-bloc2'])
            . $this->Html->tag('div', $third, ['class' => 'col-md-3 text-left', 'id' => $id.'-bloc3'])
            . $this->Html->tag('div', $fourth, ['class' => 'col-md-5 text-right', 'id' => $id.'-bloc4']);
    }

    /**
     * Gets the current key by which the recordset is sorted
     *
     * @param string|null $model   Optional model name. Uses the default if none is specified.
     * @param array       $options Options for pagination links.
     * @return string|null The name of the key by which the recordset is being sorted, or
     *  null if the results are not currently sorted.
     * @link https://book.cakephp.org/4/en/views/helpers/paginator.html#creating-sort-links
     */
    public function sortKey(?string $model = null, array $options = []): ?string
    {
        $sort = parent::sortKey($model, $options);
        if (empty($sort) && $this->getView()->getRequest()->getParam('?.sort')) {
            $sort = $this->getView()->getRequest()->getParam('?.sort');
        }
        return $sort;
    }

    /**
     * Gets the current direction the recordset is sorted
     *
     * @param string|null $model   Optional model name. Uses the default if none is specified.
     * @param array       $options Options for pagination links.
     * @return string The direction by which the recordset is being sorted, or
     *  null if the results are not currently sorted.
     * @link https://book.cakephp.org/4/en/views/helpers/paginator.html#creating-sort-links
     */
    public function sortDir(?string $model = null, array $options = []): string
    {
        $dir = parent::sortDir($model, $options);
        if ($dir === 'asc' && $this->getView()->getRequest()->getParam('?.direction') === 'desc') {
            $dir = 'desc';
        }
        return $dir;
    }

    /**
     * Merges passed URL options with current pagination state to generate a pagination URL.
     *
     * ### Url options:
     *
     * - `escape`: If false, the URL will be returned unescaped, do only use if it is manually
     *    escaped afterwards before being displayed.
     * - `fullBase`: If true, the full base URL will be prepended to the result
     *
     * @param array       $options    Pagination options.
     * @param string|null $model      Which model to paginate on
     * @param array       $url        URL.
     * @param array       $urlOptions Array of options
     * @return string By default, returns a full pagination URL string for use
     *   in non-standard contexts (i.e. JavaScript)
     * @link https://book.cakephp.org/4/en/views/helpers/paginator.html#generating-pagination-urls
     */
    public function generateUrl(
        array $options = [],
        ?string $model = null,
        array $url = [],
        array $urlOptions = []
    ): string {
        if (!is_array($urlOptions)) {
            $urlOptions = ['fullBase' => $urlOptions];
        }
        $urlOptions += [
            'escape' => true,
            'fullBase' => false
        ];
        $urlParams = $this->generateUrlParams($options, $model);
        return $this->Url->build($urlParams, $urlOptions);
    }

    /**
     * Generates a sorting link. Sets named parameters for the sort and direction. Handles
     * direction switching automatically.
     *
     * ### Options:
     *
     * - `escape` Whether you want the contents html entity encoded, defaults to true.
     * - `model` The model to use, defaults to PaginatorHelper::defaultModel().
     * - `direction` The default direction to use when this link isn't active.
     * - `lock` Lock direction. Will only use the default direction then, defaults to false.
     *
     * @param string            $key     The name of the key that the recordset should be sorted.
     * @param string|array|null $title   Title for the link. If $title is null $key will be used
     *                                   for the title and will be generated by inflection. It
     *                                   can also be an array with keys `asc` and `desc` for
     *                                   specifying separate titles based on the direction.
     * @param array             $options Options for sorting link. See above for list of keys.
     * @return string A link sorting default by 'asc'. If the resultset is sorted 'asc' by the specified
     *  key the returned link will sort by 'desc'.
     * @link https://book.cakephp.org/4/en/views/helpers/paginator.html#creating-sort-links
     */
    public function sort(string $key, $title = null, array $options = []): string
    {
        $options += ['url' => [], 'model' => null, 'escape' => true];
        $url = $options['url'];
        unset($options['url']);

        if (empty($title)) {
            $title = $key;

            if (strpos($title, '.') !== false) {
                $title = str_replace('.', ' ', $title);
            }

            $title = I18n::getTranslator()->translate(
                Inflector::humanize(
                    preg_replace('/_id$/', '', $title)
                )
            );
        }
        $defaultDir = isset($options['direction']) ? strtolower($options['direction']) : 'asc';
        unset($options['direction']);

        $locked = $options['lock'] ?? false;
        unset($options['lock']);

        $sortKey = $this->sortKey($options['model']);
        $defaultModel = $this->defaultModel();
        $model = $options['model'] ?: $defaultModel;
        [$table, $field] = explode('.', $key . '.');
        if (!$field) {
            $field = $table;
            $table = $model;
        }
        $isSorted = (
            $sortKey === $table . '.' . $field ||
            $sortKey === $model . '.' . $key ||
            $table . '.' . $field === $model . '.' . $sortKey
        );

        $template = 'sort';
        $dir = $defaultDir;
        if ($isSorted) {
            if ($locked) {
                $template = $dir === 'asc' ? 'sortDescLocked' : 'sortAscLocked';
            } else {
                $dir = $this->sortDir($options['model']) === 'asc' ? 'desc' : 'asc';
                $template = $dir === 'asc' ? 'sortDesc' : 'sortAsc';
            }
        }
        if (is_array($title) && array_key_exists($dir, $title)) {
            $title = $title[$dir];
        }

        $url = array_merge(
            ['sort' => $key, 'direction' => $dir],
            $url,
            ['order' => null]
        );
        $vars = [
            'text' => $options['escape'] ? h($title) : $title,
            'url' => $this->generateUrl($url, $options['model']),
        ];

        // Partie surchargée
        foreach (['asc', 'desc'] as $oDir) {
            if (!empty($options[$oDir])) {
                $o = $options[$oDir];
                unset($options[$oDir]);
                if ($dir === $oDir) {
                    $options = array_merge($options, $o);
                }
            }
        }
        $vars = array_merge($vars, $options);
        // FIN

        return $this->templater()->format($template, $vars);
    }
}
