<?php
/**
 * AsalaeCore\View\Helper\InputHelper
 */

namespace AsalaeCore\View\Helper;

use Cake\View\Helper;

/**
 * Mutualise certains inputs
 *
 * @category Helper
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property FormHelper Form
 */
class InputHelper extends Helper
{
    /**
     * List of helpers used by this helper
     * @var array
     */
    protected $helpers = ['AsalaeCore.Form'];

    /**
     * Donne un select de ko/mo/go...
     * @param string $name
     * @param string $default [o, ko, mo, go, to, po]
     * @param array  $params
     * @return string
     * @noinspection PhpUnusedLocalVariableInspection - utilisé dans $default par $$name
     */
    public function mult(string $name, string $default = 'mo', array $params = []): string
    {
        $params += [
            'options' => [
                $o = 1 => __("o"),
                $ko = 1024 => __("Ko"),
                $mo = pow(1024, 2) => __("Mo"),
                $go = pow(1024, 3) => __("Go"),
                $to = pow(1024, 4) => __("To"),
                $po = pow(1024, 5) => __("Po"),
            ],
            'class' => '',
            'default' => $$default,
            'templates' => [
                'inputContainer' => '{{content}}',
                'formGroup' => '{{input}}',
                'select' => isset($params['id']) // on n'ajoute l'id que si on le fixe
                    ? '<select name="{{name}}"{{attrs}}>{{content}}</select>'
                    : '<select name="{{name}}">{{content}}</select>',
            ],
        ];
        return $this->Form->control($name, $params);
    }

    /**
     * Donne un select =, <, <=, >, >=
     * @param string $name
     * @param string $default
     * @param array  $params
     * @return string
     */
    public function operator(string $name, string $default = '=', array $params = []): string
    {
        $params += [
            'name' => $name,
            'options' => [
                '=' => __x('size', '='),
                '<=' => __x('size', '<='),
                '>=' => __x('size', '>='),
                '<' => __x('size', '<'),
                '>' => __x('size', '>')
            ],
            'default' => $default,
            'templates' => [
                'inputContainer' => '{{content}}',
                'formGroup' => '{{input}}',
                'select' => isset($params['id']) // on n'ajoute l'id que si on le fixe
                    ? '<select name="{{name}}"{{attrs}}>{{content}}</select>'
                    : '<select name="{{name}}">{{content}}</select>',
            ],
        ];
        return $this->Form->control($name, $params);
    }
}
