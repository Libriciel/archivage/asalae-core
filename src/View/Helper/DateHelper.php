<?php
/**
 * AsalaeCore\View\Helper\DateHelper
 */

namespace AsalaeCore\View\Helper;

use Cake\Core\Configure;
use Cake\I18n\FrozenDate as Date;
use Cake\I18n\FrozenTime as Time;
use Cake\View\Helper;
use Cake\View\View;

/**
 * Utile pour la gestion des dates localisés
 *
 * @category Helper
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property FaHelper Fa
 * @property HtmlHelper Html
 */
class DateHelper extends Helper
{
    /**
     * @var array Helpers utilisés
     */
    public $helpers = [
        'AsalaeCore.Fa',
        'Html',
    ];

    /**
     * @var array Locales Disponibles pour le datepicker jquery
     */
    const DATE_PICKER_AVALAIBLE_LOCALES = [
        'en', 'en-US', 'fr'
    ];

    /**
     * @var string options locales pour le datepicker
     */
    public $datePickerOptions;

    /**
     * @var string ex: UTC
     */
    public $timezone;

    /**
     * Default Constructor
     *
     * @param \Cake\View\View $View   The View this helper is being attached to.
     * @param array           $config Configuration settings for the helper.
     */
    public function __construct(View $View, array $config = [])
    {
        parent::__construct($View, $config);
        $local = Configure::read('App.defaultLocale');
        list($prefix) = explode('_', $local, 2);
        switch ($local) {
            case 'fr':
            case ($prefix === 'fr'):
                $this->datePickerOptions = "$.datepicker.regional['fr']";
                break;
            case 'en_US':
                $this->datePickerOptions = "$.datepicker.regional['en-US']";
                break;
            case 'en':
            default:
                $this->datePickerOptions = "$.datepicker.regional['en']";
        }
        $this->timezone = Configure::read('App.timezone', 'UTC');
    }

    /**
     * A utiliser dans le paramètre 'append' d'un champ de formulaire
     *
     * exemple:
     *      $this->Form->control('date', ['append' => $this->Date->picker('date')])
     *
     * @param string $selector
     * @return string
     */
    public function picker(string $selector): string
    {
        return $this->Fa->i('fa-calendar')
            . $this->Html->tag(
                'script',
                '$("'.$selector.'").last()
                    .datepicker('.$this->datePickerOptions.')
                    .siblings("span.input-group-addon")
                    .last()
                    .click(function() { $(this).siblings("input").focus(); })
                ;'
            );
    }

    /**
     * A utiliser dans le paramètre 'append' d'un champ de formulaire
     *
     * exemple:
     *      $this->Form->control('date', ['append' => $this->Date->datetimePicker('date')])
     *
     * @param string $selector
     * @return string
     */
    public function datetimePicker(string $selector): string
    {
        return $this->Fa->i('fa-calendar')
            . $this->Html->tag(
                'script',
                '$("'.$selector.'").last()
                    .datetimepicker('.$this->datePickerOptions.')
                    .siblings("span.input-group-addon")
                    .click(function() { $(this).siblings("input").focus(); })
                ;'
            );
    }

    /**
     * Affiche la date du jour au format court localisé
     * ex 16/10/2018
     * @return string
     */
    public function today(): string
    {
        $today = new Date;
        return $today->setTimezone($this->timezone)->format($today);
    }

    /**
     * Affiche la date et l'heure du jour au format court localisé
     * ex 16/10/2018 16h05
     * @return string
     */
    public function now(): string
    {
        $now = new Time;
        return $now->setTimezone($this->timezone)->format($now);
    }
}
