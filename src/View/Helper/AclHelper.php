<?php
/**
 * AsalaeCore\View\Helper\AclHelper
 */

namespace AsalaeCore\View\Helper;

use AsalaeCore\Controller\Component\AclComponent;
use AsalaeCore\Model\Table\AcosTable;
use AsalaeCore\Utility\Check;
use Authorization\Identity;
use Cake\Controller\ComponentRegistry;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\Http\ServerRequest;
use Cake\Log\Log;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;
use Cake\Utility\Hash;
use Cake\View\Helper;
use Cake\View\View;
use Exception;

/**
 * Permet de vérifier les droits d'accès à un controller/action d'un utilisateur
 *
 * @category Helper
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property \Cake\View\Helper\HtmlHelper $Html
 */
class AclHelper extends Helper
{
    /**
     * @var AclComponent
     */
    public $Acl;

    /**
     * @var AcosTable
     */
    public $Acos;

    /**
     * @var bool Connecté à la base de données
     */
    private $connected;

    /**
     * @var array Liste des controllers et de leurs params
     */
    private $controllers = [];

    /**
     * @var EntityInterface
     */
    private $root;

    /**
     * @var array Helpers utilisés
     */
    public $helpers = [
        'Html',
    ];

    public $accesses = [];

    /**
     * Construct method.
     *
     * @param View  $view
     * @param array $config
     */
    public function __construct(View $view, array $config = [])
    {
        parent::__construct($view, $config);
        $collection = new ComponentRegistry();
        $this->Acl = new AclComponent($collection);
        $this->Acos = TableRegistry::getTableLocator()->get('Acos');
        $this->connected = Check::database();
        $conf = Configure::read('App.paths.controllers_rules');
        if ($conf && is_readable($conf)) {
            $this->controllers = json_decode(file_get_contents($conf), true);
        }
        if ($this->connected) {
            try {
                $this->root = $this->Acos->find()
                    ->where(['model' => 'root', 'alias' => 'controllers'])
                    ->firstOrFail();
            } catch (Exception $e) {
                $this->connected = false;
            }
        }
    }

    /**
     * Permet de vérifier l'url pour l'utilisateur connecté
     *
     * @param mixed $url
     * @return boolean
     */
    public function check($url): bool
    {
        if (is_array($url)) {
            $u = sprintf(
                '/%s/%s',
                $url['controller'] ?? '',
                $url['action'] ?? ''
            );
            if (isset($this->accesses[$u])) {
                return $this->accesses[$u];
            }
        } elseif (is_string($url) && isset($this->accesses[$url])) {
            return $this->accesses[$url];
        }
        $request = $this->getView()->getRequest();
        /** @var Identity $identity */
        $identity = $request->getAttribute('identity');
        $user = $identity ? $identity->getOriginalData() : null;
        $user_id = Hash::get($user ?: [], 'id');
        if (empty($user_id) || !$this->connected) {
            return false;
        }
        $session = $request->getSession();
        $useSession = (bool)$session->read('Auth');

        $aro = ['model' => 'Users', 'foreign_key' => $user_id];
        $params = Router::parseRequest(new ServerRequest(['url' => Router::url($url)]));
        $sessionPath = 'Acl.'.$params['controller'].'.'.$params['action'];
        $access = $useSession ? $session->read($sessionPath) : null;
        if ($access === null) {
            $commeDroits = Hash::get($this->controllers, "{$params['controller']}.{$params['action']}.commeDroits");
            if ($commeDroits) {
                [$params['controller'], $params['action']] = explode('::', $commeDroits);
            }
            $aco = $this->Acos->find()
                ->where(
                    [
                        'alias' => $params['action'],
                        'model' => $params['controller'],
                        'lft >' => $this->root->get('lft'),
                        'rght <' => $this->root->get('rght'),
                    ]
                )
                ->first();
            if (!$aco) {
                Log::error(
                    __(
                        "La méthode {0}::{1} n'est pas définie dans les acos",
                        $params['controller'],
                        $params['action']
                    )
                );
                $access = false;
            } else {
                $access = $this->Acl->check($aro, $aco->id);
            }
            if ($useSession) {
                $session->write($sessionPath, $access);
            }
        }
        $this->accesses[$u ?? $url] = $access;
        return $access;
    }

    /**
     * Génère un lien (ou un lien avec son container) si les permissions sur
     * url le permettent
     * @param string $msg
     * @param string $url
     * @param array  $params
     * @param string $container       si besoin, génère un container (ex: 'li', 'div')
     * @param array  $containerParams
     * @return string
     */
    public function link(string $msg, $url, array $params = [], $container = '', array $containerParams = []): string
    {
        if ($url[0] !== '#' && !$this->check($url)) {
            return '';
        }
        $output = $this->Html->link($msg, $url, $params);
        if (!empty($container)) {
            $output = $this->Html->tag($container, $output, $containerParams);
        }
        return $output;
    }
}
