<?php
/**
 * AsalaeCore\View\Helper\FilterHelper
 */

namespace AsalaeCore\View\Helper;

use AsalaeCore\View\Helper\Object\Filter;
use AsalaeCore\View\Helper\Object\ObjectInterface;
use Bootstrap\View\Helper\ModalHelper;
use Cake\View\Helper;
use Cake\View\Helper\UrlHelper;

/**
 * Permet de générer des filtres de recherche
 *
 * @category Helper
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property DateHelper Date
 * @property FaHelper Fa
 * @property FormHelper Form
 * @property HtmlHelper Html
 * @property ModalFormHelper ModalForm
 * @property ModalHelper Modal
 * @property TableHelper Table
 * @property UrlHelper Url
 */
class FilterHelper extends Helper implements ObjectHelperInterface
{
    /**
     * @var array Helpers utilisés
     */
    public $helpers = [
        'AsalaeCore.Date',
        'AsalaeCore.Fa',
        'AsalaeCore.Form',
        'AsalaeCore.Html',
        'AsalaeCore.ModalForm',
        'AsalaeCore.Table',
        'Bootstrap.Modal',
        'Url',
    ];

    /**
     * Permet de créer et de récupérer l'objet
     * @param string $id
     * @param array  $params
     * @return ObjectInterface|Filter
     */
    public function create(string $id, array $params = []): Filter
    {
        return new Filter($this, $id);
    }

    /**
     * Fenetre affiché au survol pour selection des filtres
     * @param array $filters
     * @return string
     */
    public function selectSaved(array $filters): string
    {
        $selected = (int)$this->getView()->getRequest()->getParam('?.SaveFilterSelect');
        $lis = [];
        foreach ($filters as $entity) {
            $id = $entity->get('id');
            $name = h($entity->get('name'));
            $filters = array_map(
                function ($v) {
                /** @var \AsalaeCore\Model\Entity\Filter $v */
                    return $v->get('key') . '=' . urlencode($v->get('value'));
                },
                $entity->get('filters')
            );
            $filters[] = "SaveFilterSelect=$id";
            $url = $this->Url->build(
                '/'.$entity->get('controller').'/'.$entity->get('action')
            ).'?'.implode('&', $filters);
            $params = $id === $selected ? ['class' => 'active'] : [];
            $params['class'] = empty($params['class'])
                ? 'savedFilter'
                : $params['class'] . ' savedFilter';
            $lis[] = $this->Html->tag(
                'li',
                $this->Html->tag(
                    'a',
                    $this->Html->tag('i', '', ['class' => 'fa fa-filter'])
                    . ' ' . $name,
                    [
                        'class' => 'navbar-link',
                        'href' => str_replace('[', '%5B', str_replace(']', '%5D', $url))
                    ]
                ),
                $params + ['data-id' => $id]
            );
        }
        return implode(PHP_EOL, $lis).PHP_EOL;
    }
}
