<?php
/**
 * AsalaeCore\View\Helper\PrevHelper
 */

namespace AsalaeCore\View\Helper;

use Cake\View\Helper;
use Cake\View\Helper\UrlHelper;

/**
 * Permet de générer une prévisualisation
 *
 * @category Helper
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property \Cake\View\Helper\HtmlHelper $Html
 * @property UrlHelper $Url
 */
class PrevHelper extends Helper
{
    /**
     * Helpers utilisés
     *
     * @var array
     */
    public $helpers = ['Html', 'Url'];

    /**
     * Retourne un element de prévisualisation
     *
     * @param  integer $id
     * @param  string  $name
     * @param  string  $path
     * @return string
     */
    public function create(int $id, string $name, string $path): string
    {
        if (!is_readable($path)) {
            return false;
        }

        $mime = mime_content_type($path);
        if (preg_match('/^image\/*/', $mime)) {
            $prev = $this->Html->image(
                $this->Url->build(
                    [
                        'controller' => 'download',
                        'action' => 'thumbnailImage',
                        $id
                    ]
                ),
                ['class' => 'img-thumbnail', 'alt' => __('Prévisualisation')]
            );
        } else {
            $prev = $this->Html->tag(
                'div',
                '<i class="fa fa-file-o" aria-hidden="true"></i>'
                . '<span>'.pathinfo($name, PATHINFO_EXTENSION).'</span>',
                ['class' => 'prev-ext']
            );
        }

        return $prev;
    }
}
