<?php
/**
 * AsalaeCore\View\Helper\ViewTableHelper
 */

namespace AsalaeCore\View\Helper;

use Cake\Datasource\EntityInterface;
use Cake\Utility\Hash;
use Cake\View\Helper;
use Cake\View\Helper\NumberHelper;
use Exception;
use Traversable;

/**
 * Génère un tableau de données d'une entité à partir d'un simple array
 *
 * ex: echo $this->ViewTable->generate([
 *      __("Nom") => 'MaTable.name',
 *      __("En lien avec") => 'MaTable.ma_sous_table.mon_champ',
 *      __("Multi-champs") => '{MaTable.mon_champ} - {MaTable.autre|default("Non")}
 * ]);
 *
 * @category Helper
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property \Cake\View\Helper\HtmlHelper Html
 * @property NumberHelper Number
 */
class ViewTableHelper extends Helper
{
    /**
     * Helpers utilisés
     *
     * @var array
     */
    public $helpers = ['Html', 'Number'];

    /**
     * Donne un tableau d'affichage des valeurs d'une entité
     * La valeur accepte plusieurs formats:
     *  - 'MaTable.name'
     *
     * @param EntityInterface $entity
     * @param array           $trs    un array de tr, la clef est le th, la valeur le td
     * @param array           $params
     * @return string
     * @throws Exception
     */
    public function generate(EntityInterface $entity, array $trs, array $params = [])
    {
        $table = $this->Html->tag(
            'table',
            null,
            $params + [
                'class' => 'table table-striped table-hover fixed-left-th'
            ]
        ).PHP_EOL;
        $table .= $this->Html->tag('tbody').PHP_EOL;
        $tbody = '';
        foreach ($trs as $th => $td) {
            $td = $this->completeTd($entity, $td);
            $tbody .= $this->Html->tag(
                'tr',
                $this->Html->tag('th', $th)
                .$this->Html->tag('td', $td)
            ).PHP_EOL;
        }
        if (!empty($params['tbody'])) {
            return $tbody;
        }
        $table .= $tbody;
        $table .= $this->Html->tag('/tbody').PHP_EOL;
        $table .= $this->Html->tag('/table').PHP_EOL;

        return $table;
    }

    /**
     * Transforme un Foo.bar ou un {{ Foo.bar }} en valeur de l'entité
     * @param EntityInterface $entity
     * @param string          $td
     * @return string
     * @throws Exception
     */
    private function parseTd(EntityInterface $entity, string $td): string
    {
        if (strpos($td, '{') === false) {
            $td = '{' . $td . '}';
        }
        $offset = 0;
        $replaces = [];
        while (($pos = strpos($td, '{', $offset)) !== false) {
            $offset = strpos($td, '}', $pos);
            if ($offset === false) {
                throw new Exception('Missing closing brace');
            }
            $extract = substr($td, $pos +1, $offset - ($pos +1));
            $filters = array_map('trim', explode('|', $extract));
            $field = array_shift($filters);

            $value = Hash::get($entity, $field);
            if (is_bool($value)) {
                $value = $value ? __("Oui") : __("Non");
            } elseif ($value && !in_array('raw', $filters)) {
                $value = h($value);
            }
            foreach ($filters as $filter) {
                $value = $this->applyFilter($value, $filter);
            }
            $replaces['{'.$extract.'}'] = $this->flatten($value);
        }
        return str_replace(array_keys($replaces), array_values($replaces), $td);
    }

    /**
     * Applati une valeur à multiple dimentions si nécéssaire
     * @param string|array|Traversable $value
     * @return string
     */
    private function flatten($value): string
    {
        if (!is_array($value) && !is_object($value)) {
            return (string)$value;
        }
        $output = '<ul class="flatten-values">';
        foreach ($value as $key => $val) {
            $output .= '<li class="li-row">';
            if (!is_array($val) && !is_object($val)) {
                if ($key === 'password') {
                    $output .= '<span class="key">password</span> <span class="value">*****</span>';
                } elseif (is_string($key)) {
                    $output .= '<span class="key">'.$key
                        .':</span> <span class="value">'.h($val).'</span>';
                } else {
                    $output .= h($val);
                }
            } else {
                $output .= $this->flatten($val);
            }
            $output .= '</li>';
        }
        $output .= '<ul>';
        return $output;
    }

    /** @noinspection PhpUnusedPrivateMethodInspection */
    /**
     * Filtre "default" (appel dynamique)
     * @param mixed $value
     * @param mixed $default
     * @return mixed
     */
    private function default($value, $default)
    {
        return $value ?: $default;
    }

    /** @noinspection PhpUnusedPrivateMethodInspection */
    /**
     * Filtre "toReadableSize" (appel dynamique)
     * @param int $value
     * @return string
     */
    private function toReadableSize($value)
    {
        return $this->Number->toReadableSize($value);
    }

    /**
     * Converti un empilement d'array en plusieurs tableaux
     * ex:
     * [
     *      'thead du tableau' => [
     *          [ // tbody
     *              'th1' => 'value1',
     *              'th2' => 'value2',
     *          ]
     *      ]
     * ]
     * @param EntityInterface $entity
     * @param array           $tables
     * @param array           $params
     * @return string
     * @throws Exception
     */
    public function multiple(EntityInterface $entity, array $tables, array $params = []): string
    {
        $output = '';
        foreach ($tables as $th => $tbodies) {
            $output .= $this->Html->tag('table.table.table-striped.table-hover.fixed-left-th');
            $output .= $this->Html->tag('thead');
            $output .= $this->Html->tag('tr');
            $output .= $this->Html->tag('th', $th, ['colspan' => 2]);
            $output .= $this->Html->tag('/tr');
            $output .= $this->Html->tag('/thead');
            foreach ($tbodies as $trs) {
                $output .= $this->Html->tag('tbody');
                $output .= $this->generate($entity, $trs, ['tbody' => true] + $params);
                $output .= $this->Html->tag('/tbody');
            }
            $output .= $this->Html->tag('/table');
        }
        return $output;
    }

    /**
     * Complète les valeurs du $td par le contenu de $entity
     * @param EntityInterface $entity
     * @param string|callable $td
     * @return string
     * @throws Exception
     */
    private function completeTd(EntityInterface $entity, $td): string
    {
        if (is_callable($td)) {
            $td = $td($entity);
            if (strpos($td, '{') === false) {
                $td = '{do_not_parse}'.$td;
            }
        }
        if (strpos($td, '{do_not_parse}') === false) {
            if (strpos($td, '{n}') !== false) {
                [$before, $after] = explode('{n}', $td);
                $ul = PHP_EOL . $this->Html->tag('ul') . PHP_EOL;
                $before = trim($before, '.');
                $after = trim($after, '.');
                foreach (Hash::get($entity, $before, []) as $n => $base) {
                    $path = "$before.$n.$after";
                    $parsed = trim($this->parseTd($entity, $path));
                    if ($parsed) {
                        $ul .= $this->Html->tag('li', $parsed) . PHP_EOL;
                    }
                }
                $ul .= $this->Html->tag('/ul') . PHP_EOL;
                $td = $ul;
            } else {
                $td = $td ? $this->parseTd($entity, $td) : '';
            }
        } else {
            $td = str_replace('{do_not_parse}', '', $td);
        }
        return $td;
    }

    /**
     * Applique un filtre. ex: ' test '|trim donnera 'test'
     * @param mixed  $value
     * @param string $filter
     * @return mixed
     */
    private function applyFilter($value, string $filter)
    {
        $argTokens = [
            T_LNUMBER, T_DNUMBER, T_CONSTANT_ENCAPSED_STRING
        ];
        $func = null;
        $args = [$value];
        foreach (token_get_all('<?php _'.$filter.'; ?>') as $token) {
            if (!$func && is_array($token) && $token[0] === T_STRING) {
                // retire le "_" ajouté au début par protection contre les mots clefs réservés (ex: default)
                $func = substr($token[1], 1);
            } elseif (is_array($token) && $token[0] === T_STRING) {
                switch (strtolower($token[1])) {
                    case 'true':
                    case 'false':
                    case 'null':
                        // $v vaut forcement null, true ou false (pas de risques)
                        $args[] = eval($token[1]); // NOSONAR
                        break;
                }
            } elseif (is_array($token) && in_array($token[0], $argTokens)) {
                $args[] = trim($token[1], '\'"');
            }
        }
        if (method_exists($this, $func)) {
            $value = call_user_func_array([$this, $func], $args);
        } elseif (function_exists($func)) {
            $value = call_user_func_array($func, $args);
        }
        return $value;
    }
}
