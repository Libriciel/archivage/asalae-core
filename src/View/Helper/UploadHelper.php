<?php

/**
 * AsalaeCore\View\Helper\UploadHelper
 */

namespace AsalaeCore\View\Helper;

use AsalaeCore\View\Helper\Object\Upload;
use Cake\Core\Configure;
use Cake\View\Helper;
use Exception;

/**
 * Permet de générer les outils pour effectuer l'upload
 *
 * @category Helper
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @property \Cake\View\Helper\HtmlHelper Html
 * @property \AsalaeCore\View\Helper\TableHelper Table
 * @property FaHelper Fa
 */
class UploadHelper extends Helper
{
    /**
     * Helpers utilisés
     *
     * @var array
     */
    public $helpers = ['Html', 'AsalaeCore.Fa', 'AsalaeCore.Table'];

    /**
     * @var array Configuration du helper
     */
    public $config;

    /**
     * Initialisation du helper
     *
     * @param array $config
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->config = (array)Configure::read('Flow') + [
            'chunkSize' => 10 * 1024 * 1024,
            'target' => '/upload',
            'simultaneousUploads' => 1,
        ];
    }

    /**
     * Créer une dropbox
     *
     * @param  string $domId
     * @param array  $params
     * @return Upload
     * @throws Exception
     */
    public function create(string $domId, array $params = []): Upload
    {
        return new Upload($this, $domId, $params);
    }

    /**
     * Donne l'id de la <table> lié
     * @param string $uploadId
     * @return string
     */
    public function getTableId(string $uploadId)
    {
        return $uploadId . '-table';
    }
}
