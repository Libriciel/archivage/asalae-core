<?php
/**
 * AsalaeCore\DataType\CommaArrayString
 */

namespace AsalaeCore\DataType;

use ArrayObject;

/**
 * Inspiré du CSV
 * Permet d'avoir un champ de type array sous la forme string par séparation des
 * valeurs par des virgules.
 *
 * @category DataType
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class CommaArrayString extends ArrayObject implements StringDataTypeInterface
{
    /**
     * Rendu du champ sous la forme value1,value2,value3
     * @return string
     */
    public function __toString(): string
    {
        $strArray = [];
        foreach ((array)$this as $value) {
            if (strpos($value, ',') !== false || strpos($value, '"') !== false) {
                $strArray[] = '"'.str_replace('"', '""', $value).'"';
            } else {
                $strArray[] = $value;
            }
        }
        return implode(',', $strArray);
    }

    /**
     * Serialize un string
     * @param string $value
     * @return CommaArrayString
     */
    public static function createFromString(string $value): ?CommaArrayString
    {
        $array = [];
        $pointer = 0;
        $escapedString = false;
        $doubleQuote = false;
        $curStr = '';
        while ($cur = mb_substr($value, $pointer, 1)) {
            $next = mb_substr($value, $pointer +1, 1);
            if ($cur === '"') {
                if ($escapedString === false) {
                    $escapedString = true;
                } elseif ($doubleQuote === false && $next === '"') {
                    $doubleQuote = true;
                } elseif ($doubleQuote) {
                    $doubleQuote = false;
                    $curStr .= '"';
                } else {
                    $escapedString = false;
                }
            } elseif ($cur === ',') {
                if ($escapedString === false) {
                    $array[] = $curStr;
                    $curStr = '';
                } else {
                    $curStr .= ',';
                }
            } else {
                $curStr .= $cur;
            }
            if ($next === '') {
                $array[] = $curStr;
                $curStr = '';
            }
            $pointer++;
        }
        return new self($array);
    }
}
