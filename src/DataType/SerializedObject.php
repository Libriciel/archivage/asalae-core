<?php
/**
 * AsalaeCore\DataType\SerializedObject
 */

namespace AsalaeCore\DataType;

use ArrayObject;

/**
 * Stockage d'un serialize sous forme de string
 *
 * @category DataType
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class SerializedObject extends ArrayObject implements ObjectDataTypeInterface
{
    /**
     * @var mixed objet stocké
     */
    private $object;

    /**
     * Construct a new array object
     * @link https://php.net/manual/en/arrayobject.construct.php
     * @param array|object $array         The input parameter accepts an array or an Object.
     * @param int          $flags         Flags to control the behaviour of the ArrayObject object.
     * @param string       $iteratorClass Specify the class that will be used for iteration of the ArrayObject object.
     *                                    ArrayIterator is the default class used.
     */
    public function __construct($array = array(), $flags = 0, $iteratorClass = "ArrayIterator")
    {
        $this->object = $array;
        parent::__construct($array, $flags, $iteratorClass);
    }

    /**
     * Rendu du champ sous la forme serialized
     * @return string
     */
    public function __toString(): string
    {
        return serialize($this->object);
    }

    /**
     * Permet de récupérer l'object stocké dans l'ObjectData
     * @return mixed
     */
    public function getObject()
    {
        return $this->object;
    }

    /**
     * Serialize un string
     * @param mixed $value
     * @return JsonString|null
     */
    public static function createFromObject($value): ?SerializedObject
    {
        $object = unserialize($value);
        return $object ? new self($object) : null;
    }
}
