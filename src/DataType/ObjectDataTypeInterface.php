<?php
/**
 * AsalaeCore\DataType\ObjectDataTypeInterface
 */

namespace AsalaeCore\DataType;

/**
 * Permet de stocker un object sous forme serialized
 *
 * @category DataType
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
interface ObjectDataTypeInterface
{
    /**
     * Rendu du champ sous la forme de string
     * @return string
     */
    public function __toString(): string;

    /**
     * Permet de récupérer l'object stocké dans l'ObjectData
     * @return mixed
     */
    public function getObject();

    /**
     * Serialize un string
     * @param mixed $value
     * @return ObjectDataTypeInterface|null
     */
    public static function createFromObject($value): ?ObjectDataTypeInterface;
}
