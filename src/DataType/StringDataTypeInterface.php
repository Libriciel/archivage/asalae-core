<?php
/**
 * AsalaeCore\DataType\CommaArrayString
 */

namespace AsalaeCore\DataType;

/**
 * Permet de stocker un array sous forme de string (pour getter/setter d'entité)
 *
 * @category DataType
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
interface StringDataTypeInterface
{
    /**
     * Rendu du champ sous la forme de string
     * @return string
     */
    public function __toString(): string;

    /**
     * Serialize un string
     * @param string $value
     * @return StringDataTypeInterface|null
     */
    public static function createFromString(string $value): ?StringDataTypeInterface;
}
