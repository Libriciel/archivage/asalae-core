<?php
/**
 * AsalaeCore\DataType\FormDataInterface
 */

namespace AsalaeCore\DataType;

/**
 * Permet de fournir au FormHelper une valeur différente de celle en base
 *
 * @category DataType
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
interface FormDataInterface
{
    /**
     * Rendu du champ pour le formulaire
     * @return string
     */
    public function toFormData();
}
