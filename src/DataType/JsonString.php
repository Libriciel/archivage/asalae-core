<?php
/**
 * AsalaeCore\DataType\JsonString
 */

namespace AsalaeCore\DataType;

use ArrayObject;

/**
 * Stockage d'un json sous forme de string
 *
 * @category DataType
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class JsonString extends ArrayObject implements StringDataTypeInterface, FormDataInterface
{
    /**
     * @var null|string|callable fonction de filtrage
     */
    public $filter;
    /**
     * @var int
     * @link https://php.net/manual/en/json.constants.php
     */
    public $flags = JSON_UNESCAPED_SLASHES;

    /**
     * Défini le filtre pour __toString()
     * @param null|string|callable $filter
     */
    public function setFilter($filter)
    {
        $this->filter = $filter;
    }

    /**
     * Rendu du champ sous la forme json
     * @return string
     */
    public function __toString(): string
    {
        $arr = (array)$this;
        if ($this->filter) {
            $arr = array_filter($arr, $this->filter);
        }
        return json_encode($arr, $this->flags);
    }

    /**
     * Serialize un string
     * @param string $value
     * @return JsonString|null
     */
    public static function createFromString(string $value): ?JsonString
    {
        $json = json_decode($value, true);
        return $json !== null ? new self($json) : null;
    }

    /**
     * Donne un JsonString adapté à $value
     * @param string|JsonString|null|array $value
     * @return JsonString|null
     */
    public static function create($value): ?JsonString
    {
        if (is_string($value) && $value) {
            return self::createFromString($value);
        } elseif ($value instanceof JsonString) {
            return $value;
        } elseif (!$value) {
            return new self;
        } else {
            return new JsonString($value);
        }
    }

    /**
     * Rendu du champ pour le formulaire
     * @return string
     */
    public function toFormData()
    {
        return (string)$this;
    }
}
