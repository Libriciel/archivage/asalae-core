<?php
/**
 * AsalaeCore\DataType\EncryptedString
 */

namespace AsalaeCore\DataType;

use Cake\Utility\Security;

/**
 * Stockage d'un mot de passe sous forme de string
 *
 * @category DataType
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class EncryptedString implements StringDataTypeInterface, FormDataInterface
{
    const DECRYPT_KEY = 'encrypted_string';

    /**
     * @var string clé de chiffrement
     */
    private static $key;

    /**
     * @var string valeur non chiffrée
     */
    private $value;

    /**
     * EncryptedString constructor.
     * @param string $value valeur chiffrée
     */
    public function __construct(string $value)
    {
        $this->value = $value;
    }

    /**
     * Rendu du champ sous la forme chiffrée
     * @return string
     */
    public function __toString(): string
    {
        return $this->value;
    }

    /**
     * Serialize un string
     * @param string $value valeur non chiffrée
     * @return JsonString|null
     */
    public static function createFromString(string $value): ?EncryptedString
    {
        return new self(Security::encrypt($value, self::decryptKey()));
    }

    /**
     * Rendu du champ pour le formulaire
     * @return string
     */
    public function toFormData()
    {
        return Security::decrypt($this->value, self::decryptKey());
    }

    /**
     * Donne la clé pour manipuler le mot de passe
     * @return string
     */
    private static function decryptKey(): string
    {
        if (empty(self::$key)) {
            self::$key = hash('sha256', self::DECRYPT_KEY);
        }
        return self::$key;
    }
}
