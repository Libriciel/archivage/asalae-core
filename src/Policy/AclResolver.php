<?php
/**
 * AsalaeCore\Policy\AclResolver
 */

namespace AsalaeCore\Policy;

use Authorization\Policy\Exception\MissingPolicyException;
use Authorization\Policy\ResolverInterface;

/**
 * AclResolver
 *
 * @category Policy
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class AclResolver implements ResolverInterface
{

    /**
     * Resolves the policy object based on the authorization resource.
     *
     * The resolver MUST throw the `\Authorization\Policy\Exception\MissingPolicyException`
     * exception if a policy cannot be resolved for a given resource.
     *
     * @param mixed $resource A resource that the access is checked against.
     * @return AclPolicy
     * @throws MissingPolicyException If a policy cannot be resolved.
     */
    public function getPolicy($resource)
    {
        return new AclPolicy;
    }
}
