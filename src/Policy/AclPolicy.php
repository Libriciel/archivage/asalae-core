<?php
/**
 * AsalaeCore\Policy\AclPolicy
 */

namespace AsalaeCore\Policy;

use AsalaeCore\Controller\Component\AclComponent;
use AsalaeCore\Model\Entity\AuthUrl;
use Authorization\IdentityInterface;
use Authorization\Policy\RequestPolicyInterface;
use Cake\Controller\ComponentRegistry;
use Cake\Core\Configure;
use Cake\Http\Exception\BadRequestException;
use Cake\Http\Exception\MethodNotAllowedException;
use Cake\Http\ServerRequest;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;
use Cake\Utility\Hash;
use Cake\Utility\Inflector;

/**
 * AclPolicy
 *
 * @category Policy
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class AclPolicy implements RequestPolicyInterface
{
    const ACL_ADMIN_FILENAME = RESOURCES . 'admin_acl.json';

    /**
     * @var array
     */
    public $controllers = [];
    /**
     * @var AclComponent
     */
    public $Acl;
    /**
     * @var Table
     */
    public $Acos;
    /**
     * @var Table
     */
    public $AuthUrls;

    /**
     * AclPolicy constructor.
     */
    public function __construct()
    {
        $collection = new ComponentRegistry();
        $this->Acl = new AclComponent($collection);
        $loc = TableRegistry::getTableLocator();
        $this->Acos = $loc->get('Acos');
        $this->AuthUrls = $loc->get('AuthUrls');
        $conf = Configure::read('App.paths.controllers_rules');
        if ($conf && is_readable($conf)) {
            $this->controllers = json_decode(file_get_contents($conf), true);
        }
    }

    /**
     * Method to check if the request can be accessed
     *
     * @param IdentityInterface|null $identity Identity
     * @param ServerRequest          $request  Server Request
     * @return bool
     */
    public function canAccess(?IdentityInterface $identity, ServerRequest $request)
    {
        if (!$identity) {
            return true; // gestion de l'accès par AuthenticationService
        }
        $user = $identity->getOriginalData();
        $url = Router::parseRequest($request);
        $controller = $url['controller'] ?? 'Home';
        $action = $url['action'] ?? 'index';

        // cas particulier pour empêcher la deconnexion
        if ($controller == 'Users' && $action === 'logout') {
            return true;
        }

        // cas particulier pour redirection suite à un auth-urls/activate/{code}
        if ($controller === 'AuthUrls' && $action === 'activate' && !empty($user['code'])) {
            return !empty($user['url_id']);
        }

        // Accès par url (email)
        if (!empty($user['url']) && !empty($user['code'])) {
            /** @var AuthUrl $code */
            $code = $this->AuthUrls->find()
                ->where(['code' => $user['code']])
                ->contain(['AuthSubUrls'])
                ->first();
            return $code && $code->allowed($request);
        }

        if ($action === 'api') {
            $root = 'api';
            if (isset($url['pass'][0]) && is_string($url['pass'][0]) && !is_numeric($url['pass'][0])) {
                $action = Inflector::variable($url['pass'][0]);
            } else {
                $action = null;
            }
            $tableMethods = [
                'post' => 'create',
                'get' => 'read',
                'put' => 'update',
                'delete' => 'delete'
            ];
            $method = strtolower($request->getEnv('REQUEST_METHOD'));
            if (!$method) {
                throw new BadRequestException(__("400 Bad Request"));
            }
            if (!isset($tableMethods[$method])) {
                throw new MethodNotAllowedException(__("405 Method Not Allowed"));
            }
            $crud = $tableMethods[$method];
        } else {
            $root = 'controllers';
            $commeDroits = Hash::get($this->controllers, "$controller.$action.commeDroits");
            if ($commeDroits) {
                [$controller, $action] = explode('::', $commeDroits);
            }
            $crud = '*';
        }
        $path = "$root/$controller".($action ? "/$action" : '');
        if (Hash::get($user, 'admin')) {
            return $this->adminCanAccess($path);
        }
        if (empty($user['id'])) {
            return false; // on ne devrait jamais arriver ici
        }
        return $this->Acl->check(
            ['Users' => $user],
            $path,
            $crud
        );
    }

    /**
     * Acl par fichier json pour l
     * @param string $path
     * @return bool
     */
    private function adminCanAccess(string $path)
    {
        $aclPath = self::ACL_ADMIN_FILENAME;
        if (!$path
            || !is_readable($aclPath)
            || !($acls = json_decode(file_get_contents($aclPath) ?: '', true))
        ) {
            return false;
        }
        $exp = explode('/', $path);
        $base = $acls[$exp[0]] ?? false;
        if (is_bool($base)) {
            return $base;
        }
        if (!is_array($base)) {
            return false;
        }

        $controller = $base[$exp[1]] ?? false;
        if (is_bool($controller)) {
            return $controller;
        }
        if (!is_array($controller)) {
            return false;
        }

        $action = $controller[$exp[2]] ?? false;
        if (is_bool($action)) {
            return $action;
        }
        return false;
    }
}
