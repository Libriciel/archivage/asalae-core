<?php
/**
 * Cake\Validation\RulesProvider
 */
namespace AsalaeCore\Validation;

use Cake\I18n\FrozenTime as Time;
use Cake\Validation\RulesProvider as CakeRulesProvider;
use DateTime;
use DateTimeInterface;
use Exception;
use ReflectionException;

/**
 * Permet de faire passer les dates au format fr
 *
 * @category Validation
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class RulesProvider extends CakeRulesProvider
{
    /**
     * Validation d'une date (compatible format fr)
     * @param string|DateTimeInterface $check
     * @param string                   $dateFormat
     * @return mixed
     * @throws ReflectionException
     * @throws Exception
     */
    public function date($check, $dateFormat = 'ymd')
    {
        if (is_string($check)) {
            if (preg_match('/^\d{4}-\d{2}-\d{2}([ T]\d{2}:\d{2}(:\d{2})?)?/', $check)) {
                $check = new DateTime($check);
            } else {
                $check = Time::parseDate($check);
            }
        }

        $method = $this->_reflection->getMethod('datetime');
        $object = is_string($this->_class) ? null : $this->_class;
        return $method->invokeArgs($object, [$check ?: '', $dateFormat, null]);
    }

    /**
     * Validation d'un datetime (compatible format fr)
     * @param string|DateTimeInterface $check
     * @param string                   $dateFormat
     * @return mixed
     * @throws ReflectionException
     * @throws Exception
     */
    public function datetime($check, $dateFormat = 'ymd')
    {
        if (is_string($check)) {
            if (preg_match('/^\d{4}-\d{2}-\d{2}([ T]\d{2}:\d{2}(:\d{2})?)?/', $check)) {
                $check = new DateTime($check);
            } else {
                $check = Time::parseDate($check);
            }
        }

        $method = $this->_reflection->getMethod('datetime');
        $object = is_string($this->_class) ? null : $this->_class;
        return $method->invokeArgs($object, [$check ?: '', $dateFormat, null]);
    }

    /**
     * Validation d'un champ boolean
     * @param mixed $value
     * @return bool
     */
    public function boolean($value)
    {
        $value = filter_var($value, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
        return is_bool($value);
    }
}
