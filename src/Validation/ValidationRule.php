<?php
/**
 * AsalaeCore\Validation\ValidationRule
 */

namespace AsalaeCore\Validation;

use Cake\Validation\ValidationRule as CakeValidationRule;

/**
 * Surcharge de ValidationRule de Cakephp afin de pouvoir utiliser des messages
 * d'erreur dynamique
 *
 * @category Validation
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ValidationRule extends CakeValidationRule
{
    /**
     * Setter pour $this->_message
     *
     * @param string $message
     * @return self
     */
    public function setMessage(string $message): self
    {
        $this->_message = $message;
        return $this;
    }
}
