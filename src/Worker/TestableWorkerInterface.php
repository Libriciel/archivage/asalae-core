<?php
/**
 * AsalaeCore\Worker\TestableWorkerInterface
 */

namespace AsalaeCore\Worker;

/**
 * Implémentation possible de test pour le worker
 *
 * @category Worker
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
interface TestableWorkerInterface
{
    /**
     * Emission d'un message de test
     * A utiliser avant getTestResults()
     * @param array $params optionnel, si le test à besoin de paramètres
     */
    public function emitTest(array $params = []);

    /**
     * Permet d'obtenir les résultats du test
     * @return array ['success' => (bool), 'errors' => (array), 'message' => (string)]
     */
    public function getTestResults(): array;
}
