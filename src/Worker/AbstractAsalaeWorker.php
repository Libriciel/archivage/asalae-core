<?php
/**
 * AsalaeCore\Worker\AbstractAsalaeWorker
 */

namespace AsalaeCore\Worker;

use AsalaeCore\Middleware\MaintenanceMiddleware;
use AsalaeCore\Utility\Config;
use AsalaeCore\Utility\Notify;
use Beanstalk\Exception\CantWorkException;
use Beanstalk\Command\AbstractWorker;
use Beanstalk\Utility\Beanstalk;
use Cake\Console\Arguments;
use Cake\Database\Connection;
use Cake\Datasource\ConnectionManager;
use DateTime;
use Exception;
use Pheanstalk\Exception\ServerException;
use Throwable;
use ZMQSocketException;

/**
 * Fonctions communes pour les workers asalae
 *
 * @category Worker
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
abstract class AbstractAsalaeWorker extends AbstractWorker
{
    /**
     * @var array Prérequis du worker ['monprerequis' => true, ...]
     */
    public $prerequisites = [];

    /**
     * @var float microtime
     */
    protected $lastTouch;

    /**
     * {@inheritdoc}
     */
    public function start()
    {
        $tube = $this->Worker->getTube();
        $pid = getmypid();
        $host = gethostname();
        Notify::emit(
            'worker_start',
            [
                'tube' => $tube,
                'pid' => $pid,
                'host' => $host,
            ]
        );
        parent::start();
        Notify::emit(
            'worker_stop',
            [
                'tube' => $tube,
                'pid' => $pid,
                'host' => $host,
            ]
        );
    }

    /**
     * {@inheritdoc}
     *
     * @param Throwable $ex   Exception envoyé depui le bloc start()
     * @param mixed     $data Data obtenu par Beanstalkd
     * @throws ZMQSocketException
     */
    public function fail(Throwable $ex, $data)
    {
        $conn = ConnectionManager::get('default');
        if ($conn instanceof Connection && $conn->inTransaction()) {
            $conn->rollback(true);
        }
        $msg = substr($ex->getMessage(), 0, 1024); // limite la taille du message
        if (!empty($this->userId)) {
            $this->notify($msg, 'alert-danger');
        }
        $this->websocketMessage('work_end', $this->data + ['error_message' => $msg]);
        parent::fail($ex, $data);
    }

    /**
     * {@inheritdoc}
     *
     * @param mixed $params
     * @return boolean
     */
    public function keepWorking($params)
    {
        return parent::keepWorking($params);
    }

    /**
     * Donne un titre à afficher pour les notifications
     *
     * @return string
     */
    protected function getTitle(): string
    {
        return '<h4>'.$this->title.'</h4>';
    }

    /**
     * @var bool N'enverra pas de notifications si vrai
     */
    protected $disabledNotifications = false;

    /**
     * @var integer Garde en mémoire l'id de l'utilisateur
     */
    protected $userId = 0;

    /**
     * @var array data
     */
    protected $data = [];

    /**
     * Initialise les notifications en fonction de data
     * @param mixed $data
     * @throws ZMQSocketException
     * @throws ServerException
     */
    public function beforeWork($data)
    {
        if (Config::readAll('Interruption.enable_workers') === false
            && MaintenanceMiddleware::isInterrupted()
        ) {
            $this->log("Work interrupted");
            $this->Worker->cancel();
            $this->shutdown();
        }
        $data = ((array)$data) + [
            'jobid' => $this->Worker->getJob()->getId(),
            'tube' => $this->Worker->getTube(),
        ];
        $this->websocketMessage('work_begin', $data);
        $this->disabledNotifications
            = (($data['silent'] ?? false) && $data['silent'])
            || empty($data['user_id']);
        if (!empty($data['user_id'])) {
            $this->userId = $data['user_id'];
        }
        $this->data = $data;
        $this->lastTouch = microtime(true);
        parent::beforeWork($data);
    }

    /**
     * Lancé après work()
     * @param mixed $data
     * @throws ZMQSocketException
     */
    public function afterWork($data)
    {
        $this->websocketMessage('work_end', $this->data);
        parent::afterWork($data);
    }

    /**
     * Envoi une notification à l'utilisateur
     * @param string $message
     * @param string $cssClass
     * @throws ZMQSocketException
     */
    protected function notify(string $message, string $cssClass = 'alert-info')
    {
        if (!$this->disabledNotifications) {
            Notify::send(
                $this->userId,
                [],
                $this->getTitle() . $message,
                $cssClass
            );
            $this->log(__("notification envoyée à {0}: {1}", 'user_'.$this->userId, $message));
        }
    }

    /**
     * Permet d'envoyer un message sur websocket
     * @param string $chanel
     * @param array  $data
     * @throws ZMQSocketException
     */
    protected function websocketMessage(string $chanel, $data = [])
    {
        if ($this->userId) {
            $data = $data ?: $this->data;
            $message = is_array($data) && !empty($this->params['tube'])
                ? $data  + ['tube' => $this->params['tube']]
                : $data
            ;
            Notify::emit($chanel, $message);
        }
    }

    /**
     * Permet d'émettre un nouveau job
     * @param string $tube
     * @param array  $data
     * @param int    $priority
     * @param int    $delay
     * @param int    $ttr
     * @throws Exception
     */
    protected function emit(
        string $tube,
        $data = [],
        int $priority = 1024,
        int $delay = 0,
        int $ttr = 60
    ) {
        if (is_array($data)) {
            if (!empty($this->data['id'])) {
                $data += ['id' => $this->data['id']];
            }
            if (!empty($this->userId)) {
                $data += ['user_id' => $this->userId];
            }
        }
        $instance = $this->Worker->getTube() !== $tube
            ? new Beanstalk($tube)
            : $this->Worker;
        $instance->emit(
            $data,
            $priority,
            $delay,
            $ttr
        );
    }

    /**
     * Envoi une exception et averti l'utilisateur de l'erreur qui empèche le
     * job de bien fonctionner.
     * @param string $message
     * @return CantWorkException
     * @throws ZMQSocketException
     */
    protected function cantWork(string $message)
    {
        $this->notify($message, 'alert-danger');
        return new CantWorkException($message);
    }

    /**
     * {@inheritdoc}
     */
    public function submitPrerequisites()
    {
        $this->emit(
            'prerequisites',
            [
                'host' => gethostname(),
                'pid' => getmypid(),
                'tube' => !empty($this->params['tube']) ? $this->params['tube'] : '',
                'date' => (new DateTime())->format('Y-m-d H:i:s'),
                'prerequisites' => $this->prerequisites
            ],
            10
        );
        parent::submitPrerequisites();
    }

    /**
     * Commande d'arret du worker
     */
    public function stopWorker()
    {
        $this->shutdown();
    }

    /**
     * Commande de redemarrage du worker
     */
    public function restartWorker()
    {
        $options = [];
        if ($this->params['shell']['args'] instanceof Arguments) {
            /** @var Arguments $args */
            $args = $this->params['shell']['args'];
            $arg = $args->getArgumentAt(0);
            foreach ($args->getOptions() as $option => $value) {
                if (is_bool($value)) {
                    if ($value) {
                        $options[] = '--'.$option;
                    }
                } else {
                    $options[] = '--'.$option.' '.escapeshellarg($value);
                }
            }
        } else {
            $arg = $this->params['shell']['args'][0];
            foreach ($this->params['shell']['params'] as $key => $value) {
                if (is_bool($value)) {
                    if ($value) {
                        $options[] = '--'.$key;
                    }
                } else {
                    $options[] = '--'.$key.' '.$value;
                }
            }
        }
        $command = sprintf(
            'nohup sleep 0.5; '.ROOT.DS.'bin'.DS.'cake worker %s %s > /dev/null &',
            escapeshellarg($arg),
            implode(' ', $options)
        );
        $this->log('Worker restarting...');
        $this->log($command);
        exec($command);
        $this->shutdown();
    }

    /**
     * Permet d'obtenir des stats d'un worker
     * @param array $stats
     * @return void
     * @throws Exception
     */
    public function submitGetStats(array $stats)
    {
        $this->emit('stats-response', $stats);
    }

    /**
     * Commande pour vérifier que le worker répond
     * @throws Exception
     */
    public function ping()
    {
        $this->emit('ping', $this->params['id']);
    }

    /**
     * Limite le touch à 1 toutes les 10 secondes
     * @param array|mixed $params
     * @return array|mixed
     * @throws ServerException
     */
    public function touch($params = [])
    {
        $mtime = microtime(true);
        if ($this->lastTouch && ($mtime - $this->lastTouch) > 10.0) {
            $this->lastTouch = $mtime;
            return parent::touch($params);
        }
        return $params;
    }
}
