<?php
/**
 * AsalaeCore\Worker\HashWorker
 */

namespace AsalaeCore\Worker;

use Cake\Console\ConsoleIo;
use Cake\Datasource\EntityInterface;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;

/**
 * Permet de tester les actions liés aux workers
 * Attend 1 minute par job
 *
 * @category Worker
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class TestWorker extends AbstractAsalaeWorkerV4 implements TestableWorkerInterface
{
    /**
     * @var string nom du fichier temporaire de test du worker
     */
    const TEST_FILENAME = 'dummy-test-hash-worker';

    /**
     * @var string /path/to/tmp/filename
     */
    private $tmpFilename;

    /**
     * TestWorker constructor.
     * @param EntityInterface $workerEntity
     * @param ConsoleIo       $io
     * @noinspection PhpDocSignatureInspection v4
     */
    public function __construct(EntityInterface $workerEntity, ConsoleIo $io = null)
    {
        parent::__construct($workerEntity, $io);
        $this->tmpFilename = sys_get_temp_dir() . DS . 'emittest-destination' . DS . self::TEST_FILENAME;
    }

    /**
     * Effectue le travail voulu
     *
     * @param mixed $data
     * @throws Exception
     */
    public function work($data)
    {
        sleep($data['duration'] ?? 60); //NOSONAR
        if (isset($data['error']) && ((is_bool($data['error']) && $data['error'])
            || (is_numeric($data['error']) && rand(0, 100) <= $data['error'])) // NOSONAR
        ) {
            throw new Exception('test error');
        }
        Filesystem::dumpFile($this->tmpFilename, 'this is a test file');
    }

    /**
     * Emission d'un message de test
     * A utiliser avant getTestResults()
     * @param array $params optionnel, si le test à besoin de paramètres
     * @throws Exception
     */
    public function emitTest(array $params = [])
    {
        $data = ['duration' => 5];
        if (is_file($this->tmpFilename)) {
            Filesystem::remove($this->tmpFilename);
        }
        $this->emit($this->params['tube'], $data);
    }

    /**
     * Permet d'obtenir les résultats du test
     * @return array ['success' => (bool), 'errors' => (array), 'message' => (string)]
     * @throws Exception
     */
    public function getTestResults(): array
    {
        $success = false;
        if (is_file($this->tmpFilename)) {
            $success = file_get_contents($this->tmpFilename) === 'this is a test file';
            Filesystem::remove($this->tmpFilename);
        }
        return [
            'success' => $success,
            'error' => $success ? '' : "Result file not found"
        ];
    }
}
