<?php
/**
 * AsalaeCore\Worker\AbstractAsalaeWorker
 */

namespace AsalaeCore\Worker;

use AsalaeCore\Middleware\MaintenanceMiddleware;
use AsalaeCore\Utility\Config;
use AsalaeCore\Utility\Notify;
use Beanstalk\Utility\Beanstalk;
use Cake\Console\ConsoleIo;
use Cake\Console\Exception\StopException;
use Cake\Database\Connection;
use Cake\Datasource\ConnectionManager;
use Cake\Datasource\EntityInterface;
use Cake\TestSuite\Stub\ConsoleInput;
use Cake\TestSuite\Stub\ConsoleOutput;
use Exception;
use Pheanstalk\PheanstalkInterface;
use ZMQSocketException;

/**
 * Fonctions communes pour les workers asalae
 *
 * @category Worker
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
abstract class AbstractAsalaeWorkerV4
{
    /**
     * @var ConsoleIo
     */
    protected ConsoleIo $io;
    /**
     * @var Beanstalk
     */
    protected Beanstalk $Beanstalk;
    /**
     * @var EntityInterface
     */
    protected EntityInterface $workerEntity;
    /**
     * @var int
     */
    protected $userId;
    /**
     * @var array
     */
    protected array $data;

    /**
     * WorkerInterface constructor.
     * @param EntityInterface $workerEntity
     * @param ConsoleIo       $io
     * @noinspection PhpDocSignatureInspection v4
     */
    public function __construct(EntityInterface $workerEntity, ConsoleIo $io = null)
    {
        if (!$io) {
            $out = new ConsoleOutput;
            $err = new ConsoleOutput;
            $io = new ConsoleIo($out, $err, new ConsoleInput([]));
        }
        $this->io = $io;
        $this->workerEntity = $workerEntity;
    }

    /**
     * Envoi un message sur beanstalk
     * @param string $tube
     * @param mixed  $data
     * @param int    $priority
     * @param int    $delay
     * @param int    $ttr
     * @return int job id
     * @noinspection RedundantSuppression
     */
    public function emit(
        string $tube,
        $data,
        $priority = PheanstalkInterface::DEFAULT_PRIORITY,
        $delay = PheanstalkInterface::DEFAULT_DELAY,
        $ttr = PheanstalkInterface::DEFAULT_TTR
    ) {
        /** @noinspection PhpUndefinedMethodInspection v4 */
        return Beanstalk::getInstance($tube)->emit($data, $priority, $delay, $ttr);
    }

    /**
     * Ajoute des informations sur un message
     * @param string $message
     * @return string
     */
    protected function messageFormatter(string $message)
    {
        return sprintf(
            '%s %s:%d (id=%d) - %s',
            date('Y-m-d H:i:s'),
            $this->workerEntity->get('hostname'),
            $this->workerEntity->get('pid'),
            $this->workerEntity->id,
            $message
        );
    }

    /**
     * Message classique sur sortie STDOUT
     * @param string $message
     * @return void
     */
    public function out(string $message)
    {
        $this->io->out($this->messageFormatter($message));
    }

    /**
     * Erreur classique sur sortie STDERR
     * @param string $message
     * @return void
     */
    public function error(string $message)
    {
        $this->io->error($this->messageFormatter($message));
    }

    /**
     * Lancé avant work()
     * @param mixed $data
     * @throws Exception
     */
    public function beforeWork($data)
    {
        if (Config::readAll('Interruption.enable_workers') === false
            && MaintenanceMiddleware::isInterrupted()
        ) {
            $this->out("Work interrupted");
            throw new StopException("Maintenance", 0);
        }
        $data = ((array)$data) + [
            'jobid' => $this->workerEntity->id,
            'tube' => $this->workerEntity->get('tube'),
        ];
        $this->websocketMessage('work_begin', $data);
        if (!empty($data['user_id'])) {
            $this->userId = $data['user_id'];
        }
        $this->data = $data;
    }

    /**
     * Lancé après work()
     * @param mixed $data
     * @throws Exception
     */
    public function afterWork($data)
    {
        /** @var Connection $conn */
        $conn = ConnectionManager::get('default');
        if ($conn->inTransaction()) {
            $conn->rollback();
            throw new Exception(__("Une transaction est restée ouverte à la fin du job"));
        }
        $data = ((array)$data) + [
            'jobid' => $this->workerEntity->id,
            'tube' => $this->workerEntity->get('tube'),
        ];
        $this->websocketMessage('work_end', $data);
        gc_collect_cycles();
    }

    /**
     * Permet d'envoyer un message sur websocket
     * @param string $chanel
     * @param array  $data
     * @throws ZMQSocketException
     */
    protected function websocketMessage(string $chanel, $data = [])
    {
        if ($this->userId) {
            $data = $data ?: $this->data;
            $message = is_array($data) && !empty($this->params['tube'])
                ? $data  + ['tube' => $this->params['tube']]
                : $data
            ;
            Notify::emit($chanel, $message);
        }
    }
}
