<?php
/**
 * AsalaeCore\Worker\FileConvertorWorker
 */

namespace AsalaeCore\Worker;

use AsalaeCore\Model\Entity\Fileupload;
use AsalaeCore\Model\Table\FileuploadsTable;
use AsalaeCore\Utility\Notify;
use Beanstalk\Exception\CantWorkException;
use Cake\Console\ConsoleIo;
use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;
use Exception;
use FileConverters\Utility\FileConverters;
use Libriciel\Filesystem\Utility\Filesystem;
use ZipStream\Exception\FileNotReadableException;
use ZMQSocketException;

/**
 * Permet de convertir un fichier uploadé
 *
 * @category Worker
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class FileConvertorWorker extends AbstractAsalaeWorkerV4 implements TestableWorkerInterface
{
    /**
     * @var array Prérequis du worker ['monprerequis' => true, ...]
     */
    public $prerequisites = [
        [
            'name' => 'handbrake-cli',
            'command' => 'HandBrakeCLI --help',
            'missing' => null,
        ], [
            'name' => 'ffmpeg',
            'command' => 'ffmpeg --help',
            'missing' => null,
        ], [
            'name' => 'libreoffice-common',
            'command' => 'libreoffice --help',
            'missing' => null,
        ], [
            'name' => 'unoconv',
            'command' => 'unoconv --help',
            'missing' => null,
        ], [
            'name' => 'xlsx2csv',
            'command' => 'xlsx2csv --help',
            'missing' => null,
        ], [
            'name' => 'imagemagick',
            'command' => 'convert -version',
            'missing' => null,
        ], [
            'name' => 'soundconverter',
            'command' => 'soundconverter --help',
            'missing' => null,
        ], [
            'name' => 'libavcodec-extra',
            'command' => 'dpkg -s libavcodec-extra',
            'missing' => null,
        ],
    ];

    /**
     * @var FileuploadsTable Fileuploads
     */
    private $Fileuploads;

    /**
     * FileConvertorWorker constructor.
     * @param EntityInterface $workerEntity
     * @param ConsoleIo       $io
     * @noinspection PhpDocSignatureInspection v4
     */
    public function __construct(EntityInterface $workerEntity, ConsoleIo $io = null)
    {
        parent::__construct($workerEntity, $io);
        $this->Fileuploads = TableRegistry::getTableLocator()->get('Fileuploads');
    }

    /**
     * Effectue le travail voulu
     *
     * @param mixed $data
     * @throws CantWorkException
     * @throws ZMQSocketException
     * @throws FileNotReadableException
     */
    public function work($data)
    {
        /** @var Fileupload $entity */
        $entity = $this->Fileuploads->find()
            ->where(['id' => $data['id']])
            ->first();

        if (!$entity) {
            throw $this->cantWork(__("Le fichier demandé n'existe pas"));
        }
        if (!$this->Fileuploads->can($entity, 'convert')) {
            throw $this->cantWork(
                __(
                    "Impossible d'effectuer la conversion sur le fichier {0}, état actuel: {1}",
                    $data['name'],
                    $entity->state
                )
            );
        }

        $this->io->out(__("Début de la conversion du fichier {0} vers {1}", $entity->name, $data['name']));

        $exist = $this->Fileuploads->find()
            ->where(['user_id' => $data['user_id'], 'name' => $data['name']])
            ->first();

        if ($exist) {
            throw $this->cantWork(
                __("Impossible d'éffectuer la conversion, Un fichier porte déjà le nom de <b>{0}</b>", $data['name'])
            );
        }

        $dir = TMP.'upload'.DS.'user_'.$data['user_id'];
        $newPath = $dir.DS.basename($entity->path).$data['to'];
        if (!is_dir($dir)) {
            Filesystem::mkdir($dir);
        }

        $this->io->out(
            __("Fonction : ")
            . "\FileConverters\Utility\FileConverters::convert("
            . "'{$entity->get('path')}', "
            . "'{$data['to']}', "
            . "'$newPath');\n"
        );
        $success = FileConverters::convert($entity->path, $data['to'], $newPath);
        $last = end(FileConverters::$logs);
        $this->io->out(__("Commande utilisée : ").PHP_EOL.$last['cmd']);

        $this->Fileuploads = TableRegistry::getTableLocator()->get('Fileuploads');

        echo $success ? __("Conversion réussie").PHP_EOL : __("Conversion échouée").PHP_EOL;

        if ($success
            && $this->Fileuploads instanceof FileuploadsTable
        ) {
            $newEntity = $this->Fileuploads->newUploadedEntity($data['name'], $newPath, (int)$data['user_id']);
            $options = ($data['silent'] ?? false) ? ['silent' => true] : [];
            $this->Fileuploads->save($newEntity, $options);
            $this->emit(
                'state',
                [
                    'table' => 'Fileuploads',
                    'transition' => 'convert'
                ]
            );
            $this->Fileuploads->save($entity);
            fclose($newEntity->get('content'));
            echo __("Conversion effectuée, envoi d'une notification à {0}", "user_".$data['user_id']).PHP_EOL;
            $message = __("Le fichier <b>{0}</b> a été créé avec succès", h($data['name']));
            Notify::send(
                $this->userId,
                [],
                sprintf('<h4>%s</h4>%s', __("<h4>Worker conversions</h4>"), $message),
                'alert-success'
            );
        } else {
            debug($last);
            echo $success
                ? __("Erreur lors de l'enregistrement du fichier {0} en base", $data['name']).PHP_EOL
                : __("Conversion fail, envoi d'une notification à {0}", "user_".$data['user_id']).PHP_EOL;

            $message = __("La conversion du fichier <b>{0}</b> a échoué", h($data['name']));
            Notify::send(
                $this->userId,
                [],
                sprintf('<h4>%s</h4>%s', __("<h4>Worker conversions</h4>"), $message),
                'alert-danger'
            );
        }
    }

    /**
     * @var string nom du fichier temporaire de test du worker
     */
    const TEST_FILENAME = 'dummy-test-file-convertor-worker';

    /**
     * Emission d'un message de test
     * A utiliser avant getTestResults()
     * @param array $params optionnel, si le test à besoin de paramètres
     * @throws Exception
     */
    public function emitTest(array $params = [])
    {
        $tmpDir = sys_get_temp_dir() . DS . 'emittest-destination';
        Filesystem::begin('emitTest');
        Filesystem::dumpFile($tmpDir . DS . self::TEST_FILENAME, 'this is a test file');
        $this->Fileuploads->deleteAll(['name LIKE' => self::TEST_FILENAME.'%']);
        $entity = $this->Fileuploads->newUploadedEntity(
            self::TEST_FILENAME,
            $tmpDir . DS . self::TEST_FILENAME,
            $params['user_id']
        );
        $this->Fileuploads->save($entity, ['silent' => true]);
        fclose($entity->get('content'));
        $data = [
            'id' => $entity->get('id'),
            'user_id' => $params['user_id'],
            'name' => $entity->get('name').'.pdf',
            'to' => 'pdf',
            'silent' => true,
        ];
        $this->emit($this->params['tube'], $data, 1);
    }

    /**
     * Permet d'obtenir les résultats du test
     * @return array ['success' => (bool), 'errors' => (array), 'message' => (string)]
     * @throws Exception
     */
    public function getTestResults(): array
    {
        $entity = $this->Fileuploads->find()
            ->where(['name' => self::TEST_FILENAME.'.pdf'])
            ->contain(['Siegfrieds'])
            ->first();
        $success = false;
        $errors = [];
        if (!$entity) {
            $errors[] = __("Le worker n'a pas converti le fichier");
        } else {
            Filesystem::addToDeleteIfRollback(self::TEST_FILENAME.'.pdf');
            $success = true;
        }
        Filesystem::rollback('emitTest');
        $this->Fileuploads->deleteAll(['name LIKE' => self::TEST_FILENAME.'%']);
        return [
            'success' => $success,
            'error' => implode(', ', $errors),
            'entity' => $entity ? $entity->toArray() : null
        ];
    }

    /**
     * Envoi un message avec notification
     * @param string $message
     * @return CantWorkException
     * @throws ZMQSocketException
     */
    protected function cantWork(string $message)
    {
        Notify::send(
            $this->userId,
            [],
            sprintf('<h4>%s</h4>%s', __("Worker de conversion"), $message),
            'alert-danger'
        );
        $this->io->out(__("notification envoyée à {0}: {1}", 'user_'.$this->userId, $message));
        return new CantWorkException($message);
    }
}
