<?php
/**
 * AsalaeCore\Worker\StateWorker
 */

namespace AsalaeCore\Worker;

use AsalaeCore\Model\Entity\Fileupload;
use AsalaeCore\Utility\Notify;
use Beanstalk\Exception\CantWorkException;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use StateMachine\Model\Behavior\StateMachineBehavior;
use ZMQSocketException;

/**
 * Permet de calculer l'état d'une entité
 *
 * @category Worker
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class StateWorker extends AbstractAsalaeWorkerV4
{
    /**
     * Effectue le travail voulu
     *
     * @param mixed $data
     * @throws CantWorkException
     * @throws ZMQSocketException
     */
    public function work($data)
    {
        /** @var Table|StateMachineBehavior $Table */
        $Table = TableRegistry::getTableLocator()->get($data['table']);
        /** @var Fileupload $entity */
        $entity = $Table->find()
            ->where(['id' => $data['id']])
            ->first();

        if (!$entity) {
            throw $this->cantWork(
                __(
                    "L'enregistrement pour la table {0} n'a pas été trouvé",
                    $Table->getAlias()
                )
            );
        }
        if (!$Table->can($entity, $data['transition'])) {
            throw $this->cantWork(
                __(
                    "Impossible d'effectuer le changement d'état sur l'enregistrement"
                    ." n°{0} de la table {1}, état actuel: {2}",
                    $data['id'],
                    $Table->getAlias(),
                    $entity->state
                )
            );
        }

        $Table->transition($entity, $data['transition']);
        $Table->save($entity);
    }

    /**
     * Envoi un message avec notification
     * @param string $message
     * @return CantWorkException
     * @throws ZMQSocketException
     */
    protected function cantWork(string $message)
    {
        Notify::send(
            $this->userId,
            [],
            sprintf('<h4>%s</h4>%s', __("Worker de changement d'états"), $message),
            'alert-danger'
        );
        $this->io->out(__("notification envoyée à {0}: {1}", 'user_'.$this->userId, $message));
        return new CantWorkException($message);
    }
}
