<?php
/**
 * AsalaeCore\Worker\HashWorker
 */

namespace AsalaeCore\Worker;

use AsalaeCore\Model\Entity\Fileupload;
use AsalaeCore\Model\Table\FileuploadsTable;
use Beanstalk\Exception\CantWorkException;
use Cake\Console\ConsoleIo;
use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;

/**
 * Permet de calculer le hash d'un fichier uploadé
 *
 * @category Worker
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class HashWorker extends AbstractAsalaeWorkerV4 implements TestableWorkerInterface
{

    /**
     * @var FileuploadsTable Fileuploads
     */
    private $Fileuploads;

    /**
     * FileConvertorWorker constructor.
     * @param EntityInterface $workerEntity
     * @param ConsoleIo       $io
     * @noinspection PhpDocSignatureInspection v4
     */
    public function __construct(EntityInterface $workerEntity, ConsoleIo $io = null)
    {
        parent::__construct($workerEntity, $io);
        $this->Fileuploads = TableRegistry::getTableLocator()->get('Fileuploads');
    }

    /**
     * Effectue le travail voulu
     *
     * @param mixed $data
     * @throws \Beanstalk\Exception\CantWorkException
     */
    public function work($data)
    {
        /** @var Fileupload $entity */
        $entity = $this->Fileuploads->find()
            ->where(['Fileuploads.id' => $data['id']])
            ->first();

        if (!$entity) {
            $message = __("Le fichier {0} n'a pas été trouvé.", $data['name']);
            throw new CantWorkException($message);
        }
        if (!$this->Fileuploads->can($entity, 'hash')) {
            $message = __(
                "Impossible d'effectuer le hash sur le fichier {0}, état actuel: {1}",
                $data['name'],
                $entity->state
            );
            throw new CantWorkException($message);
        }

        $entity->set('hash', hash_file($entity->hash_algo, $entity->path));
        $this->Fileuploads->save($entity);
        $this->io->out(__('hash calculé avec {0}: {1}', $entity->hash_algo, $entity->hash));
    }

    /**
     * @var string nom du fichier temporaire de test du worker
     */
    const TEST_FILENAME = 'dummy-test-hash-worker';

    /**
     * Emission d'un message de test
     * A utiliser avant getTestResults()
     * @param array $params optionnel, si le test à besoin de paramètres
     * @throws Exception
     */
    public function emitTest(array $params = [])
    {
        $tmpDir = TMP . 'emittest-destination';
        Filesystem::begin('emitTest');
        Filesystem::dumpFile($tmpDir . DS . self::TEST_FILENAME, 'this is a test file');
        $this->Fileuploads->deleteAll(['name' => self::TEST_FILENAME]);
        $entity = $this->Fileuploads->newUploadedEntity(
            self::TEST_FILENAME,
            $tmpDir . DS . self::TEST_FILENAME,
            $params['user_id']
        );
        $this->Fileuploads->save($entity, ['silent' => true]);
        fclose($entity->get('content'));
        $data = [
            'id' => $entity->get('id'),
            'name' => $entity->get('name'),
            'hash_algo' => $entity->get('hash_algo'),
            'path' => $entity->get('path'),
            'user_id' => $params['user_id'],
            'silent' => true,
        ];
        $this->emit($this->params['tube'], $data, 1);
    }

    /**
     * Permet d'obtenir les résultats du test
     * @return array ['success' => (bool), 'errors' => (array), 'message' => (string)]
     * @throws Exception
     */
    public function getTestResults(): array
    {
        $entity = $this->Fileuploads->find()
            ->where(['name' => self::TEST_FILENAME])
            ->first();
        $success = false;
        $errors = [];
        if ($entity && empty($entity->get('hash'))) {
            $errors[] = __("Le worker n'a pas calculé le hash");
        } elseif ($entity) {
            $hash = hash_file($entity->get('hash_algo'), $entity->get('path'));
            $success = !empty($hash) && $hash === $entity->get('hash');
            if (!$success) {
                $errors[] = __(
                    "Le hash du fichier ({0}) est différent de celui trouvé par le worker ({1})",
                    $hash,
                    $entity->get('hash')
                );
            }
        } else {
            $errors[] = __("Le fichier de test n'a pas été trouvé, le worker est peut-être actuellement occupé...");
        }
        Filesystem::rollback('emitTest');
        $this->Fileuploads->deleteAll(['name' => self::TEST_FILENAME]);

        return [
            'success' => $success,
            'tube' => $this->params['tube'],
            'error' => implode(', ', $errors)
        ];
    }
}
