<?php
/**
 * AsalaeCore\Worker\FileValidationWorker
 */

namespace AsalaeCore\Worker;

use AsalaeCore\Model\Entity\Fileupload;
use AsalaeCore\Model\Table\FileuploadsTable;
use AsalaeCore\Utility\Notify;
use Beanstalk\Exception\CantWorkException;
use Cake\Console\ConsoleIo;
use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;
use Exception;
use FileValidator\Utility\FileValidator;
use Libriciel\Filesystem\Utility\Filesystem;
use ZMQSocketException;

/**
 * Permet de valider un fichier uploadé
 *
 * @category Worker
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class FileValidationWorker extends AbstractAsalaeWorkerV4 implements TestableWorkerInterface
{
    /**
     * @var array Prérequis du worker ['monprerequis' => true, ...]
     */
    public $prerequisites = [
        [
            'name' => 'siegfried',
            'command' => 'sf -version',
            'missing' => null,
        ], [
            'name' => 'imagemagick',
            'command' => 'identify -version',
            'missing' => null,
        ], [
            'name' => 'mediainfo',
            'command' => 'mediainfo --help; [ $? -eq 0 -o $? -eq 255 ]',
            'missing' => null,
        ], [
            'name' => 'poppler-utils',
            'command' => 'pdfinfo --help',
            'missing' => null,
        ], [
            'name' => 'catdoc',
            'command' => 'catdoc',
            'missing' => null,
        ], [
            'name' => 'zip',
            'command' => 'zip --help',
            'missing' => null,
        ], [
            'name' => 'gzip',
            'command' => 'gzip --help',
            'missing' => null,
        ], [
            'name' => 'bzip2',
            'command' => 'bzip2 --help',
            'missing' => null,
        ], [
            'name' => 'gzip',
            'command' => 'gzip --help',
            'missing' => null,
        ], [
            'name' => 'webp',
            'command' => 'dwebp -version',
            'missing' => null,
        ], [
            'name' => 'libxml2-utils',
            'command' => 'xmllint -version',
            'missing' => null,
        ], [
            'name' => 'xlsx2csv',
            'command' => 'xlsx2csv --help',
            'missing' => null,
        ],
    ];

    /**
     * @var FileuploadsTable Fileuploads
     */
    private $Fileuploads;

    /**
     * FileConvertorWorker constructor.
     * @param EntityInterface $workerEntity
     * @param ConsoleIo       $io
     * @noinspection PhpDocSignatureInspection v4
     */
    public function __construct(EntityInterface $workerEntity, ConsoleIo $io = null)
    {
        parent::__construct($workerEntity, $io);
        $this->Fileuploads = TableRegistry::getTableLocator()->get('Fileuploads');
    }

    /**
     * @var string nom du fichier temporaire de test du worker
     */
    const TEST_FILENAME = 'dummy-test-file-validation-worker';

    /**
     * Effectue le travail voulu
     *
     * @param mixed $data
     * @throws Exception
     */
    public function work($data)
    {
        /** @var Fileupload $entity */
        $entity = $this->Fileuploads->find()
            ->where(['id' => $data['id']])
            ->first();

        if (!$entity) {
            $message = __("Le fichier {0} n'a pas été trouvé.", $data['name']);
            throw $this->cantWork($message);
        }
        if (!$this->Fileuploads->can($entity, 'validate')) {
            $message = __(
                "Impossible d'effectuer la validation sur le fichier {0}, état actuel: {1}",
                $data['name'],
                $entity->state
            );
            throw $this->cantWork($message);
        }

        $isValid = FileValidator::check($data['path']);
        $this->io->out(
            __(
                "Le test de validation du fichier {0} a été effectué. son résultat est : {1}",
                $data['name'],
                ($isValid ? 'valide': 'invalide')
            )
        );

        if ($isValid) {
            $this->emit(
                'state',
                [
                    'table' => 'Fileuploads',
                    'transition' => 'validate'
                ]
            );
            $message = __("La validation du fichier <b>{0}</b> a été accompli avec succès", $data['name']);
            $class = "alert-success";
        } else {
            $this->emit(
                'state',
                [
                    'table' => 'Fileuploads',
                    'transition' => 'invalidate'
                ]
            );
            $message = __("Le fichier <b>{0}</b> a échoué au test de validation", $data['name']);
            $class = "alert-danger";
        }

        $entity->set('valid', $isValid);
        $this->Fileuploads->save($entity);

        Notify::send(
            $this->userId,
            [],
            sprintf('<h4>%s</h4>%s', __("<h4>Worker validation</h4>"), h($message)),
            $class
        );
    }

    /**
     * Emission d'un message de test
     * A utiliser avant getTestResults()
     * @param array $params optionnel, si le test à besoin de paramètres
     * @throws Exception
     */
    public function emitTest(array $params = [])
    {
        $tmpDir = sys_get_temp_dir() . DS . 'emittest-destination';
        Filesystem::begin('emitTest');
        Filesystem::dumpFile($tmpDir . DS . self::TEST_FILENAME, 'this is a test file');
        $this->Fileuploads->deleteAll(['name' => self::TEST_FILENAME]);
        $entity = $this->Fileuploads->newUploadedEntity(
            self::TEST_FILENAME,
            $tmpDir . DS . self::TEST_FILENAME,
            $params['user_id']
        );
        $this->Fileuploads->save($entity, ['silent' => true]);
        fclose($entity->get('content'));
        $data = [
            'id' => $entity->get('id'),
            'user_id' => $params['user_id'],
            'name' => $entity->get('name'),
            'path' => $entity->get('path'),
            'silent' => true,
        ];
        $this->emit($this->params['tube'], $data, 1);
    }

    /**
     * Permet d'obtenir les résultats du test
     * @return array ['success' => (bool), 'errors' => (array), 'message' => (string)]
     * @throws Exception
     */
    public function getTestResults(): array
    {
        $entity = $this->Fileuploads->find()
            ->where(['name' => self::TEST_FILENAME])
            ->first();
        $success = false;
        $errors = [];
        if (!$entity) {
            $errors[] = __("Le worker n'a pas créé le fichier");
        } elseif ($entity->get('valid')) {
            $success = true;
        } else {
            $errors[] = __("Le fichier est déclaré invalide à tort");
        }
        Filesystem::rollback('emitTest');
        $this->Fileuploads->delete($entity);
        return [
            'success' => $success,
            'error' => implode(', ', $errors),
            'entity' => $entity ? $entity->toArray() : null
        ];
    }

    /**
     * Envoi un message avec notification
     * @param string $message
     * @return CantWorkException
     * @throws ZMQSocketException
     */
    protected function cantWork(string $message)
    {
        Notify::send(
            $this->userId,
            [],
            sprintf('<h4>%s</h4>%s', __("Worker de validation"), $message),
            'alert-danger'
        );
        $this->io->out(__("notification envoyée à {0}: {1}", 'user_'.$this->userId, $message));
        return new CantWorkException($message);
    }
}
