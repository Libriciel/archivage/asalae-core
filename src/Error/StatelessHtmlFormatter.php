<?php
/**
 * AsalaeCore\Error\StatelessHtmlFormatter
 * @noinspection PhpInternalEntityUsedInspection - HtmlFormatter = @internal
 */

namespace AsalaeCore\Error;

use Cake\Error\Debug\FormatterInterface;
use Cake\Error\Debug\HtmlFormatter;
use Cake\Error\Debug\NodeInterface;

/**
 * Affichage sans header du debug pour utilisation en dehors de la fonction debug()
 *
 * @category    Error
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2021, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class StatelessHtmlFormatter extends HtmlFormatter implements FormatterInterface
{
    /**
     * Convert a tree of NodeInterface objects into HTML
     *
     * @param \Cake\Error\Debug\NodeInterface $node The node tree to dump.
     * @return string
     */
    public function dump(NodeInterface $node): string
    {
        $html = $this->export($node, 0);
        return '<div class="cake-dbg">' . $html . '</div>';
    }
}
