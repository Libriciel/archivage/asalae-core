<?php
/**
 * AsalaeCore\Error\AppConsoleErrorHandler
 */

namespace AsalaeCore\Error;

use AsalaeCore\Console\ConsoleOutput;
use Cake\Error\ConsoleErrorHandler;

/**
 * Gestions d'erreur à l'application (cli)
 *
 * @category    Error
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2019, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class AppConsoleErrorHandler extends ConsoleErrorHandler
{
    /**
     * Constructor
     *
     * @param array $options Options for the error handler.
     */
    public function __construct($options = [])
    {
        if (empty($options['stderr'])) {
            $options['stderr'] = new ConsoleOutput('php://stderr');
        }
        parent::__construct($options);
    }

    /**
     * Prints an error to stderr.
     *
     * Template method of BaseErrorHandler.
     *
     * @param array $error An array of error data.
     * @param bool  $debug Whether or not the app is in debug mode.
     * @return void
     */
    protected function _displayError(array $error, bool $debug): void
    {
        if (AppErrorHandler::isErrorDisabled()) {
            AppErrorHandler::addError($error);
            return;
        }
        parent::_displayError($error, $debug);
    }
}
