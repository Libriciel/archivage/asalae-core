<?php
/**
 * AsalaeCore\Error\ExceptionTrap
 */

namespace AsalaeCore\Error;

use AsalaeCore\Error\Renderer\ConsoleExceptionRenderer;
use Cake\Console\CommandInterface;
use Cake\Console\Exception\ConsoleException;
use Cake\Error\ExceptionRendererInterface;
use Cake\Error\ExceptionTrap as CakeExceptionTrap;
use Cake\Http\ServerRequest;
use Cake\Log\Log;
use Cake\Routing\Router;
use Exception;
use Psr\Http\Message\ServerRequestInterface;
use Throwable;

/**
 * Gestions d'exceptions de l'application
 *
 * @category    Error
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2022, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ExceptionTrap extends CakeExceptionTrap
{
    /**
     * @var ServerRequest|null
     */
    private $request;

    /**
     * Get an instance of the renderer.
     *
     * @param Throwable                   $exception Exception to render
     * @param ServerRequestInterface|null $request   The request if possible.
     * @return ExceptionRendererInterface
     */
    public function renderer(Throwable $exception, $request = null)
    {
        $this->request = $request ?? Router::getRequest();
        return parent::renderer($exception, $request);
    }

    /**
     * Choose an exception renderer based on config or the SAPI
     *
     * @return class-string<ConsoleExceptionRenderer|AppExceptionRenderer>
     */
    protected function chooseRenderer(): string
    {
        $config = $this->getConfig('exceptionRenderer');
        if ($config !== null) {
            return $config;
        }

        /** @var class-string<ConsoleExceptionRenderer|AppExceptionRenderer> */
        return PHP_SAPI === 'cli' && empty($this->request)
            ? ConsoleExceptionRenderer::class
            : AppExceptionRenderer::class;
    }

    /**
     * On retire stdout et stderr des logs pour éviter d'avoir un doublon dans
     * les sorties, l'entrée est bien inscrite dans error.log mais l'affichage
     * est géré par ExceptionRenderer
     *
     * @param Throwable                   $exception The exception to log
     * @param ServerRequestInterface|null $request   The optional request
     * @return void
     */
    public function logException(Throwable $exception, ?ServerRequestInterface $request = null): void
    {
        $stdout = Log::getConfig('stdout');
        $stderr = Log::getConfig('stderr');
        Log::drop('stdout');
        Log::drop('stderr');
        parent::logException($exception, $request);
        if (PHP_SAPI === 'cli' && $stdout && $stderr) {
            Log::setConfig('stdout', $stdout);
            Log::setConfig('stderr', $stderr);
        }
    }

    /**
     * Handle uncaught exceptions.
     *
     * Uses a template method provided by subclasses to display errors in an
     * environment appropriate way.
     *
     * @param \Throwable $exception Exception instance.
     * @return void
     * @throws Exception When renderer class not found
     * @see https://secure.php.net/manual/en/function.set-exception-handler.php
     */
    public function handleException(Throwable $exception): void
    {
        parent::handleException($exception);
        if (PHP_SAPI === 'cli' && empty($this->request)) {
            $exitCode = CommandInterface::CODE_ERROR;
            if ($exception instanceof ConsoleException) {
                $exitCode = $exception->getCode();
            }
            exit($exitCode);
        }
    }
}
