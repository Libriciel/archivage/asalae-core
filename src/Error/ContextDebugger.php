<?php
/**
 * AsalaeCore\Error\ContextDebugger
 */

namespace AsalaeCore\Error;

use Cake\Core\Configure;
use Cake\Error\Debugger;
use Cake\Routing\Router;
use Cake\Utility\Hash;
use Cake\Utility\Inflector;
use DateTime;
use Exception;
use Laminas\Diactoros\CallbackStream;
use Laminas\Diactoros\UploadedFile;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\StreamInterface;
use SplFileObject;
use Symfony\Component\VarDumper\Cloner\VarCloner;
use Symfony\Component\VarDumper\Dumper\HtmlDumper;

/**
 * Amélioration du debugger de CakePHP afin d'afficher en plus, le contexte.
 *
 * @category Debugger
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ContextDebugger
{
    /**
     * @var array Structure d'affichage du debug :
     *            container - lineInfoContainer, context, degug
     *            lineInfo - fileShort, traceLine
     *            lineInfoContainer - lineInfo, uid
     *            context - uid, lineContent
     *            debug - vars
     *            var - key, value
     */
    public static $templates = [
        'html' => [
            'container' => '<div class="cake-debug-output">%s%s%s</div>',
            'lineInfo' => '<span><strong>%s</strong> (line <strong>%s</strong>)</span>',
            'lineInfoContainer' => '%s<br>'
                .'<a href="javascript:(function(){'
                .'var a=document.getElementById(\'%s\').style;a.display = a.display==\'none\'?\'block\':\'none\'})()"'
                .' role="button">Context</a>',
            'context' => '<pre class="cake-debug" style="display:none" id="%s">%s</pre>',
            'debug' => '<pre class="cake-debug">%s</pre>',
            'var' => '<b style="background:lightgray;display:block;margin:15px 0 5px;border-bottom:2px solid gray">'
                .'%s</b>%s',
            'separator_begin' => "<b>",
            'separator_end' => "</b>",
        ],
        'cli' => [
            'container' => "\n%s\n########## DEBUG ##########%s\n%s\n###########################\n",
            'lineInfo' => "%s (line %s)",
            'lineInfoContainer' => "%s",
            'context' => "\n%2\$s",
            'debug' => "%s",
            'var' => "\n========== INPUT ==========\n%s\n========== OUTPUT =========\n%s\n___________________________",
            'separator_begin' => "---------------------------------------------------\n",
            'separator_end' => "---------------------------------------------------\n",
        ],
    ];

    /**
     * @var array Liste des methodes à ignorer dans le debug_backtrace
     */
    private static $debugFunctions = ['AsalaeCore\Error\ContextDebugger::debug'];

    /**
     * @var string Chemin vers la requête loggée
     */
    private static $loggedRequest;

    /**
     * Ajoute une methode à la liste des méthodes
     * de debug pour être ignoré par le debug_backtrace
     * @param string $method obtenu par la constante magique __METHOD__
     *                       ex: AsalaeCore\Error\MyDebugger::debug
     */
    public static function registerDebugFunction(string $method)
    {
        if (!in_array($method, self::$debugFunctions)) {
            self::$debugFunctions[] = $method;
        }
    }

    /**
     * Fonction de debug, permet de chainer les arguments
     * ex: d('foo', 'bar') affichera 'foo' et 'bar'
     * Ajoute quelques lignes du contexte (code)
     *
     * @param mixed ...$args
     * @return mixed
     * @see debug()
     */
    public static function debug(...$args)
    {
        $template = PHP_SAPI === 'cli' || PHP_SAPI === 'phpdbg' ? 'cli' : 'html';
        [$file, $traceLine, $caller] = self::getFileTrace();

        $spl = new SplFileObject($file);
        $linesToParse = 10; // incrementation dans self::readFile
        do {
            $extracted = self::readFile($spl, $linesToParse, $traceLine, $template);
            $lineContent = $extracted['lineContent'];
            $fullContent = $extracted['fullContent'];
            $lines = $extracted['lines'];

            $allMatches = self::findMatchingArgs($fullContent, $lines, $traceLine, $caller);
            $parsedArgs = self::parseArgs($allMatches, count($args));
        } while (count($args) > count($parsedArgs) && $linesToParse < 100);
        foreach ($args as $k => $value) {
            $args[$parsedArgs[$k]] = $value;
            unset($args[$k]);
        }

        self::renderDebug($args, $file, $traceLine, $lineContent, $template);

        return current($args);
    }

    /**
     * Permet d'obtenir le contexte à partir d'un nom de fichier et du numéro de
     * ligne
     * @param string $filename
     * @param int    $lineNumber
     * @param int    $linesAround
     * @return string
     */
    public static function getContext(string $filename, int $lineNumber, int $linesAround = 5): string
    {
        $spl = new SplFileObject($filename);

        $lineContent = '';
        for ($line = $lineNumber - $linesAround; $line <= $lineNumber + $linesAround; $line++) {
            if (!$spl->eof() && $line > 0) {
                $spl->seek($line -1);
                $current = $spl->current();
                $content = sprintf("%0".strlen($line)."d : %s", ($line), h($current));
                $lineContent .= $line === $lineNumber ? '<b>'.$content.'</b>' : $content;
            }
        }
        return $lineContent;
    }

    /**
     * Transforme le contenu d'une variable en HTML
     * @param mixed  $value
     * @param string $template
     * @return null|string
     */
    public static function exportVar($value, $template = 'html')
    {
        if ($template === 'html'
            && method_exists('Symfony\Component\VarDumper\VarDumper', 'dump')
        ) {
            $cloner = new VarCloner();
            $dumper = new HtmlDumper();
            return $dumper->dump($cloner->cloneVar($value), true);
        } elseif ($template === 'cli') {
            return htmlspecialchars_decode(Debugger::exportVar($value, 25));
        } else {
            return h(Debugger::exportVar($value, 25));
        }
    }

    /**
     * Utilise les templates afin de générer le code html affiché
     * @param array  $args
     * @param string $file
     * @param string $traceLine
     * @param string $lineContent
     * @param string $template    html ou cli
     */
    private static function renderDebug(
        array $args,
        string $file,
        string $traceLine,
        string $lineContent,
        string $template = 'html'
    ) {
        $uid = uniqid();
        $fileShort = str_replace([ROOT, CAKE_CORE_INCLUDE_PATH], '', $file);
        $vars = '';
        foreach ($args as $key => $value) {
            if ($template === 'html') {
                $key = h($key);
            }
            $vars .= sprintf(self::$templates[$template]['var'], $key, self::exportVar($value, $template));
        }

        $lineInfo = sprintf(self::$templates[$template]['lineInfo'], $fileShort, $traceLine);
        $lineInfoContainer = sprintf(self::$templates[$template]['lineInfoContainer'], $lineInfo, $uid);
        $context = sprintf(self::$templates[$template]['context'], $uid, trim($lineContent));
        $debug = sprintf(self::$templates[$template]['debug'], $vars);
        printf(self::$templates[$template]['container'], $lineInfoContainer, $context, $debug);
    }

    /**
     * Permet d'obtenir l'emplacement de l'appel du debug
     * @return array ['/path/to/file', '123']
     */
    private static function getFileTrace(): array
    {
        $traces = Debugger::trace(['start' => 2, 'depth' => 5, 'format' => 'array']);
        krsort($traces);
        foreach ($traces as $trace) {
            $function = $trace['function'];
            $class = $trace['class'] ?? '';
            $type = $trace['type'] ?? '';
            if (in_array($class . $type . $function, self::$debugFunctions)) {
                return [
                    $trace['file'],
                    $trace['line'],
                    $function
                ];
            }
        }
        return ['', 0];
    }

    /**
     * Permet de lire un fichier autour des lignes $traceLine
     * @param SplFileObject $spl
     * @param int           $linesToParse
     * @param int           $traceLine
     * @param string        $template     html ou cli
     * @return array
     */
    private static function readFile(
        SplFileObject $spl,
        int &$linesToParse,
        int $traceLine,
        string $template = 'html'
    ): array {
        $tmpl = self::$templates[$template];
        $linesToParse += 5;
        $lineContent = '';
        $fullContent = '';
        $lines = [];
        for ($i = 0; $i < $linesToParse; $i++) {
            if (!$spl->eof() && ($traceLine + $i -8) >= 0) {
                $spl->seek($traceLine + $i -8);
                $current = $spl->current();
                $lines[$traceLine + $i -7] = $current;
                $format = "%0".strlen($traceLine + 7)."d : %s";
                $fCurrent = $current;
                if ($template === 'html') {
                    $fCurrent = h($current);
                }
                $content = sprintf($format, ($traceLine + $i -7), $fCurrent);
                $lineContent .= $i === 7
                    ? $tmpl['separator_begin'].$content.$tmpl['separator_end']
                    : $content;
                $fullContent .= $current;
            } else {
                $lineContent .= PHP_EOL;
            }
        }
        return compact('lineContent', 'fullContent', 'lines');
    }

    /**
     * Permet de récupérer les arguments possible de l'appel du debug
     * @param string $fullContent
     * @param array  $lines
     * @param int    $traceLine
     * @param string $caller      fonction appelante (ex: debug)
     * @return array
     */
    private static function findMatchingArgs(string $fullContent, array $lines, int $traceLine, string $caller): array
    {
        $allMatches = [];
        $offset = -1;
        while (preg_match(
            '/(?:^|\s|;|{|}|\*\/)(?:[\\\\\w\$]+(?:::|->))?' . $caller . '\((.*)\)\s*;/s',
            $fullContent,
            $matches,
            PREG_OFFSET_CAPTURE,
            $offset + 1
        )
        ) {
            // Detection du numéro de la ligne du paramètre
            $possibleNumbers = [];
            foreach (explode(PHP_EOL, $matches[1][0]) as $line) {
                if (empty($line)) {
                    continue;
                }
                $search = array_filter(
                    $lines,
                    function ($value) use ($line, $traceLine) {
                        return strpos($value, $line) !== false;
                    }
                );
                $possibleNumbers[] = array_keys($search);
            }

            $count = count($possibleNumbers);
            for ($i = 0; $i < $count; $i++) {
                if (isset($possibleNumbers[$i +1])) {
                    $possibleNumbers[$i] = array_filter(
                        $possibleNumbers[$i],
                        function ($value) use ($possibleNumbers, $i) {
                            return in_array(
                                $value + 1,
                                $possibleNumbers[$i + 1]
                            );
                        }
                    );
                }
                if (isset($possibleNumbers[$i -1])) {
                    $possibleNumbers[$i] = array_filter(
                        $possibleNumbers[$i],
                        function ($value) use ($possibleNumbers, $i, $traceLine) {
                            return in_array(
                                $value - 1,
                                $possibleNumbers[$i - 1]
                            );
                        }
                    );
                }
                ksort($possibleNumbers[$i]);
            }
            ksort($possibleNumbers);
            $matchingLineNumber = current($possibleNumbers[$count -1]) - $count +1;
            $offset = $matches[1][1];
            if ($matchingLineNumber && $matchingLineNumber <= $traceLine) {
                $allMatches[$matchingLineNumber] = $matches[1][0];
            }
        }
        krsort($allMatches);
        return $allMatches;
    }

    /**
     * Extraction des informations contenu dans le code
     * @param array $allMatches
     * @param int   $targetArgCount
     * @return array
     */
    private static function parseArgs(array $allMatches, int $targetArgCount): array
    {
        $parsedArgs = [];
        foreach ($allMatches as $match) {
            $cut = self::extractArgs($match);
            $parsedArgs = self::argsStringToArray($cut);
            if (count($parsedArgs) === $targetArgCount) {
                break;
            }
        }
        return $parsedArgs;
    }

    /**
     * Extraction du code de la fonction de debug
     * ex: debug('foo', 'bar') return "'foo', 'bar'"
     * @param string $match
     * @return string
     */
    private static function extractArgs(string $match): string
    {
        $cut = '';
        foreach (token_get_all('<?php '.$match) as $token) {
            if (is_array($token) && $token[0] === T_OPEN_TAG) {
                continue;
            }
            if (is_array($token)) {
                $cut .= $token[1];
            } elseif ($token === ';') {
                if ($cut[strlen($cut)-1] === ')') {
                    $cut = substr($cut, 0, strlen($cut)-1); // <?php 'test_1'); d('test_2'); => $cut = "'test_1'"
                }
                break;
            } else {
                $cut .= $token;
            }
        }
        return $cut;
    }

    /**
     * Transforme les arguments 'foo', 'bar' en array['foo', 'bar']
     * @param string $args
     * @return array
     */
    private static function argsStringToArray(string $args): array
    {
        $parsedArgs = [];
        $parsedArg = '';
        $pCount = 1;
        foreach (token_get_all('<?php '.$args) as $token) {
            if (is_array($token) && $token[0] === T_OPEN_TAG) {
                continue;
            }
            if ($token === '(') {
                $pCount++;
                if ($pCount > 1) {
                    $parsedArg .= $token;
                }
            } elseif ($token === ')') {
                $pCount--;
                $parsedArg .= $token;
            } elseif ($token === '[') {
                $parsedArg .= $token;
                $pCount++;
            } elseif ($token === ']') {
                $parsedArg .= $token;
                $pCount--;
            } elseif ($token === ',') {
                if ($pCount === 1) {
                    $parsedArgs[] = trim($parsedArg);
                    $parsedArg = '';
                } else {
                    $parsedArg .= $token;
                }
            } elseif (is_array($token) && !empty($token[1])) {
                $parsedArg .= $token[1];
            } else {
                $parsedArg .= $token;
            }
        }
        $parsedArgs[] = trim($parsedArg);
        return $parsedArgs;
    }

    /**
     * Inscrit dans un log la requête
     * @param ServerRequestInterface $request
     * @return string filename du fichier généré
     */
    public static function logRequest(ServerRequestInterface $request): string
    {
        if (!Configure::read('log_requests.enabled')) {
            return '';
        }
        $controllers = Configure::read('log_requests.controllers');
        $params = Router::parseRequest($request);
        if ($controllers !== '*') {
            $controller = $params['controller'] ?? null;
            $action = $params['action'] ?? null;
            if (!isset($controllers[$controller])
                || ($action !== '*' && !in_array($action, $controllers[$controller]))
            ) {
                return '';
            }
        }
        $suffix = '_'.strtolower($request->getMethod());
        if (isset($params['controller']) && isset($params['action'])) {
            if ($params['action'] === 'api') {
                $params['action'] = $params['pass'][0] ?? 'api';
            }
            $suffix .= sprintf(
                '_%s_%s',
                Inflector::underscore($params['controller']),
                Inflector::underscore($params['action'])
            );
        }
        $dirname = Configure::read(
            'log_requests.directory',
            LOGS.'debug_requests'
        );
        static::$loggedRequest = $dirname.DS.time().'_'.getmypid().$suffix.'.log';
        if (!is_dir($dirname)) {
            mkdir($dirname, 0777, true);
        }
        try {
            self::writeLogRequest($request);
        } catch (Exception $e) {
        }
        chmod(static::$loggedRequest, 0600);
        return static::$loggedRequest;
    }

    /**
     * Ecrit le fichier de log de la request
     * @param ServerRequestInterface $request
     * @return void
     */
    private static function writeLogRequest(ServerRequestInterface $request)
    {
        $handle = fopen(static::$loggedRequest, 'a+');
        fwrite(
            $handle,
            sprintf(
                "%s\n%s %s\n%s\n",
                $request->getServerParams()['REMOTE_ADDR'] ?? '<ip not found>',
                $request->getMethod(),
                $request->getRequestTarget(),
                (new DateTime)->format(DATE_RFC3339_EXTENDED)
            )
        );
        $sep = '--------------------------------------';
        fwrite($handle, sprintf("%s\nHEADERS\n%s\n", $sep, $sep));
        foreach ($request->getHeaders() as $header => $values) {
            foreach ($values as $value) {
                fwrite($handle, sprintf("%s: %s\n", $header, $value));
            }
        }
        // NOTE: vide en cas de redirection -> postman
        fwrite($handle, sprintf("\n%s\nORIGINAL BODY\n%s\n", $sep, $sep));
        $body = $request->getBody();
        $body->rewind();
        while (!$body->eof()) {
            fwrite($handle, $body->read(4096));
        }
        fwrite($handle, sprintf("\n%s\nPARSED BODY\n%s\n", $sep, $sep));
        $boundary = '--------------------------'.time();
        if ($type = $request->getHeaderLine('Content-Type')) {
            if ($pos = strpos($type, 'boundary=')) {
                $boundary = substr($type, $pos + 9);
            }
        }
        $parsedBody = $request->getParsedBody();
        $bodyContent = [];
        foreach ($parsedBody as $key => $value) {
            if ($value instanceof UploadedFile) {
                try {
                    $bodyContent[$key] = $value->getStream();
                    $bodyContent[$key]->rewind();
                } catch (Exception $e) {
                    $bodyContent[$key] = $e->getMessage();
                }
            } elseif (is_array($value)) {
                foreach (Hash::flatten($value, '][') as $skey => $val) {
                    $bodyContent[$key.'['.$skey.']'] = $val;
                }
            } else {
                $bodyContent[$key] = $value;
            }
        }
        foreach ($bodyContent as $key => $value) {
            fwrite(
                $handle,
                sprintf(
                    "%s\nContent-Disposition: form-data; name=\"%s\"\n\n",
                    $boundary,
                    $key
                )
            );
            if ($value instanceof StreamInterface) {
                while (!$value->eof()) {
                    fwrite($handle, $value->read(4096));
                }
                $value->rewind();
            } elseif ($value instanceof DateTime) {
                fwrite($handle, $value->format(DATE_RFC3339));
            } else {
                fwrite($handle, (string)$value);
            }
            fwrite($handle, "\n");
        }
        fclose($handle);
    }

    /**
     * Inscrit dans un log la réponse
     * @param ResponseInterface $response
     */
    public static function logResponse(ResponseInterface $response)
    {
        if (!static::$loggedRequest) {
            return;
        }
        $nName = substr(static::$loggedRequest, 0, -4).'_'.$response->getStatusCode().'.log';
        rename(static::$loggedRequest, $nName);
        static::$loggedRequest = $nName;

        $handle = fopen(static::$loggedRequest, 'a+');
        fwrite(
            $handle,
            sprintf(
                "\n======================================\nRESPONSE - %d\n%s\n",
                $response->getStatusCode(),
                (new DateTime)->format(DATE_RFC3339_EXTENDED)
            )
        );
        fwrite($handle, "======================================\n");
        $sep = '--------------------------------------';
        fwrite($handle, sprintf("%s\nHEADERS\n%s\n", $sep, $sep));
        foreach ($response->getHeaders() as $header => $values) {
            foreach ($values as $value) {
                fwrite($handle, sprintf("%s: %s\n", $header, $value));
            }
        }
        fwrite($handle, sprintf("\n%s\nBODY\n%s\n", $sep, $sep));
        $body = $response->getBody();
        if (!$body instanceof CallbackStream) {
            $body->rewind();
            while (!$body->eof()) {
                fwrite($handle, $body->read(4096));
            }
        }
        fclose($handle);
    }

    /**
     * Donne l'équivalent d'un Exception::trace() à partir d'un $filename:$line
     * @param string $filename
     * @param int    $line
     * @return array
     */
    public static function extractMethod(string $filename, int $line): array
    {
        $tokens = token_get_all(file_get_contents($filename));
        $nextIsFuncName = false;
        $nextIsNamespace = false;
        $nextIsClassname = false;
        $functionName = null;
        $last = [
            'file' => $filename,
            'line' => $line,
        ];
        $namespace = [];
        foreach ($tokens as $token) {
            if ($token === ';' && $nextIsNamespace) {
                $nextIsNamespace = false;
            }
            if (!is_array($token)) {
                continue;
            }
            if ($token[0] === T_NAMESPACE) {
                $nextIsNamespace = true;
            } elseif ($token[0] === T_CLASS) {
                $nextIsClassname = true;
            } elseif ($token[0] === T_STRING && $nextIsNamespace) {
                $namespace[] = $token[1];
            } elseif ($token[0] === T_STRING && $nextIsClassname) {
                $last['class'] = implode(
                    '\\',
                    array_merge($namespace, [$token[1]])
                );
                $nextIsClassname = false;
            } elseif ($token[0] === T_FUNCTION) {
                $nextIsFuncName = true;
            } elseif ($token[0] === T_STRING && $nextIsFuncName) {
                $functionName = $token[1];
                $nextIsFuncName = false;
            }
            if ($token[2] === $line) {
                $last['function'] = $functionName;
                break;
            }
        }
        return $last;
    }
}
