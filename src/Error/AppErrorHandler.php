<?php
/**
 * AsalaeCore\Error\AppErrorHandler
 */

namespace AsalaeCore\Error;

use AsalaeCore\Http\ResponseEmitter;
use Cake\Core\Configure;
use Cake\Error\ErrorHandler;
use Cake\Http\Response;

/**
 * Gestions d'erreur à l'application
 *
 * @category    Error
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2017, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class AppErrorHandler extends ErrorHandler
{
    /**
     * @var AppConsoleErrorHandler Error handler for Cake console.
     */
    public $ConsoleErrorHandler;

    /**
     * @var bool Affiche l'erreur si le debug > 2
     */
    private static $errorOutput = true;

    /**
     * @var array stock les erreurs si l'affichage est désactivé
     */
    private static $errors = [];

    /**
     * Constructor
     *
     * @param array $options The options for error handling.
     */
    public function __construct($options = [])
    {
        if (PHP_SAPI === 'cli') {
            $this->ConsoleErrorHandler = new AppConsoleErrorHandler($options);
        }
        parent::__construct($options);
    }

    /**
     * Appelé dans config/bootstrap.php
     * Enregistre les fonctions de callback à effectuer lors de l'arret d'un script php
     */
    public function register(): void
    {
        register_shutdown_function("\Libriciel\Filesystem\Utility\Filesystem::rollbackAll");
        register_shutdown_function([$this, 'shutdown']);
        if (PHP_SAPI === 'cli') {
            $this->ConsoleErrorHandler->register();
        } else {
            parent::register();
        }
    }

    /**
     * Display an error.
     *
     * Template method of BaseErrorHandler.
     *
     * Only when debug > 2 will a formatted error be displayed.
     *
     * @param array $error An array of error data.
     * @param bool  $debug Whether or not the app is in debug mode.
     * @return void
     */
    protected function _displayError(array $error, bool $debug): void
    {
        if (!static::$errorOutput) {
            static::$errors[] = $error;
            return;
        }
        parent::_displayError($error, $debug);
    }

    /**
     * Désactive l'affichage des erreurs (elles sont stockés dans self::$errors)
     */
    public static function disableErrorOutput()
    {
        static::$errorOutput = false;
    }

    /**
     * Active l'affichage des erreurs
     */
    public static function enableErrorOutput()
    {
        static::$errorOutput = true;
    }

    /**
     * Permet de capturer les erreurs d'une fonction en particulier
     *
     * @param callable $callback la fonction a executer sans erreurs
     * @param mixed    ...$args  arguments à envoyer au callback
     * @return array ['result' => 'résultat du callback', 'errors' => [errors]]
     */
    public static function captureErrors(callable $callback, ...$args): array
    {
        $instance = new self(Configure::read('Error'));
        $level = $instance->getConfig('errorLevel') ?: -1;
        error_reporting($level); // NOSONAR
        set_error_handler([$instance, 'handleError'], $level);
        set_exception_handler([$instance, 'handleException']);
        static::disableErrorOutput();
        $oldErrors = static::$errors;
        static::$errors = [];
        $result = call_user_func_array($callback, $args);
        $errors = static::$errors;
        static::$errors = $oldErrors;
        static::enableErrorOutput();
        restore_error_handler();
        restore_exception_handler();
        return compact('result', 'errors');
    }

    /**
     * Permet de savoir si les erreurs ont été désactivés/capturés
     * @return bool
     */
    public static function isErrorDisabled(): bool
    {
        return static::$errorOutput === false;
    }

    /**
     * Donne les erreurs capturés
     * @return array
     */
    public static function getErrors(): array
    {
        $errors = static::$errors;
        static::$errors = [];
        return $errors;
    }

    /**
     * Permet de gréffer une erreur dans la liste
     * @param array $error
     */
    public static function addError(array $error)
    {
        static::$errors[] = $error;
    }

    /**
     * Log an error.
     *
     * @param string $level The level name of the log.
     * @param array  $data  Array of error data.
     * @return bool
     */
    protected function _logError($level, $data): bool
    {
        if (static::$errorOutput) {
            return parent::_logError($level, $data);
        }
        return true;
    }

    /**
     * Method that can be easily stubbed in testing.
     *
     * @param string|Response $response Either the message or response object.
     * @return void
     */
    protected function _sendResponse($response): void
    {
        if (is_string($response)) {
            echo $response;

            return;
        }

        $emitter = new ResponseEmitter();
        $emitter->emit($response);
    }

    /**
     * On s'assure que les erreurs capturées soient envoyées
     */
    public function shutdown()
    {
        $debug = Configure::read('debug');
        static::enableErrorOutput();
        foreach (static::$errors as $error) {
            $this->_displayError($error, $debug);
        }
    }
}
