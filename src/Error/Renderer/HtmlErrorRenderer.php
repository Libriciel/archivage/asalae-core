<?php
/**
 * AsalaeCore\Error\Renderer\HtmlErrorRenderer
 */

namespace AsalaeCore\Error\Renderer;

use Cake\Error\Debugger;
use Cake\Error\Renderer\HtmlErrorRenderer as CakeHtmlErrorRenderer;
use Cake\Error\PhpError;

/**
 * Affichage des exceptions en mode CLI
 *
 * @category    Error
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2022, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class HtmlErrorRenderer extends CakeHtmlErrorRenderer
{
    /**
     * Render output for the provided error.
     *
     * @param PhpError $error The error to be rendered.
     * @param bool     $debug Whether or not the application is in debug mode.
     * @return string The output to be echoed.
     */
    public function render(PhpError $error, bool $debug): string
    {
        if (!$debug) {
            return '';
        }
        $id = 'cakeErr' . uniqid();
        $file = $error->getFile();

        // Some of the error data is not HTML safe so we escape everything.
        $description = h($error->getMessage());
        $path = h($file);
        $trace = h($error->getTraceAsString());
        $line = $error->getLine();

        $errorMessage = sprintf(
            '<b>%s</b> (%s)',
            h(ucfirst($error->getLabel())),
            h($error->getCode())
        );
        $toggle = $this->renderToggle($errorMessage, $id, 'trace');
        $codeToggle = $this->renderToggle('Code', $id, 'code');

        $excerpt = [];
        if ($file && $line) {
            $excerpt = Debugger::excerpt($file, $line, 1);
        }
        $code = implode("\n", $excerpt);

        $errorMessage = str_replace('-->', '_->', $error->getMessage());
        return <<<HTML

<!--
ERROR: $path:$line
$errorMessage
-->

<div class="cake-error">
    $toggle: $description [in <b>$path</b>, line <b>$line</b>]
    <div id="$id-trace" class="cake-stack-trace" style="display: none;">
        $codeToggle
        <pre id="$id-code" class="cake-code-dump" style="display: none;">$code</pre>
        <pre class="cake-trace">$trace</pre>
    </div>
</div>
HTML;
    }

    /**
     * Render a toggle link in the error content.
     *
     * @param string $text   The text to insert. Assumed to be HTML safe.
     * @param string $id     The error id scope.
     * @param string $suffix The element selector.
     * @return string
     */
    private function renderToggle(string $text, string $id, string $suffix): string
    {
        $selector = $id . '-' . $suffix;

        $display = "(document.getElementById('$selector').style.display === 'none' ? '' : 'none')";
        return <<<HTML
<a href="javascript:void(0);"
  onclick="document.getElementById('$selector').style.display = $display"
>
    $text
</a>
HTML;
    }
}
