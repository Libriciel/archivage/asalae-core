<?php
/**
 * AsalaeCore\Error\Renderer\ConsoleExceptionRenderer
 */

namespace AsalaeCore\Error\Renderer;

use Cake\Console\ConsoleOutput;
use Cake\Core\Configure;
use Cake\Core\Exception\CakeException;
use Psr\Http\Message\ServerRequestInterface;
use Throwable;

/**
 * Affichage des exceptions en mode CLI
 *
 * @category    Error
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2022, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ConsoleExceptionRenderer
{
    /**
     * @var Throwable
     */
    private $error;

    /**
     * @var ConsoleOutput
     */
    private $output;

    /**
     * @var bool
     */
    private $trace;

    /**
     * Constructor.
     *
     * @param Throwable                   $error   The error to render.
     * @param ServerRequestInterface|null $request Not used.
     * @param array                       $config  Error handling configuration.
     * @noinspection PhpUnusedParameterInspection reprend la signature de
     *                                            ConsoleExceptionRenderer
     */
    public function __construct(Throwable $error, ?ServerRequestInterface $request, array $config)
    {
        $this->error = $error;
        $this->output = $config['stderr'] ?? new ConsoleOutput('php://stderr');
        $this->trace = $config['trace'] ?? true;
    }

    /**
     * Render an exception into a plain text message.
     *
     * @return string
     */
    public function render()
    {
        $dash = str_repeat('-', 100);
        $out = [];
        $out[] = sprintf(
            "<error>%s\n[%s] code %d - %s</error>\n        at %s:%d\n<error>%s</error>",
            $dash,
            get_class($this->error),
            $this->error->getCode() ?: 500,
            $this->error->getMessage(),
            $this->error->getFile() ?? '',
            $this->error->getLine() ?? '',
            $dash
        );

        $debug = Configure::read('debug');
        if ($debug && $this->error instanceof CakeException) {
            $attributes = $this->error->getAttributes();
            if ($attributes) {
                $out[] = '<info>Exception Attributes</info>';
                $out[] = '';
                $out[] = var_export($this->error->getAttributes(), true);
            }
        }

        if ($this->trace) {
            $out[] = '<info>Stack Trace:</info>';
            $out[] = '';
            $out[] = $this->renderTrace($this->error);
        }
        $out[] = $dash;
        $out[] = '';

        return join("\n", $out);
    }

    /**
     * Write output to the output stream
     *
     * @param string $output The output to print.
     * @return void
     */
    public function write($output): void
    {
        $this->output->write($output);
    }

    /**
     * Rendu personnalisé du stackTrace CLI
     * @param Throwable $error
     * @return string
     */
    private function renderTrace(Throwable $error): string
    {
        $lines = [];
        foreach ($error->getTrace() as $i => $trace) {
            $path = isset($trace['file'])
                ? $trace['file'] . ':' . ($trace['line'] ?? '')
                : '[internal function]';
            $trace['function'] = $trace['function'] ?? '<unknown>';
            if (isset($trace['class']) && isset($trace['type'])) {
                $function = $trace['class'] . $trace['type'] . $trace['function'];
            } else {
                $function = $trace['function'];
            }
            $lines[] = sprintf(
                "<info>#%d</info> %s()\n        at %s",
                $i,
                $function,
                $path
            );
        }
        return implode("\n", $lines);
    }
}
