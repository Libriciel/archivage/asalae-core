<?php
/**
 * AsalaeCore\Error\Renderer\ConsoleErrorRenderer
 */

namespace AsalaeCore\Error\Renderer;

use Cake\Error\PhpError;
use Cake\Error\Renderer\ConsoleErrorRenderer as CakeConsoleErrorRenderer;

/**
 * Affichage des erreurs en mode CLI
 *
 * @category    Error
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2022, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ConsoleErrorRenderer extends CakeConsoleErrorRenderer
{
    /**
     * Render output for the provided error.
     *
     * @param PhpError $error The error to be rendered.
     * @param bool     $debug Whether or not the application is in debug mode.
     * @return string The output to be echoed.
     */
    public function render(PhpError $error, bool $debug): string
    {
        $trace = '';
        if ($this->trace) {
            $trace = "\n<info>Stack Trace:</info>\n\n" . $this->renderTrace($error);
        }

        return sprintf(
            "<error>%s\nError %s (%d) - %s</error>\n        at %s:%s\n<error>%s</error>%s\n%s\n",
            $dash = str_repeat('-', 100),
            $error->getLabel(),
            $error->getCode(),
            $error->getMessage(),
            $error->getFile() ?? '',
            $error->getLine() ?? '',
            $dash,
            $trace,
            $dash
        );
    }

    /**
     * Rendu personnalisé du stackTrace CLI
     * @param PhpError $error
     * @return string
     */
    private function renderTrace(PhpError $error): string
    {
        $lines = [];
        foreach ($error->getTrace() as $i => $trace) {
            $path = isset($trace['file'])
                ? $trace['file'] . ':' . ($trace['line'] ?? '')
                : '[internal function]';
            $function = $trace['reference'] ?? '';
            $lines[] = sprintf(
                "<info>#%d</info> %s\n        at %s",
                $i,
                $function,
                $path
            );
        }
        return implode("\n", $lines);
    }
}
