<?php
/**
 * AsalaeCore\Error\ErrorTrap
 */

namespace AsalaeCore\Error;

use AsalaeCore\Error\Renderer\ConsoleErrorRenderer;
use AsalaeCore\Error\Renderer\HtmlErrorRenderer;
use Cake\Error\ErrorTrap as CakeErrorTrap;
use Cake\Error\PhpError;
use Cake\Log\Log;

/**
 * Gestions d'erreurs de l'application
 *
 * @category    Error
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2022, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ErrorTrap extends CakeErrorTrap
{
    /**
     * Choose an error renderer based on config or the SAPI
     *
     * @return class-string<ConsoleErrorRenderer|HtmlErrorRenderer>
     */
    protected function chooseErrorRenderer(): string
    {
        $config = $this->getConfig('errorRenderer');
        if ($config !== null) {
            return $config;
        }

        /** @var class-string<ConsoleErrorRenderer|HtmlErrorRenderer> */
        return PHP_SAPI === 'cli' ? ConsoleErrorRenderer::class : HtmlErrorRenderer::class;
    }

    /**
     * Logging helper method.
     *
     * @param PhpError $error The error object to log.
     * @return void
     */
    protected function logError(PhpError $error): void
    {
        $stdout = Log::getConfig('stdout');
        $stderr = Log::getConfig('stderr');
        Log::drop('stdout');
        Log::drop('stderr');
        parent::logError($error);
        if (PHP_SAPI === 'cli' && $stdout && $stderr) {
            Log::setConfig('stdout', $stdout);
            Log::setConfig('stderr', $stderr);
        }
    }
}
