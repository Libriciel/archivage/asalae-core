<?php
/**
 * AsalaeCore\Error\AppExceptionRenderer
 * @noinspection RedundantSuppression
 */

namespace AsalaeCore\Error;

use AsalaeCore\Http\AppServerRequestFactory;
use AsalaeCore\Http\Response;
use Cake\Controller\Controller;
use Cake\Core\Configure;
use Cake\Core\Exception\Exception as CakeException;
use Cake\Core\Exception\MissingPluginException;
use Cake\Error\Debugger;
use Cake\Error\Renderer\WebExceptionRenderer;
use Cake\Event\Event;
use Cake\Http\Exception\HttpException;
use Cake\Http\Response as CakeResponse;
use Cake\Routing\Router;
use Cake\Utility\Inflector;
use Cake\View\Exception\MissingTemplateException;
use Exception;
use Throwable;

/**
 * Gestions d'exception de l'application
 *
 * @category    Error
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2017, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class AppExceptionRenderer extends WebExceptionRenderer
{
    /**
     * @var string
     */
    public $bufferStr = '';

    /**
     * @var string Redirige sur les templates du plugin
     */
    public static string $plugin = 'AsalaeCore.';

    /**
     * Surcharge de _getController() afin d'utiliser AppServerRequestFactory
     * @return Controller|null
     */
    protected function _getController(): Controller
    {
        $errorController = Configure::read('Error.errorController');
        if (!$errorController) {
            return parent::_getController();
        }

        // Copié collé (modifié du parent::_getController())
        if (!$request = Router::getRequest()) {
            $request = AppServerRequestFactory::fromGlobals(); // modifié
        }
        $response = new Response();
        $controller = null;

        try {
            // supprimé
            /* @var \Cake\Controller\Controller $controller */
            $controller = new $errorController($request, $response); // modifié
            $controller->startupProcess();
            $startup = true;
        } catch (Exception $e) {
            $startup = false;
        }

        // Retry RequestHandler, as another aspect of startupProcess()
        // could have failed. Ignore any exceptions out of startup, as
        // there could be userland input data parsers.
        if ($startup === false && !empty($controller) && isset($controller->RequestHandler)) {
            try {
                $event = new Event('Controller.startup', $controller);
                $controller->RequestHandler->startup($event);
            } catch (Exception $e) {
            }
        }
        if (empty($controller)) {
            $controller = new Controller($request, $response);
        }

        return $controller;
    }

    /**
     * Generate the response using the controller object.
     *
     * @param string $template The template to render.
     * @return Response A response object that can be sent.
     */
    protected function _outputMessage($template): CakeResponse
    {
        try {
            $this->controller->render($template);

            return $this->_shutdown();
        } catch (MissingTemplateException $e) {
            $builder = $this->controller->viewBuilder();
            if ($builder->getTemplatePath() === 'Error') {
                $nextTemplate = $this->error instanceof Exception
                && ((string)$this->error->getCode())[0] === '4'
                    ? static::$plugin . 'error400'
                    : static::$plugin . 'error500';
                if ($nextTemplate === $template) {
                    debug($e); // seul moyen de comprendre pourquoi le render à échoué
                    return $this->_outputMessageSafe(static::$plugin . 'error500');
                }
                return $this->_outputMessage($nextTemplate);
            }
            try {
                $this->error = $e;
                return $this->render();
            } catch (Exception $e2) {
            }

            $attributes = $e->getAttributes();
            if (isset($attributes['file']) && strpos($attributes['file'], 'error500') !== false) {
                return $this->_outputMessageSafe(static::$plugin . 'error500');
            }

            return $this->_outputMessage(static::$plugin . 'error500');
        } catch (MissingPluginException $e) {
            try {
                $this->error = $e;
                return $this->render();
            } catch (Exception $e2) {
            }
            $attributes = $e->getAttributes();
            if (isset($attributes['plugin']) && $attributes['plugin'] === $this->controller->getPlugin()) {
                $this->controller->setPlugin(null);
            }

            return $this->_outputMessageSafe(static::$plugin . 'error500');
        } catch (\Cake\Database\Exception $e) {
            if (PHP_SAPI === 'cli' && preg_match('/Cannot describe (\w+)./', $e->getMessage(), $m)) {
                debug("Missing fixture app.".Inflector::camelize($m[1]));
            } else {
                debug($e);
            }
            return $this->_outputMessageSafe(static::$plugin . 'error500');
        } catch (Exception $e) {
            debug($e); // seul moyen de comprendre pourquoi le render à échoué
            return $this->_outputMessageSafe(static::$plugin . 'error500');
        }
    }
    
    /**
     * A safer way to render error messages, replaces all helpers, with basics
     * and doesn't call component methods.
     *
     * @param string $template The template to render.
     * @return CakeResponse A response object that can be sent.
     */
    protected function _outputMessageSafe($template): CakeResponse
    {
        $builder = $this->controller->viewBuilder();
        $builder
            ->setLayoutPath('Layout')
            ->setTemplatePath('Error')
            ->setLayout('empty');
        $view = $this->controller->createView();

        $response = $this->controller->getResponse()
            ->withType('html')
            ->withStringBody($view->render($template, 'error'));
        $this->controller->setResponse($response);

        return $response;
    }

    /**
     * Renders the response for the exception.
     *
     * @return CakeResponse The response to be sent.
     */
    public function render(): CakeResponse
    {
        $exception = $this->error;
        // --------- bloc modifié ---------------------------------------
        $code = method_exists($this, 'getHttpCode')
            ? $this->getHttpCode($exception)
            : $this->_code($exception);
        // --------------------------------------------------------------
        $method = $this->_method($exception);
        $template = $this->_template($exception, $method, $code);
        $this->clearOutput();

        if (method_exists($this, $method)) {
            return $this->_customMethod($method, $exception);
        }

        $message = $this->_message($exception, $code);
        $url = $this->controller->getRequest()->getRequestTarget();
        $response = $this->controller->getResponse();

        if ($exception instanceof HttpException) {
            foreach ($exception->getHeaders() as $name => $value) {
                $response = $response->withHeader($name, $value);
            }
        }
        $response = $response->withStatus($code);

        $viewVars = [
            'message' => $message,
            'url' => h($url),
            'error' => $exception,
            'code' => $code,
        ];
        $serialize = ['message', 'url', 'code'];

        // --------- bloc modifié ---------------------------------------
        $isDebug = Configure::read('debug');
        if ($isDebug) {
            $rawTrace = $exception->getTrace();
            $trace = (array)Debugger::formatTrace(
                $rawTrace,
                [
                    'format' => 'array',
                    'args' => false,
                ]
            );
            $origin = [
                'file' => $exception->getFile() ?: 'null',
                'line' => $exception->getLine() ?: 'null',
            ];
            // Traces don't include the origin file/line.
            array_unshift($trace, $origin);
            $viewVars['local_trace'] = array_filter(
                $trace,
                function ($v) {
                    return strpos($v['file'], APP) !== false
                        || strpos($v['file'], ROOT.DS.'vendor'.DS.'libriciel') !== false;
                }
            );

            // stringify traces
            $shortPath = fn($path) => strpos($path, ASALAE_CORE) !== false
                ? 'CORE'.substr($path, strlen(ASALAE_CORE))
                : (strpos($path, CAKE) !== false
                    ? 'CAKE'.substr($path, strlen(CAKE) -1)
                    : 'ROOT'.substr($path, strlen(ROOT))
                );
            $mapper = fn($t)
                => $shortPath($t['file'])
                .':'.($t['line'] ?? -1)
                .' - '
                .(isset($t['class']) ? $t['class'].($t['type'] ?? '::') : '')
                .(isset($t['function']) ? $t['function'].'()' : '');
            $keys = array_keys($viewVars['local_trace']);
            $trace = array_map($mapper, $trace, $keys);
            $viewVars['local_trace'] = array_map($mapper, $viewVars['local_trace'], $keys);

            $viewVars['file'] = $exception->getFile() ?: 'null';
            $viewVars['line'] = $exception->getLine() ?: 'null';
            $viewVars['trace'] = $trace;
            $viewVars['_serialize'][] = 'local_trace';
            $viewVars['_serialize'][] = 'file';
            $viewVars['_serialize'][] = 'line';
            $viewVars['_serialize'][] = 'trace';
            $viewVars += $origin;
            $serialize[] = 'file';
            $serialize[] = 'line';
            $serialize[] = 'local_trace';
            $serialize[] = 'trace';
            if ($this->bufferStr) {
                $viewVars['buffer'] = $this->bufferStr;
                $viewVars['_serialize'][] = 'buffer';
                $serialize[] = 'buffer';
            }
        }
        $this->controller->set($viewVars);
        $this->controller->viewBuilder()->setOption('serialize', $serialize);

        if ($exception instanceof CakeException && $isDebug) {
            $this->controller->set($exception->getAttributes());
        }
        $this->controller->setResponse($response);

        return $this->_outputMessage($template);
    }

    /**
     * Clear output buffers so error pages display properly.
     *
     * @return void
     */
    protected function clearOutput(): void
    {
        if (in_array(PHP_SAPI, ['cli', 'phpdbg'])) {
            return;
        }
        while (ob_get_level()) {
            $this->bufferStr .= ob_get_clean();
        }
    }

    /**
     * Compatibilité CakePHP < 4.2.0
     * Get HTTP status code.
     *
     * @param \Throwable $exception Exception.
     * @return int A valid HTTP error status code.
     */
    protected function _code(Throwable $exception): int
    {
        $code = 500;
        $errorCode = $exception->getCode();
        if ($errorCode >= 400 && $errorCode < 600) {
            $code = $errorCode;
        }

        return $code;
    }

    /**
     * Get template for rendering exception info.
     *
     * @param Throwable $exception Exception instance.
     * @param string    $method    Method name.
     * @param int       $code      Error code.
     * @return string Template name
     */
    protected function _template(Throwable $exception, string $method, int $code): string
    {
        $customTemplate = Configure::read('AppExceptionRenderer.custom_templates.' . get_class($exception));
        if ($customTemplate && !empty($customTemplate['template'])) {
            if ($customTemplate['layout'] ?? false) {
                $builder = $this->controller->viewBuilder();
                $builder->setLayout($customTemplate['layout']);
            }
            return $customTemplate['template'];
        }
        return parent::_template($exception, $method, $code);
    }
}
