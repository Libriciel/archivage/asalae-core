<?php
/**
 * AsalaeCore\Error\Debug\HtmlFormatter
 * @noinspection PhpInternalEntityUsedInspection
 */
namespace AsalaeCore\Error\Debug;

use Cake\Error\Debug\HtmlFormatter as CakeHtmlFormatter;
use Cake\Error\Debug\FormatterInterface;
use Cake\Error\Debug\NodeInterface;
use Cake\Error\Debug\TextFormatter;

/**
 * Modification du rendu du debug en html
 *
 * @category    Error
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2022, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class HtmlFormatter extends CakeHtmlFormatter implements FormatterInterface
{

    /**
     * Convert a tree of NodeInterface objects into HTML
     *
     * @param \Cake\Error\Debug\NodeInterface $node The node tree to dump.
     * @return string
     */
    public function dump(NodeInterface $node): string
    {
        $html = $this->export($node, 0);
        $head = '';
        if (!static::$outputHeader) {
            static::$outputHeader = true;
            $head = $this->dumpHeader();
        }
        $textFormatter = new TextFormatter;
        $htmlComment = [
            '',
            '<!--',
            'DEBUG VALUE:',
            str_replace('-->', '_->', $textFormatter->dump($node)),
            '-->',
            '',
            '',
        ];
        return implode("\n", $htmlComment) . $head . '<div class="cake-dbg">' . $html . '</div>';
    }

    /**
     * Output a dump wrapper with location context.
     *
     * @param string $contents The contents to wrap and return
     * @param array  $location The file and line the contents came from.
     * @return string
     */
    public function formatWrapper(string $contents, array $location): string
    {
        $lineInfo = '';
        $inlineInfo = '';
        if (isset($location['file'], $location['file'])) {
            $lineInfo = sprintf(
                '<span><strong>%s</strong> (line <strong>%s</strong>)</span>',
                $location['file'],
                $location['line']
            );
            $inlineInfo = $location['file'] . ':' . $location['line'];
        }
        $parts = [
            '',
            '<!--',
            'DEBUG LOCATION:',
            $inlineInfo,
            '-->',
            '',
            '<div class="cake-debug-output cake-debug" style="direction:ltr">',
            $lineInfo,
            $contents,
            '</div>',
        ];

        return implode("\n", $parts);
    }
}
