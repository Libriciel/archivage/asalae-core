<?php
/**
 * AsalaeCore\Utility\DOMUtility
 */

namespace AsalaeCore\Utility;

use AsalaeCore\Form\GenericMessageFormTrait;
use AsalaeCore\Form\MessageForm;
use DOMAttr;
use DOMDocument;
use DOMElement;
use DOMException;
use DOMNameSpaceNode;
use DOMNode;
use DOMText;
use DOMXPath;
use Exception;

/**
 * Outils pour les DOMDocument
 *
 * @category Utility
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class DOMUtility
{
    /**
     * @var DOMDocument
     */
    public $dom;

    /**
     * @var string
     */
    public $namespace;

    /**
     * @var DOMXPath
     */
    public $xpath;

    /**
     * DOMUtility constructor.
     * @param DOMDocument $dom
     */
    public function __construct(DOMDocument $dom)
    {
        $this->dom = $dom;
        $xmlns = $dom->documentElement->getAttributeNode('xmlns');
        $namespace = $xmlns ? $xmlns->nodeValue : null;
        $this->xpath = new DOMXPath($dom);
        if (!$namespace) {
            $ns = current(explode(':', $dom->documentElement->tagName));
            $xmlns = $dom->documentElement->getAttributeNodeNS('http://www.w3.org/2000/xmlns/', $ns);
            $namespace = $xmlns ? $xmlns->nodeValue : null;
        }
        if ($namespace) {
            $this->namespace = $namespace;
            $this->xpath->registerNamespace('ns', $namespace);
        }
    }

    /**
     * Donne un DOMUtility à partir d'un chemin de fichier
     * @param string $path
     * @return DOMUtility
     * @throws Exception
     */
    public static function load(string $path): DOMUtility
    {
        if (!is_file($path)) {
            throw new Exception(sprintf('file not found on %s', $path));
        }
        $dom = new DOMDocument;
        $dom->formatOutput = true;
        $dom->preserveWhiteSpace = false;
        if (!$dom->load($path)) {
            throw new Exception(sprintf('unable to open the xml on %s', $path));
        }
        return new DOMUtility($dom);
    }

    /**
     * Donne un DOMUtility à partir d'un xml
     * @param string $xml
     * @return DOMUtility
     */
    public static function loadXML(string $xml): DOMUtility
    {
        $dom = new DOMDocument;
        $dom->loadXML($xml);
        return new DOMUtility($dom);
    }

    /**
     * Transforme un array en DOMDocument
     * @param array $data
     * @return void
     * @throws DOMException
     */
    public static function arrayToDOMDocument(array $data): DOMDocument
    {
        $dom = new DOMDocument;
        $dom->preserveWhiteSpace = false;
        $dom->formatOutput = true;
        $rootName = key($data);
        $dom->loadXML("<$rootName></$rootName>");
        $element = $dom->documentElement;
        $dataRoot = $data[$rootName][0] ?? $data[$rootName];
        foreach ($dataRoot as $key => $value) {
            // valeur, attributs et xmlns
            if (preg_match('/^@(.*)$/', $key, $m)) {
                $attr = $m[1] ?? '';
                // attributs et xmlns
                if ($attr) {
                    $element->setAttribute($attr, $value);
                    // valeur
                } elseif ($value) {
                    $text = static::createDomTextNode($dom, $value);
                    $element->appendChild($text);
                }
                // sous noeuds
            } elseif (is_array($value)) {
                self::arrayToDOMElement($element, $key, $value);
            } elseif (is_string($value)) {
                $text = static::createDomTextNode($dom, $value);
                $subElement = $dom->createElement($key);
                $subElement->appendChild($text);
                $element->appendChild($subElement);
            }
        }

        // interprétation des namespaces
        $dom2 = new DOMDocument;
        $dom2->preserveWhiteSpace = false;
        $dom2->formatOutput = true;
        $dom2->loadXML($dom->saveXML());
        return $dom2;
    }

    /**
     * Fonction recursive pour convertir un array en DOMElement
     * @param DOMElement $parent
     * @param string     $nodeName
     * @param array      $data
     * @return void
     */
    private static function arrayToDOMElement(DOMElement $parent, string $nodeName, array $data)
    {
        if (!isset($data[0])) {
            $data = [$data];
        }
        $dom = $parent->ownerDocument;
        foreach ($data as $values) {
            $element = $dom->createElement($nodeName);
            $parent->appendChild($element);
            if (!is_array($values)) {
                $values = ['@' => $values];
            }
            foreach ($values as $key => $value) {
                // valeur, attributs et xmlns
                if (preg_match('/^@(.*)$/', $key, $m)) {
                    $attr = $m[1] ?? '';
                    // attributs et xmlns
                    if ($attr) {
                        $element->setAttribute($attr, $value);
                        // valeur
                    } elseif ($value) {
                        $text = static::createDomTextNode($dom, $value);
                        $element->appendChild($text);
                    }

                    // cas 'NodeName' => ['@' => 'value']
                } elseif (is_array($value) && count($value) === 1 && isset($value['@'])) {
                    $text = static::createDomTextNode($dom, $value['@']);
                    $element->appendChild($text);

                    // sous noeuds
                } elseif (is_array($value)) {
                    self::arrayToDOMElement($element, $key, $value);

                    // cas 'NodeName' => 'value'
                } elseif (is_string($value)) {
                    $text = static::createDomTextNode($dom, $value);
                    $subElement = $dom->createElement($key);
                    $subElement->appendChild($text);
                    $element->appendChild($subElement);
                }
            }
        }
    }

    /**
     * Donne le chemin sous forme d'array
     * ex: ['ArchiveTransfer', 'Archive', 'Document', 'Attachment']
     * @param DOMElement $element
     * @param array      $path
     * @return array
     */
    public static function getElementPath(DOMElement $element, array $path = []): array
    {
        $path[] = $element->nodeName;
        if (!$element->parentNode || $element->parentNode->nodeName[0] === '#') {
            return array_reverse($path);
        } else {
            return self::getElementPath($element->parentNode, $path);
        }
    }

    /**
     * Défini une valeur pour un element via la création d'un TextNode
     * @param DOMElement $element
     * @param string     $value
     * @return DOMElement
     */
    public static function setValue(DOMElement $element, string $value): DOMElement
    {
        $element->nodeValue = '';
        $text = static::createDomTextNode($element->ownerDocument, $value);
        $element->appendChild($text);
        return $element;
    }

    /**
     * Utile pour utiliser la fonction xpath "normalize-space"
     * ex: sprintf('//ns:Name[normalize-space(.)=%s]', DOMUtility::normalizeSpace($search, true));
     * @param string $value
     * @param bool   $quote
     * @return string
     */
    public static function normalizeSpace(string $value, bool $quote = false): string
    {
        $normalized = trim(preg_replace('/\s+/', ' ', $value));
        if ($quote) {
            $normalized = self::xpathQuote($normalized);
        }
        return $normalized;
    }

    /**
     * Echape une chaine qui contient à la fois des quotes et des double quotes
     * @param string $value
     * @return string
     * @link https://stackoverflow.com/questions/24410671/how-to-escape-xpath-in-php
     */
    public static function xpathQuote(string $value): string
    {
        if (strpos($value, '"') === false) {
            return '"'.$value.'"';
        }
        if (strpos($value, "'") === false) {
            return "'".$value."'";
        }
        $sb = 'concat(';
        $substrings = explode('"', $value);
        $count = count($substrings);
        for ($i = 0; $i < $count; ++$i) {
            $needComma = $i > 0;
            if ($substrings[$i] !== '') {
                if ($i > 0) {
                    $sb .= ', ';
                }
                $sb .= '"'.$substrings[$i].'"';
                $needComma = true;
            }
            if ($i < ($count -1)) {
                if ($needComma) {
                    $sb .= ', ';
                }
                $sb .= "'\"'";
            }
        }
        $sb .= ')';
        return $sb;
    }

    /**
     * Enlève tous les namespaces d'un document
     * @link https://gist.github.com/odan/713515f941bb3b109e461979589ab45f
     * @param DOMDocument $domSource
     * @return DOMDocument
     */
    public static function removeNamespaces(DOMDocument $domSource): DOMDocument
    {
        $dom = new DOMDocument();
        $dom->formatOutput = true;

        $domSource = clone $domSource;
        $domSource->formatOutput = true;

        $dom->loadXML(preg_replace('/\sxmlns="(.*?)"/', '', $domSource->saveXML()));
        $xpath = new DOMXPath($dom);

        /** @var \DOMNameSpaceNode|\DOMAttr $namespaceNode */
        foreach ($xpath->query('//namespace::*') as $namespaceNode) {
            $prefix = str_replace('xmlns:', '', $namespaceNode->nodeName);
            $nodes = $xpath->query("//*[namespace::$prefix]");

            /** @var DOMElement $node */
            foreach ($nodes as $node) {
                $namespaceUri = $node->lookupNamespaceURI($prefix);
                $node->removeAttributeNS($namespaceUri, $prefix);
            }
        }

        // Important: Reload document to remove invalid xpath references from old dom
        $dom->loadXML($dom->saveXML());

        return $dom;
    }

    /**
     * Recherche par contenu, donne un array sous la forme
     * ['/Document/Element[4]', '/Document/Element[7]@attr']
     * @param string          $content
     * @param DOMElement|null $searchIn
     * @return array
     */
    public function findByContent(string $content, DOMElement $searchIn = null): array
    {
        $qContent = $this->xpathQuote($content);
        $query = $this->xpath->query(
            sprintf('.//ns:*[contains(text(), %s)]', $qContent),
            $searchIn
        );
        $result = [];
        foreach ($query as $element) {
            $result[] = $this->getElementXpath($element, '');
        }
        $query = $this->xpath->query(
            sprintf('.//ns:*[@*[contains(., %s)]]', $qContent),
            $searchIn
        );
        /** @var DOMElement $element */
        foreach ($query as $element) {
            $elementXpath = $this->getElementXpath($element, '');
            /** @var DOMAttr $attr */
            foreach ($element->attributes as $attr) {
                if (strpos($attr->nodeValue, $content) !== false) {
                    $elementXpath .= '@'.$attr->nodeName;
                    break;
                }
            }
            $result[] = $elementXpath;
        }
        return $result;
    }

    /**
     * Donne un chemin xpath sous la forme "/Document/Element[4]" à partir d'un
     * DOMElement
     * @param DOMElement $element
     * @param string     $prefix
     * @return string
     */
    public function getElementXpath(DOMElement $element, string $prefix = 'ns:'): string
    {
        $tagname = self::tagName($element);
        $query = $this->xpath->query('ns:'.$tagname, $element->parentNode);
        $count = $query->count();
        $actualNodeName = $tagname;
        if ($count !== 1) {
            foreach ($query as $i => $elem) {
                if ($elem === $element) {
                    $actualNodeName = $tagname . '[' . ($i +1) . ']';
                }
            }
        }
        if ($element->parentNode->nodeType !== XML_DOCUMENT_NODE) {
            $actualNodeName = $this->getElementXpath($element->parentNode, $prefix) . '/' . $prefix . $actualNodeName;
        } else {
            $actualNodeName = '/' . $prefix . $actualNodeName;
        }
        return $actualNodeName;
    }

    /**
     * Retire l'éventuel namespace du tagname
     * @param DOMElement $element
     * @return string
     */
    public static function tagName(DOMElement $element): string
    {
        $exp = explode(':', $element->tagName);
        return $exp[1] ?? $exp[0];
    }

    /**
     * Create new element node
     * @link http://php.net/manual/domdocument.createelement.php
     * @param string $name  <p>The tag name of the element.</p>
     * @param null   $value [optional] <p>
     *                      The value of
     *                      the element.
     *                      By default, an
     *                      empty element
     *                      will be
     *                      created. You
     *                      can also set
     *                      the value
     *                      later with
     *                      DOMElement->nodeValue.
     *                      </p>
     * @return DOMElement|bool a new instance of class DOMElement or false
     * if an error occured.
     * @throws DOMException
     */
    public function createElement(string $name, $value = null)
    {
        $element = $this->dom->createElementNS($this->namespace, $name);
        $element->appendChild($this->createTextNode($value));
        return $element;
    }

    /**
     * Ajoute un element $newNode dans $element
     * @param DOMElement $parentNode
     * @param DOMNode    $newNode
     * @return DOMElement|DOMNode
     * @throws Exception
     */
    public function appendChild(DOMElement $parentNode, DOMNode $newNode)
    {
        try {
            $schema = MessageForm::getFormClassname($this->namespace);
        } catch (Exception $e) {
            return $parentNode->appendChild($newNode);
        }
        if (empty($parentNode->ownerDocument) || !$newNode instanceof DOMElement) {
            return $parentNode->appendChild($newNode);
        }
        /** @var GenericMessageFormTrait|string $schema */
        $node = $schema::smartAppendNode($parentNode, $newNode->tagName);
        $parentNode->insertBefore($newNode, $node);
        $parentNode->removeChild($node);
        return $newNode;
    }

    /**
     * Donne la valeur du 1er noeud trouvé, null sinon
     * @param string $xpath
     * @param null   $contextnode
     * @param int    $index
     * @return string|null
     */
    public function nodeValue(string $xpath, $contextnode = null, int $index = 0)
    {
        $node = $this->node($xpath, $contextnode, $index);
        /** @noinspection PhpExpressionAlwaysNullInspection faux positif */
        return $node ? $node->nodeValue : null;
    }

    /**
     * Donne le noeud $index correspondant à $xpath dans $contextnode
     * @param string $xpath
     * @param null   $contextnode
     * @param int    $index
     * @return DOMNode|null
     */
    public function node(string $xpath, $contextnode = null, int $index = 0)
    {
        return $this->xpath->query($xpath, $contextnode)->item($index);
    }

    /**
     * Renvoi le DOMDocument sous forme d'array
     * @param bool $simplify
     * @return array
     */
    public function toArray(bool $simplify = false): array
    {
        $elem = $this->dom->documentElement;
        $nodeName = $elem->nodeName;
        $result = [
            $nodeName => [],
        ];
        $result[$nodeName][] = self::elementToArray($elem);
        return $simplify ? self::simplifyArray($result) : $result;
    }

    /**
     * Renvoi un DOMElement sous forme d'array
     * @param DOMElement $elem
     * @param array      $ns
     * @return array
     */
    public static function elementToArray(DOMElement $elem, array $ns = []): array
    {
        $result = [];
        $values = [];

        $xpath = new DOMXPath($elem->ownerDocument);
        /** @var DOMNameSpaceNode $ns */
        foreach ($xpath->query('namespace::*', $elem) as $nsNode) {
            if ($nsNode->nodeName !== 'xmlns:xml'
                && (!isset($ns['@'.$nsNode->nodeName])
                || $nsNode->nodeValue !== $ns['@'.$nsNode->nodeName])
            ) {
                $result['@'.$nsNode->nodeName] = $nsNode->nodeValue;
            }
        }
        $newNs = array_merge($result, $ns);

        /** @var DOMAttr $attr */
        foreach ($elem->attributes as $attr) {
            $result['@'.$attr->nodeName] = $attr->nodeValue;
        }

        foreach ($elem->childNodes as $child) {
            if ($child instanceof DOMElement) {
                $result[$child->nodeName][] = self::elementToArray($child, $newNs);
            } elseif ($child instanceof DOMText && trim($child->nodeValue)) {
                $values[] = trim($child->nodeValue);
            }
        }
        if ($values) {
            $result['@'] = trim(implode("\n", $values));
        }
        return $result;
    }

    /**
     * Certains caractères spéciaux ne sont pas échapés par DOMText et
     * provoquent des erreurs, cette fonction permet de les retirer.
     * @param string $str
     * @return string
     */
    public static function removeForbiddenXmlChars(string $str): string
    {
        return preg_replace(
            '/[\x01-\x08\x0B\x0C\x0E-\x0F\x10-\x19\x1A-\x1F]/u',
            '',
            $str
        );
    }

    /**
     * Donne un DOMText avec le $text sanitized
     * @param string $text
     * @return DOMText
     */
    public function createTextNode($text = ''): DOMText
    {
        return static::createDomTextNode($this->dom, $text);
    }

    /**
     * Donne un DOMText avec le $text sanitized
     * @param DOMDocument $dom
     * @param string      $text
     * @return DOMText
     */
    public static function createDomTextNode(DOMDocument $dom, $text = ''): DOMText
    {
        return $dom->createTextNode(static::removeForbiddenXmlChars((string)$text));
    }

    /**
     * Reformate l'array d'un toArray() pour resembler au résultat de Craur
     * @param array $data
     * @return array|string
     */
    private static function simplifyArray(array $data)
    {
        if (count($data) === 1 && (isset($data['@']) || isset($data[0]))) {
            $data = current($data);
            if (!is_array($data)) {
                return $data;
            } elseif (count($data) === 1 && isset($data['@'])) {
                return $data['@'];
            }
        }
        $result = [];
        foreach ($data as $key => $values) {
            if (is_array($values)) {
                $values = self::simplifyArray($values);
            }
            $result[$key] = $values;
        }
        return $result;
    }
}
