<?php
/**
 * AsalaeCore\Utility\Premis\Agent
 * @noinspection HttpUrlsUsage https correct mais http utilisé dans le schema
 */

namespace AsalaeCore\Utility\Premis;

use AsalaeCore\Utility\DOMUtility;
use AsalaeCore\Utility\Premis;
use Cake\Utility\Text;
use DOMAttr;
use DOMDocument;
use DOMElement;
use DOMException;
use Exception;

/**
 * Agent premis v3.0
 *
 * @category Utility
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Agent implements PremisElementInterface
{
    /**
     * Valeurs par défaut du bloc agentType
     */
    const AUTHORITY = 'agentType';
    const AUTHORITY_URI = 'http://id.loc.gov/vocabulary/preservation/agentType'; // NOSONAR
    const VALUE_URI = 'http://id.loc.gov/vocabulary/preservation/agentType/per'; // NOSONAR
    const TYPE_VALUE = 'person';

    /**
     * @var string agentIdentifierType obligatoire
     */
    public $identifierType;

    /**
     * @var string agentIdentifierValue obligatoire
     */
    public $identifierValue;

    /**
     * @var string|string[] agentName
     */
    public $name;

    /**
     * @var array agentType
     * @see http://id.loc.gov/vocabulary/preservation/agentType.html
     */
    public $type = [
        '@authority' => self::AUTHORITY,
        '@authorityURI' => self::AUTHORITY_URI,
        '@valueURI' => self::VALUE_URI,
        '@' => self::TYPE_VALUE,
    ];

    /**
     * Agent constructor.
     * @param string      $identifierType
     * @param string|null $identifierValue
     */
    public function __construct(string $identifierType = 'UUID', string $identifierValue = null)
    {
        $this->identifierType = $identifierType;
        $this->identifierValue = $identifierValue ?: Text::uuid();
    }

    /**
     * Transforme l'objet en \DOMElement
     * @return DOMElement
     * @throws DOMException
     */
    public function renderElement(): DOMElement
    {
        $dom = new DOMDocument;

        $agent = $dom->createElementNS(Premis::NS_PREMIS, 'agent');
        $agentIdentifier = $dom->createElementNS(Premis::NS_PREMIS, 'agentIdentifier');
        $agentIdentifierType = $dom->createElementNS(
            Premis::NS_PREMIS,
            'agentIdentifierType'
        );
        $agentIdentifierType->appendChild(DOMUtility::createDomTextNode($dom, $this->identifierType));
        $agentIdentifierValue = $dom->createElementNS(
            Premis::NS_PREMIS,
            'agentIdentifierValue'
        );
        $agentIdentifierValue->appendChild(DOMUtility::createDomTextNode($dom, $this->identifierValue));
        $agentIdentifier->appendChild($agentIdentifierType);
        $agentIdentifier->appendChild($agentIdentifierValue);
        $agent->appendChild($agentIdentifier);

        foreach ((array)$this->name as $name) {
            $agentName = $dom->createElementNS(Premis::NS_PREMIS, 'agentName');
            $agentName->appendChild(DOMUtility::createDomTextNode($dom, $name));
            $agent->appendChild($agentName);
        }
        if ($this->type) {
            $agentType = $dom->createElementNS(Premis::NS_PREMIS, 'agentType');
            foreach ($this->type as $key => $value) {
                $key = ltrim($key, '@');
                if ($key) {
                    $agentType->setAttribute($key, $value);
                } else {
                    DOMUtility::setValue($agentType, $value);
                }
            }
            $agent->appendChild($agentType);
        }

        return $agent;
    }

    /**
     * Récupère un Agent au sein d'un document
     * @param string      $identifierType
     * @param string      $identifierValue
     * @param DOMDocument $dom
     * @return null
     * @throws Exception
     */
    public static function find(string $identifierType, string $identifierValue, DOMDocument $dom)
    {
        foreach ($dom->getElementsByTagName('agent') as $agent) {
            if (!$agent instanceof DOMElement) {
                continue;
            }
            foreach ($agent->getElementsByTagName('agentIdentifier') as $agentIdentifier) {
                if (!$agentIdentifier instanceof DOMElement) {
                    continue;
                }
                $type = $agentIdentifier->getElementsByTagName('agentIdentifierType')
                    ->item(0)
                    ->nodeValue;
                $value = $agentIdentifier->getElementsByTagName('agentIdentifierValue')
                    ->item(0)
                    ->nodeValue;
                if ($type === $identifierType && $value === $identifierValue) {
                    return static::loadElement($agent);
                }
            }
        }
        return null;
    }

    /**
     * Créé un Agent à partir d'un \DOMElement
     * @param DOMElement $element
     * @return Agent
     * @throws Exception
     */
    public static function loadElement(DOMElement $element): Agent
    {
        $agentIdentifier = $element->getElementsByTagName('agentIdentifier')->item(0);
        if (!$agentIdentifier instanceof DOMElement) {
            throw new Exception;
        }
        $agentIdentifierType = $agentIdentifier
            ->getElementsByTagName('agentIdentifierType')
            ->item(0)
            ->nodeValue;
        $agentIdentifierValue = $agentIdentifier
            ->getElementsByTagName('agentIdentifierValue')
            ->item(0)
            ->nodeValue;
        $agent = new Agent($agentIdentifierType, $agentIdentifierValue);

        $agent->name = [];
        foreach ($element->getElementsByTagName('agentName') as $agentName) {
            if (!$agentName instanceof DOMElement) {
                continue;
            }
            $agent->name[] = $agentName->nodeValue;
        }

        $agentType = $element->getElementsByTagName('agentType')->item(0);
        if ($agentType) {
            /** @var DOMAttr $attr */
            foreach ($agentType->attributes as $attr) {
                $agent->type['@'.$attr->nodeName] = $attr->nodeValue;
            }
            $agent->type['@'] = $agentType->nodeValue;
        }

        return $agent;
    }

    /**
     * Donne la valeur string du rendu xml
     * @return string
     * @throws DOMException
     */
    public function __toString()
    {
        $element = $this->renderElement();
        return $element->ownerDocument->saveXML($element);
    }
}
