<?php
/**
 * AsalaeCore\Utility\Premis\PremisElementInterface
 */

namespace AsalaeCore\Utility\Premis;

use DOMElement;

/**
 * Element premis
 *
 * @category Utility
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
interface PremisElementInterface
{
    /**
     * Transforme l'objet en \DOMElement
     * @return DOMElement
     */
    public function renderElement(): DOMElement;
}
