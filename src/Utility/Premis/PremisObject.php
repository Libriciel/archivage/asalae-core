<?php
/**
 * AsalaeCore\Utility\Premis\Object
 */

namespace AsalaeCore\Utility\Premis;

use Cake\Http\Exception\NotImplementedException;
use DOMDocument;
use DOMElement;

/**
 * Fonctions communes pour les objects premis v3.0
 *
 * @category Utility
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
abstract class PremisObject
{
    /**
     * Récupère un Objet au sein d'un document
     * @param string      $identifierType
     * @param string      $identifierValue
     * @param DOMDocument $dom
     * @return null
     */
    public static function find(string $identifierType, string $identifierValue, DOMDocument $dom)
    {
        foreach ($dom->getElementsByTagName('objet') as $objet) {
            if (!$objet instanceof DOMElement) {
                continue;
            }
            foreach ($objet->getElementsByTagName('objectIdentifier') as $objectIdentifier) {
                if (!$objectIdentifier instanceof DOMElement) {
                    continue;
                }
                $type = $objectIdentifier->getElementsByTagName('objectIdentifierType')
                    ->item(0)
                    ->nodeValue;
                $value = $objectIdentifier->getElementsByTagName('objectIdentifierValue')
                    ->item(0)
                    ->nodeValue;
                if ($type === $identifierType && $value === $identifierValue) {
                    return static::loadElement($objet);
                }
            }
        }
        return null;
    }

    /**
     * Créé un Object à partir d'un \DOMElement
     * @param DOMElement $element
     * @return ObjectInterface|File|IntellectualEntity
     */
    public static function loadElement(DOMElement $element): ObjectInterface
    {
        switch ($element->getAttribute('xsi:type')) {
            case 'premis:intellectualEntity':
                $object = IntellectualEntity::loadElement($element);
                break;
            case 'premis:file':
                $object = File::loadElement($element);
                break;
            default:
                throw new NotImplementedException('Not implemented: '.$element->getAttribute('xsi:type'));
        }
        return $object;
    }
}
