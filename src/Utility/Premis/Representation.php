<?php
/**
 * AsalaeCore\Utility\Premis\Representation
 */

namespace AsalaeCore\Utility\Premis;

use DOMElement;

/**
 * Objet representation premis v3.0
 *
 * A Representation is the set of files, including structural metadata,
 * needed for a complete rendition of an Intellectual Entity.
 * For example, a journal article may be complete in one PDF file;
 * this single file constitutes the Representation. Another journal article may
 * consist of one SGML file and two image files; these three files constitute
 * the Representation. A third article may be represented by one TIFF image for
 * each of 12 pages plus an XML file of structural metadata showing the order of
 * the pages; these 13 files constitute the Representation.
 * Starting with PREMIS version 3.0 physical items, such as manuscripts or
 * printed documents, may also be Representations so that digital and
 * non-digital Representations can be captured uniformly.
 *
 * @category Utility
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Representation implements ObjectInterface
{
    /**
     * @inheritDoc
     */
    public function renderElement(): DOMElement
    {
        return new DOMElement;
    }

    /**
     * Donne l'identifiant de l'objet
     * @return string
     */
    public function getIdentifierValue(): string
    {
        return '';
    }
}
