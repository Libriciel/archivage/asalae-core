<?php
/**
 * AsalaeCore\Utility\Premis\File
 * @noinspection HttpUrlsUsage https correct mais http utilisé dans le schema
 */

namespace AsalaeCore\Utility\Premis;

use AsalaeCore\Utility\DOMUtility;
use AsalaeCore\Utility\Premis;
use Cake\Utility\Text;
use DOMAttr;
use DOMDocument;
use DOMElement;
use DOMException;
use Exception;
use FileValidator\Utility\FileValidator;

/**
 * Objet file premis v3.0
 *
 * A File is a named and ordered sequence of bytes that is known to an operating
 * system. A File can be zero or more bytes and has a File format, access
 * permissions, and File system characteristics such as size and last
 * modification date
 *
 * @category Utility
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class File implements ObjectInterface, PremisElementInterface
{
    /**
     * Valeurs par défaut du bloc messageDigestAlgorithm
     */
    const ALGO_AUTHORITY = 'cryptographicHashFunctions';
    const ALGO_AUTHORITY_URI = 'http://id.loc.gov/vocabulary/preservation/cryptographicHashFunctions'; // NOSONAR
    const ALGO_VALUE_URI = 'http://id.loc.gov/vocabulary/preservation/cryptographicHashFunctions/sha256'; // NOSONAR
    const ALGO_HASH_ALGO = 'SHA-256';
    const RELATIONSHIP_AUTHORITY = 'relationshipType';
    const RELATIONSHIP_AUTHORITY_URI = 'http://id.loc.gov/vocabulary/preservation/relationshipType'; // NOSONAR
    const RELATIONSHIP_VALUE_URI = 'http://id.loc.gov/vocabulary/preservation/relationshipType/dep'; // NOSONAR
    const RELATIONSHIP_VALUE = 'dependency';
    const RELATIONSHIP_SUB_AUTHORITY = 'relationshipSubType';
    const RELATIONSHIP_SUB_AUTHORITY_URI = 'http://id.loc.gov/vocabulary/preservation/relationshipSubType'; // NOSONAR
    const RELATIONSHIP_SUB_VALUE_URI = 'http://id.loc.gov/vocabulary/preservation/relationshipSubType/req'; // NOSONAR
    const RELATIONSHIP_SUB_VALUE = 'requires';

    /**
     * @var string objectIdentifierType obligatoire
     */
    public $identifierType;

    /**
     * @var string objectIdentifierValue obligatoire
     */
    public $identifierValue;

    /**
     * @var string formatName obligatoire
     */
    public $formatName;

    /**
     * @var array contenu de la balise messageDigestAlgorithm sous forme d'array
     */
    public $messageDigestAlgorithm = [
        '@authority' => self::ALGO_AUTHORITY,
        '@authorityURI' => self::ALGO_AUTHORITY_URI,
        '@valueURI' => self::ALGO_VALUE_URI,
        '@' => self::ALGO_HASH_ALGO,
    ];

    /**
     * @var string hash value
     */
    public $messageDigest;

    /**
     * @var int file size
     */
    public $size;

    /**
     * @var string formatRegistryName
     */
    public $formatRegistryName;

    /**
     * @var string formatRegistryKey
     */
    public $formatRegistryKey;

    /**
     * @var string originalName
     */
    public $originalName;

    /**
     * @var array storage [[
     *                      'contentLocationType' => 'local',
     *                      'contentLocationValue' => 'Volumes:1',
     *                      'storageMedium' => 'Volume 01'
     *                    ]]
     */
    public $storages = [];

    /**
     * @var array relationshipType
     */
    public $relationshipType = [
        '@authority' => self::RELATIONSHIP_AUTHORITY,
        '@authorityURI' => self::RELATIONSHIP_AUTHORITY_URI,
        '@valueURI' => self::RELATIONSHIP_VALUE_URI,
        '@' => self::RELATIONSHIP_VALUE,
    ];

    /**
     * @var array relationshipSubType
     */
    public $relationshipSubType = [
        '@authority' => self::RELATIONSHIP_SUB_AUTHORITY,
        '@authorityURI' => self::RELATIONSHIP_SUB_AUTHORITY_URI,
        '@valueURI' => self::RELATIONSHIP_SUB_VALUE_URI,
        '@' => self::RELATIONSHIP_SUB_VALUE,
    ];

    /**
     * @var array relationship [
     *                          'relatedObjectIdentifierType' => 'local',
     *                          'relatedObjectIdentifierValue' => 'Archives:1'
     *                         ]
     */
    public $relationship;

    /**
     * File constructor.
     * @param string      $format          formatName
     * @param string      $identifierType
     * @param string|null $identifierValue
     */
    public function __construct(
        string $format = 'unknown',
        string $identifierType = 'UUID',
        string $identifierValue = null
    ) {
        $this->identifierType = $identifierType;
        $this->identifierValue = $identifierValue ?: Text::uuid();
        $this->formatName = $format;
    }

    /**
     * Crée une instance à partir d'un fichier sur filesystem
     * @param string      $uri
     * @param string      $identifierType
     * @param string|null $identifierValue
     * @return File
     * @throws Exception
     */
    public static function fromFile(
        string $uri,
        string $identifierType = 'UUID',
        string $identifierValue = null
    ): File {
        if (!is_readable($uri)) {
            throw new Exception(sprintf('unable to read "%s" file', h($uri)));
        }
        $object = new static('unknown', $identifierType, $identifierValue);
        $object->messageDigest = hash_file(
            static::hashAlgoToPhp($object->messageDigestAlgorithm['@']),
            $uri
        );
        $object->size = filesize($uri);

        $puids = FileValidator::getPuid($uri);
        if ($puids && isset($puids[0]['id']) && $puids[0]['id'] !== 'UNKNOWN') {
            $object->formatName = $puids[0]['format'];
            $object->formatRegistryName = $puids[0]['ns'];
            $object->formatRegistryKey = $puids[0]['id'];
        } else {
            $object->formatName = 'UNKNOWN';
        }
        $object->originalName = basename($uri);
        return $object;
    }

    /**
     * Transforme "SHA-256" en "sha256" utilisable par la fonction hash() (php)
     * @param string $algo
     * @return string
     */
    public static function hashAlgoToPhp(string $algo): string
    {
        return strtolower(str_replace('-', '', $algo));
    }

    /**
     * Transforme l'objet en \DOMElement
     * @return DOMElement
     * @throws DOMException
     */
    public function renderElement(): DOMElement
    {
        $dom = new DOMDocument;

        $object = $dom->createElementNS(Premis::NS_PREMIS, 'object');
        $objectIdentifier = $dom->createElementNS(Premis::NS_PREMIS, 'objectIdentifier');
        $objectIdentifierType = $dom->createElementNS(
            Premis::NS_PREMIS,
            'objectIdentifierType'
        );
        $objectIdentifierType->appendChild(DOMUtility::createDomTextNode($dom, $this->identifierType));
        $objectIdentifierValue = $dom->createElementNS(
            Premis::NS_PREMIS,
            'objectIdentifierValue'
        );
        $objectIdentifierValue->appendChild(DOMUtility::createDomTextNode($dom, $this->identifierValue));
        $objectCharacteristics = $dom->createElementNS(
            Premis::NS_PREMIS,
            'objectCharacteristics'
        );
        $format = $dom->createElementNS(Premis::NS_PREMIS, 'format');
        $formatDesignation = $dom->createElementNS(
            Premis::NS_PREMIS,
            'formatDesignation'
        );
        $formatName = $dom->createElementNS(
            Premis::NS_PREMIS,
            'formatName'
        );
        $formatName->appendChild(DOMUtility::createDomTextNode($dom, $this->formatName));

        $object->setAttributeNS(Premis::NS_XSI, 'xsi:type', 'premis:file');
        $object->appendChild($objectIdentifier);
        $objectIdentifier->appendChild($objectIdentifierType);
        $objectIdentifier->appendChild($objectIdentifierValue);
        $object->appendChild($objectCharacteristics);

        // objectCharacteristics / fixity
        if ($this->messageDigest) {
            $fixity = $dom->createElementNS(Premis::NS_PREMIS, 'fixity');
            $messageDigestAlgorithm = $dom->createElementNS(
                Premis::NS_PREMIS,
                'messageDigestAlgorithm'
            );
            $messageDigest = $dom->createElementNS(
                Premis::NS_PREMIS,
                'messageDigest'
            );
            foreach ($this->messageDigestAlgorithm as $key => $value) {
                $key = ltrim($key, '@');
                if ($key) {
                    $messageDigestAlgorithm->setAttribute($key, $value);
                } else {
                    DOMUtility::setValue($messageDigestAlgorithm, $value);
                }
            }
            DOMUtility::setValue($messageDigest, $this->messageDigest);
            $fixity->appendChild($messageDigestAlgorithm);
            $fixity->appendChild($messageDigest);
            $objectCharacteristics->appendChild($fixity);
        }
        // objectCharacteristics / size
        if ($this->size) {
            $size = $dom->createElementNS(Premis::NS_PREMIS, 'size');
            DOMUtility::setValue($size, $this->size);
            $objectCharacteristics->appendChild($size);
        }
        $formatDesignation->appendChild($formatName);
        $format->appendChild($formatDesignation);
        // objectCharacteristics / format
        if ($this->formatRegistryName) {
            $formatRegistry = $dom->createElementNS(Premis::NS_PREMIS, 'formatRegistry');
            $formatRegistryName = $dom->createElementNS(
                Premis::NS_PREMIS,
                'formatRegistryName'
            );
            $formatRegistryName->appendChild(DOMUtility::createDomTextNode($dom, $this->formatRegistryName));
            $formatRegistryKey = $dom->createElementNS(
                Premis::NS_PREMIS,
                'formatRegistryKey'
            );
            $formatRegistryKey->appendChild(DOMUtility::createDomTextNode($dom, $this->formatRegistryKey));
            $formatRegistry->appendChild($formatRegistryName);
            $formatRegistry->appendChild($formatRegistryKey);
            $format->appendChild($formatRegistry);
        }
        $objectCharacteristics->appendChild($format);

        // originalName
        if ($this->originalName) {
            $originalName = $dom->createElementNS(
                Premis::NS_PREMIS,
                'originalName'
            );
            $originalName->appendChild(DOMUtility::createDomTextNode($dom, $this->originalName));
            $object->appendChild($originalName);
        }

        // storage
        foreach ($this->storages as $storage) {
            $contentLocationType = $dom->createElementNS(
                Premis::NS_PREMIS,
                'contentLocationType'
            );
            $contentLocationType->appendChild(DOMUtility::createDomTextNode($dom, $storage['contentLocationType']));
            $contentLocationValue = $dom->createElementNS(
                Premis::NS_PREMIS,
                'contentLocationValue'
            );
            $contentLocationValue->appendChild(DOMUtility::createDomTextNode($dom, $storage['contentLocationValue']));
            $contentLocation = $dom->createElementNS(
                Premis::NS_PREMIS,
                'contentLocation'
            );
            $contentLocation->appendChild($contentLocationType);
            $contentLocation->appendChild($contentLocationValue);
            $storageMedium = $dom->createElementNS(
                Premis::NS_PREMIS,
                'storageMedium'
            );
            $storageMedium->appendChild(DOMUtility::createDomTextNode($dom, $storage['storageMedium']));
            $storageElement = $dom->createElementNS(
                Premis::NS_PREMIS,
                'storage'
            );
            $storageElement->appendChild($contentLocation);
            $storageElement->appendChild($storageMedium);
            $object->appendChild($storageElement);
        }

        // relationship
        if ($this->relationship) {
            $relationshipType = $dom->createElementNS(
                Premis::NS_PREMIS,
                'relationshipType'
            );
            foreach ($this->relationshipType as $key => $value) {
                $key = ltrim($key, '@');
                if ($key) {
                    $relationshipType->setAttribute($key, $value);
                } else {
                    DOMUtility::setValue($relationshipType, $value);
                }
            }
            $relationshipSubType = $dom->createElementNS(
                Premis::NS_PREMIS,
                'relationshipSubType'
            );
            foreach ($this->relationshipSubType as $key => $value) {
                $key = ltrim($key, '@');
                if ($key) {
                    $relationshipSubType->setAttribute($key, $value);
                } else {
                    DOMUtility::setValue($relationshipSubType, $value);
                }
            }
            $relatedObjectIdentifierType = $dom->createElementNS(
                Premis::NS_PREMIS,
                'relatedObjectIdentifierType'
            );
            $relatedObjectIdentifierType->appendChild(
                DOMUtility::createDomTextNode($dom, $this->relationship['relatedObjectIdentifierType'])
            );
            $relatedObjectIdentifierValue = $dom->createElementNS(
                Premis::NS_PREMIS,
                'relatedObjectIdentifierValue'
            );
            $relatedObjectIdentifierValue->appendChild(
                DOMUtility::createDomTextNode($dom, $this->relationship['relatedObjectIdentifierValue'])
            );
            $relatedObjectIdentifier = $dom->createElementNS(
                Premis::NS_PREMIS,
                'relatedObjectIdentifier'
            );
            $relatedObjectIdentifier->appendChild($relatedObjectIdentifierType);
            $relatedObjectIdentifier->appendChild($relatedObjectIdentifierValue);
            $relationship = $dom->createElementNS(
                Premis::NS_PREMIS,
                'relationship'
            );
            $relationship->appendChild($relationshipType);
            $relationship->appendChild($relationshipSubType);
            $relationship->appendChild($relatedObjectIdentifier);
            $object->appendChild($relationship);
        }

        return $object;
    }

    /**
     * Créé un File à partir d'un \DOMElement
     * @param DOMElement $element
     * @return File
     */
    public static function loadElement(DOMElement $element): File
    {
        /**
         * @var DOMElement $objectIdentifier
         * @var DOMElement $format
         * @var DOMElement $objectCharacteristics
         * @var DOMElement $formatDesignation
         * @var DOMElement $fixity
         * @var DOMElement $formatRegistry
         * @var DOMElement $contentLocation
         * @var DOMElement $relationship
         * @var DOMElement $relatedObjectIdentifier
         */
        $objectIdentifier = $element->getElementsByTagName('objectIdentifier')->item(0);
        $objectIdentifierType = $objectIdentifier
            ->getElementsByTagName('objectIdentifierType')
            ->item(0)
            ->nodeValue;
        $objectIdentifierValue = $objectIdentifier
            ->getElementsByTagName('objectIdentifierValue')
            ->item(0)
            ->nodeValue;

        $objectCharacteristics = $element->getElementsByTagName('objectCharacteristics')->item(0);
        $format = $objectCharacteristics->getElementsByTagName('format')->item(0);
        $formatDesignation = $format->getElementsByTagName('formatDesignation')->item(0);
        $formatName = $formatDesignation->getElementsByTagName('formatName')->item(0);
        $file = new File($formatName->nodeValue, $objectIdentifierType, $objectIdentifierValue);

        $fixity = $objectCharacteristics->getElementsByTagName('fixity')->item(0);
        if ($fixity) {
            $messageDigestAlgorithm = $fixity->getElementsByTagName('messageDigestAlgorithm')->item(0);
            /** @var DOMAttr $attr */
            foreach ($messageDigestAlgorithm->attributes as $attr) {
                $file->messageDigestAlgorithm['@'.$attr->nodeName] = $attr->nodeValue;
            }
            $file->messageDigestAlgorithm['@'] = $messageDigestAlgorithm->nodeValue;

            $messageDigest = $fixity->getElementsByTagName('messageDigest')->item(0);
            $file->messageDigest = $messageDigest->nodeValue;
        }
        $size = $objectCharacteristics->getElementsByTagName('size')->item(0);
        if ($size) {
            $file->size = $size->nodeValue;
        }
        $formatRegistry = $format->getElementsByTagName('formatRegistry')->item(0);
        if ($formatRegistry) {
            $formatRegistryName = $formatRegistry->getElementsByTagName('formatRegistryName')
                ->item(0)
                ->nodeValue;
            $formatRegistryKey = $formatRegistry->getElementsByTagName('formatRegistryKey')
                ->item(0)
                ->nodeValue;
            $file->formatRegistryKey = $formatRegistryKey;
            $file->formatRegistryName = $formatRegistryName;
        }
        $originalName = $element->getElementsByTagName('originalName')->item(0);
        if ($originalName) {
            $file->originalName = $originalName->nodeValue;
        }
        foreach ($element->getElementsByTagName('storage') as $storage) {
            if (!$storage instanceof DOMElement) {
                continue;
            }
            $contentLocation = $storage->getElementsByTagName('contentLocation')->item(0);
            $contentLocationType = $contentLocation->getElementsByTagName('contentLocationType')
                ->item(0)
                ->nodeValue;
            $contentLocationValue = $contentLocation->getElementsByTagName('contentLocationValue')
                ->item(0)
                ->nodeValue;
            $storageMedium = $storage->getElementsByTagName('storageMedium')
                ->item(0)
                ->nodeValue;
            $file->storages[] = [
                'contentLocationType' => $contentLocationType,
                'contentLocationValue' => $contentLocationValue,
                'storageMedium' => $storageMedium
            ];
        }
        $relationship = $element->getElementsByTagName('relationship')->item(0);
        if ($relationship) {
            $relationshipType = $relationship->getElementsByTagName('relationshipType')->item(0);
            /** @var DOMAttr $attr */
            foreach ($relationshipType->attributes as $attr) {
                $file->relationshipType['@'.$attr->nodeName] = $attr->nodeValue;
            }
            $file->relationshipType['@'] = $relationshipType->nodeValue;
            $relationshipSubType = $relationship->getElementsByTagName('relationshipSubType')->item(0);
            /** @var DOMAttr $attr */
            foreach ($relationshipSubType->attributes as $attr) {
                $file->relationshipSubType['@'.$attr->nodeName] = $attr->nodeValue;
            }
            $file->relationshipSubType['@'] = $relationshipSubType->nodeValue;

            $relatedObjectIdentifier = $relationship
                ->getElementsByTagName('relatedObjectIdentifier')
                ->item(0);
            $relatedObjectIdentifierType = $relatedObjectIdentifier
                ->getElementsByTagName('relatedObjectIdentifierType')
                ->item(0)
                ->nodeValue;
            $relatedObjectIdentifierValue = $relatedObjectIdentifier
                ->getElementsByTagName('relatedObjectIdentifierValue')
                ->item(0)
                ->nodeValue;
            $file->relationship = [
                'relatedObjectIdentifierType' => $relatedObjectIdentifierType,
                'relatedObjectIdentifierValue' => $relatedObjectIdentifierValue,
            ];
        }

        return $file;
    }

    /**
     * Donne la valeur string du rendu xml
     * @return string
     * @throws DOMException
     */
    public function __toString()
    {
        $element = $this->renderElement();
        return $element->ownerDocument->saveXML($element);
    }

    /**
     * Donne l'identifiant de l'objet
     * @return string
     */
    public function getIdentifierValue(): string
    {
        return $this->identifierValue;
    }
}
