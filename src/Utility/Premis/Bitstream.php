<?php
/**
 * AsalaeCore\Utility\Premis\Bitstream
 */

namespace AsalaeCore\Utility\Premis;

use DOMElement;

/**
 * Objet bitstream premis v3.0
 *
 * A Bitstream is contiguous or non-contiguous data within a file that has
 * meaningful common properties for preservation purposes.
 * A Bitstream cannot be transformed into a standalone file without the addition
 * of file structure (headers, etc.) and/or reformatting the Bitstream to comply
 * with some particular file format
 *
 * @category Utility
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Bitstream implements ObjectInterface
{
    /**
     * @inheritDoc
     */
    public function renderElement(): DOMElement
    {
        return new DOMElement;
    }

    /**
     * Donne l'identifiant de l'objet
     * @return string
     */
    public function getIdentifierValue(): string
    {
        return '';
    }
}
