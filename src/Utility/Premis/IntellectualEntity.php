<?php
/**
 * AsalaeCore\Utility\Premis\IntellectualEntity
 */

namespace AsalaeCore\Utility\Premis;

use AsalaeCore\Utility\DOMUtility;
use AsalaeCore\Utility\Premis;
use Cake\Utility\Text;
use DOMDocument;
use DOMElement;
use DOMException;

/**
 * Objet intellectualEntity premis v3.0
 *
 * An Intellectual Entity is a distinct intellectual or artistic creation that
 * is considered relevant to a designated community in the context of digital
 * preservation: for example, a particular book, map, photograph, database, or
 * hardware or software. An Intellectual Entity can include other
 * Intellectual Entities;
 * for example, a web site can include a web page and a web page can include an
 * image. An Intellectual Entity may have one or more digital or non-digital
 * Representations.
 *
 * @category Utility
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class IntellectualEntity implements ObjectInterface, PremisElementInterface
{
    /**
     * @var string objectIdentifierType obligatoire
     */
    public $identifierType;

    /**
     * @var string objectIdentifierValue obligatoire
     */
    public $identifierValue;

    /**
     * @var string originalName
     */
    public $originalName;

    /**
     * @var array significantProperties
     */
    public $significantProperties = [];

    /**
     * IntellectualEntity constructor.
     * @param string      $identifierType
     * @param string|null $identifierValue
     */
    public function __construct(string $identifierType = 'UUID', string $identifierValue = null)
    {
        $this->identifierType = $identifierType;
        $this->identifierValue = $identifierValue ?: Text::uuid();
    }

    /**
     * Transforme l'objet en \DOMElement
     * @return DOMElement
     * @throws DOMException
     */
    public function renderElement(): DOMElement
    {
        $dom = new DOMDocument;

        $object = $dom->createElementNS(
            Premis::NS_PREMIS,
            'object'
        );
        $objectIdentifier = $dom->createElementNS(
            Premis::NS_PREMIS,
            'objectIdentifier'
        );
        $objectIdentifierType = $dom->createElementNS(
            Premis::NS_PREMIS,
            'objectIdentifierType'
        );
        $objectIdentifierType->appendChild(DOMUtility::createDomTextNode($dom, $this->identifierType));
        $objectIdentifierValue = $dom->createElementNS(
            Premis::NS_PREMIS,
            'objectIdentifierValue'
        );
        $objectIdentifierValue->appendChild(DOMUtility::createDomTextNode($dom, $this->identifierValue));
        $objectIdentifier->appendChild($objectIdentifierType);
        $objectIdentifier->appendChild($objectIdentifierValue);
        $object->appendChild($objectIdentifier);
        $object->setAttributeNS(
            Premis::NS_XSI,
            'xsi:type',
            'premis:intellectualEntity'
        );

        // significantProperties
        foreach ($this->significantProperties as $type => $value) {
            $significantProperties = $dom->createElementNS(
                Premis::NS_PREMIS,
                'significantProperties'
            );
            $significantPropertiesType = $dom->createElementNS(
                Premis::NS_PREMIS,
                'significantPropertiesType'
            );
            $significantPropertiesType->appendChild(DOMUtility::createDomTextNode($dom, $type));
            $significantPropertiesValue = $dom->createElementNS(
                Premis::NS_PREMIS,
                'significantPropertiesValue'
            );
            $significantPropertiesValue->appendChild(DOMUtility::createDomTextNode($dom, $value));
            $significantProperties->appendChild($significantPropertiesType);
            $significantProperties->appendChild($significantPropertiesValue);
            $object->appendChild($significantProperties);
        }
        // originalName
        if ($this->originalName) {
            $originalName = $dom->createElementNS(
                Premis::NS_PREMIS,
                'originalName'
            );
            $originalName->appendChild(DOMUtility::createDomTextNode($dom, $this->originalName));
            $object->appendChild($originalName);
        }

        return $object;
    }

    /**
     * Créé un IntellectualEntity à partir d'un \DOMElement
     * @param DOMElement $element
     * @return IntellectualEntity
     */
    public static function loadElement(DOMElement $element): IntellectualEntity
    {
        /** @var DOMElement $objectIdentifier */
        $objectIdentifier = $element->getElementsByTagName('objectIdentifier')->item(0);
        $objectIdentifierType = $objectIdentifier
            ->getElementsByTagName('objectIdentifierType')
            ->item(0)
            ->nodeValue;
        $objectIdentifierValue = $objectIdentifier
            ->getElementsByTagName('objectIdentifierValue')
            ->item(0)
            ->nodeValue;
        $intellectualEntity = new IntellectualEntity($objectIdentifierType, $objectIdentifierValue);

        foreach ($element->getElementsByTagName('significantProperties') as $significantProperties) {
            if (!$significantProperties instanceof DOMElement) {
                continue;
            }
            $type = $significantProperties->getElementsByTagName('significantPropertiesType')
                ->item(0)
                ->nodeValue;
            $value = $significantProperties->getElementsByTagName('significantPropertiesValue')
                ->item(0)
                ->nodeValue;
            $intellectualEntity->significantProperties[$type] = $value;
        }

        $originalName = $element->getElementsByTagName('originalName')->item(0);
        if ($originalName) {
            $intellectualEntity->originalName = $originalName->nodeValue;
        }

        return $intellectualEntity;
    }

    /**
     * Donne la valeur string du rendu xml
     * @return string
     * @throws DOMException
     */
    public function __toString()
    {
        $element = $this->renderElement();
        return $element->ownerDocument->saveXML($element);
    }

    /**
     * Donne l'identifiant de l'objet
     * @return string
     */
    public function getIdentifierValue(): string
    {
        return $this->identifierValue;
    }
}
