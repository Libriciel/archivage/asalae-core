<?php
/**
 * AsalaeCore\Utility\Premis\ObjectInterface
 */

namespace AsalaeCore\Utility\Premis;

use DOMElement;

/**
 * Objet premis v3.0
 *
 * @category Utility
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
interface ObjectInterface
{
    /**
     * Donne l'objet sous forme de DOMElement
     * @return DOMElement
     */
    public function renderElement(): DOMElement;

    /**
     * Donne l'identifiant de l'objet
     * @return string
     */
    public function getIdentifierValue(): string;
}
