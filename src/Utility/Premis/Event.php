<?php
/**
 * AsalaeCore\Utility\Premis\Event
 */

namespace AsalaeCore\Utility\Premis;

use AsalaeCore\Utility\DOMUtility;
use AsalaeCore\Utility\Premis;
use Cake\I18n\FrozenTime as Time;
use Cake\Utility\Text;
use DOMDocument;
use DOMElement;
use DOMException;
use Exception;

/**
 * Event premis v3.0
 *
 * @category Utility
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Event implements PremisElementInterface
{
    /**
     * @var string objectIdentifierType obligatoire
     */
    public $identifierType;

    /**
     * @var string objectIdentifierValue obligatoire
     */
    public $identifierValue;

    /**
     * @var string eventType obligatoire
     */
    public $type;

    /**
     * @var string eventDateTime obligatoire
     */
    public $dateTime;

    /**
     * @var string eventDetailInformation
     */
    public $detail;

    /**
     * @var string eventOutcome
     */
    public $outcome;

    /**
     * @var string eventOutcomeDetail
     */
    public $outcomeDetail;

    /**
     * @var ObjectInterface[]|File[]|IntellectualEntity[] objects
     */
    public $objects = [];

    /**
     * @var Agent[] agents
     */
    public $agents = [];

    /**
     * Event constructor.
     * @param string      $type            eventType
     * @param string      $identifierType
     * @param string|null $identifierValue
     */
    public function __construct(string $type, string $identifierType = 'UUID', string $identifierValue = null)
    {
        $this->identifierType = $identifierType;
        $this->identifierValue = $identifierValue ?: Text::uuid();
        $this->dateTime = (new Time())->format(DATE_RFC3339);
        $this->type = $type;
    }

    /**
     * Ajoute un objet à l'evenement
     * @param ObjectInterface $object objectComplexType
     * @return Event
     */
    public function addObject(ObjectInterface $object): self
    {
        $this->objects[] = $object;
        return $this;
    }

    /**
     * Ajoute un agent à l'evenement
     * @param Agent $agent agentComplexType
     * @return Event
     */
    public function addAgent(Agent $agent): self
    {
        $this->agents[] = $agent;
        return $this;
    }

    /**
     * Transforme l'objet en \DOMElement
     * @return DOMElement
     * @throws DOMException
     */
    public function renderElement(): DOMElement
    {
        $dom = new DOMDocument;
        $dom->formatOutput = true;
        $dom->preserveWhiteSpace = false;

        $event = $dom->createElementNS(Premis::NS_PREMIS, 'event');
        $eventIdentifier = $dom->createElementNS(Premis::NS_PREMIS, 'eventIdentifier');
        $eventIdentifierType = $dom->createElementNS(
            Premis::NS_PREMIS,
            'eventIdentifierType'
        );
        $eventIdentifierType->appendChild(DOMUtility::createDomTextNode($dom, $this->identifierType));
        $eventIdentifierValue = $dom->createElementNS(
            Premis::NS_PREMIS,
            'eventIdentifierValue'
        );
        $eventIdentifierValue->appendChild(DOMUtility::createDomTextNode($dom, $this->identifierValue));
        $eventType = $dom->createElementNS(Premis::NS_PREMIS, 'eventType');
        $eventType->appendChild(DOMUtility::createDomTextNode($dom, $this->type));
        $eventDateTime = $dom->createElementNS(
            Premis::NS_PREMIS,
            'eventDateTime'
        );
        $eventDateTime->appendChild(DOMUtility::createDomTextNode($dom, $this->dateTime));
        $eventIdentifier->appendChild($eventIdentifierType);
        $eventIdentifier->appendChild($eventIdentifierValue);
        $event->appendChild($eventIdentifier);
        $event->appendChild($eventType);
        $event->appendChild($eventDateTime);

        if ($this->detail) {
            $eventDetailInformation = $dom->createElementNS(
                Premis::NS_PREMIS,
                'eventDetailInformation'
            );
            $eventDetail = $dom->createElementNS(
                Premis::NS_PREMIS,
                'eventDetail'
            );
            $eventDetail->appendChild(DOMUtility::createDomTextNode($dom, $this->detail));
            $eventDetailInformation->appendChild($eventDetail);
            $event->appendChild($eventDetailInformation);
        }
        if ($this->outcome) {
            $eventOutcomeInformation = $dom->createElementNS(
                Premis::NS_PREMIS,
                'eventOutcomeInformation'
            );
            $eventOutcome = $dom->createElementNS(
                Premis::NS_PREMIS,
                'eventOutcome'
            );
            $eventOutcome->appendChild(DOMUtility::createDomTextNode($dom, $this->outcome));
            $eventOutcomeInformation->appendChild($eventOutcome);
            if ($this->outcomeDetail) {
                $eventOutcomeDetail = $dom->createElementNS(
                    Premis::NS_PREMIS,
                    'eventOutcomeDetail'
                );
                $eventOutcomeDetailNote = $dom->createElementNS(
                    Premis::NS_PREMIS,
                    'eventOutcomeDetailNote'
                );
                $eventOutcomeDetailNote->appendChild(DOMUtility::createDomTextNode($dom, $this->outcomeDetail));
                $eventOutcomeDetail->appendChild($eventOutcomeDetailNote);
                $eventOutcomeInformation->appendChild($eventOutcomeDetail);
            }
            $event->appendChild($eventOutcomeInformation);
        }

        foreach ($this->agents as $agent) {
            $linkingAgentIdentifier = $dom->createElementNS(
                Premis::NS_PREMIS,
                'linkingAgentIdentifier'
            );
            $linkingAgentIdentifierType = $dom->createElementNS(
                Premis::NS_PREMIS,
                'linkingAgentIdentifierType'
            );
            $linkingAgentIdentifierType->appendChild(DOMUtility::createDomTextNode($dom, $agent->identifierType));
            $linkingAgentIdentifierValue = $dom->createElementNS(
                Premis::NS_PREMIS,
                'linkingAgentIdentifierValue'
            );
            $linkingAgentIdentifierValue->appendChild(DOMUtility::createDomTextNode($dom, $agent->identifierValue));
            $linkingAgentIdentifier->appendChild($linkingAgentIdentifierType);
            $linkingAgentIdentifier->appendChild($linkingAgentIdentifierValue);
            $event->appendChild($linkingAgentIdentifier);
        }

        foreach ($this->objects as $object) {
            $linkingObjectIdentifier = $dom->createElementNS(
                Premis::NS_PREMIS,
                'linkingObjectIdentifier'
            );
            $linkingObjectIdentifierType = $dom->createElementNS(
                Premis::NS_PREMIS,
                'linkingObjectIdentifierType'
            );
            $linkingObjectIdentifierType->appendChild(DOMUtility::createDomTextNode($dom, $object->identifierType));
            $linkingObjectIdentifierValue = $dom->createElementNS(
                Premis::NS_PREMIS,
                'linkingObjectIdentifierValue'
            );
            $linkingObjectIdentifierValue->appendChild(DOMUtility::createDomTextNode($dom, $object->identifierValue));
            $linkingObjectIdentifier->appendChild($linkingObjectIdentifierType);
            $linkingObjectIdentifier->appendChild($linkingObjectIdentifierValue);
            $event->appendChild($linkingObjectIdentifier);
        }

        return $event;
    }

    /**
     * Créé un Event à partir d'un \DOMElement
     * @param DOMElement $element
     * @return Event
     * @throws Exception
     */
    public static function loadElement(DOMElement $element): Event
    {
        /**
         * @var DOMElement $eventIdentifier
         * @var DOMElement $eventDetailInformation
         * @var DOMElement $eventOutcomeInformation
         * @var DOMElement $eventOutcomeDetail
         */
        $eventIdentifier = $element->getElementsByTagName('eventIdentifier')->item(0);
        $eventIdentifierType = $eventIdentifier
            ->getElementsByTagName('eventIdentifierType')
            ->item(0)
            ->nodeValue;
        $eventIdentifierValue = $eventIdentifier
            ->getElementsByTagName('eventIdentifierValue')
            ->item(0)
            ->nodeValue;
        $eventType = $element->getElementsByTagName('eventType')
            ->item(0)
            ->nodeValue;
        $event = new Event($eventType, $eventIdentifierType, $eventIdentifierValue);

        $eventDateTime = $element->getElementsByTagName('eventDateTime')
            ->item(0)
            ->nodeValue;
        $event->dateTime = $eventDateTime;

        $eventDetailInformation = $element->getElementsByTagName('eventDetailInformation')
            ->item(0);
        if ($eventDetailInformation) {
            $eventDetail = $eventDetailInformation->getElementsByTagName('eventDetail')
                ->item(0)
                ->nodeValue;
            $event->detail = $eventDetail;
        }

        $eventOutcomeInformation = $element->getElementsByTagName('eventOutcomeInformation')
            ->item(0);
        if ($eventOutcomeInformation) {
            $eventOutcomeDetail = $eventOutcomeInformation->getElementsByTagName('eventOutcomeDetail')
                ->item(0);
            $eventOutcome = $eventOutcomeInformation->getElementsByTagName('eventOutcome')
                ->item(0)
                ->nodeValue;
            $event->outcome = $eventOutcome;

            $eventOutcomeDetailNote = $eventOutcomeDetail
                ? $eventOutcomeDetail->getElementsByTagName('eventOutcomeDetailNote')->item(0)
                : null;
            if ($eventOutcomeDetailNote) {
                $event->outcomeDetail = $eventOutcomeDetailNote->nodeValue;
            }
        }

        foreach ($element->getElementsByTagName('linkingAgentIdentifier') as $linkingAgentIdentifier) {
            if (!$linkingAgentIdentifier instanceof DOMElement) {
                continue;
            }
            $linkingAgentIdentifierType = $linkingAgentIdentifier
                ->getElementsByTagName('linkingAgentIdentifierType')
                ->item(0)
                ->nodeValue;
            $linkingAgentIdentifierValue = $linkingAgentIdentifier
                ->getElementsByTagName('linkingAgentIdentifierValue')
                ->item(0)
                ->nodeValue;
            $agent = Agent::find($linkingAgentIdentifierType, $linkingAgentIdentifierValue, $element->ownerDocument);
            $event->agents[] = $agent ?: new Agent($linkingAgentIdentifierType, $linkingAgentIdentifierValue);
        }

        foreach ($element->getElementsByTagName('linkingObjectIdentifier') as $linkingObjectIdentifier) {
            if (!$linkingObjectIdentifier instanceof DOMElement) {
                continue;
            }
            $linkingObjectIdentifierType = $linkingObjectIdentifier
                ->getElementsByTagName('linkingObjectIdentifierType')
                ->item(0)
                ->nodeValue;
            $linkingObjectIdentifierValue = $linkingObjectIdentifier
                ->getElementsByTagName('linkingObjectIdentifierValue')
                ->item(0)
                ->nodeValue;
            $object = Agent::find(
                $linkingObjectIdentifierType,
                $linkingObjectIdentifierValue,
                $element->ownerDocument
            );
            $event->objects[] = $object ?: new Agent($linkingObjectIdentifierType, $linkingObjectIdentifierValue);
        }

        return $event;
    }

    /**
     * Donne la valeur string du rendu xml
     * @return string
     * @throws DOMException
     */
    public function __toString()
    {
        $element = $this->renderElement();
        return $element->ownerDocument->saveXML($element);
    }
}
