<?php
/**
 * AsalaeCore\Utility\Antivirus\AntivirusInterface
 */

namespace AsalaeCore\Utility\Antivirus;

/**
 * Interface des utilitaires d'antivirus
 *
 * @category Utility
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
interface AntivirusInterface
{
    /**
     * Effectue un scan sur un fichier/dossier et renvoi la liste des fichiers
     * infectés (clé) et le nom du virus (valeur)
     * @param string $directory
     * @return array ['/path/to/virus/file.txt' => 'Eicar-Test-Signature']
     */
    public static function scan(string $directory): array;

    /**
     * Permet de vérifier que le service est Disponible
     * @return bool
     */
    public static function ping(): bool;
}
