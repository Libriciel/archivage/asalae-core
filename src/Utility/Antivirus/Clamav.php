<?php
/**
 * AsalaeCore\Utility\Antivirus\Clamav
 */

namespace AsalaeCore\Utility\Antivirus;

use Cake\Core\Configure;
use Exception;

/**
 * Antivirus clamav
 *
 * @category Utility
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Clamav implements AntivirusInterface
{
    /**
     * Effectue un scan sur un fichier/dossier et renvoi la liste des fichiers
     * infectés (clé) et le nom du virus (valeur)
     * @param string $directory
     * @return array ['/path/to/virus/file.txt' => 'Eicar-Test-Signature']
     * @throws Exception
     */
    public static function scan(string $directory): array
    {
        $host = Configure::read('Antivirus.host', 'localhost');
        $port = Configure::read('Antivirus.port', 3310);
        $resource = self::connect($host, $port);
        $command = sprintf("MULTISCAN %s", $directory);
        $response = fwrite($resource, $command);
        if (!$response) {
            throw new Exception(sprintf('Failed to send command %s to server', $command));
        }
        $report = [];
        while (!feof($resource)) {
            $output = trim(fread($resource, 8192));
            self::parseOutput($output, $report);
        }
        fclose($resource);
        return $report;
    }

    /**
     * Interprète la réponse
     * @param string $output
     * @param array  $report
     * @return void
     */
    private static function parseOutput(string $output, array &$report)
    {
        if (preg_match('/^(.+): (.+) ([A-Z]+)$/', $output, $m)) {
            [, $filename, $message, $verb] = $m;
            switch ($verb) {
                case 'FOUND':
                    $report[$filename] = $message;
                    break;
                case 'ERROR':
                default:
                    if (Configure::read('Antivirus.trigger_error', true)) {
                        trigger_error(
                            sprintf(
                                '%s clamav scan file %s (%s)',
                                $verb,
                                $filename,
                                $message
                            )
                        );
                    }
            }
        }
    }

    /**
     * Connection sur le socket
     * @param string $host
     * @param int    $port
     * @return resource
     * @throws Exception
     */
    private static function connect(string $host, int $port)
    {
        $resource = @stream_socket_client(
            sprintf('tcp://%s:%d', $host, $port),
            $errno,
            $errstr,
            5
        );
        if (!$resource) {
            throw new Exception(
                sprintf(
                    'Unable to connect to %s on port %s: [%s: %s]',
                    $host,
                    $port,
                    $errno,
                    $errstr
                )
            );
        }
        return $resource;
    }

    /**
     * Permet de vérifier que le service est Disponible
     * @return bool
     */
    public static function ping(): bool
    {
        $host = Configure::read('Antivirus.host', 'localhost');
        $port = Configure::read('Antivirus.port', 3310);
        try {
            self::connect($host, $port);
            return true;
        } catch (Exception $e) {
            return false;
        }
    }
}
