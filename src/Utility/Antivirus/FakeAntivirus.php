<?php
/**
 * AsalaeCore\Utility\Antivirus\FakeAntivirus
 */

namespace AsalaeCore\Utility\Antivirus;

use Exception;

/**
 * Antivirus de test/debug
 *
 * @category Utility
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class FakeAntivirus implements AntivirusInterface
{
    /**
     * Effectue un scan sur un fichier/dossier et renvoi la liste des fichiers
     * infectés (clé) et le nom du virus (valeur)
     * @param string $directory
     * @return array ['/path/to/virus/file.txt' => 'Eicar-Test-Signature']
     * @throws Exception
     */
    public static function scan(string $directory): array
    {
        $report = [];
        if (is_file($directory)
            && filesize($directory) === 69
            && hash_file('md5', $directory) === '69630e4574ec6798239b091cda43dca0'
        ) {
            return [$directory => 'Eicar-Test-Signature'];
        }
        foreach (glob($directory.DS.'*') as $dir) {
            if (is_file($dir)) {
                if (filesize($dir) === 69
                    && hash_file('md5', $dir) === '69630e4574ec6798239b091cda43dca0'
                ) {
                    $report[$dir] = 'Eicar-Test-Signature';
                }
            } elseif (is_dir($dir) && ($sub = self::scan($dir))) {
                $report = array_merge($report, $sub);
            }
        }
        return $report;
    }

    /**
     * Permet de vérifier que le service est Disponible
     * @return bool
     */
    public static function ping(): bool
    {
        return true;
    }
}
