<?php
/**
 * AsalaeCore\Utility\Parallel
 */

namespace AsalaeCore\Utility;

use AsalaeCore\Command\ParallelCommand;
use AsalaeCore\Exception\GenericException;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;

/**
 * Lance des commandes de façon asynchrone
 *
 * @package AsalaeCore\Utility
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2022, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Parallel
{
    /**
     * @var array Processes en parallèles
     */
    public array $threads;
    /**
     * @var int Nombre secondes max par process (0 = infini)
     */
    public int $timeout;
    /**
     * @var bool arret au 1er échec
     */
    public bool $stop_on_failure;
    /**
     * @var bool kill les autres processes en cas d'échec
     */
    public bool $kill_on_failure;
    /**
     * @var array résultats d'éxécutions
     */
    private array $results = [];

    /**
     * Constructeur
     * @param array $params threads, timeout, stop_on_failure
     * @throws Exception
     */
    public function __construct(array $params = [])
    {
        $this->threads = array_fill(
            0,
            $params['threads'] ?? trim(exec('getconf _NPROCESSORS_ONLN')),
            null
        );
        $this->timeout = $params['timeout'] ?? 0;
        $this->stop_on_failure = $params['stop_on_failure'] ?? false;
        $this->kill_on_failure = $params['kill_on_failure'] ?? false;
        $basedir = sys_get_temp_dir() . DS . 'as_parallel' . DS . getmypid() . DS;
        if (is_dir($basedir)) {
            Filesystem::remove($basedir);
        }
    }

    /**
     * Destructeur de classe, détruit les processes en cours et supprime les
     * fichiers des résultats
     */
    public function __destruct()
    {
        foreach ($this->threads as $value) {
            if (!$value) {
                continue;
            }
            $pid = $value['pid'];
            $ps = exec(sprintf('ps -p %d', $pid));
            if (strpos($ps, '<defunct>') !== false) {
                exec('kill -9 '.$pid);
                $ps = '';
            }
            $running = preg_match('/^\s*(\d+)/', $ps, $m)
                && (int)$m[1] === $pid;
            if ($running) {
                exec('kill -9 ' . $value['pid']);
            } if (is_file($value['stdout'])) {
                unlink($value['stdout']);
                if (count(glob(dirname($value['stdout']) . '/*')) === 0) {
                    rmdir(dirname($value['stdout']));
                }
            }
        }
    }

    /**
     * Lance une commande de façon asyncrone si on a assez de threads disponibles
     * @param string|array $fn     doit être callable et serializable
     * @param array        $params
     * @return string uuid du process lancé
     */
    public function exec($fn, array $params = [])
    {
        if ($this->availableThreadCount() === 0) {
            $this->waitForAvailableThread();
        }
        $stdout = $this->getStdoutFile();

        $command = sprintf(
            '%s parallel %s %s >%s 2>/dev/null & echo $!',
            CAKE_SHELL,
            escapeshellarg(ParallelCommand::encode($fn)),
            escapeshellarg(ParallelCommand::encode($params)),
            $stdout
        );
        $pid = (int)exec($command);
        $uuid = uuid_create(UUID_TYPE_TIME);
        $this->threads[$this->nextAvailableThreadKey()] = [
            'uuid' => $uuid,
            'pid' => $pid,
            'begin' => microtime(true),
            'stdout' => $stdout
        ];
        return $uuid;
    }

    /**
     * Donne les résultats optenus jusqu'à maintenant
     * @return array
     */
    public function getResults()
    {
        return array_map(fn($v) => $v['result'], $this->results);
    }

    /**
     * Supprime les résultats précédement obtenu
     * @return array
     */
    public function clearResults()
    {
        return array_map(fn($v) => $v['result'], $this->results);
    }

    /**
     * @return void
     */
    public function waitForNextResult()
    {
        $this->waitForAvailableThread();
    }

    /**
     * Attend jusqu'à qu'il n'y ai plus un seul thread
     * @return void
     */
    public function waitUntilAllProcessesAreFinished()
    {
        while (array_filter($this->threads)) {
            $this->waitForAvailableThread();
        }
    }

    /**
     * Donne la clé du prochain thread libre
     * @return int
     */
    private function nextAvailableThreadKey(): int
    {
        foreach ($this->threads as $key => $value) {
            if (!$value) {
                return $key;
            }
        }
        throw new GenericException;
    }

    /**
     * Donne le nombre de threads disponibles
     * @return int
     */
    private function availableThreadCount(): int
    {
        return count($this->threads) - count(array_filter($this->threads));
    }

    /**
     * Attend qu'un des threads se libère
     * @return void
     */
    private function waitForAvailableThread()
    {
        $available = false;
        foreach ($this->threads as $key => $value) {
            // ce thread est déjà disponible
            if (!$value) {
                $available = true;
                continue;
            }
            $pid = $value['pid'];
            $ps = exec(sprintf('ps -p %d', $pid));
            if (strpos($ps, '<defunct>') !== false) {
                exec('kill -9 '.$pid);
                $ps = '';
            }
            $running = preg_match('/^\s*(\d+)/', $ps, $m)
                && (int)$m[1] === $pid;
            // ce thread est terminé
            if (!$running) {
                $available = true;
                if (is_file($value['stdout'])) {
                    $response = file_get_contents($value['stdout']);
                    unlink($value['stdout']);
                    $this->results[$value['uuid']] = ParallelCommand::parseResponse($response);
                    $this->threads[$key] = null;
                }
            } elseif ($this->timeout && (microtime(true) - $value['begin']) > $this->timeout) {
                throw new GenericException(
                    sprintf(
                        "timeout - process %d is still running\n%s",
                        $value['pid'],
                        file_get_contents($value['stdout'])
                    )
                );
            }
        }
        if (!$available) {
            sleep(1); // NOSONAR
            $this->waitForAvailableThread(); // FIXME implémenter le timeout
        }
    }

    /**
     * Donne un nom de fichier log temporaire
     * @return string
     */
    private function getStdoutFile()
    {
        $basedir = sys_get_temp_dir() . DS . 'as_parallel' . DS . getmypid() . DS;
        if (!is_dir($basedir)) {
            mkdir($basedir, 0777, true);
        }
        $mtime = date("YmdHis") . gettimeofday()["usec"];
        $filename = $basedir . $mtime . '.log';
        // pratiquement impossible qu'un fichier ai été créé à la même microseconde
        if (is_file($filename)) {
            usleep(1); // NOSONAR
            return $this->getStdoutFile();
        }
        return $filename;
    }
}
