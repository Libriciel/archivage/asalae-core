<?php
/**
 * AsalaeCore\Utility\PreControlMessages
 */

namespace AsalaeCore\Utility;

use AsalaeCore\Error\AppErrorHandler;
use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;
use DOMDocument;
use DOMNode;
use DOMNodeList;
use DOMXPath;
use Exception;

/**
 * Effectue le contrôle préliminaire des messages seda/medona
 *
 * @package AsalaeCore\Utility
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class PreControlMessages
{
    const INVALID_PRONOM = 1;
    const INVALID_XML = 2;
    const INVALID_MSG_VERSION = 3;
    const INVALID_MSG_NAME = 4;
    const ID_NOT_UNIQUE = 5;
    const ARCHIVAL_AGENCY_NOT_EXISTS = 6;
    const INVALID_FORMAT = 7;
    const EXCEPTION = 8;
    const TRANSFERRING_AGENCY_NOT_EXISTS = 9;

    const NAMESPACE_SEDA_02 = 'fr:gouv:ae:archive:draft:standard_echange_v0.2';
    const NAMESPACE_SEDA_10 = 'fr:gouv:culture:archivesdefrance:seda:v1.0';
    const NAMESPACE_SEDA_21 = 'fr:gouv:culture:archivesdefrance:seda:v2.1';
    const NAMESPACE_SEDA_22 = 'fr:gouv:culture:archivesdefrance:seda:v2.2';

    /**
     * @var array Pronoms qui passent la validation
     */
    public $allowedPronoms = ['fmt/101'];

    /**
     * @var array namespace => message => table
     */
    public $allowedMessages = [
        self::NAMESPACE_SEDA_02 => [
            'ArchiveTransferRequest' => '',
            'ArchiveTransferRequestReply' => '',
            'ArchiveTransfer' => 'Transfers',
            'ArchiveTransferReply' => '',
            'ArchiveTransferAcceptance' => '',
            'ArchiveDeliveryRequest' => '',
            'ArchiveDeliveryRequestReply' => '',
            'ArchiveDelivery' => '',
            'ArchiveModificationNotification' => '',
            'ArchiveDestructionRequest' => '',
            'ArchiveDestructionRequestReply' => '',
            'ArchiveDestructionAcceptance' => '',
            'ArchiveDestructionNotification' => '',
            'ArchiveRestitutionRequest' => '',
            'ArchiveRestitutionRequestReply' => '',
            'ArchiveRestitution' => '',
            'ArchiveRestitutionAcknowledgement' => '',
        ],
        self::NAMESPACE_SEDA_10 => [
            'Acknowledgement' => '',
            'ArchiveTransferRequest' => '',
            'ArchiveTransferRequestReply' => '',
            'ArchiveTransfer' => 'Transfers',
            'ArchiveTransferReply' => '',
            'ArchiveDeliveryRequest' => '',
            'ArchiveDeliveryRequestReply' => '',
            'ArchiveModificationNotification' => '',
            'ArchiveDestructionNotification' => '',
            'ArchiveRestitutionRequest' => '',
            'ArchiveRestitutionRequestReply' => '',
            'AuthorizationOriginatingAgencyRequest' => '',
            'AuthorizationOriginatingAgencyRequestReply' => '',
            'AuthorizationControlAuthorityRequest' => '',
            'AuthorizationControlAuthorityRequestReply' => '',
        ],
        self::NAMESPACE_SEDA_21 => [
            'Acknowledgement' => '',
            'ArchiveTransferRequest' => '',
            'ArchiveTransferRequestReply' => '',
            'ArchiveTransfer' => 'Transfers',
            'ArchiveTransferReply' => '',
            'ArchiveDeliveryRequest' => '',
            'ArchiveDeliveryRequestReply' => '',
            'ArchiveModificationNotification' => '',
            'ArchiveDestructionNotification' => '',
            'ArchiveRestitutionRequest' => '',
            'ArchiveRestitutionRequestReply' => '',
            'AuthorizationOriginatingAgencyRequest' => '',
            'AuthorizationOriginatingAgencyRequestReply' => '',
            'AuthorizationControlAuthorityRequest' => '',
            'AuthorizationControlAuthorityRequestReply' => '',
        ],
        self::NAMESPACE_SEDA_22 => [
            'Acknowledgement' => '',
            'ArchiveTransferRequest' => '',
            'ArchiveTransferRequestReply' => '',
            'ArchiveTransfer' => 'Transfers',
            'ArchiveTransferReply' => '',
            'ArchiveDeliveryRequest' => '',
            'ArchiveDeliveryRequestReply' => '',
            'ArchiveModificationNotification' => '',
            'ArchiveDestructionNotification' => '',
            'ArchiveRestitutionRequest' => '',
            'ArchiveRestitutionRequestReply' => '',
            'AuthorizationOriginatingAgencyRequest' => '',
            'AuthorizationOriginatingAgencyRequestReply' => '',
            'AuthorizationControlAuthorityRequest' => '',
            'AuthorizationControlAuthorityRequestReply' => '',
        ],
    ];

    /**
     * @var array chemin vers l'indentifiant unique du message
     */
    public $identifierIdPaths = [
        self::NAMESPACE_SEDA_02 => '/ns:ArchiveTransfer/ns:TransferIdentifier',
        self::NAMESPACE_SEDA_10 => '/ns:ArchiveTransfer/ns:TransferIdentifier',
        self::NAMESPACE_SEDA_21 => '/ns:ArchiveTransfer/ns:MessageIdentifier',
        self::NAMESPACE_SEDA_22 => '/ns:ArchiveTransfer/ns:MessageIdentifier',
    ];

    /**
     * @var array chemin vers la date du message
     */
    public $datePaths = [
        self::NAMESPACE_SEDA_02 => '/ns:ArchiveTransfer/ns:Date',
        self::NAMESPACE_SEDA_10 => '/ns:ArchiveTransfer/ns:Date',
        self::NAMESPACE_SEDA_21 => '/ns:ArchiveTransfer/ns:Date',
        self::NAMESPACE_SEDA_22 => '/ns:ArchiveTransfer/ns:Date',
    ];

    /**
     * @var array chemin vers le commentaire du message
     */
    public $commentPaths = [
        self::NAMESPACE_SEDA_02 => '/ns:ArchiveTransfer/ns:Comment',
        self::NAMESPACE_SEDA_10 => '/ns:ArchiveTransfer/ns:Comment',
        self::NAMESPACE_SEDA_21 => '/ns:ArchiveTransfer/ns:Comment',
        self::NAMESPACE_SEDA_22 => '/ns:ArchiveTransfer/ns:Comment',
    ];

    /**
     * @var array chemin vers l'indentifiant du service d'archives
     */
    public $identifierArchivalAgencyPaths = [
        self::NAMESPACE_SEDA_02 => '/ns:ArchiveTransfer/ns:ArchivalAgency/ns:Identification',
        self::NAMESPACE_SEDA_10 => '/ns:ArchiveTransfer/ns:ArchivalAgency/ns:Identification',
        self::NAMESPACE_SEDA_21 => '/ns:ArchiveTransfer/ns:ArchivalAgency/ns:Identifier',
        self::NAMESPACE_SEDA_22 => '/ns:ArchiveTransfer/ns:ArchivalAgency/ns:Identifier',
    ];

    /**
     * @var array chemin vers l'indentifiant du service versant
     */
    public $identifierTransferringAgencyPaths = [
        self::NAMESPACE_SEDA_02 => '/ns:ArchiveTransfer/ns:TransferringAgency/ns:Identification',
        self::NAMESPACE_SEDA_10 => '/ns:ArchiveTransfer/ns:TransferringAgency/ns:Identification',
        self::NAMESPACE_SEDA_21 => '/ns:ArchiveTransfer/ns:TransferringAgency/ns:Identifier',
        self::NAMESPACE_SEDA_22 => '/ns:ArchiveTransfer/ns:TransferringAgency/ns:Identifier',
    ];

    /**
     * @var array chemin vers l'indentifiant du service producteur
     */
    public $identifierOriginatingAgencyPaths = [
        self::NAMESPACE_SEDA_02 => '/ns:ArchiveTransfer/ns:Archive/ns:ContentDescription'
            . '/ns:OriginatingAgency/ns:Identification',
        self::NAMESPACE_SEDA_10 => '/ns:ArchiveTransfer/ns:Archive/ns:ContentDescription'
            . '/ns:OriginatingAgency/ns:Identification',
        self::NAMESPACE_SEDA_21 => '/ns:ArchiveTransfer/ns:DataObjectPackage/ns:DescriptiveMetadata'
            . '/ns:ArchiveUnit/ns:Content/ns:OriginatingAgency/ns:Identifier',
        self::NAMESPACE_SEDA_22 => '/ns:ArchiveTransfer/ns:DataObjectPackage/ns:DescriptiveMetadata'
            . '/ns:ArchiveUnit/ns:Content/ns:OriginatingAgency/ns:Identifier',
    ];

    /**
     * @var array Uri vers le schema xsd de validation xml
     */
    public $schemas = [
        self::NAMESPACE_SEDA_02 => WWW_ROOT.'xmlSchemas'.DS.'seda_v0-2'.DS.'seda_v0-2.xsd',
        self::NAMESPACE_SEDA_10 => WWW_ROOT.'xmlSchemas'.DS.'seda_v1-0'.DS.'seda_v1-0.xsd',
        self::NAMESPACE_SEDA_21 => WWW_ROOT.'xmlSchemas'.DS.'seda_v2-1'.DS.'seda-2.1-main.xsd',
        self::NAMESPACE_SEDA_22 => WWW_ROOT.'xmlSchemas'.DS.'seda_v2-2'.DS.'seda-2.2-main.xsd',
    ];

    /**
     * @var string Chemin vers le fichier à controller
     */
    public $path;

    /**
     * @var \DOMDocument object xml du message
     */
    public $dom;

    /**
     * @var null|string namespace du xml
     */
    private $namespace = null;

    /**
     * @var int Dernière erreur
     */
    public static $lastError = 0;
    /**
     * @var string Dernière erreur
     */
    public static $lastErrorMessage = '';

    /**
     * @var Exception Dernière exception
     */
    public static $lastException;

    /**
     * @var array erreurs données par le schéma de validation
     */
    public static $lastSchemaErrors = [];

    /**
     * @var EntityInterface entité du service d'archives
     */
    private $archivalAgency;

    /**
     * @var DOMXpath
     */
    public $xpath;

    /**
     * ControlMessages constructor.
     * @param string|null $path
     * @throws Exception
     */
    public function __construct(string $path = null)
    {
        if (!is_readable($path)) {
            throw new Exception(__('File not found: {0}', $path));
        }
        $this->path = $path;
    }

    /**
     * Effectue la validation sur un xml du file system
     * @return bool
     */
    public function validate(): bool
    {
        $instance = $this;
        try {
            $success = $this->isValidPronom()
                && $this->isValidXml()
                && $this->inAllowedMessageVersion()
                && $this->inAllowedMessageName()
                && $this->idIsUnique()
                && $this->archivalAgencyExists()
                && $this->transferringAgencyExists();
            if ($success) {
                $capture = AppErrorHandler::captureErrors(
                    function () use ($instance) {
                        return $instance->validateBySchema();
                    }
                );
                self::$lastSchemaErrors = $capture['errors'];
                if (!$capture['result']) {
                    self::setError(self::INVALID_FORMAT);
                }
            }
            return $success && !empty($capture) && $capture['result'];
        } catch (Exception $e) {
            self::setError(self::EXCEPTION, $e);
            return false;
        }
    }

    /**
     * Donne le pronom d'un fichier
     * @return string
     * @throws Exception
     */
    private function extractPronom(): string
    {
        $response = exec('sf -json '.Exec::escapeshellarg($this->path).' 2>/dev/null');
        if ($response && ($data = json_decode($response)) && isset($data->files[0]->matches[0]->id)) {
            return $data->files[0]->matches[0]->id;
        }
        throw new Exception(__('Unable to find a pronom on {0}', $this->path));
    }

    /**
     * Appel statique à la fonction validate()
     * @param string $path
     * @return bool
     * @throws Exception
     */
    public static function isValid(string $path): bool
    {
        $instance = new self($path);
        return $instance->validate();
    }

    /**
     * Le pronom du fichier est autorisé
     * @return bool
     * @throws Exception
     */
    public function isValidPronom(): bool
    {
        $pronom = $this->extractPronom();
        if (!in_array($pronom, $this->allowedPronoms)) {
            self::setError(self::INVALID_PRONOM, $pronom);
            return false;
        }
        return true;
    }

    /**
     * Permet d'obtenir le DOMDocument du fichier xml du bordereau
     * @return DOMDocument
     */
    public function getDom()
    {
        if (empty($this->dom)) {
            $this->dom = new \DOMDocument;
            $initial = libxml_use_internal_errors();
            libxml_use_internal_errors(true);
            $this->dom->load($this->path);
            libxml_use_internal_errors($initial);
        }
        return $this->dom;
    }

    /**
     * Vérifi que le fichier est bien structuré
     * @return bool
     */
    public function isValidXml(): bool
    {
        $this->dom = new \DOMDocument;
        if (!@$this->dom->load($this->path)) {
            self::setError(self::INVALID_XML);
            return false;
        }
        return true;
    }

    /**
     * Getter du namespace
     * @return string
     */
    public function getNamespace(): string
    {
        if (!$this->namespace) {
            $root = $this->getDom()->documentElement;
            $namespace = $root->getAttributeNode('xmlns');
            $this->namespace = $namespace && $namespace->nodeValue
                ? $namespace->nodeValue
                : '';
        }
        return $this->namespace;
    }

    /**
     * Getter du xpath
     * @return DOMXPath
     */
    public function getXpath(): DOMXPath
    {
        if (!$this->xpath) {
            $this->xpath = new DOMXPath($this->getDom());
            $this->xpath->registerNamespace('ns', $this->getNamespace());
        }
        return $this->xpath;
    }

    /**
     * Vrai si le namespace est dans la liste des namespaces autorisés
     * @return bool
     */
    public function inAllowedMessageVersion(): bool
    {
        $namespace = $this->getNamespace();
        if (!in_array($namespace, array_keys($this->allowedMessages))) {
            self::setError(self::INVALID_MSG_VERSION, $namespace);
            return false;
        }
        return true;
    }

    /**
     * Fait parti des messages autorisés
     * @return bool
     */
    private function inAllowedMessageName(): bool
    {
        $allowed = array_keys($this->allowedMessages[$this->getNamespace()]);
        $messageName = $this->getMessageName();
        if (!in_array($messageName, $allowed)) {
            self::setError(self::INVALID_MSG_NAME, $messageName);
            return false;
        }
        return true;
    }

    /**
     * Renvoi le nom du message ex: ArchiveTransfer
     * @return string
     */
    public function getMessageName(): string
    {
        return $this->getDom()->documentElement->nodeName ?? '';
    }

    /**
     * Vrai si aucun enregistrement n'existe en base pour ce type de message et
     * cet identifiant
     * @return bool
     * @throws Exception
     */
    private function idIsUnique(): bool
    {
        $identifier = $this->getTransferIdentifier();
        if (empty($identifier)) {
            self::setError(self::ID_NOT_UNIQUE, '');
            return false;
        }
        $tableName = $this->allowedMessages[$this->getNamespace()][$this->getMessageName()];
        $Model = TableRegistry::getTableLocator()->get($tableName);
        $conditions = ['transfer_identifier' => $identifier];
        if ($archivalAgency = $this->getArchivalAgency()) {
            $conditions['archival_agency_id'] = $archivalAgency->id;
        }
        if ($Model->exists($conditions)) {
            self::setError(self::ID_NOT_UNIQUE, $identifier);
            return false;
        }
        return true;
    }

    /**
     * Donne l'identifier du service d'archives
     * @return string
     */
    public function getArchivalAgencyIdentifier(): string
    {
        $path = $this->identifierArchivalAgencyPaths[$this->getNamespace()];
        $query = $this->getXpath()->query($path);
        if ($query->count() === 0) {
            return '';
        }
        return $query->item(0)->nodeValue;
    }

    /**
     * Donne l'entité du service d'archives
     * @return array|EntityInterface|null
     */
    public function getArchivalAgency()
    {
        if (!$this->archivalAgency) {
            $Model = TableRegistry::getTableLocator()->get('OrgEntities');
            $this->archivalAgency = $Model->find()
                ->innerJoinWith('TypeEntities')
                ->where(
                    [
                        'OrgEntities.identifier' => $this->getArchivalAgencyIdentifier(),
                        'TypeEntities.code' => 'SA',
                        'OrgEntities.active' => true,
                    ]
                )
                ->first();
        }
        return $this->archivalAgency;
    }

    /**
     * Vrai si le service d'archive correspondant existe
     * @return bool
     */
    public function archivalAgencyExists(): bool
    {
        $identifier = $this->getArchivalAgencyIdentifier();
        if (empty($identifier)) {
            self::setError(self::ARCHIVAL_AGENCY_NOT_EXISTS, '');
            return false;
        }
        if (!$this->getArchivalAgency()) {
            self::setError(self::ARCHIVAL_AGENCY_NOT_EXISTS, $identifier);
            return false;
        }
        return true;
    }

    /**
     * Donne l'uri du schema (xsd)
     * @return string
     */
    public function getSchema(): string
    {
        return $this->schemas[$this->getNamespace()];
    }

    /**
     * Passe le xml à la validation xsd
     * @return bool
     */
    public function validateBySchema(): bool
    {
        if (!$this->getDom()->schemaValidate($this->getSchema())) {
            return false;
        }
        return true;
    }

    /**
     * Défini la derniere erreur selon un code
     * @param int   $code
     * @param mixed ...$args
     * @return void
     */
    private static function setError(int $code, ...$args)
    {
        switch (self::$lastError = $code) {
            case self::INVALID_PRONOM:
                self::$lastErrorMessage = __("Le pronom ({0}) ne fait pas partie des pronoms autorisés", $args[0]);
                break;
            case self::INVALID_XML:
                self::$lastErrorMessage = __("Le fichier XML semble mal formaté");
                break;
            case self::INVALID_MSG_VERSION:
                self::$lastErrorMessage = __(
                    "Le format du message ({0}) ne fait pas partie des formats pris en charge par l'application",
                    $args[0]
                );
                break;
            case self::INVALID_MSG_NAME:
                self::$lastErrorMessage = __(
                    "Le nom du message ({0}) ne fait pas partie des noms pris en charge par l'application",
                    $args[0]
                );
                break;
            case self::ID_NOT_UNIQUE:
                self::$lastErrorMessage = __(
                    "L'identifiant présent dans le message ({0}) est déjà présent dans l'application",
                    $args[0]
                );
                break;
            case self::ARCHIVAL_AGENCY_NOT_EXISTS:
                self::$lastErrorMessage = __(
                    "Le service d'archives indiqué dans le message ({0}) n'existe pas dans l'application",
                    $args[0]
                );
                break;
            case self::INVALID_FORMAT:
                $errors = array_map(
                    function ($v) {
                        return $v['description'] ?? '';
                    },
                    self::$lastSchemaErrors
                );
                self::$lastErrorMessage =  __("La validation par schéma a échoué:\n{0}", implode("\n", $errors));
                break;
            case self::EXCEPTION:
                self::$lastErrorMessage = __("Une erreur inattendue a eu lieu:\n{0}", $args[0]->getMessage());
                self::$lastException = $args[0];
                break;
            case self::TRANSFERRING_AGENCY_NOT_EXISTS:
                self::$lastErrorMessage = __(
                    "Le service versant indiqué dans le message ({0}) n'existe pas dans l'application",
                    $args[0]
                );
                break;
            default:
                self::$lastErrorMessage = '';
        }
    }

    /**
     * Donne le message d'erreur de la dernière erreur
     * @return string
     */
    public static function getLastErrorMessage(): string
    {
        return self::$lastErrorMessage;
    }

    /**
     * Vrai si le service versant correspondant existe
     * @return bool
     */
    public function transferringAgencyExists()
    {
        $path = $this->identifierTransferringAgencyPaths[$this->getNamespace()];
        $identifier = $this->getXpath()->query($path);
        if ($identifier->item(0)) {
            $identifier = $identifier->item(0)->nodeValue;
        }
        if (empty($identifier) || $identifier instanceof DOMNodeList) {
            self::setError(self::TRANSFERRING_AGENCY_NOT_EXISTS, '');
            return false;
        }
        $Model = TableRegistry::getTableLocator()->get('OrgEntities');
        $success = $Model->find()
            ->select(['qexists' => 1])
            ->innerJoinWith('TypeEntities')
            ->where(
                [
                    'OrgEntities.identifier' => $identifier,
                    'TypeEntities.code IN' => ['SA', 'SV'],
                    'OrgEntities.lft >=' => $this->getArchivalAgency()->get('lft'),
                    'OrgEntities.rght <=' => $this->getArchivalAgency()->get('rght'),
                    'OrgEntities.active' => true,
                ]
            )
            ->first();
        if (!$success) {
            self::setError(self::TRANSFERRING_AGENCY_NOT_EXISTS, $identifier);
            return false;
        }
        return true;
    }

    /**
     * Donne l'identifiant du transfert
     * @param bool $getElement
     * @return DOMNode|false|null
     */
    public function getTransferIdentifier(bool $getElement = false)
    {
        $path = $this->identifierIdPaths[$this->getNamespace()];
        $identifier = $this->getXpath()->query($path);
        $element = $identifier->item(0);
        if ($getElement) {
            return $element;
        }
        if ($element) {
            return $element->nodeValue;
        } else {
            return false;
        }
    }
}
