<?php
/**
 * AsalaeCore\Utility\SoapClient
 */

namespace AsalaeCore\Utility;

use SoapClient as OriginalSoapClient;
use SoapFault;

/**
 * Ajout manuel du port dans __doRequest afin de corriger un bug de la library
 *
 * @package AsalaeCore\Utility
 * @link https://web.archive.org/web/20111216190421/http://www.victorstanciu.ro/php-soapclient-port-bug-workaround/
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class SoapClient extends OriginalSoapClient
{
    /**
     * @var mixed
     */
    private $_port;

    /**
     * SoapClient constructor.
     * @link https://www.php.net/manual/fr/soapclient.soapclient.php
     * @param mixed $wsdl    URI of the WSDL file or <b>NULL</b> if working in non-WSDL mode.
     * @param array $options [optional]
     * @throws SoapFault A SoapFault exception will be thrown if the wsdl URI cannot be loaded.
     */
    public function __construct($wsdl, array $options = [])
    {
        $url = parse_url($wsdl);
        if (isset($url['port']) && $url['port']) {
            $this->_port = $url['port'];
        }
        parent::__construct($wsdl, $options);
    }

    /**
     * Performs a SOAP request
     * @link https://php.net/manual/en/soapclient.dorequest.php
     * @param string $request  The XML SOAP request.
     * @param string $location The URL to request.
     * @param string $action   The SOAP action.
     * @param int    $version  The SOAP version.
     * @param int    $oneWay   If one_way is set to 1, this method returns nothing.
     *                         Use this where a response is not expected.
     * @return string The XML SOAP response.
     * @since 5.0.1
     */
    public function __doRequest($request, $location, $action, $version, $oneWay = 0)
    {
        $parts = parse_url($location);
        if (isset($this->_port) && $this->_port) {
            $parts['port'] = $this->_port;
        }
        $location = $this->unparseUrl($parts);

        return parent::__doRequest($request, $location, $action, $version);
    }

    /**
     * @link http://php.net/manual/fr/function.parse-url.php#106731
     * @param array $parsedUrl
     * @return string
     */
    private function unparseUrl(array $parsedUrl): string
    {
        $scheme   = isset($parsedUrl['scheme']) ? $parsedUrl['scheme'] . '://' : '';
        $host     = $parsedUrl['host'] ?? '';
        $port     = isset($parsedUrl['port']) ? ':' . $parsedUrl['port'] : '';
        $user     = $parsedUrl['user'] ?? '';
        $pass     = isset($parsedUrl['pass']) ? ':' . $parsedUrl['pass']  : '';
        $pass     = ($user || $pass) ? "$pass@" : '';
        $path     = $parsedUrl['path'] ?? '';
        $query    = isset($parsedUrl['query']) ? '?' . $parsedUrl['query'] : '';
        $fragment = isset($parsedUrl['fragment']) ? '#' . $parsedUrl['fragment'] : '';
        return "$scheme$user$pass$host$port$path$query$fragment";
    }
}
