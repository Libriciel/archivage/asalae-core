<?php
/**
 * AsalaeCore\Utility\Object\CommandResult
 */

namespace AsalaeCore\Utility\Object;

use ArrayObject;

/**
 * Résultat des commandes bash
 *
 * @category Utility
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class CommandResult extends ArrayObject
{
    /**
     * @var bool success
     */
    public $success;

    /**
     * @var int return code
     */
    public $code;

    /**
     * @var string stdout
     */
    public $stdout;

    /**
     * @var string stderr
     */
    public $stderr;

    /**
     * CommandResult constructor.
     * @param array $results
     */
    public function __construct(array $results)
    {
        parent::__construct($results);
        $this->success = $results['success'];
        $this['success'] = $results['success'];
        $this->code = $results['code'];
        $this['code'] = $results['code'];
        $this->stdout = $results['stdout'];
        $this['stdout'] = $results['stdout'];
        $this->stderr = $results['stderr'];
        $this['stderr'] = $results['stderr'];
    }
}
