<?php
/**
 * AsalaeCore\Utility\Premis
 * @noinspection HttpUrlsUsage https correct mais http utilisé dans le schema
 */

namespace AsalaeCore\Utility;

use AsalaeCore\Utility\Premis\Agent;
use AsalaeCore\Utility\Premis\Event;
use AsalaeCore\Utility\Premis\File;
use AsalaeCore\Utility\Premis\IntellectualEntity;
use AsalaeCore\Utility\Premis\ObjectInterface;
use Cake\Http\Exception\NotImplementedException;
use Cake\Utility\Hash;
use DOMDocument;
use DOMElement;
use DOMException;
use DOMXPath;
use Exception;

/**
 * Permet de créer une liste d'évenement au format premis v3.0
 *
 * @category Utility
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Premis
{
    const SCHEMA_LOCATION = 'http://www.loc.gov/premis/v3 https://www.loc.gov/standards/premis/premis.xsd';
    const VERSION = '3.0';
    const NS_PREMIS = 'http://www.loc.gov/premis/v3'; // NOSONAR
    const NS_XLINK = 'http://www.w3.org/1999/xlink'; // NOSONAR
    const NS_XSI = 'http://www.w3.org/2001/XMLSchema-instance'; // NOSONAR

    /**
     * $objects[type][value] = ObjectInterface
     * @var array[] liste les objets
     */
    private $objects = [];

    /**
     * $objects[type][value] = Event
     * @var array[] liste les evenements
     */
    private $events = [];

    /**
     * $agents[type][value] = ObjectInterface
     * @var array[] liste les agents
     */
    private $agents = [];

    /**
     * Ajoute un evenement
     * @param Event $event eventComplexType
     * @return Premis
     */
    public function add(Event $event): self
    {
        $this->events[$event->identifierType][$event->identifierValue] = $event;
        foreach ($event->objects as $object) {
            if (!isset($this->objects[$object->identifierType][$object->identifierValue])) {
                $this->objects[$object->identifierType][$object->identifierValue] = $object;
            }
        }
        foreach ($event->agents as $agent) {
            if (!isset($this->agents[$agent->identifierType][$agent->identifierValue])) {
                $this->agents[$agent->identifierType][$agent->identifierValue] = $agent;
            }
        }
        return $this;
    }

    /**
     * Ajoute un objet isolé
     * @param ObjectInterface $object
     * @return Premis
     */
    public function addObject(ObjectInterface $object): self
    {
        $type = $object->identifierType ?? '';
        $val = $object->identifierValue ?? '';
        $this->objects[$type][$val] = $object;
        return $this;
    }

    /**
     * Donne le xml du premis
     * @return DOMDocument
     * @throws DOMException
     */
    public function generateDocument(): DOMDocument
    {
        $dom = new DOMDocument;
        $dom->formatOutput = true;
        $dom->preserveWhiteSpace = false;
        $premis = $dom->createElementNS(Premis::NS_PREMIS, 'premis');
        $premis->setAttributeNS('http://www.w3.org/2000/xmlns/', 'xmlns:premis', Premis::NS_PREMIS);
        $premis->setAttributeNS('http://www.w3.org/2000/xmlns/', 'xmlns:xlink', Premis::NS_XLINK);
        $premis->setAttributeNS('http://www.w3.org/2000/xmlns/', 'xmlns:xsi', Premis::NS_XSI);
        $premis->setAttributeNS(Premis::NS_PREMIS, 'version', '3.0');

        // import des objects
        /** @var IntellectualEntity|File $object */
        foreach (Hash::flatten($this->objects) as $object) {
            $premis->appendChild(
                $dom->importNode(
                    $object->renderElement(),
                    true
                )
            );
        }
        // import des events
        /** @var Event $event */
        foreach (Hash::flatten($this->events) as $event) {
            $premis->appendChild(
                $dom->importNode(
                    $event->renderElement(),
                    true
                )
            );
        }
        // import des agents
        /** @var Agent $agent */
        foreach (Hash::flatten($this->agents) as $agent) {
            $premis->appendChild(
                $dom->importNode(
                    $agent->renderElement(),
                    true
                )
            );
        }
        $dom->appendChild($premis);

        // prise en compte du changement de namespace
        $dom2 = new DOMDocument;
        $dom2->loadXML($dom->saveXML());
        return $dom2;
    }

    /**
     * Donne le xml du premis
     * @return string
     * @throws DOMException
     */
    public function generate(): string
    {
        return $this->generateDocument()->saveXML();
    }

    /**
     * Appel generate()
     * @return string
     * @throws DOMException
     */
    public function __toString(): string
    {
        return $this->generate();
    }

    /**
     * Charge un premis (fichier)
     * @param string $filename
     * @return Premis
     * @throws Exception
     */
    public static function load(string $filename): Premis
    {
        $dom = new DOMDocument;
        $dom->load($filename);
        return static::loadDocument($dom);
    }

    /**
     * Charge un premis (string)
     * @param string $xml
     * @return Premis
     * @throws Exception
     */
    public static function loadXML(string $xml): Premis
    {
        $dom = new DOMDocument;
        $dom->loadXML($xml);
        return static::loadDocument($dom);
    }

    /**
     * Charge un premis (document)
     * @param DOMDocument $dom
     * @return Premis
     * @throws Exception
     */
    public static function loadDocument(DOMDocument $dom): Premis
    {
        $premis = new Premis;
        foreach ($dom->getElementsByTagName('object') as $object) {
            if (!$object instanceof DOMElement) {
                continue;
            }
            switch ($object->getAttribute('xsi:type')) {
                case 'premis:intellectualEntity':
                    $object = Premis\IntellectualEntity::loadElement($object);
                    break;
                case 'premis:file':
                    $object = Premis\File::loadElement($object);
                    break;
                default:
                    throw new NotImplementedException('Not implemented: '.$object->getAttribute('xsi:type'));
            }
            $premis->addObject($object);
        }
        foreach ($dom->getElementsByTagName('event') as $event) {
            if (!$event instanceof DOMElement) {
                continue;
            }
            $ev = Premis\Event::loadElement($event);
            $premis->events[$ev->identifierType][$ev->identifierValue] = $ev;
        }
        foreach ($dom->getElementsByTagName('agent') as $agent) {
            if (!$agent instanceof DOMElement) {
                continue;
            }
            $ag = Premis\Agent::loadElement($agent);
            $premis->agents[$ag->identifierType][$ag->identifierValue] = $ag;
        }
        return $premis;
    }

    /**
     * Intègre un autre premis
     * @param Premis $premis
     * @return Premis
     */
    public function merge(Premis $premis): Premis
    {
        if (!empty($premis->objects)) {
            foreach ($premis->objects as $type => $values) {
                /** @var ObjectInterface $obj */
                foreach ($values as $value => $obj) {
                    if (!isset($this->objects[$type][$value])) {
                        $this->objects[$type][$value] = $obj;
                    }
                }
            }
        }
        if (!empty($premis->events)) {
            foreach ($premis->events as $type => $values) {
                /** @var ObjectInterface $obj */
                foreach ($values as $value => $obj) {
                    if (!isset($this->events[$type][$value])) {
                        $this->events[$type][$value] = $obj;
                    }
                }
            }
        }
        if (!empty($premis->agents)) {
            foreach ($premis->agents as $type => $values) {
                /** @var ObjectInterface $obj */
                foreach ($values as $value => $obj) {
                    if (!isset($this->agents[$type][$value])) {
                        $this->agents[$type][$value] = $obj;
                    }
                }
            }
        }
        return $this;
    }

    /**
     * Permet de récupérer un object
     * @param string $identifierType
     * @param string $identifierValue
     * @return ObjectInterface
     */
    public function getObject($identifierType, $identifierValue): Premis\ObjectInterface
    {
        return $this->objects[$identifierType][$identifierValue];
    }

    /**
     * Ajoute un event à un premis sans parser le document
     * @param DOMDocument $dom
     * @param Event       $event
     * @return DOMElement
     * @throws DOMException
     */
    public static function appendEvent(DOMDocument $dom, Event $event): DOMElement
    {
        $util = new DOMUtility($dom);
        if (!$util->namespace) {
            $xmlns = $dom->documentElement->getAttributeNodeNS('http://www.w3.org/2000/xmlns/', 'premis');
            $util->namespace = $xmlns ? $xmlns->nodeValue : null;
            $util->xpath = new DOMXPath($dom);
            if ($util->namespace) {
                $util->xpath->registerNamespace('ns', $util->namespace);
            }
        }
        foreach ($event->objects as $object) {
            $value = DOMUtility::xpathQuote($object->identifierValue);
            $query = $util->xpath->query(
                "ns:object/ns:objectIdentifier/ns:objectIdentifierValue[contains(text(), $value)]"
            );
            if (!$query->count()) {
                self::appendObject($util, $object);
            }
        }
        /** @var DOMElement $element */
        $element = $util->dom->importNode($event->renderElement(), true);

        $query = $util->xpath->query("ns:agent");
        $first = $query->item(0);
        if (!$first) {
            $util->dom->documentElement->appendChild($element);
        } else {
            $util->dom->documentElement->insertBefore($element, $first);
        }

        foreach ($event->agents as $agent) {
            $value = DOMUtility::xpathQuote($agent->identifierValue);
            $query = $util->xpath->query(
                "ns:agent/ns:agentIdentifier/ns:agentIdentifierValue[contains(text(), $value)]"
            );
            if (!$query->item(0)) {
                $agentElement = $util->dom->importNode($agent->renderElement(), true);
                $util->dom->documentElement->appendChild($agentElement);
            }
        }

        return $element;
    }

    /**
     * Ajoute un objet à un premis (dom)
     * @param DOMUtility      $util
     * @param ObjectInterface $object
     */
    private static function appendObject(DOMUtility $util, ObjectInterface $object)
    {
        $element = $util->dom->importNode($object->renderElement(), true);
        $query = $util->xpath->query("ns:event");
        $first = $query->item(0);
        if (!$first) {
            $query = $util->xpath->query("ns:agent");
            $first = $query->item(0);
            if (!$first) {
                $util->dom->documentElement->appendChild($element);
            } else {
                $util->dom->documentElement->insertBefore($element, $first);
            }
        } else {
            $util->dom->documentElement->insertBefore($element, $first);
        }
    }
}
