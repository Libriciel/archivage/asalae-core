<?php
/**
 * AsalaeCore\Utility\Timestamper
 */

namespace AsalaeCore\Utility;

use AsalaeCore\Driver\Timestamping\TimestampingIaik;
use AsalaeCore\Driver\Timestamping\TimestampingInterface;
use AsalaeCore\Driver\Timestamping\TimestampingLocal;
use AsalaeCore\Driver\Timestamping\TimestampingOpensign;
use AsalaeCore\Error\AppErrorHandler;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\Http\Exception\NotFoundException;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use DateTime;
use Exception;
use InvalidArgumentException;
use ReflectionClass;
use ReflectionException;

/**
 * Horodateurs
 *
 * @category Utility
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Timestamper
{
    /**
     * @var TimestampingInterface|TimestampingIaik|TimestampingLocal|TimestampingOpensign
     */
    private $driver;

    /**
     * @var array erreurs lors d'un ping/check
     */
    public $errors = [];

    /**
     * Timestamper constructor.
     * @param int|EntityInterface $timestamper id ou entité du timestamper
     * @throws Exception
     */
    public function __construct($timestamper)
    {
        $this->driver = is_numeric($timestamper)
            ? self::getDriverById($timestamper)
            : self::getDriverByEntity($timestamper);
    }

    /**
     * Permet d'obtenir une instance du driver d'un horodateur à partir de son id
     * @param EntityInterface $timestamper
     * @return TimestampingInterface|TimestampingIaik|TimestampingLocal|TimestampingOpensign
     * @throws Exception
     * @throws ReflectionException
     */
    public static function getDriverByEntity(EntityInterface $timestamper): TimestampingInterface
    {
        $config = Configure::read('Timestamping.drivers.'.$timestamper->get('driver'));
        if (!$config) {
            throw new NotFoundException(sprintf("The driver %s was not found", $timestamper->get('driver')));
        }

        $args = [];
        foreach ((array)Hash::get($config, 'fields', []) as $field => $params) {
            $value = null;
            if (!empty($params['read_config'])) {
                $value = Configure::read($params['read_config']);
            } elseif (isset($params['type'])
                && $params['type'] === 'password'
                && $password = $timestamper->get($field.'_decrypted')
            ) {
                $value = $password;
            } elseif ($timestamper->get($field)) {
                $value = $timestamper->get($field);
            } elseif (!empty($params['default'])) {
                $value = $params['default'];
            }
            if ($value) {
                $args[] = $value;
            } else {
                $args[] = '';
            }
        }

        $refl = new ReflectionClass(Hash::get($config, 'class', ''));
        $instance = $refl->newInstanceArgs($args);
        if (!$instance instanceof TimestampingInterface) {
            throw new Exception(
                sprintf(
                    "The driver %s class does not implement the TimestampingInterface",
                    $timestamper->get('driver')
                )
            );
        }
        return $instance;
    }

    /**
     * Permet d'obtenir une instance du driver d'un horodateur à partir de son id
     * @param string|int $timestamper_id
     * @return TimestampingInterface|TimestampingIaik|TimestampingLocal|TimestampingOpensign
     * @throws Exception
     */
    public static function getDriverById($timestamper_id): TimestampingInterface
    {
        $Timestampers = TableRegistry::getTableLocator()->get('Timestampers');
        $timestamper = $Timestampers->find()->where(['id' => $timestamper_id])->firstOrFail();
        return self::getDriverByEntity($timestamper);
    }

    /**
     * Check sur le driver en capturant les erreurs
     * @return bool
     */
    public function check(): bool
    {
        $errHandler = AppErrorHandler::captureErrors([$this->driver, 'check']);
        $output = $errHandler['result'];
        if (!empty($errHandler['errors'])) {
            $this->errors = $errHandler['errors'];
        }
        return $output;
    }

    /**
     * Lance une commande, envoi une exception avec le message d'erreur en cas de fail
     * @param string $cmd
     * @throws Exception
     */
    private static function exec(string $cmd)
    {
        exec($cmd." 2>&1 >/dev/null", $output, $code);
        if ($code !== 0) {
            throw new Exception('Unable to use openssl: '.PHP_EOL.implode(PHP_EOL, $output));
        }
    }

    /**
     * Génère le fichier token
     * @param string $responseFile
     * @return string
     * @throws Exception
     */
    private static function getTokenFile(string $responseFile): string
    {
        $filename = tempnam(sys_get_temp_dir(), 'timestamping-');
        $cmd = "openssl ts -reply -in ".escapeshellarg($responseFile)
            ." -out ".escapeshellarg($filename)
            ." -token_out";
        self::exec($cmd);
        return $filename;
    }

    /**
     * Vérifi qu'un token a été produit par $caCrtFile
     * @param string      $file
     * @param string      $tsr
     * @param string|null $caCrtFile
     * @return bool
     * @throws Exception
     */
    public static function verifyTsr(string $file, string $tsr, string $caCrtFile = null): bool
    {
        $hash = self::getFileSha1($file);
        if (@is_file($tsr)) {
            $tsrFile = $tsr;
            $isTmpFile = false;
        } else {
            $tsrFile = tempnam(sys_get_temp_dir(), 'verify-token-');
            file_put_contents($tsrFile, $tsr);
            $isTmpFile = true;
        }
        try {
            $tokenFile = self::getTokenFile($tsrFile);
        } catch (Exception $e) {
            return false;
        }
        $cmd = "openssl ts -verify -digest ".escapeshellarg($hash)
            ." -sha1"
            ." -in ".escapeshellarg($tokenFile)
            ." -token_in"
            .($caCrtFile ? " -CAfile ".escapeshellarg($caCrtFile) : '');
        exec($cmd." 2>&1 >/dev/null", $output, $code);
        if ($isTmpFile) {
            unlink($tsrFile);
        }
        return $code === 0;
    }

    /**
     * Donne le sha1 d'un fichier, qu'il soit sur disque ou en streaming
     * @param string $file
     * @return string
     */
    private static function getFileSha1($file): string
    {
        if (is_resource($file)) {
            rewind($file);
            $sha1 = sha1(stream_get_contents($file)); // NOSONAR
        } elseif (is_string($file) && is_readable($file)) {
            $sha1 = sha1_file($file);
        } else {
            throw new InvalidArgumentException();
        }
        return $sha1;
    }

    /**
     * Extraction des données du jeton (hash, algo, timestamp et tsa)
     * @param string $tsr
     * @return array
     * @throws Exception
     */
    public static function extractInfo(string $tsr)
    {
        if (@is_file($tsr)) {
            $tsrFile = $tsr;
            $isTmpFile = false;
        } else {
            $tsrFile = tempnam(sys_get_temp_dir(), 'verify-token-');
            file_put_contents($tsrFile, $tsr);
            $isTmpFile = true;
        }
        $cmd = "openssl ts -reply"
            ." -in ".escapeshellarg($tsrFile)
            ." -text";
        exec($cmd." 2>&1", $output);

        if ($isTmpFile) {
            unlink($tsrFile);
        }

        $data = ['hash' => ''];
        $hash = false;
        foreach ($output as $str) {
            if (substr($str, 0, 15) === 'Hash Algorithm:') {
                $data['hash_algo'] = trim(substr($str, 16));
            } elseif ($str === 'Message data:') {
                $hash = true;
            } elseif ($hash && preg_match('/\d{4}[ -]+((?:[a-f\d]{2}[ -]?)+)/', $str, $m)) {
                $data['hash'] .= str_replace([' ', '-'], '', $m[1]);
            } elseif (substr($str, 0, 11) === 'Time stamp:') {
                $data['timestamp'] = new DateTime(trim(substr($str, 12)));
            } elseif (substr($str, 0, 4) === 'TSA:') {
                $tsa = stripcslashes(trim(substr($str, 5)));
                foreach (explode('/', $tsa) as $value) {
                    if (!strpos($value, '=')) {
                        continue;
                    }
                    list($k, $v) = explode('=', $value);
                    $data['tsa'][$k] = $v;
                }
            }
        }
        return $data;
    }
}
