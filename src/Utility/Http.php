<?php
/**
 * AsalaeCore\Utility\Http
 */

namespace AsalaeCore\Utility;

use AsalaeCore\Http\Client;
use Cake\Http\Client\Response;

/**
 * Client HTTP de test
 *
 * @category Utility
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2022, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Http
{
    /**
     * @var array
     */
    public static $options = [];
    /**
     * @var mixed
     */
    private static $defaultOptions = [
        'verify' => false,
        'ssl_verify_peer' => false,
        'ssl_verify_peer_name' => false,
        'ssl_verify_host' => false,
        'redirect' => true,
    ];

    /**
     * Ajoute une autentication basic
     * @param string $username
     * @param string $password
     * @return static
     */
    public static function auth(string $username, string $password): self
    {
        self::$options['auth'] = ['username' => $username, 'password' => $password];
        return new self;
    }

    /**
     * Requète GET
     * @param string $url
     * @param array  $options
     * @return Response
     */
    public static function get(string $url, array $options = []): Response
    {
        $options = $options + self::$options + self::$defaultOptions;
        $client = new Client;
        $response = $client->get($url, [], $options + self::$options + self::$defaultOptions);
        $response->getBody()->rewind();
        return $response;
    }

    /**
     * Requète GET, retourne le corp du résultat en json parsé
     * @param string $url
     * @param array  $options
     * @return array|string|null
     */
    public static function getJson(string $url, array $options = [])
    {
        if (!isset($options['headers'])) {
            $options['headers'] = [];
        }
        $options['headers']['Accept'] = 'application/json';
        $response = self::get($url, $options);
        $body = $response->getBody();
        $body->rewind();
        return json_decode($body->getContents(), true);
    }

    /**
     * Requète GET, retourne le corp du résultat
     * @param string $url
     * @param array  $options
     * @return string|null
     */
    public static function getStringBody(string $url, array $options = []): ?string
    {
        $response = self::get($url, $options);
        $body = $response->getBody();
        $body->rewind();
        return $body->getContents();
    }

    /**
     * Requète POST
     * @param string $url
     * @param mixed  $data
     * @param array  $options
     * @return Response
     */
    public static function post(string $url, $data = [], array $options = []): Response
    {
        $options = $options + self::$options + self::$defaultOptions;
        $client = new Client;
        $response = $client->post($url, $data, $options + self::$options + self::$defaultOptions);
        $response->getBody()->rewind();
        return $response;
    }

    /**
     * Requète POST, retourne le corp du résultat en json parsé
     * @param string $url
     * @param array  $data
     * @param array  $options
     * @return array|string|null
     */
    public static function postJson(string $url, $data = [], array $options = [])
    {
        if (!isset($options['headers'])) {
            $options['headers'] = [];
        }
        $options['headers']['Accept'] = 'application/json';
        $response = self::post($url, $data, $options);
        $body = $response->getBody();
        $body->rewind();
        return json_decode($body->getContents(), true);
    }

    /**
     * Requète POST, retourne le corp du résultat
     * @param string $url
     * @param array  $data
     * @param array  $options
     * @return string|null
     */
    public static function postStringBody(string $url, $data = [], array $options = []): ?string
    {
        $response = self::post($url, $data, $options);
        $body = $response->getBody();
        $body->rewind();
        return $body->getContents();
    }

    /**
     * Requète PUT
     * @param string $url
     * @param mixed  $data
     * @param array  $options
     * @return Response
     */
    public static function put(string $url, $data = [], array $options = []): Response
    {
        $options = $options + self::$options + self::$defaultOptions;
        $client = new Client;
        $response = $client->put($url, $data, $options + self::$options + self::$defaultOptions);
        $response->getBody()->rewind();
        return $response;
    }

    /**
     * Requète PUT, retourne le corp du résultat en json parsé
     * @param string $url
     * @param array  $data
     * @param array  $options
     * @return array|string|null
     */
    public static function putJson(string $url, $data = [], array $options = [])
    {
        if (!isset($options['headers'])) {
            $options['headers'] = [];
        }
        $options['headers']['Accept'] = 'application/json';
        $response = self::put($url, $data, $options);
        $body = $response->getBody();
        $body->rewind();
        return json_decode($body->getContents(), true);
    }

    /**
     * Requète PUT, retourne le corp du résultat
     * @param string $url
     * @param array  $data
     * @param array  $options
     * @return string|null
     */
    public static function putStringBody(string $url, $data = [], array $options = []): ?string
    {
        $response = self::put($url, $data, $options);
        $body = $response->getBody();
        $body->rewind();
        return $body->getContents();
    }

    /**
     * Requète DELETE
     * @param string $url
     * @param mixed  $data
     * @param array  $options
     * @return Response
     */
    public static function delete(string $url, $data = [], array $options = []): Response
    {
        $options = $options + self::$options + self::$defaultOptions;
        $client = new Client;
        $response = $client->delete($url, $data, $options + self::$options + self::$defaultOptions);
        $response->getBody()->rewind();
        return $response;
    }

    /**
     * Requète DELETE, retourne le corp du résultat en json parsé
     * @param string $url
     * @param array  $data
     * @param array  $options
     * @return array|string|null
     */
    public static function deleteJson(string $url, $data = [], array $options = [])
    {
        if (!isset($options['headers'])) {
            $options['headers'] = [];
        }
        $options['headers']['Accept'] = 'application/json';
        $response = self::delete($url, $data, $options);
        $body = $response->getBody();
        $body->rewind();
        return json_decode($body->getContents(), true);
    }

    /**
     * Requète DELETE, retourne le corp du résultat
     * @param string $url
     * @param array  $data
     * @param array  $options
     * @return string|null
     */
    public static function deleteStringBody(string $url, $data = [], array $options = []): ?string
    {
        $response = self::delete($url, $data, $options);
        $body = $response->getBody();
        $body->rewind();
        return $body->getContents();
    }
}
