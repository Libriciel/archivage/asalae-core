<?php
/**
 * AsalaeCore\Utility\Config
 */

namespace AsalaeCore\Utility;

use Cake\Core\Configure;
use Cake\Utility\Hash;
use Exception;

/**
 * Gestionnaire de configuration
 *
 * @category Utility
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Config extends Configure
{
    const LIBRICIEL_CONFIG_PATH = '/data/config/app_local.json';
    const GENERIC_CONFIG_PATH = CONFIG . 'app_local.json';
    const DEFAULT_CONFIG_PATH = CONFIG . 'app_default.php';

    /**
     * @var string chemin absolu vers le fichier de configuration
     */
    private static $pathToLocal;

    /**
     * @var array configuration par défaut (app_default.php)
     */
    private static $defaultConfig;

    /**
     * @var array configurations additionnelles (Config.files)
     */
    private static $additionalConfig;

    /**
     * Donne le chemin absolu vers le fichier de configuration
     * @return string
     */
    public static function getPathToLocal(): string
    {
        if (empty(self::$pathToLocal)) {
            $pathToLocalConfig = self::read('App.paths.path_to_local_config');
            if (is_readable($pathToLocalConfig)) {
                self::$pathToLocal = include $pathToLocalConfig;
            } elseif (is_readable(self::LIBRICIEL_CONFIG_PATH)) {
                self::$pathToLocal = self::LIBRICIEL_CONFIG_PATH;
            } elseif (is_readable(CONFIG . 'app_local.json')) {
                self::$pathToLocal = CONFIG . 'app_local.json';
            } else {
                self::$pathToLocal = '';
            }
        }
        return self::$pathToLocal;
    }

    /**
     * Donne la configuration par défaut
     * @param string|null $var     Variable to obtain. Use '.' to access array elements.
     * @param mixed       $default The return value when the configure does not exist
     * @return mixed Value stored in configure, or null.
     */
    public static function readDefault($var = null, $default = null)
    {
        if (self::$defaultConfig) {
            $config = self::$defaultConfig;
        } elseif (is_readable(self::DEFAULT_CONFIG_PATH)) {
            $config = include self::DEFAULT_CONFIG_PATH;
            self::$defaultConfig = $config;
        } else {
            $config = [];
        }
        return $var === null ? $config : Hash::get($config, $var, $default);
    }

    /**
     * Donne la configuration local
     * @param string|null $var     Variable to obtain. Use '.' to access array elements.
     * @param mixed       $default The return value when the configure does not exist
     * @return mixed Value stored in configure, or null.
     */
    public static function readLocal($var = null, $default = null)
    {
        if (is_readable(self::getPathToLocal())) {
            $config = json_decode(file_get_contents(self::getPathToLocal()), true);
            return $var === null ? $config : Hash::get($config, $var, $default);
        }
        return [];
    }

    /**
     * Donne la configuration supplémentaire (Dans Config.files)
     * @param string|null $var     Variable to obtain. Use '.' to access array elements.
     * @param mixed       $default The return value when the configure does not exist
     * @return mixed Value stored in configure, or null.
     * @throws Exception
     */
    public static function readAdditional($var = null, $default = null)
    {
        if (self::$additionalConfig) {
            $config = self::$additionalConfig;
        } else {
            $configs = [];
            $workdir = getcwd();
            chdir(rtrim(CONFIG, DS));
            foreach (self::readLocal('Config.files', []) as $path) {
                if (!is_file($path)) {
                    throw new Exception("Config file not found");
                }
                $configs[] = include $path;
            }
            if (count($configs) > 1) {
                $config = call_user_func_array([Hash::class, "merge"], $configs);
            } elseif (count($configs) === 1) {
                $config = current($configs);
            } else {
                $config = [];
            }
            chdir($workdir);
            self::$additionalConfig = $config;
        }
        return $var === null ? $config : Hash::get($config, $var, $default);
    }

    /**
     * Donne toute la configuration
     * @param string|null $var     Variable to obtain. Use '.' to access array elements.
     * @param mixed       $default The return value when the configure does not exist
     * @return mixed Value stored in configure, or null.
     * @throws Exception
     */
    public static function readAll($var = null, $default = null)
    {
        $config = Hash::merge(
            self::readDefault(null, []),
            self::readLocal(null, []),
            self::readAdditional(null, [])
        );
        return $var === null ? $config : Hash::get($config, $var, $default);
    }

    /**
     * Supprime le cache static
     */
    public static function reset()
    {
        self::$pathToLocal = null;
        self::$defaultConfig = null;
        self::$additionalConfig = null;
    }

    /**
     * Recharge la config de la même façon que dans le bootstrap.php
     * @return void
     * @throws Exception
     */
    public static function reload()
    {
        Configure::load('app_default', 'default', false);
        $pathToLocalConfig = Configure::read('App.paths.path_to_local_config');
        if (is_readable($pathToLocalConfig)) {
            $json = include $pathToLocalConfig;
        } elseif (is_readable('/data/config/app_local.json')) {
            $json = '/data/config/app_local.json';
        } elseif (is_readable(CONFIG . 'app_local.json')) {
            $json = CONFIG . 'app_local.json';
        }
        if (!empty($json) && is_file($json)) {
            $local = json_decode(file_get_contents($json), true);
            $configs = [
                Configure::read(),
                $local
            ];
            $workdir = getcwd();
            chdir(CONFIG);
            foreach ($local['Config']['files'] ?? [] as $path) {
                if (!is_file($path)) {
                    throw new Exception("Config file not found");
                }
                $configs[] = include $path;
            }
            $merged = call_user_func_array(["Cake\Utility\Hash", "merge"], $configs);
            Configure::write($merged);
            chdir($workdir);
        }
    }

    /**
     * Modifi la config et fait persister la modification
     * @param      $config
     * @param null $value
     * @return void
     */
    public static function writeAndSave($config, $value = null): void
    {
        if (!is_array($config)) {
            $config = [$config => $value];
        }
        $pathToLocal = self::getPathToLocal();
        $localConfig = json_decode(file_get_contents($pathToLocal), true);

        foreach ($config as $name => $valueToInsert) {
            $localConfig = Hash::insert($localConfig, $name, $valueToInsert);
            static::$_values = Hash::insert(static::$_values, $name, $valueToInsert);
        }

        ksort($localConfig);

        file_put_contents($pathToLocal, json_encode($localConfig, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));
    }
}
