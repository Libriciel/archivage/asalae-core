<?php

/**
 * AsalaeCore\Utility\FormatError
 * @noinspection PhpInternalEntityUsedInspection - HtmlFormatter = internal
 */

namespace AsalaeCore\Utility;

use AsalaeCore\Error\StatelessHtmlFormatter;
use AsalaeCore\Factory\Utility;
use Cake\Datasource\EntityInterface;
use Cake\Error\Debug\HtmlFormatter;
use Cake\Error\Debugger;
use Cake\Form\Form;
use Cake\Log\Log;
use Cake\ORM\Entity;
use Cake\Utility\Hash;
use Exception;

/**
 * Permet un affichage facilité d'erreurs communes
 *
 * @category Utility
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class FormatError
{
    /**
     * Affiche la liste des erreurs d'une entité sous la forme:
     * field1: ('foo')
     *     - error1
     *     - error2
     * field2: (null)
     *     - error3
     * ...
     * @param EntityInterface|Entity $entity
     * @param bool                   $withValue
     * @return string
     */
    public static function entityErrors(EntityInterface $entity, bool $withValue = true): string
    {
        $output = '';
        foreach ($entity->getErrors() as $field => $errors) {
            if ($withValue) {
                $value = $entity->isAccessible($field) && !in_array($field, $entity->getHidden())
                    ? Debugger::exportVarAsPlainText($entity->getInvalidField($field), 1)
                    : '******';
                $output .= $field . ': (' . $value . ')' . PHP_EOL;
            } else {
                $output .= $field . ':' . PHP_EOL;
            }

            foreach (Hash::flatten($errors) as $key => $error) {
                $msg = '    - ' . $error;
                if (strpos($key, '.') !== false) {
                    $msg .= ' (' . substr($key, 0, strrpos($key, '.')) . ')';
                }
                $output .= $msg . PHP_EOL;
            }
        }
        return trim($output);
    }

    /**
     * Affiche la liste des erreurs d'une entité sous la forme:
     * field1: ('foo')
     *     - error1
     *     - error2
     * field2: (null)
     *     - error3
     * ...
     * @param Form $form
     * @param bool $withValue
     * @return string
     */
    public static function formErrors(Form $form, bool $withValue = true): string
    {
        $formatter = Debugger::getInstance()->getExportFormatter();
        if ($formatter instanceof HtmlFormatter) {
            $newFormatter = new StatelessHtmlFormatter();
            Debugger::getInstance()->setConfig('exportFormatter', $newFormatter);
        }
        $output = '';
        foreach ($form->getErrors() as $field => $errors) {
            if ($withValue) {
                $value = Debugger::exportVar($form->getData($field), 1);
                $output .= $field . ': (' . $value . ')' . PHP_EOL;
            } else {
                $output .= $field . ':' . PHP_EOL;
            }

            foreach (Hash::flatten($errors) as $key => $error) {
                $msg = '    - ' . $error;
                if (strpos($key, '.') !== false) {
                    $msg .= ' (' . substr($key, 0, strrpos($key, '.')) . ')';
                }
                $output .= $msg . PHP_EOL;
            }
        }
        Debugger::getInstance()->setConfig('exportFormatter', $formatter);
        return trim($output);
    }

    /**
     * Donne un moyen d'identifier une entité
     * exemple:
     * "Archives:56 - sa_56"
     * @param EntityInterface $entity
     * @return string
     */
    public static function entity(EntityInterface $entity): string
    {
        $output = ($source = $entity->getSource()) ? $source . ':' : '';
        $output .= $entity->id ?: '<null>';
        $identifier = false;
        foreach ($entity->toArray() as $field => $value) {
            if (is_string($value) && strpos($field, 'identifier') !== false) {
                $output .= ' - ' . $value;
                $identifier = true;
                break;
            }
        }
        if (!$identifier && ($name = $entity->get('name'))) {
            $output .= ' - ' . $name;
        }
        return $output;
    }

    /**
     * Ecrit un message sur les erreur d'une entité dans les logs
     * @param EntityInterface $entity
     * @param bool            $withValue
     * @param bool            $withPrefix
     * @return string
     * @throws Exception
     */
    public static function logEntityErrors(
        EntityInterface $entity,
        bool $withValue = true,
        bool $withPrefix = true
    ): string {
        $errors = self::entityErrors($entity, $withValue);
        if (!$errors) {
            return '';
        }
        if ($withPrefix) {
            $output = __("Echec de validation de l'entité: {0}", self::entity($entity));
            $output .= PHP_EOL . $errors;
        } else {
            $output = $errors;
        }
        /** @var Log $Debugger */
        $Debugger = Utility::get(Log::class);
        $Debugger->debug($output);
        return $output;
    }

    /**
     * Ecrit un message sur les erreur d'une entité dans les logs
     * @param Form $form
     * @param bool $withValue
     * @param bool $withPrefix
     * @return string
     * @throws Exception
     */
    public static function logFormErrors(
        Form $form,
        bool $withValue = true,
        bool $withPrefix = true
    ): string {
        $errors = self::formErrors($form, $withValue);
        if (!$errors) {
            return '';
        }
        if ($withPrefix) {
            $output = __("Echec de validation du formulaire");
            $output .= PHP_EOL . $errors;
        } else {
            $output = $errors;
        }
        /** @var Log $Debugger */
        $Debugger = Utility::get(Log::class);
        $Debugger->debug($output);
        return $output;
    }
}
