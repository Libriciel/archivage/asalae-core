<?php
/**
 * AsalaeCore\Utility\Json
 */

namespace AsalaeCore\Utility;

/**
 * Permet d'éffectuer des opérations json en économisant la mémoire
 *
 * @category Utility
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Json
{
    /**
     * Renvoi un objet json
     * @param mixed $data
     * @param int   $options
     * @param int   $depth   Set the maximum depth. Must be greater than zero.
     * @return string
     */
    public static function encode(&$data, $options = 0, $depth = 512): string
    {
        if (is_array($data)) {
            $i = 0;
            $assos = false;
            foreach ($data as $key => $value) {
                if ($key !== $i) {
                    $assos = true;
                    break;
                }
                $i++;
            }
            if ($depth === 0) {
                return $assos ? '{}' : '[]';
            }
            $output = $assos ? '{' : '[';
            $first = true;
            foreach ($data as $key => $value) {
                if (!$first) {
                    $output .= ',';
                }
                if ($assos) {
                    $output .= '"'.addcslashes($key, '"\\').'":';
                }
                $output .= self::encode($data[$key], $options, $depth -1);
                $first = false;
            }
            $output .= $assos ? '}' : ']';
            return $output;
        } else {
            return json_encode($data, $options, $depth -1);
        }
    }

    /**
     * Renvoi un json decode
     * @param string $json
     * @param bool   $assoc
     * @param int    $depth
     * @param int    $options
     * @return mixed
     */
    public static function decode($json, $assoc = false, $depth = 512, $options = 0)
    {
        return json_decode($json, $assoc, $depth, $options);
    }
}
