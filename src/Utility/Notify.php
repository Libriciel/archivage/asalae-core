<?php
/**
 * AsalaeCore\Utility\Notify
 */

namespace AsalaeCore\Utility;

use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;
use DateTime;
use ZMQ;
use ZMQContext;
use ZMQSocket;
use ZMQSocketException;

/**
 * Utilitaire d'envoi des notifications
 *
 * @category    Utility
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2017, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Notify
{
    /**
     * @var Notify|null
     */
    public static ?Notify $instance = null;

    /**
     * @var ZMQSocket socket
     */
    protected static $socket;

    /**
     * Donne l'instance
     * @return static
     */
    public static function getInstance(): self
    {
        if (!self::$instance) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    /**
     * Permet d'envoyer des notifications aux utilisateurs
     *
     * @param int    $user_id
     * @param array  $tokens
     * @param string $message text affiché à l'utilisateur (HTML)
     * @param string $class   CSS
     * @return array
     * @throws ZMQSocketException
     */
    public static function send(
        int $user_id,
        array $tokens = [],
        string $message = '',
        string $class = 'alert-info'
    ): array {
        $data = [
            'user_id' => $user_id,
            'text' => $message,
            'css_class' => $class,
            'created' => new DateTime('now')
        ];

        $Notifications = TableRegistry::getTableLocator()->get('Notifications');
        $notif = $Notifications->newEntity($data);
        $Notifications->save($notif);
        $data['id'] = $notif->get('id');

        if (!$tokens) {
            $Sessions = TableRegistry::getTableLocator()->get('Sessions');
            $query = $Sessions->find()->select(['token'])->where(['user_id' => $user_id]);
            /** @var EntityInterface $session */
            foreach ($query as $session) {
                static::emit('user_'.$user_id.'_'.$session->get('token'), $data);
            }
        } else {
            foreach ($tokens as $token) {
                static::emit('user_'.$user_id.'_'.$token, $data);
            }
        }
        return $data;
    }

    /**
     * Permet d'envoyer un message sur le serveur Ratchet
     *
     * @param string $chanel
     * @param mixed  $message
     * @throws ZMQSocketException
     */
    public static function emit(string $chanel, $message)
    {
        $emit = [
            'category' => $chanel,
            'message' => $message
        ];
        static::getInstance()->getSocket()->send(json_encode($emit), ZMQ::MODE_DONTWAIT);
    }

    /**
     * Donne le socket ZMQ
     * @return ZMQSocket
     * @throws ZMQSocketException
     */
    protected function getSocket(): ZMQSocket
    {
        if (empty(static::$socket)) {
            $config = Configure::read('Ratchet.zmq');
            $protocol = $config['protocol'];
            $domain = $config['domain'];
            if ($domain === '0.0.0.0') {
                $domain = 'ratchet';
            }
            $port = $config['port'];
            if (($ip = gethostbyname($domain)) !== $domain) {
                $domain = $ip;
            }
            $dsn = "$protocol://$domain:$port";
            $context = new ZMQContext();
            static::$socket = $context->getSocket(ZMQ::SOCKET_PUSH);
            static::$socket->setSockOpt(ZMQ::SOCKOPT_LINGER, 30);
            static::$socket->setSockOpt(ZMQ::SOCKOPT_MAXMSGSIZE, 255);
            static::$socket->connect($dsn);
        }
        return static::$socket;
    }
}
