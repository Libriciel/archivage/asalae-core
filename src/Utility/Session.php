<?php
/**
 * AsalaeCore\Utility\Session
 */

namespace AsalaeCore\Utility;

use AsalaeCore\Factory\Utility;
use Cake\Datasource\EntityInterface;
use Cake\Log\Log;
use Cake\ORM\TableRegistry;
use Cake\Utility\Security;
use Cake\Http\Session as CakeSession;
use Exception;
use ZMQSocketException;

/**
 * Permet de manipuler la session
 *
 * @category Utility
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2017, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Session
{
    public const SESSION_KEY = 'Auth';
    public const ACL_KEY = 'Acl';

    /**
     * @var string clef de sécurité
     */
    private static $key;

    /**
     * Permet d'obtenir la clef de sécurité
     * @return string
     */
    protected static function getSecurityKey(): string
    {
        if (!self::$key) {
            self::$key = md5(__METHOD__); // NOSONAR permet simplement d'avoir la bonne taille
        }
        return self::$key;
    }

    /**
     * Permet d'envoyer à un utilisateur, un ordre de reconstruction de la
     * session
     * @param int|string $user_id
     * @throws ZMQSocketException
     * @throws Exception
     */
    public static function emitReset($user_id)
    {
        $json = json_encode(
            [
                'id' => $user_id,
                'key' => bin2hex(random_bytes(8)),
                'expire' => strtotime(date('Y-m-d H:i:s').' +1 minute')
            ]
        );
        $message = base64_encode(Security::encrypt($json, static::getSecurityKey()));
        Notify::emit('session_'.$user_id, $message);
    }

    /**
     * Effectue le reset commandé par self::emitReset()
     * @param CakeSession   $session
     * @param string        $encrypted
     * @param callable|null $getUserFn user_id en paramètre, doit retourner
     *                                 l'user en array
     * @return bool
     * @see self::getGenericSessionUser()
     */
    public static function doReset(
        CakeSession $session,
        string $encrypted,
        callable $getUserFn = null
    ): bool {
        $decoded = base64_decode($encrypted);
        $data = json_decode(
            Security::decrypt($decoded, static::getSecurityKey()),
            true
        );
        $resets = (array)$session->read('reset_keys') ?: [];
        if ((string)$data['id'] === (string)$session->read(self::SESSION_KEY . '.id')
            && !in_array($data['key'], $resets)
            && $data['expire'] > strtotime(date('Y-m-d H:i:s'))
        ) {
            if (!$getUserFn) {
                $getUserFn = [self::class, 'getGenericSessionUser'];
            }
            $user = $getUserFn((int)$data['id']);
            $session->write(self::ACL_KEY, []);
            $session->write(self::SESSION_KEY, $user);
            $session->write('reset_keys', array_merge($resets, [$data['key']]));
            return true;
        }
        return false;
    }

    /**
     * Donne l'utilisateur à placer dans la clé Auth (de la session)
     * @param int $user_id
     * @return EntityInterface
     */
    private static function getGenericSessionUser(int $user_id): EntityInterface
    {
        return TableRegistry::getTableLocator()->get('Users')
            ->find()
            ->where(['Users.id' => $user_id])
            ->contain(
                [
                    'OrgEntities' => [
                        'ArchivalAgencies' => ['Configurations'],
                        'TypeEntities',
                    ],
                    'Ldaps',
                    'Roles',
                ]
            )
            ->first();
    }

    /**
     * Unserialize le data d'une session
     * NOTE: très difficile à parser: $key|$serialized$key|$serialized...
     * Le seul séparateur est le | mais il peut exister dans une chaine
     * du serialized. Le seul moyen serait de compter le nombre de caractères
     * dans serialied: ex: a:1:{s:5:"token";}
     * => un array avec une valeur: un string de 5 chars
     * PS: le ";" et le "}" sont des chars autorisés dans $key
     * @param string $serialized
     * @return array
     * @throws Exception
     */
    public static function unserialize(string $serialized): array
    {
        if (session_status() === PHP_SESSION_ACTIVE) {
            $currentSession = session_encode();
            $_SESSION = [];
            session_decode($serialized);
            $decodedSession = $_SESSION;
            $_SESSION = [];
            session_decode($currentSession);
            return $decodedSession;
        } else {
            $result = Utility::get('Exec')
                ->command(
                    ROOT.DS.'bin'.DS.'session_unserialize.php',
                    base64_encode($serialized)
                );
            if ($unserialized = unserialize($result->stdout)) {
                return $unserialized;
            }
            Log::error(sprintf("%s\n- unable to unserialize", base64_encode($serialized)));
            return [];
        }
    }

    /**
     * Serialize de session
     * @param array $data
     * @return string
     * @throws Exception
     */
    public static function serialize(array $data): string
    {
        if (session_status() === PHP_SESSION_ACTIVE) {
            $currentSession = session_encode();
            $_SESSION = $data;
            $serialized = session_encode();
            $_SESSION = [];
            session_decode($currentSession);
            return $serialized;
        } else {
            $result = Utility::get('Exec')
                ->command(
                    ROOT.DS.'bin'.DS.'session_serialize.php',
                    base64_encode(serialize($data))
                );
            if ($serialized = trim($result->stdout)) {
                return $serialized;
            }
            Log::error(sprintf("%s\n- unable to serialize", base64_encode(serialize($data))));
            return '';
        }
    }
}
