<?php
/**
 * AsalaeCore\Utility\ValidatorXml
 */

namespace AsalaeCore\Utility;

use Cake\Core\Configure;
use DOMDocument;
use InvalidArgumentException;

/**
 * Permet de valider un fichier xml par le biais d'un xsd ou rng
 *
 * @category Utility
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ValidatorXml
{
    /**
     * @var array|mixed Configuration de la classe, pour choisir les méthodes de validation
     */
    private $config = [];

    /**
     * @var bool|string chemin vers le fichier de validation
     */
    private $validatorFilePath;

    /**
     * @var string xsd ou rng
     */
    private $type;

    /**
     * @var array liste des erreurs
     */
    public $errors = [];

    /**
     * @var array description des erreurs
     */
    public $errorsDetails = [];

    /**
     * ValidatorXml constructor.
     * @param string|resource $file
     * @param array           $config
     */
    public function __construct($file, array $config = [])
    {
        $this->setSchema($file, $config);
    }

    /**
     * Défini le schema à partir duquel faire la validation
     * @param string|resource $file
     * @param array           $config
     */
    public function setSchema($file, array $config = [])
    {
        if ($this->validatorFilePath === $file) {
            return;
        } elseif (is_string($file)) {
            $this->validatorFilePath = $file;
        } elseif (is_resource($file)) {
            $this->validatorFilePath = tempnam(sys_get_temp_dir(), "ValidatorXml-");
            file_put_contents($this->validatorFilePath, $file);
            rewind($file);
        } else {
            throw new InvalidArgumentException('$file must be a string or a resource');
        }

        $dom = new DOMDocument;
        $dom->load($this->validatorFilePath);
        $prev = libxml_use_internal_errors(true);
        libxml_clear_errors();
        $this->type = $dom->relaxNGValidate(WWW_ROOT.'xmlSchemas'.DS.'relaxng'.DS.'relaxng.rng')
            ? 'rng'
            : 'xsd';
        libxml_use_internal_errors($prev);
        $this->config = $config + Configure::read('Validation', []) + [
            'relaxng' => [
                'use_domdocument' => false,
                'cmd' => 'java -jar "'.RESOURCES.'jing.jar"',
            ],
            'schema' => [
                'use_domdocument' => true,
                'cmd' => 'xmllint --noout --schema'
            ]
        ];
    }

    /**
     * Valide le fichier, les erreurs sont stocké dans $this->errors
     * @param string|resource $file
     * @return bool
     */
    public function validate($file)
    {
        if (is_string($file)) {
            $path = $file;
        } elseif (is_resource($file)) {
            $path = tempnam(sys_get_temp_dir(), "ValidatorXml-");
            file_put_contents($path, $file);
            rewind($file);
        } else {
            throw new InvalidArgumentException('$file must be a string or a resource');
        }

        $config = $this->type === 'xsd' ? $this->config['schema'] : $this->config['relaxng'];
        return $config['use_domdocument']
            ? $this->validateByDom($path)
            : $this->validateByExternal($path);
    }

    /**
     * Effectue la validation d'un fichier par la classe DOMDocument
     * @param string $path
     * @return bool
     */
    private function validateByDom(string $path): bool
    {
        $dom = new DOMDocument;
        $dom->load($path, XML_PARSE_BIG_LINES);
        $initial = libxml_use_internal_errors();
        libxml_use_internal_errors(true);
        $success = $this->type === 'xsd'
            ? @$dom->schemaValidate($this->validatorFilePath)
            : @$dom->relaxNGValidate($this->validatorFilePath);
        if (!$success) {
            foreach (libxml_get_errors() as $error) {
                $err = '';
                switch ($error->level) {
                    case LIBXML_ERR_WARNING:
                        $err = "Warning $error->code.";
                        break;
                    case LIBXML_ERR_ERROR:
                        $err = "Error $error->code.";
                        break;
                    case LIBXML_ERR_FATAL:
                        $err = "Fatal Error $error->code.";
                        break;
                }
                $this->errorsDetails[] = [
                    'level' => $error->level,
                    'code' => $error->code,
                    'line' => $error->line,
                    'column' => $error->column,
                    'message' => trim($error->message)
                ];
                $message = __(
                    "{0}. Ligne {1}, colonne {2} -> {3}{4}",
                    $err,
                    $error->line,
                    $error->column,
                    trim($error->message),
                    '<br/>'
                );
                $this->errors[] = $message;
            }
            libxml_clear_errors();
        }
        libxml_use_internal_errors($initial);
        return $success;
    }

    /**
     * Validation par jing ou xmllint
     * @param string $path
     * @return bool
     */
    private function validateByExternal(string $path): bool
    {
        $config = $this->type === 'xsd' ? $this->config['schema'] : $this->config['relaxng'];
        $exec = new Exec;
        $results = $exec->command($config['cmd'], $this->validatorFilePath, $path);
        $success = $results->success;
        if (!$success) {
            switch ($results->code) {
                case 1:
                    $level = "Unclassified";
                    break;
                case 2:
                    $level = "Error in DTD";
                    break;
                case 3:
                case 4:
                    $level = "Validation error";
                    break;
                case 5:
                    $level = "Error in schema compilation";
                    break;
                case 6:
                    $level = "Error writing output";
                    break;
                case 7:
                    $level = "Error in pattern (generated when --pattern option is used)";
                    break;
                case 8:
                    $level = "Error in Reader registration (generated when --chkregister option is used)";
                    break;
                case 9:
                    $level = "Out of memory error";
                    break;
                default:
                    $level = 'Unknown';
            }
            // Corrige les problèmes d'encodage
            $this->errors = array_map(
                function ($v) use ($path) {
                    $v = str_replace($path, 'transfer-message.xml', $v);
                    return utf8_decode($v);
                },
                explode(PHP_EOL, trim($results->stdout))
            );

            foreach (explode(PHP_EOL, trim($results->stdout)) as $error) {
                $error = str_replace($path, 'transfer-message.xml', $error);
                $error = utf8_decode($error);
                $error = utf8_encode($error);
                $line = 0;
                $col = 0;
                if (preg_match('/[\w\-.]+:(\d+):(\d+)/', $error, $m)) {
                    [, $line, $col] = $m;
                }

                $this->errorsDetails[] = [
                    'level' => $level,
                    'code' => $results->code,
                    'line' => $line,
                    'column' => $col,
                    'message' => trim($error)
                ];
                $message = __("{0}. Ligne {1}, colonne {2} -> {3}{4}", $level, $line, $col, trim($error), '<br/>');
                $this->errors[] = $message;
            }
        }
        return $success;
    }
}
