<?php
/**
 * AsalaeCore\Utility\Check
 */

namespace AsalaeCore\Utility;

use AsalaeCore\Factory\Utility;
use AsalaeCore\Utility\Antivirus\AntivirusInterface;
use AsalaeCore\Utility\Antivirus\Clamav;
use Beanstalk\Utility\Beanstalk;
use Cake\Database\Connection;
use Cake\Database\Exception\MissingConnectionException;
use Cake\Datasource\ConnectionManager;
use Composer\Semver\Semver;
use Exception;
use PDOException;
use Cake\Core\Configure;

/**
 * Utilitaire de vérification de l'application
 *
 * @category    Utility
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2017, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
abstract class Check
{

    /**
     * Fonctions shell qui doivent être installé pour un fonctionnement optimal
     *
     * @var array   la valeur --help est défini par défaut,
     *              indiquer une autre commande si besoin (doit renvoyer un code 0)
     */
    public static $prerequis = [
        'bzip2',
        'catdoc',
        'ffmpeg',
        'ghostscript',
        'gzip',
        'HandBrakeCLI',
        'imagemagick' => 'identify',
        'java',
        'libreoffice',
        'libxml2-utils' => 'xmllint',
        'poppler-utils' => 'pdfinfo',
        'rar',
        'sf',
        'unoconv',
        'webp' => 'dwebp',
        'wkhtmltopdf',
        'xlsx2csv',
        'xvfb-run',
        'zip',
    ];

    /**
     * @var \string[][] contraintes sur la version d'un prérequis
     */
    public static $prerequisVersion = [
        'bzip2' => [ // bzip2, a block-sorting file compressor.  Version 1.0.8, 13-Jul-2019.
            'command' => 'bzip2 --version 2>&1 <&-',
            'regex' => '/Version (\d+\.\d+(?:\.\d)?(?:\.\d)?)/',
            'constrain' => '^1.0',
        ],
        'catdoc' => [ // Catdoc Version 0.95
            'command' => 'catdoc -V',
            'regex' => '/Version (\d+\.\d+(?:\.\d)?(?:\.\d)?)/',
            'constrain' => '^0.95',
        ],
        'ffmpeg' => [ // ffmpeg version 4.2.4-1ubuntu0.1 Copyright (c) 2000-2020 the FFmpeg developers
            'command' => 'ffmpeg -version',
            'regex' => '/ffmpeg version (\d+\.\d+(?:\.\d)?(?:\.\d)?)/',
            'constrain' => '^4.2',
        ],
        'ghostscript' => [ // GPL Ghostscript 9.50 (2019-10-15)
            'command' => 'ghostscript -v',
            'regex' => '/GPL Ghostscript (\d+\.\d+(?:\.\d)?(?:\.\d)?)/',
            'constrain' => '^9.27',
        ],
        'gzip' => [ // gzip 1.10
            'command' => 'gzip --version',
            'regex' => '/gzip (\d+\.\d+(?:\.\d)?(?:\.\d)?)/',
            'constrain' => '^1.9',
        ],
        'HandBrakeCLI' => [ // HandBrake 1.3.1
            'command' => 'HandBrakeCLI --version 2>&1',
            'regex' => '/HandBrake (\d+\.\d+(?:\.\d)?(?:\.\d)?)/',
            'constrain' => '^1.3',
        ],
        'imagemagick' => [ // Version: ImageMagick 6.9.10-23 Q16 x86_64 20190101 https://imagemagick.org
            'command' => 'identify --version',
            'regex' => '/ImageMagick (\d+\.\d+(?:\.\d)?(?:\.\d)?)/',
            'constrain' => '^6.9',
        ],
        'java' => [ // openjdk 11.0.9.1 2020-11-04
            'command' => 'java --version',
            'regex' => '/openjdk "?(\d+\.\d+(?:\.\d)?(?:\.\d)?)/',
            'constrain' => '>=1.8',
        ],
        'libreoffice' => [ // LibreOffice 6.4.6.2 40(Build:2)
            'command' => 'libreoffice --version',
            'regex' => '/LibreOffice (\d+\.\d+(?:\.\d)?(?:\.\d)?)/',
            'constrain' => '^6.0 || ^7.0',
        ],
        'libxml2-utils' => [ // xmllint: using libxml version 20910
            'command' => 'xmllint --version 2>&1',
            'regex' => '/version (\d+(?:\.\d+)?(?:\.\d)?(?:\.\d)?)/',
            'constrain' => '*',
        ],
        'poppler-utils' => [ // pdfinfo version 0.86.1
            'command' => 'pdfinfo -v 2>&1',
            'regex' => '/version (\d+\.\d+(?:\.\d)?(?:\.\d)?)/',
            'constrain' => '0.* || 20.* || 22.*',
        ],
        'rar' => [ // RAR 5.50   Copyright (c) 1993-2017 Alexander Roshal   11 Aug 2017
            'command' => 'rar -?',
            'regex' => '/RAR (\d+\.\d+(?:\.\d)?(?:\.\d)?)/',
            'constrain' => '^5.40',
        ],
        'sf' => [ // siegfried 1.9.1
            'command' => 'sf --version',
            'regex' => '/siegfried (\d+\.\d+(?:\.\d)?(?:\.\d)?)/',
            'constrain' => '^1.9',
        ],
        'unoconv' => [ // unoconv 0.7
            'command' => 'unoconv --version',
            'regex' => '/unoconv (\d+\.\d+(?:\.\d)?(?:\.\d)?)/',
            'constrain' => '>=0.7',
        ],
        'webp' => [ // 0.6.1
            'command' => 'dwebp -version',
            'regex' => '/(\d+\.\d+(?:\.\d)?(?:\.\d)?)/',
            'constrain' => '>=0.6',
        ],
        'wkhtmltopdf' => [ // 0.6.1
            'command' => 'wkhtmltopdf --version',
            'regex' => '/(\d+\.\d+(?:\.\d)?(?:\.\d)?)/',
            'constrain' => '>=0.12',
        ],
        'xlsx2csv' => [ // 0.7.5
            'command' => 'xlsx2csv --version',
            'regex' => '/(\d+\.\d+(?:\.\d)?(?:\.\d)?)/',
            'constrain' => '^0.7 || ^0.8',
        ],
        'zip' => [ // This is Zip 3.0 (July 5th 2008), by Info-ZIP.
            'command' => 'zip --version',
            'regex' => '/Zip (\d+\.\d+(?:\.\d)?(?:\.\d)?)/',
            'constrain' => '^3.0',
        ],
    ];

    /**
     * Liste les service utilisés par l'application
     *
     * @var array
     * @deprecated
     */
    public static $services = [
        'apache2',
        'beanstalkd',
        'cron',
    ];

    /**
     * Vérifie la connexion à la base de données
     *
     * @return boolean Vrai si la base de donnée fonctionne
     */
    public static function database(): bool
    {
        try {
            $conn = ConnectionManager::get('default');
            if ($conn instanceof Connection) {
                $conn->query('select 1');
            }
            return true;
        } catch (PDOException|MissingConnectionException $e) {
            return false;
        }
    }

    /**
     * Vérifie la connexion à Beanstalkd
     *
     * @return boolean Vrai si Beanstalkd fonctionne
     */
    public static function beanstalkd(): bool
    {
        return Beanstalk::connected();
    }

    /**
     * Vérifie la connexion à Ratchet
     *
     * @return boolean Vrai si Ratchet fonctionne
     */
    public static function ratchet(): bool
    {
        set_error_handler(
            function () {
            }
        );
        $header = @get_headers(Configure::read('Ratchet.ping'));
        restore_error_handler();
        return !empty($header[0]) && $header[0] !== 'HTTP/1.1 400 Bad Request';
    }

    /**
     * Vérifie la connexion à Clamav
     * @return boolean Vrai si Clamav fonctionne
     * @throws Exception
     */
    public static function clamav(): bool
    {
        /** @var Clamav|AntivirusInterface $clamav */
        $clamav = Utility::get(Configure::read('Antivirus.classname', Clamav::class));
        if ($clamav->ping()) {
            $path = Configure::read('App.paths.data').DS.'test_antivirus.com';
            if (is_file($path)) {
                unlink($path);
            }
            $dir = dirname($path);
            if (!is_dir($dir)) {
                mkdir($dir, 0777, true);
            }
            // NOTE: scanner qh360 -> faux positif sur la chaine entière
            file_put_contents(
                $path,
                'X5O!P%@AP[4\PZX'.(50 + 4).'(P^)7CC)7}$EICAR-STANDARD-ANTIVIRUS-TEST-FILE!$H+H*'.PHP_EOL
            );
            $detectVirus = count($clamav->scan($path)) === 1;
            file_put_contents($path, 'test');
            $detectSafe = count($clamav->scan($path)) === 0;
            unlink($path);
            return $detectVirus && $detectSafe;
        } else {
            return false;
        }
    }

    /**
     * Liste des logiciels non installés
     *
     * @return array d'installations manquante
     */
    public static function missings(): array
    {
        $missing = [];
        $prerequis = Configure::read('Check.prerequis', static::$prerequis);
        foreach ($prerequis as $key => $value) {
            if (is_numeric($key)) {
                $key = $value;
            }
            $bin = escapeshellarg($value);
            exec("command -v $bin 2> /dev/null", $output, $code);

            if ($code !== 0) {
                $missing[] = $key;
            } elseif (isset(self::$prerequisVersion[$key])) {
                $command = self::$prerequisVersion[$key]['command'];
                $m = null;
                $o = null;
                exec($command, $o);
                preg_match(self::$prerequisVersion[$key]['regex'], implode("\n", $o), $m);
                if (!$m || !Semver::satisfies($m[1], self::$prerequisVersion[$key]['constrain'])) {
                    $missing[] = $key;
                }
            }
        }
        return $missing;
    }

    /**
     * Vérifi la présence des classes de module PHP
     *
     * @return array
     */
    public static function missingsExtPhp(): array
    {
        $missing = [];
        foreach (static::extPhp() as $value) {
            if (!extension_loaded($value)) {
                $missing[] = $value;
            }
        }
        return $missing;
    }

    /**
     * Permet de savoir si il faut installer un logiciel
     *
     * @return boolean Vrai si il manque une installation
     */
    public static function needInstall(): bool
    {
        return !empty(static::missings()) || !empty(static::missingsExtPhp());
    }

    /**
     * Vérifi l'existance d'un service
     *
     * @param string $serviceName
     * @return boolean
     */
    public static function serviceExists(string $serviceName): bool
    {
        $exists = false;
        foreach (static::getServicesList() as $service) {
            if (preg_match('/[\s\[\]+\-]+'.$serviceName.'$/', $service)) {
                $exists = true;
                break;
            }
        }
        return $exists;
    }

    /**
     * Garde en mémoire le résultat de la commande "service --status-all"
     * @var array
     */
    private static $servicesList;

    /**
     * Permet d'obtenir la liste des services du systeme
     * @return array
     */
    private static function getServicesList(): array
    {
        if (empty(static::$servicesList)) {
            exec('service --status-all', static::$servicesList);
        }
        return static::$servicesList;
    }

    /**
     * Vérifi qu'un service est en cours de fonctionnement
     * @param string $serviceName
     * @return null|bool    null si le service n'a pas été trouvé,
     *                      vrai ou faux selon si il est lancé ou pas
     */
    public static function serviceIsRunning(string $serviceName)
    {
        $running = null;
        foreach (static::getServicesList() as $service) {
            if (preg_match('/[\s\[\]]+([+\-])[\s\[\]]+'.$serviceName.'$/', $service, $match)) {
                $running = $match[1] === '+';
                break;
            }
        }
        return $running;
    }

    /**
     * Vérifi la cohérence du paramétrage PHP
     *
     * @return array
     */
    public static function php(): array
    {
        $upload_max_filesize = self::getBytes(ini_get('upload_max_filesize'));
        $post_max_size = self::getBytes(ini_get('post_max_size'));

        $chunkSize = (int)Configure::read('Flow.chunkSize');

        return [
            'upload_max_filesize' => $upload_max_filesize >= $chunkSize,
            'post_max_size' => $post_max_size >= $chunkSize,
        ];
    }

    /**
     * Donne la valeur en bytes
     * 4M = 4194304
     * @param string $value
     * @return int
     */
    private static function getBytes(string $value): int
    {
        $val = trim($value);
        $last = strtolower($val[strlen($val)-1]);
        $val = (int)$val;
        switch ($last) {
            /** @noinspection PhpMissingBreakStatementInspection */
            case 'g':
                $val *= 1024;
                // fallthrough no-break
            /** @noinspection PhpMissingBreakStatementInspection */
            // no break
            case 'm':
                $val *= 1024;
                // fallthrough no-break
            // no break
            case 'k':
                $val *= 1024;
        }
        return $val;
    }

    /**
     * Y a t'il un problème avec les paramétrages de PHP ?
     * @return bool
     */
    public static function phpOk()
    {
        return empty(
            array_filter(
                self::php(),
                function ($v) {
                    return !$v;
                }
            )
        );
    }

    /**
     * Test d'écriture sur les dossiers inscriptibles
     * Utile car à cause de apparmor, il est possible d'avoir un
     * is_writable('/tmp') === true, mais de ne pas pouvoir y ajouter un fichier
     * @return array
     */
    public static function writables(): array
    {
        $dirs = array_filter(
            [
                sys_get_temp_dir(),
                TMP,
                LOGS,
                Configure::read('App.paths.data'),
            ]
        );
        $basename = uniqid('writable_', true);
        $writables = [];
        foreach ($dirs as $path) {
            if (!is_dir($path) || !is_writable($path)) {
                $writables[$path] = false;
                continue;
            }
            $filename = rtrim($path, DS).DS.$basename;
            $content = @file_put_contents($filename, 'test');
            if ($content) {
                unlink($filename);
                $writables[$path] = true;
            } else {
                $writables[$path] = false;
            }
        }
        return $writables;
    }

    /**
     * Renvoi true si tous les dossiers sont inscriptibles
     * @return bool
     */
    public static function writableOk(): bool
    {
        foreach (self::writables() as $ok) {
            if (!$ok) {
                return false;
            }
        }
        return true;
    }

    /**
     * Donne la liste des modules php requis
     * @return string[]
     */
    public static function extPhp()
    {
        $composerJson = json_decode(file_get_contents(ROOT.DS.'composer.json'), true);
        $requiredExts = array_filter(
            array_keys($composerJson['require']),
            function ($v) {
                return substr($v, 0, 4) === 'ext-';
            }
        );
        $modulesPhp = array_map(
            function ($v) {
                return substr($v, 4);
            },
            $requiredExts
        );
        sort($modulesPhp);
        return $modulesPhp;
    }
}
