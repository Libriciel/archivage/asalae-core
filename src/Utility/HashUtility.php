<?php
/**
 * AsalaeCore\Utility\HashUtility
 */

namespace AsalaeCore\Utility;

/**
 * Opérations sur le hash
 *
 * @category Utility
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class HashUtility
{
    /**
     * Transforme une chaine mentionné dans un xml en algo de hashage php
     * ex:
     * - sha256
     * - SHA-256
     * - http://www.w3.org/2001/04/xmlenc#sha256
     * @param string $algo
     * @return bool|mixed
     */
    public static function toPhpAlgo(string $algo)
    {
        // cas http://url#algo
        if (strpos($algo, '#') !== false) {
            list(, $algo) = explode('#', $algo);
        }
        $formatedAlgo = strtolower(preg_replace('/[^a-zA-Z\d]+/', '', $algo));
        foreach (hash_algos() as $algo) {
            $formated = strtolower(preg_replace('/[^a-zA-Z\d]+/', '', $algo));
            if ($formatedAlgo === $formated) {
                return $algo;
            }
        }
        return false;
    }

    /**
     * Compare $hash avec $compare, ce dernier peut être binaire et/ou base64
     * ex:
     * HashUtility::hashMatch('accf8b33', 'accf8b33') === true
     * HashUtility::hashMatch('accf8b33', 'rM+LMw==') === true
     * HashUtility::hashMatch('accf8b33', 'YWNjZjhiMzM=') === true
     * @param string $hash
     * @param string $compare
     * @return bool
     */
    public static function hashMatch(string $hash, string $compare)
    {
        if ($hash === $compare) {
            return true;
        }
        $hashb64 = @base64_encode(hex2bin($hash));
        if ($hashb64 === $compare) {
            return true;
        }
        $hashb64hex = @base64_encode($hash);
        return $hashb64hex === $compare;
    }
}
