<?php
/**
 * AsalaeCore\Utility\Ip
 */

namespace AsalaeCore\Utility;

use Cake\Core\Configure;

/**
 * Manipulation de l'ip
 *
 * utilise la clé de configuration Ip.whitelist
 *
 * @category Utility
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Ip
{
    /**
     * Transforme une ipv6 réduite en sa forme complète
     * ex:
     * ::1 = 0000:0000:0000:0000:0000:0000:0000:0001
     * 2340:23:AABA:A01:55:5054:9ABC:ABB0 = 2340:0023:AABA:0A01:0055:5054:9ABC:ABB0
     * 12:88f8:*::1 = 0012:88f8:****:0000:0000:0000:0000:0001
     *
     * @param string $ip
     * @return string
     */
    public static function formatIpV6(string $ip): string
    {
        $parts = explode('::', $ip, 2);
        foreach ($parts as $key => $value) {
            $val = [];
            foreach (explode(':', $value) as $v) {
                $val[] = str_pad(
                    $v ?: 0,
                    4,
                    $v === '*' ? '*' : '0',
                    STR_PAD_LEFT
                );
            }
            $parts[$key] = implode(':', $val);
        }
        if (count($parts) === 2) {
            $beginCount = substr_count($parts[0], ':');
            $endCount = substr_count($parts[1], ':');
            $formatedIp = $parts[0].str_repeat(':0000',  6- $beginCount - $endCount).':'.$parts[1];
        } else {
            $formatedIp = $parts[0];
        }
        return $formatedIp;
    }

    /**
     * Renvoi la whitelist trié par version ip et reformaté
     * @param array|null $ips
     * @return array[]
     */
    public static function whitelist(array $ips = null): array
    {
        $whitelist = ['v4' => [], 'v6' => []];
        $ips = $ips ?? Configure::read('Ip.whitelist', []);
        foreach ($ips as $value) {
            if (strpos($value, '.') !== false) {
                $whitelist['v4'][] = $value;
            } else {
                $whitelist['v6'][] = self::formatIpV6($value);
            }
        }
        return $whitelist;
    }

    /**
     * 0 partir d'une ip donnée, permet de savoir si elle est whitelisté
     * @param string     $ip
     * @param array|null $ips list d'ips ou null pour la conf Ip.whitelist
     * @return bool
     */
    public static function inWhitelist(string $ip, array $ips = null): bool
    {
        $whitelist = self::whitelist($ips);
        if (strpos($ip, ':') !== false) {
            $ip = self::formatIpV6($ip);
            foreach ($whitelist['v6'] as $compare) {
                $compare = str_replace('*', '[0-9a-fA-F]', $compare);
                if (preg_match("/$compare/i", $ip)) {
                    return true;
                }
            }
        } else {
            foreach ($whitelist['v4'] as $compare) {
                $compare = str_replace('*', '\d{1,3}', $compare);
                if (preg_match("/$compare/", $ip)) {
                    return true;
                }
            }
        }
        return false;
    }
}
