<?php
/**
 * AsalaeCore\Utility\XmlUtility
 */

namespace AsalaeCore\Utility;

use Cake\ORM\Entity;
use Craur;
use DOMDocument;
use Exception;

/**
 * Regroupement de méthodes de manipulation de données xml
 *
 * @category    Utility
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2017, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class XmlUtility
{
    /**
     * @var string
     */
    public static $domClass = DOMDocument::class;

    /**
     * Converti un xml en json
     *
     * @param string $xmlString
     * @return string
     * @throws Exception
     */
    public static function xmlStringToJson(string $xmlString): string
    {
        return Craur::createFromXml($xmlString)->toJsonString();
    }

    /**
     * Recréer un xml à partir d'un json (obtenu par self::xmlStringToJson())
     *
     * @param string $json
     * @return string
     */
    public static function jsonToXmlString(string $json): string
    {
        $data = json_decode($json, true);
        return static::arrayToXmlString($data);
    }

    /**
     * Créer un xml à partir d'un array
     * @param array $data
     * @return string
     */
    public static function arrayToXmlString(array $data): string
    {
        if (count($data) > 1) {
            unset($data['oxygen'], $data['xml'], $data['xml-stylesheet']);
        }
        static::deepRemoveComment($data);
        static::recursiveToArray($data);
        $Craur = new Craur($data);

        $dom = new DOMDocument;
        $dom->formatOutput = true;
        $dom->preserveWhiteSpace = false;
        $dom->loadXML($Craur->toXmlString());
        return $dom->saveXML();
    }

    /**
     * Effectue un Entity->toArray() sur toutes les profondeurs
     * @param Entity|array $data
     */
    public static function recursiveToArray(&$data)
    {
        if ($data instanceof Entity) {
            $data = $data->toArray();
        }
        if (is_array($data)) {
            foreach ($data as $key => $values) {
                self::recursiveToArray($data[$key]);
            }
        }
    }

    /**
     * Retire les "commentaire" du data pour le préparer à être parsé en xml à
     * cause d'un "bug" de conversion (le commentaire est transformé en json avec
     * une valeur vide et n'est pas retransformé en balise commentaire)
     *
     * @param array $data issue d'un xml transformé en json
     */
    private static function deepRemoveComment(array &$data)
    {
        foreach ($data as $key => $values) {
            if ($key === '#comment') {
                unset($data[$key]);
                continue;
            }
            if (is_array($values)) {
                self::deepRemoveComment($data[$key]);
            }
        }
    }

    /**
     * Permet de valider un xml sans afficher d'erreurs
     *
     * @param string $xmlString     contenu d'un xml
     * @param string $xsd           path ou contenu d'un xsd
     * @param bool   $displayErrors Affiche ou pas les erreurs
     * @return bool
     */
    public static function validateXmlString(string $xmlString, string $xsd, bool $displayErrors = false): bool
    {
        /** @var DOMDocument $xml */
        $xml = new self::$domClass;
        $xml->loadXML($xmlString);
        if (is_file($xsd)) {
            return $displayErrors
                ? $xml->schemaValidate($xsd)
                : @$xml->schemaValidate($xsd);
        } else {
            return $displayErrors
                ? $xml->schemaValidateSource($xsd)
                : @$xml->schemaValidateSource($xsd);
        }
    }

    /**
     * Permet de valider un xml transformé en json
     *
     * @param string $json          xml transformé en
     *                              json
     * @param string $xsd           path ou contenu d'un xsd
     * @param bool   $displayErrors Affiche ou pas les erreurs
     * @return bool
     */
    public static function validateJson(string $json, string $xsd, bool $displayErrors = false): bool
    {
        return static::validateXmlString(static::jsonToXmlString($json), $xsd, $displayErrors);
    }
}
