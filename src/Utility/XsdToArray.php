<?php
/**
 * AsalaeCore\Utility\XsdToArray
 * @noinspection PhpDeprecationInspection - self
 */

namespace AsalaeCore\Utility;

use Cake\I18n\I18n;
use Cake\Utility\Hash;
use Exception;

/**
 * Rend un xsd exploitable par javascript
 *
 * @category   Utility
 *
 * @author     Libriciel SCOP <contact@libriciel.coop>
 * @copyright  (c) 2017, Libriciel
 * @license    https://www.gnu.org/licenses/agpl-3.0.txt
 * @deprecated a retirer lorsque l'EAC-CPF aura eu sa refonte
 */
class XsdToArray
{
    /**
     * @var string Préfix ou namespace précédent le nom d'un noeud
     */
    private static $prefix = '';

    /**
     * Construit un array à partir d'un xsd
     *
     * @param string $xsd Chemin vers le xsd
     * @return array
     * @throws Exception
     */
    public static function build(string $xsd): array
    {
        if (is_file($xsd)) {
            $xsd = file_get_contents($xsd);
        }

        $data = json_decode(XmlUtility::xmlStringToJson($xsd), true);
        unset($data['#comment']);
        $key = array_keys($data)[0];
        [static::$prefix, ] = explode(':', $key);
        static::$prefix = static::$prefix ? static::$prefix.':' : '';
        $schema = $data[$key];
        unset($schema[static::$prefix.'import']);

        static::reformatSchema($schema);
        static::flattenizeSchema($schema);
        $elements = static::simplifyElements($schema);

        return ['elements' => $elements];
    }

    /**
     * Donne les références du premier niveau d'element nommé :
     * ex:  [['complexType' => [...], 'name' => 'exemple'], ...];
     *      donnera : $refs = ['parent' => [...], 'child' => ['name' => 'exemple', ...]]
     *
     * @param array $schema array à parcourir
     * @param array $refs   liste de résultat, laisser
     *                      vide
     * @return array les références
     */
    private static function getFirstLevelRefs(array &$schema, array &$refs = []): array
    {
        foreach ($schema as $key => $values) {
            if (!empty($values['@name'])) {
                $refs[] = [
                    'parent' => &$schema,
                    'child' => &$schema[$key],
                ];
            } elseif (is_array($values)) {
                self::getFirstLevelRefs($schema[$key], $refs);
            }
        }
        return $refs;
    }

    /**
     * Applati le schema pour utiliser au maximum les @ref plutôt que des
     * nested elements
     *
     * @param array $schema
     */
    private static function flattenizeSchema(array &$schema)
    {
        static::replaceGroup($schema, $schema);
        foreach (static::getFirstLevelRefs($schema) as $ref) {
            $ref['parent'] = array_merge(
                $ref['parent'],
                static::flattenizeSchemaData($ref['child']['@name'], $ref['child'])
            );
        }
        static::replaceAttributeGroup($schema, $schema);
    }

    /**
     * Remplace les occurences de attributeGroup par la liste d'attributs
     * du groupe
     *
     * @param array $schema
     * @param array $searchIn
     */
    private static function replaceAttributeGroup(array &$schema, array &$searchIn = [])
    {
        foreach ($searchIn as $key => $values) {
            if (!is_array($values)) {
                continue;
            }
            if (!empty($values[static::$prefix.'attributeGroup'])) {
                static::replaceAttributeSubGroup($schema, $searchIn[$key]);
            } elseif (!empty($values)) {
                static::replaceAttributeGroup($schema, $searchIn[$key]);
            }
        }
    }

    /**
     * Traite les attributeGroup dans un attributeGroup
     * @param array $schema
     * @param array $searchIn
     * @return void
     */
    private static function replaceAttributeSubGroup(array &$schema, array &$searchIn)
    {
        $groupValues = $searchIn[static::$prefix.'attributeGroup'];
        $value = !empty($groupValues['@ref'])
            ? [$groupValues['@ref']]
            : $groupValues['@ref'];
        foreach ($value as $v) {
            $ref = static::findNodeByAttributeName('attributeGroup', $v, $schema);
            $appendValue = static::extractAttributes($ref);
            if (empty($appendValue)) {
                continue;
            }
            $appendValue = current($appendValue);

            $attribute =& $searchIn[static::$prefix.'attribute'];
            if (!empty($attribute)) {
                if (isset($attribute['@ref'])) {
                    $attribute = [$attribute];
                }
                $attribute = array_merge($attribute, $appendValue);
            } else {
                $attribute = $appendValue;
            }
        }
    }

    /**
     * Récupère le contenu du groupe avant de l'injecter à la place de
     * la référence
     * @param array $schema
     * @param array $searchIn
     */
    private static function replaceGroup(array &$schema, array &$searchIn = [])
    {
        foreach ($searchIn as $key => $values) {
            if (!is_array($values)) {
                continue;
            }
            if (!empty($values[static::$prefix.'group'])) {
                $ref = static::findNodeByAttributeName('group', $values[static::$prefix.'group']['@ref'], $schema);
                unset($ref['@name']);
                /** @noinspection PhpArrayAccessCanBeReplacedWithForeachValueInspection - reference */
                $searchIn[$key] = array_merge($ref, $searchIn[$key]);
                unset($searchIn[$key][static::$prefix.'group']);
            } elseif (!empty($values)) {
                static::replaceGroup($schema, $searchIn[$key]);
            }
        }
    }

    /**
     * Renvoi le noeud $nodeName, qui porte l'attribut name $attributeName
     *
     * @param string $nodeName      Nom du noeud ex: attributeGroup
     * @param string $attributeName Valeur de l'attribut name du noeud
     * @param array  $schema
     * @return array
     * @noinspection PhpArrayAccessCanBeReplacedWithForeachValueInspection - reference
     */
    private static function &findNodeByAttributeName(string $nodeName, string $attributeName, array &$schema): array
    {
        foreach ($schema as $key => $values) {
            if ($key !== static::$prefix.$nodeName) {
                continue;
            }
            if (!empty($values['@name']) && $values['@name'] === $attributeName) {
                return $schema[$key];
            }
            foreach ($values as $k => $v) {
                if (!empty($v['@name']) && $v['@name'] === $attributeName) {
                    return $schema[$key][$k];
                }
            }
        }
        $default = [];
        return $default;
    }

    /**
     * Transforme les name en ref et renvoi les elements "capturés"
     *
     * @param string $parentName Utile pour construire le nom de la référence
     *                           qui sera $parentName . '/' . $childName
     * @param array  $ref        data à modifier (name en ref) et
     *                           à copier
     * @return array
     */
    private static function flattenizeSchemaData(string $parentName, array &$ref): array
    {
        $extracted = [];
        foreach (static::getFirstLevelRefs($ref) as $subRef) {
            $sub = static::flattenizeSchemaData($parentName.'/'.$subRef['child']['@name'], $subRef['child']);
            if (!empty($sub)) {
                $extracted = array_merge($sub, $extracted);
            }
            $subRef['child']['@name'] = $parentName.'/'.$subRef['child']['@name'];
            $extracted[] = $subRef['child'];
            $subRef['child']['@ref'] = $subRef['child']['@name'];
            unset($subRef['child']['@name']);
        }
        return $extracted;
    }

    /**
     * Garde les mêmes informations en modifiant la structure pour la rendre
     * plus facile à manipuler
     *
     * @param array $schema
     * @return array
     */
    private static function simplifyElements(array $schema): array
    {
        $elements = [];
        $schemaElements = $schema[static::$prefix.'element'];
        if (!empty($schemaElements['@name'])) {
            $schemaElements = [$schemaElements];
        }
        foreach ($schemaElements as $element) {
            if (!empty($element['@name'])) {
                $elements[$element['@name']] = static::extractElementData($element);
            }
        }
        return $elements;
    }

    /**
     * Spécial cas 1.1 + 1.n = 2.n
     *
     * @param array $data
     * @return array
     */
    private static function mergeElements(array $data): array
    {
        $elements = [];
        foreach (Hash::extract($data, '{n}.{n}.{n}') as $value) {
            if (isset($elements[$value['@ref']])) {
                $elements[$value['@ref']]['minOccurs']++;
                if ($value['@maxOccurs'] === 'unbounded') {
                    $elements[$value['@ref']]['maxOccurs'] = 'unbounded';
                }
            } else {
                $elements[$value['@ref']] = [
                    'minOccurs' => $value['@minOccurs'],
                    'maxOccurs' => $value['@maxOccurs'],
                ];
            }
        }
        return $elements;
    }

    /**
     * Renvoi un tableau des données simplifiés
     *
     * @param array $element
     * @return array
     */
    private static function extractElementData(array $element): array
    {
        $rawSequences = static::findElementsIn('sequence', $element);
        $rawChoices = static::findElementsIn('choice', $element);
        $enumeration = Hash::extract(
            static::findNodesByName(static::$prefix.'enumeration', $element),
            '{n}.{n}.@value'
        );
        $rawAttributes = static::extractAttributes($element);
        $refOrder = array_unique(Hash::extract($rawSequences, '{n}.{n}.{n}.@ref'));
        $choices = [];
        foreach (Hash::extract($rawChoices, '{n}.{n}') as $choice) {
            $choices[] = Hash::extract($choice, '{n}.@ref');
        }

        $attributes = [];
        $choicesToElements = static::mergeElements($rawChoices);
        $sequencesToElements = static::mergeElements($rawSequences);
        $elements = Hash::merge($choicesToElements, $sequencesToElements);
        foreach (Hash::extract($rawAttributes, '{n}.{n}') as $attribute) {
            $attributes[$attribute['@ref']] = [
                'use' => $attribute['@use']
            ];
        }

        $type = static::getElementType($element);
        $mixed = isset($element[static::$prefix.'complexType'])
            && (
                (!empty($element[static::$prefix.'complexType']['@mixed'])
                    && $element[static::$prefix.'complexType']['@mixed'])
                || !empty($element[static::$prefix.'complexType'][static::$prefix.'simpleContent'])
                || !empty($element[static::$prefix.'complexType'][static::$prefix.'complexContent'])
            )
        ;

        return [
            'traduction' => self::translate($element['@name']),
            'name' => $element['@name'],
            'type' => $type,
            'sequence' => $refOrder,
            'choice' => $choices,
            'elements' => $elements,
            'attributes' => $attributes,
            'enumeration' => $enumeration,
            'content' => $type !== 'complex' || $mixed ? 'string' : 'element-only',
        ];
    }

    /**
     * Fourni la traduction d'un element nommé
     *
     * @param string $name
     * @return null|string
     */
    private static function translate(string $name)
    {
        if ($name == 'cpfDescription') {
            return __x('XsdToArray', 'toto');
        }
        return I18n::getTranslator()->translate($name, ['_context' => 'XsdToArray']);
    }

    /**
     * Donne la liste des elements contenu dans un $nodeName
     * @param string $nodeName
     * @param array  $base
     * @return array
     */
    private static function findElementsIn(string $nodeName, array $base): array
    {
        $nodes = static::findNodesByName(static::$prefix.$nodeName, $base);
        $elements = [];
        foreach ($nodes as $node) {
            $extracted = static::extractElements($node);
            if ($extracted) {
                $elements[] = $extracted;
            }
        }
        return $elements;
    }

    /**
     * Donne le type d'element
     *
     * @param array $element
     * @return string
     */
    private static function getElementType(array $element): string
    {
        if (isset($element[static::$prefix.'complexType'])) {
            return 'complex';
        } elseif (isset($element[static::$prefix.'simpleType'])) {
            return 'simple';
        } elseif (isset($element['@type'])) {
            return $element['@type'];
        } else {
            return 'unknown';
        }
    }

    /**
     * Donne la liste des noeuds portant le nom nodeName
     *
     * @param string $nodeName
     * @param array  $element
     * @return array
     */
    private static function findNodesByName(string $nodeName, array $element): array
    {
        $result = [];
        foreach ($element as $node => $values) {
            if ($node === $nodeName) {
                $result[] = $values;
            } elseif (is_array($values)) {
                $result = array_merge($result, self::findNodesByName($nodeName, $values));
            }
        }
        return $result;
    }

    /**
     * Donne une liste d'element que contient $node
     *
     * @param array $node
     * @return array
     */
    private static function extractElements(array $node): array
    {
        $result = [];
        foreach ($node as $key => $values) {
            if (!is_array($values)) {
                continue;
            }

            if ($key === static::$prefix.'element') {
                $v = $values;
            } elseif (isset($values[static::$prefix.'element'])) {
                $v = $values[static::$prefix.'element'];
            } else {
                $result = array_merge($result, self::extractElements($values));
                continue;
            }

            if (isset($v['@ref'])) {
                $v = [$v];
            }
            $newResult =& $result[count($result)];
            $pMinOccurs = $values['@minOccurs'] ?? '1';
            $pMaxOccurs = $values['@maxOccurs'] ?? '1';
            foreach ($v as $element) {
                $newResult[] = [
                    '@ref' => $element['@ref'],
                    '@minOccurs' => $element['@minOccurs'] ?? $pMinOccurs,
                    '@maxOccurs' => $element['@maxOccurs'] ?? $pMaxOccurs,
                ];
            }
        }
        return $result;
    }

    /**
     * Donne une liste d'attributs que contient $node
     *
     * @param array $node
     * @return array
     */
    private static function extractAttributes(array $node): array
    {
        $result = [];
        foreach ($node as $key => $values) {
            if (!is_array($values)) {
                continue;
            }

            if ($key === static::$prefix.'attribute') {
                $v = $values;
            } elseif (isset($values[static::$prefix.'attribute'])) {
                $v = $values[static::$prefix.'attribute'];
            } else {
                $result = array_merge($result, self::extractAttributes($values));
                continue;
            }

            if (isset($v['@ref'])) {
                $v = [$v];
            }
            $newResult =& $result[count($result)];
            $pUse = $values['@use'] ?? 'optional';
            foreach ($v as $element) {
                $newResult[] = [
                    '@ref' => $element['@ref'],
                    '@use' => $element['@use'] ?? $pUse,
                ];
            }
        }
        return $result;
    }

    /**
     * Assure un formatage unique du schema pour faciliter le traitement
     *
     * @param array $schema
     */
    private static function reformatSchema(array &$schema)
    {
        if (!empty($schema[static::$prefix.'element']['@name'])) {
            $schema[static::$prefix.'element'] = [$schema[static::$prefix.'element']];
        }
        if (!empty($schema[static::$prefix.'attribute']['@name'])) {
            $schema[static::$prefix.'attribute'] = [$schema[static::$prefix.'attribute']];
        }
    }
}
