<?php
/**
 * AsalaeCore\Utility\CascadeDelete
 */

namespace AsalaeCore\Utility;

use AsalaeCore\Exception\GenericException;
use Cake\Core\App;
use Cake\Core\Configure;
use Cake\Database\StatementInterface;
use Cake\Datasource\ConnectionManager;
use Cake\Datasource\EntityInterface;
use Cake\Error\Debugger;
use Cake\ORM\Association;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Association\HasOne;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Utility\Inflector;
use Exception;
use PDO;
use PDOException;

/**
 * Permet la suppression en cascade
 * NOTE: pas complètement abouti - erreur fréquente: "FOREIGN KEY constraint failed"
 *
 * @category Utility
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class CascadeDelete
{
    /**
     * Liste-les "tables affectées" avec le nombre de résultats
     * @param HasMany|Table $model
     * @param array         $conditions
     * @param HasMany|null  $linked
     * @param array         $done
     * @return array exemple: ['Transfers' => 5, 'Archives' => 3, ...]
     */
    public static function dryrun(
        Table $model,
        array $conditions = [],
        HasMany $linked = null,
        array &$done = []
    ): array {
        /** @var Table|HasMany $linked */
        $linked = $linked ?: $model;
        $loc = TableRegistry::getTableLocator();
        $results = [];
        $modelName = Inflector::camelize($model->getTable());
        /** @var EntityInterface $entity */
        foreach ($linked->find()->where($conditions) as $entity) {
            $id = $entity->id;
            $relName = $modelName .'.'.$id;
            if (in_array($relName, $done)) {
                continue;
            }
            $done[] = $relName;
            foreach ($model->associations() as $assoc) {
                $relModelName = Inflector::camelize($assoc->getTable());
                if ($assoc instanceof HasMany && $assoc->getForeignKey()) {
                    $schema = $assoc->getSchema();
                    // on ne supprime pas si le champ est nullable
                    if ($schema->getColumn($assoc->getForeignKey())['null']) {
                        continue;
                    }
                    try {
                        $results = array_merge(
                            self::dryrun(
                                $loc->get($relModelName),
                                [$assoc->getAlias().'.'.$assoc->getForeignKey() => $id]
                                + $assoc->getConditions(),
                                $assoc,
                                $done
                            ),
                            $results
                        );
                    } catch (PDOException $e) {
                        throw new GenericException(
                            __(
                                "Echec dans la relation {0}->{1} (table {2}) {1}.{3}",
                                $modelName,
                                $assoc->getAlias(),
                                $relModelName,
                                $assoc->getForeignKey()
                            ),
                            null,
                            $e
                        );
                    }
                }
            }
            $results[$modelName] = ($results[$modelName] ?? 0) + 1;
        }
        return $results;
    }

    /**
     * Supprime les données des tables avec cascade
     * @param HasMany|Table $model
     * @param array         $conditions
     * @return array exemple: ['Transfers' => 5, 'Archives' => 3, ...]
     * @throws Exception
     */
    public static function all(Table $model, array $conditions = []): array
    {
        self::disableConstraints();
        $conn = $model->getConnection();
        $conn->begin();
        try {
            $results = self::recursiveDelete($model, $conditions);
            $conn->commit();
        } catch (Exception $e) {
            if ($conn->inTransaction()) {
                $conn->rollback();
            }
            $results = [];
            throw $e;
        } finally {
            self::enableConstraints();
        }
        return $results;
    }

    /**
     * Récursion
     * @param HasMany|HasOne|Table $model
     * @param array                $conditions
     * @param array                $done
     * @return array
     */
    private static function recursiveDelete(
        $model,
        array $conditions,
        array &$done = []
    ) {
        $modelName = Inflector::camelize($model->getTable());
        $model = TableRegistry::getTableLocator()->get($modelName);
        $query = $model->find()->where($conditions);
        $results = [];
        try {
            $sql = $query->sql();
            $entities = $query->all();
        } catch (PDOException $e) {
            foreach ($conditions as $key => $value) {
                if ($value instanceof Query) {
                    $conditions[$key] = $value->sql();
                }
            }
            throw new GenericException(
                __(
                    "Echec lors du find sur les dépendances d'un {0} avec les conditions : {1}\n{2}",
                    $modelName,
                    Debugger::exportVarAsPlainText($conditions),
                    $sql ?? __("echec lors du \$query->sql()")
                ),
                null,
                $e
            );
        }
        /** @var EntityInterface $entity */
        foreach ($entities as $entity) {
            $relName = $modelName .'.'.$entity->id;
            if (in_array($relName, $done)) {
                continue;
            }
            $done[] = $relName;
            $newConditions = [$model->getAlias().'.'.$model->getPrimaryKey() => $entity->id];
            foreach ($model->associations() as $assoc) {
                self::deleteAssoc($model, $assoc, $entity->id, $newConditions, $done, $results);
            }
            try {
                $model->deleteAll($newConditions);
            } catch (PDOException $e) {
                if (Configure::read('CascadeDelete.debug')) {
                    self::logDatabaseContent();
                }
                throw new GenericException(
                    __(
                        "Echec lors de la suppression d'un {0} avec les conditions {1}\n{2}",
                        $modelName,
                        Debugger::exportVarAsPlainText($newConditions),
                        $e->getMessage()
                    ),
                    null,
                    $e
                );
            }
            $results[$modelName] = ($results[$modelName] ?? 0) + 1;
        }
        return $results;
    }

    /**
     * Désactive les contraintes
     * @return StatementInterface|false
     */
    public static function disableConstraints()
    {
        $conn = ConnectionManager::get('default');
        $driver = $conn->getDriver();
        try {
            if ($driver->getConnection()->getAttribute(PDO::ATTR_DRIVER_NAME) === 'pgsql') {
                return $conn->execute("SET session_replication_role = 'replica';");
            }
            return $conn->execute('PRAGMA ignore_check_constraints = true;');
        } catch (PDOException $e) {
        }
        return false;
    }

    /**
     * Désactive les contraintes
     * @return StatementInterface|false
     */
    public static function enableConstraints()
    {
        $conn = ConnectionManager::get('default');
        $driver = $conn->getDriver();
        try {
            if ($driver->getConnection()->getAttribute(PDO::ATTR_DRIVER_NAME) === 'pgsql') {
                return $conn->execute("SET session_replication_role = 'origin';");
            }
            return $conn->execute('PRAGMA ignore_check_constraints = false;');
        } catch (PDOException $e) {
        }
        return false;
    }

    /**
     * Effectue un truncate
     * @param Table $model
     * @param bool  $restartIdentity
     * @return StatementInterface
     */
    public static function truncate(Table $model, bool $restartIdentity = false)
    {
        $conn = $model->getConnection();
        $driver = $conn->getDriver();
        if ($driver->getConnection()->getAttribute(PDO::ATTR_DRIVER_NAME) === 'pgsql') {
            return $conn
                ->execute(
                    'TRUNCATE TABLE '.$model->getTable()
                    .($restartIdentity ? ' RESTART IDENTITY' : '').' CASCADE;'
                );
        }
        return $conn->execute('DELETE FROM '.$model->getTable().';');
    }

    /**
     * Supprime les tables liés
     * @param HasMany|HasOne|Table $model
     * @param Association          $assoc
     * @param int                  $id
     * @param array                $conditions
     * @param array                $done
     * @param array                $results
     * @return void
     */
    private static function deleteAssoc(
        $model,
        Association $assoc,
        int $id,
        array $conditions,
        array &$done,
        array &$results
    ) {
        $modelName = Inflector::camelize($model->getTable());
        $relModelName = Inflector::camelize($assoc->getTable());
        if (($assoc instanceof HasMany || $assoc instanceof HasOne) && $assoc->getForeignKey()) {
            $schema = $assoc->getSchema();
            if (!in_array($assoc->getForeignKey(), $schema->columns())) {
                throw new GenericException(
                    __(
                        "La foreign_key {0} ne semble pas exister dans {1}->{2}",
                        $assoc->getForeignKey(),
                        $model->getAlias(),
                        $assoc->getAlias()
                    )
                );
            }
            // on ne supprime pas si le champ est nullable
            if ($schema->getColumn($assoc->getForeignKey())['null']) {
                try {
                    $assoc->updateAll(
                        [$assoc->getForeignKey() => null],
                        [$assoc->getForeignKey() => $id]
                    );
                } catch (PDOException $e) {
                    throw new GenericException(
                        __(
                            "Echec lors de du SET NULL sur {0}->{1}"
                            ." (table {2}) {1}.{3}\n{4}:{5} {6}",
                            $modelName,
                            $assoc->getAlias(),
                            $relModelName,
                            $assoc->getForeignKey(),
                            str_replace(ROOT, 'ROOT', $e->getFile()),
                            $e->getLine(),
                            $e->getMessage()
                        ),
                        null,
                        $e
                    );
                }
                return;
            }
            $tField = $assoc->getAlias().'.'.$assoc->getPrimaryKey();
            if ($assoc->getConditions()) {
                try {
                    $ent = $model->find()
                        ->select(['target_id' => $tField])
                        ->innerJoinWith($assoc->getAlias())
                        ->where($conditions)
                        ->first();
                } catch (PDOException $e) {
                    throw new GenericException(
                        __(
                            "Echec lors d'un find avec conditions de jointure sur {0}->{1}"
                            ." (table {2}) ({3})\n{4}:{5} {6}",
                            $modelName,
                            $assoc->getAlias(),
                            $relModelName,
                            var_export($conditions, true),
                            str_replace(ROOT, 'ROOT', $e->getFile()),
                            $e->getLine(),
                            $e->getMessage()
                        ),
                        null,
                        $e
                    );
                }
                if (!$ent) {
                    return;
                }
                $targetId = $model->find()
                    ->select(['target_id' => $tField])
                    ->innerJoinWith($assoc->getAlias())
                    ->where($conditions);
            } else {
                $targetId = $id;
            }
            $results = array_merge(
                self::recursiveDelete(
                    $assoc,
                    [$relModelName . '.' . $assoc->getPrimaryKey() => $targetId],
                    $done
                ),
                $results
            );
        }
    }

    /**
     * Ecrit dans debug.log, l'état actuel de la base de données
     * @return void
     */
    public static function logDatabaseContent()
    {
        $paths = App::classPath('Model/Table');
        $models = glob($paths[0] . '*Table.php');
        $loc = TableRegistry::getTableLocator();
        $data = '';
        foreach ($models as $file) {
            $model = basename($file, 'Table.php');
            try {
                $Model = $loc->get($model);
                $fields = $Model->getSchema()->columns();
                $data .= sprintf('TABLE %s (%d results) :', $Model->getTable(), $Model->find()->count()) . PHP_EOL;
                $data .= implode(' | ', $fields) . PHP_EOL;
                $query = $Model->find()
                    ->select($fields)
                    ->orderAsc($Model->getPrimaryKey())
                    ->disableHydration();
                foreach ($query as $row) {
                    $row = json_decode(json_encode($row), true);
                    $data .= implode(' | ', $row) . PHP_EOL;
                }
                $data .= PHP_EOL;
            } catch (Exception $e) {
            }
        }
        dforcelog($data);
    }
}
