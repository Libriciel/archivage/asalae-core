<?php
/**
 * AsalaeCore\Utility\Exec
 */

namespace AsalaeCore\Utility;

use AsalaeCore\Exception\GenericException;
use AsalaeCore\Utility\Object\CommandResult;
use Cake\Core\Configure;
use Cake\I18n\FrozenTime;
use Exception;

/**
 * Permet d'éffectuer des commandes bash
 *
 * @category Utility
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Exec
{
    /**
     * @var string chemin vers le fichier de sorti (pour du async)
     */
    public $defaultStdout = '/dev/null';

    /**
     * @var string chemin vers le fichier d'erreur (pour du async)
     */
    public $defaultStderr = '&1';

    /**
     * @var string|null locale par défaut (null réstaure la valeur système)
     */
    public $locale = null;

    /**
     * @var array Liste les process id issue des commandes asynchrones
     */
    public static $pids = [];

    /**
     * Setter du fichier de sorti par défaut
     * @param string $default
     * @return Exec
     */
    public function setDefaultStdout(string $default = '/dev/null')
    {
        $this->defaultStdout = $default;
        return $this;
    }

    /**
     * Setter du fichier de sorti par défaut
     * @param string $default
     * @return Exec
     */
    public function setDefaultStderr(string $default = '&1')
    {
        $this->defaultStderr = $default;
        return $this;
    }

    /**
     * Echape une liste d'arguments
     * ex: ['foo', ['-bar' => 'baz']] return "'foo' -bar 'baz'"
     * @param array $args
     * @param array $escaped
     * @return string
     */
    private function escapeArgs(array $args = [], array $escaped = []): string
    {
        setlocale(LC_CTYPE, 'en_US.UTF-8');
        foreach ($args as $value) {
            if (is_array($value)) {
                foreach ($value as $k => $v) {
                    $escaped[] = $k;
                    if ($v) {
                        $escaped[] = escapeshellarg($v);
                    }
                }
            } else {
                $escaped[] = escapeshellarg($value);
            }
        }
        setlocale(LC_ALL, $this->locale);
        return implode(' ', $escaped);
    }

    /**
     * Lance une commande de façon asynchrone
     * @param string $command
     * @param mixed  ...$args
     * return int
     */
    public function async($command, ...$args)
    {
        $command = $this->escapeArgs($args, [$command]);
        $noup = sprintf(
            'nohup bash -c %s >%s 2>%s & echo $!',
            $this->escapeCommand($command),
            $this->defaultStdout,
            $this->defaultStderr
        );
        $pid = exec($noup);
        $this->log($noup, (int)$pid);

        self::$pids[$pid] = $pid;
        return $pid;
    }

    /**
     * Vrais s'il reste un process en cours
     * @param int $sleep durée d'attente entre 2 vérifications
     * @return bool
     */
    public static function runningAsync(int $sleep = 1): bool
    {
        $running = false;
        foreach (self::$pids as $pid) {
            if (!self::procIsRunning($pid)) {
                unset(self::$pids[$pid]);
            } else {
                $running = true;
            }
        }
        if ($running && $sleep) {
            sleep($sleep); //NOSONAR
        }
        return $running;
    }

    /**
     * Vérifi si un processus existe, tente de le tuer si il est zombie
     * @param int $pid
     * @return bool
     */
    public static function procIsRunning(int $pid): bool
    {
        if (posix_kill($pid, 0) !== false) {
            if (in_array($pid, self::getZombieList())) {
                posix_kill($pid, 9);
                return false;
            }
            return true;
        }
        return false;
    }

    /**
     * Donne la liste des pids zombies
     * @return array
     */
    private static function getZombieList(): array
    {
        $instance = new self();
        $instance->rawCommand('ps aux | egrep "Z|defunct"', $output);
        $pids = [];
        foreach ($output as $str) {
            if (preg_match('/^(.*?)\s+(\d+)/', $str, $m)) {
                $pids[] = $m[1];
            }
        }
        return $pids;
    }

    /**
     * Attend que les processes asynchrones soit fini
     * @param float $timeout en secondes
     * @param int   $sleep   durée d'attente entre 2 vérifications
     * @throws Exception
     */
    public static function waitUntilAsyncFinish(float $timeout = 10, int $sleep = 1)
    {
        $spent = 0;
        $step = $sleep;// $sleep en secondes
        while (self::runningAsync($sleep)) {
            $spent += $step;
            if ($spent > $timeout) {
                self::$pids = []; // évite les attentes en cascade dans les tests
                throw new Exception('Exec::waitUntilAsyncFinish timeout');
            }
        }
    }

    /**
     * Echape une commande pour quelle soit lancée par "bash -c"
     * @param string $command
     * @return string
     */
    public function escapeCommand(string $command)
    {
        $command = str_replace('`', '\\`', $command);
        if (strpos($command, "'") === false) {
            return sprintf("'%s'", $command);
        }
        if (strpos($command, '"') === false) {
            return sprintf('"%s"', str_replace('$', '\\$', $command));
        }
        $offset = 0;
        $parts = [];
        while (($pos = strpos($command, "'", $offset)) !== false) {
            $parts[] = sprintf("'%s'", substr($command, 0, $pos));
            $parts[] = "\"\\'\"";
            $offset = $pos +1;
        }
        $parts[] = "'".substr($command, $offset)."'";
        return implode('', $parts);
    }

    /**
     * Lance une commande et renvoi un array
     * @param string $command
     * @param string ...$args
     * @return CommandResult
     *      'success' => code retour 0,
     *      'code' => contenu du &$return_var de la fonction exec,
     *      'stdout' => sorti standard,
     *      'stderr' => sorti d'erreur,
     */
    public function command($command, ...$args): CommandResult
    {
        $stdoutFile = tempnam(sys_get_temp_dir(), 'exec-utility-');
        $stderrFile = tempnam(sys_get_temp_dir(), 'exec-utility-');

        $command = sprintf(
            '%s > %s 2> %s',
            $this->escapeArgs($args, [$command]),
            $stdoutFile,
            $stderrFile
        );
        $this->log($command, getmypid());
        exec($command, $output, $return_var);
        $stdout = file_get_contents($stdoutFile);
        $stderr = file_get_contents($stderrFile);
        unlink($stdoutFile);
        unlink($stderrFile);
        return new CommandResult(
            [
                'success' => $return_var === 0,
                'code' => $return_var,
                'stdout' => $stdout,
                'stderr' => $stderr,
            ]
        );
    }

    /**
     * Effectu une commande sans rediriger les sorties (stdout et stderr)
     * @param string     $command
     * @param array|null $output
     * @param null       $return_var
     * @param mixed      ...$args    arguments
     *                               à échaper
     * @return string
     */
    public function rawCommand($command, array &$output = null, &$return_var = null, ...$args)
    {
        $command = $this->escapeArgs($args, [$command]);
        return exec($command, $output, $return_var);
    }

    /**
     * Permet d'échanper un paramètre sans retirer les accents
     * @param string      $arg
     * @param string|null $locale cette locale sera mise après le escape
     * @return string
     */
    public static function escapeshellarg(string $arg, string $locale = null): string
    {
        setlocale(LC_CTYPE, 'en_US.UTF-8'); // pour gérer les accents dans la fonction escapeshellarg()
        $escaped = escapeshellarg($arg);
        setlocale(LC_ALL, $locale);
        return $escaped;
    }

    /**
     * Ajoute une commande au log
     * @param string $command
     * @param int    $pid
     * @return void
     */
    private function log(string $command, int $pid)
    {
        if (Configure::read('debug')) {
            $trace = preg_replace(
                "/^(.*)/m",
                "    $1",
                dlast(2, null, false)
            );
            $msg = sprintf(
                "%s - pid=%d - %s\n%s\n",
                (new FrozenTime)->format('Y-m-d H:i:s'),
                $pid,
                $command,
                $trace
            );
            file_put_contents(LOGS . 'exec.log', $msg, FILE_APPEND);
        }
    }

    /**
     * Proc_open avec redirection sur les sorties du terminal (par défaut)
     * @param string     $command
     * @param array|null $descriptor_spec
     * @return int
     */
    public function proc(string $command, ?array $descriptor_spec = null): int
    {
        $descriptor_spec = $descriptor_spec ?? [
            ['file', 'php://stdout', 'w'],
            ['file', 'php://stderr', 'w'],
        ];

        $process = proc_open($command, $descriptor_spec, $pipes);

        if (is_resource($process)) {
            return proc_close($process);
        } else {
            throw new GenericException("unable to launch command");
        }
    }
}
