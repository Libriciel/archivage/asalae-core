<?php
/**
 * AsalaeCore\Utility\Php
 */

namespace AsalaeCore\Utility;

/**
 * Appel les fonctions native de php
 *
 * @package AsalaeCore\Utility
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Php
{
    /**
     * Appel générique
     *
     * @param string $name
     * @param mixed  $args
     * @return mixed
     */
    public function __call(string $name, $args)
    {
        return call_user_func_array($name, $args);
    }
}
