<?php
/**
 * AsalaeCore\Auth\Identifier\UrlIdentifier
 */

namespace AsalaeCore\Auth\Identifier;

use AsalaeCore\Model\Table\AuthUrlsTable;
use Authentication\Identifier\AbstractIdentifier;
use Authentication\Identifier\IdentifierInterface;
use Cake\ORM\TableRegistry;

/**
 * UrlIdentifier
 *
 * @category Identifier
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class UrlIdentifier extends AbstractIdentifier implements IdentifierInterface
{
    /**
     * Identifies an user or service by the passed credentials
     *
     * @param array $credentials Authentication credentials
     * @return array|null
     */
    public function identify(array $credentials)
    {
        return !empty($credentials['url']) && !empty($credentials['code'])
            ? $this->identifyUrl($credentials)
            : null;
    }

    /**
     * Login par url
     * @param array $data Authentication credentials
     * @return array|null
     */
    private function identifyUrl(array $data)
    {
        /** @var AuthUrlsTable $AuthUrls */
        $AuthUrls = TableRegistry::getTableLocator()->get('AuthUrls');
        $auth = $AuthUrls->find()->where(['code' => $data['code']])->first();
        if (!$auth) {
            return null;
        }
        $auth->set('url_id', $auth->id);
        $auth->unset('id');
        return $auth->toArray();
    }
}
