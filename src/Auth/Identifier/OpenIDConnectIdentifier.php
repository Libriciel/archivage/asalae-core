<?php
/**
 * AsalaeCore\Auth\Identifier\OpenIDConnectIdentifier
 */

namespace AsalaeCore\Auth\Identifier;

use AsalaeCore\Factory\Utility;
use Authentication\Identifier\AbstractIdentifier;
use Cake\Database\Exception\MissingConnectionException;
use Cake\Datasource\EntityInterface;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Http\Client;
use Cake\ORM\TableRegistry;
use Exception;
use PDOException;

/**
 * OpenIDConnectIdentifier
 *
 * @category Identifier
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2022, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class OpenIDConnectIdentifier extends AbstractIdentifier
{
    /**
     * Default configuration
     *
     * @var array
     */
    protected $_defaultConfig = [
        'base_url' => 'http://localhost:8080',
        'username' => 'preferred_username',
        'client_id' => 'asalae',
        'client_secret' => null,
        'redirect_uri' => null,
        'scope' => null,
        'model_class' => 'Users',
        'model_username' => 'username',
        'model_active' => 'active',
        'contain' => [
            'OrgEntities' => [
                'ArchivalAgencies' => ['Configurations'],
                'TypeEntities',
            ],
            'Ldaps',
            'Roles',
        ],
        'force_endpoint_methods' => [
            'userinfo' => Client\Message::METHOD_POST,
        ],
    ];

    /**
     * Connexion au keycloak
     * @param array $credentials
     * @return array|EntityInterface|null
     * @throws Exception
     */
    public function identify(array $credentials)
    {
        if (empty($credentials['access_token'])) {
            return null;
        }
        $data = ['access_token' => $credentials['access_token']];
        /** @var Client $client */
        $client = Utility::get(Client::class);
        foreach (['client_id', 'client_secret', 'redirect_uri', 'scope'] as $add) {
            if ($value = $this->getConfig($add)) {
                $data[$add] = $value;
            }
        }
        // pour retrocompatibilité keycloak uniquement
        $defaultUrl = sprintf(
            '%s/auth/realms/%s/protocol/openid-connect/userinfo',
            $this->getConfig('base_url'),
            $this->getConfig('realm')
        );

        $method = strtolower(
            $this->getConfig('force_endpoint_methods.userinfo', Client\Message::METHOD_POST)
        );
        $client = new $client(['headers' => ['Authorization' => 'Bearer ' . $data['access_token']]]);
        $response = $client->$method(
            $this->getConfig('endpoints.userinfo', $defaultUrl),
            $data,
            $this->getConfig('clientParams', [])
        );
        $json = json_decode($response->getStringBody(), true);
        if ($json && !empty($json[$this->getConfig('username')])) {
            $username = $json[$this->getConfig('username')];
            $modelName = $this->getConfig('model_class', 'Users');
            $field = $this->getConfig('model_username', 'username');
            $Users = TableRegistry::getTableLocator()->get($modelName);
            $conditions = ["$modelName.$field" => $username];
            if ($active = $this->getConfig('model_active')) {
                $conditions["$modelName.$active"]  = true;
            }
            try {
                return $Users->find()
                    ->where($conditions)
                    ->contain($this->getConfig('contain', []))
                    ->firstOrFail()
                    ->set('auth_openid', true)
                    ->set('auth_url', $this->getAuthUrl());
            } catch (PDOException|MissingConnectionException|RecordNotFoundException $e) {
                return $Users->newEmptyEntity()
                    ->set('username', $json ? ($json[$this->getConfig('username')] ?? null) : null)
                    ->set('auth_openid', true);
            }
        } elseif ($json) {
            file_put_contents(
                LOGS . 'last_failed_userinfo.log',
                json_encode($json, JSON_PRETTY_PRINT)
            );
        }

        return null;
    }

    /**
     * Donne l'url de redirection vers la page de connexion
     * @return string
     */
    public function getAuthUrl(): string
    {
        $params = ['response_type' => 'code'];
        foreach (['client_id', 'redirect_uri', 'scope'] as $arg) {
            if ($val = $this->getConfig($arg)) {
                $params[$arg] = $val;
            }
        }
        $params = '?' . http_build_query($params);
        return $this->getConfig('endpoints.authorization') . $params;
    }
}
