<?php
/**
 * AsalaeCore\Auth\Identifier\AttemptTrait
 */

namespace AsalaeCore\Auth\Identifier;

use ArrayAccess;
use AsalaeCore\Middleware\RateLimiterMiddleware;
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Utility\Hash;

/**
 * Mémorisation des tentatives de connexion
 */
trait AttemptTrait
{
    /**
     * Mémorise la tentative de connexion (supprime les essais expirés)
     * @param ArrayAccess|array|null $identity
     * @param bool                   $success
     * @return void
     */
    private function logAttempt($identity, bool $success)
    {
        $config = Cache::getConfig('login');
        $username = Hash::get($identity, 'username');
        if ($config && $username && !RateLimiterMiddleware::$attemptLogged) {
            $data = $this->getAttemptsData($username);
            $data[] = ['time' => microtime(true), 'success' => $success];
            Cache::write($username, $data, 'login');
            RateLimiterMiddleware::$username = $username;
            RateLimiterMiddleware::$attempts = $this->getFailedAttemptsCount($data);
            RateLimiterMiddleware::$consecutiveFailedAttempts = $success
                ? 0
                : $this->getConsecutiveFailedCount($data);
            RateLimiterMiddleware::$attemptLogged = true;
        }
    }

    /**
     * Donne la liste des tentatives de connexions sur un utilisateur
     * @param string $username
     * @return array
     */
    private function getAttemptsData(string $username): array
    {
        $duration = Configure::read('Login.attempt_limit.cache_duration', 3600);
        $expired = microtime(true) - $duration;
        return array_filter(
            Cache::read($username, 'login') ?: [],
            fn(array $v) => $v['time'] > $expired
        );
    }

    /**
     * Donne le nombre d'essais en echec
     * @param array $data
     * @return int
     */
    private function getFailedAttemptsCount(array $data): int
    {
        return count(
            array_filter(
                $data,
                fn(array $v) => $v['success'] === false
            )
        );
    }

    /**
     * Donne la dernère serie d'échecs consécutifs
     * @param array $data
     * @return int
     */
    private function getConsecutiveFailedCount(array $data): int
    {
        $count = 0;
        foreach ($data as $attempt) {
            if ($attempt['success']) {
                $count = 0;
            } else {
                $count++;
            }
        }
        return $count;
    }
}
