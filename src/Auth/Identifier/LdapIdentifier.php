<?php
/**
 * AsalaeCore\Auth\Identifier\LdapIdentifier
 */

namespace AsalaeCore\Auth\Identifier;

use Adldap\Adldap;
use Adldap\Auth\BindException;
use Adldap\Auth\Guard;
use Adldap\Auth\PasswordRequiredException;
use Adldap\Connections\Provider;
use Adldap\Models\Model;
use AsalaeCore\Model\Entity\Ldap;
use AsalaeCore\Factory\Utility;
use Authentication\Identifier\AbstractIdentifier;
use Cake\Database\Exception\MissingConnectionException;
use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;
use Exception;
use PDOException;

/**
 * LdapIdentifier
 *
 * @category Identifier
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class LdapIdentifier extends AbstractIdentifier
{
    use AttemptTrait;

    /**
     * Default configuration
     *
     * @var array
     */
    protected $_defaultConfig = [
        'fields' => [
            'username' => 'username',
            'password' => 'password',
        ]
    ];

    /**
     * Connexion au ldap
     * @param array $credentials
     * @return EntityInterface|false|null
     * @throws Exception
     */
    public function identify(array $credentials)
    {
        if (!isset($credentials[$this->getConfig('fields.username')])
            || !isset($credentials[$this->getConfig('fields.password')])
        ) {
            return null;
        }
        $username = $credentials[$this->getConfig('fields.username')];
        $password = $credentials[$this->getConfig('fields.password')];
        $Users = TableRegistry::getTableLocator()->get('Users');
        try {
            $identity = $Users->find()
                ->where(
                    [
                        'Users.username' => $username,
                        'Users.active' => true,
                    ]
                )
                ->contain(
                    [
                        'OrgEntities' => [
                            'ArchivalAgencies' => ['Configurations'],
                            'TypeEntities',
                        ],
                        'Ldaps',
                        'Roles',
                    ]
                )
                ->first();
        } catch (PDOException|MissingConnectionException $e) {
            return null;
        }
        if (!$identity || !$identity->get('ldap_id')) {
            return null;
        }
        /** @var Ldap $ldap */
        $ldap = $identity->get('ldap');
        $ldapLogin = $identity->get('ldap_login');
        /** @var Adldap $ad */
        $ad = Utility::get(Adldap::class);
        $ad->addProvider($ldap->getLdapConfig());
        try {
            /** @var Provider $provider */
            $provider = $ad->connect();
            /** @var Guard $auth */
            $auth = $provider->auth();
            $passwordOk = $auth->attempt($ldapLogin, $password, true);
            $this->logAttempt($identity, !$passwordOk);
            if (!$passwordOk) {
                return null;
            }
            $search = $provider->search();
            $users = $search->users();
            /** @var Model|false $ldapUser */
            $ldapUser = $users->findBy($ldap->get('user_login_attribute'), $ldapLogin);
            if (!empty($ldapUser)) {
                $name = $ldapUser->getAttribute($ldap->get('user_name_attribute'));
                $mail = $ldapUser->getAttribute($ldap->get('user_mail_attribute'));
                if ($name) {
                    $identity->set('name', current($name));
                }
                if ($mail) {
                    $identity->set('email', current($mail));
                }
            }
        } catch (BindException|PasswordRequiredException $e) {
            return null;
        }
        return $Users->save($identity);
    }

    /**
     * Donne la config pour initialiser un ldap via Adldap2
     * @param EntityInterface $ldap
     * @return array
     */
    public function getLdapConfig(EntityInterface $ldap): array
    {
        $opts = [];
        foreach ($ldap->get('meta') ?? [] as $option => $val) {
            if (defined('LDAP_OPT_'.$option)) {
                $opts[constant('LDAP_OPT_'.$option)] = $val;
            }
        }

        return [
            // Mandatory Configuration Options
            'hosts' => [$ldap->get('host')],
            'base_dn' => $ldap->get('ldap_root_search'),
            'username' => $ldap->get('user_query_login'),
            'password' => $ldap->get('user_query_password'),

            // Optional Configuration Options
            'schema' => $ldap->get('schema'),
            'account_prefix' => $ldap->get('account_prefix'),
            'account_suffix' => $ldap->get('account_suffix'),
            'port' => $ldap->get('port'),
            'follow_referrals' => $ldap->get('follow_referrals'),
            'use_ssl' => (bool)$ldap->get('use_ssl'),
            'use_tls' => (bool)$ldap->get('use_tls'),
            'version' => $ldap->get('version'),
            'timeout' => $ldap->get('timeout'),

            // Custom LDAP Options
            'custom_options' => $opts,
        ];
    }
}
