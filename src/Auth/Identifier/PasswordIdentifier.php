<?php
/**
 * AsalaeCore\Auth\Identifier\PasswordIdentifier
 */

namespace AsalaeCore\Auth\Identifier;

use ArrayAccess;
use Authentication\Identifier\PasswordIdentifier as CakePasswordIdentifier;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Core\Configure;
use Cake\Database;
use Cake\Database\Exception\MissingConnectionException;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use PDOException;

/**
 * PasswordIdentifier
 *
 * @category Identifier
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class PasswordIdentifier extends CakePasswordIdentifier
{
    use AttemptTrait;

    /**
     * Identifies an user or service by the passed credentials
     *
     * @param array $credentials Authentication credentials
     * @return ArrayAccess|array|null
     */
    public function identify(array $credentials)
    {
        $identity = !empty($credentials['admin'])
            ? $this->identifyAdmin($credentials)
            : parent::identify($credentials);
        $this->logAttempt($credentials, (bool)$identity);
        return $identity;
    }

    /**
     * Find a user record using the username/identifier provided.
     *
     * @param string $identifier The username/identifier.
     * @return ArrayAccess|array|null
     */
    protected function _findIdentity(string $identifier)
    {
        $fields = $this->getConfig('fields.' . self::CREDENTIAL_USERNAME);
        $conditions = [];
        foreach ((array)$fields as $field) {
            $conditions[$field] = $identifier;
        }
        try {
            $user = TableRegistry::getTableLocator()->get('Users')->find()
                ->where(
                    $conditions
                    + [
                        'Users.active' => true,
                    ]
                )
                ->contain(
                    [
                        'OrgEntities' => [
                            'ArchivalAgencies' => ['Configurations'],
                            'TypeEntities',
                        ],
                        'Ldaps',
                        'Roles',
                    ]
                )
                ->firstOrFail();
        } catch (RecordNotFoundException|PDOException|MissingConnectionException|Database\Exception $e) {
            return null;
        }
        return $user;
    }

    /**
     * Login de l'administrateur technique
     * @param array $data Authentication credentials
     * @return ArrayAccess|array|null
     */
    private function identifyAdmin(array $data)
    {
        $path = Configure::read('App.paths.administrators_json');
        if (!$path || !is_readable($path)) {
            return null;
        }
        $hasher = new DefaultPasswordHasher();
        $admins = json_decode(file_get_contents($path), true) ?: [];
        $login = Hash::get(
            $data,
            $this->getConfig('fields.' . self::CREDENTIAL_USERNAME)
        );
        if (!$login) {
            return null;
        }
        $password = Hash::get(
            $data,
            $this->getConfig('fields.' . self::CREDENTIAL_PASSWORD)
        );
        foreach ($admins as $values) {
            if ($login === $values['username']) {
                if ($hasher->check($password, $values['password'])) {
                    unset($values['password'], $values['confirm-password']);
                    $this->logAttempt($values, true);
                    return $values + ['admin' => true];
                } else {
                    $this->logAttempt($values, false);
                }
                break;
            }
        }
        return null;
    }
}
