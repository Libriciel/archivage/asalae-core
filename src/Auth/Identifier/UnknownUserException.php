<?php
/**
 * AsalaeCore\Auth\Identifier\UnknownUserException
 */

namespace AsalaeCore\Auth\Identifier;

use Cake\Http\Exception\HttpException;
use Throwable;

/**
 * Utilisateur identifié mais n'existant pas dans l'application
 *
 * @category Exception
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2022, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class UnknownUserException extends HttpException
{
    /**
     * @var string|null
     */
    public $username = null;

    /**
     * Construct the exception. Note: The message is NOT binary safe.
     * @link https://php.net/manual/en/exception.construct.php
     * @param string         $message  [optional] The Exception message to throw.
     * @param int            $code     [optional] The Exception code.
     * @param null|Throwable $previous [optional] The previous throwable used for the exception chaining.
     */
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        $message = $message ?: __("L'utilisateur n'a pas été trouvé sur l'application");
        $code = $code ?: 401;
        parent::__construct(
            $message,
            $code,
            $previous
        );
    }
}
