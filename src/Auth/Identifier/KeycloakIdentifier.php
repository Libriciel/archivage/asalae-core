<?php
/**
 * AsalaeCore\Auth\Identifier\KeycloakIdentifier
 */

namespace AsalaeCore\Auth\Identifier;

/**
 * KeycloakIdentifier
 *
 * @category Identifier
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @deprecated since 2.1.7 use OpenIDConnectIdentifier instead
 */
class KeycloakIdentifier extends OpenIDConnectIdentifier
{
}
