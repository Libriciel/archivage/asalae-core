<?php
/**
 * AsalaeCore\Auth\AuthenticationService
 */

namespace AsalaeCore\Auth;

use ArrayAccess;
use ArrayObject;
use Authentication\AuthenticationService as CakeAuthenticationService;
use Authentication\Identity;
use Cake\Datasource\EntityInterface;
use Cake\Http\Session;
use Cake\Utility\Hash;
use Exception;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Authentication Service
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2022, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class AuthenticationService extends CakeAuthenticationService
{
    /**
     * Default configuration
     * {@inheritdoc}
     * @var array
     */
    protected $_defaultConfig = [
        'authenticators' => [],
        'identifiers' => [],
        'identityClass' => Identity::class,
        'identityAttribute' => 'identity',
        'queryParam' => null,
        'unauthenticatedRedirect' => null,
        'sessionKey' => 'Session',
        'sessionKeyAdmin' => 'Admin',
    ];

    /**
     * Sets identity data and persists it in the authenticators that support it.
     *
     * @param ServerRequestInterface $request  The request.
     * @param ResponseInterface      $response The response.
     * @param ArrayAccess|array      $identity Identity data.
     * @return array
     * @throws Exception
     */
    public function persistIdentity(ServerRequestInterface $request, ResponseInterface $response, $identity): array
    {
        $id = Hash::get($identity, 'id');
        if (Hash::get($identity, 'auth_basic')) {
            return [
                'request' => $request->withAttribute($this->getConfig('identityAttribute'), $identity),
                'response' => $response,
            ];
        }
        if ($id) {
            $sessionKey = $this->getConfig('sessionKey');
            /** @var Session $session */
            $session = $request->getAttribute('session');
            if ($session && !$session->read($sessionKey.'.token')) {
                $session->write($sessionKey.'.token', bin2hex(random_bytes(8)));
            }

            $config = [];
            $configs = Hash::get($identity, 'org_entity.archival_agency.configurations', []);
            /** @var EntityInterface $conf */
            foreach ($configs as $conf) {
                $config[$conf->get('name')] = $conf->get('setting');
            }
            if ($session) {
                $session->write('ConfigArchivalAgency', $config);
            }
        }
        $admin = Hash::get($identity, 'admin');
        if ($admin) {
            $sessionKey = $this->getConfig('sessionKeyAdmin');
            /** @var Session $session */
            $session = $request->getAttribute('session');
            if ($session) {
                $session->write(
                    $sessionKey,
                    [
                        'tech' => true,
                        'data' => $identity instanceof ArrayObject
                            ? $identity->getArrayCopy()
                            : $identity,
                    ]
                );
            }
        }

        return parent::persistIdentity($request, $response, $identity);
    }
}
