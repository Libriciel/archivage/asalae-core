<?php
/**
 * AsalaeCore\Auth\Authenticator\UrlAuthenticator
 */

namespace AsalaeCore\Auth\Authenticator;

use ArrayAccess;
use Authentication\Authenticator\PersistenceInterface;
use Authentication\Authenticator\Result;
use Authentication\Authenticator\ResultInterface;
use Authentication\Authenticator\SessionAuthenticator as CakeSessionAuthenticator;
use Cake\Http\Session;
use Cake\Utility\Hash;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * UrlAuthenticator
 *
 * @category Authenticator
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class UrlAuthenticator extends CakeSessionAuthenticator implements PersistenceInterface
{
    /**
     * Authenticate a user using session data.
     *
     * @param \Psr\Http\Message\ServerRequestInterface $request The request to authenticate with.
     * @return \Authentication\Authenticator\ResultInterface
     */
    public function authenticate(ServerRequestInterface $request): ResultInterface
    {
        $sessionKey = $this->getConfig('sessionKey');
        /** @var Session $session */
        $session = $request->getAttribute('session');
        $url = $session->read($sessionKey);

        if (empty($url) || (!Hash::get($url, 'url') && !Hash::get($url, 'code'))) {
            return new Result(null, ResultInterface::FAILURE_IDENTITY_NOT_FOUND);
        }
        if ($authUrl = $this->_identifier->identify($url)) {
            return new Result($authUrl, ResultInterface::SUCCESS);
        }
        return new Result(null, ResultInterface::FAILURE_IDENTITY_NOT_FOUND);
    }

    /**
     * Persists the users data
     * @param ServerRequestInterface $request  The request object.
     * @param ResponseInterface      $response The response object.
     * @param ArrayAccess|array      $identity Identity data to persist.
     * @return array
     */
    public function persistIdentity(ServerRequestInterface $request, ResponseInterface $response, $identity): array
    {
        $sessionKey = $this->getConfig('sessionKey');
        /** @var \Cake\Http\Session $session */
        $session = $request->getAttribute('session');

        if (!$session->check($sessionKey)) {
            $session->renew();
        } elseif (!$session->read($sessionKey.'.url_id') && Hash::get($identity, 'url_id')) {
            $session->write($sessionKey, $identity);
        }

        return [
            'request' => $request,
            'response' => $response,
        ];
    }
}
