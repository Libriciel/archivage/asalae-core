<?php
/**
 * AsalaeCore\Auth\Authenticator\OpenIDConnectAuthenticator
 */

namespace AsalaeCore\Auth\Authenticator;

use ArrayAccess;
use AsalaeCore\Auth\Identifier\UnknownUserException;
use AsalaeCore\Factory\Utility;
use Authentication\Authenticator\AbstractAuthenticator;
use Authentication\Authenticator\PersistenceInterface;
use Authentication\Authenticator\Result;
use Authentication\Authenticator\ResultInterface;
use Cake\Datasource\EntityInterface;
use Cake\Http\Client;
use Cake\Http\Session;
use Cake\Utility\Hash;
use Exception;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * KeycloakAuthenticator
 *
 * @category Authenticator
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2022, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class OpenIDConnectAuthenticator extends AbstractAuthenticator implements PersistenceInterface
{
    /**
     * Default config for this object.
     * - `fields` The fields to use to identify a user by.
     *
     * @var array
     */
    protected $_defaultConfig = [
        'client_id' => null,
        'client_secret' => null,
        'base_url' => null,
        'refresh_expires_in' => null,
        'identityAttribute' => 'identity',
        'sessionKey' => 'Auth',
        'sessionKeyAuthenticator' => 'OpenIDConnect',
        'endpoints' => [],
    ];

    /**
     * @var array réponse d'auth
     */
    protected $auth = [];

    /**
     * Authenticate a user based on the request information.
     *
     * @param ServerRequestInterface $request Request to get authentication information from.
     * @return ResultInterface Returns a result object.
     * @throws Exception
     */
    public function authenticate(ServerRequestInterface $request): ResultInterface
    {
        $sessionKey = $this->getConfig('sessionKey');
        /** @var \Cake\Http\Session $session */
        $session = $request->getAttribute('session');
        $this->auth = $session->read($this->getConfig('sessionKeyAuthenticator'));

        // Utilisateur déjà connecté
        if ($this->auth) {
            $result = $this->renewToken($request);
            if (!$result->isValid()) {
                $session->delete($this->getConfig('sessionKeyAuthenticator'));
                $session->delete($sessionKey);
            }
            return $result;
        }

        // login
        if ($request->getQueryParams()['code'] ?? false) {
            $code = $request->getQueryParams()['code'];
            return $this->accessToken(
                [
                    'grant_type' => 'authorization_code',
                    'code' => $code,
                ]
            );
        }

        $auth = trim($request->getHeaderLine('Authorization'));
        if ($auth && strpos($auth, ' ') !== false) {
            [$type, $token] = explode(' ', $auth);
            if ($type === 'Bearer') {
                if ($user = $this->_identifier->identify(['access_token' => $token])) {
                    return new Result($user, ResultInterface::SUCCESS);
                }
            }
        }

        return new Result(null, ResultInterface::FAILURE_IDENTITY_NOT_FOUND);
    }

    /**
     * Lance un appel http pour optenir le token
     * @param array $data
     * @param array $options
     * @return Result
     * @throws Exception
     */
    protected function accessToken(array $data = [], array $options = []): Result
    {
        /** @var Client $client */
        $client = Utility::get(Client::class);
        foreach (['client_id', 'client_secret', 'redirect_uri', 'scope'] as $add) {
            if ($value = $this->getConfig($add)) {
                $data[$add] = $value;
            }
        }
        $response = $client->post(
            $this->getConfig('endpoints.token'),
            $data,
            $options + $this->getConfig('clientParams', [])
        );
        $json = json_decode($response->getStringBody(), true) ?: [];
        if ($json && !empty($json['access_token'])) {
            /** @var EntityInterface|null $user */
            $user = $this->_identifier->identify($json);
            if ($user && $user->id === null) {
                $json['time'] = time();
                $this->logout($json);
                $exception = new UnknownUserException();
                if (!empty($user->get('username'))) {
                    $exception->username = $user->get('username');
                }
                throw $exception;
            }
            if ($user) {
                $json['time'] = time();
                $this->auth = $json;
                return new Result($user, ResultInterface::SUCCESS);
            } else {
                $this->logout($json);
            }
        } elseif ($json) {
            file_put_contents(
                LOGS . 'last_failed_accesstoken.log',
                json_encode($json, JSON_PRETTY_PRINT)
            );
        }
        return new Result(null, ResultInterface::FAILURE_IDENTITY_NOT_FOUND);
    }

    /**
     * Persists the users data
     *
     * @param ServerRequestInterface $request  The request object.
     * @param ResponseInterface      $response The response object.
     * @param ArrayAccess|array      $identity Identity data to persist.
     * @return array
     */
    public function persistIdentity(ServerRequestInterface $request, ResponseInterface $response, $identity): array
    {
        if (!Hash::get($identity, 'auth_openid')) {
            return [
                'request' => $request,
                'response' => $response,
            ];
        }
        $sessionKey = $this->getConfig('sessionKey');
        /** @var Session $session */
        $session = $request->getAttribute('session');

        if (!$session->check($sessionKey)) {
            $session->renew();
            $session->write($sessionKey, $identity);
        } elseif (!$session->read($sessionKey.'.id') && Hash::get($identity, 'id')) {
            $session->write($sessionKey, $identity);
        }

        if ($this->auth) {
            $session->write($this->getConfig('sessionKeyAuthenticator'), $this->auth);
        } else {
            $this->auth = $session->read($this->getConfig('sessionKeyAuthenticator'));
        }

        return [
            'request' => $request,
            'response' => $response,
        ];
    }

    /**
     * Clears the identity data
     *
     * @param ServerRequestInterface $request  The request object.
     * @param ResponseInterface      $response The response object.
     * @return array
     * @throws Exception
     */
    public function clearIdentity(ServerRequestInterface $request, ResponseInterface $response): array
    {
        $sessionKey = $this->getConfig('sessionKey');
        /** @var Session $session */
        $session = $request->getAttribute('session');
        $this->auth = $session->read($this->getConfig('sessionKeyAuthenticator'));
        $session->delete($sessionKey);
        $session->renew();

        if ($this->auth) {
            $response = $this->logout($this->auth, $response);
        }

        return [
            'request' => $request->withoutAttribute($this->getConfig('identityAttribute')),
            'response' => $response,
        ];
    }

    /**
     * Deconnexion keycloak
     * @param array                  $auth
     * @param ResponseInterface|null $response
     * @return ResponseInterface
     * @throws Exception
     */
    protected function logout(array $auth, ResponseInterface $response = null)
    {
        if (!$this->getConfig('endpoints.end_session')) {
            return $response;
        }
        $additionnals = [
            'client_id' => $this->getConfig('client_id'),
        ];
        if ($secret = $this->getConfig('client_secret')) {
            $additionnals['client_secret'] = $secret;
        }
        /** @var Client $client */
        $client = Utility::get(Client::class);
        if ($this->getConfig('logout_redirection') && $response) {
            return $response->withStatus(302)
                ->withHeader('Location', $this->getConfig('endpoints.end_session'));
        } else {
            $client->post(
                $this->getConfig('endpoints.end_session'),
                (isset($auth['refresh_token'])
                    ? ['refresh_token' => $auth['refresh_token']] + $additionnals
                    : $additionnals)
                + $this->getConfig('clientParams', []),
                ['headers' => ['Authorization' => 'Bearer ' . ($auth['access_token'] ?? '')]]
                + $this->getConfig('clientParams', [])
            );
        }
        return $response;
    }

    /**
     * Renouvel l'authorization d'accès
     * @param ServerRequestInterface $request
     * @return Result
     * @throws Exception
     */
    protected function renewToken(ServerRequestInterface $request)
    {
        /** @var \Cake\Http\Session $session */
        $session = $request->getAttribute('session');
        $auth = $session->read($this->getConfig('sessionKeyAuthenticator'));
        if (!$auth) {
            return new Result(null, ResultInterface::FAILURE_IDENTITY_NOT_FOUND);
        }
        $auth += ['refresh_expires_in' => $this->getConfig('refresh_expires_in')];
        $time = time() + 1;
        $options = ['headers' => ['Authorization' => 'Bearer ' . $auth['access_token']]];
        if (($auth['time'] + $auth['expires_in']) > $time) {
            $user = $this->_identifier->identify($auth) ?: [];
            $sessionAuth = $session->read($this->getConfig('sessionKey'));
            if (isset($sessionAuth['org_entity']['archival_agency'])) {
                $user['org_entity']['archival_agency'] = $sessionAuth['org_entity']['archival_agency'];
            }
            return $user
                ? new Result($user, ResultInterface::SUCCESS)
                : new Result(null, ResultInterface::FAILURE_IDENTITY_NOT_FOUND);
        } elseif (($auth['time'] + $auth['refresh_expires_in']) > $time) {
            return $this->accessToken(
                [
                    'grant_type' => 'refresh_token',
                    'refresh_token' => $auth['refresh_token'],
                ],
                $options
            );
        } else {
            return new Result(null, ResultInterface::FAILURE_IDENTITY_NOT_FOUND);
        }
    }
}
