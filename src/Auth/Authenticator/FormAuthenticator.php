<?php
/**
 * AsalaeCore\Auth\Authenticator\FormAuthenticator
 */

namespace AsalaeCore\Auth\Authenticator;

use Authentication\Authenticator\FormAuthenticator as CakeFormAuthenticator;
use Authentication\Identifier\IdentifierInterface;
use Cake\Routing\Router;
use Cake\Utility\Inflector;
use Psr\Http\Message\ServerRequestInterface;

/**
 * FormAuthenticator
 *
 * @category Authenticator
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class FormAuthenticator extends CakeFormAuthenticator
{
    const ADMINS_CONTROLLER = 'Admins';

    /**
     * Default config for this object.
     * - `fields` The fields to use to identify a user by.
     * - `loginUrl` Login URL or an array of URLs.
     * - `urlChecker` Url checker config.
     *
     * @var array
     */
    protected $_defaultConfig = [
        'loginUrl' => null,
        'urlChecker' => 'Authentication.Default',
        'fields' => [
            IdentifierInterface::CREDENTIAL_USERNAME => 'username',
            IdentifierInterface::CREDENTIAL_PASSWORD => 'password',
        ],
    ];

    /**
     * Checks the fields to ensure they are supplied.
     *
     * @param \Psr\Http\Message\ServerRequestInterface $request The request that contains login information.
     * @return array|null Username and password retrieved from a request body.
     */
    protected function _getData(ServerRequestInterface $request): ?array
    {
        $data = parent::_getData($request);
        if ($data) {
            $url = Router::parseRequest($request);
            $data['admin'] = ($url['controller'] ?? '') === self::ADMINS_CONTROLLER;
        }

        return $data;
    }

    /**
     * Checks the Login URL
     *
     * @param \Psr\Http\Message\ServerRequestInterface $request The request that contains login information.
     * @return bool
     */
    protected function _checkUrl(ServerRequestInterface $request): bool
    {
        $url = Router::parseRequest($request);
        foreach ((array)$this->getConfig('loginUrl') as $checkUrl) {
            [$controller, $action] = explode('/', trim($checkUrl, '/'));
            if (($url['controller'] ?? '') === Inflector::camelize($controller)
                && ($url['action'] ?? '') === Inflector::variable($action)
            ) {
                return true;
            }
        }
        return false;
    }
}
