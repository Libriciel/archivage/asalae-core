<?php
/**
 * AsalaeCore\Auth\Authenticator\KeycloakAuthenticator
 */

namespace AsalaeCore\Auth\Authenticator;

use Authentication\Authenticator\PersistenceInterface;

/**
 * KeycloakAuthenticator
 *
 * @category Authenticator
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class KeycloakAuthenticator extends OpenIDConnectAuthenticator implements PersistenceInterface
{
    /**
     * Default config for this object.
     * - `fields` The fields to use to identify a user by.
     *
     * @var array
     */
    protected $_defaultConfig = [
        'client_id' => 'asalae',
        'client_secret' => null,
        'base_url' => 'http://localhost:8080',
        'realm' => 'master',
        'sessionKey' => 'Auth',
        'sessionKeyAuthenticator' => 'Keycloak',
        'identify' => false,
        'identityAttribute' => 'identity',
        'clientParams' => [],
        'endpoints' => [],
    ];
}
