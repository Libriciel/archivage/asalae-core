<?php
/**
 * AsalaeCore\Auth\Authenticator\BasicAuthenticator
 */

namespace AsalaeCore\Auth\Authenticator;

use Authentication\Authenticator\AbstractAuthenticator;
use Authentication\Authenticator\Result;
use Authentication\Authenticator\ResultInterface;
use Authentication\Identifier\IdentifierInterface;
use Cake\Datasource\EntityInterface;
use Cake\Http\ServerRequest;
use Psr\Http\Message\ServerRequestInterface;

/**
 * BasicAuthenticator
 *
 * @category Authenticator
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class BasicAuthenticator extends AbstractAuthenticator
{
    const ADMINS_CONTROLLER = 'Admins';

    /**
     * Authenticate a user based on the request information.
     *
     * @param ServerRequestInterface|ServerRequest $request Request to get authentication information from.
     * @return ResultInterface Returns a result object.
     */
    public function authenticate(ServerRequestInterface $request): ResultInterface
    {
        $auth = trim($request->getHeaderLine('Authorization'));
        if ($auth && strpos($auth, ' ') !== false) {
            [$type, $token] = explode(' ', $auth);
            if ($type === 'Basic') {
                $data = explode(':', base64_decode($token));
                $usernameField = $this->getConfig('fields.'.IdentifierInterface::CREDENTIAL_USERNAME);
                $passwordField = $this->getConfig('fields.'.IdentifierInterface::CREDENTIAL_PASSWORD);
                $user = $this->_identifier->identify(
                    [
                        $usernameField => $data[0] ?? '',
                        $passwordField => $data[1] ?? '',
                    ]
                );
                if ($user instanceof EntityInterface) {
                    $user->set('auth_basic', true);
                }
                if ($user) {
                    return new Result($user, ResultInterface::SUCCESS);
                }
            }
        }
        return new Result(null, ResultInterface::FAILURE_IDENTITY_NOT_FOUND);
    }
}
