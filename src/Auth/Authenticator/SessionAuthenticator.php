<?php
/**
 * AsalaeCore\Auth\Authenticator\SessionAuthenticator
 */

namespace AsalaeCore\Auth\Authenticator;

use ArrayAccess;
use ArrayObject;
use Authentication\Authenticator\PersistenceInterface;
use Authentication\Authenticator\Result;
use Authentication\Authenticator\ResultInterface;
use Authentication\Authenticator\SessionAuthenticator as CakeSessionAuthenticator;
use Cake\Http\Session;
use Cake\Utility\Hash;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * SessionAuthenticator
 *
 * @category Authenticator
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class SessionAuthenticator extends CakeSessionAuthenticator implements PersistenceInterface
{
    /**
     * Authenticate a user using session data.
     *
     * @param \Psr\Http\Message\ServerRequestInterface $request The request to authenticate with.
     * @return \Authentication\Authenticator\ResultInterface
     */
    public function authenticate(ServerRequestInterface $request): ResultInterface
    {
        $sessionKey = $this->getConfig('sessionKey');
        /** @var Session $session */
        $session = $request->getAttribute('session');
        $user = $session->read($sessionKey);
        if (empty($user) || (!Hash::get($user, 'id') && !Hash::get($user, 'admin'))) {
            return new Result(null, ResultInterface::FAILURE_IDENTITY_NOT_FOUND);
        }

        if (!($user instanceof ArrayAccess)) {
            $user = new ArrayObject($user);
        }

        return new Result($user, ResultInterface::SUCCESS);
    }

    /**
     * Persists the users data
     * @param ServerRequestInterface $request  The request object.
     * @param ResponseInterface      $response The response object.
     * @param ArrayAccess|array      $identity Identity data to persist.
     * @return array
     */
    public function persistIdentity(ServerRequestInterface $request, ResponseInterface $response, $identity): array
    {
        if ((!Hash::get($identity, 'id') && !Hash::get($identity, 'admin'))
            || Hash::get($identity, 'auth_basic')
            || Hash::get($identity, 'auth_openid')
        ) {
            return [
                'request' => $request,
                'response' => $response,
            ];
        }
        $sessionKey = $this->getConfig('sessionKey');
        /** @var \Cake\Http\Session $session */
        $session = $request->getAttribute('session');

        if (!$session->check($sessionKey)) {
            $session->renew();
            $session->write($sessionKey, $identity);
        } elseif (!$session->read($sessionKey.'.id') && Hash::get($identity, 'id')) {
            $session->write($sessionKey, $identity);
        }

        return [
            'request' => $request,
            'response' => $response,
        ];
    }
}
