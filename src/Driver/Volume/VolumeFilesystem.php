<?php
/**
 * AsalaeCore\Driver\Volume\VolumeFilesystem
 */

namespace AsalaeCore\Driver\Volume;

use AsalaeCore\Exception\IntegrityException;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use Cake\Log\Log;
use DateTime;
use DirectoryIterator;
use Exception;

/**
 * Gestion de volume type Filesystem (ex: /data/Volume01)
 *
 * @category Driver/Volume
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class VolumeFilesystem implements VolumeInterface
{
    /**
     * Traits
     */
    use VolumeTestTrait;

    /**
     * @var string
     */
    protected $path;

    /**
     * Constructeur de la classe du driver
     * @param array $driverSettings tableau associatif clé-valeur du driver
     * @throws VolumeException  VOLUME_NOT_ACCESSIBLE
     *                          Le volume de stockage n'existe pas
     *                          ou n'est pas accessible
     */
    public function __construct(array $driverSettings)
    {
        $this->path = rtrim($driverSettings['path'] ?? '', DS);
        if (!$this->ping()) {
            Log::debug(
                sprintf(
                    'error: %s:%s:%d:ping() = false',
                    __CLASS__,
                    __FUNCTION__,
                    __LINE__
                )
            );
            throw new VolumeException(VolumeException::VOLUME_NOT_ACCESSIBLE);
        }
    }

    /**
     * Teste la disponibilité et l'accessibilité d'un volume de stockage.
     * @return bool true si le volume est disponible et accessible,
     *              false dans le cas contraire
     */
    public function ping(): bool
    {
        return is_writable($this->path);
    }

    /**
     * Retourne true si le volume de stockage est vide et false dans le cas contraire
     * @return bool true si le volume est vide,
     *              false dans le cas contraire
     */
    public function isEmpty(): bool
    {
        if (!$this->ping()) {
            return false;
        }
        $handle = opendir($this->path);
        while ($entry = readdir($handle)) {
            if ($entry !== "." && $entry !== "..") {
                closedir($handle);
                return false;
            }
        }
        closedir($handle);
        return true;
    }

    /**
     * Retourne le contenu d'un fichier sur un volume de stockage
     * en fournissant sa référence de stockage.
     * @param string      $storageReference
     *      Référence de stockage du fichier sur le volume.
     *      Cette référence a été retournée par les fonctions d'écriture
     *      des fichiers sur le volume.
     * @param string      $hashAlgo
     * @param string|null $hash
     * @return string binaire du fichier
     * @throws VolumeException FILE_NOT_FOUND
     *          Le fichier n'existe pas ou n'est pas accessible
     *      FILE_READ_ERROR
     *          La lecture du fichier ne s'est pas correctement effectuée
     */
    public function fileGetContent(
        string $storageReference,
        string $hashAlgo = 'crc32',
        string $hash = null
    ): string {
        $uri = $this->getAbsolutePath($storageReference);
        if (!file_exists($uri)) {
            Log::debug(
                sprintf(
                    'error: %s:%s:%d:fileExists($uri) = false (%s)',
                    __CLASS__,
                    __FUNCTION__,
                    __LINE__,
                    $uri
                )
            );
            throw new VolumeException(VolumeException::FILE_NOT_FOUND);
        } elseif (!is_readable($uri)) {
            Log::debug(
                sprintf(
                    'error: %s:%s:%d:is_readable($uri) = false (%s)',
                    __CLASS__,
                    __FUNCTION__,
                    __LINE__,
                    $uri
                )
            );
            throw new VolumeException(VolumeException::FILE_READ_ERROR);
        }

        if ($hashAlgo && $hash) {
            $currentHash = hash_file($hashAlgo, $uri);
            if ($currentHash !== $hash) {
                throw new IntegrityException(
                    __(
                        "Test d'intégrité en échec sur le fichier {0}. "
                        . "Hash d'origine (BDD): {1} ; Hash actuel: {2}",
                        $storageReference,
                        $hash,
                        $currentHash
                    )
                );
            }
        }
        return file_get_contents($uri);
    }

    /**
     * Ecrit un fichier présent sur le FileSystem
     * dans un fichier de destination sur un volume de stockage.
     * Le nom du fichier de destination doit être unique et peut comporter une
     * arborescence de dossiers et sous dossiers.
     * L'arborescence utilise le caractère / comme le système de fichier linux.
     * @param string $sourceFileUri
     *      Chemin absolu du fichier à copier sur le volume de stockage.
     * @param string $destinationFilename
     *      Nom du fichier de destination comportant une arborescence relative
     *      (ex: /ARC001/publications/offre_01.odt).
     *      Le fichier de destination ne doit pas exister.
     * @return string référence de stockage du fichier sur le volume de stockage.
     * @throws \AsalaeCore\Driver\Volume\Exception\VolumeException
     *      FILE_ALREADY_EXISTS
     *          Le fichier de destination existe déjà sur le volume de stockage
     *      FILE_WRITE_ERROR
     *          L'écriture du fichier de destination ne s'est pas correctement effectuée
     *      LOCAL_FILE_NOT_ACCESSIBLE
     *          Le fichier source n'existe pas ou n'est pas accessible
     */
    public function fileUpload(string $sourceFileUri, string $destinationFilename): string
    {
        $uri = $this->getAbsolutePath($destinationFilename);
        if (file_exists($uri)) {
            if (filesize($sourceFileUri) === filesize($uri)
                && hash_file('crc32', $sourceFileUri) === hash_file('crc32', $uri)
            ) {
                return $destinationFilename;
            }
            Log::debug(
                sprintf(
                    'error: %s:%s:%d:file already exists (%s)',
                    __CLASS__,
                    __FUNCTION__,
                    __LINE__,
                    $destinationFilename
                )
            );
            throw new VolumeException(VolumeException::FILE_ALREADY_EXISTS);
        } elseif (!is_readable($sourceFileUri)) {
            Log::debug(
                sprintf(
                    'error: %s:%s:%d:is_readable($sourceFileUri) = false (%s)',
                    __CLASS__,
                    __FUNCTION__,
                    __LINE__,
                    $sourceFileUri
                )
            );
            throw new VolumeException(VolumeException::LOCAL_FILE_NOT_ACCESSIBLE);
        }
        $dirname = dirname($uri);
        $dirExists = !file_exists($dirname) ? mkdir($dirname, 0777, true) : is_dir($dirname);
        if (!$dirExists || !is_writable($dirname) || !copy($sourceFileUri, $uri)) {
            Log::debug(
                sprintf(
                    'error: %s:%s:%d:$dirname not exists nor writable nor copyable (%s)',
                    __CLASS__,
                    __FUNCTION__,
                    __LINE__,
                    $dirname
                )
            );
            throw new VolumeException(VolumeException::FILE_WRITE_ERROR);
        }
        return $destinationFilename;
    }

    /**
     * Ecrit le contenu d'un fichier dans un fichier de destination
     * sur un volume de stockage. Le nom du fichier de destination doit
     * être unique et peut comporter une arborescence de dossiers et sous dossiers.
     * L'arborescence utilise le caractère / comme le système de fichier linux.
     * @param string $destinationFilename
     * @param string $content
     * @return string référence de stockage du fichier sur le volume de stockage.
     * @throws \AsalaeCore\Driver\Volume\Exception\VolumeException
     *      FILE_ALREADY_EXISTS
     *          Le fichier de destination existe déjà sur le volume de stockage
     *      FILE_WRITE_ERROR
     *          L'écriture du fichier de destination ne s'est pas correctement effectuée
     */
    public function filePutContent(string $destinationFilename, string $content): string
    {
        $uri = $this->getAbsolutePath($destinationFilename);
        if (file_exists($uri)) {
            if (filesize($uri) === strlen($content)
                && hash_file('crc32', $uri) === hash('crc32', $content)
            ) {
                return $destinationFilename;
            }
            Log::debug(
                sprintf(
                    'error: %s:%s:%d:file already exists (%s)',
                    __CLASS__,
                    __FUNCTION__,
                    __LINE__,
                    $destinationFilename
                )
            );
            throw new VolumeException(VolumeException::FILE_ALREADY_EXISTS);
        }
        $dirname = dirname($uri);
        $dirExists = is_dir($dirname) || mkdir($dirname, 0777, true);
        if (!$dirExists || !is_writable($dirname) || file_put_contents($uri, $content) === false) {
            Log::debug(
                sprintf(
                    'error: %s:%s:%d:$dirname not exists nor writable or file_put_contents failed (%s)',
                    __CLASS__,
                    __FUNCTION__,
                    __LINE__,
                    $dirname
                )
            );
            throw new VolumeException(VolumeException::FILE_WRITE_ERROR);
        }
        return $destinationFilename;
    }

    /**
     * Supprime un fichier sur un volume de stockage en fournissant sa
     * référence de stockage. Dans le cas d'un volume de type File System,
     * il faut utiliser la commande shred pour supprimer les fichiers.
     * @param string $storageReference
     *      Référence de stockage du fichier sur le volume.
     *      Cette référence a été retournée par les fonctions d'écriture des fichiers sur le volume.
     * @throws \AsalaeCore\Driver\Volume\Exception\VolumeException
     *      FILE_NOT_FOUND
     *          Le fichier n'est pas présent ou n'est pas accessible.
     *      FILE_DELETE_ERROR
     *          La suppression du fichier ne s'est pas correctement effectuée.
     */
    public function fileDelete(string $storageReference)
    {
        $uri = $this->getAbsolutePath($storageReference);
        if (!file_exists($uri)) {
            Log::debug(
                sprintf(
                    'error: %s:%s:%d:fileExists($uri) = false (%s)',
                    __CLASS__,
                    __FUNCTION__,
                    __LINE__,
                    $uri
                )
            );
            throw new VolumeException(VolumeException::FILE_NOT_FOUND);
        } elseif (!is_writable($uri) || !unlink($uri)) {
            Log::debug(
                sprintf(
                    'error: %s:%s:%d:$uri is not writable nor unlinkable (%s)',
                    __CLASS__,
                    __FUNCTION__,
                    __LINE__,
                    $uri
                )
            );
            throw new VolumeException(VolumeException::FILE_DELETE_ERROR);
        }
        $this->removeEmptyFolders(dirname($uri));
    }

    /**
     * Supprime les dossiers vide
     * @param string $path
     * @return void|null
     */
    private function removeEmptyFolders($path)
    {
        if (!is_dir($path) || $path === $this->path) {
            return null;
        }
        if (count(glob($path.DS."{*,.[!.]*,..?*}", GLOB_BRACE)) === 0) {
            rmdir($path);
            $this->removeEmptyFolders(dirname($path));
        }
    }

    /**
     * Teste la présence d'un fichier sur un volume de stockage
     * en fournissant sa référence de stockage
     * @param string $storageReference Référence de stockage du fichier sur le volume.
     *                                 Cette référence a été retournée par les fonctions
     *                                 d'écriture des fichiers sur le volume.
     * @return bool true si le fichier est présent, false dans le cas contraire
     */
    public function fileExists(string $storageReference): bool
    {
        return file_exists($this->getAbsolutePath($storageReference));
    }

    /**
     * Teste la présence d'un dossier sur un volume de stockage
     * en fournissant sa référence de stockage
     * @param string $storageReference
     * @return bool true si le dossier est présent, false dans le cas contraire
     */
    public function dirExists(string $storageReference): bool
    {
        return is_dir($this->getAbsolutePath($storageReference));
    }

    /**
     * Donne le chemin absolut à partir d'une référence (relative)
     * @param string $storageReference
     * @return string
     */
    protected function getAbsolutePath(string $storageReference): string
    {
        return $this->path.DS.$storageReference;
    }

    /**
     * Ecrit sur le File System un fichier stocké sur un volume de stockage
     * en fournissant sa référence de stockage.
     * @param string      $storageReference
     *      Référence de stockage du fichier sur le volume.
     *      Cette référence a été retournée par les fonctions d'écriture des fichiers sur le volume.
     * @param string      $destinationFileUri
     *      Chemin absolu sur le File System du fichier de destination dans
     *      lequel sera copié le fichier présent sur le volume de stockage.
     * @param string      $hashAlgo
     * @param string|null $hash
     * @throws VolumeException FILE_NOT_FOUND
     *          Le fichier n'est pas présent ou n'est pas accessible.
     *      LOCAL_FILE_WRITE_ERROR
     *          L'écriture du fichier ne s'est pas correctement effectuée
     */
    public function fileDownload(
        string $storageReference,
        string $destinationFileUri,
        string $hashAlgo = 'crc32',
        string $hash = null
    ) {
        $uri = $this->getAbsolutePath($storageReference);
        if (!is_readable($uri)) {
            Log::debug(
                sprintf(
                    'error: %s:%s:%d:is_readable($uri) = false (%s)',
                    __CLASS__,
                    __FUNCTION__,
                    __LINE__,
                    $uri
                )
            );
            throw new VolumeException(VolumeException::FILE_NOT_FOUND);
        }
        if ($hashAlgo && $hash) {
            $currentHash = hash_file($hashAlgo, $uri);
            if ($currentHash !== $hash) {
                throw new IntegrityException(
                    __(
                        "Test d'intégrité en échec sur le fichier {0}. "
                        . "Hash d'origine (BDD): {1} ; Hash actuel: {2}",
                        $storageReference,
                        $hash,
                        $currentHash
                    )
                );
            }
        }
        $dirname = dirname($destinationFileUri);
        $dirExists = is_dir($dirname) || mkdir($dirname, 0777, true);
        if (!$dirExists || !is_writable($dirname) || !copy($uri, $destinationFileUri)) {
            Log::debug(
                sprintf(
                    'error: %s:%s:%d:$dirname not exists nor writable nor copyable (%s)',
                    __CLASS__,
                    __FUNCTION__,
                    __LINE__,
                    $dirname
                )
            );
            throw new VolumeException(VolumeException::LOCAL_FILE_WRITE_ERROR);
        }
    }

    /**
     * Permet de renommer une référence
     * @param string $storageReference
     * @param string $newReference
     * @return string $newReference
     * @throws VolumeException
     */
    public function rename(string $storageReference, string $newReference): string
    {
        $uriIn = $this->getAbsolutePath($storageReference);
        $uriTarget = $this->getAbsolutePath($newReference);
        if (!is_readable($uriIn)) {
            Log::debug(
                sprintf(
                    'error: %s:%s:%d:is_readable($uriIn) = false (%s)',
                    __CLASS__,
                    __FUNCTION__,
                    __LINE__,
                    $uriIn
                )
            );
            throw new VolumeException(VolumeException::FILE_NOT_FOUND);
        }
        $dirname = dirname($uriTarget);
        $dirExists = is_dir($dirname) || mkdir($dirname, 0777, true);
        if (!$dirExists || !is_writable($dirname) || !rename($uriIn, $uriTarget)) {
            Log::debug(
                sprintf(
                    'error: %s:%s:%d:$uriTarget not exists nor writable nor renamable (%s)',
                    __CLASS__,
                    __FUNCTION__,
                    __LINE__,
                    $uriTarget
                )
            );
            throw new VolumeException(VolumeException::FILE_WRITE_ERROR);
        }
        return $newReference;
    }

    /**
     * Permet de copier une référence
     * @param string $storageReference
     * @param string $newReference
     * @return string $newReference
     * @throws VolumeException
     */
    public function copy(string $storageReference, string $newReference): string
    {
        $uriIn = $this->getAbsolutePath($storageReference);
        $uriTarget = $this->getAbsolutePath($newReference);
        if (!is_readable($uriIn)) {
            Log::debug(
                sprintf(
                    'error: %s:%s:%d:is_readable($uriIn) = false (%s)',
                    __CLASS__,
                    __FUNCTION__,
                    __LINE__,
                    $uriIn
                )
            );
            throw new VolumeException(VolumeException::FILE_NOT_FOUND);
        }
        $dirname = dirname($uriTarget);
        $dirExists = is_dir($dirname) || mkdir($dirname, 0777, true);
        if (!$dirExists || !is_writable($dirname) || !copy($uriIn, $uriTarget)) {
            Log::debug(
                sprintf(
                    'error: %s:%s:%d:$uriTarget not exists nor writable nor copyable (%s)',
                    __CLASS__,
                    __FUNCTION__,
                    __LINE__,
                    $uriTarget
                )
            );
            throw new VolumeException(VolumeException::FILE_WRITE_ERROR);
        }
        return $newReference;
    }

    /**
     * Permet le téléchargement du fichier
     * @param string      $storageReference
     * @param string      $hashAlgo
     * @param string|null $hash
     * @return bool hash du fichier lu
     * @throws VolumeException
     */
    public function readfile(
        string $storageReference,
        string $hashAlgo = 'crc32',
        string $hash = null
    ): bool {
        $uri = $this->getAbsolutePath($storageReference);
        if (!file_exists($uri)) {
            Log::debug(
                sprintf(
                    'error: %s:%s:%d:fileExists($uri) = false (%s)',
                    __CLASS__,
                    __FUNCTION__,
                    __LINE__,
                    $uri
                )
            );
            throw new VolumeException(VolumeException::FILE_NOT_FOUND);
        } elseif (!is_readable($uri)) {
            Log::debug(
                sprintf(
                    'error: %s:%s:%d:is_readable($uri) = false (%s)',
                    __CLASS__,
                    __FUNCTION__,
                    __LINE__,
                    $uri
                )
            );
            throw new VolumeException(VolumeException::FILE_READ_ERROR);
        }
        $ctx = hash_init($hashAlgo);
        $fh = fopen($uri, 'r');
        while (!feof($fh)) {
            $buffer = fread($fh, 8192);
            echo $buffer;
            if ($hash) {
                hash_update($ctx, $buffer);
            }
        }
        fclose($fh);
        return !$hash || hash_final($ctx) === $hash;
    }

    /**
     * Donne le hash d'un fichier
     * @param string $storageReference
     * @param string $algo
     * @return string
     * @throws VolumeException
     */
    public function hash(string $storageReference, string $algo = 'sha256'): string
    {
        $uri = $this->getAbsolutePath($storageReference);
        if (!file_exists($uri)) {
            Log::debug(
                sprintf(
                    'error: %s:%s:%d:fileExists($uri) = false (%s)',
                    __CLASS__,
                    __FUNCTION__,
                    __LINE__,
                    $uri
                )
            );
            throw new VolumeException(VolumeException::FILE_NOT_FOUND);
        } elseif (!is_readable($uri)) {
            Log::debug(
                sprintf(
                    'error: %s:%s:%d:is_readable($uri) = false (%s)',
                    __CLASS__,
                    __FUNCTION__,
                    __LINE__,
                    $uri
                )
            );
            throw new VolumeException(VolumeException::FILE_READ_ERROR);
        }
        return hash_file($algo, $uri);
    }

    /**
     * Donne la liste des dossiers / fichiers dans $storageReference
     * @param string $storageReference
     * @param bool   $allFiles
     * @return array
     * @throws VolumeException
     */
    public function ls(string $storageReference = '', bool $allFiles = false): array
    {
        $uri = $this->getAbsolutePath($storageReference);
        if (!is_readable($uri)) {
            Log::debug(
                sprintf(
                    'error: %s:%s:%d:is_readable($uri) = false (%s)',
                    __CLASS__,
                    __FUNCTION__,
                    __LINE__,
                    $uri
                )
            );
            throw new VolumeException(VolumeException::FILE_NOT_FOUND);
        }
        $storageReference = trim($storageReference, '/ ');
        $trimLength = strlen($this->path.DS.$storageReference);
        $output = ['--- /'.$storageReference.' ---'];
        if (is_file($uri)) {
            $output[] = '<comment>'.basename($uri).'</comment>';
            return $output;
        }
        if ($allFiles) {
            foreach ($this->listFiles(rtrim($uri, ' '.DS)) as $file) {
                $filename = ltrim(substr($file, $trimLength), '/');
                $output[] = $filename;
            }
        } else {
            foreach (glob(rtrim($uri, '/').DS.'*') as $file) {
                $filename = ltrim(substr($file, $trimLength), '/');
                if (is_dir($file)) {
                    $filename = '<warning>'.$filename.'/</warning>';
                } else {
                    $filename = '<comment>'.$filename.'</comment>';
                }
                $output[] = $filename;
            }
        }
        return $output;
    }

    /**
     * Liste la totalité des fichiers
     * @param string $dir
     * @return array
     */
    private function listFiles(string $dir): array
    {
        if (!is_readable($dir)) {
            return [];
        }
        $iterator = new DirectoryIterator($dir);
        $files = [];
        foreach ($iterator as $item) {
            if (!$item->isDot()) {
                if ($item->isDir()) {
                    $files = array_merge($this->listFiles("$dir/$item"), $files);
                } else {
                    $files[] = $dir . "/" . $item->getFilename();
                }
            }
        }
        return $files;
    }

    /**
     * Donne les metadonnées d'un fichier
     * @param string $storageReference
     * @return array
     * @throws VolumeException
     * @throws Exception
     */
    public function metadata(string $storageReference): array
    {
        $uri = $this->getAbsolutePath($storageReference);
        if (!is_readable($uri)) {
            Log::debug(
                sprintf(
                    'error: %s:%s:%d:is_readable($uri) = false (%s)',
                    __CLASS__,
                    __FUNCTION__,
                    __LINE__,
                    $uri
                )
            );
            throw new VolumeException(VolumeException::FILE_NOT_FOUND);
        }
        return [
            'modified' => new DateTime('@'.filemtime($uri)),
            'mime' => mime_content_type($uri),
            'size' => filesize($uri),
        ];
    }

    /**
     * Envoi un fichier à un autre volume sans avoir à stocker le fichier
     * @param string          $storageReference
     * @param VolumeInterface $target
     * @param string|null     $targetReference
     * @return string         $newReference
     * @throws VolumeException
     */
    public function streamTo(
        string $storageReference,
        VolumeInterface $target,
        string $targetReference = null
    ): string {
        if ($targetReference === null) {
            $targetReference = $storageReference;
        }
        $uri = $this->getAbsolutePath($storageReference);
        if (!is_readable($uri)) {
            Log::debug(
                sprintf(
                    'error: %s:%s:%d:is_readable($uri) = false (%s)',
                    __CLASS__,
                    __FUNCTION__,
                    __LINE__,
                    $uri
                )
            );
            throw new VolumeException(VolumeException::FILE_NOT_FOUND);
        }
        $handle = fopen($uri, 'r');
        $newReference = $target->streamUpload($handle, $targetReference);
        fclose($handle);
        return $newReference;
    }

    /**
     * Donne le hash d'une resource
     * @param resource $handle
     * @param string   $algo
     * @return string
     */
    private function getStreamHash($handle, $algo = 'crc32'): string
    {
        $ctx = hash_init($algo);
        while (!feof($handle)) {
            $buffer = fgets($handle, 8196);
            hash_update($ctx, $buffer);
        }
        return hash_final($ctx);
    }

    /**
     * Envoi un fichier en streaming
     * @param resource $handle
     * @param string   $targetReference
     * @return string  $newReference
     * @throws VolumeException
     */
    public function streamUpload($handle, string $targetReference): string
    {
        $uri = $this->getAbsolutePath($targetReference);
        if ($this->fileExists($targetReference)) {
            $targetHash = $this->hash($targetReference, 'crc32');
            if ($targetHash === $this->getStreamHash($handle)) {
                return $targetReference;
            }
            Log::debug(
                sprintf(
                    'error: %s:%s:%d:file already exists (%s)',
                    __CLASS__,
                    __FUNCTION__,
                    __LINE__,
                    $targetReference
                )
            );
            throw new VolumeException(VolumeException::FILE_ALREADY_EXISTS);
        }
        $dirname = dirname($uri);
        $dirExists = is_dir($dirname) || mkdir($dirname, 0777, true);
        if (!$dirExists || !is_writable($dirname) || file_put_contents($uri, $handle) === false) {
            Log::debug(
                sprintf(
                    'error: %s:%s:%d:$dirname not exists nor writable nor copyable (%s)',
                    __CLASS__,
                    __FUNCTION__,
                    __LINE__,
                    $dirname
                )
            );
            throw new VolumeException(VolumeException::FILE_WRITE_ERROR);
        }
        return $targetReference;
    }
}
