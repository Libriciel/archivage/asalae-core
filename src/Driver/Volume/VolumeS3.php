<?php
/**
 * AsalaeCore\Driver\Volume\VolumeS3
 */

namespace AsalaeCore\Driver\Volume;

use AsalaeCore\Exception\IntegrityException;
use AsalaeCore\Driver\Volume\Exception\VolumeException;
use Aws\Result;
use Aws\S3\Exception\S3Exception;
use Aws\S3\MultipartUploader;
use Aws\S3\S3Client;
use Cake\Log\Log;
use Exception;
use GuzzleHttp\Psr7\Stream;
use InvalidArgumentException;

/**
 * Gestion de volume type Minio Object Storage
 *
 * @category Driver/Volume
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @link https://docs.aws.amazon.com/aws-sdk-php/v3/api/class-Aws.S3.S3Client.html
 */
class VolumeS3 implements VolumeInterface
{
    /**
     * Traits
     */
    use VolumeTestTrait;

    const MULTIPART_MIN_SIZE = 1000000;

    /**
     * @var S3Client client S3 de AWS SDK
     */
    private $client;

    /**
     * @var string nom du bucket
     */
    private $bucket;

    /**
     * Constructeur de la classe du driver
     * @param array $driverSettings tableau associatif clé-valeur du driver
     * @throws InvalidArgumentException Erreur sur le paramétrage du driver
     * @throws VolumeException
     */
    public function __construct(array $driverSettings)
    {
        // vérification des paramètres
        $required = [
            'endpoint',
            'bucket',
            'credentials_key',
            'credentials_secret',
        ];
        foreach ($required as $key) {
            if (empty($driverSettings[$key])) {
                Log::debug(
                    sprintf(
                        'error: %s:%s:%d:missing required argument (%s)',
                        __CLASS__,
                        __FUNCTION__,
                        __LINE__,
                        $key
                    )
                );
                throw new InvalidArgumentException("Missing key ".$key." in array argument");
            }
            $this->$key = $driverSettings[$key];
        }
        // copie des paramètres
        $this->bucket = $driverSettings['bucket'];

        // initialisation du client S3
        if (!empty($driverSettings['client'])) {
            $this->client = $driverSettings['client'];
        } else {
            $httpOptions = ['connect_timeout' => $driverSettings['connect_timeout'] ?? 5];
            if (!empty($driverSettings['use_proxy']) && !empty($driverSettings['proxy_host'])) {
                // url proxy sous la forme https://username:password@192.168.16.1:10
                /** @noinspection HttpUrlsUsage */
                $proxyProtocol = strpos($driverSettings['proxy_host'], "https://") === false ? "http://" : "https://";
                $proxyUrl = str_replace($proxyProtocol, '', $driverSettings['proxy_host']);
                $proxyUserPasswd = $driverSettings['proxy_login']
                    .($driverSettings['proxy_password'] ? ':'.$driverSettings['proxy_password'] : '');
                $proxyUserPasswd = $proxyUserPasswd ? $proxyUserPasswd.'@' : $proxyUserPasswd;
                $proxyPort = $driverSettings['proxy_port'] ? ':'.$driverSettings['proxy_port'] : '';
                $httpOptions['proxy'] = "$proxyProtocol$proxyUserPasswd$proxyUrl$proxyPort";
            }
            if (!empty($driverSettings['verify'])) {
                if ($driverSettings['verify'] === 'false') {
                    $httpOptions['verify'] = false;
                } else {
                    $httpOptions['verify'] = $driverSettings['verify'];
                }
            }
            $this->client = new S3Client(
                [
                    'version' => 'latest',
                    'region'  => ($driverSettings['region'] ?? 'EU') ?: 'EU',
                    'endpoint' => $driverSettings['endpoint'],
                    'use_path_style_endpoint' => true,
                    'credentials' => [
                        'key'    => $driverSettings['credentials_key'],
                        'secret' => $driverSettings['credentials_secret'],
                    ],
                    'http' => $httpOptions,
                    'suppress_php_deprecation_warning' => true,
                ]
            );
        }
        if (!$this->ping()) {
            Log::debug(
                sprintf(
                    'error: %s:%s:%d:ping() = false',
                    __CLASS__,
                    __FUNCTION__,
                    __LINE__
                )
            );
            throw new VolumeException(VolumeException::VOLUME_NOT_ACCESSIBLE);
        }
    }

    /**
     * retourne true si le volume est accessible et fonctionnel
     * @return bool
     * @throws Exception
     */
    public function ping(): bool
    {
        try {
            return $this->client->doesBucketExist($this->bucket);
        } catch (Exception $e) {
            Log::debug(
                sprintf(
                    'error: %s:%s:%d:Exception : %s',
                    __CLASS__,
                    __FUNCTION__,
                    __LINE__,
                    $e
                )
            );
            return false;
        }
    }

    /**
     * Retourne true si le volume de stockage est vide et false dans le cas contraire
     * @return bool true si le volume est vide,
     *              false dans le cas contraire
     */
    public function isEmpty(): bool
    {
        try {
            $objects = $this->client->listObjectsV2(
                [
                    'Bucket' => $this->bucket,
                    'MaxKeys' => 1
                ]
            );
            return $objects && $objects->get('Contents') === null;
        } catch (Exception $e) {
            Log::debug(
                sprintf(
                    'error: %s:%s:%d:Exception : %s',
                    __CLASS__,
                    __FUNCTION__,
                    __LINE__,
                    $e
                )
            );
            return false;
        }
    }

    /**
     * Ecrit le contenu d'un fichier dans un fichier de destination
     * sur un volume de stockage. Le nom du fichier de destination doit
     * être unique et peut comporter une arborescence de dossiers et sous dossiers.
     * L'arborescence utilise le caractère / comme le système de fichier linux.
     * @param string $destinationFilename
     * @param string $content
     * @return string référence de stockage du fichier sur le volume de stockage.
     * @throws \AsalaeCore\Driver\Volume\Exception\VolumeException
     *      FILE_ALREADY_EXISTS
     *          Le fichier de destination existe déjà sur le volume de stockage
     *      FILE_WRITE_ERROR
     *          L'écriture du fichier de destination ne s'est pas correctement effectuée
     */
    public function filePutContent(string $destinationFilename, string $content): string
    {
        if ($this->fileExists($destinationFilename)) {
            if ($this->compareFile($destinationFilename, strlen($content), hash('crc32', $content))) {
                return $destinationFilename;
            }
            Log::debug(
                sprintf(
                    'error: %s:%s:%d:file already exists (%s)',
                    __CLASS__,
                    __FUNCTION__,
                    __LINE__,
                    $destinationFilename
                )
            );
            throw new VolumeException(VolumeException::FILE_ALREADY_EXISTS);
        }
        $url = $this->fileUrl($destinationFilename);
        try {
            $this->client->putObject(
                [
                    'Bucket' => $this->bucket,
                    'Key' => $url,
                    'Body' => $content
                ]
            );
        } catch (S3Exception $e) {
            Log::debug(
                sprintf(
                    'error: %s:%s:%d:S3Exception : %s',
                    __CLASS__,
                    __FUNCTION__,
                    __LINE__,
                    $e
                )
            );
            throw new VolumeException(VolumeException::FILE_WRITE_ERROR, 0, $e);
        }
        return $destinationFilename;
    }

    /**
     * Retourne le contenu d'un fichier sur un volume de stockage
     * en fournissant sa référence de stockage.
     * @param string      $storageReference
     *      Référence de stockage du fichier sur le volume.
     *      Cette référence a été retournée par les fonctions d'écriture
     *      des fichiers sur le volume.
     * @param string|null $hashAlgo
     * @param string|null $hash
     * @return string binaire du fichier
     * @throws VolumeException FILE_NOT_FOUND
     *          Le fichier n'existe pas ou n'est pas accessible
     *      FILE_READ_ERROR
     *          La lecture du fichier ne s'est pas correctement effectuée
     */
    public function fileGetContent(
        string $storageReference,
        string $hashAlgo = 'crc32',
        string $hash = null
    ): string {
        if (!$this->fileExists($storageReference)) {
            Log::debug(
                sprintf(
                    'error: %s:%s:%d:fileExists($storageReference) = false (%s)',
                    __CLASS__,
                    __FUNCTION__,
                    __LINE__,
                    $storageReference
                )
            );
            throw new VolumeException(VolumeException::FILE_NOT_FOUND);
        }
        $url = $this->fileUrl($storageReference);
        try {
            $response = $this->client->getObject(
                [
                    'Bucket' => $this->bucket,
                    'Key' => $url
                ]
            );
        } catch (S3Exception $e) {
            Log::debug(
                sprintf(
                    'error: %s:%s:%d:S3Exception : %s',
                    __CLASS__,
                    __FUNCTION__,
                    __LINE__,
                    $e
                )
            );
            throw new VolumeException(VolumeException::FILE_READ_ERROR, 0, $e);
        }

        $content = $response->get('Body')->getContents();
        if ($hashAlgo && $hash) {
            $currentHash = hash($hashAlgo, $content);
            if ($currentHash !== $hash) {
                throw new IntegrityException(
                    __(
                        "Test d'intégrité en échec sur le fichier {0}. "
                        . "Hash d'origine (BDD): {1} ; Hash actuel: {2}",
                        $storageReference,
                        $hash,
                        $currentHash
                    )
                );
            }
        }
        return $content;
    }

    /**
     * Vrai si fichier correspond à size et hash
     * @param string $storageReference
     * @param int    $size
     * @param string $hash
     * @param string $algo
     * @return bool
     * @throws VolumeException
     */
    public function compareFile(
        string $storageReference,
        int $size,
        string $hash,
        string $algo = 'crc32'
    ): bool {
        $url = $this->fileUrl($storageReference);
        try {
            $response = $this->client->getObject(
                [
                    'Bucket' => $this->bucket,
                    'Key' => $url
                ]
            );
        } catch (S3Exception $e) {
            Log::debug(
                sprintf(
                    'error: %s:%s:%d:S3Exception : %s',
                    __CLASS__,
                    __FUNCTION__,
                    __LINE__,
                    $e
                )
            );
            throw new VolumeException(VolumeException::FILE_READ_ERROR, 0, $e);
        }
        if ($response->get('ContentLength') !== $size) {
            return false;
        }
        /** @var Stream $stream */
        $stream = $response->get('Body');
        $ctx = hash_init($algo);
        while (!$stream->eof()) {
            hash_update($ctx, $stream->read(8192));
        }
        $stream->close();
        return hash_final($ctx) === $hash;
    }

    /**
     * Ecrit un fichier présent sur le FileSystem
     * dans un fichier de destination sur un volume de stockage.
     * Le nom du fichier de destination doit être unique et peut comporter une
     * arborescence de dossiers et sous dossiers.
     * L'arborescence utilise le caractère / comme le système de fichier linux.
     * @param string $sourceFileUri
     *      Chemin absolu du fichier à copier sur le volume de stockage.
     * @param string $destinationFilename
     *      Nom du fichier de destination comportant une arborescence relative
     *      (ex: /ARC001/publications/offre_01.odt).
     *      Le fichier de destination ne doit pas exister.
     * @return string référence de stockage du fichier sur le volume de stockage.
     * @throws \AsalaeCore\Driver\Volume\Exception\VolumeException
     *      FILE_ALREADY_EXISTS
     *          Le fichier de destination existe déjà sur le volume de stockage
     *      FILE_WRITE_ERROR
     *          L'écriture du fichier de destination ne s'est pas correctement effectuée
     *      LOCAL_FILE_NOT_ACCESSIBLE
     *          Le fichier source n'existe pas ou n'est pas accessible
     */
    public function fileUpload(string $sourceFileUri, string $destinationFilename): string
    {
        if ($this->fileExists($destinationFilename)
            && !$this->compareFile(
                $destinationFilename,
                filesize($sourceFileUri),
                hash_file('crc32', $sourceFileUri)
            )
        ) {
            Log::debug(
                sprintf(
                    'error: %s:%s:%d:file already exists (%s)',
                    __CLASS__,
                    __FUNCTION__,
                    __LINE__,
                    $destinationFilename
                )
            );
            throw new VolumeException(VolumeException::FILE_ALREADY_EXISTS);
        } elseif (!is_readable($sourceFileUri)) {
            Log::debug(
                sprintf(
                    'error: %s:%s:%d:is_readable($sourceFileUri) = false (%s)',
                    __CLASS__,
                    __FUNCTION__,
                    __LINE__,
                    $sourceFileUri
                )
            );
            throw new VolumeException(VolumeException::LOCAL_FILE_NOT_ACCESSIBLE);
        }
        $url = $this->fileUrl($destinationFilename);
        $size = filesize($sourceFileUri);
        try {
            if ($size > self::MULTIPART_MIN_SIZE) {
                $uploader = new MultipartUploader(
                    $this->client,
                    $sourceFileUri,
                    [
                        'Bucket' => $this->bucket,
                        'Key' => $url,
                    ]
                );
                @$uploader->upload();
            } else {
                $handle = fopen($sourceFileUri, "r");
                $this->client->putObject(
                    [
                        'Bucket' => $this->bucket,
                        'Key' => $url,
                        'Body' => $handle
                    ]
                );
                fclose($handle);
            }
        } catch (Exception $e) {
            Log::debug(
                sprintf(
                    'error: %s:%s:%d:%s : %s',
                    __CLASS__,
                    __FUNCTION__,
                    __LINE__,
                    substr(get_class($e), strrpos(get_class($e), '\\') +1),
                    $e
                )
            );
            throw new VolumeException(VolumeException::FILE_WRITE_ERROR, 0, $e);
        }
        return $destinationFilename;
    }

    /**
     * Ecrit sur le File System un fichier stocké sur un volume de stockage
     * en fournissant sa référence de stockage.
     * @param string      $storageReference
     *      Référence de stockage du fichier sur le volume.
     *      Cette référence a été retournée par les fonctions d'écriture des fichiers sur le volume.
     * @param string      $destinationFileUri
     *      Chemin absolu sur le File System du fichier de destination dans
     *      lequel sera copié le fichier présent sur le volume de stockage.
     * @param string|null $hashAlgo
     * @param string|null $hash
     * @throws VolumeException FILE_NOT_FOUND
     *          Le fichier n'est pas présent ou n'est pas accessible.
     *      LOCAL_FILE_WRITE_ERROR
     *          L'écriture du fichier ne s'est pas correctement effectuée
     */
    public function fileDownload(
        string $storageReference,
        string $destinationFileUri,
        string $hashAlgo = 'crc32',
        string $hash = null
    ) {
        if (!$this->fileExists($storageReference)) {
            Log::debug(
                sprintf(
                    'error: %s:%s:%d:fileExists($storageReference) = false (%s)',
                    __CLASS__,
                    __FUNCTION__,
                    __LINE__,
                    $storageReference
                )
            );
            throw new VolumeException(VolumeException::FILE_NOT_FOUND);
        }
        if (is_file($destinationFileUri) || !is_writable(dirname($destinationFileUri))) {
            Log::debug(
                sprintf(
                    'error: %s:%s:%d:$destinationFileUri is not a file nor writable (%s)',
                    __CLASS__,
                    __FUNCTION__,
                    __LINE__,
                    $destinationFileUri
                )
            );
            throw new VolumeException(VolumeException::LOCAL_FILE_WRITE_ERROR);
        }
        $url = $this->fileUrl($storageReference);
        $ctx = hash_init($hashAlgo);
        try {
            $fh = fopen($destinationFileUri, 'w');
            $response = $this->client->getObject(
                [
                    'Bucket' => $this->bucket,
                    'Key' => $url
                ]
            );
            /** @var Stream $stream */
            $stream = $response->get('Body');
            while (!$stream->eof()) {
                $buffer = $stream->read(8192);
                fwrite($fh, $buffer);
                if ($hashAlgo && $hash) {
                    hash_update($ctx, $buffer);
                }
            }
            $stream->close();
            fclose($fh);
        } catch (S3Exception $e) {
            Log::debug(
                sprintf(
                    'error: %s:%s:%d:S3Exception : %s',
                    __CLASS__,
                    __FUNCTION__,
                    __LINE__,
                    $e
                )
            );
            throw new VolumeException(VolumeException::LOCAL_FILE_WRITE_ERROR, 0, $e);
        }

        if ($hashAlgo && $hash) {
            $currentHash = hash_final($ctx);
            if ($currentHash !== $hash) {
                unlink($destinationFileUri);
                throw new IntegrityException(
                    __(
                        "Test d'intégrité en échec sur le fichier {0}. "
                        . "Hash d'origine (BDD): {1} ; Hash actuel: {2}",
                        $storageReference,
                        $hash,
                        $currentHash
                    )
                );
            }
        }
    }

    /**
     * Supprime un fichier sur un volume de stockage en fournissant sa
     * référence de stockage. Dans le cas d'un volume de type File System,
     * il faut utiliser la commande shred pour supprimer les fichiers.
     * @param string $storageReference
     *      Référence de stockage du fichier sur le volume.
     *      Cette référence a été retournée par les fonctions d'écriture des fichiers sur le volume.
     * @throws \AsalaeCore\Driver\Volume\Exception\VolumeException
     *      FILE_NOT_FOUND
     *          Le fichier n'est pas présent ou n'est pas accessible.
     *      FILE_DELETE_ERROR
     *          La suppression du fichier ne s'est pas correctement effectuée.
     */
    public function fileDelete(string $storageReference)
    {
        if (!$this->fileExists($storageReference)) {
            Log::debug(
                sprintf(
                    'error: %s:%s:%d:fileExists($storageReference) = false (%s)',
                    __CLASS__,
                    __FUNCTION__,
                    __LINE__,
                    $storageReference
                )
            );
            throw new VolumeException(VolumeException::FILE_NOT_FOUND);
        }
        $url = $this->fileUrl($storageReference);
        try {
            $this->client->deleteObject(
                [
                    'Bucket' => $this->bucket,
                    'Key' => $url
                ]
            );
        } catch (S3Exception $e) {
            Log::debug(
                sprintf(
                    'error: %s:%s:%d:S3Exception : %s',
                    __CLASS__,
                    __FUNCTION__,
                    __LINE__,
                    $e
                )
            );
            throw new VolumeException(VolumeException::FILE_DELETE_ERROR, 0, $e);
        }
    }

    /**
     * Teste la présence d'un fichier sur un volume de stockage
     * en fournissant sa référence de stockage
     * @param string $storageReference
     *      Référence de stockage du fichier sur le volume.
     *      Cette référence a été retournée par les fonctions d'écriture des fichiers sur le volume.
     * @return bool true si le fichier est présent, false dans le cas contraire
     * @throws Exception
     */
    public function fileExists(string $storageReference): bool
    {
        $url = $this->fileUrl($storageReference);
        try {
            return $this->client->doesObjectExist($this->bucket, $url);
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Teste la présence d'un dossier sur un volume de stockage
     * en fournissant sa référence de stockage
     * @param string $storageReference
     * @return bool true si le dossier est présent, false dans le cas contraire
     * @throws VolumeException
     */
    public function dirExists(string $storageReference): bool
    {
        $storageReference = trim($storageReference, '/ ');
        try {
            /** @var Result $response */
            $results = $this->client->getPaginator(
                'ListObjects',
                [
                    'Bucket' => $this->bucket,
                    'Prefix' => $storageReference.'/',
                    'MaxKeys' => 1,
                ]
            );
        } catch (S3Exception $e) {
            Log::debug(
                sprintf(
                    'error: %s:%s:%d:S3Exception : %s',
                    __CLASS__,
                    __FUNCTION__,
                    __LINE__,
                    $e
                )
            );
            throw new VolumeException(VolumeException::FILE_READ_ERROR, 0, $e);
        }
        try {
            $result = $results->current();
            if ($result->get('Contents')) {
                $path = $result->get('Contents')[0]['Key'];
                return $storageReference !== $path;
            }
        } catch (S3Exception $e) {
            Log::debug(
                sprintf(
                    'error: %s:%s:%d:S3Exception : %s',
                    __CLASS__,
                    __FUNCTION__,
                    __LINE__,
                    $e
                )
            );
            throw new VolumeException(VolumeException::FILE_READ_ERROR, 0, $e);
        }
        return false;
    }

    /**
     * Donne l'url d'un fichier
     * @param string $file
     * @return string
     */
    private function fileUrl(string $file): string
    {
        return $file;
    }

    /**
     * Permet de renommer une référence
     * @param string $storageReference
     * @param string $newReference
     * @return string $newReference
     * @throws VolumeException
     */
    public function rename(string $storageReference, string $newReference): string
    {
        $this->copy($storageReference, $newReference);
        $this->fileDelete($storageReference);
        return $newReference;
    }

    /**
     * Permet de copier une référence
     * @param string $storageReference
     * @param string $newReference
     * @return string $newReference
     * @throws VolumeException
     */
    public function copy(string $storageReference, string $newReference): string
    {
        $url = $this->fileUrl($newReference);
        if (!$this->fileExists($storageReference)) {
            Log::debug(
                sprintf(
                    'error: %s:%d:fileExists($storageReference) = false (%s)',
                    __CLASS__,
                    __LINE__,
                    $storageReference
                )
            );
            throw new VolumeException(VolumeException::FILE_NOT_FOUND);
        }
        if ($this->fileExists($newReference)) {
            Log::debug(
                sprintf(
                    'error: %s:%d:fileExists($newReference) = true (%s)',
                    __CLASS__,
                    __LINE__,
                    $newReference
                )
            );
            throw new VolumeException(VolumeException::FILE_ALREADY_EXISTS);
        }
        $formatedKey = [];
        foreach (explode('/', $storageReference) as $dir) {
            $formatedKey[] = urlencode($dir);
        }
        try {
            $this->client->copyObject(
                [
                    'Bucket' => $this->bucket,
                    'Key' => $url,
                    'CopySource' => $this->bucket.'/'.implode('/', $formatedKey),
                ]
            );
        } catch (S3Exception $e) {
            Log::debug(
                sprintf(
                    'error: %s:%s:%d:S3Exception : %s',
                    __CLASS__,
                    __FUNCTION__,
                    __LINE__,
                    $e
                )
            );
            throw new VolumeException(VolumeException::FILE_WRITE_ERROR, 0, $e);
        }
        return $newReference;
    }

    /**
     * Renvoi le contenu de fichier dans la sorti standard
     * @param string      $storageReference
     * @param string      $hashAlgo
     * @param string|null $hash
     * @return bool hash du fichier lu égal au fichier d'origine
     * @throws VolumeException
     */
    public function readfile(
        string $storageReference,
        string $hashAlgo = 'crc32',
        string $hash = null
    ): bool {
        if (!$this->fileExists($storageReference)) {
            Log::debug(
                sprintf(
                    'error: %s:%d:fileExists($storageReference) = true (%s)',
                    __CLASS__,
                    __LINE__,
                    $storageReference
                )
            );
            throw new VolumeException(VolumeException::FILE_NOT_FOUND);
        }
        $url = $this->fileUrl($storageReference);
        try {
            $response = $this->client->getObject(
                [
                    'Bucket' => $this->bucket,
                    'Key' => $url
                ]
            );
        } catch (S3Exception $e) {
            Log::debug(
                sprintf(
                    'error: %s:%s:%d:S3Exception : %s',
                    __CLASS__,
                    __FUNCTION__,
                    __LINE__,
                    $e
                )
            );
            throw new VolumeException(VolumeException::FILE_READ_ERROR, 0, $e);
        }
        $ctx = hash_init($hashAlgo);
        /** @var Stream $stream */
        $stream = $response->get('Body');
        while (!$stream->eof()) {
            $buffer = $stream->read(8192);
            echo $buffer;
            if ($hash) {
                hash_update($ctx, $buffer);
            }
        }
        $stream->close();
        return !$hash || hash_final($ctx) === $hash;
    }

    /**
     * Donne le hash d'un fichier
     * @param string $storageReference
     * @param string $algo
     * @return string
     * @throws VolumeException
     */
    public function hash(string $storageReference, string $algo = 'sha256'): string
    {
        if (!$this->fileExists($storageReference)) {
            Log::debug(
                sprintf(
                    'error: %s:%d:fileExists($storageReference) = false (%s)',
                    __CLASS__,
                    __LINE__,
                    $storageReference
                )
            );
            throw new VolumeException(VolumeException::FILE_NOT_FOUND);
        }
        $url = $this->fileUrl($storageReference);
        try {
            $response = $this->client->getObject(
                [
                    'Bucket' => $this->bucket,
                    'Key' => $url
                ]
            );
        } catch (S3Exception $e) {
            Log::debug(
                sprintf(
                    'error: %s:%s:%d:S3Exception : %s',
                    __CLASS__,
                    __FUNCTION__,
                    __LINE__,
                    $e
                )
            );
            throw new VolumeException(VolumeException::FILE_READ_ERROR, 0, $e);
        }
        /** @var Stream $stream */
        $stream = $response->get('Body');
        $ctx = hash_init($algo);
        while (!$stream->eof()) {
            hash_update($ctx, $stream->read(8192));
        }
        $stream->close();
        return hash_final($ctx);
    }

    /**
     * Donne la liste des dossiers / fichiers dans $storageReference
     * @param string $storageReference
     * @param bool   $allFiles
     * @return array
     * @throws VolumeException
     */
    public function ls(string $storageReference = '', bool $allFiles = false): array
    {
        $storageReference = trim($storageReference, '/ ');
        try {
            /** @var Result $response */
            $results = $this->client->getPaginator(
                'ListObjects',
                [
                    'Bucket' => $this->bucket,
                    'Prefix' => $storageReference
                ]
            );
        } catch (S3Exception $e) {
            Log::debug(
                sprintf(
                    'error: %s:%s:%d:S3Exception : %s',
                    __CLASS__,
                    __FUNCTION__,
                    __LINE__,
                    $e
                )
            );
            throw new VolumeException(VolumeException::FILE_READ_ERROR, 0, $e);
        }
        $output = ['--- /'.$storageReference.' ---'];
        try {
            foreach ($results as $result) {
                $this->lsOutput(
                    $result->get('Contents') ?: [],
                    $storageReference,
                    $allFiles,
                    $output
                );
            }
        } catch (S3Exception $e) {
            Log::debug(
                sprintf(
                    'error: %s:%s:%d:S3Exception : %s',
                    __CLASS__,
                    __FUNCTION__,
                    __LINE__,
                    $e
                )
            );
            throw new VolumeException(VolumeException::FILE_READ_ERROR, 0, $e);
        }
        return $output;
    }

    /**
     * Retravail la sortie d'un output du ls()
     * @param array  $content
     * @param string $storageReference
     * @param bool   $allFiles
     * @param array  $output
     * @return void
     */
    private function lsOutput(
        array $content,
        string $storageReference,
        bool $allFiles,
        array &$output
    ) {
        foreach ($content as $v) {
            if ($allFiles) {
                $output[] = $v['Key'] ?? '';
            } else {
                $path = ltrim(
                    substr(
                        $v['Key'] ?? '',
                        strlen($storageReference)
                    ),
                    '/'
                );
                if (strlen($path) === 0) {
                    $path = basename($storageReference);
                }
                $dir = strpos($path, '/');
                if ($dir) {
                    $path = '<warning>'.substr($path, 0, $dir + 1).'</warning>';
                } else {
                    $path = '<comment>'.$path.'</comment>';
                }
                $output[$path] = $path;
            }
        }
    }

    /**
     * Donne les metadonnées d'un fichier
     * @param string $storageReference
     * @return array
     * @throws VolumeException
     */
    public function metadata(string $storageReference): array
    {
        try {
            $response = $this->client->HeadObject(
                [
                    'Bucket' => $this->bucket,
                    'Key' => $storageReference
                ]
            );
        } catch (S3Exception $e) {
            Log::debug(
                sprintf(
                    'error: %s:%s:%d:S3Exception : %s',
                    __CLASS__,
                    __FUNCTION__,
                    __LINE__,
                    $e
                )
            );
            throw new VolumeException(VolumeException::FILE_READ_ERROR, 0, $e);
        }
        return [
            'modified' => $response->get('LastModified'),
            'mime' => $response->get('ContentType'),
            'size' => $response->get('ContentLength'),
        ];
    }

    /**
     * Envoi un fichier à un autre volume sans avoir à stocker le fichier
     * @param string          $storageReference
     * @param VolumeInterface $target
     * @param string|null     $targetReference
     * @return string         $newReference
     * @throws VolumeException
     */
    public function streamTo(string $storageReference, VolumeInterface $target, string $targetReference = null): string
    {
        if ($targetReference === null) {
            $targetReference = $storageReference;
        }
        if (!$this->fileExists($storageReference)) {
            Log::debug(
                sprintf(
                    'error: %s:%s:%d:fileExists($storageReference) = false (%s)',
                    __CLASS__,
                    __FUNCTION__,
                    __LINE__,
                    $storageReference
                )
            );
            throw new VolumeException(VolumeException::FILE_NOT_FOUND);
        }

        $url = $this->fileUrl($storageReference);
        try {
            $response = $this->client->getObject(
                [
                    'Bucket' => $this->bucket,
                    'Key' => $url,
                ]
            );
        } catch (S3Exception $e) {
            Log::debug(
                sprintf(
                    'error: %s:%s:%d:S3Exception : %s',
                    __CLASS__,
                    __FUNCTION__,
                    __LINE__,
                    $e
                )
            );
            throw new VolumeException(VolumeException::LOCAL_FILE_WRITE_ERROR, 0, $e);
        }

        /** @var Stream $stream */
        $stream = $response->get('Body');
        $handle = $stream->detach();
        $newReference = $target->streamUpload($handle, $targetReference);
        fclose($handle);
        return $newReference;
    }

    /**
     * Donne le hash d'une resource
     * @param resource $handle
     * @param string   $algo
     * @return string
     */
    private function getStreamHash($handle, $algo = 'crc32'): string
    {
        $ctx = hash_init($algo);
        while (!feof($handle)) {
            $buffer = fgets($handle, 8196);
            hash_update($ctx, $buffer);
        }
        return hash_final($ctx);
    }

    /**
     * Envoi un fichier en streaming
     * @param resource $handle
     * @param string   $targetReference
     * @return string  $newReference
     * @throws VolumeException
     */
    public function streamUpload($handle, string $targetReference): string
    {
        if ($this->fileExists($targetReference)) {
            $targetHash = $this->hash($targetReference, 'crc32');
            if ($targetHash === $this->getStreamHash($handle)) {
                return $targetReference;
            }
            Log::debug(
                sprintf(
                    'error: %s:%s:%d:file already exists (%s)',
                    __CLASS__,
                    __FUNCTION__,
                    __LINE__,
                    $targetReference
                )
            );
            throw new VolumeException(VolumeException::FILE_ALREADY_EXISTS);
        }
        $url = $this->fileUrl($targetReference);
        try {
            $this->client->putObject(
                [
                    'Bucket' => $this->bucket,
                    'Key' => $url,
                    'Body' => $handle
                ]
            );
        } catch (S3Exception $e) {
            Log::debug(
                sprintf(
                    'error: %s:%s:%d:S3Exception : %s',
                    __CLASS__,
                    __FUNCTION__,
                    __LINE__,
                    $e
                )
            );
            throw new VolumeException(VolumeException::FILE_WRITE_ERROR, 0, $e);
        }
        return $targetReference;
    }
}
