<?php
/**
 * AsalaeCore\Driver\Volume\VolumeTestTrait
 */

namespace AsalaeCore\Driver\Volume;

use AsalaeCore\Driver\Volume\Exception\VolumeException;
use AsalaeCore\Error\AppErrorHandler;
use Cake\Core\Configure;
use Exception;

/**
 * Trait d'un volume à tester
 *
 * @category Controller
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin VolumeInterface
 */
trait VolumeTestTrait
{
    private $testFilename = 'test_file.txt';
    private $testDirname = 'volume-test';

    /**
     * Teste d'écriture, de lecture et de suppression d'un fichier sur le volume
     * @return array
     */
    public function test(): array
    {
        $responseData = ['errors' => []];
        try {
            $errHandler = AppErrorHandler::captureErrors([$this, 'testVolume']);
            $responseData['success'] = $errHandler['result'];
            if (!empty($errHandler['errors'])) {
                $responseData['errors'] = $errHandler['errors'];
            }
        } catch (Exception $e) {
            $responseData['success'] = false;
            $responseData['exception'] = $e->getMessage();
            if (Configure::read('debug')) {
                $responseData['file'] = $e->getFile();
                $responseData['line'] = $e->getLine();
            }
        } finally {
            $this->deleteTestFiles();
        }

        return $responseData;
    }

    /**
     * Batterie de tests pour le volume
     * @return bool
     * @throws VolumeException
     */
    public function testVolume(): bool
    {
        $filename = $this->testFilename;
        $dirname = $this->testDirname;
        $filecontent = md5(rand(0, 1000)); // NOSONAR juste pour un test
        $hash = hash('sha256', $filecontent);

        if ($this->fileExists($filename)) {
            $this->fileDelete($filename);
        }
        if (!$this->filePutContent($filename, $filecontent)) {
            trigger_error(__("Echec lors de l'écriture"));
            return false;
        }
        $content = $this->fileGetContent($filename);
        if (!$content) {
            trigger_error(__("Echec lors de la lecture"));
            return false;
        }
        if ($content !== $filecontent) {
            trigger_error(__("Le fichier a bien été écrit, puis lu, mais son contenu a changé"));
            return false;
        }
        $this->fileDelete($filename);
        if ($this->fileExists($filename)) {
            trigger_error(__("Echec lors de la suppression"));
            return false;
        }

        $tmpFile = TMP . uniqid($filename);
        file_put_contents($tmpFile, $filecontent);
        try {
            $upload = $this->fileUpload($tmpFile, "$dirname/$filename");
            if (!$upload || !$this->fileExists("$dirname/$filename")) {
                // NOTE: si problème 403 sur S3: ajouter la permission pour le MultipartUploader
                trigger_error(__("Echec lors de l'upload"));
                return false;
            }
        } finally {
            unlink($tmpFile);
        }
        if (!$this->dirExists($dirname)) {
            trigger_error(__("Echec de vérification de l'existance du dossier"));
            return false;
        }

        $rename = $this->rename("$dirname/$filename", "$dirname/$filename.bak");
        $fileExists = $this->fileExists("$dirname/$filename");
        $bakExists = $this->fileExists("$dirname/$filename.bak");
        /** @noinspection PhpConditionAlreadyCheckedInspection faux positif */
        if (!$rename || $fileExists || !$bakExists) {
            trigger_error(__("Echec lors du renommage"));
            return false;
        }
        $copy = $this->copy("$dirname/$filename.bak", "$dirname/$filename");
        $fileExists = $this->fileExists("$dirname/$filename");
        if (!$copy || !$fileExists) {
            trigger_error(__("Echec lors de la copie"));
            return false;
        }
        $this->fileDelete("$dirname/$filename.bak");
        ob_start();
        try {
            $this->readfile("$dirname/$filename");
        } finally {
            $content = ob_get_clean();
        }
        if ($content !== $filecontent) {
            trigger_error(__("Echec lors du readfile"));
            return false;
        }
        /** @noinspection PhpRedundantOptionalArgumentInspection */
        if ($this->hash("$dirname/$filename", 'sha256') !== $hash) {
            trigger_error(__("Echec lors du hash"));
            return false;
        }
        $ls = $this->ls($dirname);
        $in = false;
        foreach ($ls as $file) {
            if (mb_strpos($file, $filename) !== false) {
                $in = true;
                break;
            }
        }
        if (!$in) {
            trigger_error(__("Echec lors du ls"));
            return false;
        }
        $metadata = $this->metadata("$dirname/$filename");
        if (!$metadata || !isset($metadata['size']) || !$metadata['size']) {
            trigger_error(__("Echec lors du metadata"));
            return false;
        }
        $streamTo = $this->streamTo(
            "$dirname/$filename",
            $this,
            "$dirname/$filename.bak"
        );
        $bakExists = $this->fileExists("$dirname/$filename.bak");
        if (!$streamTo || !$bakExists) {
            trigger_error(__("Echec lors du streamTo"));
            return false;
        }

        $this->fileDelete("$dirname/$filename");
        $this->fileDelete("$dirname/$filename.bak");

        file_put_contents($tmpFile, $filecontent);
        $handle = fopen($tmpFile, 'r');
        try {
            $streamUpload = $this->streamUpload($handle, "$dirname/$filename");
            $fileExists = $this->fileExists("$dirname/$filename");
            if (!$streamUpload || !$fileExists) {
                trigger_error(__("Echec lors du streamUpload"));
                return false;
            }
        } finally {
            fclose($handle);
            unlink($tmpFile);
        }

        $this->fileDelete("$dirname/$filename");

        // juste pour trigger une exception
        $this->isEmpty();

        return true;
    }

    /**
     * Supprime les fichiers de test
     */
    private function deleteTestFiles()
    {
        $filename = $this->testFilename;
        $dirname = $this->testDirname;
        try {
            $possibleFilenames = [
                $filename,
                "$dirname/$filename",
                "$dirname/$filename.bak",
            ];
            foreach ($possibleFilenames as $f) {
                if ($this->fileExists($f)) {
                    $this->fileDelete($f);
                }
            }
        } catch (Exception $e) {
            trigger_error($e->getMessage());
        }
    }
}
