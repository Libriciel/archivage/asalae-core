<?php
/**
 * AsalaeCore\Driver\Volume\Exception\VolumeException
 */

namespace AsalaeCore\Driver\Volume\Exception;

use Cake\Datasource\EntityInterface;
use Exception;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

/**
 * Exception lancées par les volumes
 *
 * @category Driver/Volume/Exception
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018 Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class VolumeException extends Exception
{
    const VOLUME_NOT_ACCESSIBLE = 1;
    const FILE_NOT_FOUND = 2;
    const FILE_ALREADY_EXISTS = 3;
    const FILE_WRITE_ERROR = 4;
    const FILE_READ_ERROR = 5;
    const LOCAL_FILE_NOT_ACCESSIBLE = 6;
    const LOCAL_FILE_WRITE_ERROR = 7;
    const FILE_DELETE_ERROR = 8;
    const FULL_DISK = 9;

    /**
     * @var int code d'erreur du volume
     */
    public $volumeErrorCode;

    /**
     * VolumeException constructor.
     * @param string         $message  ou un message predéfini dans les constantes
     *                                 de class
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        $this->volumeErrorCode = $message;
        switch ($message) {
            case self::VOLUME_NOT_ACCESSIBLE:
                $message = __("Le volume de stockage n'existe pas ou n'est pas accessible");
                $code = Response::HTTP_BAD_REQUEST;
                break;
            // Il est important que ce soit l'unique erreur 404 de cette classe (utilisé dans VolumeManager)
            case self::FILE_NOT_FOUND:
                $message = __("Le fichier n'a pas été trouvé sur le volume");
                $code = Response::HTTP_NOT_FOUND;
                break;
            case self::FILE_ALREADY_EXISTS:
                $message = __("Le fichier de destination existe déjà sur le volume de stockage");
                $code = Response::HTTP_CONFLICT;
                break;
            case self::FILE_WRITE_ERROR:
                $message = __("L'écriture du fichier de destination ne s'est pas correctement effectuée");
                $code = Response::HTTP_FORBIDDEN;
                break;
            case self::FILE_READ_ERROR:
                $message = __("La lecture du fichier ne s'est pas correctement effectuée");
                $code = Response::HTTP_FORBIDDEN;
                break;
            case self::LOCAL_FILE_NOT_ACCESSIBLE:
                $message = __("Le fichier source n'existe pas ou n'est pas accessible");
                $code = Response::HTTP_NOT_FOUND;
                break;
            case self::LOCAL_FILE_WRITE_ERROR:
                $message = __("L'écriture du fichier ne s'est pas correctement effectuée");
                $code = Response::HTTP_FORBIDDEN;
                break;
            case self::FILE_DELETE_ERROR:
                $message = __("La suppression du fichier ne s'est pas correctement effectuée.");
                $code = Response::HTTP_FORBIDDEN;
                break;
            case self::FULL_DISK:
                $message = __("L'écriture ne s'est pas effectuée car la taille du fichier excède la taille restante.");
                $code = Response::HTTP_INTERNAL_SERVER_ERROR;
                break;
        }
        parent::__construct($message, $code, $previous);
    }

    /**
     * @var string
     */
    private $originalMessage;

    /**
     * permet d'ajouter des informations utiles au message d'erreur
     * @param EntityInterface $volume
     * @param string|null     $destinationFilename
     * @return VolumeException
     */
    public function wrap(EntityInterface $volume, string $destinationFilename = null): self
    {
        if (empty($this->originalMessage)) {
            $this->originalMessage = $this->message;
        }
        $file = $destinationFilename
            ? sprintf(' file: "%s"', h($destinationFilename))
            : '';
        $this->message = sprintf(
            'volume: "%s"(id=%d)%s - %s',
            h($volume->get('name')),
            $volume->get('id'),
            $file,
            $this->originalMessage
        );
        return $this;
    }
}
