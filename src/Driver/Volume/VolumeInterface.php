<?php
/**
 * AsalaeCore\Driver\Volume\VolumeInterface
 */

namespace AsalaeCore\Driver\Volume;

use AsalaeCore\Driver\Volume\Exception\VolumeException;
use InvalidArgumentException;

/**
 * Interface des drivers de volumes
 *
 * @category Driver/Volume
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
interface VolumeInterface
{
    /**
     * Constructeur de la classe du driver
     * @param array $driverSettings tableau associatif clé-valeur du driver
     * @throws InvalidArgumentException Erreur sur le paramétrage du driver
     * @throws \AsalaeCore\Driver\Volume\Exception\VolumeException VOLUME_NOT_ACCESSIBLE
     *                          Le volume de stockage n'existe pas
     *                          ou n'est pas accessible
     */
    public function __construct(array $driverSettings);

    /**
     * Teste la disponibilité et l'accessibilité d'un volume de stockage.
     * @return bool true si le volume est disponible et accessible,
     *              false dans le cas contraire
     * @throws VolumeException
     */
    public function ping(): bool;

    /**
     * Retourne true si le volume de stockage est vide et false dans le cas contraire
     * @return bool true si le volume est vide,
     *              false dans le cas contraire
     */
    public function isEmpty(): bool;

    /**
     * Ecrit le contenu d'un fichier dans un fichier de destination
     * sur un volume de stockage. Le nom du fichier de destination doit
     * être unique et peut comporter une arborescence de dossiers et sous dossiers.
     * L'arborescence utilise le caractère / comme le système de fichier linux.
     * @param string $destinationFilename
     * @param string $content
     * @return string référence de stockage du fichier sur le volume de stockage.
     * @throws VolumeException
     *      FILE_ALREADY_EXISTS
     *          Le fichier de destination existe déjà sur le volume de stockage
     *      FILE_WRITE_ERROR
     *          L'écriture du fichier de destination ne s'est pas correctement effectuée
     */
    public function filePutContent(string $destinationFilename, string $content): string;

    /**
     * Retourne le contenu d'un fichier sur un volume de stockage
     * en fournissant sa référence de stockage.
     * @param string      $storageReference
     *      Référence de stockage du fichier sur le volume.
     *      Cette référence a été retournée par les fonctions d'écriture
     *      des fichiers sur le volume.
     * @param string      $hashAlgo
     * @param string|null $hash
     * @return string binaire du fichier
     * @throws VolumeException FILE_NOT_FOUND
     *          Le fichier n'existe pas ou n'est pas accessible
     *      FILE_READ_ERROR
     *          La lecture du fichier ne s'est pas correctement effectuée
     */
    public function fileGetContent(
        string $storageReference,
        string $hashAlgo = 'crc32',
        string $hash = null
    ): string;

    /**
     * Ecrit un fichier présent sur le FileSystem
     * dans un fichier de destination sur un volume de stockage.
     * Le nom du fichier de destination doit être unique et peut comporter une
     * arborescence de dossiers et sous dossiers.
     * L'arborescence utilise le caractère / comme le système de fichier linux.
     * @param string $sourceFileUri
     *      Chemin absolu du fichier à copier sur le volume de stockage.
     * @param string $destinationFilename
     *      Nom du fichier de destination comportant une arborescence relative
     *      (ex: /ARC001/publications/offre_01.odt).
     *      Le fichier de destination ne doit pas exister.
     * @return string référence de stockage du fichier sur le volume de stockage.
     * @throws VolumeException
     *      FILE_ALREADY_EXISTS
     *          Le fichier de destination existe déjà sur le volume de stockage
     *      FILE_WRITE_ERROR
     *          L'écriture du fichier de destination ne s'est pas correctement effectuée
     *      LOCAL_FILE_NOT_ACCESSIBLE
     *          Le fichier source n'existe pas ou n'est pas accessible
     */
    public function fileUpload(string $sourceFileUri, string $destinationFilename): string;

    /**
     * Ecrit sur le File System un fichier stocké sur un volume de stockage
     * en fournissant sa référence de stockage.
     * @param string      $storageReference
     *      Référence de stockage du fichier sur le volume.
     *      Cette référence a été retournée par les fonctions d'écriture des fichiers sur le volume.
     * @param string      $destinationFileUri
     *      Chemin absolu sur le File System du fichier de destination dans
     *      lequel sera copié le fichier présent sur le volume de stockage.
     * @param string      $hashAlgo
     * @param string|null $hash
     * @throws VolumeException FILE_NOT_FOUND
     *          Le fichier n'est pas présent ou n'est pas accessible.
     *      LOCAL_FILE_WRITE_ERROR
     *          L'écriture du fichier ne s'est pas correctement effectuée
     */
    public function fileDownload(
        string $storageReference,
        string $destinationFileUri,
        string $hashAlgo = 'crc32',
        string $hash = null
    );

    /**
     * Supprime un fichier sur un volume de stockage en fournissant sa
     * référence de stockage. Dans le cas d'un volume de type File System,
     * il faut utiliser la commande shred pour supprimer les fichiers.
     * @param string $storageReference
     *      Référence de stockage du fichier sur le volume.
     *      Cette référence a été retournée par les fonctions d'écriture des fichiers sur le volume.
     * @throws VolumeException
     *      FILE_NOT_FOUND
     *          Le fichier n'est pas présent ou n'est pas accessible.
     *      FILE_DELETE_ERROR
     *          La suppression du fichier ne s'est pas correctement effectuée.
     */
    public function fileDelete(string $storageReference);

    /**
     * Teste la présence d'un fichier sur un volume de stockage
     * en fournissant sa référence de stockage
     * @param string $storageReference
     *      Référence de stockage du fichier sur le volume.
     *      Cette référence a été retournée par les fonctions d'écriture des fichiers sur le volume.
     * @return bool true si le fichier est présent, false dans le cas contraire
     * @throws VolumeException
     */
    public function fileExists(string $storageReference): bool;

    /**
     * Teste la présence d'un dossier sur un volume de stockage
     * en fournissant sa référence de stockage
     * @param string $storageReference
     * @return bool true si le dossier est présent, false dans le cas contraire
     * @throws VolumeException
     */
    public function dirExists(string $storageReference): bool;

    /**
     * Teste d'écriture, de lecture et de suppression d'un fichier sur le volume
     * @return array
     */
    public function test(): array;

    /**
     * Permet de renommer une référence
     * @param string $storageReference
     * @param string $newReference
     * @return string $newReference
     * @throws VolumeException
     */
    public function rename(string $storageReference, string $newReference): string;

    /**
     * Permet de copier une référence
     * @param string $storageReference
     * @param string $newReference
     * @return string $newReference
     * @throws VolumeException
     */
    public function copy(string $storageReference, string $newReference): string;

    /**
     * Renvoi le contenu de fichier dans la sorti standard
     * @param string      $storageReference
     * @param string      $hashAlgo
     * @param string|null $hash
     * @return bool comparaison avec le hash fourni
     * @throws VolumeException
     */
    public function readfile(
        string $storageReference,
        string $hashAlgo = 'crc32',
        string $hash = null
    ): bool;

    /**
     * Donne le hash d'un fichier
     * @param string $storageReference
     * @param string $algo
     * @return string
     * @throws VolumeException
     */
    public function hash(string $storageReference, string $algo = 'sha256'): string;

    /**
     * Donne la liste des dossiers / fichiers dans $storageReference
     * @param string $storageReference
     * @param bool   $allFiles
     * @return array
     * @throws VolumeException
     */
    public function ls(string $storageReference = '', bool $allFiles = false): array;

    /**
     * Donne les metadonnées d'un fichier
     * @param string $storageReference
     * @return array
     * @throws VolumeException
     */
    public function metadata(string $storageReference): array;

    /**
     * Envoi un fichier à un autre volume sans avoir à stocker le fichier
     * @param string          $storageReference
     * @param VolumeInterface $target
     * @param string|null     $targetReference
     * @return string         $newReference
     * @throws VolumeException
     */
    public function streamTo(
        string $storageReference,
        VolumeInterface $target,
        string $targetReference = null
    ): string;

    /**
     * Envoi un fichier en streaming
     * @param resource $handle
     * @param string   $targetReference
     * @return string  $newReference
     * @throws VolumeException
     */
    public function streamUpload($handle, string $targetReference): string;
}
