<?php
/**
 * AsalaeCore\Driver\Timestamping\TimestampingGeneric
 */

namespace AsalaeCore\Driver\Timestamping;

use Cake\Http\Client;
use Exception;
use InvalidArgumentException;
use TrustedTimestamps\TrustedTimestamps;

/**
 * Horodatage Générique
 *
 * @category Driver/Timestamping
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class TimestampingGeneric implements TimestampingInterface
{
    /**
     * @var Client HTTP
     */
    public $client;

    /**
     * @var string|TrustedTimestamps vendor lib
     */
    public $trustedTimestampClass = TrustedTimestamps::class;

    /**
     * @var string url du serveur d'horodatage
     */
    protected $tsaUrl;

    /**
     * @var string format de la date à parser
     */
    protected $timestampFormat;

    /**
     * @var string [user]:[password] du Timestamp Authority (TSA)
     */
    protected $tsaPass;

    /**
     * @var bool utiliser le proxy
     */
    private $useProxy;

    /**
     * @var string host du proxy
     */
    protected $proxyHost;

    /**
     * @var int port du proxy
     */
    protected $proxyPort;

    /**
     * @var string login:mdp du proxy
     */
    protected $proxyPass;
    /**
     * @var string
     */
    private string $hashAlgo;

    /**
     * TimestampingOpensign constructor.
     * @param string $host
     * @param string $timestampFormat
     * @param string $hash_algo
     * @param string $tsaLogin
     * @param string $tsaPassword
     * @param string $useProxy
     * @param string $proxyHost
     * @param string $proxyPort
     * @param string $proxyPass
     */
    public function __construct(
        string $host,
        string $timestampFormat = '',
        string $hash_algo = '',
        string $tsaLogin = '',
        string $tsaPassword = '',
        string $useProxy = '',
        string $proxyHost = '',
        string $proxyPort = '',
        string $proxyPass = ''
    ) {
        $this->tsaUrl = $host;
        $this->timestampFormat = $timestampFormat;
        $this->hashAlgo = $hash_algo ?: 'sha1';
        $this->tsaPass = $tsaLogin ? $tsaLogin.':'.$tsaPassword : '';
        $this->useProxy = (bool)$useProxy;
        $this->proxyHost = $proxyHost;
        $this->proxyPort = $proxyPort;
        $this->proxyPass = $proxyPass;
        $this->client = new Client;
    }

    /**
     * Vérifie que le service est accéssible (ne consomme pas de jeton)
     * @return bool
     */
    public function ping(): bool
    {
        $response = $this->client->head($this->tsaUrl, [], ['curl' => $this->getCurlOptions()]);
        return $response->getStatusCode() !== 404;
    }

    /**
     * Test la connexion au service
     * @return bool
     * @throws Exception
     */
    public function check(): bool
    {
        $file = $this->trustedTimestampClass::createTempFile(
            "asalae : fichier temporaire de test d'horodatage"
        );
        $token = $this->generateToken($file);
        unlink($file);
        return !empty($token);
    }

    /**
     * Donne le sha1 d'un fichier, qu'il soit sur disque ou en streaming
     * @param string $file
     * @param string $hash_algo
     * @return string
     */
    protected function getFileHash($file, $hash_algo): string
    {
        if (is_resource($file)) {
            rewind($file);
            $hash = hash($hash_algo, stream_get_contents($file));
        } elseif (is_string($file) && is_readable($file)) {
            $hash = hash_file($hash_algo, $file);
        } else {
            throw new InvalidArgumentException();
        }
        return $hash;
    }

    /**
     * Genère un token à partir d'un fichier
     * @param resource|string $file
     * @return string binaire du token
     * @throws Exception
     */
    public function generateToken($file): string
    {
        $hash = $this->getFileHash($file, $this->hashAlgo);
        $requestFile = $this->trustedTimestampClass::createRequestfile(
            $hash,
            $this->hashAlgo
        );

        $opts = $this->getCurlOptions();
        if (!empty($this->tsaPass)) {
            $opts[CURLOPT_USERPWD] = $this->tsaPass;
        }

        $token = $this->trustedTimestampClass::signRequestfile(
            $requestFile,
            $this->tsaUrl,
            $opts,
            $this->timestampFormat
        );
        return base64_decode($token['response_string']);
    }

    /**
     * Donne les options CURL
     * @return array
     */
    private function getCurlOptions(): array
    {
        if ($this->useProxy) {
            return array_filter(
                [
                    CURLOPT_PROXY => $this->proxyHost,
                    CURLOPT_PROXYPORT => $this->proxyPort,
                    CURLOPT_PROXYUSERPWD => $this->proxyPass,
                ]
            );
        }
        return [];
    }
}
