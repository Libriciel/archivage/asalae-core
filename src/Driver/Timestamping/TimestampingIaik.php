<?php
/**
 * AsalaeCore\Driver\Timestamping\TimestampingIaik
 */

namespace AsalaeCore\Driver\Timestamping;

/**
 * Horodatage IAIK
 *
 * @category Driver/Timestamping
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class TimestampingIaik extends TimestampingGeneric implements TimestampingInterface
{
    /**
     * TimestampingOpensign constructor.
     * @param string $host
     * @param string $timestampFormat
     * @param string $useProxy
     * @param string $proxyHost
     * @param string $proxyPort
     * @param string $proxyPass
     */
    public function __construct(
        string $host,
        string $timestampFormat = 'M d H:i:s.u Y T',
        string $useProxy = '',
        string $proxyHost = '',
        string $proxyPort = '',
        string $proxyPass = ''
    ) {
        parent::__construct(
            $host,
            $timestampFormat,
            'sha1',
            '',
            '',
            $useProxy,
            $proxyHost,
            $proxyPort,
            $proxyPass
        );
    }
}
