<?php
/**
 * AsalaeCore\Driver\Timestamping\TimestampingOpensign
 */

namespace AsalaeCore\Driver\Timestamping;

use AsalaeCore\Utility\SoapClient;
use Exception;
use InvalidArgumentException;
use SoapClient as PhpSoapClient;
use SoapFault;

/**
 * Horodatage par Opensign
 *
 * @category Driver
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class TimestampingOpensign implements TimestampingInterface
{
    /**
     * @var string|SoapClient
     */
    public $soapClient = SoapClient::class;

    /**
     * @var string host du serveur d'horodatage OpenSign
     */
    protected $host;

    /**
     * @var bool utiliser le proxy
     */
    private $useProxy;

    /**
     * @var string host du proxy
     */
    private $proxyHost;

    /**
     * @var int port du proxy
     */
    private $proxyPort;

    /**
     * @var string login:mdp du proxy
     */
    private $proxyPass;

    /**
     * TimestampingOpensign constructor.
     * @param string $host
     * @param string $useProxy
     * @param string $proxyHost
     * @param string $proxyPort
     * @param string $proxyPass
     */
    public function __construct(
        string $host,
        string $useProxy = '',
        string $proxyHost = '',
        string $proxyPort = '',
        string $proxyPass = ''
    ) {
        $this->host = $host;
        $this->useProxy = (bool)$useProxy;
        $this->proxyHost = $proxyHost;
        $this->proxyPort = $proxyPort;
        $this->proxyPass = $proxyPass;
    }

    /**
     * Vérifie que le service est accéssible (ne consomme pas de jeton)
     * @return bool
     * @throws SoapFault
     */
    public function ping(): bool
    {
        return $this->check();
    }

    /**
     * Test la connexion au service
     * @return bool
     * @throws SoapFault
     */
    public function check(): bool
    {
        $client = $this->getSoapClient(['wsdl_cache' => 0, 'trace' => 1]);
        return !empty($client->__soapCall('getOpensslVersion', []));
    }

    /**
     * Complète si besoin $params
     * @param array $params
     * @return array $params
     */
    private function getSoapParams(array $params = []): array
    {
        if ($this->useProxy) {
            if ($this->proxyHost) {
                $params += ['proxy_host' => $this->proxyHost];
            }
            if ($this->proxyPort) {
                $params += ['proxy_port' => $this->proxyPort];
            }
            if ($this->proxyPass) {
                [$proxy_login, $proxy_password] = explode(':', $this->proxyPass);
                $params += compact('proxy_login', 'proxy_password');
            }
        }
        return $params;
    }

    /**
     * Donne une nouvelle instance de SoapClient
     * @param array $params
     * @return PhpSoapClient
     */
    public function getSoapClient(array $params = []): PhpSoapClient
    {
        return new $this->soapClient($this->host, $this->getSoapParams($params));
    }

    /**
     * Donne le sha1 d'un fichier, qu'il soit sur disque ou en streaming
     * @param string $file
     * @return string
     */
    private function getFileSha1($file): string
    {
        if (is_resource($file)) {
            rewind($file);
            $sha1 = sha1(stream_get_contents($file)); // NOSONAR
        } elseif (is_string($file) && is_readable($file)) {
            $sha1 = sha1_file($file);
        } else {
            throw new InvalidArgumentException();
        }
        return $sha1;
    }

    /**
     * Genère un token à partir d'un fichier
     * @param resource|string $file
     * @return string binaire du token
     * @throws Exception
     */
    public function generateToken($file): string
    {
        $params = $this->getSoapParams();
        $client = $this->getSoapClient($params);

        $args = ['parameters' => ['sha1' => $this->getFileSha1($file)]];
        $request = $client->__soapCall("createRequest", $args);
        $args = ['parameters' => ['request' => $request]];
        $response = $client->__soapCall("createResponse", $args);
        $args = ['parameter' => ['response' => $response]];
        $token = $client->__soapCall("extractToken", $args);
        if (!$token || $token === 'KO') {
            throw new Exception('Unable de generate token');
        }
        return base64_decode($token);
    }
}
