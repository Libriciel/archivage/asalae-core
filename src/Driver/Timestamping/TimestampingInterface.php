<?php
/**
 * AsalaeCore\Driver\Timestamping\TimestampingInterface
 */

namespace AsalaeCore\Driver\Timestamping;

use Exception;

/**
 * Interface des drivers d'horodatage
 *
 * @category Driver
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
interface TimestampingInterface
{
    /**
     * Vérifie que le service est accéssible (ne consomme pas de jeton)
     * @return bool
     */
    public function ping(): bool;

    /**
     * Test la connexion au service (peut consommer des jetons)
     * @return bool
     */
    public function check(): bool;

    /**
     * Genère un token à partir d'un fichier
     * @param resource|string $file
     * @return string
     * @throws Exception
     */
    public function generateToken($file): string;
}
