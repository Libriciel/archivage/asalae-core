<?php
/**
 * AsalaeCore\Driver\Timestamping\TimestampingLocal
 */

namespace AsalaeCore\Driver\Timestamping;

use AsalaeCore\Utility\Timestamper;
use Exception;
use InvalidArgumentException;

/**
 * Horodatage en local (openssl)
 *
 * @category Driver
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2018, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class TimestampingLocal implements TimestampingInterface
{
    /**
     * @var string
     */
    private $ca;
    /**
     * @var string
     */
    private $crt;
    /**
     * @var string
     */
    private $pem;
    /**
     * @var string fichier de configuration openssl
     */
    private $cnf;
    /**
     * @var string[] liste des fichiers temporaires
     */
    private $tempnames = [];

    /**
     * TimestampingLocal constructor.
     * @param string $ca
     * @param string $crt
     * @param string $pem
     * @param string $cnf fichier de configuration openssl
     */
    public function __construct(
        string $ca,
        string $crt,
        string $pem,
        string $cnf
    ) {
        $this->ca = $ca;
        $this->crt = $crt;
        $this->pem = $pem;
        $this->cnf = $cnf;
    }

    /**
     * Vérifie que le service est accéssible (ne consomme pas de jeton)
     * @return bool
     */
    public function ping(): bool
    {
        exec('openssl version', $output, $code);
        return $code === 0;
    }

    /**
     * Test la connexion au service (peut consommer des jetons)
     * @return bool
     * @throws Exception
     */
    public function check(): bool
    {
        $sha1 = sha1(uniqid('check', true)); // NOSONAR
        try {
            $requestFile = $this->getRequestFile($sha1);
            $responseFile = $this->getResponseFile($requestFile);
            $tsrFile = $this->getTokenFile($responseFile);
            $content = file_get_contents($tsrFile);
        } catch (Exception $e) {
        }
        if (isset($requestFile) && is_file($requestFile)) {
            unlink($requestFile);
        }
        if (isset($responseFile) && is_file($responseFile)) {
            unlink($responseFile);
        }
        if (isset($tsrFile) && is_file($tsrFile)) {
            unlink($tsrFile);
        }
        if (!empty($e)) {
            throw $e;
        }
        return !empty($content);
    }

    /**
     * Genère un token à partir d'un fichier
     * @param resource|string $file
     * @return string
     * @throws Exception
     */
    public function generateToken($file): string
    {
        $sha1 = $this->getFileSha1($file);
        $requestFile = $this->getRequestFile($sha1);
        $responseFile = $this->getResponseFile($requestFile);
        return file_get_contents($responseFile);
    }

    /**
     * Lance une commande, envoi une exception avec le message d'erreur en cas de fail
     * @param string $cmd
     * @throws Exception
     */
    private function exec(string $cmd)
    {
        exec($cmd." 2>&1 >/dev/null", $output, $code);
        if ($code !== 0) {
            throw new Exception('Unable to use openssl: '.PHP_EOL.implode(PHP_EOL, $output));
        }
    }

    /**
     * Génère le fichier de requete
     * @param string $hash
     * @return string
     * @throws Exception
     */
    private function getRequestFile(string $hash): string
    {
        $filename = tempnam(sys_get_temp_dir(), 'timestamping-');
        $cmd = "openssl ts -query -sha1 -digest ".escapeshellarg($hash)
            . " -cert -out ".escapeshellarg($filename);
        $this->exec($cmd);
        $this->tempnames[] = $filename;
        return $filename;
    }

    /**
     * Génère le fichier de réponse
     * @param string $requestFile
     * @return string
     * @throws Exception
     */
    private function getResponseFile(string $requestFile): string
    {
        $filename = tempnam(sys_get_temp_dir(), 'timestamping-');
        $cmd = "openssl ts -reply -queryfile ".escapeshellarg($requestFile)
            ." -signer ".escapeshellarg($this->crt)
            ." -inkey ".escapeshellarg($this->pem)
            ." -config ".escapeshellarg($this->cnf)
            ." -out ".escapeshellarg($filename);
        $this->exec($cmd);
        $this->tempnames[] = $filename;
        return $filename;
    }

    /**
     * Génère le fichier token
     * @param string $responseFile
     * @return string
     * @throws Exception
     */
    private function getTokenFile(string $responseFile): string
    {
        $filename = tempnam(sys_get_temp_dir(), 'timestamping-');
        $cmd = "openssl ts -reply -in ".escapeshellarg($responseFile)
            ." -out ".escapeshellarg($filename)
            ." -token_out";
        $this->exec($cmd);
        $this->tempnames[] = $filename;
        return $filename;
    }

    /**
     * Donne le sha1 d'un fichier, qu'il soit sur disque ou en streaming
     * @param string $file
     * @return string
     */
    private function getFileSha1($file): string
    {
        if (is_resource($file)) {
            rewind($file);
            $sha1 = sha1(stream_get_contents($file)); // NOSONAR
        } elseif (is_string($file) && is_readable($file)) {
            $sha1 = sha1_file($file);
        } else {
            throw new InvalidArgumentException();
        }
        return $sha1;
    }

    /**
     * Vérifi qu'un token a été produit par ce serveur
     * @param string $file
     * @param string $tsr
     * @return bool
     * @throws Exception
     */
    public function verifyTsr(string $file, string $tsr): bool
    {
        return Timestamper::verifyTsr($file, $tsr, $this->ca);
    }

    /**
     * Destructeur de classe
     */
    public function __destruct()
    {
        foreach ($this->tempnames as $filename) {
            if (is_writable($filename)) {
                unlink($filename);
            }
        }
    }
}
