<?php
/**
 * AsalaeCore\Command\HashDirCommand
 */

namespace AsalaeCore\Command;

use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Http\Exception\NotFoundException;
use Exception;

/**
 * Donne le hash des fichiers d'un répertoire
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2019, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class HashDirCommand extends Command
{
    /**
     * @var Arguments
     */
    public $args;
    /**
     * @var ConsoleIo
     */
    public $io;
    /**
     * @var resource handler du fichier de sorti
     */
    private $outputFile;
    /**
     * @var bool garde en mémoire le résultat si true
     */
    public $noOutput = false;
    /**
     * @var string si noOutput, la sorti est redirigé dans cet attribut;
     */
    public $out = '';

    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'hash_dir';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = new ConsoleOptionParser;
        $parser->addSubcommand(
            'check',
            [
                'help' => __("Vérifie le hash d'un fichier d'export"),
            ]
        );
        $parser->addSubcommand(
            'compare',
            [
                'help' => __("Compare l'état actuel par rapport à un fichier"),
            ]
        );
        $parser->addOption(
            'recursive',
            [
                'short' => 'r',
                'help' => __("Liste les fichiers de façon récursive"),
                'boolean' => true,
            ]
        );
        $parser->addOption(
            'filter',
            [
                'help' => __("Filtres le nom de fichiers correspondant (cumulable)").PHP_EOL
                    .'exemple: bin/cake hash-dir path/to/my/dir --filter *.foo --filter Bar*',
                'multiple' => true,
            ]
        );
        $parser->addOption(
            'hash-algo',
            [
                'help' => __("Algorithme de hashage utilisé"),
                'default' => 'md5',
                'choices' => hash_algos(),
            ]
        );
        $parser->addOption(
            'basedir',
            [
                'help' => __("Dossier racine"),
                'default' => getcwd(),
            ]
        );
        $parser->addOption(
            'output',
            [
                'short' => 'o',
                'help' => __("Fichier cible"),
            ]
        );
        $parser->setDescription(
            __("Donne une liste de fichiers avec leurs hashs").PHP_EOL
            ."example: bin/cake hash-dir -r src/ webroot/ --filter webroot/org-entity-data/*"
        );

        return $parser;
    }

    /**
     * Main command
     * @param Arguments $args
     * @param ConsoleIo $io
     * @return int|null
     * @throws Exception
     */
    public function execute(Arguments $args, ConsoleIo $io): ?int
    {
        $this->args = $args;
        $this->io = $io;
        if ($args->getOption('output')) {
            $outputFile = $args->getOption('basedir').DS.$args->getOption('output');
            if (!is_writable(dirname($outputFile))) {
                throw new Exception(dirname($outputFile). ' IS NOT WRITABLE');
            }
            $this->outputFile = fopen($outputFile, 'w');
        }
        $dirs = $args->getArguments();
        sort($dirs);
        $this->displayHeader($dirs);
        $trim = strlen($args->getOption('basedir').DS);
        foreach ($dirs as $dir) {
            $d = $args->getOption('basedir').DS.trim($dir, DS);
            if (is_file($d)) {
                $this->outputFile($d, $trim);
            } elseif (is_dir($d)) {
                $this->outputDir($d, $trim);
            } else {
                throw new NotFoundException('404 DIR NOT FOUND: {'.$d.'}');
            }
        }
        if ($this->outputFile) {
            fclose($this->outputFile);
            $outputFile = $args->getOption('basedir').DS.$args->getOption('output');
            if (is_file($outputFile)) {
                $md5sum = md5_file($outputFile);
                $md5 = hex2bin($md5sum);
                file_put_contents(
                    $outputFile . '.md5',
                    $md5
                );
                $io->out($md5sum);
            }
        }
        return self::CODE_SUCCESS;
    }

    /**
     * Entête du fichier
     * @param string[] $dirs
     */
    private function displayHeader(array $dirs)
    {
        $this->output('####################################');
        $str = $dirs;
        $params = [
            'recursive',
            'filter',
            'hash-algo',
        ];
        $args = $this->args->getArguments();
        $options = [];
        foreach ($params as $key) {
            $values = $this->args->getOption($key);
            $options[$key] = $values;
            if (is_array($values) && $values) {
                foreach ($values as $value) {
                    $str[] = '--'.$key.' '.addcslashes($value, ' *');
                }
            } elseif (is_string($values)) {
                $str[] = '--'.$key.' '.addcslashes($values, ' *');
            } elseif ($values === true) {
                $str[] = '--'.$key;
            }
        }
        $pass = [
            'args' => $args,
            'options' => $options,
        ];
        rsort($params);
        $this->output(implode(' ', $str));
        $this->output(base64_encode(serialize($pass)));
        $this->output('####################################', 2);
    }

    /**
     * Affichage conditionnel de la sortie
     * @param string $out
     * @param int    $nl
     */
    private function output(string $out, int $nl = 1)
    {
        if ($this->noOutput) {
            $this->out .= $out . str_repeat(PHP_EOL, $nl);
        } elseif ($this->args->getOption('output')) {
            fwrite($this->outputFile, $out . str_repeat(PHP_EOL, $nl));
        } else {
            $this->io->out($out, $nl);
        }
    }

    /**
     * Affiche le résultat, de façon récursive ou pas
     * @param string $dir
     * @param int    $ltrimAmount
     */
    private function outputDir(string $dir, int $ltrimAmount)
    {
        foreach (glob($dir.DS.'*') as $filename) {
            if (is_dir($filename)) {
                if ($this->args->getOption('recursive')) {
                    $this->outputDir($filename, $ltrimAmount);
                }
            } else {
                $this->outputFile($filename, $ltrimAmount);
            }
        }
    }

    /**
     * Affiche un fichier (fichier -> hash)
     * @param string $filename
     * @param int    $ltrimAmount
     */
    private function outputFile(string $filename, int $ltrimAmount)
    {
        $truncatedFilename = substr($filename, $ltrimAmount);
        $filter = false;
        foreach ($this->args->getOption('filter') ?: [] as $f) {
            if (fnmatch($f, $truncatedFilename)) {
                $filter = true;
                break;
            }
        }
        if (!$filter) {
            $this->output(
                $truncatedFilename
                .':'.hash_file($this->args->getOption('hash-algo'), $filename)
            );
        }
    }
}
