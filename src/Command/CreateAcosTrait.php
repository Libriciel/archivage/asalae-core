<?php
/**
 * AsalaeCore\Command\CreateAcosTrait
 */

namespace AsalaeCore\Command;

use AsalaeCore\Controller\ApiInterface;
use AsalaeCore\Model\Table\AcosTable;
use AsalaeCore\Model\Table\ArosAcosTable;
use AsalaeCore\Model\Table\ArosTable;
use Cake\Command\Command;
use Cake\Controller\Controller as CakeController;
use Cake\Core\App;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\Utility\Hash;
use Exception;

/**
 * Création et mise à jour des acos et permissions
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2018, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 * @property AcosTable $Acos
 * @property ArosTable $Aros
 * @property ArosAcosTable $Permissions
 * @mixin Command
 */
trait CreateAcosTrait
{
    /**
     * @var array options d'initialisation de table
     */
    private array $opts = [];

    /**
     * Création préliminaire des acos type controller
     *
     * @param array $opts options d'initialisation de table
     * @return EntityInterface alias controllers
     * @throws Exception
     */
    public function createControllerAcos(array $opts = []): EntityInterface
    {
        $this->opts = $opts;
        $this->Acos = $this->fetchTable('Acos', $opts);
        $rootdata = [
            'alias' => 'controllers',
            'model' => 'root',
            'parent_id' => $this->Acos->findOrCreate(['alias' => 'root'])->get('id'),
        ];
        $controllersRoot = $this->Acos->findOrCreate($rootdata);

        $controllers = json_decode(
            file_get_contents(Configure::read('App.paths.controllers_rules')),
            true
        );

        $this->createAcos($controllersRoot, $controllers);

        return $this->Acos->get($controllersRoot->id);
    }

    /**
     * Création de l'arbre d'api
     * @param array $opts options d'initialisation de table
     * @return EntityInterface Aco alias api
     * @throws Exception
     */
    public function createApiAcos(array $opts = []): EntityInterface
    {
        $this->opts = $opts;
        $rootdata = [
            'alias' => 'api',
            'model' => 'root',
            'parent_id' => $this->Acos->findOrCreate(['alias' => 'root'])->get('id'),
        ];
        $apiRoot = $this->Acos->findOrCreate($rootdata);
        $controllers = json_decode(
            file_get_contents(Configure::read('App.paths.apis_rules')),
            true
        );

        $this->createAcos($apiRoot, $controllers);

        return $this->Acos->get($apiRoot->id);
    }

    /**
     * Donne à tout le monde l'accès sur $aco
     *
     * @param EntityInterface $aco
     */
    private function allowForAll(EntityInterface $aco)
    {
        $this->Permissions = $this->fetchTable('ArosAcos', $this->opts);
        $this->Aros = $this->fetchTable('Aros', $this->opts);
        $aros = $this->Aros->find();
        /** @var EntityInterface $aro */
        foreach ($aros as $aro) {
            $entity = $this->Permissions->find()
                ->where(
                    $link = [
                        'aro_id' => $aro->get('id'),
                        'aco_id' => $aco->get('id'),
                    ]
                )
                ->first();
            if (empty($entity)) {
                $entity = $this->Permissions->newEntity($link);
            }
            $this->Permissions->patchEntity(
                $entity,
                [
                    '_create' => 1,
                    '_read' => 1,
                    '_update' => 1,
                    '_delete' => 1,
                ]
            );
            $this->Permissions->saveOrFail($entity);
        }
    }

    /**
     * Création des acos au sein d'un root (controllers/api)
     * @param EntityInterface $rootAco
     * @param array           $controllers
     * @return void
     * @throws Exception
     */
    private function createAcos(EntityInterface $rootAco, array $controllers)
    {
        $rootdata = [
            'alias' => $rootAco->get('alias'),
            'model' => $rootAco->get('model'),
            'parent_id' => $rootAco->get('parent_id'),
        ];
        $commeDroits = [];
        $isApi = $rootAco->get('alias') === 'api';
        foreach ($controllers as $model => $actions) {
            /** @var string|CakeController $classname */
            $classname = App::className($model, 'Controller', 'Controller');
            if (!class_exists($classname) || !is_subclass_of($classname, CakeController::class)) {
                continue;
            }
            if ($isApi && !is_subclass_of($classname, ApiInterface::class)) {
                continue;
            }
            $data = [
                'alias' => $model,
                'model' => $rootAco->get('alias'),
                'parent_id' => $rootAco->get('id'),
            ];
            $parent = $this->Acos->find()
                ->where($data)
                ->andWhere(
                    [
                        'lft >' => $rootAco->get('lft'),
                        'rght <' => $rootAco->get('rght'),
                    ]
                )
                ->first();
            if (empty($parent)) {
                $parent = $this->Acos->newEntity($data);
                $this->Acos->save($parent);
                $rootAco = $this->Acos->find()->where($rootdata)->firstOrFail();
            }
            if ($isApi) {
                $apiActions = Hash::normalize($classname::getApiActions());
                unset($apiActions['default']); // default === $parent
                $apiActions = array_keys($apiActions);
            }
            foreach ($actions as $action => $params) {
                if ($isApi && !in_array($action, $apiActions)) {
                    continue;
                }
                $data = [
                    'alias' => $action,
                    'model' => $model,
                ];
                $entity = $this->Acos->find()
                    ->where($data)
                    ->andWhere(
                        [
                            'lft >' => $rootAco->get('lft'),
                            'rght <' => $rootAco->get('rght'),
                        ]
                    )
                    ->first();

                if (!$entity) {
                    $entity = $this->Acos->newEntity($data + ['parent_id' => $parent->get('id')]);
                    $this->Acos->saveOrFail($entity);
                    if (!empty($params['accesParDefaut'])) {
                        $this->allowForAll($entity);
                    }
                    $rootAco = $this->Acos->find()->where($rootdata)->first();
                } elseif (empty($params['commeDroits']) && $entity->get('parent_id') !== $parent->get('id')) {
                    $entity->set('parent_id', $parent->get('id'));
                    $this->Acos->saveOrFail($entity);
                    $rootAco = $this->Acos->find()->where($rootdata)->first();
                }
                if (!empty($params['commeDroits'])) {
                    $commeDroits[] = [
                        'id' => $entity->get('id'),
                        'target' => $params['commeDroits']
                    ];
                }
            }
        }
        $this->doCommeDroit($rootAco, $commeDroits, $isApi);
    }

    /**
     * Fait le commeDroit
     * @param EntityInterface $rootAco
     * @param array           $commeDroits
     * @param bool            $isApi
     * @return void
     * @throws Exception
     */
    private function doCommeDroit(EntityInterface $rootAco, array $commeDroits, bool $isApi)
    {
        $rootdata = [
            'alias' => $rootAco->get('alias'),
            'model' => $rootAco->get('model'),
            'parent_id' => $rootAco->get('parent_id'),
        ];
        foreach ($commeDroits as $params) {
            $entity = $this->Acos->get($params['id']);
            [$model, $action] = explode('::', $params['target']);
            /** @var string|ApiInterface $classname */
            $classname = App::className($model, 'Controller', 'Controller');
            if (!class_exists($classname) || !is_subclass_of($classname, CakeController::class)) {
                continue;
            }
            if ($isApi) {
                if (!is_subclass_of($classname, ApiInterface::class)) {
                    continue;
                }
                if ($action === 'default') {
                    $action = $model;
                    $model = 'api';
                }
            }
            $target = $this->Acos->find()
                ->where(['alias' => $action, 'model' => $model])
                ->andWhere(
                    [
                        'lft >' => $rootAco->get('lft'),
                        'rght <' => $rootAco->get('rght'),
                    ]
                )
                ->first();
            if (empty($target)) {
                throw new Exception(
                    __(
                        "Erreur dans les comme droits: tentative d'affecter les droits de"
                        ." {0} sur {1}::{2}, mais {0} n'a pas été trouvé en base",
                        $params['target'],
                        $entity->get('model'),
                        $entity->get('alias')
                    )
                );
            }
            $entity->set('parent_id', $target->get('id'));
            $this->Acos->saveOrFail($entity);
            $rootAco = $this->Acos->find()->where($rootdata)->first();
        }
    }
}
