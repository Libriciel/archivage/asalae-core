<?php
/**
 * AsalaeCore\Command\CompletionCommand
 * @noinspection PhpDeprecationInspection - appel à Cake\Console\Shell pour retro-compatibilité
 */

namespace AsalaeCore\Command;

use Cake\Command\Command;
use Cake\Command\CompletionCommand as CakeCompletionCommand;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\Shell;
use Cake\Utility\Inflector;
use ReflectionClass;
use ReflectionException;

/**
 * Corrige la Command pour l'autocompletion des CommandShells en terminal
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2020, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class CompletionCommand extends CakeCompletionCommand
{
    /**
     * Get the list of defined sub-commands.
     *
     * @param Arguments $args The command arguments.
     * @param ConsoleIo $io   The console io
     * @return int
     * @throws ReflectionException
     */
    protected function getSubcommands(Arguments $args, ConsoleIo $io): int
    {
        $name = $args->getArgument('command');
        if ($name === null || !strlen($name)) {
            return static::CODE_SUCCESS;
        }

        $options = [];
        foreach ($this->commands as $key => $value) {
            $parts = explode(' ', $key);
            if ($parts[0] !== $name) {
                continue;
            }

            // Space separate command name, collect
            // hits as subcommands
            if (count($parts) > 1) {
                $options[] = implode(' ', array_slice($parts, 1));
                continue;
            }

            // Handle class strings
            if (is_string($value)) {
                $reflection = new ReflectionClass($value);
                $value = $reflection->newInstance();
            }
            if ($value instanceof Shell) {
                $shellCommands = $this->shellSubcommands($value);
                $options = array_merge($options, $shellCommands);
            }
            /**
             * Seul ce bloc à été ajouté dans cette fonction
             */
            if ($value instanceof Command) {
                $commandCommands = $this->commandSubcommands($value);
                $options = array_merge($options, $commandCommands);
            }
        }
        $options = array_unique($options);
        $io->out(implode(' ', $options));

        return static::CODE_SUCCESS;
    }

    /**
     * Reflect the subcommands names out of a shell.
     *
     * @param Command $command The shell to get commands for
     * @return string[] A list of commands
     */
    protected function commandSubcommands(Command $command): array
    {
        $command->initialize();

        $optionParser = @$command->getOptionParser();
        $subcommands = $optionParser->subcommands();

        $output = array_keys($subcommands);
        return array_unique($output);
    }

    /**
     * Get the options for a command or subcommand
     *
     * NOTE: complètement modifié pour les CommandShells
     *
     * @param \Cake\Console\Arguments $args The command arguments.
     * @param \Cake\Console\ConsoleIo $io   The console io
     * @return int
     * @throws ReflectionException
     */
    protected function getOptions(Arguments $args, ConsoleIo $io): ?int
    {
        $name = $args->getArgument('command');
        $subcommand = $args->getArgument('subcommand');

        $options = [];
        $parser = null;
        $commands = $this->commands->getIterator();
        if ($subcommand) {
            if (isset($commands["$name $subcommand"])) {
                $value = $commands["$name $subcommand"];
                if (is_string($value)) {
                    $reflection = new ReflectionClass($value);
                    $value = $reflection->newInstance();
                }
                $parser = $value->getOptionParser();
            } elseif (isset($commands[$name])) {
                $value = $commands[$name];
                if (is_string($value)) {
                    $reflection = new ReflectionClass($value);
                    $value = $reflection->newInstance();
                }
                if ($value instanceof Command) {
                    $parser = $value->getOptionParser();
                    $subcommand = Inflector::underscore($subcommand);
                    $subcommands = $parser->subcommands();
                    if ($subcommand && isset($subcommands[$subcommand])) {
                        $parser = $subcommands[$subcommand]->parser();
                    }
                }
            }
        } elseif (isset($commands[$name])) {
            $value = $commands[$name];
            if (is_string($value)) {
                $reflection = new ReflectionClass($value);
                $value = $reflection->newInstance();
            }
            $parser = $value->getOptionParser();
        }
        if ($parser) {
            foreach ($parser->options() as $name => $option) {
                $options[] = "--$name";
                $short = $option->short();
                if ($short) {
                    $options[] = "-$short";
                }
            }
        }
        $options = array_unique($options);
        $io->out(implode(' ', $options));

        return static::CODE_SUCCESS;
    }
}
