<?php
/**
 * AsalaeCore\Command\ApisCommand
 */

namespace AsalaeCore\Command;

use AsalaeCore\Controller\ApiInterface;
use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Core\App;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\Utility\Hash;
use Cake\Utility\Inflector;
use Exception;

/**
 * Permet la gestion des apis comme pour les controllers
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2021, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ApisCommand extends Command
{
    /**
     * Traits
     */
    use CreateAcosTrait;

    /**
     * @var Arguments
     */
    public $args;
    /**
     * @var ConsoleIo
     */
    public $io;

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = new ConsoleOptionParser();
        $parser->addSubcommand(
            'get',
            [
                'help' => __("Donne le paramétrage effectué pour une action"),
            ]
        );
        $parser->addSubcommand(
            'list',
            [
                'help' => __("Donne la liste des actions"),
            ]
        );

        $parser->addArgument(
            'target',
            [
                'help' => __("Action sous la forme Controller/action"),
            ]
        );
        $parser->addOption(
            'invisible',
            [
                'short' => 'i',
                'boolean' => true,
                'help' => __("Affiche l'action dans l'édition des permissions"),
            ]
        );
        $parser->addOption(
            'accesParDefaut',
            [
                'short' => 'a',
                'boolean' => true,
                'help' => __(
                    "Donne un accès à tous les aros déjà créés,"
                    ." et ajoutera un accès lors de la création d'un nouvel aro"
                ),
            ]
        );
        $parser->addOption(
            'commeDroits',
            [
                'short' => 'c',
                'help' => __(
                    "L'aco créé sera déclaré enfant de la cible défini ici. Syntaxe: NomDuController::nomDeLaction"
                ),
            ]
        );
        $parser->addOption(
            'plugin',
            [
                'help' => __("Indique le plugin du Controller cible"),
            ]
        );
        $parser->setDescription(
            [
                __("Permet d'ajouter une entrée dans apis.json")
            ]
        );
        $parser->addOption(
            'interactive',
            [
                'help' => __("Demandera toutes les options si à Yes"),
                'default' => 'y',
                'choice' => ['y', 'n']
            ]
        );
        return $parser;
    }

    /**
     * Ajoute/modifi les apis
     * @param Arguments $args
     * @param ConsoleIo $io
     * @return int|null
     * @throws Exception
     */
    public function execute(Arguments $args, ConsoleIo $io): ?int
    {
        $this->args = $args;
        $this->io = $io;
        $target = $args->getArgument('target');
        $this->Acos = $this->fetchTable('Acos');
        $root = $this->Acos->find()
            ->select(['id', 'lft', 'rght'])
            ->where(['model' => 'root', 'alias' => 'api'])
            ->first();
        if (!$root) {
            $io->abort(__("Les permissions n'ont pas été initialisés"));
        }

        $apis = $this->getApisActions();
        $pathToApisJson = Configure::read(
            'App.paths.apis_rules',
            RESOURCES . 'apis.json'
        );
        $data = json_decode(file_get_contents($pathToApisJson), true);

        if (!$target) {
            $target = $this->askForTarget($apis, $data);
        }
        $invisible = (string)(int)$args->getOption('invisible');
        $allowedByDefault = (string)(int)$args->getOption('accesParDefaut');
        $commeDroit = $args->getOption('commeDroits') ?: '';
        $group = null;
        if ($args->getOption('interactive') === 'y') {
            if (!$commeDroit) {
                $this->askForCommeDroit($apis, $data, $target);
            }
            if (!$commeDroit && $args->getOption('invisible') === false) {
                $invisible = $this->io->askChoice(
                    __("L'action est-elle visible dans les permissions ?"),
                    ['y', 'n'],
                    'y'
                ) === 'y' ? '0' : '1';
            }
            if (!$commeDroit && $args->getOption('accesParDefaut') === false) {
                $allowedByDefault = $this->io->askChoice(
                    __("L'action est-elle accessible par défaut ?"),
                    ['y', 'n'],
                    'n'
                ) === 'y' ? '1' : '0';
            }
            if (!$commeDroit && $invisible === '0') {
                $group = $this->askForGroup();
                if ($group === 'empty') {
                    unset($group);
                }
            }
        }

        if ($args->getOption('commeDroits')) {
            [$controller, $action] = explode('::', $args->getOption('commeDroits'));
            if (!$this->checkExistance($controller, $action)) {
                $this->io->abort(__("l'action du commeDroits n'a pas été trouvée"));
            }
        }
        while (empty($target)
            || !preg_match('/^([A-Za-z\d.-]+)\/([A-Za-z\d-]+)$/', $target, $match)
            || !$this->checkExistance($match[1], $match[2])
        ) {
            $this->io->out("La cible doit être indiquée sous la forme Controllers/actionEnCamelCase");
            $target = $this->io->ask(__("Veuillez saisir une cible"));
        }
        [, $controller, $action] = $match;
        $controller = Inflector::camelize($controller, '-');
        $action = Inflector::variable(Inflector::camelize($action, '-'));
        if (!isset($data[$controller])) {
            $data[$controller] = [];
        }
        $data[$controller][$action] = [
            'invisible' => $invisible,
            'accesParDefaut' => $allowedByDefault,
            'commeDroits' => $commeDroit,
        ];
        if (isset($group) && $group) {
            $data[$controller][$action]['group'] = $group;
        }
        ksort($data[$controller]);
        ksort($data);

        file_put_contents($pathToApisJson, json_encode($data, JSON_PRETTY_PRINT));

        $parent = $this->getParentAco($root, $controller);
        $aco = $this->Acos->findOrCreate(
            [
                'model' => $controller,
                'alias' => $action,
                'parent_id' => $parent->get('id')
            ]
        );

        if ($args->getOption('accesParDefaut')) {
            $this->allowForAll($aco);
        }

        $io->out('done');
        return self::CODE_SUCCESS;
    }

    /**
     * Donne les liste des [model => actions]
     * @return array
     */
    private function getApisActions(): array
    {
        $apis = [];
        $plugin = $this->args->getOption('plugin');
        $app = Configure::read('App.namespace');
        foreach (App::classPath('Controller', $plugin) as $basePath) {
            $controllers = glob($basePath . '*Controller.php');
            foreach ($controllers as $path) {
                /** @var string|ApiInterface $classname */
                $classname = ($plugin ?: $app) . '\\Controller\\' . basename($path, '.php');
                if (!class_exists($classname) || !is_subclass_of($classname, ApiInterface::class)) {
                    continue;
                }
                $model = basename($path, 'Controller.php');
                $apis[$model] = array_keys(Hash::normalize($classname::getApiActions()));
            }
        }
        return $apis;
    }

    /**
     * Vérifi l'existance d'une action
     * @param string $controller
     * @param string $action
     * @return bool
     */
    private function checkExistance(string $controller, string $action): bool
    {
        $plugin = $this->args->getOption('plugin');
        $plugin = $plugin ? $plugin.'.' : '';
        $controller = Inflector::camelize($controller, '-');
        $action = Inflector::variable(Inflector::camelize($action, '-'));
        /** @var ApiInterface $classname */
        $classname = App::className($plugin.$controller, 'Controller', 'Controller');
        if (!$classname) {
            return false;
        }
        $actions = Hash::normalize($classname::getApiActions());
        if (!array_key_exists($action, $actions)) {
            return false;
        }
        if (empty($actions[$action])) {
            $fn = $action === 'default' ? 'apiGetAction' : $action;
            return method_exists($classname, $fn);
        }
        foreach (['*', 'get', 'post', 'put', 'delete', 'patch', 'head'] as $method) {
            if (isset($actions[$action][$method]['function'])
                && !method_exists($classname, $actions[$action][$method]['function'])
            ) {
                return false;
            }
        }
        return true;
    }

    /**
     * Demande la cible, renvoi un controller/action
     * @param array $apis
     * @param array $data
     * @return string
     */
    private function askForTarget(array $apis, array $data): string
    {
        $missings = [];
        foreach ($apis as $controller => $actions) {
            foreach ($actions as $action) {
                if (!Hash::get($data, $controller.'.'.$action)) {
                    $missings[$controller][$action] = true;
                }
            }
        }
        if (!$missings) {
            $this->io->warning(__("Toutes les actions ont été définies"));
            $defaultController = null;
        } else {
            $defaultController = current(array_keys($missings));
        }
        $controller = $this->io->askChoice(
            __("Sélectionner un Controller"),
            array_keys($apis),
            $defaultController
        );
        if (empty($missings[$controller])) {
            $this->io->warning(__("Toutes les actions ont été définies"));
            $defaultAction = null;
        } else {
            $defaultAction = current(array_keys($missings[$controller]));
        }
        $action = $this->io->askChoice(
            __("Sélectionner une action"),
            $apis[$controller],
            $defaultAction
        );
        return $controller.'/'.$action;
    }

    /**
     * Défini le commeDroit
     * @param array  $apis
     * @param array  $data
     * @param string $target
     * @return string
     */
    private function askForCommeDroit(array $apis, array $data, string $target): string
    {
        $commeDroit = $this->io->askChoice(
            __("L'action hérite des permissions d'accès d'une autre action ?"),
            ['y', 'n'],
            'n'
        );
        if ($commeDroit === 'y') {
            $controller = $this->io->askChoice(
                __("Sélectionner un Controller"),
                array_keys($apis),
                explode('/', $target)[0]
            );
            $action = $this->io->askChoice(
                __("Sélectionner une action"),
                $apis[$controller]
            );
            $commeDroit = Hash::get($data, $controller.'.'.$action.'.commeDroits')
                ?: $controller.'::'.$action;
        } else {
            $commeDroit = '';
        }
        return $commeDroit;
    }

    /**
     * Défini le groupe
     * @return mixed|string
     */
    private function askForGroup()
    {
        $pathToControllersGroupJson = Configure::read(
            'App.paths.controllers_group',
            RESOURCES . 'controllers_group.json'
        );
        $groups = json_decode(file_get_contents($pathToControllersGroupJson), true);
        $groups[] = 'empty';
        foreach ($groups as $key => $group) {
            $this->io->info(sprintf('%d - %s', $key, $group));
        }
        $key = $this->io->askChoice(
            __("Indiquer le groupe de la permission"),
            array_keys($groups)
        );
        return $groups[$key] ?? $key;
    }

    /**
     * Donne l'aco api niveau controlleur ex: root/api/Transfers
     * @param EntityInterface $root
     * @param string          $controller
     * @return array|EntityInterface
     */
    private function getParentAco(EntityInterface $root, string $controller)
    {
        if (!$this->args->getOption('commeDroits')) {
            $parent = $this->Acos->findOrCreate(
                [
                    'model' => 'api',
                    'alias' => $controller,
                    'parent_id' => $root->get('id')
                ]
            );
        } else {
            [$cdController, $cdAction] = explode('::', $this->args->getOption('commeDroits'));
            $parent = $this->Acos->find()
                ->select(['id'])
                ->where(
                    [
                        'model' => $cdController,
                        'alias' => $cdAction,
                        'lft >=' => $root->get('lft'),
                        'rght <=' => $root->get('rght'),
                    ]
                )
                ->firstOrFail();
        }
        return $parent;
    }
}
