<?php
/**
 * AsalaeCore\Command\MaintenanceOffCommand
 */

namespace AsalaeCore\Command;

use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use DateTime;
use DateTimeInterface;
use Exception;

/**
 * Gestion du mode maintenance OFF
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2024, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class MaintenanceOffCommand extends AbstractMaintenance
{
    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'maintenance off';
    }

    /**
     * Action principale
     * @param Arguments $args The command arguments.
     * @param ConsoleIo $io   The console io
     * @throws Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $config = $this->getConfig();
        $config['enabled'] = false;

        $this->unsetScheduledIfNeeded($config, $io);
        $this->unsetPeriodicIfNeeded($config, $io);

        $this->setConfig($config);
        $io->success('Maintenance is OFF');
    }

    /**
     * Si on est en cours d'interruption pour cause d'interruption programée...
     * @param array     $config
     * @param ConsoleIo $io
     * @return void
     * @throws Exception
     */
    private function unsetScheduledIfNeeded(array &$config, ConsoleIo $io)
    {
        $scheduledBegin = $config['scheduled']['begin'] ?? null;
        $scheduledEnd = $config['scheduled']['end'] ?? null;
        $scheduledBegin = $scheduledBegin ? new DateTime($scheduledBegin) : null;
        $scheduledEnd = $scheduledEnd ? new DateTime($scheduledEnd) : null;
        $now = new DateTime();

        $unsetScheduled = false;
        if ($scheduledBegin && $scheduledEnd && $now > $scheduledBegin && $now < $scheduledEnd) {
            $io->warning(
                __(
                    "La maintenance a été programmée pour la période {0} à {1}",
                    $scheduledBegin->format(DateTimeInterface::ATOM),
                    $scheduledEnd->format(DateTimeInterface::ATOM),
                )
            );
            $unsetScheduled = true;
        } elseif ($scheduledBegin && $now > $scheduledBegin) {
            $io->warning(
                __(
                    "La maintenance a été programmée pour à partir de {0}",
                    $scheduledBegin->format(DateTimeInterface::ATOM),
                )
            );
            $unsetScheduled = true;
        } elseif ($scheduledEnd && $now < $scheduledEnd) {
            $io->warning(
                __(
                    "La maintenance a été programmée jusqu'à {0}",
                    $scheduledEnd->format(DateTimeInterface::ATOM),
                )
            );
            $unsetScheduled = true;
        }
        if ($unsetScheduled) {
            unset($config['scheduled']);
            $io->warning(
                __("La maintenance programmée a donc été retirée pour désactiver le mode maintenance")
            );
        }
    }

    /**
     * Si on est en cours d'interruption pour cause d'interruption périodique...
     * @param array     $config
     * @param ConsoleIo $io
     * @return void
     * @throws Exception
     */
    private function unsetPeriodicIfNeeded(array &$config, ConsoleIo $io)
    {
        $periodicBegin = $config['periodic']['begin'] ?? null;
        $periodicEnd = $config['periodic']['end'] ?? null;
        $periodicBegin = $periodicBegin ? new DateTime($periodicBegin) : null;
        $periodicEnd = $periodicEnd ? new DateTime($periodicEnd) : null;
        $now = new DateTime();

        if ($periodicBegin && $periodicEnd && $now > $periodicBegin && $now < $periodicEnd) {
            $io->warning(
                __(
                    "Il est actuellement {0} et la maintenance périodique" .
                    " a été programmée pour la période {1} à {2}",
                    $now->format('H:i:s'),
                    $config['periodic']['begin'],
                    $config['periodic']['end'],
                )
            );
            unset($config['periodic']);
            $io->warning(
                __("La maintenance periodique a donc été retirée pour désactiver le mode maintenance")
            );
        }
    }
}
