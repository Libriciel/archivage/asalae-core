<?php

/**
 * AsalaeCore\Command\XsdExploreCommand
 */

namespace AsalaeCore\Command;

use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use DOMDocument;
use DOMElement;
use DOMXPath;
use Exception;

/**
 * Aide la compréhension d'un XSD
 *
 * @category    Command
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2024, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class XsdExploreCommand extends Command
{
    /**
     * Gets the option parser instance and configures it.
     *
     * @param ConsoleOptionParser $parser Option parser to update.
     * @return ConsoleOptionParser
     */
    public function buildOptionParser(ConsoleOptionParser $parser): ConsoleOptionParser
    {
        $parser->setDescription(
            __("Aide la compréhension d'un XSD en affichant les noeuds enfants dans l'ordre de sequence")
        );
        $parser->addArgument(
            'xsdFilename',
            [
                'help' => __("Chemin vers le fichier XSD"),
                'required' => true,
            ]
        );
        $parser->addArgument(
            'nodeName',
            [
                'help' => __("Nom du noeud sur lequel afficher la liste des noeuds possibles"),
                'required' => true,
            ]
        );
        return $parser;
    }

    /**
     * Main command
     * @param Arguments $args
     * @param ConsoleIo $io
     * @return void
     * @throws Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        try {
            $data = $this->getNodeChildrenFromXSD(
                $this->loadAndMergeXsd(realpath($args->getArgument('xsdFilename'))),
                $args->getArgument('nodeName')
            );

            // En-tête du tableau avec une nouvelle colonne "Cardinality" et "Type"
            $tableData = [
                [
                    $args->getArgument('nodeName') . ' (' . count($data) . ' nodes)',
                    'Cardinality',
                    'Type',
                ]
            ];
            foreach ($data as $node) {
                // Affichage de l'élément, de sa cardinalité, et de son type
                $tableData[] = [$node['name'], $node['cardinality'], $node['type']];
            }

            $io->helper('Table')->output($tableData);
        } catch (Exception $e) {
            $io->abort($e->getMessage());
        }
        $this->abort(self::CODE_SUCCESS);
    }

    /**
     * Charge un fichier XSD et fusionne ses inclusions et imports de façon récursive.
     *
     * @param string $xsdFilename Le chemin du fichier XSD à charger.
     * @return DOMDocument Le document XSD fusionné.
     * @throws Exception Si le fichier XSD ne peut pas être chargé.
     */
    private function loadAndMergeXsd($xsdFilename): DOMDocument
    {
        if (!file_exists($xsdFilename)) {
            throw new Exception("Le fichier XSD '$xsdFilename' est introuvable.");
        }

        $dom = new DOMDocument();
        $dom->load($xsdFilename);

        $this->mergeImportsAndIncludes($dom, dirname($xsdFilename));

        return $dom;
    }

    /**
     * Fusionne les éléments <xsd:include> et <xsd:import> d'un XSD donné dans le document principal.
     *
     * @param DOMDocument $dom           Le document XSD principal.
     * @param string      $basePath      Le chemin de base pour résoudre les chemins relatifs des fichiers XSD inclus
     *                                   ou importés.
     * @param array       $importedFiles
     */
    private function mergeImportsAndIncludes(
        DOMDocument $dom,
        string $basePath,
        array &$importedFiles = []
    ): void {
        $xpath = new DOMXPath($dom);
        $xpath->registerNamespace('xs', 'http://www.w3.org/2001/XMLSchema');

        // Rechercher les imports et les includes
        $importNodes = $xpath->query('//xs:import | //xs:include');

        foreach ($importNodes as $importNode) {
            $schemaLocation = $importNode->getAttribute('schemaLocation');
            $fullPath = realpath($basePath . '/' . $schemaLocation);

            // Vérifier si le fichier a déjà été importé
            if ($fullPath && !in_array($fullPath, $importedFiles, true)) {
                $importedFiles[] = $fullPath;

                // Charger le document importé
                $importedDom = new DOMDocument();
                $importedDom->load($fullPath);

                // Appeler récursivement pour traiter les imports/includes dans le fichier importé
                $this->mergeImportsAndIncludes($importedDom, dirname($fullPath), $importedFiles);

                // Importer les noeuds du document importé dans le document actuel
                foreach ($importedDom->documentElement->childNodes as $importedNode) {
                    $importedNode = $dom->importNode($importedNode, true);
                    $dom->documentElement->appendChild($importedNode);
                }
            }

            // Supprimer le noeud d'importation/include du document d'origine
            $importNode->parentNode->removeChild($importNode);
        }
    }

    /**
     * Recherche les nœuds enfants possibles pour un nœud donné dans un document XSD fusionné.
     *
     * @param DOMDocument $dom      Le document XSD
     *                              fusionné.
     * @param string      $nodeName Le nom du nœud cible dont on veut obtenir les
     *                              enfants.
     * @return array Un tableau contenant les noms des nœuds enfants possibles.
     * @throws Exception
     */
    private function getNodeChildrenFromXSD(DOMDocument $dom, string $nodeName): array
    {
        $xpath = new DOMXPath($dom);
        $schemaNamespace = $dom->documentElement->namespaceURI;
        $prefix = $dom->documentElement->prefix ?: 'xs';  // Default to 'xs' if no prefix is defined
        $xpath->registerNamespace($prefix, $schemaNamespace);

        // Rechercher l'élément par nom
        $xpathQuery = "//$prefix:element[@name='$nodeName']";
        $elementNodeList = $xpath->query($xpathQuery);

        if ($elementNodeList->length === 0) {
            throw new Exception(__("{0} n'a pas été trouvé.", $xpathQuery));
        }

        $seenElements = [];
        $children = [];

        /** @var DOMElement $elementNode */
        foreach ($elementNodeList as $elementNode) {
            // Vérifie si l'élément a un complexType local
            $localComplexType = $xpath->query(".//$prefix:complexType", $elementNode);
            if ($localComplexType->length > 0) {
                foreach ($localComplexType as $complexTypeNode) {
                    $children = array_merge(
                        $children,
                        $this->parseComplexType($complexTypeNode, $xpath, $prefix, $seenElements)
                    );
                }
            } else {
                // Gérer le cas où un type est référencé
                $typeName = $elementNode->getAttribute('type');
                if ($typeName) {
                    $children = array_merge(
                        $children,
                        $this->parseTypeReference($typeName, $xpath, $prefix, $seenElements)
                    );
                }
            }
        }

        return $children;
    }
    /**
     * Parse les types référencés détectés dans les attributs de type d'éléments.
     *
     * @param string   $typeName     Nom du type référencé.
     * @param DOMXPath $xpath        Objet XPath pour les requêtes.
     * @param string   $prefix       Préfixe utilisé dans le namespace XSD.
     * @param array    $seenElements Éléments déjà traités pour éviter les doublons.
     * @return array Liste des enfants trouvés pour le type.
     */
    private function parseTypeReference($typeName, DOMXPath $xpath, $prefix, &$seenElements): array
    {
        $children = [];
        $typeNodeList = $xpath->query(
            "//$prefix:complexType[@name='$typeName'] | //$prefix:simpleType[@name='$typeName']"
        );
        foreach ($typeNodeList as $typeNode) {
            // Gestion des extensions pour le type référencé
            $extensionNodeList = $xpath->query(".//$prefix:extension", $typeNode);
            foreach ($extensionNodeList as $extensionNode) {
                $baseTypeName = $extensionNode->getAttribute('base');
                if ($baseTypeName) {
                    // Récupère les enfants du type de base étendu
                    $children = array_merge(
                        $children,
                        $this->parseTypeReference($baseTypeName, $xpath, $prefix, $seenElements)
                    );
                }

                // Récupère les enfants spécifiques à cette extension
                $sequenceOrChoiceNodeList = $xpath->query(".//$prefix:sequence | .//$prefix:choice", $extensionNode);
                foreach ($sequenceOrChoiceNodeList as $container) {
                    $children = array_merge(
                        $children,
                        $this->parseContainer($container, $xpath, $prefix, $seenElements)
                    );
                }
            }

            // Gestion des séquences et choix dans le complexType référencé
            $children = array_merge($children, $this->parseComplexType($typeNode, $xpath, $prefix, $seenElements));
        }
        return $children;
    }

    /**
     * Analyse un nœud complexType pour en extraire les noms des nœuds enfants
     * possibles au premier niveau dans l'ordre d'apparition, sans doublons.
     *
     * @param DOMElement $complexTypeNode Le nœud complexType à analyser.
     * @param DOMXPath   $xpath           L'objet DOMXPath utilisé pour effectuer des
     *                                    requêtes XPath.
     * @param string     $prefix          Le préfixe à utiliser pour les
     *                                    requêtes XPath.
     * @param array      $seenElements    Tableau des éléments déjà vus pour
     *                                    éviter les doublons.
     * @return array Un tableau contenant les noms des nœuds enfants possibles au premier niveau, sans doublons.
     */
    private function parseComplexType(
        DOMElement $complexTypeNode,
        DOMXPath $xpath,
        string $prefix,
        array &$seenElements
    ): array {
        $children = [];

        // Gestion des extensions
        $extensionNodeList = $xpath->query(".//$prefix:extension", $complexTypeNode);
        foreach ($extensionNodeList as $extensionNode) {
            $baseTypeName = $extensionNode->getAttribute('base');
            if ($baseTypeName) {
                // Récupère les enfants du type de base étendu
                $children = array_merge(
                    $children,
                    $this->parseTypeReference($baseTypeName, $xpath, $prefix, $seenElements)
                );
            }

            // Récupère les enfants spécifiques à cette extension
            $sequenceOrChoiceNodeList = $xpath->query(".//$prefix:sequence | .//$prefix:choice", $extensionNode);
            foreach ($sequenceOrChoiceNodeList as $container) {
                $children = array_merge($children, $this->parseContainer($container, $xpath, $prefix, $seenElements));
            }
        }

        // Gestion des séquences et choix dans le complexType (hors extension)
        $nodeList = $xpath->query(".//$prefix:sequence | .//$prefix:choice", $complexTypeNode);
        foreach ($nodeList as $container) {
            $children = array_merge($children, $this->parseContainer($container, $xpath, $prefix, $seenElements));
        }

        // Ajout pour gérer les références de groupe dans les types complexes
        $groupRefs = $xpath->query(".//$prefix:group/@ref", $complexTypeNode);
        foreach ($groupRefs as $groupRef) {
            $groupName = $groupRef->nodeValue;
            $children = array_merge($children, $this->parseGroupByName($groupName, $xpath, $prefix, $seenElements));
        }

        return $children;
    }

    /**
     * Traite un groupe par son nom pour en extraire les noms des nœuds enfants possibles au premier niveau.
     *
     * Cette méthode cherche un groupe spécifié par son nom dans le document XSD, puis analyse les éléments
     * qu'il contient pour extraire les noms des enfants possibles. Il s'agit d'une extension de la gestion des
     * groupes pour permettre l'analyse directe des références de groupe dans les types complexes.
     *
     * @param string   $groupName    Le nom du groupe à analyser.
     * @param DOMXPath $xpath        L'objet DOMXPath utilisé pour effectuer des requêtes XPath.
     * @param string   $prefix       Le préfixe à utiliser pour les requêtes XPath.
     * @param array    $seenElements Tableau des éléments déjà vus pour éviter les doublons.
     * @return array Un tableau contenant les noms des nœuds enfants possibles au premier niveau,
     *               extraits du groupe spécifié.
     */
    private function parseGroupByName($groupName, DOMXPath $xpath, $prefix, &$seenElements)
    {
        $children = [];
        $groupNodeList = $xpath->query("//$prefix:group[@name='$groupName']");
        if ($groupNodeList->length > 0) {
            $groupNode = $groupNodeList->item(0);
            $childNodeList = $xpath->query(".//$prefix:sequence | .//$prefix:choice", $groupNode);
            foreach ($childNodeList as $container) {
                $children = array_merge($children, $this->parseContainer($container, $xpath, $prefix, $seenElements));
            }
        }
        return $children;
    }

    /**
     * @var array Garde les noeuds choices en mémoire pour les différencier
     */
    private array $choiceNodes = [];

    /**
     * Quel est l'index de ce choice ? en ajoute un si nécéssaire
     * @param DOMElement $choiceNode
     * @return int
     */
    private function getChoiceIndex(DOMElement $choiceNode): int
    {
        foreach ($this->choiceNodes as $index => $node) {
            if ($choiceNode === $node) {
                return $index;
            }
        }
        $index = count($this->choiceNodes);
        $this->choiceNodes[] = $choiceNode;
        return $index;
    }

    /**
     * Liste le contenu pour un element
     * @param DOMElement $child
     * @param string     $prefix
     * @param array      $seenElements
     * @param array      $children
     * @return void
     */
    private function parseContainerElement(
        DOMElement $child,
        string $prefix,
        array &$seenElements,
        array &$children
    ): void {
        $elementName = $child->getAttribute('name');
        $typeName = $child->getAttribute('type');
        if ($elementName && !in_array($elementName, $seenElements, true)) {
            /** @var DOMElement $parent */
            $parent = $child->parentNode;
            $beforeCardinality = '';
            if ($parent->nodeName === "$prefix:choice") {
                $minOccurs = ($parent->getAttribute('minOccurs') !== '') ? $parent->getAttribute('minOccurs') : '1';
                $maxOccurs = ($parent->getAttribute('maxOccurs') !== '') ? $parent->getAttribute('maxOccurs') : '1';
                if ($maxOccurs === 'unbounded') {
                    $maxOccurs = 'n';
                }
                $index = $this->getChoiceIndex($parent);
                $beforeCardinality = "choice[$index]:$minOccurs..$maxOccurs - ";
            }

            $minOccurs = ($child->getAttribute('minOccurs') !== '') ? $child->getAttribute('minOccurs') : '1';
            $maxOccurs = ($child->getAttribute('maxOccurs') !== '') ? $child->getAttribute('maxOccurs') : '1';
            if ($maxOccurs === 'unbounded') {
                $maxOccurs = 'n';
            }
            $cardinality = "$beforeCardinality$minOccurs..$maxOccurs";

            if (!$typeName) {
                $typeName = 'Unknown';  // Définit un type inconnu si aucun n'est trouvé
            }

            // Ajouter le nom de l'élément, la cardinalité et le type
            $children[] = ['name' => $elementName, 'cardinality' => $cardinality, 'type' => $typeName];

            $seenElements[] = $elementName;
        }
    }

    /**
     * Liste le contenu pour un groupe
     * @param DOMElement $child
     * @param string     $prefix
     * @param DOMXPath   $xpath
     * @param array      $seenElements
     * @param array      $children
     * @return void
     */
    private function parseContainerGroup(
        DOMElement $child,
        string $prefix,
        DOMXPath $xpath,
        array &$seenElements,
        array &$children
    ): void {
        $groupName = $child->getAttribute('ref');
        if ($groupName && !in_array($groupName, $seenElements, true)) {
            // Traite les groupes uniquement pour éviter la récursion
            $groupNodeList = $xpath->query("//$prefix:group[@name='$groupName']");
            if ($groupNodeList->length > 0) {
                $groupNode = $groupNodeList->item(0);
                $children = array_merge(
                    $children,
                    $this->parseGroup($groupNode, $xpath, $prefix, $seenElements)
                );
            }
        }
    }

    /**
     * Analyse un conteneur (sequence ou choice) pour extraire les noms des nœuds enfants possibles au premier niveau.
     *
     * @param DOMElement $container    Le conteneur (sequence ou choice) à
     *                                 analyser.
     * @param DOMXPath   $xpath        L'objet DOMXPath utilisé pour effectuer des
     *                                 requêtes XPath.
     * @param string     $prefix       Le préfixe à utiliser pour les
     *                                 requêtes XPath.
     * @param array      $seenElements Tableau des éléments déjà vus pour éviter les
     *                                 doublons.
     * @return array Un tableau contenant les noms des nœuds enfants possibles au premier niveau.
     */
    private function parseContainer(DOMElement $container, DOMXPath $xpath, string $prefix, array &$seenElements): array
    {
        $children = [];
        $childNodeList = $xpath->query(".//*", $container);

        foreach ($childNodeList as $child) {
            if ($child->nodeName === "$prefix:element") {
                $this->parseContainerElement($child, $prefix, $seenElements, $children);
            } elseif ($child->nodeName === "$prefix:group") {
                $this->parseContainerGroup($child, $prefix, $xpath, $seenElements, $children);
            }
        }

        return $children;
    }

    /**
     * Traite un groupe pour en extraire les noms des nœuds enfants possibles au premier niveau.
     *
     * @param DOMElement $group        Le nœud groupe à
     *                                 analyser.
     * @param DOMXPath   $xpath        L'objet DOMXPath utilisé pour effectuer des
     *                                 requêtes XPath.
     * @param string     $prefix       Le préfixe à utiliser pour les
     *                                 requêtes XPath.
     * @param array      $seenElements Tableau des éléments déjà vus pour éviter les
     *                                 doublons.
     * @return array Un tableau contenant les noms des nœuds enfants possibles au premier niveau.
     */
    private function parseGroup(DOMElement $group, DOMXPath $xpath, string $prefix, array &$seenElements): array
    {
        $children = [];
        $ref = $group->getAttribute('name'); // Utilise 'name' au lieu de 'ref'
        $groupNodeList = $xpath->query("//$prefix:group[@name='$ref']");
        $groupNode = $groupNodeList->length > 0 ? $groupNodeList->item(0) : null;

        if ($groupNode) {
            // Traite le groupe sans aller plus loin dans les éléments contenus
            $childNodeList = $xpath->query(".//$prefix:sequence | .//$prefix:choice", $groupNode);

            foreach ($childNodeList as $container) {
                $children = array_merge($children, $this->parseContainer($container, $xpath, $prefix, $seenElements));
            }
        }

        return $children;
    }
}
