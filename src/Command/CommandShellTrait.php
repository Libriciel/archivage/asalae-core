<?php
/**
 * AsalaeCore\Command\CommandShellTrait
 */

namespace AsalaeCore\Command;

use AsalaeCore\Factory\Utility;
use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOutput;
use Cake\Console\Exception\ConsoleException;
use Cake\Console\Exception\StopException;
use Cake\Console\Helper;
use Cake\Utility\Inflector;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;

/**
 * Conversion d'un shell en commande
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2020, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin Command
 */
trait CommandShellTrait
{
    /**
     * @var array
     */
    public $paramsShell;
    /**
     * @var bool|null
     */
    public $interactiveShell = true;
    /**
     * @var ConsoleIo
     */
    public $ioShell;
    /**
     * @var array Array of arguments to run the shell with.
     */
    public $argv = [];
    /**
     * @var string
     */
    public $subcommand;

    /**
     * Override the default behavior to save the command called
     * in order to pass it to the command dispatcher
     *
     * @param array     $argv Arguments from the CLI environment.
     * @param ConsoleIo $io   The console io
     * @return int|null Exit code or null for success.
     * @throws Exception
     */
    public function run(array $argv, ConsoleIo $io): ?int
    {
        $this->argv = $argv;

        if (isset($argv[0])) {
            $subcommand = Inflector::variable($argv[0]);
            if (method_exists($this, $subcommand)) {
                $this->subcommand = $subcommand;
            } else {
                $this->subcommand = 'main';
            }
        } else {
            $this->subcommand = 'main';
        }

        $this->initialize();

        $parser = $this->getOptionParser();
        try {
            [$options, $arguments] = $parser->parse($argv);
            $args = new Arguments(
                $arguments,
                $options,
                $parser->argumentNames()
            );
        } catch (ConsoleException $e) {
            $io->err('Error: ' . $e->getMessage());
            return static::CODE_ERROR;
        }
        $this->setOutputLevel($args, $io);

        if ($args->getOption('help')) {
            $format = 'text';
            if ($args->getArgumentAt(0) === 'xml') {
                $format = 'xml';
                $io->setOutputAs(ConsoleOutput::RAW);
            }

            $helpCommand = $this->subcommand === 'main' ? null : $this->subcommand;
            $io->out($parser->help($helpCommand, $format));

            return static::CODE_SUCCESS;
        }

        if ($args->getOption('quiet')) {
            $io->setInteractive(false);
        }

        return $this->execute($args, $io);
    }

    /**
     * Main command
     * @param Arguments $args
     * @param ConsoleIo $io
     * @return int|null
     */
    public function execute(Arguments $args, ConsoleIo $io): ?int
    {
        $this->ioShell = $io;
        $arguments = $args->getArguments();
        $this->paramsShell = $args->getOptions();
        $parser = $this->getOptionParser();
        $subcommands = $parser->subcommands();
        if ($this->subcommand !== 'main' && isset($subcommands[$this->subcommand])) {
            $subcommand = $subcommands[$this->subcommand];
            $parser = $subcommand->parser();
        }
        $required = 0;
        if ($parser) {
            foreach ($parser->arguments() as $arg) {
                if ($arg->isRequired()) {
                    $required++;
                }
            }
        }
        if ($required && (count($arguments) < $required)) {
            $helpCommand = $this->subcommand === 'main' ? null : $this->subcommand;
            $io->out($this->getOptionParser()->help($helpCommand));
            $this->abort();
        }

        if (method_exists($this, $this->subcommand)) {
            call_user_func_array([$this, $this->subcommand], $arguments);
        } else { // si pas de main() ni de sous commande choisie, on affiche l'aide
            $io->out($this->getOptionParser()->help());
            $this->abort();
        }
        return self::CODE_SUCCESS;
    }

    /**
     * Outputs a single or multiple messages to stdout. If no parameters
     * are passed outputs just a newline.
     *
     * ### Output levels
     *
     * There are 3 built-in output level. ConsoleIo::QUIET, ConsoleIo::NORMAL, ConsoleIo::VERBOSE.
     * The verbose and quiet output levels, map to the `verbose` and `quiet` output switches
     * present in most shells. Using ConsoleIo::QUIET for a message means it will always display.
     * While using ConsoleIo::VERBOSE means it will only display when verbose output is toggled.
     *
     * @param string|string[] $message  A string or an array of strings to output
     * @param int             $newlines Number of newlines to append
     * @param int             $level    The message's output level, see above.
     * @return int|null The number of bytes returned from writing to stdout.
     * @link https://book.cakephp.org/4/en/console-and-shells.html#Shell::out
     */
    public function out($message, int $newlines = 1, int $level = ConsoleIo::NORMAL): ?int
    {
        return $this->ioShell->out($message, $newlines, $level);
    }

    /**
     * Outputs a single or multiple error messages to stderr. If no parameters
     * are passed outputs just a newline.
     *
     * @param string|string[] $message  A string or an array of strings to output
     * @param int             $newlines Number of newlines to append
     * @return int The number of bytes returned from writing to stderr.
     */
    public function err($message, int $newlines = 1): int
    {
        return $this->ioShell->error($message, $newlines);
    }

    /**
     * Render a Console Helper
     *
     * Create and render the output for a helper object. If the helper
     * object has not already been loaded, it will be loaded and constructed.
     *
     * @param string $name     The name of the helper to render
     * @param array  $settings Configuration data for the helper.
     * @return \Cake\Console\Helper The created helper instance.
     */
    public function helper(string $name, array $settings = []): Helper
    {
        return $this->ioShell->helper($name, $settings);
    }

    /**
     * Safely access the values in $this->params.
     *
     * @param string $name The name of the parameter to get.
     * @return string|bool|null Value. Will return null if it doesn't exist.
     */
    public function param(string $name)
    {
        if (!isset($this->paramsShell[$name])) {
            return null;
        }

        return $this->paramsShell[$name];
    }

    /**
     * Prompts the user for input, and returns it.
     *
     * @param string            $prompt  Prompt text.
     * @param string|array|null $options Array or string of options.
     * @param string|null       $default Default input value.
     * @return string|null Either the default value, or the user-provided input.
     * @link https://book.cakephp.org/4/en/console-and-shells.html#Shell::in
     */
    public function in(string $prompt, $options = null, ?string $default = null): ?string
    {
        if (!$this->interactiveShell) {
            return $default;
        }
        if ($options) {
            return $this->ioShell->askChoice($prompt, $options, $default);
        }

        return $this->ioShell->ask($prompt, $default);
    }

    /**
     * Convenience method for err() that wraps message between <warning /> tag
     *
     * @param string|string[] $message  A string or an array of strings to output
     * @param int             $newlines Number of newlines to append
     * @return int The number of bytes returned from writing to stderr.
     * @see https://book.cakephp.org/4/en/console-and-shells.html#Shell::err
     */
    public function warn($message, int $newlines = 1): int
    {
        return $this->ioShell->warning($message, $newlines);
    }

    /**
     * Convenience method for out() that wraps message between <info /> tag
     *
     * @param string|string[] $message  A string or an array of strings to output
     * @param int             $newlines Number of newlines to append
     * @param int             $level    The message's output level, see above.
     * @return int|null The number of bytes returned from writing to stdout.
     * @see https://book.cakephp.org/4/en/console-and-shells.html#Shell::out
     */
    public function info($message, int $newlines = 1, int $level = ConsoleIo::NORMAL): ?int
    {
        return $this->ioShell->info($message, $newlines, $level);
    }

    /**
     * Convenience method for out() that wraps message between <success /> tag
     *
     * @param string|string[] $message  A string or an array of strings to output
     * @param int             $newlines Number of newlines to append
     * @param int             $level    The message's output level, see above.
     * @return int|null The number of bytes returned from writing to stdout.
     * @see https://book.cakephp.org/4/en/console-and-shells.html#Shell::out
     */
    public function success($message, int $newlines = 1, int $level = ConsoleIo::NORMAL): ?int
    {
        return $this->ioShell->success($message, $newlines, $level);
    }

    /**
     * Returns a single or multiple linefeeds sequences.
     *
     * @param int $multiplier Number of times the linefeed sequence should be repeated
     * @return string
     * @link https://book.cakephp.org/4/en/console-and-shells.html#Shell::nl
     */
    public function nl(int $multiplier = 1): string
    {
        return $this->ioShell->nl($multiplier);
    }

    /**
     * Outputs a series of minus characters to the standard output, acts as a visual separator.
     *
     * @param int $newlines Number of newlines to pre- and append
     * @param int $width    Width of the line, defaults to 63
     * @return void
     * @link https://book.cakephp.org/4/en/console-and-shells.html#Shell::hr
     */
    public function hr(int $newlines = 0, int $width = 63): void
    {
        $this->ioShell->hr($newlines, $width);
    }

    /**
     * Creates a file at given path
     *
     * @param string $path     Where to put the file.
     * @param string $contents Content to put in the file.
     * @return bool Success
     * @throws Exception
     * @link https://book.cakephp.org/4/en/console-and-shells.html#creating-files
     */
    public function createFile(string $path, string $contents): bool
    {
        $path = str_replace(DIRECTORY_SEPARATOR . DIRECTORY_SEPARATOR, DIRECTORY_SEPARATOR, $path);

        $this->ioShell->out();

        $fileExists = is_file($path);
        if ($fileExists && !$this->param('force') && !$this->interactiveShell) {
            $this->ioShell->out('<warning>File exists, skipping</warning>.');

            return false;
        }

        if ($fileExists && $this->interactiveShell && empty($this->paramsShell['force'])) {
            $this->ioShell->out(sprintf('<warning>File `%s` exists</warning>', $path));
            $key = $this->ioShell->askChoice('Do you want to overwrite?', ['y', 'n', 'a', 'q'], 'n');

            if (strtolower($key) === 'q') {
                $this->ioShell->out('<error>Quitting</error>.', 2);
                $this->_stop();
            }
            if (strtolower($key) === 'a') {
                $this->paramsShell['force'] = true;
                $key = 'y';
            }
            if (strtolower($key) !== 'y') {
                $this->ioShell->out(sprintf('Skip `%s`', $path), 2);

                return false;
            }
        } else {
            $this->out(sprintf('Creating file %s', $path));
        }

        try {
            /** @var Filesystem $fs */
            $fs = Utility::get('Filesystem');
            $fs->dumpFile($path, $contents);

            $this->ioShell->out(sprintf('<success>Wrote</success> `%s`', $path));
        } catch (\Cake\Core\Exception\Exception $e) {
            $this->ioShell->err(sprintf('<error>Could not write to `%s`</error>.', $path), 2);

            return false;
        }

        return true;
    }

    /**
     * Stop execution of the current script.
     * Raises a StopException to try and halt the execution.
     *
     * @param int $status see https://secure.php.net/exit for values
     * @throws StopException
     * @return void
     */
    protected function _stop(int $status = self::CODE_SUCCESS): void
    {
        throw new StopException('Halting error reached', $status);
    }

    /**
     * Displays a formatted error message
     * and exits the application with an error code.
     *
     * @param string $message  The error message
     * @param int    $exitCode The exit code for the shell task.
     * @throws StopException
     * @return void
     * @link https://book.cakephp.org/4/en/console-and-shells.html#styling-output
     * @psalm-return never-return
     */
    public function abortShell(string $message, int $exitCode = self::CODE_ERROR): void
    {
        if (!$this->ioShell) {
            $this->ioShell = new ConsoleIo();
        }
        $this->ioShell->err('<error>' . $message . '</error>');
        throw new StopException($message, $exitCode);
    }
}
