<?php
/**
 * AsalaeCore\Command\JobDeleteAllCommand
 */

namespace AsalaeCore\Command;

use AsalaeCore\Factory\Utility;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;
use Exception;

/**
 * Permet de supprimer tous les jobs en erreur (buried) d'un tube
 * ex: bin/cake job delete_all test --state buried
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2021, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class JobDeleteAllCommand extends Command
{
    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'job delete_all';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = parent::getOptionParser();
        $parser->addArgument(
            'tube',
            [
                'help' => __("Tube sur lequel relancer tous les jobs"),
                'required' => true,
            ]
        );
        $parser->addOption(
            'state',
            [
                'help' => __("Etat des jobs à supprimer"),
                'default' => 'buried',
            ]
        );
        return $parser;
    }

    /**
     * Action principale
     * @param Arguments $args The command arguments.
     * @param ConsoleIo $io   The console io
     * @throws Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $tube = $args->getArgument('tube');
        $Jobs = TableRegistry::getTableLocator()->get('BeanstalkJobs');
        $query = $Jobs->find()
            ->where(['BeanstalkJobs.tube' => $tube])
            ->order(['priority']);
        $deleted = 0;
        $Beanstalk = Utility::get('Beanstalk');
        $Beanstalk->setTube($tube);
        /** @var EntityInterface $job */
        foreach ($query as $job) {
            if ($job->get('state') !== $args->getOption('state')) {
                continue;
            }
            try {
                $Beanstalk->selectJob($job->get('jobid'))->done();
                $deleted++;
            } catch (Exception $e) {
            }
        }
        if ($deleted === 0) {
            $io->warning(__("Aucun job n'a été supprimé"));
            return;
        }
        $this->logEvent(
            'shell_delete_all_jobs',
            'success',
            __("Suppression de {0} jobs du tube ''{0}''", $deleted, $tube)
        );
        $io->success(__("{0} jobs ont été supprimés", $deleted));
    }
}
