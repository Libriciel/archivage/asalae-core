<?php
/**
 * AsalaeCore\Command\TraductionCommand
 */

namespace AsalaeCore\Command;

use AsalaeCore\Error\ContextDebugger;
use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Core\Configure;
use Cake\Cache\Cache;
use Exception;

/**
 * Permet de manipuler les traductions
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2017-2018, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class TraductionCommand extends Command
{
    /**
     * Traits
     */
    use FileTrait;

    /**
     * @var array
     */
    public $paramsShell;
    /**
     * @var bool
     */
    public $interactiveShell = true;
    /**
     * @var ConsoleIo
     */
    public $ioShell;

    /**
     * Méthode principale
     *
     * @param Arguments $args The command arguments.
     * @param ConsoleIo $io   The console io
     * @throws Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $this->ioShell = $io;
        $locales = Configure::read('App.paths.locales');
        $localDir = count($locales) > 1
            ? $io->askChoice(
                __d('traduction_shell', "Veuillez sélectionner le répertoire du Locale de sortie"),
                $locales,
                current($locales)
            )
            : current($locales);

        $choices = [];
        foreach (scandir($localDir) as $dir) {
            if ($dir[0] !== '.' && is_dir($localDir.$dir)) {
                $choices[] = $dir;
            }
        }

        if (count($choices) > 1) {
            $defaultLocale = Configure::read('App.defaultLocale');
            if ($defaultLocale && !in_array($defaultLocale, $choices) && strpos($defaultLocale, '_')) {
                [$defaultLocale, ] = explode('_', $defaultLocale);
            }
            if (empty($defaultLocale) || !in_array($defaultLocale, $choices)) {
                $defaultLocale = current($choices);
            }
            $locale = $localDir.$io->askChoice(
                __d('traduction_shell', "Veuillez sélectionner la langue à traiter"),
                $choices,
                $defaultLocale
            );
        } else {
            $locale = $localDir.current($choices);
        }

        Cache::clear('_cake_core_');
        $templates = [];
        foreach (Configure::read('App.paths.locales') as $localPaths) {
            $templates = array_merge(
                $templates,
                glob($localPaths . '*.pot')
            );
        }
        $todoList = [];
        $domain = null;
        foreach ($templates as $path) {
            $domain = pathinfo($path, PATHINFO_FILENAME);
            $poName = $domain . '.po';
            $savePath = $locale . DS . $poName;
            $pot = $this->parsePo($path);
            if (!is_file($locale . DS . $poName)) {
                $po = [];
            } else {
                $po = $this->parsePo($locale . DS . $poName);
            }
            if ($this->completeMissingStr($domain, $pot, $po, $savePath, true)) {
                $todoList[] = compact('domain', 'pot', 'po', 'savePath');
            }
        }
        while ($todoList) {
            $keys = array_keys($todoList);
            $list = array_map(
                function ($k, $v) {
                    return $k.' - '.$v['domain'];
                },
                $keys,
                $todoList
            );
            $key = $io->askChoice(
                __("Choisir un domaine à traduire parmis la liste suivante: \n{0}", implode(PHP_EOL, $list)),
                $keys,
                current($keys)
            );
            $todo = $todoList[$key];
            if ($this->completeMissingStr($domain, $todo['pot'], $todo['po'], $todo['savePath'])) {
                $newPo = $this->renderPo($todo['po']);
                $this->createFileWithDefault($todo['savePath'], $newPo, 'y');
            }
            unset($todoList[$key]);
        }
    }

    /**
     * Transforme un fichier po en array
     *
     * L'array est structuré ainsi :
     * [
     *      __header__ => En-tête du fichier
     *      $context => [
     *          $msgid => [
     *              'id' => ...,
     *              'str' => ...,
     *              'comments' => ...
     *          ]
     *      ]
     * ]
     * Le context est défini à __default__ si aucun contexte n'est indiqué
     * Supporte également les msgid_plural et msgstr[0], msgstr[1] ...
     *
     * @param string $filename
     * @return array
     */
    private function parsePo(string $filename): array
    {
        $handle = fopen($filename, 'r');
        $begin = true;
        $parsed = [
            '__header__' => ''
        ];
        $context = '__default__';
        $id = null;
        $comments = [];

        while (!feof($handle)) {
            $buffer = fgets($handle);
            if ($begin && !preg_match('/(^#: .*\d+$|^msg(ctxt|id) ".+")/', $buffer)) {
                if (strpos($buffer, '"PO-Revision-Date: ') === 0) {
                    $parsed['__header__'] .= '"PO-Revision-Date: ' . date('Y-m-d H:i') . '+0000\n"'.PHP_EOL;
                } elseif (empty(trim($buffer))) {
                    $begin = false;
                } else {
                    $parsed['__header__'] .= $buffer;
                }
                continue;
            }
            $begin = false;
            if (empty(trim($buffer))) {
                $context = '__default__';
                $id = null;
                $comments = [];
            } elseif (strpos($buffer, '#: ') === 0) {
                $comments[] = trim($buffer);
            } elseif (preg_match('/msgctxt "(.+)"/', $buffer, $match)) {
                $context = $match[1];
            } elseif (preg_match('/msgid "(.+)"/', $buffer, $match)) {
                $id = $match[1];
                $parsed[$context][$id]['id'] = $id;
                $parsed[$context][$id]['comments'] = $comments;
            } elseif (preg_match('/msg(id_plural|str|str\[\d+]) "(.+)"/', $buffer, $match)) {
                $parsed[$context][$id][$match[1]] = $match[2];
            }
        }
        fclose($handle);
        return $parsed;
    }

    /**
     * Ajoute les valeurs manquante au po (demande pour retirer ceux en trop)
     *
     * @param string $domain
     * @param array  $pot
     * @param array  $po
     * @param string $savePath
     * @param bool   $check
     * @return bool Y a t'il des modifications qui mérite une sauvegarde ?
     * @throws Exception
     */
    private function completeMissingStr(
        string $domain,
        array $pot,
        array &$po,
        string $savePath,
        bool $check = false
    ): bool {
        $hasChanged = false;
        if (empty($po['__header__']) && !empty($pot['__header__'])) {
            $po['__header__'] = $pot['__header__'];
        }

        // Ajoute les valeurs manquante au po et écrase les commentaires
        foreach ($pot as $context => $ids) {
            if ($context === '__header__') {
                continue;
            }
            foreach ($ids as $id => $params) {
                if (empty($po[$context][$id])) {
                    $po[$context][$id] = $params;
                } else {
                    $po[$context][$id]['comments'] = $params['comments'];
                }
            }
        }

        // Complete les msgstr vide
        $toRemove = [];
        $toAdd = [];
        foreach ($po as $context => $ids) {
            if ($context === '__header__') {
                continue;
            }
            foreach ($ids as $id => $params) {
                $comment = empty($params['comments']) ? '' : current($params['comments']);
                if (!isset($pot[$context][$id])) {
                    $toRemove[] = compact('context', 'id');
                } elseif (!empty($params['id_plural'])) {
                    if (empty($params['str[0]'])) {
                        $toAdd[] = [
                            'ref' => &$po[$context][$id]['str[0]'],
                            'callback' => [$this, 'completeStr'],
                            'args' => [$domain, $context, $id, $comment]
                        ];
                        $toAdd[] = [
                            'ref' => &$po[$context][$id]['str[1]'],
                            'callback' => [$this, 'completeStr'],
                            'args' => [$domain, $context, $params['id_plural'], $comment]
                        ];
                    }
                } elseif (empty($params['str'])) {
                    $toAdd[] = [
                        'ref' => &$po[$context][$id]['str'],
                        'callback' => [$this, 'completeStr'],
                        'args' => [$domain, $context, $id, $comment]
                    ];
                }
            }
        }
        $cRemove = count($toRemove);
        $cAdd = count($toAdd);
        if ($check) {
            return $cRemove || $cAdd;
        }
        if ($cRemove || $cAdd) {
            $this->ioShell->out(
                __d(
                    "traduction_shell",
                    "<comment>{0} : Il y a {1} traduction(s) en trop et il manque {2} traduction(s)</comment>",
                    $domain,
                    count($toRemove),
                    count($toAdd)
                )
            );
            $hasChanged = $this->removeExcessTranslations($domain, $toRemove, $po, $savePath);
            $this->ioShell->out(
                __d(
                    'traduction_shell',
                    "<warning>Tapez </warning><success>`__cmd help`</success><warning>"
                    ." pour effectuer une autre action que une traduction</warning>"
                )
            );
            $this->ioShell->out(
                __d(
                    "traduction_shell",
                    "<warning>La chaine à traduire se situera automatiquement dans votre presse papier</warning>"
                )
            );
            $hasChanged = $this->addMissingsTraductions($toAdd, $po, $savePath) || $hasChanged;
        }
        return $hasChanged;
    }

    /**
     * Permet la suppression des traductions
     * qui n'existent plus dans les fichiers pot
     * @param string $domain
     * @param array  $toRemove
     * @param array  $po
     * @param string $savePath
     * @param int    $i
     * @return bool
     * @throws Exception
     */
    protected function removeExcessTranslations(
        string $domain,
        array $toRemove,
        array &$po,
        string $savePath,
        int $i = 0
    ): bool {
        $hasChanged = false;
        $c = count($toRemove);
        for (; $i < $c; $i++) {
            $context = $toRemove[$i]['context'];
            $id = $toRemove[$i]['id'];
            exec(sprintf('printf "%%s" "%s" | xclip -sel clip >/dev/null 2>&1', trim(addcslashes($id, '`"'), "'")));
            $this->ioShell->out(sprintf("<info>%s / %s</info>", $i+1, $c));
            $comment = !empty($po[$context][$id]['comments']) ? ' ('.current($po[$context][$id]['comments']).')' : '';
            if (preg_match('/^#: ([^:]+):(\d+)/', trim($comment, '() '), $matches)) {
                if (is_readable($filename = APP . $matches[1])) {
                    $extracted = ContextDebugger::getContext($filename, (int)$matches[2], 3);
                    $extracted = str_replace('<b>', '</comment><error>', $extracted);
                    $extracted = str_replace('</b>', '</error><comment>', $extracted);
                    $this->ioShell->out("<comment>".html_entity_decode($extracted, ENT_QUOTES)."</comment>");
                }
            }
            $choice = $this->ioShell->askChoice(
                __d(
                    'traduction_shell',
                    "Une traduction pour : \"{0}\" \"{1}\" \"{2}\"{3} a été trouvé"
                    ." dans le po mais pas dans le pot, souhaitez-vous conserver cette traduction ?",
                    $domain,
                    $context,
                    $id,
                    $comment
                ),
                ['y', 'n', '__cmd']
            );
            if ($choice === 'n') {
                unset($po[$context][$id]);
                $hasChanged = true;
            }

            if ($choice === '__cmd') {
                $cmd = $this->ioShell->askChoice(
                    __d('traduction_shell', "Quelle commande souhaitez-vous entrer ?"),
                    ['help', 'prev', 'next', 'save', 'skip', 'exit', 'repeat'],
                    'help'
                );
                switch ($this->doCommand($cmd, $i, $po, $savePath)) {
                    case 'continue':
                        continue 2;
                    case 'break':
                        break 2;
                }
            }
        }
        return $hasChanged;
    }

    /**
     * Ajoute les traductions manquante au po avec gestion d'un système de commande
     * @param array  $toAdd
     * @param array  $po
     * @param string $savePath
     * @param int    $i
     * @return bool Si modification
     * @throws Exception
     */
    protected function addMissingsTraductions(array $toAdd, array $po, string $savePath, int $i = 0): bool
    {
        $hasChanged = false;
        $c = count($toAdd);
        for (; $i < $c; $i++) {
            $this->ioShell->out(sprintf("<info>%s / %s</info>", $i+1, $c));
            $input = call_user_func_array($toAdd[$i]['callback'], $toAdd[$i]['args']);
            if (preg_match('/^__cmd(?: (.*))?$/', $input, $match)) {
                $cmd = isset($match[1]) ? strtolower(trim($match[1])) : '';
                switch ($this->doCommand($cmd, $i, $po, $savePath)) {
                    case 'continue':
                        continue 2;
                    case 'break':
                        break 2;
                }
            } else {
                $this->ioShell->out('<success>'.$input.'</success>');
                $toAdd[$i]['ref'] = $input;
                $hasChanged = true;
            }
            if ($i === $c -1 && $this->ioShell->askChoice(
                __d(
                    'traduction_shell',
                    "Vous confirmez la dernière traduction ?"
                ),
                ['y', 'n'],
                'y'
            ) === 'n'
            ) {
                $i--;
            }
        }
        return $hasChanged;
    }

    /**
     * Commande custom permettant d'effectuer différentes actions
     * comme la sauvegarde, le retour en arrière ou encore le passage au fichier
     * suivant
     * @param string $cmd
     * @param int    $i
     * @param array  $po
     * @param string $savePath
     * @return string
     * @throws Exception
     */
    protected function doCommand(string $cmd, int &$i, array $po, string $savePath): string
    {
        if ($cmd === 'help') {
            $this->outputCmdHelp();
            $i--;
        } elseif ($cmd === 'prev') {
            if ($i > 0) {
                $i -= 2;
            } else {
                $this->ioShell->out(__d('traduction_shell', '<error>Impossible de revenir en arrière !</error>'));
                $i--;
            }
        } elseif ($cmd === 'next') {
            return 'continue';
        } elseif ($cmd === 'save') {
            $newPo = $this->renderPo($po);
            $this->createFileWithDefault($savePath, $newPo, 'y');
            $i--;
        } elseif ($cmd === 'skip') {
            return 'break';
        } elseif ($cmd === 'exit') {
            $newPo = $this->renderPo($po);
            $this->createFileWithDefault($savePath, $newPo, 'y');
            exit(0);
        } elseif ($cmd === 'repeat') {
            $i--;
        } else {
            $this->ioShell->out(
                __d(
                    'traduction_shell',
                    "<error>La commande ''{0}'' n'existe pas, essayez la commande help</error>",
                    $cmd
                )
            );
            $i--;
        }
        return '';
    }

    /**
     * Demande à l'utilisateur de traduire une phrase dans
     * un domain/context particulier
     *
     * @param string $domain
     * @param string $context
     * @param string $id
     * @param string $comment
     * @return string
     */
    private function completeStr(string $domain, string $context, string $id, string $comment): string
    {
        exec(sprintf('printf "%%s" "%s" | xclip -sel clip >/dev/null 2>&1', trim(addcslashes($id, '`"'), "'")));
        if (preg_match('/^#: ([^:]+):(\d+)/', $comment, $matches)) {
            if (is_readable($filename = APP . $matches[1])) {
                $extracted = ContextDebugger::getContext($filename, (int)$matches[2], 3);
                $extracted = str_replace('<b>', '</comment><error>', $extracted);
                $extracted = str_replace('</b>', '</error><comment>', $extracted);
                $this->ioShell->out("<comment>".html_entity_decode($extracted, ENT_QUOTES)."</comment>");
            }
        }
        return $this->ioShell->ask(
            __d(
                'traduction_shell',
                "Traduisez : \"{0}\" \"{1}\" \"{2}\" situé dans {3}",
                $domain,
                $context,
                $id,
                $comment
            ),
            $id
        );
    }

    /**
     * Fait l'inverse de parsePo, transforme l'array en string
     *
     * @param array $po
     * @return string
     */
    private function renderPo(array $po): string
    {
        $newPo = '';
        if (!empty($po['__header__'])) {
            $newPo .= $po['__header__'];
        }
        unset($po['__header__']);
        foreach ($po as $context => $ids) {
            foreach ($ids as $params) {
                $newPo .= $params['comments']
                    ? PHP_EOL.implode(PHP_EOL, $params['comments']).PHP_EOL
                    : PHP_EOL;
                unset($params['comments']);
                if ($context !== '__default__') {
                    $newPo .= 'msgctxt "'.$context.'"'.PHP_EOL;
                }
                foreach ($params as $key => $value) {
                    $newPo .= 'msg'.$key.' "'.$value.'"'.PHP_EOL;
                }
            }
        }
        return $newPo;
    }

    /**
     * Aide affichée lorsque on tape "__cmd help"
     */
    protected function outputCmdHelp()
    {
        $this->ioShell->out("<info>".__d('traduction_shell', 'help : affiche ce message')."</info>");
        $this->ioShell->out("<info>".__d('traduction_shell', 'prev : Refait la traduction précédente')."</info>");
        $this->ioShell->out(
            "<info>"
            .__d('traduction_shell', 'next : Passe au message suivant sans traduire le message actif')
            ."</info>"
        );
        $this->ioShell->out(
            "<info>"
            .__d('traduction_shell', 'skip : Passe au fichier de traduction suivant (avec sauvegarde)')
            ."</info>"
        );
        $this->ioShell->out("<info>".__d('traduction_shell', 'save : Sauvegarde le travail accompli')."</info>");
        $this->ioShell->out("<info>".__d('traduction_shell', 'exit : Sauvegarde et fin au shell')."</info>");
        $this->ioShell->out("<info>".__d('traduction_shell', 'repeat : Recommence la traduction active')."</info>");
    }
}
