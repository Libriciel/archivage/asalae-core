<?php
/**
 * AsalaeCore\Command\UpdateCommand
 * @noinspection PhpDeprecationInspection WebservicesTable deprecated
 */

namespace AsalaeCore\Command;

use AsalaeCore\Model\Table\AcosTable;
use AsalaeCore\Model\Table\ArosAcosTable;
use AsalaeCore\Model\Table\ArosTable;
use AsalaeCore\Model\Table\RolesTable;
use AsalaeCore\Model\Table\UsersTable;
use AsalaeCore\Model\Table\WebservicesTable;
use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Exception;

/**
 * Facilite la mise à jour de l'application
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2017, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 * @property AcosTable $Acos
 * @property ArosAcosTable $Permissions
 * @property ArosTable $Aros
 * @property RolesTable Roles
 * @property UsersTable Users
 * @property WebservicesTable Webservices
 */
class UpdateCommand extends Command
{
    /**
     * Traits
     */
    use CreateAcosTrait;

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = new ConsoleOptionParser();
        $parser->setDescription(
            '*************************************************************' . PHP_EOL
            . __("Met à jour les permissions") . PHP_EOL
            . '*************************************************************'
        );
        $parser->addOption(
            'datasource',
            [
                'help' => __("Datasource utilisé"),
                'default' => 'default',
            ]
        );
        return $parser;
    }

    /**
     * Main command
     * @param Arguments $args
     * @param ConsoleIo $io
     * @throws Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        TableRegistry::getTableLocator()->clear();
        $io->out(__("Mise à jours des permissions..."));

        $pathToControllersJson = Configure::read('App.paths.controllers_rules');
        $pathToApisJson = Configure::read('App.paths.apis_rules');
        if (!is_readable($pathToControllersJson)) {
            throw new Exception(
                __(
                    "l'utilisateur courant n'a pas accès à controllers.json en lecture!"
                )
            );
        }
        if (!is_readable($pathToApisJson)) {
            throw new Exception(
                __("l'utilisateur courant n'a pas accès à api.json en lecture!")
            );
        }
        $opts = ['connectionName' => $args->getOption('datasource')];
        $this->Acos = $this->fetchTable('Acos', $opts);
        $conn = $this->Acos->getConnection();
        $conn->begin();

        $this->Aros = $this->fetchTable('Aros', $opts);
        $this->Permissions = $this->fetchTable('ArosAcos', $opts);
        $controllersAco = $this->createControllerAcos($opts);
        $apiAco = $this->createApiAcos($opts);
        $controllers = json_decode(
            file_get_contents($pathToControllersJson),
            true
        );
        $apis = json_decode(file_get_contents($pathToApisJson), true) ?? [];
        $accesParDefaut = array_merge(
            $this->extractAccesParDefaut($controllers, $controllersAco),
            $this->extractAccesParDefaut($apis, $apiAco)
        );

        $this->Aros = $this->fetchTable('Aros', $opts);
        $this->Permissions = $this->fetchTable('ArosAcos', $opts);
        $this->Users = $this->fetchTable('Users', $opts);
        $this->Roles = $this->fetchTable('Roles', $opts);
        $this->Webservices = $this->fetchTable('Webservices', $opts);

        // suppression des aros excédentaire
        $this->deleteOrphanAros();

        // ajout des aros manquant
        $this->addMissingAros($accesParDefaut);

        $conn->commit();

        $io->out("Update done");
    }

    /**
     * Donne la liste des acos avec accesParDefaut
     * @param array           $controllers
     * @param EntityInterface $rootAco
     * @return array
     */
    private function extractAccesParDefaut(array $controllers, EntityInterface $rootAco): array
    {
        $accesParDefaut = [];
        foreach ($controllers as $controller => $actions) {
            foreach ($actions as $action => $params) {
                $params += [
                    'accesParDefaut' => null,
                    'commeDroits' => null,
                ];
                if ($params['accesParDefaut'] !== '1') {
                    continue;
                }
                $model = $controller;
                if ($action === 'default' && $rootAco->get('alias') === 'api') {
                    $action = $model;
                    $model = 'api';
                }
                $aco = $this->Acos->find()
                    ->select(['id'])
                    ->where(
                        [
                            'model' => $model,
                            'alias' => $action,
                            'lft >' => $rootAco->get('lft'),
                            'rght <' => $rootAco->get('rght'),
                        ]
                    )
                    ->first();
                if ($aco) {
                    $accesParDefaut[] = $aco;
                }
            }
        }
        return $accesParDefaut;
    }

    /**
     * Supprime les aros qui ont un model/foreign_key qui pointe nul part
     * @return void
     */
    private function deleteOrphanAros()
    {
        foreach ($this->Aros->find() as $aro) {
            $model = $aro->get('model');
            $Table = $this->$model;
            if ($Table instanceof Table
                && !$Table->exists(['id' => $aro->get('foreign_key')])
            ) {
                $this->Aros->delete($aro);
            }
        }
    }

    /**
     * Ajout les aros manquant
     * @param array $accesParDefaut
     * @return void
     */
    private function addMissingAros(array $accesParDefaut)
    {
        foreach (['Roles', 'Webservices', 'Users'] as $model) {
            $Table = $this->$model;
            if (get_class($Table) === Table::class || !$Table instanceof Table) {
                continue;
            }
            $order = $model === 'Roles' ? 'lft' : 'id';
            foreach ($Table->find()->order($order) as $entity) {
                $data = [
                    'model' => $model,
                    'foreign_key' => $entity->get('id'),
                ];
                $aro = $this->Aros->find()
                    ->where(
                        ['model' => $model, 'foreign_key' => $entity->get('id')]
                    )
                    ->first();
                if (!$aro) {
                    $parentNode = method_exists($entity, 'parentNode')
                        ? $entity->parentNode()
                        : null;

                    if ($parentNode) {
                        $data['parent_id'] = $this->Aros->find()
                            ->select(['id'])
                            ->where($parentNode)
                            ->firstOrFail()
                            ->get('id');
                    }
                    $aro = $this->Aros->newEntity(
                        $data + [
                            'alias' => $entity->get('username')
                                ?: $entity->get('name')
                        ]
                    );
                    $this->Aros->saveOrFail($aro);
                }
                foreach ($accesParDefaut as $aco) {
                    $perm = $this->Permissions->findOrCreate(
                        [
                            'aco_id' => $aco->get('id'),
                            'aro_id' => $aro->get('id')
                        ]
                    );
                    $this->Permissions->patchEntity(
                        $perm,
                        [
                            '_create' => '1',
                            '_read' => '1',
                            '_update' => '1',
                            '_delete' => '1',
                        ]
                    );
                    $this->Permissions->saveOrFail($perm);
                }
            }
        }
    }
}
