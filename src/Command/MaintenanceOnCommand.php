<?php
/**
 * AsalaeCore\Command\MaintenanceOnCommand
 */

namespace AsalaeCore\Command;

use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use DateTime;
use Exception;

/**
 * Gestion du mode maintenance ON
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2024, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class MaintenanceOnCommand extends AbstractMaintenance
{
    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'maintenance on';
    }

    /**
     * Action principale
     * @param Arguments $args The command arguments.
     * @param ConsoleIo $io   The console io
     * @throws Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $config = $this->getConfig();
        $config['enabled'] = true;

        if (isset($config['scheduled']['end']) && new DateTime($config['scheduled']['end']) < new DateTime()) {
            unset($config['scheduled']);
        }

        $this->setConfig($config);
        $io->success('Maintenance is ON');
    }
}
