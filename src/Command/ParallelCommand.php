<?php
/**
 * AsalaeCore\Command\ParallelCommand
 */

namespace AsalaeCore\Command;

use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Exception;
use Throwable;

/**
 * Utilisé par l'utilitaire Parallel pour lancer des fonctions
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2022, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ParallelCommand extends Command
{
    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'parallel';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = parent::getOptionParser();
        $parser->addArgument(
            'base64_serialized_function',
            [
                'help' => __("Callable serializable à éxécuter"),
                'required' => true,
            ]
        );
        $parser->addArgument(
            'base64_serialized_params',
            [
                'help' => __("Arguments à donner à {0}", 'base64_serialized_function'),
            ]
        );
        return $parser;
    }

    /**
     * Action principale
     * @param Arguments $args The command arguments.
     * @param ConsoleIo $io   The console io
     * @throws Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $errors = [];
        $e = null;
        $result = null;
        set_error_handler(
            function ($errno, $errstr, $errfile, $errline) use (&$errors) {
                $errors[] = compact('errno', 'errstr', 'errfile', 'errline');
                return null;
            }
        );
        try {
            $fn = $args->getArgument('base64_serialized_function');
            $fn = unserialize(base64_decode($fn));
            $params = $args->getArgument('base64_serialized_params');
            if ($params) {
                $params = unserialize(base64_decode($params));
            }
            $result = call_user_func_array($fn, $params ?: []);
        } catch (Throwable $e) {
        }
        restore_error_handler();
        $io->out(
            base64_encode(
                serialize(
                    [
                        'result' => $result,
                        'errors' => $errors,
                        'exception' => $e,
                    ]
                )
            )
        );
        if ($e) {
            $io->abort($e->getMessage());
        }
    }

    /**
     * Transforme le code de la réponse de cette commande en array
     * @param string $response
     * @return array ['result' => ..., 'errors' => ..., 'exception' => ...]
     */
    public static function parseResponse(string $response): array
    {
        return self::decode(trim($response));
    }

    /**
     * Serialize et encode $data
     * @param mixed $data
     * @return string
     */
    public static function encode($data): string
    {
        return base64_encode(serialize($data));
    }

    /**
     * Décode et unserialize une chaîne donné en $b64
     * @param string $b64
     * @return mixed
     */
    public static function decode(string $b64)
    {
        return unserialize(base64_decode($b64));
    }
}
