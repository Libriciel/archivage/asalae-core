<?php
/**
 * AsalaeCore\Command\DatabaseFixturesCommand
 */

namespace AsalaeCore\Command;

use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Core\App;
use Cake\Core\Configure;
use Cake\I18n\FrozenDate;
use Cake\I18n\FrozenTime;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\TwigView\View\TwigView;
use DateTime;
use Exception;

/**
 * Transforme la base courante en fixtures
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2021, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class DatabaseFixturesCommand extends Command
{
    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'database fixtures';
    }
    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = new ConsoleOptionParser();
        $parser->setDescription(
            __("Création des fixtures à partir d'une base de données")
        );
        $parser->addOption(
            'datasource',
            [
                'help' => __(
                    "Datasource sur lequel travailler (supprimera les "
                    . "données existantes)"
                ),
                'default' => 'fixtures',
            ]
        );
        return $parser;
    }

    /**
     * Génère des fixtures (dans le dossier tmp/Fixture)
     * @param Arguments $args
     * @param ConsoleIo $io
     * @throws Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $paths = App::classPath('Model/Table');
        $models = glob($paths[0] . '*Table.php');
        $loc = TableRegistry::getTableLocator();
        $opt = ['connectionName' => $args->getOption('datasource')];

        $twigView = new TwigView;
        $twigView->initialize();
        $twigView->disableAutoLayout();
        $twigView->setTemplate('Fixtures/tmpl');

        if (!is_dir(TMP . 'Fixture')) {
            mkdir(TMP . 'Fixture', 0777, true);
        }
        foreach ($models as $file) {
            $name = basename($file, 'Table.php');
            $io->out(__("Table {0}...", $name));
            $loc->clear();
            $model = $loc->get($name, $opt);
            $query = $model->find()
                ->disableHydration()
                ->order($model->getPrimaryKey());
            $twigView->set(
                [
                    'clearTable' => in_array($name, ['Acos', 'Aros']),
                    'namespace' => Configure::read('App.namespace') . '\Test\Fixture',
                    'name' => $name,
                    'records' => $this->queryToRecords($query, $model),
                ]
            );
            file_put_contents(
                TMP . 'Fixture/' . $name . 'Fixture.php',
                $twigView->render()
            );
            $io->success('written ' . TMP . 'Fixture/' . $name . 'Fixture.php');
        }
        $io->out('done');
    }

    /**
     * Génère le code php issue des enregistrements en base
     * @param Query $query
     * @param Table $model
     * @return string
     */
    private function queryToRecords(Query $query, Table $model)
    {
        $records = '';
        $prev = 0;
        $replace = [
            trim(json_encode(Configure::read('App.paths.data'), JSON_UNESCAPED_SLASHES), '"'),
            Configure::read('App.paths.data'),
            ROOT,
            trim(json_encode(ROOT, JSON_UNESCAPED_SLASHES), '"'),
            gethostname(),
        ];
        $by = [
            trim(json_encode(sys_get_temp_dir(), JSON_UNESCAPED_SLASHES), '"'),
            sys_get_temp_dir(),
            '/var/www/html',
            trim(json_encode('/var/www/html', JSON_UNESCAPED_SLASHES), '"'),
            'testhost',
        ];
        foreach ($query as $data) {
            $records .= "\n            [";
            // non autoincremental et not null
            if ($model->getAlias() !== 'Sessions') {
                $id = $data[$model->getPrimaryKey()];
                $records .= "// id=" . $id;
                if (is_int($id) && $id > $prev + 1) {
                    $records .= ' ----------------------------- missing id ------------------------------------';
                }
                $prev = $id;
                unset($data[$model->getPrimaryKey()]);
            }
            foreach ($data as $key => $value) {
                if (is_resource($value)) {
                    $value = stream_get_contents($value);
                }
                if ($value instanceof FrozenDate) {
                    $value = $value->format('Y-m-d');
                    $value = "'$value'";
                } elseif ($value instanceof DateTime || $value instanceof FrozenTime) {
                    $value = $value->format('Y-m-d\TH:i:s');
                    $value = "'$value'";
                } elseif (is_string($value)) {
                    $value = addcslashes($value, "'");
                    $value = str_replace($replace, $by, $value);
                    $value = "'$value'";
                    if (preg_match('/\{\{CONST:([^}]+)}}/', $value, $m)) {
                        $value = str_replace($m[0], "' . $m[1] . '", $value);
                        // remplace: `'test' => '' . CONSTANTE` par `'test' => CONSTANTE`
                        // remplace: `'test' => CONSTANTE . '' . 'test'` par `CONSTANTE . 'test'`
                        $value = str_replace("'' . ", '', $value);
                    }
                } elseif ($value === true) {
                    $value = 'true';
                } elseif ($value === false) {
                    $value = 'false';
                } elseif ($value === null) {
                    $value = 'null';
                } elseif (is_float($value)) {
                    $value = str_replace(',', '.', (string)$value);
                } elseif (!is_numeric($value)) {
                    $this->io->warning(__("Champ {0}.{1} ignoré", $model->getAlias(), $key));
                    continue;
                }
                $records .= "\n                '$key' => $value,";
                if ($key === 'aro_id') {
                    $alias = $this->fetchTable('Aros')->get($value)->get('alias');
                    $records .= " // $alias";
                }
                if ($key === 'aco_id') {
                    $Acos = $this->fetchTable('Acos');
                    $aco = $Acos->get($value);
                    $path = $Acos->find('list', ['valueField' => 'alias'])
                        ->where(['lft <=' => $aco->get('lft'), 'rght >=' => $aco->get('rght')])
                        ->order(['lft'])
                        ->toArray();
                    $alias = implode('/', $path);
                    $records .= " // $alias";
                }
                if ($key === 'parent_id' && $model->getAlias() === 'Acos' && is_numeric($value)) {
                    $Acos = $Acos ?? $this->fetchTable('Acos');
                    $aco = $Acos->get($value);
                    $path = $Acos->find('list', ['valueField' => 'alias'])
                        ->where(['lft <=' => $aco->get('lft'), 'rght >=' => $aco->get('rght')])
                        ->order(['lft'])
                        ->toArray();
                    $alias = implode('/', $path);
                    $records .= " // $alias";
                }
            }
            $records .= "\n            ],";
        }
        return $records;
    }
}
