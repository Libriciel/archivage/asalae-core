<?php
/**
 * AsalaeCore\Command\JobViewCommand
 */

namespace AsalaeCore\Command;

use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use DateTimeInterface;
use Exception;

/**
 * Permet de visualiser un job
 * ex: bin/cake job view 4531
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2021, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class JobViewCommand extends Command
{
    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'job view';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = parent::getOptionParser();
        $parser->addArgument(
            'jobid',
            [
                'help' => __("Identifiant beanstalkd du job"),
                'required' => true,
            ]
        );
        return $parser;
    }

    /**
     * Action principale
     * @param Arguments $args The command arguments.
     * @param ConsoleIo $io   The console io
     * @throws Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $Jobs = TableRegistry::getTableLocator()->get('BeanstalkJobs');
        $entity = $Jobs->find()
            ->where(['BeanstalkJobs.jobid' => $args->getArgument('jobid')])
            ->contain(['Users'])
            ->first();
        if (!$entity) {
            $io->abort(
                __(
                    "Le job (jobid={0}) n'a pas été trouvé en base de données",
                    $args->getArgument('jobid')
                )
            );
        }
        $created = $entity->get('created');
        if ($created instanceof DateTimeInterface) {
            $created = $created->format(DateTimeInterface::RFC3339);
        }
        $user = Hash::get($entity, 'user.username');
        if ($name = Hash::get($entity, 'user.name')) {
            $user .= " ($name)";
        }
        $arr = [
            'jobid' => $entity->get('jobid'),
            'id' => $entity->get('id'),
            'tube' => $entity->get('tube'),
            'priority' => $entity->get('priority'),
            'last_status' => $entity->get('last_status'),
            'created' => $created,
            'user' => $user,
            'delay' => $entity->get('delay'),
            'ttr' => $entity->get('ttr'),
            'errors' => $entity->get('errors'),
            '----------------------' => '----------------------',
            __("Champs beanstalkd:") => '',
            '---------------------- ' => '----------------------',
            'state' => $entity->get('state'),
            'data' => http_build_query($entity->get('data') ?: [], '', ', '),
        ];
        $data = [];
        foreach ($arr as $key => $value) {
            $data[] = [$key, (string)$value];
        }
        $io->helper('Table')->output($data);
    }
}
