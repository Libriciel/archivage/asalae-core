<?php
/**
 * AsalaeCore\Command\Command
 */

namespace AsalaeCore\Command;

use Cake\Command\Command as CakeCommand;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;

/**
 * Simule le daemon Clavam, renvoi toujours les fichiers sans virus détectés
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2022, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class ClamavSimulatorCommand extends CakeCommand
{
    /**
     * @var ConsoleIo
     */
    private ConsoleIo $io;

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = new ConsoleOptionParser();
        $parser->setDescription(
            __("Simule le comportement d'un clamav-daemon")
        );
        $parser->addOption(
            'will-find-virus',
            [
                'help' => __(
                    "Si cette option est donnée, trouvera des virus (pour de faux)"
                ),
                'boolean' => true,
            ]
        );
        $parser->addOption(
            'port',
            [
                'help' => __("Port de connexion TCP"),
                'default' => 5353,
            ]
        );
        $parser->addOption(
            'time-limit',
            [
                'help' => __("Secondes avant l'arret de la commande"),
                'default' => -1,
            ]
        );
        return $parser;
    }

    /**
     * Commande principale
     * @param Arguments $args
     * @param ConsoleIo $io
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        parent::execute($args, $io);
        $this->io = $io;
        $port = $args->getOption('port');
        $server = stream_socket_server("tcp://127.0.0.1:$port");
        $io->out(__("Clamav Simulator lancé sur le port {0}", $port));
        $connections = [$server];
        $begin = microtime(true);
        $timelimit = (float)$args->getOption('time-limit');
        while ($timelimit === -1 || microtime(true) - $begin < $timelimit) {
            $reads = $connections;
            $writes = null;
            $excepts = null;
            $modified = stream_select($reads, $writes, $excepts, 5);
            if ($modified === false) {
                break;
            }
            foreach ($reads as $resource) {
                if ($resource === $server) {
                    $conn = stream_socket_accept($server);
                    fwrite($conn, "réponse du serveur\n");
                    $connections[] = $conn;
                } else {
                    $command = fread($resource, 1024);
                    $io->out('réception de la commande: '.$command);
                    if ($command && preg_match('/MULTISCAN (.*)/', $command, $m)) {
                        $this->doMultiscan($resource, rtrim($m[1], " \n/"));
                    }
                    $key = array_search($resource, $connections, true);
                    fclose($resource);
                    unset($connections[$key]);
                }
            }
        }
    }

    /**
     * Simule un multiscan
     * @param resource $resource
     * @param string   $directory
     * @return void
     */
    private function doMultiscan($resource, string $directory)
    {
        if (is_dir($directory)) {
            foreach (glob($directory . '/*') as $filename) {
                if (is_dir($filename)) {
                    $this->doMultiscan($filename);
                } else {
                    $msg = sprintf("%s: asalae.fake.virus FOUND\n", $filename);
                    fwrite($resource, $msg);
                    $this->io->out($msg, 0);
                }
            }
        }
    }
}
