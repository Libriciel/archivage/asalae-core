<?php
/**
 * AsalaeCore\Command\JobListCommand
 */

namespace AsalaeCore\Command;

use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;
use DateTimeInterface;

/**
 * Permet de liste les jobs
 * ex: bin/cake job list test --page 1
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2021, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class JobListCommand extends Command
{
    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'job list';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = parent::getOptionParser();
        $parser->addArgument(
            'tube',
            [
                'help' => __("Filtre par Tube"),
            ]
        );
        $parser->addOption(
            'state',
            [
                'help' => __("Etat du job"),
                'choices' => [
                    'ready',
                    'reserved',
                    'delayed',
                    'buried',
                ]
            ]
        );
        $parser->addOption(
            'fields',
            [
                'help' => __("Champs à afficher séparés par des virgules"),
                'default' => 'jobid,tube,priority,state,data,created,user_id,delay,ttr,errors',
            ]
        );
        $parser->addOption(
            'page',
            [
                'help' => __("Numéro de page (pagination)"),
                'default' => 1,
            ]
        );
        $parser->addOption(
            'limit',
            [
                'help' => __("Nombre d'enregistrements par page"),
                'default' => 100,
            ]
        );
        return $parser;
    }

    /**
     * Action principale
     * @param Arguments $args The command arguments.
     * @param ConsoleIo $io   The console io
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $inline = 'bin/cake job list';
        $conditions = [];
        if ($tube = $args->getArgument('tube')) {
            $inline .= ' '.$tube;
            $conditions['tube'] = $tube;
        }
        if ($state = $args->getOption('state')) {
            $inline .= ' --state '.$state;
            $conditions['last_status'] = $state;
        }
        $limit = $args->getOption('limit');
        $inline .= ' --fields '.$args->getOption('fields');
        $inline .= ' --limit '.$limit;
        $fields = explode(',', $args->getOption('fields'));
        $withoutVirtuals = array_filter(
            explode(
                ',',
                str_replace(['state', 'data'], '', $args->getOption('fields'))
            )
        );
        $withoutVirtuals[] = 'id'; // nécéssaire pour les champs virtuels

        $page = $args->getOption('page');
        $offset = $limit * ($page -1);
        $Jobs = TableRegistry::getTableLocator()->get('BeanstalkJobs');
        $count = $Jobs->find()->where($conditions)->count();
        $maxPage = ceil($count / $limit);

        $data = $Jobs->find()
            ->select($withoutVirtuals)
            ->where($conditions)
            ->offset($offset)
            ->limit($args->getOption('limit'))
            ->all()
            ->map(
                function (EntityInterface $entity) use ($fields) {
                    $out = [];
                    foreach ($fields as $k) {
                        $v = $entity->get($k);
                        if ($v instanceof DateTimeInterface) {
                            $v = $v->format('Y-m-d H:i:s');
                        }
                        if (is_array($v)) {
                            $v = http_build_query($v, '', ', ');
                        }
                        $out[$k] = (string)$v;
                    }
                    return $out;
                }
            )
            ->toArray();
        array_unshift($data, $fields);

        $io->helper('Table')->output($data);
        $io->out(
            __(
                "Affichage des résultats de {0} à {1} sur un total de {2} jobs",
                $offset,
                $offset + count($data) -1,
                $count
            )
        );
        if ($page > 1) {
            $io->warning(__("Page précédente:"));
            $io->info($inline.' --page '.($page -1));
        }
        if ($page < $maxPage) {
            if ($page > 1) {
                $io->out();
            }
            $io->warning(__("Page suivante:"));
            $io->info($inline.' --page '.($page +1));
        }
    }
}
