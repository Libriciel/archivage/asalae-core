<?php
/**
 * AsalaeCore\Command\HashDirCompareCommand
 */

namespace AsalaeCore\Command;

use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Core\Configure;
use Exception;

/**
 * Donne la liste des fichiers modifiés
 * code 1 si un fichier est différent
 * code 0 si aucunes différence
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2021, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class HashDirCompareCommand extends Command
{
    /**
     * @var Arguments
     */
    public $args;
    /**
     * @var ConsoleIo
     */
    public $io;
    /**
     * @var bool garde en mémoire le résultat si true
     */
    private $noOutput = false;

    /**
     * @var string si noOutput, la sorti est redirigé dans cet attribut;
     */
    private $out = '';

    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'hash_dir compare';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parserCompare = new ConsoleOptionParser;
        $parserCompare->addArgument(
            'filename',
            ['help' => __("Fichier cible")]
        );
        $parserCompare->addOption(
            'basedir',
            [
                'help' => __("Dossier racine"),
                'default' => getcwd(),
            ]
        );
        return $parserCompare;
    }

    /**
     * Main command
     * @param Arguments $args
     * @param ConsoleIo $io
     * @return int|null
     * @throws Exception
     */
    public function execute(Arguments $args, ConsoleIo $io): ?int
    {
        $this->io = $io;
        $this->args = $args;
        $filename = $args->getArgument('filename');
        if (!$filename) {
            $filename = Configure::read(
                'HashDir.defaultHashFilename',
                RESOURCES.'hashes.txt'
            );
        }
        if (!is_file($filename)) {
            throw new Exception('file not found: '.$filename);
        }

        // parse le fichier
        $file = file($filename, FILE_IGNORE_NEW_LINES);

        // appel de hash_dir pour obtenir la liste des fichiers actuel avec leurs hashs
        $this->getHashsList($file);

        $missing = [];
        $diff = [];
        $added = [];
        $c = count($file);
        for ($i = 5; $i < $c; $i++) {
            if (!strpos($file[$i], ':') || strpos($this->out, $file[$i]) !== false) {
                continue;
            }
            [$filename, $hash] = explode(':', $file[$i]);
            $pos = strpos($this->out, $filename.':');
            if (!$pos) {
                $missing[] = $filename;
            } else {
                $nextNL = strpos($this->out, PHP_EOL, $pos +1);
                $diff[$filename] = [
                    'expected' => $hash,
                    'actual' => substr(
                        substr(
                            $this->out,
                            $pos,
                            $nextNL - $pos
                        ),
                        strlen($filename) +1
                    )
                ];
            }
        }
        $new = implode(PHP_EOL, $file);
        foreach (explode(PHP_EOL, $this->out) as $out) {
            if (!strpos($out, ':') || strpos($new, $out) !== false) {
                continue;
            }
            [$filename] = explode(':', $out);
            if (!strpos($new, $filename.':')) {
                $added[] = $filename;
            }
        }
        $this->outputFile = false;
        if (empty($missing) && empty($diff) && empty($added)) {
            $io->success('OK');
        } else {
            if (!empty($missing)) {
                $io->warning(__("Les fichiers suivants sont manquants:"));
                foreach ($missing as $m) {
                    $io->out('    '.$m);
                }
                $io->hr();
            }
            if (!empty($diff)) {
                $io->warning(__("Les fichiers suivants sont différents (<hash_attendu>:<hash_actuel>):"));
                foreach ($diff as $filename => $hashes) {
                    $io->out('    '.$filename.' ('.$hashes['expected'].':'.$hashes['actual'].')');
                }
                $io->hr();
            }
            if (!empty($added)) {
                $io->warning(__("Les fichiers suivants sont en trop:"));
                foreach ($added as $m) {
                    $io->out('    '.$m);
                }
                $io->hr();
            }
            $io->abort(__("Une différence a été détectée"));
        }

        return self::CODE_SUCCESS;
    }

    /**
     * Affiche le résultat, de façon récursive ou pas
     * @param string $dir
     * @param int    $ltrimAmount
     */
    private function outputDir(string $dir, int $ltrimAmount)
    {
        foreach (glob($dir.DS.'*') as $filename) {
            if (is_dir($filename)) {
                if ($this->args->getOption('recursive')) {
                    $this->outputDir($filename, $ltrimAmount);
                }
            } else {
                $this->outputFile($filename, $ltrimAmount);
            }
        }
    }

    /**
     * Affiche un fichier (fichier -> hash)
     * @param string $filename
     * @param int    $ltrimAmount
     */
    private function outputFile(string $filename, int $ltrimAmount)
    {
        $truncatedFilename = substr($filename, $ltrimAmount);
        $filter = false;
        foreach ($this->args->getOption('filter') ?: [] as $f) {
            if (fnmatch($f, $truncatedFilename)) {
                $filter = true;
                break;
            }
        }
        if (!$filter) {
            $this->output(
                $truncatedFilename
                .':'.hash_file($this->args->getOption('hash-algo'), $filename)
            );
        }
    }

    /**
     * Affichage conditionnel de la sortie
     * @param string $out
     */
    private function output(string $out)
    {
        if ($this->noOutput) {
            $this->out .= $out . str_repeat(PHP_EOL, 1);
        } elseif ($this->args->getOption('output')) {
            fwrite($this->outputFile, $out . str_repeat(PHP_EOL, 1));
        } else {
            $this->io->out($out);
        }
    }

    /**
     * Inscrit dans $this->noOutput la liste des fichiers et leurs hashes
     * @param array $file
     * @return void
     * @throws Exception
     */
    private function getHashsList(array $file)
    {
        // extraction des paramètres de la commande hash_dir
        $pass = unserialize(base64_decode(trim($file[2] ?? '')));
        if (!$pass) {
            $this->io->abort('unable to read params');
        }
        $this->noOutput = true;

        $hashCommand = new HashDirCommand;
        $hashCommand->noOutput = true;
        $hcArgs = new Arguments(
            $pass['args'],
            ['basedir' => $this->args->getOption('basedir')] + $pass['options'],
            []
        );
        $hashCommand->execute($hcArgs, $this->io);
        $this->args = $hcArgs; // pour les fonctions outputFile et outputDir

        $dirs = $hcArgs->getArguments();
        sort($dirs);
        $this->out = '';
        $trim = strlen($this->args->getOption('basedir').DS);
        foreach ($dirs as $dir) {
            $d = $this->args->getOption('basedir').DS.trim($dir, DS);
            if (is_file($d)) {
                $this->outputFile($d, $trim);
            } elseif (is_dir($d)) {
                $this->outputDir($d, $trim);
            }
        }
    }
}
