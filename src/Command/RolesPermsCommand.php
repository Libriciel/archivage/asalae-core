<?php
/**
 * AsalaeCore\Command\RolesPermsCommand
 */

namespace AsalaeCore\Command;

use Acl\Model\Entity\Aco as Aco;
use Acl\Model\Entity\Aro as Aro;
use Acl\Model\Entity\Permission as ArosAco;
use AsalaeCore\Application;
use AsalaeCore\Controller\ApiInterface;
use AsalaeCore\Controller\Controller;
use AsalaeCore\Model\Entity\Role;
use AsalaeCore\Model\Table\AcosTable;
use AsalaeCore\Model\Table\ArosAcosTable;
use AsalaeCore\Model\Table\ArosTable;
use AsalaeCore\Model\Table\RolesTable;
use Cake\Command\Command;
use Cake\Console\ConsoleOptionParser;
use Cake\Core\Configure;
use Cake\Database\Expression\IdentifierExpression;
use Cake\Utility\Hash;
use ErrorException;
use Exception;
use Throwable;

/**
 * Permet d'exporter/importer les permissions liés aux rôles globaux
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2018, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 * @property RolesTable $Roles
 * @property AcosTable $Acos
 * @property ArosTable $Aros
 * @property ArosAcosTable $Permissions
 */
class RolesPermsCommand extends Command
{
    /**
     * Traits
     */
    use FileTrait, CommandShellTrait;

    /**
     * Export des roles
     * @throws Exception
     */
    public function export()
    {
        $this->Roles = $this->fetchTable('Roles');
        $this->Acos = $this->fetchTable('Acos');
        $query = $this->Roles->find()
            ->where(['Roles.org_entity_id IS' => null])
            ->contain(['Aros' => ['ArosAcos' => ['Acos']]])
            ->order(['Roles.name']);
        $export = [];
        /** @var Role $role */
        foreach ($query as $role) {
            $export[$role->get('name')] = [];
            /** @var ArosAco $perm */
            foreach (Hash::get($role, 'aro.aros_acos', []) as $perm) {
                $access = '';
                foreach (['_create', '_read', '_update', '_delete'] as $crud) {
                    $access .= $perm->get($crud) === '1' ? '1' : '0';
                }
                $q = $this->Acos->find()
                    ->select(['parents.alias'])
                    ->where(['Acos.id' => $perm->get('aco_id')])
                    ->innerJoin(
                        ['parents' => 'acos'],
                        [
                            'parents.lft <=' => new IdentifierExpression(
                                'Acos.lft'
                            ),
                            'parents.rght >=' => new IdentifierExpression(
                                'Acos.rght'
                            ),
                        ]
                    )
                    ->orderAsc('parents.lft')
                    ->all()
                    ->map(
                        function (Aco $entity) {
                            return $entity->get('parents')['alias'];
                        }
                    );
                $path = implode('/', $q->toArray());
                $export[$role->get('name')][$path] = $access;
            }
        }
        $controllers = [];
        $apis = [];
        $basePath = Configure::read('App.namespace') . '\\Controller\\';

        $appControllerMethods = class_exists($basePath . "AppController")
            ? get_class_methods($basePath . "AppController")
            : [];
        if (!$appControllerMethods) {
            $appControllerMethods = get_class_methods($basePath . "Controller");
        }
        foreach (Application::getControllers() as $controllerClass) {
            /** @var string|Controller $controllerClass */
            $methods = get_class_methods($controllerClass);
            $arrayName = explode('\\', $controllerClass);
            $name = end($arrayName);
            $controllerName = substr(
                $name,
                0,
                strlen($name) - strlen('Controller')
            );

            // get_class_methods() envoi les methodes protégés dans ce contexte
            if ($controllerName === 'Devs') {
                $methods = ['index'];
            }

            $controllers[$controllerName] = array_diff(
                $methods,
                $appControllerMethods
            );
            sort($controllers[$controllerName]);
            if (in_array(ApiInterface::class, class_implements($controllerClass))) {
                $apis[$controllerName] = $controllerClass::getApiActions();
            }
        }
        foreach ($export as $role => $actions) {
            ksort($export[$role]);
            $export[$role] = array_filter(
                $export[$role],
                function ($key) use ($controllers, $apis) {
                    if (preg_match(
                        '#^root/api/([^/]+)(?:/([^/]+))?$#',
                        $key,
                        $m
                    )
                    ) {
                        $controller = $m[1];
                        $action = $m[2] ?? 'default';
                        if ($action === 'default') {
                            return isset($apis[$controller]);
                        }
                        return isset($apis[$controller]) && in_array(
                            $action,
                            $apis[$controller]
                        );
                    }
                    if (preg_match(
                        '#^root/controllers/([^/]+)/([^/]+)$#',
                        $key,
                        $m
                    )
                    ) {
                        return isset($controllers[$m[1]]) && in_array(
                            $m[2],
                            $controllers[$m[1]]
                        );
                    }
                    return true;
                },
                ARRAY_FILTER_USE_KEY
            );
        }
        do {
            $file = $this->in(
                __("Sélectionner un fichier de sortie"),
                null,
                TMP . 'export_roles.json'
            );
            $writtable = true;
            if (!is_writable(dirname($file))) {
                $writtable = false;
                $this->err(__("Impossible d'écrire sur {0}", $file));
            }
        } while (!$writtable);
        $json = json_encode(
            $export,
            ($this->param('pretty-print') ? JSON_PRETTY_PRINT : 0)
            | JSON_UNESCAPED_SLASHES
        );
        $this->createFileWithDefault($file, $json, 'y');
    }

    /**
     * Import des permissions de roles
     * @param null|string $filename
     * @throws ErrorException
     */
    public function import($filename = null)
    {
        while (empty($filename) || !is_readable($filename)) {
            $filename = $this->in(
                __("Veuillez saisir le chemin vers le fichier d'import")
            );
            if (!is_readable($filename)) {
                $this->err(__("Impossible de lire le fichier {0}", $filename));
                $filename = null;
            }
        }
        $opts = ['connectionName' => $this->param('datasource')];
        $import = json_decode(file_get_contents($filename), true);
        $this->Roles = $this->fetchTable('Roles', $opts);
        $this->Aros = $this->fetchTable('Aros', $opts);
        $this->Permissions = $this->fetchTable('ArosAcos', $opts);
        $this->checkAros();
        $conn = $this->Roles->getConnection();
        $conn->begin();

        $error = false;
        $deleteAll = false;
        if ($this->param('delete')) {
            $deleteAll = true;
        }
        if ($deleteAll) {
            $ids = $this->Roles->find()
                ->innerJoinWith('Aros')
                ->select(['Aros.id'])
                ->where(
                    [
                        'Roles.name IN' => array_keys($import),
                        'org_entity_id IS' => null
                    ]
                )
                ->innerJoinWith('Aros')
                ->all()
                ->map(
                    function (Role $entity) {
                        /** @var Aro $aro */
                        $aro = $entity->get('_matchingData')['Aros'];
                        return $aro->get('id');
                    }
                )
                ->toArray();
            if (!empty($ids)) {
                $this->Permissions->deleteAll(['aro_id IN' => $ids]);
                $this->out(__("Les permissions ont été supprimées"));
            }
        }
        foreach ($import as $role => $accesses) {
            $roleEntity = $this->Roles->find()
                ->where(['Roles.name' => $role, 'org_entity_id IS' => null])
                ->contain(['Aros'])
                ->first();

            if (!$roleEntity) {
                continue;
            }

            set_error_handler(
                function ($errno, $errstr, $errfile, $errline) {
                    throw new ErrorException(
                        $errstr,
                        0,
                        $errno,
                        $errfile,
                        $errline
                    );
                }
            );
            foreach ($accesses as $acosPath => $access) {
                /** @var string|array $aro Permissions->allow() la doc indique un string
                 *                         mais la fonction autorise les array
                 */
                $aro = [
                    'foreign_key' => $roleEntity->get('id'),
                    'model' => 'Roles',
                ];
                try {
                    $this->allow($access, $aro, $acosPath);
                } catch (Throwable $e) {
                    $error = true;
                    $this->ioShell->err(
                        __(
                            "Erreur lors de l'import de {0}:{1} l'aco n'existe pas.",
                            $role,
                            $acosPath,
                            $access
                        )
                    );
                }
                $this->ioShell->verbose(
                    $role . ' : ' . $acosPath . ' : ' . $access
                );
            }
        }
        restore_error_handler();

        if ($error) {
            $conn->rollback();
            $this->ioShell->abort(__("Une erreur a été détectée, rollback..."));
        }

        $conn->commit();
        $this->out(__("L'import s'est effectué avec succès"));
    }

    /**
     * Crée la permission
     * @param string $access
     * @param array  $aro
     * @param string $acosPath
     * @return void
     */
    protected function allow(string $access, array $aro, string $acosPath): void
    {
        if ($access === '1111' || $access === '1') {
            $this->Permissions->allow($aro, $acosPath);
        } elseif ($access === '0000' || $access === '-1') {
            $this->Permissions->allow($aro, $acosPath, '*', -1);
        } else {
            $subAccesses = [
                '_create' => (bindec($access) & 8) !== 0,
                '_read' => (bindec($access) & 4) !== 0,
                '_update' => (bindec($access) & 2) !== 0,
                '_delete' => (bindec($access) & 1) !== 0,
            ];
            foreach ($subAccesses as $crud => $subAccess) {
                $this->Permissions->allow(
                    $aro,
                    $acosPath,
                    $crud,
                    $subAccess ? 1 : -1
                );
            }
        }
    }

    /**
     * Ce shell ne peux pas fonctionner si les aros sont cassés
     */
    private function checkAros()
    {
        $this->Roles = $this->fetchTable('Roles');
        $count = $this->Roles->find()
            ->where(['org_entity_id IS' => null, 'Aros.id IS' => null])
            ->leftJoinWith('Aros')
            ->count();
        if ($count) {
            $this->abortShell(__("Il y a {0} roles sans leurs aros", $count));
        }
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = parent::getOptionParser();
        $parserExport = parent::getOptionParser();
        $parserImport = parent::getOptionParser();

        $parser->addSubcommand(
            'export',
            [
                'parser' => $parserExport,
                'help' => __(
                    "Permet d'exporter les permissions au format json"
                ),
            ]
        );
        $parserExport->addOption(
            'pretty-print',
            [
                'boolean' => true,
                'help' => __("effectue l'export en mode pretty-print"),
            ]
        );

        $parser->addSubcommand(
            'import',
            [
                'parser' => $parserImport,
                'help' => __(
                    "Permet d'importer les permissions à partir d'un fichier json"
                ),
            ]
        );

        $parserImport->addOption(
            'delete',
            [
                'boolean' => true,
                'help' => __(
                    "Suppression des permissions par défaut (à faire si les permissions par défaut ont changé)"
                ),
            ]
        );
        $parserImport->addOption(
            'keep',
            [
                'boolean' => true,
                'help' => __(
                    "On ne touche pas aux permissions par défaut existantes"
                ),
            ]
        );
        $parserImport->addOption(
            'datasource',
            [
                'help' => __("Datasource utilisé"),
                'default' => 'default',
            ]
        );

        return $parser;
    }
}
