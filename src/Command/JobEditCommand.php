<?php
/**
 * AsalaeCore\Command\JobEditCommand
 */

namespace AsalaeCore\Command;

use AsalaeCore\Factory\Utility;
use Beanstalk\Utility\Beanstalk;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\ORM\TableRegistry;
use Exception;
use Pheanstalk\PheanstalkInterface;

/**
 * Permet de modifier un job
 * ex: bin/cake job edit 4531 '{"user_id":5,"foo":"bar"}'
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2021, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class JobEditCommand extends Command
{
    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'job edit';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = parent::getOptionParser();
        $parser->addArgument(
            'jobid',
            [
                'help' => __("Identifiant beanstalkd du job"),
                'required' => true,
            ]
        );
        $parser->addArgument(
            'data',
            [
                'help' => __("Data du job au format json"),
            ]
        );
        $parser->addOption(
            'priority',
            [
                'help' => __("Priorité du job"),
                'default' => PheanstalkInterface::DEFAULT_PRIORITY,
            ]
        );
        $parser->addOption(
            'delay',
            [
                'help' => __("Délai du job"),
                'default' => PheanstalkInterface::DEFAULT_DELAY,
            ]
        );
        $parser->addOption(
            'ttr',
            [
                'help' => __("Durée de la réservation"),
                'default' => PheanstalkInterface::DEFAULT_TTR,
            ]
        );
        return $parser;
    }

    /**
     * Action principale
     * @param Arguments $args The command arguments.
     * @param ConsoleIo $io   The console io
     * @throws Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $Jobs = TableRegistry::getTableLocator()->get('BeanstalkJobs');
        $entity = $Jobs->find()
            ->where(['BeanstalkJobs.jobid' => $args->getArgument('jobid')])
            ->first();
        if (!$entity) {
            $io->abort(
                __(
                    "Le job (jobid={0}) n'a pas été trouvé en base de données",
                    $args->getArgument('jobid')
                )
            );
        }
        if (!$args->getArgument('data')) {
            $io->out(
                escapeshellarg(
                    json_encode($entity->get('data'), JSON_UNESCAPED_SLASHES)
                )
            );
            return;
        } elseif (!($data = json_decode($args->getArgument('data'), true))) {
            $io->abort(__("Erreur lors du parsing json"));
        } elseif (!is_array($data)) {
            $io->abort(__("Les données doivent être sous la forme d'un tableau"));
        }

        /** @var Beanstalk $Beanstalk */
        $Beanstalk = Utility::get('Beanstalk');
        // v3 only (évite les erreurs de l'IDE en v4)
        if (!method_exists($Beanstalk, 'selectJob') || !method_exists($Beanstalk, 'done')) {
            return;
        }
        $Beanstalk->setTube($entity->get('tube'));
        if ($entity->get('state') === 'buried') {
            $Beanstalk->selectJob($entity->get('jobid'))->kick()->reserve();
        } else {
            $Beanstalk->selectJob($entity->get('jobid'))->reserve();
        }
        $Beanstalk->done();
        $prevJobid = $entity->get('jobid');
        $jobid = $Beanstalk->emit(
            $data + ['prev_jobid' => $prevJobid],
            $args->getOption('priority'),
            max(1, $args->getOption('delay')),
            $args->getOption('ttr')
        );
        $this->logEvent(
            'shell_edit_job',
            'success',
            __("Modification du jobid={0} du tube ''{1}''", $entity->get('jobid'), $entity->get('tube'))
        );
        $io->success(__("{0} modifié avec succès, le nouveau jobid est le {1}", $prevJobid, $jobid));
    }
}
