<?php
/**
 * AsalaeCore\Command\FileTrait
 */

namespace AsalaeCore\Command;

use Cake\Command\Command;
use Cake\Console\ConsoleIo;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;
use Symfony\Component\Filesystem\Exception\IOException;

/**
 * Ensemble de méthodes pour la manipulation de mot de passe
 *
 * @category    Shell
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2017, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin Command
 * @mixin CommandShellTrait
 */
trait FileTrait
{

    /**
     * Creates a file at given path
     *
     * @see Shell::createFile()
     * @param string $path     Where to put the file.
     * @param string $contents Content to put in the file.
     * @param string $default
     * @return bool Success
     * @throws Exception
     * @mixin Command
     * @mixin CommandShellTrait
     */
    private function createFileWithDefault($path, $contents, $default = 'n')
    {
        /** @noinspection PhpInstanceofIsAlwaysTrueInspection */
        if ($this instanceof Command) {
            $io = $this->ioShell;
            $force =& $this->paramsShell['force'];
            $interactive = (bool)$this->interactiveShell;
        } else {
            $io = $this->_io;
            $force =& $this->params['force'];
            $interactive = (bool)$this->interactive;
        }
        $path = str_replace(DIRECTORY_SEPARATOR . DIRECTORY_SEPARATOR, DIRECTORY_SEPARATOR, $path);

        if (!$io instanceof ConsoleIo) {
            throw new Exception(__("_io must be an instance of ConsoleIo"));
        }
        $io->out();

        if (is_file($path) && !$force && $interactive) {
            $io->out(sprintf('<warning>File `%s` exists</warning>', $path));
            $key = $io->askChoice(__('Do you want to overwrite?'), ['y', 'n', 'a', 'q'], $default);

            if (strtolower($key) === 'q') {
                $io->out(__('<error>Quitting</error>.'), 2);
                $this->_stop();
            }
            if (strtolower($key) === 'a') {
                $force = true;
                $key = 'y';
            }
            if (strtolower($key) !== 'y') {
                $io->out(__('Skip `{0}`', $path), 2);

                return false;
            }
        } else {
            $io->out(__('Creating file {0}', $path));
        }

        try {
            Filesystem::dumpFile($path, $contents);
            $io->out(__('<success>Wrote</success> `{0}`', $path));
            return true;
        } catch (IOException $e) {
            $io->err(__('<error>Could not write to `{0}`</error>.', $path), 2);
            return false;
        }
    }
}
