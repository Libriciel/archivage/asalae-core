<?php
/**
 * AsalaeCore\Command\AdminCommand
 */

namespace AsalaeCore\Command;

use AsalaeCore\Controller\Component\AclComponent;
use AsalaeCore\Exception\GenericException;
use AsalaeCore\Form\AdminShellForm;
use Cake\Command\Command;
use Cake\Console\ConsoleOptionParser;
use Cake\Controller\ComponentRegistry;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Exception;
use stdClass;

/**
 * Command d'install d'admin fonctionnel
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2019, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class AdminCommand extends Command
{
    const ACTION_ADD = 'a';
    const ACTION_EDIT = 'm';
    const ACTION_DELETE = 's';
    const ACTION_QUIT = 'q';

    /**
     * Traits
     */
    use PasswordTrait, CommandShellTrait;

    /**
     * @var array Champs affichés dans la liste d'admins ['champ' => 'traduction']
     */
    public $fields = [];

    /**
     * Chemin vers le fichier json qui conserve les données
     * @var string
     */
    protected $adminFileUri = '';

    /**
     * @var stdClass liste les administrateurs configurés
     */
    protected $admins = [];

    /**
     * @var AdminShellForm Classe de formulaire
     */
    protected $form;
    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = new ConsoleOptionParser();
        $parserWebservice = new ConsoleOptionParser();
        $parser->addSubcommand(
            'webservice',
            [
                'help' => __("Ajoute un webservice de type administrateur technique"),
                'parser' => $parserWebservice,
            ]
        );
        $parserWebservice->addArgument(
            'username',
            [
                'help' => __("Identifiant de connexion pour le webservice"),
                'required' => true,
            ]
        );
        $parserWebservice->addArgument(
            'password',
            [
                'help' => __("Mot de passe pour le webservice"),
                'required' => true,
            ]
        );

        return $parser;
    }

    /**
     * Constructs this Shell instance.
     *
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#Shell
     */
    public function __construct()
    {
        parent::__construct();
        $this->fields = [
            'username' => [
                'label' => __("Identifiant de connexion"),
                'primary' => true,
                'required' => true,
            ],
            'name' => [
                'label' => __("Nom de l'utilisateur"),
                'required' => true,
            ],
            'email' => [
                'label' => __("Courriel"),
                'email' => true,
                'required' => false,
            ],
            'password' => [
                'label' => __("Mot de passe"),
                'password' => true,
                'confirm' => 'confirm-password',
                'display' => false,
                'required' => true,
            ],
        ];
    }

    /**
     * Initialisation du shell
     */
    public function initialize(): void
    {
        parent::initialize();

        $this->adminFileUri = Configure::read(
            'App.paths.administrators_json',
            Configure::read('App.paths.data') . DS . 'administrateurs.json'
        );
        if (!is_writeable(dirname($this->adminFileUri))) {
            $this->abortShell(
                __(
                    "Le repertoire de configuration {0} n'est pas accessible en écriture",
                    $this->adminFileUri
                )
            );
        }
        $this->form = AdminShellForm::buildForm();
    }

    /**
     * liste des actions disponibles
     * @return array ['action' => ['label' => ..., 'short' => ..., 'enabled' => ...]]
     */
    protected function getActions(): array
    {
        return [
            'edit' => [
                'label' => __("Modifier"),
                'short' => self::ACTION_EDIT,
                'enabled' => !empty($this->admins),
            ],
            'add' => [
                'label' => __("Ajouter"),
                'short' => self::ACTION_ADD,
                'enabled' => true,
            ],
            'delete' => [
                'label' => __("Supprimer"),
                'short' => self::ACTION_DELETE,
                'enabled' => !empty($this->admins),
            ],
            'quit' => [
                'label' => __("Quitter"),
                'short' => self::ACTION_QUIT,
                'enabled' => true,
            ],
        ];
    }

    /**
     * Permet de choisir parmis une liste d'actions possibles
     */
    protected function selectAction()
    {
        $actions = $this->getActions();
        $message = [];
        $enabled = [];
        foreach ($actions as $params) {
            if (!$params['enabled']) {
                continue;
            }
            $message[] = $params['label'].' ('.$params['short'].')';
            $enabled[] = $params['short'];
        }
        return $this->in(implode(', ', $message), $enabled, 'a');
    }

    /**
     * méthode principale
     * @return void
     * @throws Exception
     */
    public function main()
    {
        $this->admins = is_file($this->adminFileUri)
            ? json_decode(file_get_contents($this->adminFileUri), true)
            : [];
        $this->renderAdminsList();

        switch ($this->selectAction()) {
            case self::ACTION_ADD:
                $this->actionAdd();
                break;
            case self::ACTION_EDIT:
                $this->actionEdit();
                break;
            case self::ACTION_DELETE:
                $this->actionDelete();
                break;
            case self::ACTION_QUIT:
                $this->actionQuit();
                return;
        }
        $this->main();
    }

    /**
     * Ajoute un webservice administrateur
     * @param string $username
     * @param string $password
     */
    public function webservice(string $username, string $password)
    {
        $Users = TableRegistry::getTableLocator()->get('Users');
        $OrgEntities = TableRegistry::getTableLocator()->get('OrgEntities');
        $se = $OrgEntities->find()
            ->innerJoinWith('TypeEntities')
            ->where(['TypeEntities.code' => 'SE'])
            ->firstOrFail();
        if ($Users->exists(['username' => $username])) {
            throw new GenericException("$username already exists");
        }
        $user = $Users->newEntity(
            [
                'username' => $username,
                'password' => $password,
                'high_contrast' => false,
                'active' => true,
                'org_entity_id' => $se->id,
                'agent_type' => 'software',
                'is_validator' => false,
                'use_cert' => false,
            ]
        );
        $conn = $Users->getConnection();
        $conn->begin();
        $Users->saveOrFail($user);
        $registry = new ComponentRegistry();
        $Acl = new AclComponent($registry);
        $aro = ['model' => 'Users', 'foreign_key' => $user->id];
        $Acl->allow($aro, 'root/api');
        $Acl->deny($aro, 'root/controllers');
        $conn->commit();
        $this->out('new user id='.$user->id);
    }

    /**
     * Affiche la liste des administrateurs sous forme de tableau
     */
    protected function renderAdminsList()
    {
        $tds = [];
        $fields = [];
        foreach ($this->fields as $field => $params) {
            if (isset($params['display']) && !$params['display']) {
                continue;
            }
            $fields[] = $field;
            $tds[] = $params['label'];
        }
        $tds[] = __("Notifications");
        $trs = [$tds];

        foreach ($this->admins as $admin) {
            $tds = [];
            foreach ($fields as $field) {
                $tds[] = $admin[$field] ?? null;
            }
            $tds[] = ($admin['notify'] ?? '1') ? __("Oui") : '';
            $trs[] = $tds;
        }
        $this->helper('Table')->output($trs);
    }

    /**
     * Renvoi la clé primaire dans $this->fields()
     * @return string
     * @throws Exception
     */
    protected function getPrimaryKey(): string
    {
        foreach ($this->fields as $field => $params) {
            if (!empty($params['primary'])) {
                return $field;
            }
        }
        throw new Exception("Missing primary key");
    }

    /**
     * Effectue une validation avec affichage d'erreurs
     * @param array $data
     * @return bool
     */
    protected function validateField(array $data): bool
    {
        $success = $this->form->validate($data);
        if (!$success) {
            foreach (Hash::flatten($this->form->getErrors()) as $error) {
                $this->err($error);
            }
        }
        return $success;
    }

    /**
     * Modification d'un administrateur existant
     */
    protected function actionEdit()
    {
        $this->out(__('Modification d\'un administrateur (CTRL+C pour quitter)'));
        $this->hr();
        $options = [];
        $primary = $this->getPrimaryKey();
        foreach ($this->admins as $admin) {
            $options[] = $admin[$primary];
        }
        $username = $this->in(__('Identifiant de l\'administrateur a modifier'), $options, $options[0]);
        $data = null;
        foreach ($this->admins as $admin) {
            if (strcasecmp($admin[$primary], $username) === 0) {
                $data = $admin;
            }
        }
        $data['username'] = $username;
        $data['action'] = 'edit';
        $initial = $data;
        foreach ($this->fields as $field => $params) {
            do {
                $success = $this->askField($field, $data, 'edit', $initial);
            } while (!$success);
        }
        $notify = $this->in(
            __("Recevoir les notifications d'exploitation ?"),
            ['o', 'n'],
            'o'
        );
        $data['notify'] = $notify === 'o' ? '1' : '0';
        if (!$this->form->execute($data)) {
            $this->abortShell(current(Hash::flatten($this->form->getErrors())));
        }
    }

    /**
     * Ajout d'un administrateur
     * @throws Exception
     */
    protected function actionAdd()
    {
        $this->out(__("Ajout d'un administrateur (CTRL+C pour quitter)"));
        $this->hr();
        $data = ['action' => 'add'];
        foreach (array_keys($this->fields) as $field) {
            do {
                $success = $this->askField($field, $data, 'add');
            } while (!$success);
        }
        $notify = $this->in(
            __("Recevoir les notifications d'exploitation ?"),
            ['o', 'n'],
            'o'
        );
        $data['notify'] = $notify === 'o' ? '1' : '0';
        if (!$this->form->execute($data)) {
            $this->abortShell(current(Hash::flatten($this->form->getErrors())));
        }
    }

    /**
     * Demande un champ de formulaire
     * @param string $field
     * @param array  $data
     * @param string $action
     * @param array  $initial
     * @return bool
     * @throws Exception
     */
    private function askField(string $field, array &$data, string $action, array $initial = []): bool
    {
        $params = $this->fields[$field];
        if (!empty($params['primary']) && $action === 'edit') {
            $success = true;
        } elseif (!empty($params['password'])) {
            $askPasswordMessage = $action === 'edit'
                ? __('{0} (laisser vide pour ne pas modidier la valeur du mot de passe)', $params['label'])
                : $params['label'];
            $data[$field] = $this->enterPassword($askPasswordMessage);
            if ($data[$field]) {
                $data[$params['confirm']] = $this->enterPassword(__("Confirmez le mot de passe").' ');
            }
            if (empty($data[$field]) && $action === 'edit') {
                $data[$field] = $initial[$field];
                $data[$params['confirm']] = $data[$field];
                $data['encrypted'] = true;
                $success = true;
            } else {
                $success = $this->validateField(
                    [
                        $field => $data[$field],
                        $params['confirm'] => $data[$params['confirm']],
                        'action' => $action
                    ]
                );
            }
            if (!$success) {
                $choice = $this->in(__("Voulez-vous malgré tout l'utiliser ?"), ['o', 'n'], 'o');
                if (strcasecmp($choice, 'o') === 0) {
                    $success = true;
                    $data['ignorePasswordComplexity'] = true;
                }
            }
        } else {
            $data[$field] = $this->in($params['label'], null, $initial[$field] ?? null);
            $success = $this->validateField([$field => $data[$field], 'action' => $action]);
        }
        return $success;
    }

    /**
     * Suppression d'un administrateur
     * @throws Exception
     */
    protected function actionDelete()
    {
        $this->out(__('Suppression d\'un administrateur (CTRL+C pour quitter)'));
        $this->hr();
        $options = [];
        $primary = $this->getPrimaryKey();
        foreach ($this->admins as $admin) {
            $options[] = $admin[$primary];
        }
        $username = $this->in(__('Identifiant de l\'administrateur a supprimer'), $options, end($options));
        $confirm = $this->in(__("Êtes-vous certain de vouloir supprimer {0} ?", $username), ['y', 'n']);
        if ($confirm === 'y') {
            $form = new AdminShellForm;
            $form->execute(['action' => 'delete', 'username' => $username]);
        }
    }

    /**
     * Arret du shell
     */
    protected function actionQuit()
    {
        $this->_stop();
    }
}
