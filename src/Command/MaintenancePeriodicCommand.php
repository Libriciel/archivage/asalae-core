<?php
/**
 * AsalaeCore\Command\MaintenancePeriodicCommand
 */

namespace AsalaeCore\Command;

use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Exception;

/**
 * Gestion du mode maintenance journalier
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2024, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class MaintenancePeriodicCommand extends AbstractMaintenance
{
    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'maintenance periodic';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = parent::getOptionParser();
        $parser->addArgument(
            'begin_time',
            [
                'help' => __("Heure de début de l'interruption au format hh:mm:ss"),
                'required' => true,
            ]
        );
        $parser->addArgument(
            'end_time',
            [
                'help' => __("Heure de fin de l'interruption au format hh:mm:ss"),
                'required' => true,
            ]
        );
        return $parser;
    }

    /**
     * Action principale
     * @param Arguments $args The command arguments.
     * @param ConsoleIo $io   The console io
     * @throws Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        if (!preg_match('/^\d{2}:\d{2}:\d{2}$/', $args->getArgument('begin_time'))) {
            $io->abort(__("Format horraire incorrect pour le debut"));
        }
        if (!preg_match('/^\d{2}:\d{2}:\d{2}$/', $args->getArgument('end_time'))) {
            $io->abort(__("Format horraire incorrect pour a fin"));
        }

        $config = $this->getConfig();
        $config['periodic']['begin'] = $args->getArgument('begin_time');
        $config['periodic']['end'] = $args->getArgument('end_time');

        $this->setConfig($config);
        $io->success(
            sprintf(
                'Maintenance is will be ON from %s to %s',
                $config['periodic']['begin'],
                $config['periodic']['end']
            )
        );
    }
}
