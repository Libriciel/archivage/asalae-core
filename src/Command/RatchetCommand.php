<?php
/**
 * AsalaeCore\Command\RatchetCommand
 */

namespace AsalaeCore\Command;

use AsalaeCore\Console\AppConsoleOptionParser;
use AsalaeCore\Lib\Ratchet\Pusher;
use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Core\Configure;
use Exception;
use Ratchet\Http\HttpServer;
use Ratchet\Server\IoServer;
use Ratchet\Wamp\WampServer;
use Ratchet\WebSocket\WsServer;
use React\EventLoop\Loop;
use React\EventLoop\StreamSelectLoop;
use React\Socket\SocketServer;
use React\ZMQ\Context;
use React\ZMQ\SocketWrapper;
use ZMQ;
use ZMQContext;
use ZMQSocket;

/**
 * Lancement du serveur Ratchet (notifications)
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2017, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class RatchetCommand extends Command
{
    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $config = (array)Configure::read('Ratchet') + [
            'host' => '127.0.0.1',
            'listen_port' => 8080,
            'zmq' => [
                'protocol' => 'tcp',
                'domain' => '127.0.0.1',
                'port' => 5555,
            ],
        ];
        $parser = new AppConsoleOptionParser();
        $parser->addOption(
            'protocol',
            [
                'help' => __("Protocol du ZMQ"),
                'default' => $config['zmq']['protocol']
            ]
        );
        $parser->addOption(
            'domain',
            [
                'help' => __("Domain du ZMQ"),
                'default' => $config['zmq']['domain']
            ]
        );
        $parser->addOption(
            'port',
            [
                'help' => __("Port du ZMQ"),
                'default' => $config['zmq']['port']
            ]
        );
        $parser->addOption(
            'host',
            [
                'help' => __("Hote"),
                'default' => $config['host']
            ]
        );
        $parser->addOption(
            'listen-port',
            [
                'help' => __("Port d'écoute"),
                'default' => $config['listen_port']
            ]
        );
        return $parser;
    }

    /**
     * Méthode principale
     *
     * @param Arguments $args The command arguments.
     * @param ConsoleIo $io   The console io
     * @throws Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $protocol = $args->getOption('protocol');
        $domain = $args->getOption('domain');
        $port = $args->getOption('port');
        if (($ip = gethostbyname($domain)) !== $domain) {
            $domain = $ip;
        }

        /** @var StreamSelectLoop $loop */
        $loop   = Loop::get();
        $pusher = new Pusher($args->getOption('verbose'));

        /** @var $context ZMQContext */
        $context = new Context($loop);
        /** @var $pull SocketWrapper|ZMQSocket */
        $pull = $context->getSocket(ZMQ::SOCKET_PULL);
        $pull->bind("$protocol://$domain:$port");
        $pull->on('message', array($pusher, 'onNewMessage'));

        // Set up our WebSocket server for clients wanting real-time updates
        $webSock = new SocketServer(
            $args->getOption('host').':'.$args->getOption('listen-port'),
            [],
            $loop
        ); // Binding to 0.0.0.0 means remotes can connect
        $server = new IoServer(
            new HttpServer(
                new WsServer(
                    new WampServer(
                        $pusher
                    )
                )
            ),
            $webSock,
            $loop
        );
        $server->run();
    }
}
