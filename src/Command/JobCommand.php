<?php
/**
 * AsalaeCore\Command\JobCommand
 */

namespace AsalaeCore\Command;

use AsalaeCore\Factory\Utility;
use Beanstalk\Model\Table\BeanstalkWorkersTable;
use Beanstalk\Utility\Beanstalk;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Core\Configure;
use Cake\Http\Exception\ServiceUnavailableException;
use Exception;
use Pheanstalk\PheanstalkInterface;

/**
 * Permet de lancer des jobs
 * ex: bin/cake job test --data "[{\"user_id\":1,\"duration\":10}]" --repeat 10
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2019, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 * @property BeanstalkWorkersTable BeanstalkWorkers
 */
class JobCommand extends Command
{
    /**
     * Action principale
     * @param Arguments $args The command arguments.
     * @param ConsoleIo $io   The console io
     * @throws Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $tube = $args->getArgument('tube');
        $this->BeanstalkWorkers = $this->fetchTable('BeanstalkWorkers');
        $tubes = $this->BeanstalkWorkers->find(
            'list',
            [
                'keyField' => 'tube',
                'valueField' => 'tube',
            ]
        )->toArray();
        if (!in_array($tube, $tubes) && !$args->getOption('force')) {
            $tube = $io->askChoice(__("Veuillez sélectionner un tube"), $tubes);
        }
        /** @var Beanstalk $beanstalk */
        $beanstalk = Utility::get(
            'Beanstalk',
            $tube,
            [
                'host' => $args->getOption('host'),
                'port' => $args->getOption('port'),
                'timeout' => $args->getOption('timeout'),
                'persistant' => $args->getOption('persistant'),
                'table_jobs' => $args->getOption('table_jobs'),
            ]
        );
        if (!$beanstalk->isConnected()) {
            throw new ServiceUnavailableException;
        }
        if ($data = $args->getOption('data')) {
            $data = json_decode($data, true);
            if (!is_array($data)) {
                trigger_error(__("data est mal formaté"));
            }
        }
        if (!$data) {
            $data = $this->askForData($io);
        }
        $repeat = $args->getOption('repeat');
        for ($i=0; $i < $repeat; $i++) {
            foreach ($data as $values) {
                $beanstalk->emit(
                    $values,
                    $args->getOption('priority'),
                    $args->getOption('delay'),
                    $args->getOption('ttr')
                );
            }
        }
        $io->success('done');
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = parent::getOptionParser();
        $parser->addSubcommand(
            'list',
            [
                'help' => __("Liste les jobs"),
            ]
        );
        $parser->addArgument(
            'tube',
            [
                'help' => __("Tube sur lequel envoyer le job"),
            ]
        );
        $parser->addOption(
            'force',
            [
                'help' => __("Supprime le contrôle de validité du tube"),
                'short' => 'f',
                'boolean' => true,
            ]
        );
        $parser->addOption(
            'data',
            [
                'help' => __(
                    "Array de données au format json, 1 jeu de données par job à envoyer\n"
                    ."exemple: [{\"user_id\":1,\"foo\":\"bar\"},{\"user_id\"=1,\"foo\":\"baz\"}]"
                ),
            ]
        );
        $parser->addOption(
            'repeat',
            [
                'help' => __("Nombre de fois qu'il faut lancer les jobs"),
                'default' => 1,
            ]
        );
        $parser->addOption(
            'host',
            [
                'default' => Configure::read('Beanstalk.host', '127.0.0.1'),
                'help' => __("Hote du serveur Beanstalkd"),
            ]
        );
        $parser->addOption(
            'port',
            [
                'default' => Configure::read('Beanstalk.port', PheanstalkInterface::DEFAULT_PORT),
                'help' => __("Port du serveur Beanstalkd"),
            ]
        );
        $parser->addOption(
            'timeout',
            [
                'default' => Configure::read('Beanstalk.timeout'),
            ]
        );
        $parser->addOption(
            'persistant',
            [
                'default' => Configure::read('Beanstalk.persistant', false),
            ]
        );
        $parser->addOption(
            'table_jobs',
            [
                'default' => Configure::read('Beanstalk.table_jobs', 'Beanstalk.BeanstalkJobs'),
            ]
        );
        $parser->addOption(
            'priority',
            [
                'default' => PheanstalkInterface::DEFAULT_PRIORITY,
            ]
        );
        $parser->addOption(
            'delay',
            [
                'default' => PheanstalkInterface::DEFAULT_DELAY,
            ]
        );
        $parser->addOption(
            'ttr',
            [
                'default' => PheanstalkInterface::DEFAULT_TTR,
            ]
        );
        return $parser;
    }

    /**
     * Permet de définir le data à partir des inputs
     * @param ConsoleIo $io
     * @return array
     */
    private function askForData(ConsoleIo $io): array
    {
        $data = [];
        do {
            $job = [];
            do {
                do {
                    $key = $io->ask(__("Clé"));
                } while (!$key);
                do {
                    $value = $io->ask(__("Valeur"));
                } while (!$value);
                if ($value === 'true') {
                    $value = true;
                } elseif ($value === 'false') {
                    $value = false;
                } elseif ($value === 'null') {
                    $value = null;
                } elseif (preg_match('/^\d+\.\d*$/', $value)) {
                    $value = (float)$value;
                } elseif (is_numeric($value)) {
                    $value = (int)$value;
                }
                $job[$key] = $value;
                $io->info(addslashes(json_encode($job, JSON_UNESCAPED_SLASHES)));
            } while ($io->askChoice(__("Ajouter une valeur ?"), ['y', 'n'], 'y') === 'y');
            $data[] = $job;
            $io->info('"'.addslashes(json_encode($data, JSON_UNESCAPED_SLASHES)).'"');
        } while ($io->askChoice(__("Ajouter un job ?"), ['y', 'n'], 'y') === 'y');
        return $data;
    }
}
