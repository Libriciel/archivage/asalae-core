<?php
/**
 * AsalaeCore\Command\JobResumeAllCommand
 */

namespace AsalaeCore\Command;

use AsalaeCore\Factory\Utility;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;
use Exception;

/**
 * Permet de relancer tous les jobs d'un tube
 * ex: bin/cake job resume_all test
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2021, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class JobResumeAllCommand extends Command
{
    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'job resume_all';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = parent::getOptionParser();
        $parser->addArgument(
            'tube',
            [
                'help' => __("Tube sur lequel relancer tous les jobs"),
                'required' => true,
            ]
        );
        return $parser;
    }

    /**
     * Action principale
     * @param Arguments $args The command arguments.
     * @param ConsoleIo $io   The console io
     * @throws Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $tube = $args->getArgument('tube');
        $Jobs = TableRegistry::getTableLocator()->get('BeanstalkJobs');
        $query = $Jobs->find()
            ->where(['BeanstalkJobs.tube' => $tube])
            ->order(['priority']);
        $kicked = 0;
        $Beanstalk = Utility::get('Beanstalk');
        $Beanstalk->setTube($tube);
        /** @var EntityInterface $job */
        foreach ($query as $job) {
            try {
                $Beanstalk->selectJob($job->get('jobid'))->kick();
                $kicked++;
            } catch (Exception $e) {
            }
        }
        if ($kicked === 0) {
            $io->warning(__("Aucun job n'a été relancé"));
            return;
        }
        $this->logEvent(
            'shell_resume_all_jobs',
            'success',
            __("Reprise de {0} jobs du tube ''{0}''", $kicked, $tube)
        );
        $io->success(__("{0} jobs ont été relancés", $kicked));
    }
}
