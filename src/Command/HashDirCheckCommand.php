<?php
/**
 * AsalaeCore\Command\HashDirCheckCommand
 */

namespace AsalaeCore\Command;

use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Core\Configure;
use Exception;

/**
 * Vérifi qu'un fichier d'export $filename n'a pas été modifié
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2021, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class HashDirCheckCommand extends Command
{
    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'hash_dir check';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parserCheck = new ConsoleOptionParser;
        $parserCheck->addArgument(
            'filename',
            ['help' => __("Fichier cible")]
        );
        return $parserCheck;
    }

    /**
     * Main command
     * @param Arguments $args
     * @param ConsoleIo $io
     * @return int|null
     * @throws Exception
     */
    public function execute(Arguments $args, ConsoleIo $io): ?int
    {
        $filename = $args->getArgument('filename');
        if (!$filename) {
            $filename = Configure::read(
                'HashDir.defaultHashFilename',
                RESOURCES.'hashes.txt'
            );
        }
        if (!is_file($filename)) {
            throw new Exception('file not found: '.$filename);
        }
        if (!is_file($filename.'.md5')) {
            throw new Exception('.md5 file not found: '.$filename.'.md5');
        }
        $md5 = md5_file($filename);
        if ($md5 === bin2hex(file_get_contents($filename.'.md5'))) {
            $io->success('OK');
        } else {
            $io->abort('NOT MATCH');
        }

        return self::CODE_SUCCESS;
    }
}
