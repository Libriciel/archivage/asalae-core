<?php
/**
 * AsalaeCore\Command\Command
 */

namespace AsalaeCore\Command;

use Cake\Command\Command as CakeCommand;
use Cake\Datasource\EntityInterface;

/**
 * Permet de visualiser un job
 * ex: bin/cake job view 4531
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2021, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Command extends CakeCommand
{
    /**
     * Ajoute un evenement de commande
     * @param string               $type
     * @param string               $outcome
     * @param string               $detail
     * @param EntityInterface|null $object
     */
    protected function logEvent(
        string $type,
        string $outcome,
        string $detail,
        $object = null
    ) {
    }
}
