<?php
/**
 * AsalaeCore\Command\MaintenanceStatusCommand
 */

namespace AsalaeCore\Command;

use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use DateTime;
use DateTimeInterface;
use Exception;

/**
 * Donne les informations sur la maintenance
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2024, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class MaintenanceStatusCommand extends AbstractMaintenance
{
    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'maintenance status';
    }

    /**
     * Action principale
     * @param Arguments $args The command arguments.
     * @param ConsoleIo $io   The console io
     * @throws Exception
     */
    public function execute(Arguments $args, ConsoleIo $io): int
    {
        $config = $this->getConfig() + [
            'enabled' => false,
            'scheduled' => [],
            'periodic' => [],
        ];
        $config['scheduled'] += [
            'begin' => null,
            'end' => null,
        ];
        $config['periodic'] += [
            'begin' => null,
            'end' => null,
        ];
        $interrupted = false;
        $scheduledBegin = $config['scheduled']['begin'] ? new DateTime($config['scheduled']['begin']) : null;
        $scheduledEnd = $config['scheduled']['end'] ? new DateTime($config['scheduled']['end']) : null;

        $now = new DateTime();
        $message = null;
        if ($config['enabled']) {
            $interrupted = true;
            $message = __("Interruption immédiate");
        }
        if ($scheduledBegin && $scheduledBegin < $now) {
            $interrupted = true;
            $message = __(
                "Interruption programmée depuis le {0}",
                $scheduledBegin->format(DateTimeInterface::ATOM)
            );
        }
        if ($interrupted && $scheduledEnd) {
            if ($scheduledEnd < $now) {
                $interrupted = false; // l'interruption a dépassé la date
            } else {
                $message .= ' ' . __(
                    "jusqu'au {0}",
                    $scheduledEnd->format(DateTimeInterface::ATOM)
                );
            }
        }
        if (!$interrupted && $config['periodic']['begin'] && $config['periodic']['end']) {
            $begin = new DateTime($config['periodic']['begin']);
            $end = new DateTime($config['periodic']['end']);
            // chevauchement des jours ex: 23h30 - 01h30 => si now est > 23h30 ou < 01h30
            $inBetweenDays = $begin > $end && ($now > $begin || $now < $end);
            $interrupted = $inBetweenDays || ($begin < $now && $end > $now);
            if ($interrupted) {
                $message = __(
                    "Interruption journalière de {0} à {1}",
                    $begin->format('H:i:s'),
                    $end->format('H:i:s'),
                );
            }
        }
        if ($interrupted) {
            $io->error($message);
            return 1;
        } else {
            $io->success(__("N'est pas actuellement en mode maintenance"));
            return 0;
        }
    }
}
