<?php
/**
 * AsalaeCore\Command\PromptOptionsTrait
 */

namespace AsalaeCore\Command;

use AsalaeCore\Console\AppConsoleOptionParser;
use AsalaeCore\Console\ConsoleIo;
use Cake\Command\Command;
use Exception;

/**
 * Permet de transformer les options en input
 *
 * @category    Shell
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2017, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin Command
 */
trait PromptOptionsTrait
{
    use PasswordTrait;

    /**
     * Demande les options qui n'ont pas été précisé
     * @throws Exception
     */
    public function promptOptions()
    {
        $optionParser = $this->getOptionParser();
        if (!$optionParser instanceof AppConsoleOptionParser) {
            throw new Exception(
                __(
                    "optionParser doit être une instance de {0} pour utiliser PromptOptionsTrait::promptOptions()",
                    AppConsoleOptionParser::class
                )
            );
        }
        $longs = [];
        $shorts = [];
        foreach ($optionParser->options() as $key => $option) {
            $longs[$key] = $key;
            if ($option->short()) {
                $shorts[$key] = $option->short();
            }
        }
        $argv = $_SERVER['argv'];
        foreach ($argv as $arg) {
            if (preg_match('/^--(\w+)$/', $arg, $match)) {
                unset($longs[$match[1]], $shorts[$match[1]]);
            } elseif (preg_match('/^-(\w)$/', $arg, $match)) {
                $key = array_search($match[1], $shorts);
                unset($longs[$key], $shorts[$key]);
            }
        }
        unset($longs['help'], $longs['verbose'], $longs['quiet']);
        foreach ($longs as $key) {
            $options = $optionParser->options[$key];
            $msg = __("{0}: {1}", $key, $options['help'] ?? null);
            $choices = ($options['choices'] ?? null) ?: [];
            $default = $options['default'] ?? null;

            if (!empty($options['password'])) {
                $this->params[$key] = $this->enterPassword($msg);
            } elseif (isset($this->ioShell) && $this->ioShell instanceof ConsoleIo) {
                if ($choices) {
                    $this->params[$key] = $this->ioShell->askChoice($msg, $choices, $default);
                } else {
                    $this->ioShell->ask($msg, $default);
                }
            }
        }
    }
}
