<?php
/**
 * AsalaeCore\Command\FixturesDatabaseCommand
 */

namespace AsalaeCore\Command;

use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Core\App;
use Cake\Core\Configure;
use Cake\Database\Connection;
use Cake\Database\Schema\TableSchema;
use Cake\Datasource\ConnectionManager;
use Cake\Datasource\FixtureInterface;
use Cake\TestSuite\Fixture\TestFixture;
use Exception;
use Migrations\Command\MigrationsSeedCommand;
use Migrations\MigrationsDispatcher;
use ReflectionClass;
use ReflectionException;
use Symfony\Component\Console\Input\ArgvInput;
use Symfony\Component\Console\Output\ConsoleOutput;

/**
 * Garni une base de données avec les données de fixtures
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2022, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class FixturesDatabaseCommand extends Command
{
    const DEFINE_CONSTS = [
        'TMP_VOL1',
        'TMP_VOL2',
        'TMP_VOL3',
        'TMP_TESTDIR',
    ];

    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'fixtures database';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = new ConsoleOptionParser();
        $parser->setDescription(
            __(
                "Insert les données de fixtures dans un datasource et "
                . "applique migrations, seed, update et import_perms. "
                . "A utiliser en combinaison de DatabaseToFixtures"
            )
        );
        $parser->addOption(
            'datasource',
            [
                'help' => __(
                    "Datasource sur lequel travailler (supprimera les "
                    . "données existantes)"
                ),
                'default' => 'fixtures',
            ]
        );
        $parser->addOption(
            'with-volumes',
            [
                'help' => __("Défini les volumes avec App.paths.data"),
                'boolean' => true,
            ]
        );
        return $parser;
    }

    /**
     * Méthode principale
     * @param Arguments $args
     * @param ConsoleIo $io
     * @return void
     * @throws Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        if ($args->getOption('with-volumes')) {
            $basePath = Configure::read('App.paths.data') . DS;
            if (!defined('TMP_VOL1')) {
                define('TMP_VOL1', $basePath . 'volume01');
            }
            if (!defined('TMP_VOL2')) {
                define('TMP_VOL2', $basePath . 'volume02');
            }
            if (!defined('TMP_VOL3')) {
                define('TMP_VOL3', $basePath . 'volume03');
            }
        }

        // vérifi l'existance de la base de données
        $datasourceName = $args->getOption('datasource');
        /* @var $conn Connection */
        $conn = ConnectionManager::get($datasourceName);
        $conn->connect();

        // migration migrate
        $app = new MigrationsDispatcher('UNKNOWN');
        $input = new ArgvInput(
            [
                'migrations',
                'migrate',
                '--connection',
                $datasourceName
            ]
        );
        $app->setAutoExit(false);
        $exitCode = $app->run($input, new ConsoleOutput());
        if ($exitCode !== 0) {
            $this->abort();
        }

        // gestion des constantes PHP
        $this->setFixturesConstants();

        // load fixtures
        $io->out('loading fixtures...');
        $this->loadFixtures($datasourceName, $io, $conn);

        // update
        (new UpdateCommand)->execute(
            new Arguments([], ['datasource' => $datasourceName], []),
            $io
        );

        // migrations seed
        (new MigrationsSeedCommand)->run(
            [
                '--connection',
                $datasourceName,
            ],
            $io
        );

        // roles_perms import
        $io->out('import roles...');
        (new RolesPermsCommand)->run(
            [
                'import',
                '--datasource',
                $datasourceName,
                RESOURCES . 'export_roles.json'
            ],
            $io
        );
    }

    /**
     * Charge les fixtures dans le datasource
     * @param string     $datasourceName
     * @param ConsoleIo  $io
     * @param Connection $conn
     * @return void
     * @throws ReflectionException
     */
    private function loadFixtures(string $datasourceName, ConsoleIo $io, Connection $conn)
    {
        $glob = glob(TESTS . 'Fixture/*Fixture.php');
        $fixtureNames = array_map(fn($v) => basename($v, 'Fixture.php'), $glob);
        $fixtures = [];

        foreach ($fixtureNames as $fixtureName) {
            if ($fixtureName === 'Phinxlog' || substr($fixtureName, 0, 6) === '_Empty') {
                continue;
            }
            $className = App::className($fixtureName, 'Test/Fixture', 'Fixture');
            $ref = new ReflectionClass($className);
            /** @var TestFixture $fixture */
            $fixture = $ref->newInstanceWithoutConstructor();
            $fixture->connection = $datasourceName;
            $fixture->init();
            $fixtures[] = $fixture;
            $io->verbose($fixtureName);
        }
        $fixtures = $this->sortByConstraint($conn, $fixtures);
        $conn->disableConstraints(fn() => array_map(fn($f) => $f->truncate($conn), $fixtures));
        $conn->disableConstraints(fn() => array_map(fn($f) => $f->insert($conn), $fixtures));
    }

    /**
     * Certaines fixtures sont composés de constantes, on permet un
     * rechercher-remplacer
     * ex: str_replace('{{CONST:TMP_VOL1}}', TMP_VOL1)
     */
    private function setFixturesConstants()
    {
        foreach (self::DEFINE_CONSTS as $const) {
            if (!defined($const)) {
                define($const, "{{CONST:$const}}");
            }
        }
    }

    /**
     * Tri les tables en fonction de leurs foreign keys
     * @param Connection $connection
     * @param array      $fixtures
     * @return array|null
     */
    protected function sortByConstraint(Connection $connection, array $fixtures): ?array
    {
        $constrained = [];
        $unconstrained = [];
        $allFixtures = [];
        foreach ($fixtures as $fixture) {
            $references = $this->getForeignReferences($connection, $fixture);
            if ($references) {
                $constrained[$fixture->sourceName()] = ['references' => $references, 'fixture' => $fixture];
            } else {
                $unconstrained[$fixture->sourceName()] = $fixture;
            }
            $allFixtures[$fixture->sourceName()] = $fixture;
        }
        return $this->sortConstrained($unconstrained, $allFixtures, $constrained);
    }

    /**
     * Tri les models qui on des contraintes
     * @param array $unconstrained
     * @param array $allFixtures
     * @param array $constrained
     * @return array
     */
    private function sortConstrained(array $unconstrained, array $allFixtures, array $constrained): array
    {
        $fixtures = array_values($unconstrained);
        $done = array_keys($unconstrained);
        foreach ($constrained as $sourceName => $arr) {
            $fixtures = array_merge(
                $fixtures,
                $this->recursiveAddFixture($sourceName, $constrained, $allFixtures, $done)
            );
        }
        return $fixtures;
    }

    /**
     * Fonction récursive pour sortConstrained()
     * @param string $sourceName
     * @param array  $constrained
     * @param array  $allFixtures
     * @param array  $done
     * @return array
     */
    private function recursiveAddFixture(
        string $sourceName,
        array $constrained,
        array $allFixtures,
        array &$done
    ): array {
        $prerequisites = $constrained[$sourceName] ?? ['references' => []];
        $fixtures = [];
        if (in_array($sourceName, $done)) {
            return [];
        }
        $done[] = $sourceName; // anti boucle infinie
        foreach ($prerequisites['references'] as $pre) {
            if (in_array($pre, $done) || $pre === $sourceName) {
                continue;
            }
            $fixtures = array_merge(
                $fixtures,
                $this->recursiveAddFixture($pre, $constrained, $allFixtures, $done)
            );
        }
        $fixtures[] = $allFixtures[$sourceName];
        return $fixtures;
    }

    /**
     * Donne les dépendances et l'instance de fixture d'un model
     * @param Connection       $connection
     * @param FixtureInterface $fixture
     * @return array [
     *                  'references' => (array) liste des tables prérequises,
     *                  'fixture' => FixtureInterface
     *              ]
     */
    protected function getForeignReferences(Connection $connection, FixtureInterface $fixture): array
    {
        static $schemas = [];

        $tableName = $fixture->sourceName();
        if (!isset($schemas[$tableName])) {
            $schemas[$tableName] = $connection->getSchemaCollection()->describe($tableName);
        }
        $schema = $schemas[$tableName];

        $references = [];
        foreach ($schema->constraints() as $constraintName) {
            $constraint = $schema->getConstraint($constraintName);

            if ($constraint && $constraint['type'] === TableSchema::CONSTRAINT_FOREIGN) {
                $references[] = $constraint['references'][0];
            }
        }

        return $references;
    }
}
