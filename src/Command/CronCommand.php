<?php
/**
 * AsalaeCore\CronCommand
 */

namespace AsalaeCore\Command;

use AsalaeCore\Model\Table\CronExecutionsTable;
use AsalaeCore\Console\ConsoleIo;
use AsalaeCore\Controller\Component\ConditionComponent;
use AsalaeCore\Cron\CronInterface;
use AsalaeCore\Utility\Notify;
use AsalaeCore\View\AppView;
use Cake\Command\Command;
use Cake\Console\ConsoleIo as CakeConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Console\ConsoleOutput;
use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Http\Exception\BadRequestException;
use Cake\Http\Response as CakeResponse;
use Cake\I18n\FrozenTime as Time;
use Cake\Mailer\Mailer;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Cake\View\Helper\UrlHelper;
use DateInterval;
use DateTime;
use DateTimeInterface;
use Exception;
use IntlDateFormatter;
use Libriciel\Filesystem\Utility\Filesystem;

/**
 * Permet de lancer un cron (utilisé par crontab)
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2018, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 * @property Table $Crons
 * @property Table $CronExecutions
 */
class CronCommand extends Command
{
    /**
     * Traits
     */
    use CommandShellTrait {
        CommandShellTrait::run as runTrait;
    }

    /**
     * @var EntityInterface|null Cron
     */
    public $cron;

    /**
     * @var EntityInterface|null CronExecution
     */
    public $exec;

    /**
     * CronCommand constructor.
     */
    public function __construct()
    {
        parent::__construct();
        Time::$niceFormat = [IntlDateFormatter::SHORT, IntlDateFormatter::MEDIUM];
    }

    /**
     * Override the default behavior to save the command called
     * in order to pass it to the command dispatcher
     *
     * @param array         $argv Arguments from the CLI environment.
     * @param CakeConsoleIo $io   The console io
     * @return int|null Exit code or null for success.
     * @throws Exception
     */
    public function run(array $argv, CakeConsoleIo $io): ?int
    {
        if (isset($argv[0]) && $argv[0] === 'run') {
            $argv[0] = 'runCron';
        }
        return $this->runTrait($argv, $io);
    }

    /**
     * Getter du stdout
     * @return ConsoleOutput
     */
    public function getOut(): ConsoleOutput
    {
        if ($this->ioShell instanceof ConsoleIo) {
            return $this->ioShell->getOut();
        }
        return new ConsoleOutput('php://stdout');
    }

    /**
     * Getter du stderr
     * @return ConsoleOutput
     */
    public function getErr(): ConsoleOutput
    {
        if ($this->ioShell instanceof ConsoleIo) {
            return $this->ioShell->getErr();
        }
        return new ConsoleOutput('php://stderr');
    }

    /**
     * Display help for this console.
     *
     * @return ConsoleOptionParser
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = new ConsoleOptionParser();
        $parserList = new ConsoleOptionParser('list');
        $parserRun = new ConsoleOptionParser('run');
        $parserUnlock = new ConsoleOptionParser('unlock');

        $parser->addOption(
            'no-interaction',
            [
                'help' => __("Désactive la partie interactive"),
                'boolean' => true,
            ]
        );

        $parser->addSubcommand(
            'run',
            [
                'help' => __("Lance un cron sans vérifier la date"),
                'parser' => $parserRun
            ]
        );
        $parser->addSubcommand(
            'run_cron',
            [
                'help' => __("Alias de run (compatibilité ascendente)"),
                'parser' => $parserRun
            ]
        );
        $parserRun->addArgument(
            'cron',
            ['help' => __("ID du cron (DB)"), 'required' => true]
        );

        $parser->addSubcommand(
            'list',
            [
                'help' => __("Donne la liste des crons"),
                'parser' => $parserList,
            ]
        );
        $parserList->addOption(
            'id',
            ['help' => __("Liste par ID"), 'boolean' => true]
        );

        $parserRun->addOption(
            'params',
            [
                'short' => 'p',
                'help' => __("Permet de passer des paramètres sous la forme key=value"),
                'multiple' => true,
            ]
        );
        $parserRun->addOption(
            'context',
            [
                'help' => __("Permet de spécifier un contexte d'execution"),
                'default' => 'shell',
            ]
        );
        $parserRun->addOption(
            'no-interaction',
            [
                'help' => __("Désactive la partie interactive"),
                'boolean' => true,
            ]
        );

        $parser->addSubcommand(
            'unlock',
            [
                'help' => __("Déverrouille un cron"),
                'parser' => $parserUnlock
            ]
        );
        $parserUnlock->addArgument(
            'cron',
            ['help' => __("ID du cron (DB)"), 'required' => true]
        );

        return $parser;
    }

    /**
     * Lancement des crons
     * @throws Exception
     */
    public function main()
    {
        register_shutdown_function([$this, 'shutdown']);
        $this->out(__("Lancement des crons..."));
        $this->Crons = $this->fetchTable('Crons');
        $this->CronExecutions = $this->fetchTable('CronExecutions');

        $conditions = [
            'Crons.active' => true,
            'Crons.locked' => false,
            'Crons.next <' => new DateTime,
        ];
        if ($this->Crons->exists(['locked' => true, 'active' => true])) {
            $conditions['Crons.parallelisable'] = true;
        }

        $crons = $this->Crons->find()
            ->where($conditions)
            ->all()
            ->toArray();
        $ids = array_map(
            function (EntityInterface $v) {
                return $v->get('id');
            },
            $crons
        );
        $this->out(__n("Il y a {0} cron à lancer", "Il y a {0} crons à lancer", $c = count($ids), $c));
        /** @var EntityInterface $cron */
        foreach ($ids as $id) {
            $this->exec = null;
            $this->cron = null;
            $cron = $this->Crons->find()
                ->where(['Crons.id' => $id])
                ->andWhere($conditions)
                ->first();
            if (!$cron) {
                continue;
            }
            try {
                $this->runCron($cron);
            } catch (Exception $e) {
                $this->err((string)$e);
                $cron->set('locked', false);
                $this->Crons->save($cron);
                if ($this->exec) {
                    $this->exec->set(
                        'report',
                        $this->exec->get('report') . " \n"
                        . __("Erreur lors de l'execution du cron ({0})", $e->getMessage())
                    );
                }
            }
        }
        $this->out(__("Fin."));
    }

    /**
     * Envoi un message d'alerte aux administrateurs
     * @param EntityInterface $cron
     * @param EntityInterface $exec
     * @throws Exception
     */
    private function sendMail(EntityInterface $cron, EntityInterface $exec)
    {
        $lastmail = $cron->get('last_email');
        $interval = date_interval_create_from_date_string(
            Configure::read('Crons.emails.min_interval', '1 day')
        );
        if ($lastmail instanceof DateTimeInterface && $lastmail->add($interval) > new DateTime) {
            return;
        }
        // lecture des mails des administateurs techniques (les adresses mails xxx@test.fr sont ignorées)
        $mailField = Configure::read('AsalaeCore.CronCommand.email.adminEmailField', 'email');
        $notifyField = Configure::read('AsalaeCore.CronCommand.email.notificationField', 'notify');
        $adminFileUri = Configure::read(
            'App.paths.administrators_json',
            Configure::read('App.paths.data') . DS . 'administrateurs.json'
        );
        $mails = array_map(
            function ($v) use ($mailField, $notifyField) {
                return ($v[$notifyField] ?? '') === '0' || !isset($v[$mailField]) ? false : $v[$mailField];
            },
            json_decode(file_get_contents($adminFileUri), true)
        );
        $mails = array_filter(
            $mails,
            function ($v) {
                return $v && substr($v, strpos($v, '@') +1) !== 'test.fr';
            }
        );
        if (empty($mails)) {
            return;
        }

        // initialisation et envoi du mail
        $email = new Mailer;
        $email->viewBuilder()
            ->setTemplate(Configure::read('AsalaeCore.CronCommand.email.template', 'AsalaeCore.cronfailed'))
            ->setLayout(Configure::read('AsalaeCore.CronCommand.email.layout', 'AsalaeCore.default'))
            ->addHelpers(['Html', 'Url']);
        $mailTitlePrefix = Configure::read('AsalaeCore.CronCommand.email.titlePrefix', '');
        try {
            $success = $email->setEmailFormat(Configure::read('AsalaeCore.CronCommand.email.format', 'both'))
                ->setSubject(
                    $mailTitlePrefix.($mailTitlePrefix ? '  ' : '')
                    .__("Rapport de tâche planifiée ''{0}''", $cron->get('name'))
                )
                ->setViewVars(compact('cron', 'exec'))
                ->setTo(array_values($mails))
                ->send();
            if ($response = $this->debugMail($email)) {
                $body = $response->getBody();
                $body->rewind();
                $this->out('debug_mail: '.$body->getContents());
            }
        } catch (Exception $e) {
            $exec->set(
                'report',
                $exec->get('report') . " \n"
                . __("Erreur lors de l'envoi de la notification d'erreur ({0})", $e->getMessage())
            );
            $this->CronExecutions->save($exec);
            throw $e;
        }

        if ($success) {
            $cron->set('last_email', new DateTime);
            $this->Crons->saveOrFail($cron);
            $this->out(
                __n(
                    "Un email a été envoyé à l'administrateur: {0}",
                    "Un email a été envoyé aux administrateurs: {0}",
                    count($mails),
                    implode(', ', $mails)
                )
            );
        }
    }

    /**
     * Choisi un cron en bdd à partir d'une chaine de caractère
     * @param string $cron
     * @return EntityInterface
     */
    private function chooseCron(string $cron): EntityInterface
    {
        $Conditions = new ConditionComponent(new ComponentRegistry);
        do {
            $crons = $this->Crons->find()
                ->where(
                    [
                        'OR' => [
                            $Conditions->ilike('name', '%'.$cron.'%'),
                            $Conditions->ilike('classname', '%'.$cron.'%'),
                        ],
                    ]
                )
                ->all();
            if ($crons->count() === 1) {
                $classname = $crons->first()->get('classname');
                $classname = substr($classname, strrpos($classname, '\\') + 1);
                $this->out(__("Trouvé: {0}", $crons->first()->get('name')));
                $response = $this->in(__("Lancer le cron {0} ?", $classname), ['y', 'n'], 'y');
                if ($response === 'y') {
                    $cron = $crons->first();
                    break;
                }
            } else {
                if ($crons->count() === 0) {
                    $crons = $this->Crons->find()->all();
                }
                $this->out(__("Choisir parmis :"));
                $classnames = [];
                foreach ($crons as $cronEntity) {
                    $classname = $cronEntity->get('classname');
                    $classnames[] = substr($classname, strrpos($classname, '\\') + 1);
                }
                sort($classnames);
                array_map(fn($v) => $this->info($v), $classnames);
            }
            $cron = $this->in(__("Choisir un cron"));
        } while (true);
        return $cron;
    }

    /**
     * Lance un cron
     * @param int|string|EntityInterface $cron
     * @throws Exception
     */
    public function runCron($cron)
    {
        $now = new DateTime;
        $this->Crons = $this->fetchTable('Crons');
        $this->CronExecutions = $this->fetchTable('CronExecutions');
        if (is_numeric($cron)) {
            try {
                $cron = $this->Crons->get($cron);
            } catch (RecordNotFoundException $e) {
                $this->abortShell(__("Le cron id={0} n'existe pas", $cron));
            }
        } elseif ($this->param('no-interaction') === false && is_string($cron)) {
            $cron = $this->chooseCron($cron);
        }
        if (!$cron instanceof EntityInterface) {
            throw new BadRequestException;
        }
        $this->cron = $cron;
        $this->hr();
        $this->out((new Time())->nice().' - '.__("Lancement du cron: {0}", $cron->get('name')));
        $this->hr();
        $cron->set('locked', true);
        $this->Crons->saveOrFail($cron);
        $pid = getmypid();
        $exec = $this->CronExecutions->newEntity(
            [
                'cron_id' => $cron->get('id'),
                'date_end' => null,
                'state' => CronExecutionsTable::S_RUNNING,
                'report' => '',
                'pid' => file_exists('/proc/'.$pid) ? $pid : null,
            ]
        );
        // NOTE: le setter de cake ne prend pas les microsecondes
        $exec->set('date_begin', (new DateTime)->format('Y-m-d H:i:s.u'), ['setter' => false]);
        $this->CronExecutions->saveOrFail($exec);
        Notify::emit(
            'crons',
            [
                'cron_id' => $cron->id,
                'exec_id' => $exec->id,
                'state' => CronExecutionsTable::S_RUNNING,
            ]
        );
        $this->exec = $exec;
        $params = (array)json_decode($cron->get('app_meta'));
        $params['context'] = $this->param('context');
        foreach ((array)$this->param('params') as $p) {
            if (!strpos($p, '=')) {
                $params[$p] = true;
            } else {
                [$key, $value] = explode('=', $p, 2);
                $params[$key] = $value;
            }
        }
        $Class = null;
        try {
            $classname = $cron->get('classname');
            /** @var CronInterface $Class */
            $Class = new $classname($params, $this->getOut(), $this->getErr());
            $state = $Class->work($exec, $cron);
            $output = $Class->getOutput();
        } catch (Exception $e) {
            $conn = $this->Crons->getConnection();
            while ($conn->inTransaction()) {
                $conn->rollback();
            }
            try {
                $Class->err((string)$e);
                $output = $Class->getOutput();
                if ($this->exec) {
                    $this->exec->set(
                        'report',
                        $this->exec->get('report') . " \n"
                        . __("Erreur lors de l'execution du cron ({0})", $e->getMessage())
                    );
                }
            } catch (Exception $f) {
                $output = $e->getMessage();
            }
            $state = CronExecutionsTable::S_ERROR;
        }
        $this->CronExecutions->patchEntity(
            $exec,
            [
                'state' => $state,
                'report' => $output,
            ]
        );
        $exec->set('date_end', (new DateTime)->format('Y-m-d H:i:s.u'), ['setter' => false]);
        $this->CronExecutions->save($exec);
        Notify::emit('crons', ['cron_id' => $cron->id, 'exec_id' => $exec->id, 'state' => $state]);
        $cron->set('locked', false);
        if ($cron->get('frequency') === 'one_shot') {
            if ($state === CronExecutionsTable::S_SUCCESS) {
                $this->CronExecutions->updateAll(['cron_id' => null], ['cron_id' => $cron->get('id')]);
                $this->Crons->delete($cron);
                Notify::emit('crons', ['cron_id' => $cron->id, 'deleted' => true]);
            } else {
                $cron->set('active', false);
                $this->Crons->saveOrFail($cron);
            }
            $this->logOneShot($cron, $output);
        } else {
            $interval = new DateInterval($cron->get('frequency'));
            /** @var DateTime $next */
            $next = $cron->get('next');
            while ($next < $now) {
                $next = $next->add($interval);
            }
            $cron->set('next', $next);
            $this->Crons->saveOrFail($cron);
        }
        if ($state === CronExecutionsTable::S_ERROR) {
            try {
                $this->sendMail($cron, $exec);
            } catch (Exception $e) {
                $this->err(
                    __(
                        "Erreur lors de l'envoi des mails de notification d'erreur: {0} ({1}:{2})",
                        $e->getMessage(),
                        'ROOT' . substr($e->getFile(), strlen(ROOT)),
                        $e->getLine()
                    )
                );
            }
        }
        $status = '<'.$state.'>'.$state.'</'.$state.'>';
        $this->out(
            __("Le cron a terminé (execution n°{0}), son statut est {1}", $exec->get('id'), $status)
        );
    }

    /**
     * Liste des crons
     */
    public function list()
    {
        $fields = ['id', 'classname', 'active', 'locked', 'frequency', 'next', 'parallelisable'];
        $data = TableRegistry::getTableLocator()->get('Crons')->find()
            ->select($fields)
            ->order([$this->param('id') ? 'id' : 'classname'])
            ->disableHydration()
            ->all()
            ->map(
                function ($data) {
                    foreach ($data as $k => $v) {
                        if ($v instanceof DateTimeInterface) {
                            $data[$k] = $v->format('Y-m-d H:i:s');
                        }
                        $data[$k] = (string)$data[$k];
                    }
                    return $data;
                }
            )
            ->toArray();

        $now = new Time;
        $today = new Time($now->format('Y-m-d'));
        // On récupère le nom de classe sans namespace pour pouvoir le trier
        $nData = [];
        foreach ($data as $row) {
            $row['classname'] = substr(
                $row['classname'],
                strrpos($row['classname'], '\\') + 1
            );
            $row['active'] = $row['active'] ? 'true' : '';
            $row['locked'] = $row['locked'] ? 'true' : '';
            $row['parallelisable'] = $row['parallelisable'] ? 'true' : '';
            $row['next'] = new Time($row['next']);
            $days = (new Time($row['next']->format('Y-m-d')))->diffInDays($today);
            if ($row['next'] < $now) {
                $row['next'] = __("prochainement");
            } elseif ($days === 0) {
                debug([$row['classname'] => $row['next']]);
                $row['next'] = __("aujourd'hui à {0}", $row['next']->format('H:i:s'));
            } elseif ($days === 1) {
                $row['next'] = __("demain à {0}", $row['next']->format('H:i:s'));
            } else {
                $row['next'] = __(
                    "dans {0} jours à {1}",
                    $days,
                    $row['next']->format('H:i:s')
                );
            }
            $nData[$row['classname']] = $row;
        }
        ksort($nData);
        $nData = array_values($nData);
        array_unshift($nData, $fields);

        $this->helper('Table')->output($nData);
    }

    /**
     * S'assure de lever le vérrou
     */
    public function __destruct()
    {
        $this->shutdown();
    }

    /**
     * S'assure de lever le vérrou
     */
    public function shutdown()
    {
        if (!empty($this->cron) && $this->cron->get('locked')) {
            try { // fails on testing 'cause the cron_execution table is allready down (sqlite)
                if ($this->cron->get('frequency') === 'one_shot') {
                    $this->CronExecutions->updateAll(['cron_id' => null], ['cron_id' => $this->cron->get('id')]);
                    $this->Crons->delete($this->cron);
                    unset($this->cron);
                    return;
                }
                $now = new DateTime;
                $interval = new DateInterval($this->cron->get('frequency'));
                /** @var DateTime $next */
                $next = $this->cron->get('next');
                while ($next < $now) {
                    $next = $next->add($interval);
                }
                $this->cron->set('next', $next);
                $this->cron->set('locked', false);
                $this->Crons->saveOrFail($this->cron);

                $this->exec->set('state', CronExecutionsTable::S_ABORTED);
                $this->exec->set('date_end', (new DateTime)->format('Y-m-d H:i:s.u'), ['setter' => false]);
                $this->CronExecutions->save($this->exec);
                unset($this->cron);
            } catch (Exception $e) {
            }
        }
    }

    /**
     * Déverrouille un cron
     * @param string $cron_id
     */
    public function unlock(string $cron_id)
    {
        $this->Crons = $this->fetchTable('Crons');
        $this->CronExecutions = $this->fetchTable('CronExecutions');
        $cron = $this->Crons->find()
            ->where(['Crons.id' => $cron_id])
            ->contain(
                [
                    'CronExecutions' => function (Query $q) {
                        return $q->where(['CronExecutions.state' => CronExecutionsTable::S_RUNNING]);
                    }
                ]
            )
            ->firstOrFail();
        /** @var EntityInterface $exec */
        foreach (Hash::get($cron, 'cron_executions', []) as $exec) {
            if (file_exists('/proc/'.$exec->get('pid'))) {
                $this->abortShell(__("Le cron semble toujours actif dans le process id n°{0}", $exec->get('pid')));
            }
            $this->CronExecutions->updateAll(
                [
                    'state' => CronExecutionsTable::S_CANCELED,
                    'report' => trim($exec->get('report').' - '.__("Annulé par un administrateur"), ' -'),
                ],
                ['id' => $exec->id]
            );
            $this->out(__("Execution n°{0} annulé par administrateur", $exec->id));
        }
        if ($cron->get('state') === CronExecutionsTable::S_RUNNING) {
            $cron->set('state', CronExecutionsTable::S_CANCELED);
        }
    }

    /**
     * Si le debug est activé, renvoi une url pour consulter le mail
     * @param Mailer $email
     * @return CakeResponse|null
     * @throws Exception
     */
    public function debugMail(Mailer $email): ?CakeResponse
    {
        if (Configure::read('debug_mails')) {
            $basePath = Configure::read('debug_mails_basepath', TMP.'debug_mails');
            $filename = (new DateTime())->format('Ymd_H-i').uniqid('_sendTestMail_').'.html';
            Filesystem::dumpFile(
                $basePath . DS . $filename,
                $email->getMessage()->getBodyHtml()
            );
            $url = new UrlHelper(new AppView);
            $registry = new ComponentRegistry();
            $component = new Component($registry);
            return $component->getController()
                ->getResponse()
                ->withType('txt')
                ->withHeader('X-Email-Debug', 'true')
                ->withStringBody(
                    $url->build('/devs/debug-mail/'.base64_encode($filename), ['fullBase' => true])
                );
        }
        return null;
    }

    /**
     * Permet de consulter le rapport d'un cron "one_shot" dans les logs
     * @param EntityInterface $cron
     * @param string          $output
     * @return void
     */
    private function logOneShot(EntityInterface $cron, string $output): void
    {
        if (is_writable(dirname($cron->get('log_filename')))) {
            file_put_contents($cron->get('log_filename'), $output."\n", FILE_APPEND);
        }
    }
}
