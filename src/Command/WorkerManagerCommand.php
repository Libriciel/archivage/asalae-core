<?php
/**
 * AsalaeCore\WorkerManagerCommand
 */

namespace AsalaeCore\Command;

use AsalaeCore\Factory\Utility;
use AsalaeCore\Middleware\MaintenanceMiddleware;
use AsalaeCore\Utility\Exec;
use AsalaeCore\Utility\Object\CommandResult;
use Beanstalk\Model\Entity\BeanstalkWorker;
use Beanstalk\Model\Table\BeanstalkWorkersTable;
use Beanstalk\Utility\Beanstalk;
use Cake\Command\Command;
use Cake\Console\ConsoleOptionParser;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;
use DateTimeInterface;
use Exception;
use ZMQSocketException;

/**
 * Gestion des workers beanstalkd par configuration
 * Assure le lancement de nouveaux processes au besoin
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2019, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class WorkerManagerCommand extends Command
{
    /**
     * Traits
     */
    use CommandShellTrait;

    /**
     * @var Beanstalk
     */
    private $Beanstalk;

    /**
     * @var BeanstalkWorkersTable
     */
    private $BeanstalkWorkers;

    /**
     * WorkerManagerCommand constructor.
     * @throws Exception
     */
    public function __construct()
    {
        parent::__construct();
        $this->Beanstalk = Utility::get('Beanstalk');
        $this->BeanstalkWorkers = TableRegistry::getTableLocator()->get('Beanstalk.BeanstalkWorkers');
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = new ConsoleOptionParser();
        $parserKill = new ConsoleOptionParser();
        $parserStop = new ConsoleOptionParser();
        $parserRestart = new ConsoleOptionParser();
        $parser->addSubcommand(
            'kill',
            [
                'help' => __("Permet de tuer un worker par son id"),
                'parser' => $parserKill,
            ]
        );
        $parser->addSubcommand(
            'list',
            [
                'help' => __("Donne la liste des workers"),
            ]
        );
        $parser->addSubcommand(
            'list-workers',
            [
                'help' => __("Donne la liste des workers"),
            ]
        );
        $parser->addSubcommand(
            'stop',
            [
                'help' => __("Ordre d'arrêt à un worker"),
                'parser' => $parserStop,
            ]
        );
        $parser->addSubcommand(
            'stop-all',
            [
                'help' => __("Ordre d'arrêt envoyé à tous les workers"),
            ]
        );
        $parser->addSubcommand(
            'restart',
            [
                'help' => __("Relance un worker"),
                'parser' => $parserRestart,
            ]
        );
        $parser->addSubcommand(
            'restart-all',
            [
                'help' => __("Relance tous les workers"),
            ]
        );
        $parser->addSubcommand(
            'kill-all',
            [
                'help' => __("Tues tous les workers"),
            ]
        );
        $parser->addSubcommand(
            'kill-zombies',
            [
                'help' => __("Tues les workers qui ne sont pas en base"),
            ]
        );
        $parser->setDescription(
            [
                __("Gestion des workers")
            ]
        );
        $parserKill->addArgument(
            'id',
            [
                'help' => __("primary de la table {0}", 'beanstalk_workers'),
            ]
        );
        $parserStop->addArgument(
            'id',
            [
                'help' => __("primary de la table {0}", 'beanstalk_workers'),
            ]
        );
        $parserRestart->addArgument(
            'id',
            [
                'help' => __("primary de la table {0}", 'beanstalk_workers'),
            ]
        );
        return $parser;
    }

    /**
     * Lancement des workers
     * @throws Exception
     */
    public function main()
    {
        if (MaintenanceMiddleware::isInterrupted()) {
            $this->out('maintenance mode');
            $this->_stop();
        }
        $BeanstalkWorkers = TableRegistry::getTableLocator()->get('BeanstalkWorkers');
        $Exec = Utility::get('Exec');
        $this->out(__("Lancement des workers..."));

        $config = Configure::read('Beanstalk.workers');
        foreach ($config as $name => $args) {
            if (!$args['active']) {
                continue;
            }
            $count = $BeanstalkWorkers->find()
                ->where(['name' => $name])
                ->count();
            while ($count < $args['quantity']) {
                $this->out($name);
                $command = preg_replace('/^bin\/cake(\.bat|\.php)?/', ROOT.DS.'bin'.DS.'cake', $args['run']);
                $Exec->async($command);
                $count++;
            }
        }

        $this->out(__("Fin."));
    }

    /**
     * Donne la liste des workers lancés
     */
    public function listWorkers()
    {
        $data = $this->BeanstalkWorkers->find()
            ->select($fields = ['id', 'name', 'tube', 'hostname', 'pid', 'last_launch'])
            ->order(['name' => 'asc'])
            ->disableHydration()
            ->all()
            ->map(
                function ($data) {
                    foreach ($data as $k => $v) {
                        if ($v instanceof DateTimeInterface) {
                            $data[$k] = $v->format('Y-m-d H:i:s');
                        }
                        $data[$k] = (string)$data[$k];
                    }
                    return $data;
                }
            )
            ->toArray();
        array_unshift($data, $fields);

        $this->helper('Table')->output($data);
    }

    /**
     * Permet de tuer un worker par son id
     * @param string $id
     * @throws Exception
     */
    public function kill(string $id)
    {
        $result = self::killWorker($id);
        $this->out($result->stdout ?: "killed: id=$id");
        if ($result->stderr) {
            $this->abortShell($result->stderr);
        }
    }

    /**
     * Permet de tuer un worker par son id
     * @param string $id
     * @return CommandResult [
     *      'success' => code retour 0,
     *      'code' => contenu du &$return_var de la fonction exec,
     *      'stdout' => sorti standard,
     *      'stderr' => sorti d'erreur,
     *  ]
     * @throws Exception
     */
    public static function killWorker(string $id): CommandResult
    {
        $BeanstalkWorkers = TableRegistry::getTableLocator()->get('BeanstalkWorkers');
        $Exec = Utility::get('Exec');
        $worker = $BeanstalkWorkers->get($id);
        $config = Configure::read('Beanstalk.workers.'.$worker->get('name'));
        $kill = str_replace('{{pid}}', $worker->get('pid'), $config['kill']);
        $kill = str_replace('{{hostname}}', $worker->get('hostname'), $kill);
        return $Exec->command($kill);
    }

    /**
     * Demande l'arret d'un worker
     * @param int $id
     * @throws ZMQSocketException
     */
    public function stop($id)
    {
        $this->stopWorker($this->BeanstalkWorkers->get($id));
    }

    /**
     * Arrète un worker par entité
     * @param EntityInterface $worker
     * @throws ZMQSocketException
     */
    private function stopWorker(EntityInterface $worker)
    {
        $this->Beanstalk->setTube($worker->get('tube'));
        $this->Beanstalk->emit(
            [
                'command' => 'stopWorker',
                'beanstalk_worker_id' => (int)$worker->get('id'),
            ],
            1
        );
        $this->out(__("Arret demandé pour le worker {0} - {1}", $worker->id, h($worker->get('name'))));
    }

    /**
     * Ordre d'arret de tous les workers
     */
    public function stopAll()
    {
        $workers = $this->BeanstalkWorkers->find();
        /** @var BeanstalkWorker $worker */
        foreach ($workers as $worker) {
            $this->stopWorker($worker);
        }
    }

    /**
     * Donne la liste des workers actifs
     */
    public function list()
    {
        $this->listWorkers();
    }

    /**
     * Relance un worker par id
     * @param string $id
     * @throws ZMQSocketException
     */
    public function restart($id)
    {
        $this->restartWorker($this->BeanstalkWorkers->get($id));
    }

    /**
     * Relance un worker par entité
     * @param EntityInterface $worker
     * @throws ZMQSocketException
     */
    private function restartWorker(EntityInterface $worker)
    {
        $this->Beanstalk->setTube($worker->get('tube'));
        $this->Beanstalk->setTube($worker->get('tube'))->emit(
            [
                'command' => 'restartWorker',
                'beanstalk_worker_id' => $worker->get('id'),
            ],
            1
        );
        $this->out(__("Redémarrage demandé pour le worker {0} - {1}", $worker->id, h($worker->get('name'))));
    }

    /**
     * Relance tous les workers
     */
    public function restartAll()
    {
        $workers = $this->BeanstalkWorkers->find();
        /** @var BeanstalkWorker $worker */
        foreach ($workers as $worker) {
            $this->restartWorker($worker);
        }
    }

    /**
     * Effectue un kill sur les processes non stockés en base
     * @throws Exception
     */
    public function killZombies()
    {
        /** @var Exec $Exec */
        $Exec = Utility::get('Exec');
        $results = explode(
            PHP_EOL,
            trim($Exec->command("ps aux | grep 'bin\/cake.* worker '")->stdout)
        );
        $Workers = TableRegistry::getTableLocator()->get('BeanstalkWorkers');
        $hostname = gethostname();
        foreach ($results as $result) {
            if (preg_match('/^\S+\s+(\d+)\s.*bin\/cake(?:\.php|\/.bat)? worker (.*)( .*)?$/', $result, $m)) {
                $found = $Workers->find()
                    ->where(['pid' => $m[1], 'hostname' => $hostname])
                    ->count();
                if (!$found) {
                    $Exec->command('kill -9', $m[1]);
                    $this->out('killed '.$m[2].' pid '.$m[1]);
                }
            }
        }
    }

    /**
     * Permet de tuer tous les workers
     * @throws Exception
     */
    public function killAll()
    {
        foreach ($this->BeanstalkWorkers->find() as $worker) {
            if ($worker->get('running')) {
                $this->kill($worker->id);
            }
            $this->BeanstalkWorkers->delete($worker);
        }
        $this->success('done');
    }
}
