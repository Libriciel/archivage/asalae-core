<?php
/**
 * AsalaeCore\Command\MaintenanceScheduleCommand
 */

namespace AsalaeCore\Command;

use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use DateTime;
use DateTimeInterface;
use Exception;

/**
 * Gestion du mode maintenance programmé
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2024, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class MaintenanceScheduleCommand extends AbstractMaintenance
{
    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'maintenance schedule';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = parent::getOptionParser();
        $parser->addArgument(
            'begin_datetime',
            [
                'help' => __("Date de début de l'interruption programée sous la forme ISO (\"now\" fonctionne)"),
                'required' => true,
            ]
        );
        $parser->addArgument(
            'end_datetime',
            [
                'help' => __("Date de fin de l'interruption programée sous la forme ISO"),
            ]
        );
        $parser->addOption(
            'no-begin',
            [
                'help' => __("Si cette option est passée, seule la date de fin est mentionnée"),
                'boolean' => true,
            ]
        );
        return $parser;
    }

    /**
     * Action principale
     * @param Arguments $args The command arguments.
     * @param ConsoleIo $io   The console io
     * @throws Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        try {
            $begin = new DateTime($args->getArgument('begin_datetime'));
            $endArg = $args->getArgument('end_datetime');
            $end = $endArg ? new DateTime($endArg) : null;
        } catch (Exception $e) {
            $io->abort(__("Format de date incorrecte"));
        }
        $config = $this->getConfig();

        if ($args->getOption('no-begin')) {
            if ($end) {
                $io->abort(__("Si l'option --no-begin est mentionnée, il ne faut pas de 2e argument"));
            }

            unset($config['scheduled']['begin']);
            $config['scheduled']['end'] = $begin->format(DateTimeInterface::ATOM);
            $config['enabled'] = true;
            $this->setConfig($config);
            $io->success(sprintf('Maintenance is ON until %s', $begin->format(DateTimeInterface::ATOM)));
            return 0;
        }

        $config['scheduled']['begin'] = $begin->format(DateTimeInterface::ATOM);
        if ($end) {
            $config['scheduled']['end'] = $end->format(DateTimeInterface::ATOM);
            $this->setConfig($config);
            $io->success(
                sprintf(
                    'Maintenance is scheduled for %s to %s',
                    $begin->format(DateTimeInterface::ATOM),
                    $end->format(DateTimeInterface::ATOM),
                )
            );
            return 0;
        }

        unset($config['scheduled']['end']);
        $this->setConfig($config);
        $io->success(sprintf('Maintenance is scheduled for %s', $begin->format(DateTimeInterface::ATOM)));
    }
}
