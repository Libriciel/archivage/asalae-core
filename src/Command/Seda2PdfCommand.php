<?php
/**
 * AsalaeCore\Command\Seda2PdfCommand
 */

namespace AsalaeCore\Command;

use Exception;
use DOMDocument;
use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\I18n\Number;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Seda2Pdf\Seda2Pdf;

/**
 * Création d'un pdf à partir d'un SEDA en ligne de commande
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2020, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Seda2PdfCommand extends Command
{
    /**
     * Gets the option parser instance and configures it.
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = new ConsoleOptionParser();
        $parser->addArgument(
            'seda',
            [
                'help' => __("Fichier SEDA au format XML"),
                'required' => true,
            ]
        );
        $parser->addArgument(
            'out',
            [
                'help' => __("Fichier PDF de sortie"),
                'required' => true,
            ]
        );
        $parser->addOption(
            'force',
            [
                'help' => __("Supprime le fichier de destination s'il existe déjà"),
                'boolean' => true,
            ]
        );
        return $parser;
    }

    /**
     * Méthode principale
     *
     * @param Arguments $args The command arguments.
     * @param ConsoleIo $io   The console io
     * @throws Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $seda = realpath($args->getArgument('seda'));
        $pdf = $args->getArgument('out');
        if (!is_file($seda) || !is_readable($seda)) {
            $io->abort(__("Le fichier SEDA n'existe pas"));
        }
        $dom = new DOMDocument;
        if (!$dom->load($seda)) {
            $io->abort(__("Le fichier SEDA n'est pas un xml valide"));
        }
        try {
            $seda2pdf = new Seda2Pdf($dom);
        } catch (Exception $e) {
            $io->abort($e->getMessage());
        }
        if (is_file($pdf) && !$args->getOption('force')) {
            $io->abort(__("Le fichier de destination existe déjà. Pour passer outre, ajoutez l'option --force"));
        } elseif (is_file($pdf)) {
            unlink($pdf);
        } elseif (!is_writable(dirname($pdf))) {
            $io->abort(__("Le dossier de destination ({0}) n'est pas inscriptible", dirname($pdf)));
        }

        $begin = microtime(true);
        $seda2pdf->generate($pdf);
        $pdf = realpath($pdf);
        $duration = round(microtime(true) - $begin, 4);
        $size = filesize($pdf);
        $io->success(__("Le fichier {0} a été créé avec succès.", $pdf));
        $io->out('XML: '.Number::toReadableSize(filesize($seda)));
        $io->out('PDF: '.Number::toReadableSize($size));
        $io->out(__("Conversion effectuée en {0} secondes", $duration));
    }
}
