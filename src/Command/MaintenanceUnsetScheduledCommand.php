<?php
/**
 * AsalaeCore\Command\MaintenanceUnsetScheduledCommand
 */

namespace AsalaeCore\Command;

use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Exception;

/**
 * Retrait du mode maintenance programmé
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2024, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class MaintenanceUnsetScheduledCommand extends AbstractMaintenance
{
    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'maintenance unset scheduled';
    }

    /**
     * Action principale
     * @param Arguments $args The command arguments.
     * @param ConsoleIo $io   The console io
     * @throws Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $config = $this->getConfig();
        unset($config['scheduled']);
        $this->setConfig($config);
        $io->success('Scheduled maintenance has been unset.');
    }
}
