<?php
/**
 * AsalaeCore\Command\TreeCommand
 */

namespace AsalaeCore\Command;

use Cake\Command\Command;
use Cake\Console\ConsoleOptionParser;
use Cake\Core\Configure;
use Cake\I18n\FrozenTime as Time;
use Cake\ORM\Behavior\TreeBehavior;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Utility\Inflector;
use Exception;

/**
 * Permet de réparer un arbre (bdd)
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2019, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class TreeCommand extends Command
{
    /**
     * Traits
     */
    use CommandShellTrait;

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = new ConsoleOptionParser;
        $parserRepair = new ConsoleOptionParser;
        $parserRepairAll = new ConsoleOptionParser;
        $parser->addSubcommand(
            'repair',
            [
                'help' => __("Effectue un Tree::recover sur le model de votre choix"),
                'parser' => $parserRepair,
            ]
        );
        $parser->addSubcommand(
            'repair_all',
            [
                'help' => __("Effectue un Tree::recover sur tous les models"),
                'parser' => $parserRepairAll,
            ]
        );
        $parserRepair->addArgument(
            'model_name',
            [
                'help' => __("Nom du model sur lequel effectuer un recover"),
                'required' => true,
            ]
        );
        return $parser;
    }

    /**
     * Fonction de réparation
     * @param string $model
     * @throws Exception
     */
    public function repair(string $model)
    {
        $modelName = Inflector::camelize($model);
        /** @var Table|TreeBehavior $Model */
        $Model = TableRegistry::getTableLocator()->get($modelName);
        if (!$Model->hasBehavior('Tree')) {
            throw new Exception('TreeBehavior nor found on '.$modelName);
        }
        $this->out(__("Réparation de {0} commencée à {1}", $modelName, (new Time)->toTimeString()));
        $Model->recover();
        $this->out(__("Réparation de {0} terminée à {1}", $modelName, (new Time)->toTimeString()));
    }

    /**
     * Répare tous les models compatibles
     * @throws Exception
     */
    public function repairAll()
    {
        $loc = TableRegistry::getTableLocator();
        $basePath = Configure::read('Tree.model_path', APP.'Model'.DS.'Table');
        foreach (glob($basePath.DS.'*Table.php') as $filename) {
            $modelname = basename($filename, 'Table.php');
            $Model = $loc->get($modelname);
            if ($Model->hasBehavior('Tree')) {
                $this->repair($modelname);
            }
        }
    }
}
