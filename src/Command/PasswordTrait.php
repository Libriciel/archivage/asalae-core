<?php
/**
 * AsalaeCore\Command\PasswordTrait
 */

namespace AsalaeCore\Command;

use AsalaeCore\Factory\Utility;
use Exception;

/**
 * Ensemble de méthodes pour la manipulation de mot de passe
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2017, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
trait PasswordTrait
{

    /**
     * Interactively prompts for input without echoing to the terminal.
     * Requires a bash shell or Windows and won't work with
     * safe_mode settings (Uses `shell_exec`)
     *
     * @see http://stackoverflow.com/questions/187736/command-line-password-prompt-in-php
     * @param string $prompt Valeur affiché pour demander le mot de passe
     * @return string le mot de passe tapé par l'utilisateur
     * @throws Exception
     */
    public function enterPassword(string $prompt = ''): string
    {
        if (empty($prompt)) {
            $prompt = __("Enter Password:");
        }
        $prompt .= "\n";
        $exec = Utility::get('Exec');
        if (preg_match('/^win/i', PHP_OS)) {
            $vbscript = sys_get_temp_dir() . 'prompt_password.vbs';
            file_put_contents(
                $vbscript,
                'wscript.echo(InputBox("'
                . trim(addslashes($prompt))
                . '", "", "password here"))'
            );
            $command = "cscript //nologo " . escapeshellarg($vbscript);
            $password = rtrim($exec->command($command)->stdout);
            unlink($vbscript);
        } else {
            $command = "/usr/bin/env bash -c 'echo OK'";
            $stdr = rtrim($exec->rawCommand($command));
            if ($stdr !== 'OK') {
                trigger_error(__("Can't invoke bash: {0}", $stdr));
                return '';
            }
            $command = "/usr/bin/env bash -c 'read -s -p \""
                . addslashes($prompt)
                . "\" mypassword && echo \$mypassword'";
            $password = rtrim($exec->rawCommand($command));
            if (method_exists($this, 'out')) {
                $this->out('');
            } elseif (isset($this->io)) {
                $this->io->out();
            }
        }
        return $password;
    }
}
