<?php
/**
 * AsalaeCore\Command\MaintenanceCommand
 */

namespace AsalaeCore\Command;

use AsalaeCore\Exception\GenericException;
use AsalaeCore\Utility\Config;
use Cake\Command\Command;
use Cake\Utility\Hash;
use Exception;

/**
 * Gestion du mode maintenance
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2021, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
abstract class AbstractMaintenance extends Command
{
    /**
     * Donne la config de Interruption ou renvoi une exception
     * @return array
     * @throws Exception
     */
    protected function getConfig(): array
    {
        $localJson = Config::getPathToLocal();
        if (!file_exists($localJson) || !is_writable($localJson)) {
            throw new GenericException("$localJson does not exist or is not writable");
        }
        return Config::readAll('Interruption', []);
    }

    /**
     * Donne la config de Interruption ou renvoi une exception
     * @param array $config
     * @return void
     */
    protected function setConfig(array $config): void
    {
        Config::writeAndSave(
            'Interruption',
            Hash::filter($config, fn($v) => $v !== null && $v !== [])
        );
    }
}
