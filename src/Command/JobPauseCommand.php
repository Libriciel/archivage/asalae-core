<?php
/**
 * AsalaeCore\Command\JobPauseCommand
 */

namespace AsalaeCore\Command;

use AsalaeCore\Factory\Utility;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\ORM\TableRegistry;
use Exception;
use Pheanstalk\Exception\ServerException;

/**
 * Permet de mettre un job en pause
 * ex: bin/cake job pause 4531
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2021, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */
class JobPauseCommand extends Command
{
    /**
     * Get the command name.
     * @return string
     */
    public static function defaultName(): string
    {
        return 'job pause';
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return ConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = parent::getOptionParser();
        $parser->addArgument(
            'jobid',
            [
                'help' => __("Identifiant beanstalkd du job"),
                'required' => true,
            ]
        );
        return $parser;
    }

    /**
     * Action principale
     * @param Arguments $args The command arguments.
     * @param ConsoleIo $io   The console io
     * @throws Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $Jobs = TableRegistry::getTableLocator()->get('BeanstalkJobs');
        $entity = $Jobs->find()
            ->where(['BeanstalkJobs.jobid' => $args->getArgument('jobid')])
            ->first();
        if (!$entity) {
            $io->abort(
                __(
                    "Le job (jobid={0}) n'a pas été trouvé en base de données",
                    $args->getArgument('jobid')
                )
            );
        }
        $Beanstalk = Utility::get('Beanstalk');
        $Beanstalk->setTube($entity->get('tube'));
        try {
            $Beanstalk->selectJob($entity->get('jobid'))->reserve()->fail('pause');
        } catch (ServerException $e) {
            $io->abort($e->getMessage());
        }
        $io->out('done');
    }
}
