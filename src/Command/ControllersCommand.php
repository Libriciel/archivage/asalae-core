<?php
/**
 * AsalaeCore\Command\ControllersCommand
 */

namespace AsalaeCore\Command;

use AsalaeCore\Model\Table\AcosTable;
use Cake\Command\Command;
use Cake\Console\ConsoleOptionParser;
use Cake\Console\ConsoleOptionParser as CakeConsoleOptionParser;
use Cake\Core\App;
use Cake\Core\Configure;
use Cake\Error\Debugger;
use Cake\Utility\Hash;
use Cake\Utility\Inflector;
use ReflectionClass;
use ReflectionException;

/**
 * Shell de modification du controllers.json et des Acos
 *
 * @category    Command
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2019, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 * @property AcosTable $Acos
 */
class ControllersCommand extends Command
{
    /**
     * Traits
     */
    use CreateAcosTrait, CommandShellTrait;

    /**
     * Gets the option parser instance and configures it.
     *
     * By overriding this method you can configure the ConsoleOptionParser before returning it.
     *
     * @return CakeConsoleOptionParser
     * @link https://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     */
    public function getOptionParser(): CakeConsoleOptionParser
    {
        $parser = new ConsoleOptionParser();
        $parserGet = new ConsoleOptionParser();
        $parserList = new ConsoleOptionParser();
        $parserCheck = new ConsoleOptionParser();
        $parser->addSubcommand(
            'get',
            [
                'help' => __("Donne le paramétrage effectué pour une action"),
                'parser' => $parserGet,
            ]
        );
        $parserGet->addArgument(
            'target',
            [
                'help' => __("Action sous la forme Controller/action"),
            ]
        );
        $parserGet->addOption(
            'plugin',
            [
                'help' => __("Cible un plugin"),
            ]
        );

        $parser->addSubcommand(
            'list',
            [
                'help' => __("Donne la liste des actions"),
                'parser' => $parserList,
            ]
        );
        $parserList->addArgument(
            'controller',
            [
                'help' => __("Filtre les résultats sur un unique Controller"),
            ]
        );
        $parserList->addOption(
            'plugin',
            [
                'help' => __("Cible un plugin"),
            ]
        );

        $parser->addArgument(
            'target',
            [
                'help' => __("Action sous la forme Controller/action"),
            ]
        );
        $parser->addOption(
            'invisible',
            [
                'short' => 'i',
                'boolean' => true,
                'help' => __("Affiche l'action dans l'édition des permissions"),
            ]
        );
        $parser->addOption(
            'accesParDefaut',
            [
                'short' => 'a',
                'boolean' => true,
                'help' => __(
                    "Donne un accès à tous les aros déjà créés,"
                    ." et ajoutera un accès lors de la création d'un nouvel aro"
                ),
            ]
        );
        $parser->addOption(
            'commeDroits',
            [
                'short' => 'c',
                'help' => __(
                    "L'aco créé sera déclaré enfant de la cible défini ici. Syntaxe: NomDuController::nomDeLaction"
                ),
            ]
        );
        $parser->setDescription(
            [
                __(
                    "Permet d'ajouter une entrée dans controllers.json"
                    ." suite à l'ajout d'une action (évite de passer par /devs/index)"
                )
            ]
        );
        $parser->addOption(
            'interactive',
            [
                'help' => __("Demandera toutes les options si à Yes"),
                'default' => 'y',
                'choice' => ['y', 'n']
            ]
        );
        $parser->addOption(
            'force',
            [
                'help' => __("Ne contrôle pas les choix possibles"),
                'boolean' => true,
            ]
        );
        $parser->addOption(
            'plugin',
            [
                'help' => __("Cible un plugin"),
            ]
        );

        $parser->addSubcommand(
            'check',
            [
                'help' => __("Vérifi controllers.json"),
                'parser' => $parserCheck,
            ]
        );
        return $parser;
    }

    /**
     * Action principale
     * @param null $args
     * @throws ReflectionException
     */
    public function main($args = null)
    {
        $this->Acos = $this->fetchTable('Acos');
        $root = $this->Acos->find()
            ->select(['id', 'lft', 'rght'])
            ->where(['model' => 'root', 'alias' => 'controllers'])
            ->first();
        if (!$root) {
            $this->abortShell(__("Les permissions n'ont pas été initialisés"));
        }
        $controllers = $this->getControllersActions();
        $pathToControllersJson = Configure::read(
            'App.paths.controllers_rules',
            RESOURCES . 'controllers.json'
        );
        $data = json_decode(file_get_contents($pathToControllersJson), true);
        if ($args === null) {
            $args = $this->askControllerAction($data, $controllers);
        }
        $params = $this->param('interactive') === 'y'
            ? $this->askParams($data, $controllers, $args)
            : [
                'invisible' => (string)(int)$this->param('invisible'),
                'accesParDefaut' => (string)(int)$this->param('accesParDefaut'),
                'commeDroits' => $this->param('commeDroits') ?: '',
            ];

        if ($this->param('commeDroits')) {
            [$controller, $action] = explode('::', $this->param('commeDroits'));
            if (!$this->checkExistance($controller, $action)) {
                $this->abortShell(__("l'action du commeDroits n'a pas été trouvée"));
            }
        }
        while (empty($args)
            || !preg_match('/^([A-Za-z\d.-]+)\/([A-Za-z\d-]+)$/', $args, $match)
            || !$this->checkExistance($match[1], $match[2])
        ) {
            $this->out("La cible doit être indiquée sous la forme Controllers/actionEnCamelCase");
            $args = $this->in(__("Veuillez saisir une cible"));
        }
        $pathToControllersJson = Configure::read(
            'App.paths.controllers_rules',
            RESOURCES . 'controllers.json'
        );
        $data = json_decode(file_get_contents($pathToControllersJson), true);
        [, $controller, $action] = $match;
        $controller = Inflector::camelize($controller, '-');
        $action = Inflector::variable(Inflector::camelize($action, '-'));
        if (!isset($data[$controller])) {
            $data[$controller] = [];
        }
        $data[$controller][$action] = $params;
        ksort($data[$controller]);
        ksort($data);

        file_put_contents($pathToControllersJson, json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));

        if (!$this->param('commeDroits')) {
            $parent = $this->Acos->findOrCreate(
                [
                    'model' => 'controllers',
                    'alias' => $controller,
                    'parent_id' => $root->get('id')
                ]
            );
        } else {
            [$cdController, $cdAction] = explode('::', $this->param('commeDroits'));
            $parent = $this->Acos->find()
                ->select(['id'])
                ->where(
                    [
                        'model' => $cdController,
                        'alias' => $cdAction,
                        'lft >=' => $root->get('lft'),
                        'rght <=' => $root->get('rght'),
                    ]
                )
                ->firstOrFail();
        }
        $aco = $this->Acos->findOrCreate(
            [
                'model' => $controller,
                'alias' => $action,
                'parent_id' => $parent->get('id')
            ]
        );

        if ($this->param('accesParDefaut')) {
            $this->allowForAll($aco);
        }

        $this->out('done');
    }

    /**
     * Vérifi l'existance d'une action
     * @param string $controller
     * @param string $action
     * @return bool
     */
    private function checkExistance(string $controller, string $action): bool
    {
        $controller = Inflector::camelize($controller, '-');
        $action = Inflector::variable(Inflector::camelize($action, '-'));
        $plugin = $this->param('plugin');
        $classname = App::className(
            ($plugin ? $plugin . '.' : '') . $controller,
            'Controller',
            'Controller'
        );
        if (!$classname) {
            return false;
        }
        return method_exists($classname, $action);
    }

    /**
     * Donne le paramétrage effectué pour une action
     * @param null $target Controllers/action
     */
    public function get($target = null)
    {
        while (empty($target)
            || !preg_match('/^([A-Za-z\d.-]+)\/([A-Za-z\d-]+)$/', $target, $match)
            || !$this->checkExistance($match[1], $match[2])
        ) {
            $this->out("La cible doit être indiquée sous la forme Controllers/actionEnCamelCase");
            $target = $this->in(__("Veuillez saisir une cible"));
        }
        $pathToControllersJson = Configure::read(
            'App.paths.controllers_rules',
            RESOURCES . 'controllers.json'
        );
        $data = json_decode(file_get_contents($pathToControllersJson), true);
        [, $controller, $action] = $match;
        $controller = Inflector::camelize($controller, '-');
        $action = Inflector::variable(Inflector::camelize($action, '-'));
        if (!isset($data[$controller])) {
            $data[$controller] = [];
        }
        if (!isset($data[$controller][$action])) {
            $data[$controller][$action] = [];
        }
        $this->out(Debugger::exportVarAsPlainText($data[$controller][$action]));
    }

    /**
     * Donne la liste des Controller/action (rouge si ils ne sont pas dans controllers.json)
     * @param null $controllerName
     * @throws ReflectionException
     */
    public function list($controllerName = null)
    {
        $controllers = $this->getControllersActions();
        $pathToControllersJson = Configure::read(
            'App.paths.controllers_rules',
            RESOURCES . 'controllers.json'
        );
        $data = json_decode(file_get_contents($pathToControllersJson), true);
        if ($controllerName) {
            $controllers = [$controllerName => $controllers[$controllerName] ?? []];
        }
        $error = 0;
        foreach ($controllers as $controller => $actions) {
            foreach ($actions as $action) {
                if (Hash::get($data, $controller.'.'.$action)) {
                    $this->out("$controller/$action");
                } else {
                    $this->err("$controller/$action");
                    $error++;
                }
            }
        }
        if ($error) {
            $this->abortShell(__n("Il manque une action", "Il manque {0} actions", $error, $error));
        }
    }

    /**
     * Donne un array avec le nom du controlleur en clé et le nom de l'action en valeur
     * @return array
     * @throws ReflectionException
     */
    private function getControllersActions()
    {
        $controllers = [];
        $plugin = $this->param('plugin');
        $namespace = Configure::read('App.namespace');
        $appControllerMethods = class_exists($namespace."\\Controller\\AppController")
            ? get_class_methods($namespace."\\Controller\\AppController")
            : [];
        if (!$appControllerMethods) {
            $appControllerMethods = get_class_methods(
                $namespace."\\Controller\\Controller"
            );
        }
        foreach (App::classPath('Controller', $plugin) as $dir) {
            foreach (glob($dir.'*Controller.php') as $filename) {
                $controller = basename($filename, 'Controller.php');
                $classname = App::className(
                    ($plugin ? $plugin . '.' : '') . $controller,
                    'Controller',
                    'Controller'
                );
                if (!$classname) {
                    continue;
                }
                $refl = new ReflectionClass($classname);
                if ($refl->isAbstract() || $refl->isTrait() || $refl->isInterface()) {
                    continue;
                }
                $methods = [];
                foreach ($refl->getMethods() as $method) {
                    if ($method->isPublic() && !$method->isStatic() && $method->class === $classname) {
                        $methods[] = $method->getName();
                    }
                }
                $controllers[$controller] = array_diff($methods, $appControllerMethods);
            }
        }
        return $controllers;
    }

    /**
     * Mode interactif
     * @param array $data
     * @param array $controllers
     * @return string
     */
    private function askControllerAction(array $data, array $controllers): string
    {
        $missings = [];
        foreach ($controllers as $controller => $actions) {
            foreach ($actions as $action) {
                if (!Hash::get($data, $controller.'.'.$action)) {
                    $missings[$controller][$action] = true;
                }
            }
        }
        if (!$missings) {
            $this->warn(__("Toutes les actions ont été définies"));
            $defaultController = null;
        } else {
            $defaultController = current(array_keys($missings));
        }
        $controller = $this->in(
            __("Sélectionner un Controller"),
            $this->param('force') ? null : array_keys($controllers),
            $defaultController
        );
        if (empty($missings[$controller])) {
            $this->warn(__("Toutes les actions ont été définies"));
            $defaultAction = null;
        } else {
            $defaultAction = current(array_keys($missings[$controller]));
        }
        $action = $this->in(
            __("Sélectionner une action"),
            $this->param('force') ? null : $controllers[$controller],
            $defaultAction
        );
        return $controller.'/'.$action;
    }

    /**
     * Paramètres de l'action (commeDroit, invisible, etc..)
     * @param array  $data
     * @param array  $controllers
     * @param string $args
     * @return array
     */
    private function askParams(
        array $data,
        array $controllers,
        string $args
    ): array {
        $params = [
            'invisible' => (string)(int)$this->param('invisible'),
            'accesParDefaut' => (string)(int)$this->param('accesParDefaut'),
            'commeDroits' => $this->param('commeDroits') ?: '',
        ];
        if (!$params['commeDroits']) {
            $params['commeDroits'] = $this->in(
                __("L'action hérite des permissions d'accès d'une autre action ?"),
                ['y', 'n'],
                'n'
            );
            if ($params['commeDroits'] === 'y') {
                $controller = $this->in(
                    __("Sélectionner un Controller"),
                    $this->param('force') ? null : array_keys($controllers),
                    explode('/', $args)[0]
                );
                $action = $this->in(
                    __("Sélectionner une action"),
                    $this->param('force') ? null : $controllers[$controller]
                );
                $params['commeDroits'] = Hash::get($data, $controller.'.'.$action.'.commeDroits')
                    ?: $controller.'::'.$action;
            } else {
                $params['commeDroits'] = '';
            }
        }
        if (!$params['commeDroits'] && $this->param('invisible') === false) {
            $params['invisible'] = $this->in(
                __("L'action est-elle visible dans les permissions ?"),
                ['y', 'n'],
                'y'
            ) === 'y' ? '0' : '1';
        }
        if (!$params['commeDroits'] && $this->param('accesParDefaut') === false) {
            $params['accesParDefaut'] = $this->in(
                __("L'action est-elle accessible par défaut ?"),
                ['y', 'n'],
                'n'
            ) === 'y' ? '1' : '0';
        }
        if (!$params['commeDroits'] && $params['invisible'] === '0') {
            $pathToControllersGroupJson = Configure::read(
                'App.paths.controllers_group',
                RESOURCES . 'controllers_group.json'
            );
            $groups = json_decode(file_get_contents($pathToControllersGroupJson), true);
            $groups[] = 'empty';
            foreach ($groups as $key => $group) {
                $this->info(sprintf('%d - %s', $key, $group));
            }
            $key = $this->in(
                __("Indiquer le groupe de la permission"),
                $this->param('force') ? null : array_keys($groups)
            );
            $params['group'] = $groups[$key] ?? $key;
            if ($params['group'] === 'empty') {
                unset($params['group']);
            }
        }
        return $params;
    }

    /**
     * Vérifi controllers.json
     * @return void
     * @throws ReflectionException
     */
    public function check()
    {
        $controllers = $this->getControllersActions();

        $pathToControllersJson = Configure::read(
            'App.paths.controllers_rules',
            RESOURCES . 'controllers.json'
        );
        $data = json_decode(file_get_contents($pathToControllersJson), true);
        $error = false;
        foreach ($data as $controller => $actions) {
            foreach (array_keys($actions) as $action) {
                if (!isset($controllers[$controller])) {
                    $error = true;
                    $this->err(__("Le controller {0} n'a pas été trouvé dans l'application", $controller));
                } elseif (!in_array($action, $controllers[$controller])) {
                    $error = true;
                    $this->err(
                        __(
                            "L'action {0} : {1} n'a pas été trouvé dans l'application",
                            $controller,
                            $action
                        )
                    );
                }
            }
        }
        foreach ($controllers as $controller => $actions) {
            foreach ($actions as $action) {
                if (!Hash::get($data, $controller.'.'.$action)) {
                    $this->err(
                        __(
                            "L'action {0} : {1} n'est pas définie dans controllers.json",
                            $controller,
                            $action
                        )
                    );
                    $error = true;
                }
            }
        }
        if (!$error) {
            $this->success('ok');
        }
    }
}
