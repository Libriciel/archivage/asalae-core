<?php
/**
 * AsalaeCore\ORM\Entity
 */

namespace AsalaeCore\ORM;

use Cake\Datasource\EntityInterface;
use Cake\Datasource\InvalidPropertyInterface;
use Cake\ORM\Entity as CakeEntity;
use Cake\Utility\Hash;
use Throwable;

/**
 * Surcharge de Entity
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2022, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Entity extends CakeEntity implements EntityInterface, InvalidPropertyInterface
{

    /**
     * Entité déjà controllé
     *
     * @var bool
     */
    protected $_disableHasErrors = false;

    /**
     * Returns whether this entity has errors.
     *
     * @param bool $includeNested true will check nested entities for hasErrors()
     * @return bool
     */
    public function hasErrors(bool $includeNested = true): bool
    {
        if (Hash::filter($this->_errors)) {
            return true;
        }

        if ($includeNested === false || $this->_disableHasErrors) {
            return false;
        }

        $this->_disableHasErrors = true;
        foreach ($this->_fields as $field) {
            if ($this->_readHasErrors($field)) {
                return true;
            }
        }
        $this->_disableHasErrors = false;

        return false;
    }

    /**
     * Returns an array that can be used to describe the internal state of this
     * object.
     *
     * @return array<string, mixed>
     */
    public function __debugInfo(): array
    {
        try {
            $fields = $this->_fields;
            foreach ($this->_virtual as $field) {
                $fields[$field] = $this->$field;
            }

            return $fields + [
                '[new]' => $this->isNew(),
                '[accessible]' => $this->_accessible,
                '[dirty]' => $this->_dirty,
                '[original]' => $this->_original,
                '[virtual]' => $this->_virtual,
                '[hasErrors]' => $this->hasErrors(),
                '[errors]' => $this->_errors,
                '[invalid]' => $this->_invalid,
                '[repository]' => $this->_registryAlias,
            ];
        } catch (Throwable $e) {
            dforcelog(
                sprintf(
                    '__debugInfo failed on %s (id=%d)',
                    get_class($this),
                    $this->id
                )
            );
            return $fields;
        }
    }
}
