<?php
/**
 * AsalaeCore\ORM\Locator\TableLocator
 */

namespace AsalaeCore\ORM\Locator;

use Cake\Error\Debugger;
use Cake\ORM\Locator\TableLocator as CakeTableLocator;
use Cake\ORM\Table;
use RuntimeException;

/**
 * Surcharge de TableLocator
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2022, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class TableLocator extends CakeTableLocator
{
    /**
     * @var string[] [model => backtrace]
     */
    private array $origins = [];

    /**
     * Get a table instance from the registry.
     *
     * Tables are only created once until the registry is flushed.
     * This means that aliases must be unique across your application.
     * This is important because table associations are resolved at runtime
     * and cyclic references need to be handled correctly.
     *
     * The options that can be passed are the same as in {@link \Cake\ORM\Table::__construct()}, but the
     * `className` key is also recognized.
     *
     * ### Options
     *
     * - `className` Define the specific class name to use. If undefined, CakePHP will generate the
     *   class name based on the alias. For example 'Users' would result in
     *   `App\Model\Table\UsersTable` being used. If this class does not exist,
     *   then the default `Cake\ORM\Table` class will be used. By setting the `className`
     *   option you can define the specific class to use. The className option supports
     *   plugin short class references {@link \Cake\Core\App::shortName()}.
     * - `table` Define the table name to use. If undefined, this option will default to the underscored
     *   version of the alias name.
     * - `connection` Inject the specific connection object to use. If this option and `connectionName` are undefined,
     *   The table class' `defaultConnectionName()` method will be invoked to fetch the connection name.
     * - `connectionName` Define the connection name to use. The named connection will be fetched from
     *   {@link \Cake\Datasource\ConnectionManager}.
     *
     * *Note* If your `$alias` uses plugin syntax only the name part will be used as
     * key in the registry. This means that if two plugins, or a plugin and app provide
     * the same alias, the registry will only store the first instance.
     *
     * @param string               $alias   The alias name you want to get. Should be in CamelCase format.
     * @param array<string, mixed> $options The options you want to build the table with.
     *                                      If a table has already been loaded the options will be ignored.
     * @return Table
     * @throws RuntimeException When you try to configure an alias that already exists.
     */
    public function get(string $alias, array $options = []): Table
    {
        if (!isset($this->instances[$alias])) {
            $this->origins[$alias] = dlast(1, null, false);
        }
        try {
            $storeOptions = $options;
            unset($storeOptions['allowFallbackClass']);

            if (isset($this->instances[$alias])) {
                if (!empty($storeOptions) && !$this->compareOptions($alias, $storeOptions)) {
                    throw new RuntimeException(
                        sprintf(
                            'You cannot configure "%s", it already exists in the registry.',
                            $alias
                        )
                    );
                }

                return $this->instances[$alias];
            }

            $this->options[$alias] = $storeOptions;

            return $this->instances[$alias] = $this->createInstance($alias, $options);
        } catch (RuntimeException $e) {
            if ($options) {
                $definedOptions = $this->getOptions($alias);
                throw new RuntimeException(
                    __(
                        "Différence d'initialisation du model {0}\n\n"
                        ."options initiales: {1}\n{2}\n\n"
                        ."options actuelles: {3}\n{4}",
                        $alias,
                        Debugger::exportVarAsPlainText($definedOptions),
                        $this->origins[$alias],
                        Debugger::exportVarAsPlainText($options + $definedOptions),
                        dlast(1, null, false)
                    ),
                    500,
                    $e
                );
            } else {
                throw $e;
            }
        }
    }

    /**
     * Compare les options avec celles enregistrées
     * @param string $alias
     * @param array  $options
     * @return bool vrai si les options sont identiques
     */
    private function compareOptions(string $alias, array $options): bool
    {
        $definedOptions = $this->getOptions($alias);
        $options += $definedOptions;
        ksort($definedOptions);
        ksort($options);
        return $definedOptions === $options;
    }

    /**
     * Donne les options liés à un alias
     * @param string $alias
     * @return array
     */
    private function getOptions(string $alias): array
    {
        return $this->options[$alias] + [
            'className' => $this->instances[$alias]->getRegistryAlias(),
            'connectionName' => $this->instances[$alias]->getConnection()->configName(),
        ];
    }
}
