<?php
/**
 * AsalaeCore\ORM\Marshaller
 */

namespace AsalaeCore\ORM;

use AsalaeCore\DataType\CommaArrayString;
use AsalaeCore\DataType\EncryptedString;
use AsalaeCore\DataType\JsonString;
use AsalaeCore\DataType\SerializedObject;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Marshaller as CakeMarshaller;
use ReflectionClass;
use ReflectionException;

/**
 * Surcharge de Marshaller
 *
 * @category Entity
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2023, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class Marshaller extends CakeMarshaller
{
    /**
     *
     * @param array $data
     * @param array $options
     * @return EntityInterface
     */
    public function one(array $data, array $options = []): EntityInterface
    {
        $entity = parent::one($data, $options);
        if ($entity->isNew()) {
            return $entity;
        }
        foreach ($entity->toArray() as $field => $value) {
            if (!isset($data[$field])) {
                continue;
            }
            if ($value instanceof CommaArrayString) {
                $class = CommaArrayString::class;
            } elseif ($value instanceof EncryptedString) {
                $class = EncryptedString::class;
            } elseif ($value instanceof JsonString) {
                $class = JsonString::class;
            } elseif ($value instanceof SerializedObject) {
                $class = SerializedObject::class;
            } else {
                continue;
            }
            $original = (string)$entity->getOriginal($field);
            if (is_string($data[$field])) {
                $dataValue = $class::createFromString($data[$field]);
            } else {
                $dataValue = new $class($data[$field]);
            }
            $entity->setDirty($field, $original !== (string)$dataValue);
        }

        return $entity;
    }

    /**
     * Returns data and options prepared to validate and marshall.
     *
     * @param array                $data    The data to prepare.
     * @param array<string, mixed> $options The options passed to this marshaller.
     * @return array An array containing prepared data and options.
     * @throws ReflectionException
     */
    protected function _prepareDataAndOptions(array $data, array $options): array
    {
        $preparedData = parent::_prepareDataAndOptions($data, $options);

        $relf = new ReflectionClass($this->_table->getEntityClass());
        $entity = $relf->newInstanceWithoutConstructor();
        foreach ($entity->jsonFields ?? ['app_meta'] as $jsonField) {
            if (isset($data[$jsonField])) {
                $preparedData[0][$jsonField] = (string)JsonString::create($data[$jsonField]);
            }
        }
        return $preparedData;
    }
}
