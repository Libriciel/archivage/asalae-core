<?php
/**
 * AsalaeCore\TestSuite\TestCase
 */

namespace AsalaeCore\TestSuite;

use AsalaeCore\Exception\GenericException;
use Cake\Core\App;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\Fixture\FixtureStrategyInterface;
use AsalaeCore\TestSuite\Fixture\TruncateStrategy;
use Cake\TestSuite\TestCase as CakeTestSuite;

/**
 * TestCase avec fixtures settable
 *
 * @category TestSuite
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020, Libriciel
 * @license   http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html
 */
class TestCase extends CakeTestSuite
{
    /**
     * @var array
     */
    public $genericSessionData = [];

    /**
     * Constructor
     * @param string|null $name
     * @param array       $data
     * @param int|string  $dataName
     * @noinspection RedundantSuppression
     */
    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        if (defined('GENERIC_SESSION_DATA')) {
            /** @noinspection PhpUndefinedConstantInspection */
            $this->genericSessionData = unserialize(GENERIC_SESSION_DATA);
        }
    }

    /**
     * Destruction du test
     * @return void
     */
    public function tearDown(): void
    {
        parent::tearDown();
        $loc = TableRegistry::getTableLocator();
        $paths = App::classPath('Model/Table');
        $models = glob($paths[0] . '*Table.php');
        $missingFixtures = [];
        foreach ($models as $file) {
            $name = basename($file, 'Table.php');
            $model = $loc->get($name);
            if ($model->find()->count()) {
                $missingFixtures[] = $model->getAlias();
            }
        }
        if ($missingFixtures) {
            sort($missingFixtures);
            throw new GenericException(
                __("Il manque {0} dans les fixtures", implode(', ', $missingFixtures))
            );
        }
        $loc->clear();
    }

    /**
     * Setter de fixtures
     * @param array $value
     */
    public function setFixture(array $value): void
    {
        $this->fixtures = $value;
    }

    /**
     * Returns fixture strategy used by these tests.
     *
     * @return \Cake\TestSuite\Fixture\FixtureStrategyInterface
     */
    protected function getFixtureStrategy(): FixtureStrategyInterface
    {
        $instance = new TruncateStrategy();
        $instance->emptyFixtures = $this->emptyFixtures ?? false;

        return $instance;
    }
}
