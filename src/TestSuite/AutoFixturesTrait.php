<?php
/**
 * AsalaeCore\TestSuite\AutoFixturesTrait
 */

namespace AsalaeCore\TestSuite;

use Cake\ORM\Association\BelongsTo;
use Cake\ORM\TableRegistry;
use Cake\Utility\Inflector;

/**
 * Récupère-les models ainsi que leurs dépendances (belongsTo)
 *
 * @category TestSuite
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2021, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin TestCase
 * @property bool debugFixtures
 * @property array appAutoFixtures
 * @property array appAppendFixtures
 */
trait AutoFixturesTrait
{
    // DEBUG possible: private $debugFixtures = true;

    /**
     * Get the fixtures this test should use.
     *
     * @return array<string>
     */
    public function getFixtures(): array
    {
        $fixtures = array_merge(
            $this->autoFixtures($this->appAutoFixtures ?? []),
            $this->appendFixtures($this->appAppendFixtures ?? []),
            $this->fixtures
        );
        TableRegistry::getTableLocator()->clear();
        if ($this->debugFixtures ?? false) {
            sort($fixtures);
            debug(array_unique($fixtures));
        }
        return array_unique($fixtures);
    }

    /**
     * Récupère-les models ainsi que leurs dépendances (belongsTo)
     * @param array $models
     * @param array $done
     * @return array
     */
    private function autoFixtures(array $models = [], array &$done = []): array
    {
        $loc = TableRegistry::getTableLocator();
        $fixtures = [];
        foreach ($models as $modelName) {
            $done[] = $modelName;
            $belongsToModels = [];
            foreach ($loc->get($modelName)->associations() as $assoc) {
                $assocModel = Inflector::camelize($assoc->getTable());
                if ($assoc instanceof BelongsTo
                    && !in_array('app.' . $assocModel, $fixtures)
                    && !in_array($assocModel, $done)
                ) {
                    $belongsToModels[] = $assocModel;
                }
            }
            $fixtures = array_merge($fixtures, $this->autoFixtures($belongsToModels, $done));
            if (!in_array('app.' . $modelName, $fixtures)) {
                $fixtures[] = 'app.'.$modelName;
            }
        }
        return array_unique($fixtures);
    }

    /**
     * Ajoute des fixtures à la façon \AsalaeCore\TestSuite\FixtureManager
     * @param array $append
     * @return array
     */
    private function appendFixtures(array $append = [])
    {
        $fixtures = [];
        foreach ($append as $fixture) {
            if (is_array($fixture)) {
                $fixtures = array_merge($fixtures, $fixture);
            } else {
                $fixtures[] = $fixture;
            }
        }
        return array_unique($fixtures);
    }
}
