<?php
/**
 * AsalaeCore\TestSuite\EmptyFixturesTrait
 */

namespace AsalaeCore\TestSuite;

/**
 * À utiliser pour une classe de test qui emploi des fixtures vides
 * (tables correctement définies mais sans données)
 *
 * @category TestSuite
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
trait EmptyFixturesTrait
{
    /**
     * @var bool pour des fixtures vides
     */
    protected $emptyFixtures = true;
}
