<?php
/**
 * AsalaeCore\TestSuite\MiddlewareDispatcher
 *
 * @noinspection PhpInternalEntityUsedInspection
 */

namespace AsalaeCore\TestSuite;

use Cake\TestSuite\MiddlewareDispatcher as CakeMiddlewareDispatcher;

/**
 * Integre AsalaeCore\Http\Response
 *
 * @category TestSuite
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @deprecated since cakephp 4.2.0
 */
class MiddlewareDispatcher extends CakeMiddlewareDispatcher
{
}
