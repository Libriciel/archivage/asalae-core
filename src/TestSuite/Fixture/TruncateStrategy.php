<?php
declare(strict_types=1);

namespace AsalaeCore\TestSuite\Fixture;

use Cake\TestSuite\Fixture\TruncateStrategy as CakeTruncateStrategy;

/**
 * Fixture strategy that truncates all fixture ables at the end of test.
 */
class TruncateStrategy extends CakeTruncateStrategy
{
    /**
     * @var bool à true pour un chargement vide des fixtures
     */
    public $emptyFixtures = false;

    /**
     * Initialize strategy.
     */
    public function __construct()
    {
        parent::__construct();
        $this->helper = new FixtureHelper();
    }

    /**
     * Called before each test run in each TestCase.
     *
     * @param array<string> $fixtureNames Name of fixtures used by test.
     * @return void
     */
    public function setupTest(array $fixtureNames): void
    {
        if (empty($fixtureNames)) {
            return;
        }

        $this->fixtures = $this->helper->loadFixtures($fixtureNames);

        if (!$this->emptyFixtures ?? true) {
            $this->helper->insert($this->fixtures);
        }
    }
}
