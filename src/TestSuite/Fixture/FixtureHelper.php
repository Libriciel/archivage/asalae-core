<?php
/**
 * AsalaeCore\TestSuite\Fixture\FixtureHelper
 * @noinspection PhpInternalEntityUsedInspection
 */

namespace AsalaeCore\TestSuite\Fixture;

use AsalaeCore\Exception\InfiniteLoopException;
use Cake\Core\App;
use Cake\Database\Connection;
use Cake\Datasource\ConnectionInterface;
use Cake\Datasource\FixtureInterface;
use Cake\ORM\Locator\LocatorAwareTrait;
use Cake\TestSuite\ConnectionHelper;
use Cake\TestSuite\Fixture\FixtureHelper as CakeFixtureHelper;
use Cake\Utility\Inflector;

/**
 * Surcharge du FixtureHelper permettant un chargement des fixtures gérant les dépendances des clefs étrangères
 * et l'ordre correct de chargement (exclut toute référence circulaire dans le graphe de dépendances).
 *
 * @category Fixture
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2022, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class FixtureHelper extends CakeFixtureHelper
{
    use LocatorAwareTrait;

    /**
     * @var Connection Database connection
     */
    private $connection;

    /**
     * @var array<string>
     * Seulement les noms de classe, indexées par nom de base ['org_entities' => 'OrgEntitiesFixtures']
     */
    private $allFixtures = [];

    /**
     * @var array<bool>
     * En index le nom des fixtures déjà traitées lors du tri,
     * permet de détecter une référence circulaire qui provoquerait une boucle infinie
     */
    private $allreadyVisited = [];

    /**
     * Liste des dépendences pour chaques tables
     * @var array [table => [table1, table2, ...]]
     */
    private $dependencies = [];

    /**
     * Sort fixtures with foreign constraints last if possible, otherwise returns null.
     *
     * @param \Cake\Database\Connection                $connection Database connection
     * @param array<\Cake\Datasource\FixtureInterface> $fixtures   Database fixtures
     * @return array|null
     * @throws InfiniteLoopException
     */
    protected function sortByConstraint(Connection $connection, array $fixtures): ?array
    {
        $this->connection = $connection;
        $this->getTableLocator()->clear();

        // rangement des fixtures
        $fixtures = array_reduce(
            $fixtures,
            function (array $arr, FixtureInterface $item): array {
                $arr[$item->sourceName()] = $item;
                return $arr;
            },
            []
        );

        $this->allFixtures = [];
        $glob = glob(TESTS . 'Fixture/*Fixture.php');
        $fixtureNames = array_map(fn($v) => basename($v, 'Fixture.php'), $glob);
        foreach ($fixtureNames as $fixtureName) {
            if ($fixtureName === 'Phinxlog') {
                continue;
            }
            $className = App::className($fixtureName, 'Test/Fixture', 'Fixture');
            $this->allFixtures[Inflector::underscore($fixtureName)] = $className;
        }

        // ajout récursif des fixtures manquantes
        foreach ($fixtures as $fixtureName => $fixture) {
            $references = $this->getForeignReferences($this->connection, $fixture);
            $this->dependencies[$fixtureName] = array_unique(
                array_filter($references, fn ($e) => $e !== $fixture->sourceName())
            );
            foreach ($this->dependencies[$fixtureName] as $ref) {
                $this->addNecessaryFixture($ref, $fixtures);
            }
        }

        return array_map(
            fn ($e) => $fixtures[$e],
            $this->sortDependencies()
        );
    }

    /**
     * Ajout récursif des fixtures en regardant leurs dépendances
     * @param string $ref
     * @param array  $fixtures
     * @return void
     */
    protected function addNecessaryFixture($ref, &$fixtures): void
    {
        if (!isset($fixtures[$ref])) { // pas besoin si déjà traité
            $fixture = new $this->allFixtures[$ref];
            $fixtures[$ref] = $fixture; // ajout aux fixtures
            $references = $this->getForeignReferences($this->connection, $fixture);
            $this->dependencies[$fixture->sourceName()]
                = array_filter($references, fn ($e) => $e !== $fixture->sourceName());
            foreach ($this->dependencies[$ref] as $subRef) {
                $this->addNecessaryFixture($subRef, $fixtures);
            }
        }
    }

    /**
     * Ordonne les fixtures selon leurs dépendances
     * @return array
     * @throws InfiniteLoopException
     */
    private function sortDependencies()
    {
        $this->allreadyVisited = [];
        $sorted = [];
        foreach (array_keys($this->dependencies) as $table) {
            if (isset($sorted[$table])) {
                continue;
            }
            $this->sortTable($table, $sorted);
        }
        return $sorted;
    }

    /**
     * Fonction récursive de sortDependencies()
     * @param string      $table
     * @param array       $sorted
     * @param string|null $parent
     * @return void
     * @throws InfiniteLoopException
     */
    private function sortTable(string $table, array &$sorted, string $parent = null)
    {
        $parents = explode('/', $parent ?: '');
        if (in_array($table, $parents)) {
            throw new InfiniteLoopException(
                "Circular reference found: "
                . $parent
                . '/' . $table
            );
        }
        foreach ($this->dependencies[$table] as $dependency) {
            if (isset($sorted[$dependency])) {
                continue;
            }
            $this->sortTable(
                $dependency,
                $sorted,
                $parent ? $parent . '/' . $table : $table
            );
        }
        $sorted[$table] = $table;
        $this->removeSortedTableFromDependencies($table);
    }

    /**
     * Retire une table des dépendances des autres table car cette dépendance a
     * été remplie
     * @param string $targetTable
     * @return void
     */
    private function removeSortedTableFromDependencies(string $targetTable)
    {
        foreach ($this->dependencies as $table => $dependencies) {
            $index = array_search($targetTable, $dependencies);
            if ($index) {
                unset($this->dependencies[$table][$index]);
            }
        }
    }

    /**
     * Inserts fixture data.
     *
     * @param array<\Cake\Datasource\FixtureInterface> $fixtures Test fixtures
     * @return void
     * @internal
     */
    public function insert(array $fixtures): void
    {
        $this->runPerConnection(
            function (ConnectionInterface $connection, array $groupFixtures): void {
                $helper = new ConnectionHelper();
                $helper->runWithoutConstraints(
                    $connection,
                    function (Connection $connection) use ($groupFixtures): void {
                        $this->insertConnection($connection, $groupFixtures);
                    }
                );
            },
            $fixtures
        );
    }

    /**
     * Truncates fixture tables.
     *
     * @param array<\Cake\Datasource\FixtureInterface> $fixtures Test fixtures
     * @return void
     * @internal
     */
    public function truncate(array $fixtures): void
    {
        $this->runPerConnection(
            function (ConnectionInterface $connection, array $groupFixtures): void {
                $helper = new ConnectionHelper();
                $helper->runWithoutConstraints(
                    $connection,
                    function (Connection $connection) use ($groupFixtures): void {
                        $this->truncateConnection($connection, $groupFixtures);
                    }
                );
            },
            $fixtures
        );
    }
}
