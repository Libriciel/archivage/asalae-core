<?php
/**
 * AsalaeCore\TestSuite\VolumeSample
 */

namespace AsalaeCore\TestSuite;

use AsalaeCore\Driver\Volume\Exception\VolumeException;
use AsalaeCore\Driver\Volume\VolumeFilesystem;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Exception;
use Libriciel\Filesystem\Utility\Filesystem;
use Throwable;

/**
 * Permet la gestion des volumes pour les tests
 *
 * @category TestSuite
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
abstract class VolumeSample
{
    /**
     * @var VolumeFilesystem
     */
    public static $volume1;

    /**
     * @var VolumeFilesystem
     */
    public static $volume2;

    /**
     * @var string
     */
    protected static $sampleDir;

    /**
     * Initialise les dossiers et les volumes pour les tests
     * @throws VolumeException
     */
    public static function init()
    {
        Filesystem::reset();
        if (is_dir(TMP_TESTDIR)) {
            Filesystem::remove(TMP_TESTDIR);
        }
        if (defined('TMP_VOL1')) {
            if (is_dir(TMP_VOL1)) {
                Filesystem::remove(TMP_VOL1);
            }
            if (is_dir(TMP_VOL2)) {
                Filesystem::remove(TMP_VOL2);
            }
            Filesystem::mkdir(TMP_VOL1);
            Filesystem::mkdir(TMP_VOL2);
            self::$volume1 = new VolumeFilesystem(['path' => TMP_VOL1 ]);
            self::$volume2 = new VolumeFilesystem(['path' => TMP_VOL2 ]);
        }
        Filesystem::mkdir(TMP_TESTDIR);
    }

    /**
     * Détruit les données des volumes
     * @throws Exception
     */
    public static function destroy()
    {
        Filesystem::reset();
        if (is_dir(TMP_TESTDIR)) {
            Filesystem::remove(TMP_TESTDIR);
        }
        if (defined('TMP_VOL1')) {
            if (is_dir(TMP_VOL1)) {
                Filesystem::remove(TMP_VOL1);
            }
            if (is_dir(TMP_VOL2)) {
                Filesystem::remove(TMP_VOL2);
            }
        }
    }

    /**
     * Ajoute un fichier sample (versionné dans le dossier "tests/Data")
     * @param string $sample
     * @param string $storageReference
     * @return string destinationFilename
     * @throws VolumeException
     */
    public static function insertSample(string $sample, string $storageReference): string
    {
        $path = self::getSampleDir() . $sample;
        self::$volume1->fileUpload($path, $storageReference);
        self::$volume2->fileUpload($path, $storageReference);
        self::updateSampleHash(self::getSampleContent($sample), $storageReference);
        return $storageReference;
    }

    /**
     * Emplacement du dossier sample
     * @return string
     */
    protected static function getSampleDir(): string
    {
        if (empty(self::$sampleDir)) {
            self::$sampleDir = dirname(__DIR__, 2) .DS.'tests'.DS.'Data'.DS;
        }
        return self::$sampleDir;
    }

    /**
     * Donne le contenu d'un sample
     * @param string $sample
     * @return string
     */
    public static function getSampleContent(string $sample): string
    {
        return file_get_contents(self::getSampleDir().$sample);
    }

    /**
     * Copie d'un sample dans le dossier temporaire
     * @param string      $sample
     * @param string|null $filename
     * @return string destinationFilename
     * @throws Exception
     */
    public static function copySample(string $sample, string $filename = null): string
    {
        Filesystem::copy(
            self::getSampleDir().$sample,
            TMP_TESTDIR.DS.($filename ?: $sample),
            true
        );
        return TMP_TESTDIR.DS.($filename ?: $sample);
    }

    /**
     * Créé un fichier temporaire
     * @param string $filename
     * @param string $content
     * @return string destinationFilename
     * @throws Exception
     */
    public static function tmpPutContent(string $filename, string $content)
    {
        Filesystem::dumpFile(
            TMP_TESTDIR.DS.$filename,
            $content
        );
        return TMP_TESTDIR.DS.$filename;
    }

    /**
     * Insère $content dans les volumes de test
     * @param string $storageReference
     * @param string $content
     * @param bool   $overwrite
     * @return string destinationFilename
     * @throws VolumeException
     */
    public static function volumePutContent(string $storageReference, string $content, bool $overwrite = false)
    {
        if ($overwrite) {
            if (self::$volume1->fileExists($storageReference)) {
                self::$volume1->fileDelete($storageReference);
            }
            if (self::$volume2->fileExists($storageReference)) {
                self::$volume2->fileDelete($storageReference);
            }
        }
        self::$volume1->filePutContent($storageReference, $content);
        self::$volume2->filePutContent($storageReference, $content);
        self::updateSampleHash($content, $storageReference);
        return $storageReference;
    }

    /**
     * alias du self::$volume1->fileGetContent($storageReference)
     * @param string $storageReference
     * @return string
     * @throws VolumeException
     */
    public static function volumeGetContent(string $storageReference): string
    {
        return self::$volume1->fileGetContent($storageReference);
    }

    /**
     * alias du self::$volume1->volumeDownload(string $storageReference, string $destinationFileUri)
     * @param string $storageReference
     * @param string $destinationFileUri
     * @throws VolumeException
     */
    public static function volumeDownload(string $storageReference, string $destinationFileUri)
    {
        self::$volume1->fileDownload($storageReference, $destinationFileUri);
    }

    /**
     * alias du self::$volume1->hash($storageReference, $algo);
     * @param string $storageReference
     * @param string $algo
     * @return string
     * @throws VolumeException
     */
    public static function volumeHash(string $storageReference, string $algo = 'sha256'): string
    {
        return self::$volume1->hash($storageReference, $algo);
    }

    /**
     * Donne l'emplacement du fichier sample (attention à ne pas le modifier !)
     * @param string $sample
     * @return string
     */
    public static function getSamplePath(string $sample): string
    {
        return self::getSampleDir().$sample;
    }

    /**
     * Tente de mettre à jour stored_files.hash avec le hash du sample si $path
     * correspond à un stored_file du secure_data_space.id = 1
     * @param string $content
     * @param string $storageReference
     * @return void
     */
    public static function updateSampleHash(string $content, string $storageReference)
    {
        try {
            $algo = Configure::read('hash_algo', 'sha256');
            $hash = hash($algo, $content);
            $StoredFiles = TableRegistry::getTableLocator()->get('StoredFiles');
            $StoredFiles->updateAll(
                ['hash' => $hash, 'hash_algo' => $algo],
                ['secure_data_space_id' => 1, 'name' => $storageReference]
            );
        } catch (Throwable $e) {
        }
    }
}
