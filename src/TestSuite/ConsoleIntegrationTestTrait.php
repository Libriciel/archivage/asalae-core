<?php
/**
 * AsalaeCore\TestSuite\IntegrationCronTrait
 */

namespace AsalaeCore\TestSuite;

use AsalaeCore\Console\ConsoleIo;
use Cake\Console\Exception\StopException;
use Cake\TestSuite\ConsoleIntegrationTestTrait as CakeConsoleIntegrationTestTrait;
use Cake\TestSuite\Stub\ConsoleInput;
use Cake\TestSuite\Stub\ConsoleOutput;

/**
 * ConsoleIo custom dans exec() (getter du stdout et stderr)
 *
 * @category TestSuite
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
trait ConsoleIntegrationTestTrait
{
    use CakeConsoleIntegrationTestTrait;


    /**
     * Runs cli integration test
     *
     * @param string $command Command to run
     * @param array  $input   Input values to pass to an interactive shell
     * @return void
     */
    public function exec($command, array $input = [])
    {
        $runner = $this->makeRunner();

        $this->_out = new ConsoleOutput();
        $this->_err = new ConsoleOutput();
        $this->_in = new ConsoleInput($input);

        $args = $this->commandStringToArgs("cake $command");
        $io = new ConsoleIo($this->_out, $this->_err, $this->_in);

        try {
            $this->_exitCode = $runner->run($args, $io);
        } catch (StopException $exception) {
            $this->_exitCode = $exception->getCode();
        }
    }
}
