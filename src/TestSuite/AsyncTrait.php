<?php
/**
 * AsalaeCore\TestSuite\AsyncTrait
 * @noinspection RedundantSuppression
 * @noinspection PhpUndefinedMethodInspection
 */

namespace AsalaeCore\TestSuite;

use PHPUnit\Framework\AssertionFailedError;

/**
 * Assertions sur des evenements asynchrones
 *
 * @category TestSuite
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
trait AsyncTrait
{
    /**
     * @var float Durée d'attente maximum en secondes
     */
    public $maxWaitTime = 0.5;

    /**
     * @var float durée d'attente entre 2 contrôles
     */
    public $checkInterval = 0.05;

    /**
     * @var bool decuple la durée max et affiche la durée réelle d'une assertion
     */
    public $debugMode = false;

    /**
     * Timer
     *
     * @param callable $check
     * @param array    $params
     * @param float    $timer
     */
    private function runTimer(callable $check, array $params, float &$timer)
    {
        $waitTime = $this->debugMode ? $this->maxWaitTime * 10 : $this->maxWaitTime;
        $interval = $this->checkInterval * 1000000;
        while ($timer < $waitTime && !call_user_func_array($check, $params)) {
            $timer += $this->checkInterval;
            usleep($interval);
        }
        if ($this->debugMode) {
            debug($timer.' seconds');
        }
    }

    /**
     * Asserts that a file will contains a needle.
     *
     * @param string $filename
     * @param string $needle
     * @param string $message
     * @param float  $timer
     */
    private function assertFileWillContain(
        string $filename,
        string $needle,
        string $message = '',
        float &$timer = 0
    ) {
        try {
            $this->assertFileWillExists($filename, $message, $timer);
            $this->runTimer(
                function ($filename, $expected) {
                    return strpos(file_get_contents($filename), $expected) !== false;
                },
                [$filename, $needle],
                $timer
            );
            $this->assertStringContainsString(
                $needle,
                file_get_contents($filename),
                $message
            );
        } catch (AssertionFailedError $e) {
            trigger_error('assertFileWillContain failed! '.$e, E_USER_WARNING);
        }
    }

    /**
     * Asserts that a file will exists.
     *
     * @param string $filename
     * @param string $message
     * @param float  $timer
     */
    private function assertFileWillExists(string $filename, string $message = '', float &$timer = 0)
    {
        $this->runTimer('file_exists', [$filename], $timer);
        $this->assertFileExists($filename, $message);
    }

    /**
     * Répète une commande pour qu'un fichier existe
     * @param callable $func
     * @param string   $filename
     * @param int      $limit
     * @return bool
     */
    private function repeatUntilFileExist(callable $func, string $filename, int $limit = 10): bool
    {
        $interval = $this->checkInterval * 1000000;
        for ($i = 0; $i < $limit; $i++) {
            $func();
            usleep($interval);
            if (file_exists($filename)) {
                return true;
            }
        }
        return false;
    }
}
