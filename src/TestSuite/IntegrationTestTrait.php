<?php
/**
 * AsalaeCore\TestSuite\IntegrationTestTrait
 */

namespace AsalaeCore\TestSuite;

use Cake\TestSuite\IntegrationTestTrait as CakeIntegrationTestTrait;

/**
 * Integre AsalaeCore\Http\Response
 *
 * @category TestSuite
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin CakeIntegrationTestTrait
 * @deprecated since cakephp 4.2.0
 */
trait IntegrationTestTrait
{
    /**
     * Traits
     */
    use CakeIntegrationTestTrait;
}
