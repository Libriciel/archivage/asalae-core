<?php
/**
 * AsalaeCore\TestSuite\IntegrationCronTrait
 * @noinspection PhpInternalEntityUsedInspection
 * @noinspection PhpPrivateFieldCanBeLocalVariableInspection
 * @noinspection RedundantSuppression
 */

namespace AsalaeCore\TestSuite;

use AsalaeCore\Cron\CronInterface;
use Cake\Console\Exception\StopException;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\ConsoleIntegrationTestTrait;
use Cake\TestSuite\Constraint\Console\ContentsNotContain;
use Cake\TestSuite\Stub\ConsoleOutput;
use Cake\TestSuite\TestCase;

/**
 * Integration des crons pour les tests
 *
 * @category TestSuite
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin TestCase
 */
trait IntegrationCronTrait
{
    /**
     * Traits
     */
    use ConsoleIntegrationTestTrait;

    /**
     * @var CronInterface
     */
    private $cron;

    /**
     * @var null|string
     */
    private $workOutput = null;

    /**
     * Lance un cron pour les tests
     * @param string $className
     * @param array  $params
     * @noinspection PhpUnusedPrivateMethodInspection
     */
    private function runCron(string $className, array $params = [])
    {
        $this->_out = new ConsoleOutput();
        $this->_err = new ConsoleOutput();
        $this->cron = new $className($params, $this->_out, $this->_err);
        $params += [
            'cron_id' => 1,
        ];
        $loc = TableRegistry::getTableLocator();
        $CronExecutions = $loc->get('CronExecutions');
        $Crons = $loc->get('Crons');
        $exec = $CronExecutions->newEntity(
            [
                'cron_id' => $params['cron_id'],
                'date_begin' => '1950-01-01',
                'state' => 'running',
                'report' => ''
            ]
        );
        $cron = $Crons->newEntity(
            [
                'id' => $params['cron_id'],
                'name' => 'test',
            ],
            ['validate' => false]
        );
        try {
            $this->workOutput = $this->cron->work($exec, $cron);
            $this->_exitCode = $this->workOutput === 'success' ? 0 : 1;
        } catch (StopException $exception) {
            $this->_exitCode = $exception->getCode();
        }
    }

    /**
     * Asserts `stderr` does not contain expected output
     *
     * @param string $expected Expected output
     * @param string $message  Failure message
     * @return void
     */
    public function assertErrorNotContains($expected, $message = '')
    {
        $this->assertThat(
            $expected,
            new ContentsNotContain(
                $this->_err->messages(),
                'error output'
            ),
            $message
        );
    }
}
