<?php
/**
 * AsalaeCore\TestSuite\FixtureManager
 */

namespace AsalaeCore\TestSuite;

use Cake\Core\Exception\Exception;
use Cake\TestSuite\Fixture\FixtureManager as CakeFixtureManager;
use Cake\TestSuite\TestCase as CakeTestCase;
use RuntimeException;
use UnexpectedValueException;

/**
 * Gestion des fixtures améliorés
 *
 * @category TestSuite
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2020, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
class FixtureManager extends CakeFixtureManager
{
    /**
     * Looks for fixture files and instantiates the classes accordingly
     *
     * @param CakeTestCase $test The test suite to load fixtures for.
     * @return void
     * @throws UnexpectedValueException when a referenced fixture does not exist.
     */
    protected function _loadFixtures($test): void
    {
        if ($test instanceof TestCase) {
            $fixtures = $this->flattenFixtures((array)$test->getFixtures() ?: []);
            $test->setFixture($fixtures);
        }

        parent::_loadFixtures($test);
    }

    /**
     * Creates the fixtures tables and inserts data on them.
     *
     * @param CakeTestCase $test The test to inspect for fixture loading.
     * @return void
     * @throws Exception When fixture records cannot be inserted.
     * @throws RuntimeException
     */
    public function load($test): void
    {
        if ($test instanceof TestCase) {
            $fixtures = $this->flattenFixtures((array)$test->getFixtures() ?: []);
            $test->setFixture($fixtures);
        }

        parent::load($test);
    }

    /**
     * Run a function on each connection and collection of fixtures.
     *
     * @param string[] $fixtures  A list of fixtures to operate on.
     * @param callable $operation The operation to run on each connection + fixture set.
     * @return void
     */
    protected function _runOperation($fixtures, $operation): void
    {
        parent::_runOperation($this->flattenFixtures($fixtures), $operation);
    }

    /**
     * Get the unique list of connections that a set of fixtures contains.
     *
     * @param string[] $fixtures The array of fixtures a list of connections is needed from.
     * @return array An array of connection names.
     */
    protected function _fixtureConnections($fixtures): array
    {
        return parent::_fixtureConnections($this->flattenFixtures($fixtures));
    }

    /**
     * Truncates the fixtures tables
     *
     * @param CakeTestCase $test The test to inspect for fixture unloading.
     * @return void
     */
    public function unload($test): void
    {
        if ($test instanceof TestCase) {
            $fixtures = $this->flattenFixtures((array)$test->getFixtures() ?: []);
            $test->setFixture($fixtures);
        }
        parent::unload($test);
    }

    /**
     * Retourne un array unidimensionnel
     * @param array $fixtures
     * @return array
     */
    private function flattenFixtures(array $fixtures): array
    {
        $output = [];
        foreach ($fixtures as $fixture) {
            if (is_array($fixture)) {
                $output = array_merge($output, $fixture);
            } else {
                $output[] = $fixture;
            }
        }
        return array_unique($output);
    }
}
