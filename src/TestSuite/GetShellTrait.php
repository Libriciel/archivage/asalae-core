<?php
/**
 * AsalaeCore\TestSuite\GetShellTrait
 */

namespace AsalaeCore\TestSuite;

use AsalaeCore\Console\ConsoleIo;
use Cake\Command\Command;
use Cake\TestSuite\Stub\ConsoleInput;
use Cake\TestSuite\Stub\ConsoleOutput;
use Exception;

/**
 * Permet de récupérer une instance de shell
 *
 * @category TestSuite
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 * @mixin ConsoleIntegrationTestTrait
 */
trait GetShellTrait
{
    /**
     * Donne une instance de shell
     * @param string $classname
     * @param array  $inputs
     * @return Command
     * @throws Exception
     */
    public function getShell(string $classname, array $inputs = [])
    {
        if (!class_exists($classname)) {
            throw new Exception(sprintf('%s does not exists', h($classname)));
        }

        $this->_out = new ConsoleOutput;
        $this->_err = new ConsoleOutput;
        $this->_in = new ConsoleInput($inputs);

        $io = new ConsoleIo($this->_out, $this->_err, $this->_in);
        $instance = new $classname($io);
        $instance->ioShell = $io;
        return $instance;
    }
}
