<?php
/**
 * AsalaeCore\TestSuite\InvokePrivateTrait
 */

namespace AsalaeCore\TestSuite;

use ReflectionClass;
use ReflectionException;

/**
 * Permet d'accéder aux méthodes privés d'une classe pour les tests
 *
 * @category TestSuite
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
trait InvokePrivateTrait
{

    /**
     * Call protected/private method of a class.
     *
     * @param object $object     Instantiated object that we will run method on.
     * @param string $methodName Method name to call
     * @param array  $parameters Array of parameters to pass into method.
     *
     * @return mixed Method return.
     * @throws ReflectionException
     */
    public function invokeMethod($object, $methodName, array $parameters = array())
    {
        $reflection = new ReflectionClass(get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true); // NOSONAR

        return $method->invokeArgs($object, $parameters);
    }

    /**
     * Call protected/private property of a class.
     * @param object $object  Instantiated object that we will run method on.
     * @param string $argName Property name to call
     * @param string $verb    get or set
     * @param null   $value   if setter, set this value
     * @return mixed
     * @throws ReflectionException
     */
    public function invokeProperty($object, $argName, string $verb = 'get', $value = null)
    {
        $reflection = new ReflectionClass(get_class($object));
        $property = $reflection->getProperty($argName);
        $property->setAccessible(true); // NOSONAR

        if ($verb === 'get') {
            return $property->getValue($object); // NOSONAR
        } elseif ($verb === 'set') {
            $property->setValue($object, $value); // NOSONAR
        }
    }
}
