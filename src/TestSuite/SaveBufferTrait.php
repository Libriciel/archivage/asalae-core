<?php
/**
 * AsalaeCore\TestSuite\SaveBufferTrait
 * @noinspection RedundantSuppression
 */

namespace AsalaeCore\TestSuite;

use Throwable;

/**
 * Sauvegarde l'état des buffers avant de lancer un test qui peut manipuler le buffer
 * évite l'erreur : "Test code or tested code did not (only) close its own output buffers"
 *
 * @category TestSuite
 *
 * @author    Libriciel SCOP <contact@libriciel.coop>
 * @copyright (c) 2019, Libriciel
 * @license   https://www.gnu.org/licenses/agpl-3.0.txt
 */
trait SaveBufferTrait
{

    /**
     * @var array buffer mémorisé
     */
    private $savedBuffer = [];

    /**
     * @var bool assure que saveBuffer() est appelé 1 seule fois avant restaureBuffer()
     */
    private $isBufferSaved = false;

    /**
     * stock le(s) buffer(s) dans $savedBuffer, le(s) ferme(nt)
     * et ouvre un nouveau buffer
     * @param int $openNewBuffer Nombre de buffers à ouvrir
     * @return bool retour du ob_start()
     */
    private function saveBuffer(int $openNewBuffer = 2): bool
    {
        if ($this->isBufferSaved) {
            trigger_error('saveBuffer() called 2 times before restoreBuffer()');
            return true;
        }
        $this->savedBuffer = [];
        while (ob_get_level()) {
            $this->savedBuffer[] = ob_get_clean();
        }
        $this->isBufferSaved = true;
        for ($i = 0; $i < $openNewBuffer; $i++) {
            if (!ob_start()) {
                return false;
            }
        }
        return true;
    }

    /**
     * Reconstruit le buffer tel qu'il était lors de l'appel de saveBuffer()
     * @return string
     */
    private function restoreBuffer(): string
    {
        if (!$this->isBufferSaved) {
            trigger_error('restoreBuffer() called before a saveBuffer()');
            return '';
        }
        $ignored = '';
        while (ob_get_level()) {
            $ignored .= ob_get_clean();
        }
        for ($i = count($this->savedBuffer) -1; $i >= 0; $i--) {
            ob_start();
            echo $this->savedBuffer[$i];
        }
        $this->savedBuffer = [];
        $this->isBufferSaved = false;
        return $ignored;
    }

    /**
     * récupère le buffer renvoyé par une fonction
     * @param callable $callback
     * @param mixed    ...$arg
     * @return string
     * @throws Throwable
     * @noinspection PhpUnusedPrivateMethodInspection
     */
    private function preserveBuffer(callable $callback, ...$arg): string
    {
        $this->saveBuffer();
        try {
            call_user_func_array($callback, $arg);
        } catch (Throwable $e) {
            $this->restoreBuffer();
            throw $e;
        }
        return $this->restoreBuffer();
    }
}
