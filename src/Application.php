<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     3.3.0
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace AsalaeCore;

use AsalaeCore\Error\ExceptionTrap;
use AsalaeCore\Factory\Utility;
use AsalaeCore\Middleware\AsalaeMiddleware;
use AsalaeCore\Middleware\CorsMiddleware;
use AsalaeCore\Middleware\MaintenanceMiddleware;
use AsalaeCore\Middleware\RateLimiterMiddleware;
use AsalaeCore\Middleware\UrlAuthMiddleware;
use AsalaeCore\Policy\AclResolver;
use Authentication\AuthenticationService;
use Authentication\AuthenticationServiceInterface;
use Authentication\AuthenticationServiceProviderInterface;
use Authentication\Authenticator\FormAuthenticator;
use Authentication\Authenticator\SessionAuthenticator;
use Authentication\Identifier\PasswordIdentifier;
use Authentication\Identity;
use Authentication\Middleware\AuthenticationMiddleware;
use Authorization\AuthorizationService;
use Authorization\AuthorizationServiceInterface;
use Authorization\AuthorizationServiceProviderInterface;
use Authorization\Middleware\AuthorizationMiddleware;
use Authorization\Middleware\RequestAuthorizationMiddleware;
use Cake\Core\App;
use Cake\Core\Configure;
use Cake\Core\Exception\MissingPluginException;
use Cake\Error\Middleware\ErrorHandlerMiddleware;
use Cake\Event\EventManagerInterface;
use Cake\Http\BaseApplication;
use Cake\Http\ControllerFactoryInterface;
use Cake\Http\MiddlewareQueue;
use Cake\Http\Middleware\BodyParserMiddleware;
use Cake\Routing\Middleware\AssetMiddleware;
use Cake\Routing\Middleware\RoutingMiddleware;
use Cake\Routing\Router;
use Exception;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Application setup class.
 *
 * This defines the bootstrapping logic and middleware layers you
 * want to use in your application.
 */
class Application extends BaseApplication implements
    AuthenticationServiceProviderInterface,
    AuthorizationServiceProviderInterface
{
    /**
     * @var \string[][] params d'auth
     */
    public $authentication = [
        'service' => [
            'className' => AuthenticationService::class,
            'identityClass' => Identity::class,
            'unauthenticatedRedirect' => '/users/login',
            'queryParam' => 'redirect',
        ],
    ];

    /**
     * Constructor
     *
     * @param string                          $configDir         The directory the bootstrap configuration is held in.
     * @param EventManagerInterface|null      $eventManager      Application event manager instance.
     * @param ControllerFactoryInterface|null $controllerFactory Controller factory.
     */
    public function __construct(
        string $configDir,
        ?EventManagerInterface $eventManager = null,
        ?ControllerFactoryInterface $controllerFactory = null
    ) {
        parent::__construct($configDir, $eventManager, $controllerFactory);
        $this->authentication['service']['unauthenticatedRedirect']
            = Router::url($this->authentication['service']['unauthenticatedRedirect']);
    }

    /**
     * Load all the application configuration and bootstrap logic.
     *
     * Override this method to add additional bootstrap logic for your application.
     *
     * @return void
     */
    public function bootstrap(): void
    {
        parent::bootstrap();
        $this->addPlugin('Libriciel/Filesystem');
        $this->addPlugin('Bootstrap');
        $this->addPlugin('Acl', ['bootstrap' => true]);
        $this->addPlugin('Beanstalk');
        if (PHP_SAPI === 'cli') {
            // no-dev
            try {
                $this->addPlugin('Bake');
            } catch (MissingPluginException $e) {
                // Do not halt if the plugin is missing
            }
            $this->addPlugin('Migrations');
        }
    }

    /**
     * Setup the middleware your application will use.
     *
     * @param MiddlewareQueue $middlewareQueue The middleware queue to setup.
     * @return MiddlewareQueue The updated middleware queue.
     */
    public function middleware(MiddlewareQueue $middlewareQueue): MiddlewareQueue
    {
        $middlewareQueue
            // Cross-Origin Resource Sharing
            ->add(new CorsMiddleware)

            // Interruption de service
            ->add(new MaintenanceMiddleware)

            // Catch any exceptions in the lower layers,
            // and make an error page/response
            ->add(new ErrorHandlerMiddleware(ExceptionTrap::instance()))

            // Handle plugin/theme assets like CakePHP normally does.
            ->add(
                new AssetMiddleware(
                    [
                        'cacheTime' => Configure::read('Asset.cacheTime'),
                    ]
                )
            )

            // Add routing middleware.
            // If you have a large number of routes connected, turning on routes
            // caching in production could improve performance. For that when
            // creating the middleware instance specify the cache config name by
            // using it's second constructor argument:
            // `new RoutingMiddleware($this, '_cake_routes_')`
            ->add(new RoutingMiddleware($this))

            // add Authentication after RoutingMiddleware
            ->add(new AuthenticationMiddleware($this))

            // limite les tentatives de login
            ->add(new RateLimiterMiddleware)

            // Gestion des erreurs dans un context UrlAuth
            ->add(new UrlAuthMiddleware)

            // Add authorization (after authentication if you are using that plugin too).
            ->add(new AuthorizationMiddleware($this))
            ->add(new RequestAuthorizationMiddleware)

            // Parse various types of encoded request bodies so that they are
            // available as array through $request->getData()
            // https://book.cakephp.org/4/en/controllers/middleware.html#body-parser-middleware
            ->add(new BodyParserMiddleware())
            ->add(new AsalaeMiddleware);

        return $middlewareQueue;
    }

    /**
     * Donne le service d'auth
     * @param ServerRequestInterface $request
     * @return AuthenticationServiceInterface
     * @throws Exception
     */
    public function getAuthenticationService(ServerRequestInterface $request): AuthenticationServiceInterface
    {
        /** @var AuthenticationService $authenticationService */
        $authenticationService = Utility::get(
            $this->authentication['service']['className'],
            $this->authentication['service']
        );
        $authenticationService->loadIdentifier(
            'Password',
            [
                'className' => PasswordIdentifier::class,
                'fields' => [
                    'username' => 'username',
                    'password' => 'password',
                ],
            ]
        );
        $authenticationService->loadAuthenticator(
            'Session',
            [
                'className' => SessionAuthenticator::class,
            ]
        );
        $authenticationService->loadAuthenticator(
            'Form',
            [
                'className' => FormAuthenticator::class,
                'fields' => [
                    'username' => 'username',
                    'password' => 'password',
                ],
                'loginUrl' => $this->authentication['service']['unauthenticatedRedirect'],
            ]
        );

        return $authenticationService;
    }

    /**
     * Returns authorization service instance.
     * @param ServerRequestInterface $request Request
     * @return AuthorizationServiceInterface
     */
    public function getAuthorizationService(ServerRequestInterface $request): AuthorizationServiceInterface
    {
        $resolver = new AclResolver;
        return new AuthorizationService($resolver);
    }

    /**
     * Permet de récupérer la liste des controlleurs de l'application
     *
     * @return array
     * @throws Exception
     */
    public static function getControllers(): array
    {
        $results = [];
        $files = glob(
            rtrim(
                current(
                    Utility::get(App::class)->classPath('Controller')
                ),
                DS
            ) . DS . '*Controller.php'
        );

        foreach ($files as $file) {
            $info = pathinfo($file);
            if ($info['filename'] !== 'AppController' && $info['filename'] !== 'Controller') {
                $results[] = self::extractNamespace($file);
            }
        }
        return $results;
    }

    /**
     * Extract namespace from a PHP file
     *
     * @param  string $file
     * @return string
     */
    public static function extractNamespace(string $file): string
    {
        $contents = file_get_contents($file);
        $namespace = $class = '';
        $gettingNamespace = $gettingClass = false;

        foreach (token_get_all($contents) as $token) {
            if (is_array($token) && $token[0] === T_NAMESPACE) {
                $gettingNamespace = true;
            }

            if (is_array($token) && $token[0] === T_CLASS) {
                $gettingClass = true;
            }

            if ($gettingNamespace) {
                if (is_array($token) && in_array($token[0], [T_STRING, T_NS_SEPARATOR])) {
                    $namespace .= $token[1];
                } elseif ($token === ';') {
                    $gettingNamespace = false;
                }
            }

            if ($gettingClass) {
                if (is_array($token) && $token[0] === T_STRING) {
                    $class = $token[1];
                    break;
                }
            }
        }

        return $namespace ? $namespace . '\\' . $class : $class;
    }
}
