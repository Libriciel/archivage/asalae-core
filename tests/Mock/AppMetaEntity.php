<?php
namespace AsalaeCore\Test\Mock;

use Cake\ORM\Entity;

class AppMetaEntity extends Entity
{
    use \AsalaeCore\Model\Entity\AppMetaTrait;

    protected $_metaFields = ['active', 'default', 'created_user_id', 'modifieds'];
}
