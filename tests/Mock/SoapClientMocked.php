<?php
namespace AsalaeCore\Test\Mock;

use SoapClient;

class SoapClientMocked extends SoapClient
{
    const TOKEN_SAMPLE = TESTS . 'Data' . DS . 'sample.token';

    public function __construct($wsdl, array $options = null)
    {
    }

    public function __soapCall(
        $name,
        /** @noinspection PhpSignatureMismatchDuringInheritanceInspection NOTE: mauvaise doc du parent */
        $array,
        $options = null,
        $input = null,
        &$output = null
    ) {
        switch ($name) {
            case 'createRequest':
            case 'createResponse':
                return true;
            case 'extractToken':
                return base64_encode(file_get_contents(self::TOKEN_SAMPLE));
            case 'getOpensslVersion':
                return 'not empty';
        }
    }
}
