<?php
namespace AsalaeCore\Test\Mock;

use Cake\ORM\Entity;

class StaticGenericUtility extends Entity
{
    public static $methods = [];

    public function __call($name, $arguments)
    {
        return self::__callStatic($name, $arguments);
    }

    public static function __callStatic($name, $arguments)
    {
        if (isset(self::$methods[$name])) {
            if (is_callable(self::$methods[$name])) {
                call_user_func_array(self::$methods[$name], $arguments);
            } elseif (is_array(self::$methods[$name])) {
                return next(self::$methods[$name]);
            } else {
                return self::$methods[$name];
            }
        }
    }
}
