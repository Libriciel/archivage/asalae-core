<?php

namespace AsalaeCore\Test\Mock;

use TrustedTimestamps\TrustedTimestamps;

class FakeTrustedTimestampMock extends TrustedTimestamps
{
    public static function createRequestfile($hash, $hash_algo = 'sha1')
    {
        return 'test';
    }

    public static function signRequestfile(
        $requestfile_path,
        $tsa_url,
        array $curlOpts = [],
        $timestamp_format = null
    ) {
        return [
            'response_string' => base64_encode('test'),
            'response_time' => 1
        ];
    }
}
