<?php
namespace AsalaeCore\Test\Mock;

use AsalaeCore\Driver\Timestamping\TimestampingIaik;

class TimestampingIaikMocked extends TimestampingIaik
{
    const TOKEN_SAMPLE = TESTS . 'Data' . DS . 'sample.token';

    public function generateToken($file): string
    {
        $this->getFileHash(self::TOKEN_SAMPLE, 'sha1');
        return file_get_contents(self::TOKEN_SAMPLE);
    }
}
