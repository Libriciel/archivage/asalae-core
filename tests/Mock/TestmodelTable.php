<?php
namespace AsalaeCore\Test\Mock;

use Cake\ORM\Table;

class TestmodelTable extends Table
{
    public function initialize(array $config): void
    {
        $this->addBehavior(
            'AsalaeCore.Options',
            [
                'my_field_name' => [
                    'value 1',
                    'value 2',
                    'value 3',
                ]
            ]
        );
    }
}
