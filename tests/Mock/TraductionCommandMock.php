<?php
namespace AsalaeCore\Test\Mock;

use AsalaeCore\Command\TraductionCommand;
use Cake\Console\ConsoleIo;

class TraductionCommandMock extends TraductionCommand
{
    public $out = [];
    public $in = [];
    public $files = [];
    public $index = 0;

    public function out($message, int $newlines = 1, int $level = ConsoleIo::NORMAL): ?int
    {
        $this->out[] = $message;
        return strlen($message);
    }

    public function in(string $prompt, $options = null, ?string $default = null): ?string
    {
        $result = $this->in[$this->index];
        $this->index++;
        return $result;
    }

    public function nextInputWillBe($foo)
    {
        $this->in[] = $foo;
    }

    public function createFileWithDefault($path, $contents, $default = 'n')
    {
        $this->files[] = compact('path', 'contents');
        return true;
    }
}
