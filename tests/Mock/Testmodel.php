<?php
namespace AsalaeCore\Test\Mock;

use Cake\ORM\Entity;

class Testmodel extends Entity
{
    protected function _getMyFieldNametrad()
    {
        switch ($this->_fields['my_field_name']) {
            case 'value 1':
                return __dx('testmodel', 'my_field_name', 'value 1');
            case 'value 2':
                return __dx('testmodel', 'my_field_name', 'value 2');
            case 'value 3':
                return __dx('testmodel', 'my_field_name', 'value 3');
            default:
                return null;
        }
    }
}
