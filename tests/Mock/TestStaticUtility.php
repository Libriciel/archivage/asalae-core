<?php
namespace AsalaeCore\Test\Mock;

abstract class TestStaticUtility
{
    public static function main()
    {
        return true;
    }

    public static function name()
    {
        return __CLASS__;
    }
}
