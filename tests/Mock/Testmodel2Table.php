<?php
namespace AsalaeCore\Test\Mock;

use Cake\ORM\Table;

class Testmodel2Table extends Table
{
    public function initialize(array $config): void
    {
        $this->addBehavior('AsalaeCore.Alias');
    }
}
