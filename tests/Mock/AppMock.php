<?php

namespace AsalaeCore\Test\Mock;

use Cake\Core\App;

class AppMock extends App
{
    public static function classPath(string $type, ?string $plugin = null): array
    {
        return [dirname(__DIR__, 2) .DS . 'tests' .DS.'MyPlugin'.DS.'src'.DS . $type . DS];
    }
}
