<?php
namespace AsalaeCore\Test\Mock;

class TestPrivateUtility
{
    private function __construct()
    {
    }

    public static function getInstance()
    {
        return new self;
    }

    public function main()
    {
        return true;
    }

    public function name()
    {
        return __CLASS__;
    }
}
