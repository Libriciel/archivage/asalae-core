<?php
namespace AsalaeCore\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * PronomsFixture
 */
class PronomsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'name' => 'Microsoft Word for Macintosh Document',
                'puid' => 'x-fmt/1',
                'mime' => 'application/msword'
            ],
            [
                'name' => 'Acrobat PDF 1.4 - Portable Document Format',
                'puid' => 'fmt/18',
                'mime' => 'application/pdf'
            ],
            [
                'name' => 'OpenDocument Text',
                'puid' => 'fmt/291',
                'mime' => 'application/vnd.oasis.opendocument.text'
            ],
        ];
        parent::init();
    }
}
