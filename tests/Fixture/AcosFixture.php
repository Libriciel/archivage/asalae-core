<?php

namespace AsalaeCore\Test\Fixture;

use Cake\Core\Configure;
use Cake\TestSuite\Fixture\TestFixture;
use Cake\Utility\Hash;

/**
 * AcosFixture
 *
 */
class AcosFixture extends TestFixture
{
    public $records = [
        [// id=1
            '_id' => 1,
            'parent_id' => null,
            'model' => '',
            'foreign_key' => null,
            'alias' => 'root',
            'lft' => 1,
            'rght' => 10,
        ],
        [// id=2
            '_id' => 2,
            'parent_id' => 1,
            'model' => 'root',
            'foreign_key' => null,
            'alias' => 'controllers',
            'lft' => 2,
            'rght' => 7,
        ],
        [// id=3
            '_id' => 3,
            'parent_id' => 2,
            'model' => 'controllers',
            'foreign_key' => null,
            'alias' => 'Home',
            'lft' => 3,
            'rght' => 6,
        ],
        [// id=4
            '_id' => 4,
            'parent_id' => 3,
            'model' => 'Home',
            'foreign_key' => null,
            'alias' => 'index',
            'lft' => 4,
            'rght' => 5,
        ],
        [// id=5
            '_id' => 5,
            'parent_id' => 1,
            'model' => 'root',
            'foreign_key' => null,
            'alias' => 'api',
            'lft' => 8,
            'rght' => 9,
        ],
    ];

    private static $genRecords = [];

    /**
     * Ajout dans self::$records du contenu de src/Data/controllers.json
     */
    public function __construct()
    {
        if (empty(self::$genRecords)) {
            $controllersJson = Configure::read('App.paths.controllers_rules', APP . 'Data' . DS . 'controllers.json');
            $apisJson = Configure::read('App.paths.apis_rules', APP . 'Data' . DS . 'apis.json');
            if (!is_file($controllersJson)
                || !($controllers = json_decode(file_get_contents($controllersJson), true))
                || !is_file($apisJson)
                || !($apis = json_decode(file_get_contents($apisJson), true))
            ) {
                return parent::__construct();
            }
            foreach (Hash::merge($controllers, $apis) as $controller => $actions) {
                $id = count($this->records) +1;
                $apiId = $id +1;
                $this->records[$id] = [
                    '_id' => $id,
                    'alias' => $controller,
                    'model' => 'controllers',
                    'parent_id' => 2,
                ];
                $this->records[$apiId] = [
                    '_id' => $apiId,
                    'alias' => $controller,
                    'model' => 'api',
                    'parent_id' => 3,
                ];
                $actionId = $apiId;
                foreach ($actions as $action => $params) {
                    $this->records[] = [
                        '_id' => ++$actionId,
                        'alias' => $action,
                        'model' => $controller,
                        'parent_id' => $id
                    ];
                }
                foreach ($apis[$controller] ?? [] as $action => $params) {
                    $this->records[] = [
                        '_id' => ++$actionId,
                        'alias' => $action,
                        'model' => $controller,
                        'parent_id' => $apiId
                    ];
                }
            }
            $this->calcTree();
            self::$genRecords = $this->records;
        } else {
            $this->records = self::$genRecords;
        }
        return parent::__construct();
    }

    /**
     * Calcul du lft et rght ($Acos->recover() cause un timeout (30s))
     * @throws \Exception
     */
    private function calcTree()
    {
        foreach ($this->records as $key => $values) {
            if ($values['parent_id'] === null) {
                $root =& $this->records[$key];
                $root['lft'] = 1;
                $root['rght'] = 4;
                break;
            }
        }
        if (empty($root)) {
            throw new \Exception();
        }
        foreach ($this->records as $key => $values) {
            if ($values['parent_id'] === $root['_id'] && $values['alias'] === 'controllers') {
                $controllers =& $this->records[$key];
                $controllers['lft'] = 2;
                $controllers['rght'] = 3;
                break;
            }
        }
        if (empty($controllers)) {
            throw new \Exception();
        }
        $lft = 2;
        foreach ($this->records as $key => $values) {
            if ($values['parent_id'] === $controllers['_id']) {
                $controller =& $this->records[$key];
                $controller['lft'] = ++$lft;
                foreach ($this->records as $k => $sv) {
                    if ($sv['parent_id'] === $controller['_id']) {
                        $action =& $this->records[$k];
                        $action['lft'] = ++$lft;
                        $action['rght'] = ++$lft;
                    }
                }
                $controller['rght'] = ++$lft;
                $controllers['rght'] = $lft +1;
                $root['rght'] = $lft +2;
            }
        }
        foreach ($this->records as $key => $values) {
            if ($values['parent_id'] === $root['_id'] && $values['alias'] === 'api') {
                $api =& $this->records[$key];
                $api['lft'] = ++$lft;
                $api['rght'] = ++$lft;
                $root['rght'] = $lft +1;
                break;
            }
        }
        if (empty($api)) {
            throw new \Exception();
        }
        foreach ($this->records as $key => $values) {
            if ($values['parent_id'] === $api['_id']) {
                $controller =& $this->records[$key];
                $controller['lft'] = ++$lft;
                foreach ($this->records as $k => $sv) {
                    if ($sv['parent_id'] === $controller['_id']) {
                        $action =& $this->records[$k];
                        $action['lft'] = ++$lft;
                        $action['rght'] = ++$lft;
                    }
                }
                $controller['rght'] = ++$lft;
                $api['rght'] = $lft +1;
                $root['rght'] = $lft +2;
            }
        }
    }
}
