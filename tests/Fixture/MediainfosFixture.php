<?php
namespace AsalaeCore\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * MediainfosFixture
 */
class MediainfosFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'fileupload_id' => 1,
                'unique_id' => 'Lorem ipsum dolor sit amet',
                'complete_name' => 'Lorem ipsum dolor sit amet',
                'format' => 'Lorem ipsum dolor sit amet',
                'format_version' => 'Lorem ipsum dolor sit amet',
                'file_size' => 'Lorem ipsum dolor sit amet',
                'duration' => 'Lorem ipsum dolor sit amet',
                'overall_bit_rate' => 'Lorem ipsum dolor sit amet',
                'movie_name' => 'Lorem ipsum dolor sit amet',
                'encoded_date' => 'Lorem ipsum dolor sit amet',
                'writing_application' => 'Lorem ipsum dolor sit amet',
                'writing_library' => 'Lorem ipsum dolor sit amet',
            ],
        ];
        parent::init();
    }
}
