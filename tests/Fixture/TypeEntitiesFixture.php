<?php
namespace AsalaeCore\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * TypeEntitiesFixture
 */
class TypeEntitiesFixture extends TestFixture
{
    /**
     * Valeurs par défaut
     * @return array[]
     */
    public static function defaultValues()
    {
        return [
            [
                'name' => "Service d'Exploitation",
                'active' => true,
                'code' => 'SE',
                'created' => '2018-06-27 16:06:31',
                'modified' => '2018-06-27 16:06:31',
            ],
            [
                'name' => "Service d'Archives",
                'active' => true,
                'code' => 'SA',
                'created' => '2018-06-27 16:06:31',
                'modified' => '2018-06-27 16:06:31',
            ],
            [
                'name' => "Service de Contrôle Scientifique et Technique",
                'active' => true,
                'code' => 'CST',
                'created' => '2018-06-27 16:06:31',
                'modified' => '2018-06-27 16:06:31',
            ],
            [
                'name' => "Service Versant",
                'active' => true,
                'code' => 'SV',
                'created' => '2018-06-27 16:06:31',
                'modified' => '2018-06-27 16:06:31',
            ],
            [
                'name' => "Service Producteur",
                'active' => true,
                'code' => 'SP',
                'created' => '2018-06-27 16:06:31',
                'modified' => '2018-06-27 16:06:31',
            ],
            [
                'name' => "Service Demandeur",
                'active' => true,
                'code' => 'SD',
                'created' => '2018-06-27 16:06:31',
                'modified' => '2018-06-27 16:06:31',
            ],
            [
                'name' => "Opérateur de versement",
                'active' => true,
                'code' => 'OV',
                'created' => '2018-06-27 16:06:31',
                'modified' => '2018-06-27 16:06:31',
            ],
            [
                'name' => "Organisationnel",
                'active' => true,
                'code' => 'SO',
                'created' => '2018-06-27 16:06:31',
                'modified' => '2018-06-27 16:06:31',
            ],
        ];
    }

    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = static::defaultValues();
        parent::init();
    }
}
