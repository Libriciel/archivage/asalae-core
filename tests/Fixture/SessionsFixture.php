<?php
namespace AsalaeCore\Test\Fixture;

use AsalaeCore\Utility\Session;
use Cake\ORM\Entity;
use Cake\TestSuite\Fixture\TestFixture;
use Exception;

/**
 * SessionsFixture
 */
class SessionsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     * @throws Exception
     */
    public function init(): void
    {
        $user = new Entity(['id' => 1]);
        $orgEntity = new Entity(['id' => 2, 'lft' => 1, 'rght' => 2]);

        $orgEntity->set('archival_agency', clone $orgEntity);
        $user->set('org_entity', $orgEntity);
        $this->records = [
            [
                'id' => 'f45df7be-ca83-44ef-960c-1856a8893eb6',
                'created' => 1550055324,
                'modified' => 1550055324,
                'data' => Session::serialize(['Auth' => $user]),
                'expires' => time() + 1440,// dans 24 minutes (durée session par défaut)
                'user_id' => 1,
                'token' => 'Lorem ipsum dolor sit amet'
            ],
        ];
        parent::init();
    }
}
