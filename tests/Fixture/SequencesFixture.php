<?php
namespace AsalaeCore\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * SequencesFixture
 */
class SequencesFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'org_entity_id' => 1,
                'name' => 'test',
                'description' => 'Sequence de test',
                'value' => 0,
                'created' => '2018-06-27 16:06:31',
                'modified' => '2018-06-27 16:06:31',
            ],
        ];
        for ($i = 0; $i < 14; $i++) {
            $this->records[] = [
                'org_entity_id' => 2,
                'name' => 'seq'.$i,
                'description' => 'Sequence de test',
                'value' => 0,
                'created' => '2018-06-27 16:06:31',
                'modified' => '2018-06-27 16:06:31',
            ];
        }
        parent::init();
    }
}
