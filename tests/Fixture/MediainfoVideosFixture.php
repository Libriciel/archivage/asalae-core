<?php
namespace AsalaeCore\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * MediainfoVideosFixture
 */
class MediainfoVideosFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'mediainfo_id' => 1,
                'format' => 'Lorem ipsum dolor sit amet',
                'format_info' => 'Lorem ipsum dolor sit amet',
                'format_profile' => 'Lorem ipsum dolor sit amet',
                'format_settings_cabac' => 'Lorem ipsum dolor sit amet',
                'format_settings_reframes' => 'Lorem ipsum dolor sit amet',
                'codec_id' => 'Lorem ipsum dolor sit amet',
                'duration' => 'Lorem ipsum dolor sit amet',
                'bit_rate' => 'Lorem ipsum dolor sit amet',
                'width' => 'Lorem ipsum dolor sit amet',
                'height' => 'Lorem ipsum dolor sit amet',
                'display_aspect_ratio' => 'Lorem ipsum dolor sit amet',
                'frame_rate_mode' => 'Lorem ipsum dolor sit amet',
                'frame_rate' => 'Lorem ipsum dolor sit amet',
                'color_space' => 'Lorem ipsum dolor sit amet',
                'chroma_subsampling' => 'Lorem ipsum dolor sit amet',
                'bit_depth' => 'Lorem ipsum dolor sit amet',
                'scan_type' => 'Lorem ipsum dolor sit amet',
                'bits_pixel_frame_' => 'Lorem ipsum dolor sit amet',
                'stream_size' => 'Lorem ipsum dolor sit amet',
                'title' => 'Lorem ipsum dolor sit amet',
                'writing_library' => 'Lorem ipsum dolor sit amet',
                'encoding_settings' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
                'language' => 'Lorem ipsum dolor sit amet',
                '_default' => 'Lorem ipsum dolor sit amet',
                'forced' => 'Lorem ipsum dolor sit amet',
                'color_range' => 'Lorem ipsum dolor sit amet',
                'color_primaries' => 'Lorem ipsum dolor sit amet',
                'transfer_characteristics' => 'Lorem ipsum dolor sit amet',
                'matrix_coefficients' => 'Lorem ipsum dolor sit amet',
            ],
        ];
        parent::init();
    }
}
