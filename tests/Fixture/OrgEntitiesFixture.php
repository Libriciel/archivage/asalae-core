<?php
namespace AsalaeCore\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * OrgEntitiesFixture
 */
class OrgEntitiesFixture extends TestFixture
{
    /**
     * Valeurs par défaut
     * @return array[]
     */
    public static function defaultValues()
    {
        return [
            [// id=1
                'name' => 'Service d\'exploitation',
                'identifier' => 'se',
                'parent_id' => null,
                'lft' => 1,
                'rght' => 10,
                'created' => '2021-12-09T13:54:14',
                'modified' => '2021-12-09T13:54:14',
                'type_entity_id' => 1,
                'timestamper_id' => null,
                'active' => true,
                'is_main_archival_agency' => false,
                'description' => null,
                'short_name' => null,
            ],
            [// id=2
                'name' => 'Mon Service d\'archive',
                'identifier' => 'sa',
                'parent_id' => 1,
                'lft' => 2,
                'rght' => 7,
                'created' => '2021-12-09T13:54:15',
                'modified' => '2021-12-09T13:54:15',
                'type_entity_id' => 2,
                'timestamper_id' => 1,
                'active' => true,
                'is_main_archival_agency' => true,
                'description' => null,
                'short_name' => null,
            ],
            [// id=3
                'name' => 'Mon autre service d\'archive',
                'identifier' => 'sa2',
                'parent_id' => 1,
                'lft' => 8,
                'rght' => 9,
                'created' => '2021-12-09T13:54:15',
                'modified' => '2021-12-09T16:29:18',
                'type_entity_id' => 2,
                'timestamper_id' => 1,
                'active' => true,
                'is_main_archival_agency' => false,
                'description' => null,
                'short_name' => null,
            ],
            [// id=4
                'name' => 'Mon service versant',
                'identifier' => 'sv',
                'parent_id' => 2,
                'lft' => 3,
                'rght' => 6,
                'created' => '2021-12-09T13:54:15',
                'modified' => '2021-12-09T13:54:15',
                'type_entity_id' => 4,
                'timestamper_id' => null,
                'active' => true,
                'is_main_archival_agency' => null,
                'description' => '',
                'short_name' => null,
            ],
            [// id=5
                'name' => 'Mon service producteur',
                'identifier' => 'sp',
                'parent_id' => 4,
                'lft' => 4,
                'rght' => 5,
                'created' => '2021-12-09T13:54:15',
                'modified' => '2021-12-09T13:54:15',
                'type_entity_id' => 5,
                'timestamper_id' => null,
                'active' => true,
                'is_main_archival_agency' => null,
                'description' => '',
                'short_name' => null,
            ],
        ];
    }

    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = static::defaultValues();
        parent::init();
    }
}
