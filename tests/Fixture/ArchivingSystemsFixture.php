<?php
declare(strict_types=1);

namespace AsalaeCore\Test\Fixture;

use AsalaeCore\DataType\EncryptedString;
use Cake\TestSuite\Fixture\TestFixture;
use Cake\Utility\Security;

/**
 * ArchivingSystemsFixture
 */
class ArchivingSystemsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $decryptKey = hash('sha256', EncryptedString::DECRYPT_KEY);
        $this->records = [
            [
                'org_entity_id' => 2,
                'name' => 'SAE distant',
                'description' => 'test',
                'url' => 'https://localhost',
                'username' => 'testunit',
                'password' => Security::encrypt('testunit', $decryptKey),
                'use_proxy' => false,
                'created' => 1613054329,
                'modified' => 1613054329,
                'active' => true,
                'ssl_verify_peer' => true,
                'ssl_verify_peer_name' => true,
                'ssl_verify_depth' => 5,
                'ssl_verify_host' => true,
                'ssl_cafile' => null,
            ],
        ];
        parent::init();
    }
}
