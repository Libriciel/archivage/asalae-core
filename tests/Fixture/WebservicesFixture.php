<?php
namespace AsalaeCore\Test\Fixture;

use AsalaeCore\Model\Entity\Webservice;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Core\Configure;
use Cake\TestSuite\Fixture\TestFixture;

/**
 * WebservicesFixture
 */
class WebservicesFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $baseUrl = Configure::read('App.fullBaseUrl');
        $hasher = new DefaultPasswordHasher;
        $entity = new Webservice;
        $this->records = [
            [
                'name' => 'Test',
                'description' => 'Webservice de test',
                'username' => 'test',
                'password' => $hasher->hash('test'),
                'active' => 1,
                'created' => '2018-07-26 15:57:30',
                'modified' => '2018-07-26 15:57:30',
                'homepage' => null,
                'callback' => null,
                'client_id' => null,
                'client_secret' => null
            ],
            [
                'name' => 'OAuth2',
                'description' => 'Webservice de test OAuth2',
                'username' => 'test',
                'password' => $hasher->hash('test'),
                'active' => 1,
                'created' => '2018-07-26 15:57:30',
                'modified' => '2018-07-26 15:57:30',
                'homepage' => $baseUrl,
                'callback' => $baseUrl,
                'client_id' => 'some_random_generated_code',
                'client_secret' => $entity->clientSecretSetter('some_random_generated_code')
            ],
        ];
        parent::init();
    }
}
