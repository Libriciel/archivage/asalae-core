<?php
namespace AsalaeCore\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ConfigurationsFixture
 */
class ConfigurationsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'org_entity_id' => 4,
                'name' => 'background-image',
                'setting' => DS . 'org-entity-data' . DS . '4' . DS . 'testunit.png'
            ],
            [
                'org_entity_id' => 4,
                'name' => 'background-position',
                'setting' => '20px 20px'
            ],
        ];
        parent::init();
    }
}
