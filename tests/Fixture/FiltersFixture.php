<?php
namespace AsalaeCore\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * FiltersFixture
 */
class FiltersFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'saved_filter_id' => 1,
                'key' => 'Lorem ipsum dolor sit amet',
                'value' => 'Lorem ipsum dolor sit amet',
                'created' => 1568965756
            ],
        ];
        parent::init();
    }
}
