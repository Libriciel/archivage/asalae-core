<?php
namespace AsalaeCore\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * NotificationsFixture
 */
class NotificationsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'user_id' => 1,
                'text' => "Ceci n'est pas un test",
                'created' => '2018-07-26 10:47:22',
                'css_class' => 'alert alert-info',
            ],
        ];
        parent::init();
    }
}
