<?php
namespace AsalaeCore\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ArosFixture
 */
class ArosFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'parent_id' => null,
                'model' => 'Roles',
                'foreign_key' => 1,
                'alias' => 'test',
                'lft' => 1,
                'rght' => 4
            ],
            [
                'parent_id' => 1,
                'model' => 'Users',
                'foreign_key' => 1,
                'alias' => 'test',
                'lft' => 2,
                'rght' => 3
            ],
            [
                'parent_id' => null,
                'model' => 'Webservices',
                'foreign_key' => 1,
                'alias' => 'test',
                'lft' => 5,
                'rght' => 6
            ],
            [
                'parent_id' => null,
                'model' => 'Roles',
                'foreign_key' => 2,
                'alias' => 'test2',
                'lft' => 7,
                'rght' => 8
            ],
            [
                'parent_id' => null,
                'model' => 'Roles',
                'foreign_key' => 3,
                'alias' => 'Archiviste',
                'lft' => 9,
                'rght' => 10
            ],
            [
                'parent_id' => null,
                'model' => 'Roles',
                'foreign_key' => 4,
                'alias' => 'Webservice versant',
                'lft' => 11,
                'rght' => 12
            ],
        ];
        parent::init();
    }
}
