<?php
namespace AsalaeCore\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * TransferAttachmentsFixture
 */
class TransferAttachmentsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'transfer_id' => 1,
                'filename' => 'file',
                'size' => 1,
                'hash' => 'ABD1472D2DDB6BC98565B56CE59A5FB4C66D108EB9643E5E503C35C243EB69C9',
                'hash_algo' => 'sha256',
                'virus_name' => null,
                'valid' => null,
                'mime' => 'Lorem ipsum dolor sit amet',
                'extension' => 'Lorem ipsum dolor sit amet',
                'format' => 'Lorem ipsum dolor sit amet',
                'path' => sys_get_temp_dir().'/testunit/transfers/1/attachments/file', // virtual field
                'xpath' => null,
            ],
            [
                'transfer_id' => 1,
                'filename' => 'sample.pdf',
                'size' => 1,
                'hash' => 'ABD1472D2DDB6BC98565B56CE59A5FB4C66D108EB9643E5E503C35C243EB69C9',
                'hash_algo' => 'sha256',
                'virus_name' => null,
                'valid' => null,
                'mime' => 'Lorem ipsum dolor sit amet',
                'extension' => 'Lorem ipsum dolor sit amet',
                'format' => 'Lorem ipsum dolor sit amet',
                'path' => sys_get_temp_dir().'/testunit/transfers/1/attachments/sample.pdf', // virtual field
                'xpath' => null,
            ],
            [
                'transfer_id' => 3,
                'filename' => 'sample.pdf',
                'size' => 1,
                'hash' => 'ABD1472D2DDB6BC98565B56CE59A5FB4C66D108EB9643E5E503C35C243EB69C9',
                'hash_algo' => 'sha256',
                'virus_name' => null,
                'valid' => null,
                'mime' => 'Lorem ipsum dolor sit amet',
                'extension' => 'Lorem ipsum dolor sit amet',
                'format' => 'Lorem ipsum dolor sit amet',
                'path' => sys_get_temp_dir().'/testunit/transfers/3/attachments/sample.pdf', // virtual field
                'xpath' => null,
            ],
        ];
        parent::init();
    }
}
