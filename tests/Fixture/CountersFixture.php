<?php
namespace AsalaeCore\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;
use Cake\Utility\Hash;

/**
 * CountersFixture
 */
class CountersFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'org_entity_id' => 2,
                'identifier' => 'test',
                'name' => 'Compteur de test',
                'description' => "Permet d'effectuer les tests unitaires",
                'type' => 'interne',
                'definition_mask' => 'TEST_#TestString#_#AAAA#-#MM#-#JJ#_#AA#-#M#-#J#x#S#x#SS#x#SSS#xxx#0#x#00#x#000#xxx#s#',
                'sequence_reset_mask' => '#AAAA#',
                'sequence_reset_value' => '',
                'sequence_id' => 1,
                'active' => true,
                'created' => '2018-07-26 10:47:22',
                'modified' => '2018-07-26 10:47:22'
            ],
            [
                'org_entity_id' => 2,
                'identifier' => 'ArchiveTransfer',
                'name' => 'ArchiveTransfert',
                'description' => "Utilisé pour générer le code TransferIdentifier des bordereaux de transfert.",
                'definition_mask' => 'AT_#s#',
                'sequence_reset_mask' => null,
                'sequence_reset_value' => null,
                'sequence_id' => 1,
                'active' => true,
                'type' => 'interne',
                'created' => '2018-07-26 10:47:22',
                'modified' => '2018-07-26 10:47:22'
            ],
            [
                'org_entity_id' => 2,
                'identifier' => 'ArchiveTransferReply',
                'name' => 'ArchiveTransferReply',
                'description' => "",
                'definition_mask' => 'ATR_#s#',
                'sequence_reset_mask' => null,
                'sequence_reset_value' => null,
                'sequence_id' => 1,
                'active' => true,
                'type' => 'interne',
                'created' => '2018-07-26 10:47:22',
                'modified' => '2018-07-26 10:47:22'
            ],
            [
                'org_entity_id' => 2,
                'identifier' => 'AcknowledgementIdentifier',
                'name' => 'AcknowledgementIdentifier',
                'description' => "",
                'definition_mask' => 'ACK_#s##0000000000#',
                'sequence_reset_mask' => null,
                'sequence_reset_value' => null,
                'sequence_id' => 1,
                'active' => true,
                'type' => 'interne',
                'created' => '2018-07-26 10:47:22',
                'modified' => '2018-07-26 10:47:22'
            ],
            [
                'org_entity_id' => 2,
                'identifier' => 'ArchiveTransferAcceptance',
                'name' => 'ArchiveTransferAcceptance',
                'description' => "",
                'definition_mask' => 'ATA_#s#',
                'sequence_reset_mask' => null,
                'sequence_reset_value' => null,
                'sequence_id' => 1,
                'active' => true,
                'type' => 'interne',
                'created' => '2018-07-26 10:47:22',
                'modified' => '2018-07-26 10:47:22'
            ],
        ];
        $counters = json_decode(file_get_contents(RESOURCES.'counters.json'), true);
        $sequence_id = 2;
        $identifiers = Hash::extract($this->records, '{n}.identifier');
        foreach ($counters as $counter) {
            if (in_array($counter['identifier'], $identifiers)) {
                continue;
            }
            $this->records[] = [
                'org_entity_id' => 2,
                'sequence_id' => $sequence_id++,
                'created' => '2018-07-26 10:47:22',
                'modified' => '2018-07-26 10:47:22',
            ] + $counter;
        }
        parent::init();
    }
}
