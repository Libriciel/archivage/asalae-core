<?php
namespace AsalaeCore\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * EaccpfsFixture
 */
class EaccpfsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'record_id' => 'dsi',
                'entity_id' => null,
                'entity_type' => 'corporateBody',
                'name' => 'DSI Libriciel',
                'data' => '{"eac-cpf":{"control":{"recordId":"dsi","maintenanceStatus":"new","maintenanceAgency":{"agencyName":"DSI Libriciel"},"maintenanceHistory":{"maintenanceEvent":[{"eventType":"created","eventDateTime":{"@standardDateTime":"2018-06-27T16:06:31","@":"2018-06-27T16:06:31"},"agentType":"human","agent":"L","eventDescription":"Created from Asalae"}]}},"cpfDescription":{"identity":{"entityId":null,"entityType":"corporateBody","nameEntry":{"part":"DSI Libriciel"}},"description":{"existDates":{"date":"2018-06-27T00:00:00+02:00"}}},"@xmlns":"urn:isbn:1-931666-33-4","@xmlns:xlink":"http:\/\/www.w3.org\/1999\/xlink"}}',
                'created' => '2018-06-27 16:06:31',
                'modified' => '2018-06-27 16:06:31',
                'agency_name' => 'DSI Libriciel',
                'org_entity_id' => 1,
                'from_date' => '2018-06-27',
                'to_date' => null,
            ],
            [
                'record_id' => 'sa',
                'entity_id' => null,
                'entity_type' => 'corporateBody',
                'name' => 'SA Libriciel',
                'data' => '{"eac-cpf":{"control":{"recordId":"sa","maintenanceStatus":"new","maintenanceAgency":{"agencyName":"SA Libriciel"},"maintenanceHistory":{"maintenanceEvent":[{"eventType":"created","eventDateTime":{"@standardDateTime":"2018-06-27T16:06:31","@":"2018-06-27T16:06:31"},"agentType":"human","agent":"L","eventDescription":"Created from Asalae"}]}},"cpfDescription":{"identity":{"entityId":null,"entityType":"corporateBody","nameEntry":{"part":"SA Libriciel"}},"description":{"existDates":{"date":"2018-06-27T00:00:00+02:00"}}},"@xmlns":"urn:isbn:1-931666-33-4","@xmlns:xlink":"http:\/\/www.w3.org\/1999\/xlink"}}',
                'created' => '2018-06-27 16:06:31',
                'modified' => '2018-06-27 16:06:31',
                'agency_name' => 'SA Libriciel',
                'org_entity_id' => 2,
                'from_date' => '2018-06-27',
                'to_date' => null,
            ],
            [
                'record_id' => 'cst',
                'entity_id' => null,
                'entity_type' => 'corporateBody',
                'name' => 'Direction',
                'data' => '{"eac-cpf":{"control":{"recordId":"cst","maintenanceStatus":"new","maintenanceAgency":{"agencyName":"Direction"},"maintenanceHistory":{"maintenanceEvent":[{"eventType":"created","eventDateTime":{"@standardDateTime":"2018-06-27T16:06:31","@":"2018-06-27T16:06:31"},"agentType":"human","agent":"L","eventDescription":"Created from Asalae"}]}},"cpfDescription":{"identity":{"entityId":null,"entityType":"corporateBody","nameEntry":{"part":"SA Libriciel"}},"description":{"existDates":{"date":"2018-06-27T00:00:00+02:00"}}},"@xmlns":"urn:isbn:1-931666-33-4","@xmlns:xlink":"http:\/\/www.w3.org\/1999\/xlink"}}',
                'created' => '2018-06-27 16:06:31',
                'modified' => '2018-06-27 16:06:31',
                'agency_name' => 'SA Libriciel',
                'org_entity_id' => 3,
                'from_date' => '2018-06-27',
                'to_date' => null,
            ],
        ];
        parent::init();
    }
}
