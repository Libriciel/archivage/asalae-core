<?php
namespace AsalaeCore\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ServiceLevelsFixture
 */
class ServiceLevelsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [// 1
                'org_entity_id' => 2,
                'identifier' => 'service-level-test',
                'name' => 'Niveau de service de test',
                'description' => 'Niveau de service de test',
                'active' => true,
                'created' => '2018-07-26 10:47:22',
                'modified' => '2018-07-26 10:47:22',
                'ts_msg_id' => 1,
                'ts_pjs_id' => 1,
                'ts_conv_id' => 1,
                'default_level' => true,
                'secure_data_space_id' => 1,
                'convert_preservation' => null,
                'convert_dissemination' => null,
            ],
        ];
        parent::init();
    }
}
