<?php
namespace AsalaeCore\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * FileuploadsFixture
 */
class FileuploadsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'name' => 'testfile.txt',
                'path' => TMP_TESTDIR.DS.'testfile.txt',
                'size' => 1,
                'hash' => 'ABD1472D2DDB6BC98565B56CE59A5FB4C66D108EB9643E5E503C35C243EB69C9',
                'hash_algo' => 'sha256',
                'created' => 1547021711,
                'modified' => 1547021711,
                'user_id' => 1,
                'valid' => 1,
                'state' => 'Lorem ipsum dolor sit amet',
                'mime' => 'plain/text',
                'locked' => 1
            ],
        ];
        parent::init();
    }
}
