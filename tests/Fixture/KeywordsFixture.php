<?php
namespace AsalaeCore\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * KeywordsFixture
 */
class KeywordsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'keyword_list_id' => 1,
                'version' => 0,
                'code' => 'code-1',
                'name' => 'Code 1',
                'app_meta' => '{"created_user_id":1}',
                'parent_id' => null,
                'lft' => 1,
                'rght' => 2,
                'exact_match' => null,
                'change_note' => null,
                'created' => '2018-07-26 10:47:22',
                'modified' => '2018-07-26 10:47:22'
            ],
            [
                'keyword_list_id' => 2,
                'version' => 1,
                'code' => 'testunit1',
                'name' => 'testunit 1',
                'app_meta' => '{"created_user_id":1}',
                'parent_id' => null,
                'lft' => 3,
                'rght' => 4,
                'exact_match' => null,
                'change_note' => null,
                'created' => '2018-07-26 10:47:22',
                'modified' => '2018-07-26 10:47:22'
            ],
            [
                'keyword_list_id' => 2,
                'version' => 1,
                'code' => 'testunit2',
                'name' => 'testunit 2',
                'app_meta' => '{"created_user_id":1}',
                'parent_id' => null,
                'lft' => 5,
                'rght' => 6,
                'exact_match' => null,
                'change_note' => null,
                'created' => '2018-07-26 10:47:22',
                'modified' => '2018-07-26 10:47:22'
            ],
        ];
        parent::init();
    }
}
