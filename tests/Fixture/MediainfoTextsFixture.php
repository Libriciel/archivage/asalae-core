<?php
namespace AsalaeCore\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * MediainfoTextsFixture
 */
class MediainfoTextsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'mediainfo_id' => 1,
                'format' => 'Lorem ipsum dolor sit amet',
                'codec_id' => 'Lorem ipsum dolor sit amet',
                'codec_id_info' => 'Lorem ipsum dolor sit amet',
                'duration' => 'Lorem ipsum dolor sit amet',
                'bit_rate' => 'Lorem ipsum dolor sit amet',
                'count_of_elements' => 'Lorem ipsum dolor sit amet',
                'stream_size' => 'Lorem ipsum dolor sit amet',
                'title' => 'Lorem ipsum dolor sit amet',
                'language' => 'Lorem ipsum dolor sit amet',
                '_default' => 'Lorem ipsum dolor sit amet',
                'forced' => 'Lorem ipsum dolor sit amet',
            ],
        ];
        parent::init();
    }
}
