<?php
namespace AsalaeCore\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * RolesFixture
 */
class RolesFixture extends TestFixture
{
    /**
     * Valeurs par défaut
     * @return array[]
     */
    public static function defaultValues()
    {
        return [
            [
                'name' => 'test',
                'active' => true,
                'created' => '2018-06-27 16:06:31',
                'modified' => '2018-06-27 16:06:31',
                'org_entity_id' => 2,
                'code' => 'test',
                'parent_id' => null,
                'lft' => 1,
                'rght' => 2,
                'hierarchical_view' => false,
                'description' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
                'agent_type' => 'person',
            ],
            [
                'name' => 'test2',
                'active' => true,
                'created' => '2018-06-27 16:06:31',
                'modified' => '2018-06-27 16:06:31',
                'org_entity_id' => 2,
                'code' => 'test2',
                'parent_id' => null,
                'lft' => 3,
                'rght' => 4,
                'hierarchical_view' => false,
                'description' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
                'agent_type' => 'person',
            ],
            [
                'name' => 'Archiviste',
                'active' => true,
                'created' => '2018-06-27 16:06:31',
                'modified' => '2018-06-27 16:06:31',
                'org_entity_id' => null,
                'code' => 'AA',
                'parent_id' => null,
                'lft' => 5,
                'rght' => 6,
                'hierarchical_view' => true,
                'description' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
                'agent_type' => 'person',
            ],
            [
                'name' => 'Webservice versant',
                'active' => true,
                'created' => '2018-06-27 16:06:31',
                'modified' => '2018-06-27 16:06:31',
                'org_entity_id' => null,
                'code' => 'WSV',
                'parent_id' => null,
                'lft' => 7,
                'rght' => 8,
                'hierarchical_view' => false,
                'description' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
                'agent_type' => 'software',
            ],
        ];
    }

    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = static::defaultValues();
        parent::init();
    }
}
