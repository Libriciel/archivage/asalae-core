<?php
namespace AsalaeCore\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ProfilesFixture
 */
class ProfilesFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'org_entity_id' => 2,
                'identifier' => 'test',
                'name' => 'Test',
                'description' => 'sample',
                'active' => true,
                'created' => '2018-06-27 16:06:31',
                'modified' => '2018-06-27 16:06:31',
            ]
        ];
        parent::init();
    }
}
