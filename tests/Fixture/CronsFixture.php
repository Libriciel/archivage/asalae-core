<?php
namespace AsalaeCore\Test\Fixture;

use AsalaeCore\Cron\LiberSign;
use AsalaeCore\Cron\Unlocker;
use Cake\TestSuite\Fixture\TestFixture;

/**
 * CronsFixture
 */
class CronsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'name' => 'Lorem ipsum dolor sit amet',
                'description' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
                'classname' => LiberSign::class,
                'active' => 1,
                'locked' => 1,
                'frequency' => 'P1D',
                'next' => 1553271145,
                'app_meta' => '{}',
                'parallelisable' => 1,
                'max_duration' => 'PT1H',
                'last_email' => 1584629104,
            ],
            [
                'name' => 'Lorem ipsum dolor sit amet 2',
                'description' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
                'classname' => Unlocker::class,
                'active' => 1,
                'locked' => 1,
                'frequency' => 'P1D',
                'next' => 1553271145,
                'app_meta' => '{}',
                'parallelisable' => 1,
                'max_duration' => 'PT1H',
                'last_email' => 1584629104,
            ],
        ];
        parent::init();
    }
}
