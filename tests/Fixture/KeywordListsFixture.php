<?php
namespace AsalaeCore\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * KeywordListsFixture
 */
class KeywordListsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'org_entity_id' => 2,
                'identifier' => 'test',
                'name' => 'Test',
                'description' => 'Liste de test',
                'active' => true,
                'created' => '2018-04-25 09:34:17.000000',
                'modified' => '2018-04-25 09:34:17.000000',
                'version' => 0,
            ],
            [
                'org_entity_id' => 2,
                'identifier' => 'test2',
                'name' => 'Test2',
                'description' => 'Liste de test 2',
                'active' => true,
                'created' => '2018-04-25 09:34:17.000000',
                'modified' => '2018-04-25 09:34:17.000000',
                'version' => 1,
            ],
        ];
        parent::init();
    }
}
