<?php
namespace AsalaeCore\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * AgreementsFixture
 */
class AgreementsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        parent::init();
        $this->records = [
            [// 1
                'org_entity_id' => 2,
                'identifier' => 'agr-test01',
                'name' => 'Accord de versement de test 01',
                'description' => 'Accord de versement de test 01',
                'active' => true,
                'created' => '2018-07-26 10:47:22',
                'modified' => '2018-07-26 10:47:22',
                'allow_all_transferring_agencies' => true,
                'allow_all_originating_agencies' => true,
                'allow_all_service_levels' => true,
                'allow_all_profiles' => true,
                'auto_validate' => true,
                'proper_chain_id' => 1,
                'improper_chain_id' => 1,
                'date_begin' => '2018-07-26',
                'date_end' => '2118-07-26',
                'default_agreement' => true,
                'allowed_formats' => '["mime-application/vnd.oasis.opendocument.text","pronom-fmt/18","ext-rng"]',
                'max_transfers' => 100,
                'transfer_period' => 'P1M',
                'max_size_per_transfer' => 1073741824,
            ],
            [// 2
                'org_entity_id' => 2,
                'identifier' => 'agr-test02',
                'name' => 'Accord de versement de test 02',
                'description' => '',
                'active' => false,
                'created' => '2018-07-26 10:47:22',
                'modified' => '2018-07-26 10:47:22',
                'allow_all_transferring_agencies' => false,
                'allow_all_originating_agencies' => false,
                'allow_all_service_levels' => false,
                'allow_all_profiles' => false,
                'auto_validate' => true,
                'proper_chain_id' => null,
                'improper_chain_id' => 1,
                'date_begin' => null,
                'date_end' => null,
                'default_agreement' => false,
                'allowed_formats' => '',
                'max_transfers' => null,
                'transfer_period' => null,
                'max_size_per_transfer' => null,
            ],
        ];
    }
}
