<?php
namespace AsalaeCore\Test\Fixture;

use Cake\Auth\DefaultPasswordHasher;
use Cake\TestSuite\Fixture\TestFixture;

/**
 * UsersFixture
 */
class UsersFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $hasher = new DefaultPasswordHasher;
        $this->records = [
            [
                'username' => 'testunit',
                'created' => '2018-04-25 09:34:17.000000',
                'modified' => '2018-04-25 09:34:17.000000',
                'password' => $hasher->hash('testunit'),
                'cookies' => null,
                'menu' => null,
                'high_constrast' => false,
                'active' => true,
                'role_id' => 1,
                'org_entity_id' => 2,
                'email' => 'test@test.fr',
                'name' => 'test test',
                'ldap_id' => null,
                'agent_type' => 'person',
            ],
            [
                'username' => 'ldap_user',
                'created' => '2018-04-25 09:34:17.000000',
                'modified' => '2018-04-25 09:34:17.000000',
                'password' => null,
                'cookies' => null,
                'menu' => null,
                'high_constrast' => false,
                'active' => true,
                'role_id' => 1,
                'org_entity_id' => 2,
                'email' => 'test@test.fr',
                'name' => 'test test',
                'ldap_id' => 1,
                'agent_type' => 'person',
            ],
            [
                'username' => 'archiviste',
                'created' => '2018-04-25 09:34:17.000000',
                'modified' => '2018-04-25 09:34:17.000000',
                'password' => null,
                'cookies' => null,
                'menu' => null,
                'high_constrast' => false,
                'active' => true,
                'role_id' => 3,
                'org_entity_id' => 2,
                'email' => 'admin@test.fr',
                'name' => 'admin test',
                'ldap_id' => 1,
                'agent_type' => 'person',
            ],
        ];
        parent::init();
    }
}
