<?php
namespace AsalaeCore\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * FiltersFixture
 *
 */
class AppMetaFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'app_meta' => '{"test":"foo"}'
            ]
        ];
        parent::init();
    }
}
