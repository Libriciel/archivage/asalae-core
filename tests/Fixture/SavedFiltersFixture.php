<?php
namespace AsalaeCore\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * SavedFiltersFixture
 */
class SavedFiltersFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'user_id' => 1,
                'name' => 'filter-name',
                'controller' => 'ctrl',
                'action' => 'act',
                'created' => 1567170083
            ],
        ];
        parent::init();
    }
}
