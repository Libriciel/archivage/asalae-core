<?php
namespace AsalaeCore\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * FileExtensionsPronomsFixture
 */
class FileExtensionsPronomsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'file_extension_id' => 1,
                'pronom_id' => 1
            ],
        ];
        parent::init();
    }
}
