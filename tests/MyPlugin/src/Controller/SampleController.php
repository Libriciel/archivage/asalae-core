<?php

namespace MyPlugin\Controller;

use AsalaeCore\Controller\ApiInterface;
use AsalaeCore\Controller\ApiTrait;
use Cake\Controller\Controller;

class SampleController extends Controller implements ApiInterface
{
    use ApiTrait;

    public static function getApiActions(): array
    {
        return [
            'default' => [],
        ];
    }

    public function test()
    {
    }
    public function add()
    {
    }
}
