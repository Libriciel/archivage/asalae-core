<?php

namespace MyPlugin\Controller;

use AsalaeCore\Controller\ApiInterface;
use AsalaeCore\Controller\ApiTrait;
use AsalaeCore\Controller\FiltersControllerTrait;
use AsalaeCore\Controller\RenderDataTrait;
use Cake\Controller\Controller;
use Cake\Event\EventInterface;
use Cake\Http\Response;

class WebservicesController extends Controller implements ApiInterface
{
    use ApiTrait, FiltersControllerTrait, RenderDataTrait;

    public function beforeFilter(EventInterface $event)
    {
        $this->loadComponent('Rest');
        $this->setApiActions(self::getApiActions());
        return parent::beforeFilter($event);
    }

    public static function getApiActions(): array
    {
        return [
            'default' => [],
            'test' => [
                'get' => [
                    'function' => 'test'
                ],
                'post' => [
                    'function' => 'testrender'
                ]
            ],
            'forbidden' => [
                '*' => ['function' => 'forbidden']
            ],
        ];
    }

    protected function test()
    {
        return $this->getResponse()->withStringBody('ok');
    }

    protected function testrender()
    {
    }

    public function render(?string $template = null, ?string $layout = null): Response
    {
        return $this->getResponse()->withStringBody('test');
    }
}
