<?php
/**
 * Test runner bootstrap.
 *
 * Add additional configuration/setup your application needs when running
 * unit tests in this file.
 */

use Cake\Core\Configure;
use Migrations\TestSuite\Migrator;

require dirname(__DIR__) . '/vendor/autoload.php';

require dirname(__DIR__) . '/config/bootstrap.php';

$_SERVER['PHP_SELF'] = '/';

if (getenv('TEST_TOKEN')) {
    putenv('NO_COLOR=true'); // désactive les couleurs de debug pour un bon affichage avec paratest
}
$tokenSuffix = getenv('TEST_TOKEN') ? '_'.getenv('TEST_TOKEN') : '';
if (!defined('TMP_TESTDIR')) {
    define('TMP_TESTDIR', sys_get_temp_dir().DS.'testunit'.$tokenSuffix);
}
if (!defined('TMP_VOL1')) {
    define('TMP_VOL1', sys_get_temp_dir().DS.'volume01'.$tokenSuffix);
}
if (!defined('TMP_VOL2')) {
    define('TMP_VOL2', sys_get_temp_dir().DS.'volume02'.$tokenSuffix);
}
if (!is_dir(TMP_TESTDIR)) {
    mkdir(TMP_TESTDIR, 0777, true);
}
define('BROWSER_SESSION', 'phpunit');

Configure::write('App.paths.plugins', [TESTS]);
Configure::write('App.paths.templates', [ROOT . '/templates/', TESTS . 'templates/']);

// create schema
$migrator = new Migrator();
$migrator->run();
