<?php

namespace AsalaeCore\Test\TestCase\Model\Table;

use AsalaeCore\Model\Entity\ArchivingSystem;
use AsalaeCore\DataType\EncryptedString;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\View\Helper\FormHelper;
use Cake\ORM\TableRegistry;
use Cake\View\View;

class ArchivingSystemsTableTest extends TestCase
{
    public $fixtures = [
        'app.ArchivingSystems',
        'app.OrgEntities',
    ];

    public function testValidation()
    {
        $ArchivingSystems = TableRegistry::getTableLocator()->get('ArchivingSystems');
        /** @var ArchivingSystem $as */
        $as = $ArchivingSystems->get(1);

        $formHelper = new FormHelper(new View);
        $formHelper->create($as);
        $this->assertStringContainsString(
            'value="testunit"',
            $formHelper->control('password')
        );
        $formHelper->end();

        $as->set('password', 'foobar');
        $ArchivingSystems->saveOrFail($as);
        $formHelper->create($as);
        $this->assertStringContainsString(
            'value="foobar"',
            $formHelper->control('password')
        );
        $this->assertCount(
            0,
            $ArchivingSystems->find()->where(['password' => 'foobar'])
        );
        $ar = $ArchivingSystems->find()->where(['id' => 1])->disableHydration()->firstOrFail();
        $this->assertNotEmpty($ar['password']);
        $this->assertEquals('foobar', (new EncryptedString(stream_get_contents($ar['password'])))->toFormData());
    }
}
