<?php

namespace AsalaeCore\Test\TestCase\Model\Table;

use AsalaeCore\Model\Table\EaccpfsTable;
use AsalaeCore\TestSuite\InvokePrivateTrait;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\Utility\DOMUtility;
use AsalaeCore\Utility\XmlUtility;
use Cake\Core\Configure;
use Cake\I18n\FrozenTime as Time;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Craur;
use Exception;

/**
 * AsalaeCore\Model\Table\EaccpfsTable Test Case
 */
class EaccpfsTableTest extends TestCase
{
    use InvokePrivateTrait;

    const EACCPF = TEST_DATA . DS . 'sample-eaccpf.xml';

    /**
     * Test subject
     *
     * @var EaccpfsTable
     */
    public $Eaccpfs;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Eaccpfs',
        'app.OrgEntities'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Eaccpfs') ? [] : ['className' => EaccpfsTable::class];
        $this->Eaccpfs = TableRegistry::getTableLocator()->get('Eaccpfs', $config);
        Configure::write('App.defaultLocale', 'fr_FR');
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Eaccpfs);
        EaccpfsTable::$domDocumentClassname = \DOMDocument::class;
        XmlUtility::$domClass = \DOMDocument::class;

        parent::tearDown();
    }

    /**
     * Test newEntityFromXml method
     *
     * @return void
     * @throws Exception
     */
    public function testNewEntityFromXml()
    {
        $eac = $this->Eaccpfs->newEntityFromXml('<Foo><Bar>baz</Bar></Foo>');
        $this->assertNotEmpty($eac->getErrors());

        $eac = $this->Eaccpfs->newEntityFromXml(self::EACCPF);
        $this->assertEmpty($eac->getErrors());

        // on vide existDates, on y place un dateSet avec dateRange
        $util = DOMUtility::load(self::EACCPF);
        $existDates = $util->xpath->query('//ns:existDates')->item(0);
        $date = $existDates->getElementsByTagName('date')->item(0);
        $dateSet = $util->dom->createElement('dateSet');
        $dateRange = $util->dom->createElement('dateRange');
        $fromDate = $util->dom->createElement('fromDate', $from = '2000-01-01');
        $toDate = $util->dom->createElement('toDate', $to = '2010-01-01');

        $dateRange->appendChild($fromDate);
        $dateRange->appendChild($toDate);
        $dateSet->appendChild($dateRange);
        $dateSet->appendChild($date);
        $existDates->appendChild($dateSet);
        $eac = $this->Eaccpfs->newEntityFromXml($util->dom->saveXML());
        $this->assertEmpty($eac->getErrors());
        $this->assertInstanceOf(Time::class, $eac->get('from_date'));
        $this->assertInstanceOf(Time::class, $eac->get('to_date'));
        $this->assertEquals($from, $eac->get('from_date')->format('Y-m-d'));
        $this->assertEquals($to, $eac->get('to_date')->format('Y-m-d'));
    }

    public function testElementToDate()
    {
        $dates = [
            '2000',
            '2001-02',
            '200202',
            '20030203',
            '2004-02-03',
        ];
        $dom = new \DOMDocument;
        $element = $dom->createElement('Test');

        foreach ($dates as $i => $date) {
            $element->setAttribute('standardDate', $date);
            $obj = $this->invokeMethod($this->Eaccpfs, 'elementToDate', [$element]);
            $this->assertInstanceOf(\DateTime::class, $obj);
            $this->assertEquals(2000 + $i, $obj->format('Y'));
        }

        $element = $dom->createElement('Test');
        $dates = [
            'text 2000-01-01 text',
            'text 2001-2-1 text',
            'text 2002/03/01 text',
            'text 2003/4/1 text',
            'text 01/05/2004 text',
            'text 1/6/2005 text',
        ];
        foreach ($dates as $i => $date) {
            DOMUtility::setValue($element, $date);
            $obj = $this->invokeMethod($this->Eaccpfs, 'elementToDate', [$element]);
            $this->assertInstanceOf(\DateTime::class, $obj);
            $this->assertEquals(2000 + $i, $obj->format('Y'));
            $this->assertEquals($i + 1, $obj->format('m'));
        }
        DOMUtility::setValue($element, 'text 2006 text');
        $obj = $this->invokeMethod($this->Eaccpfs, 'elementToDate', [$element]);
        $this->assertInstanceOf(\DateTime::class, $obj);
        $this->assertEquals(2006, $obj->format('Y'));

        DOMUtility::setValue($element, 'text no-date text');
        $obj = $this->invokeMethod($this->Eaccpfs, 'elementToDate', [$element]);
        $this->assertNull($obj);
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @throws Exception
     */
    public function testValidationDefault()
    {
        $eac = $this->Eaccpfs->newEntity(['record_id' => 'test']);
        $this->assertEmpty($eac->getErrors());

        // identifier != record_id
        $data = [
            'org_entity_id' => 1,
            'record_id' => 'test',
        ];
        $this->Eaccpfs->patchEntity($eac, $data);
        $this->assertNotEmpty($eac->getErrors());

        // identifier == record_id
        $data = [
            'org_entity_id' => 1,
            'record_id' => 'se',
        ];
        $this->Eaccpfs->patchEntity($eac, $data);
        $this->assertEmpty($eac->getErrors());

        // invalidé par schema
        $eac = $this->Eaccpfs->newEntity(['data' => '{"Foo":{"Bar":"baz"}}']);
        $this->assertNotEmpty($eac->getErrors());

        $json = Craur::createFromXml(file_get_contents(self::EACCPF))->toJsonString();
        $eac = $this->Eaccpfs->newEntity(['data' => $json]);
        $this->assertEmpty($eac->getErrors());
    }

    /**
     * Test initializeData method
     *
     * @return void
     */
    public function testInitializeData()
    {
        $eac = $this->Eaccpfs->newEntity(
            [
                'record_id' => 'se',
                'entity_id' => '',
                'entity_type' => 'corporateBody',
                'name' => 'test',
                'agency_name' => 'libriciel',
                'org_entity_id' => 1,
                'from_date' => new \DateTime('2000-01-01'),
            ]
        );
        $this->assertEmpty($eac->getErrors());
        $this->Eaccpfs->initializeData($eac, ['name' => 'foo']);
        $this->assertTrue(XmlUtility::validateJson($eac->get('data'), EAC_CPF_XSD));

        $eac = $this->Eaccpfs->newEntity(
            [
                'record_id' => 'se',
                'entity_id' => '',
                'entity_type' => 'corporateBody',
                'name' => 'test',
                'agency_name' => 'libriciel',
                'org_entity_id' => 1,
                'from_date' => new \DateTime('2000-01-01'),
                'to_date' => new \DateTime('2010-01-01'),
            ]
        );
        $this->assertEmpty($eac->getErrors());
        $this->Eaccpfs->initializeData($eac, ['name' => 'foo']);
        $this->assertTrue(XmlUtility::validateJson($eac->get('data'), EAC_CPF_XSD));
    }

    /**
     * Test extractXsdValidationErrors method
     *
     * @return void
     */
    public function testExtractXsdValidationErrors()
    {
        $errors = $this->Eaccpfs->extractXsdValidationErrors('{"Foo":{"Bar":"baz"}}');
        $this->assertNotEmpty($errors);

        $json = Craur::createFromXml(file_get_contents(self::EACCPF))->toJsonString();
        $errors = $this->Eaccpfs->extractXsdValidationErrors($json);
        $this->assertEmpty($errors);
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->Eaccpfs->deleteAll([]);
        $eac = new Entity(
            [
                'record_id' => 'se', // === org_entity.identifier
                'entity_id' => '',
                'entity_type' => 'corporateBody',
                'name' => 'test',
                'agency_name' => 'libriciel',
                'org_entity_id' => 1,
                'from_date' => new \DateTime('2000-01-01'),
                'data' => '',
            ]
        );
        $this->assertNotFalse($this->Eaccpfs->save($eac));

        $this->Eaccpfs->deleteAll([]);
        $eac = new Entity(
            [
                'record_id' => 'not se', // !== org_entity.identifier
                'entity_id' => '',
                'entity_type' => 'corporateBody',
                'name' => 'test',
                'agency_name' => 'libriciel',
                'org_entity_id' => 1,
                'from_date' => new \DateTime('2000-01-01'),
                'data' => '',
            ]
        );
        $this->assertEmpty($eac->getErrors());
        $this->assertFalse($this->Eaccpfs->save($eac));
    }
}
