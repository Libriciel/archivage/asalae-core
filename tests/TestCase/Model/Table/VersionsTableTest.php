<?php

namespace AsalaeCore\Test\TestCase\Model\Table;

use AsalaeCore\Model\Table\VersionsTable;
use AsalaeCore\TestSuite\TestCase;
use Cake\ORM\TableRegistry;

class VersionsTableTest extends TestCase
{
    public $fixtures = [
        'app.Versions',
    ];

    public function testValidation()
    {
        $table = TableRegistry::getTableLocator()->get('Versions');
        $entity = $table->newEntity(
            [
                'subject' => uniqid('foo'),
                'version' => 1,
            ]
        );
        $this->assertNotFalse($table->save($entity));
    }

    public function testGetLast()
    {
        /** @var VersionsTable $versions */
        $versions = TableRegistry::getTableLocator()->get('Versions');
        $this->assertEquals('testunit', $versions->getLast('asalae'));
    }
}
