<?php

namespace AsalaeCore\Test\TestCase\Model\Table;

use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\Utility\Config;
use Asalaecore\Model\Table\FileuploadsTable;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Libriciel\Filesystem\Utility\Filesystem;

class FilesuploadsTableTest extends TestCase
{
    public $fixtures = [
        'app.Fileuploads',
        'app.Mediainfos',
        'app.Siegfrieds',
    ];

    public function setUp(): void
    {
        parent::setUp();
        Config::reset();
        Filesystem::reset();
        Filesystem::begin('testunit');
        Filesystem::mkdir($dir = TMP_TESTDIR);
        if (is_dir($dir)) {
            Filesystem::remove($dir);
        }
        mkdir($dir);
    }

    public function tearDown(): void
    {
        parent::tearDown();
        if (is_dir(TMP_TESTDIR)) {
            Filesystem::remove(TMP_TESTDIR);
        }
        Filesystem::setNamespace();
        Filesystem::reset();
    }

    public function testValidationProfileXsd()
    {
        Filesystem::copy(
            TEST_DATA . 'sample_seda02.xsd',
            $file = TMP_TESTDIR . DS . 'sample_seda02.xsd'
        );
        /** @var FileuploadsTable $Fileuploads */
        $Fileuploads = TableRegistry::getTableLocator()->get('Fileuploads');
        $entity = $Fileuploads->newUploadedEntity('foo.xsd', $file);
        $errors = $Fileuploads->validationProfile(new Validator)->validate($entity->toArray());
        $this->assertEmpty($errors);
    }

    public function testValidationProfileRng()
    {
        Filesystem::copy(TEST_DATA . 'profil.rng', $file = sys_get_temp_dir().DS.'testunit'.DS.'profil.rng');
        /** @var FileuploadsTable $Fileuploads */
        $Fileuploads = TableRegistry::getTableLocator()->get('Fileuploads');
        $entity = $Fileuploads->newUploadedEntity('foo.rng', $file);
        $errors = $Fileuploads->validationProfile(new Validator)->validate($entity->toArray());
        $this->assertEmpty($errors);
    }

    public function testValidationProfileRngInvalid()
    {
        Filesystem::copy(TEST_DATA . 'profil_ko.rng', $file = sys_get_temp_dir().DS.'testunit'.DS.'profil_ko.rng');
        /** @var FileuploadsTable $Fileuploads */
        $Fileuploads = TableRegistry::getTableLocator()->get('Fileuploads');
        $entity = $Fileuploads->newUploadedEntity('foo', $file);
        $errors = $Fileuploads->validationProfile(new Validator)->validate($entity->toArray());

        $this->assertArrayHasKey('path', $errors);
    }

    public function testValidationImportKeywords()
    {
        Filesystem::copy(TEST_DATA . 'continents-skos.rdf', $file = sys_get_temp_dir().DS.'testunit'.DS.'test.rdf');
        /** @var FileuploadsTable $Fileuploads */
        $Fileuploads = TableRegistry::getTableLocator()->get('Fileuploads');
        $entity = $Fileuploads->newUploadedEntity(basename($file), $file);
        $errors = $Fileuploads->validationImportKeywords(new Validator)->validate($entity->toArray());

        $this->assertEmpty($errors);
    }

    public function testValidationImportKeywordsEmpty()
    {
        Filesystem::copy(TEST_DATA . 'empty.rdf', $file = sys_get_temp_dir().DS.'testunit'.DS.'empty.rdf');
        /** @var FileuploadsTable $Fileuploads */
        $Fileuploads = TableRegistry::getTableLocator()->get('Fileuploads');
        $entity = $Fileuploads->newUploadedEntity('foo.rdf', $file);
        $errors = $Fileuploads->validationImportKeywords(new Validator)->validate($entity->toArray());

        $this->assertArrayHasKey('path', $errors);
    }
}
