<?php

namespace AsalaeCore\Test\TestCase\Model\Table;

use AsalaeCore\TestSuite\TestCase;
use Cake\ORM\TableRegistry;

class FileExtensionsPronomsTableTest extends TestCase
{
    public $fixtures = [
        'app.FileExtensionsPronoms',
    ];

    public function testValidation()
    {
        $table = TableRegistry::getTableLocator()->get('FileExtensionsPronoms');
        $entity = $table->newEntity([], ['validate' => false]);
        $this->assertEmpty($entity->getError('id'));
    }
}
