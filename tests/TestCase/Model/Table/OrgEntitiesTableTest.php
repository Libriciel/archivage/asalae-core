<?php
namespace AsalaeCore\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use AsalaeCore\Model\Table\OrgEntitiesTable;
use AsalaeCore\TestSuite\TestCase;

/**
 * AsalaeCore\Model\Table\OrgEntitiesTable Test Case
 */
class OrgEntitiesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var OrgEntitiesTable
     */
    public $OrgEntities;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.OrgEntities',
        'app.TypeEntities',
        'app.Users',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->OrgEntities = TableRegistry::getTableLocator()->get('OrgEntities');
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->OrgEntities);
        parent::tearDown();
    }

    public function testQueryParentGroupSelect()
    {
        $sa = $this->OrgEntities->get(2);

        $entities = $this->OrgEntities->find()
            ->select(
                [
                    'id',
                    'name',
                    'parents' => $this->OrgEntities->queryParentGroupSelect()
                ]
            )
            ->where(
                [
                    'OrgEntities.lft >=' => $sa->get('lft'),
                    'OrgEntities.rght <=' => $sa->get('rght'),
                ]
            )
            ->disableHydration()
            ->all()
            ->toArray();

        $sa = [
            'id' => 2,
            'name' => "Mon Service d'archive",
            'parents' => null,
        ];

        $dir = [
            'id' => 4,
            'name' => 'Mon service versant',
            'parents' => null,
        ];

        $sv = [
            'id' => 5,
            'name' => 'Mon service producteur',
            'parents' => 'Mon service versant'
        ];

        $this->assertContains($sa, $entities);
        $this->assertContains($dir, $entities);
        $this->assertContains($sv, $entities);
    }
}
