<?php
namespace AsalaeCore\Test\TestCase\Model\Table;

use AsalaeCore\Model\Table\PronomsTable;
use Cake\ORM\TableRegistry;
use AsalaeCore\TestSuite\TestCase;

/**
 * AsalaeCore\Model\Table\PronomsTable Test Case
 */
class PronomsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var PronomsTable
     */
    public $Pronoms;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Pronoms',
        'app.FileExtensions',
        'app.FileExtensionsPronoms'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Pronoms') ? [] : ['className' => PronomsTable::class];
        $this->Pronoms = TableRegistry::getTableLocator()->get('Pronoms', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Pronoms);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $pronom = $this->Pronoms->newEntity(['name' => 'test', 'puid' => 'fmt/9999']);
        $this->assertEmpty($pronom->getErrors());

        $this->assertNotFalse($this->Pronoms->save($pronom));
        $pronom = $this->Pronoms->newEntity(['name' => 'test', 'puid' => 'fmt/9999']);
        $this->assertNotEmpty($pronom->getErrors());
    }
}
