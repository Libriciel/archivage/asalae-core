<?php

namespace AsalaeCore\Test\TestCase\Model\Table;

use AsalaeCore\Model\Table\AgreementsTable;
use AsalaeCore\TestSuite\TestCase;
use Cake\ORM\TableRegistry;

class AgreementsTableTest extends TestCase
{
    /**
     * @var AgreementsTable
     */
    public $Agreements;

    public $fixtures = [
        'app.Agreements',
    ];

    public function setUp(): void
    {
        parent::setUp();
        $this->Agreements = TableRegistry::getTableLocator()->get('Agreements');
    }

    public function testDefaultAgreements()
    {
        $this->Agreements->saveOrFail(
            $newEntity = $this->Agreements->newEntity(
                [
                    'org_entity_id' => 2,
                    'identifier' => 'new-agr',
                    'name' => 'Nouvel accord par défaut',
                    'description' => 'par défaut',
                    'active' => false,
                    'default_agreement' => true,
                    'improper_chain_id' => 1,
                    'allowed_formats' => '',
                ]
            )
        );

        $otherAgreements = $this->Agreements->find()
            ->where(
                [
                    'id IS NOT' => $newEntity->get('id'),
                    'default_agreement' => true,
                ]
            )
            ->all()
            ->toArray();

        $this->assertEmpty($otherAgreements);
    }
}
