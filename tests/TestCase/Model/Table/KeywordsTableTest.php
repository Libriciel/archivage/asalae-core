<?php

namespace AsalaeCore\Test\TestCase\Model\Table;

use AsalaeCore\Model\Table\KeywordsTable;
use AsalaeCore\TestSuite\TestCase;
use Cake\I18n\I18n;
use Cake\ORM\TableRegistry;

class KeywordsTableTest extends TestCase
{
    public $fixtures = [
        'app.KeywordLists',
        'app.Keywords',
    ];

    const RDF = TEST_DATA . DS . 'continents-skos.rdf';

    public function testImportSkos()
    {
        /** @var KeywordsTable $Keywords */
        $Keywords = TableRegistry::getTableLocator()->get('Keywords');

        I18n::setLocale('fr_FR');
        $entities = $Keywords->importSkos(file_get_contents(self::RDF), 1, 1);
        $Keywords->saveMany($entities);
        $entity = $Keywords->find()->where(['name' => 'Amérique'])->first();
        $this->assertTrue((bool)$entity);

        $Keywords->deleteAll([]);

        I18n::setLocale('el');
        $entities = $Keywords->importSkos(file_get_contents(self::RDF), 1, 1);
        $Keywords->saveMany($entities);
        $entity = $Keywords->find()->where(['name' => 'Αμερική'])->first();
        $this->assertTrue((bool)$entity);

        $Keywords->deleteAll([]);

        I18n::setLocale('foo');
        $entities = $Keywords->importSkos(file_get_contents(self::RDF), 1, 1);
        $Keywords->saveMany($entities);
        $entity = $Keywords->find()->where(['name' => 'America'])->first();
        $this->assertTrue((bool)$entity);
    }

    public function testImportCsv()
    {
        /** @var KeywordsTable $Keywords */
        $Keywords = TableRegistry::getTableLocator()->get('Keywords');
        // Note: params par défaut
        $params = [
            'separator' => ',',
            'delimiter' => '"',
            'way' => 'row',
            'pos_label_col' => 1,
            'pos_label_row' => 1,
            'auto_code' => false,
            'pos_code_col' => 2,
            'pos_code_row' => 1,
        ];

        $csv = <<<EOT
Nom,code
Foo,bar
Dieu,deus
EOT;
        $file = tmpfile();
        fwrite($file, $csv);
        $entities = $Keywords->importCsv($file, 1, 1, $params);
        fclose($file);
        $Keywords->saveMany($entities);
        $entity = $Keywords->find()->where(['name' => 'Nom'])->first();
        $this->assertTrue((bool)$entity);
        $this->assertEquals('code', $entity->get('code'));
        $entity = $Keywords->find()->where(['name' => 'Dieu'])->first();
        $this->assertTrue((bool)$entity);
        $this->assertEquals('deus', $entity->get('code'));

        $Keywords->deleteAll([]);

        $params['pos_label_row'] = 2;
        $params['pos_code_row'] = 2;
        $file = tmpfile();
        fwrite($file, $csv);
        $entities = $Keywords->importCsv($file, 1, 1, $params);
        fclose($file);
        $Keywords->saveMany($entities);
        $entity = $Keywords->find()->where(['name' => 'Nom'])->first();
        $this->assertFalse((bool)$entity);
        $entity = $Keywords->find()->where(['name' => 'Dieu'])->first();
        $this->assertTrue((bool)$entity);
        $this->assertEquals('deus', $entity->get('code'));

        $Keywords->deleteAll([]);

        $params['auto_code'] = true;
        $file = tmpfile();
        fwrite($file, $csv);
        $entities = $Keywords->importCsv($file, 1, 1, $params);
        fclose($file);
        $Keywords->saveMany($entities);
        $entity = $Keywords->find()->where(['name' => 'Dieu'])->first();
        $this->assertTrue((bool)$entity);
        $this->assertEquals('dieu', $entity->get('code'));

        $csv = <<<EOT
Nom;'Un label';'Un autre ''label'''
code;bar;deus
EOT;
        $params = [
            'separator' => ';',
            'delimiter' => "'",
            'way' => 'col',
            'pos_label_col' => 1,
            'pos_label_row' => 1,
            'auto_code' => false,
            'pos_code_col' => 1,
            'pos_code_row' => 2,
        ];
        $file = tmpfile();
        fwrite($file, $csv);
        $entities = $Keywords->importCsv($file, 1, 1, $params);
        fclose($file);
        $Keywords->saveMany($entities);
        $entity = $Keywords->find()->where(['name' => 'Nom'])->first();
        $this->assertTrue((bool)$entity);
        $this->assertEquals('code', $entity->get('code'));
        $entity = $Keywords->find()->where(['name' => 'Un label'])->first();
        $this->assertTrue((bool)$entity);
        $this->assertEquals('bar', $entity->get('code'));
        $entity = $Keywords->find()->where(['name' => "Un autre 'label'"])->first();
        $this->assertTrue((bool)$entity);
        $this->assertEquals('deus', $entity->get('code'));

        $Keywords->deleteAll([]);

        $params['pos_label_col'] = 2;
        $params['pos_code_col'] = 2;
        $file = tmpfile();
        fwrite($file, $csv);
        $entities = $Keywords->importCsv($file, 1, 1, $params);
        fclose($file);
        $Keywords->saveMany($entities);
        $entity = $Keywords->find()->where(['name' => 'Nom'])->first();
        $this->assertFalse((bool)$entity);
        $entity = $Keywords->find()->where(['name' => 'Un label'])->first();
        $this->assertTrue((bool)$entity);
        $this->assertEquals('bar', $entity->get('code'));
    }
}
