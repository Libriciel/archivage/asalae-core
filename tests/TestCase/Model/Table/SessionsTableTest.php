<?php

namespace AsalaeCore\Test\TestCase\Model\Table;

use AsalaeCore\TestSuite\TestCase;
use Cake\ORM\TableRegistry;

class SessionsTableTest extends TestCase
{
    public $fixtures = [
        'app.Sessions',
    ];

    public function testValidation()
    {
        $table = TableRegistry::getTableLocator()->get('Sessions');
        $data = 'Session|'.serialize(['token' => 'foo'])
            .'Auth|'.serialize(['id' => 1]);
        $entity = $table->newEntity(
            [
                'id' => 'testunit',
                'data' => $data,
                'expires' => time() + 3600
            ]
        );
        $this->assertNotFalse($table->save($entity));
        $this->assertEquals('foo', $entity->get('token'));
        $this->assertEquals(1, $entity->get('user_id'));
    }
}
