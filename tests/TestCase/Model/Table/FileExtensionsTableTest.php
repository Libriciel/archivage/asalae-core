<?php

namespace AsalaeCore\Test\TestCase\Model\Table;

use AsalaeCore\TestSuite\TestCase;
use Cake\ORM\TableRegistry;

class FileExtensionsTableTest extends TestCase
{
    public $fixtures = [
        'app.FileExtensions',
    ];

    public function testValidation()
    {
        $table = TableRegistry::getTableLocator()->get('FileExtensions');
        $entity = $table->newEntity([], ['validate' => false]);
        $this->assertEmpty($entity->getError('id'));
    }
}
