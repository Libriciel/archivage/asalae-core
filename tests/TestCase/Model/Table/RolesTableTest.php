<?php
namespace AsalaeCore\Test\TestCase\Model\Table;

use AsalaeCore\Model\Table\RolesTable;
use AsalaeCore\TestSuite\TestCase;
use Cake\ORM\TableRegistry;

/**
 * AsalaeCore\Model\Table\RolesTable Test Case
 */
class RolesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var RolesTable
     */
    public $Roles;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Aros',
        'app.OrgEntities',
        'app.Roles',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->Roles = TableRegistry::getTableLocator()->get('Roles');
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Roles);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationMemeNomPourDeuxTenants()
    {
        $roles1 = $this->Roles->newEntity(
            [
                'name' => 'foo',
                'active' => true,
                'org_entity_id' => 2,
            ]
        );
        $this->assertEmpty($roles1->getErrors());
        $this->assertNotFalse($this->Roles->save($roles1));

        $roles2 = $this->Roles->newEntity(
            [
                'name' => 'foo',
                'active' => true,
                'org_entity_id' => 2,
            ]
        );
        $this->assertNotEmpty($errors = $roles2->getErrors());
        $this->assertArrayHasKey('name', $errors);
        $this->assertArrayHasKey('unique', $errors['name']);

        $roles3 = $this->Roles->newEntity(
            [
                'name' => 'foo',
                'active' => true,
                'org_entity_id' => 3,
            ]
        );
        $this->assertEmpty($roles3->getErrors());
    }
}
