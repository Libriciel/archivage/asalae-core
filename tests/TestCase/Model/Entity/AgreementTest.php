<?php

namespace AsalaeCore\Test\TestCase\Model\Entity;

use AsalaeCore\Model\Entity\Agreement;
use AsalaeCore\TestSuite\TestCase;

class AgreementTest extends TestCase
{
    public $fixtures = [

    ];

    public function testMaxSizePerTransferConv()
    {
        $agreement = new Agreement(
            [
                'max_size_per_transfer' => 1073741824, // 1024 * 1024 * 1024
            ]
        );
        $this->assertEquals(1073741824, $agreement->get('max_size_per_transfer'));
        $agreement->set('mult_max_size_per_transfer', 1024 * 1024);
        $agreement->set('max_size_per_transfer_conv', 5);
        $this->assertEquals(5242880, $agreement->get('max_size_per_transfer'));
        $agreement->set('mult_max_size_per_transfer', null);
        $agreement->set('max_size_per_transfer_conv', 5);
        $this->assertEquals(5, $agreement->get('max_size_per_transfer'));
        $agreement->set('max_size_per_transfer_conv', null);
        $this->assertNull($agreement->get('max_size_per_transfer'));
    }

    public function testTransferPeriodTrad()
    {
        $agreement = new Agreement;
        foreach (['year', 'month', 'week', 'day'] as $period) {
            $agreement->set('transfer_period', $period);
            $this->assertNotEmpty($trad = $agreement->get('transfer_periodtrad'));
            $this->assertNotEquals($period, $trad);
        }
        $agreement->set('transfer_period', 'unknown');
        $this->assertEquals('unknown', $agreement->get('transfer_periodtrad'));
    }
}
