<?php

namespace AsalaeCore\Test\TestCase\Model\Entity;

use Adldap\Adldap;
use Adldap\Auth\Guard;
use Adldap\Connections\Provider;
use Adldap\Models\Model;
use Adldap\Query\Builder;
use Adldap\Query\Factory as SearchFactory;
use AsalaeCore\Factory\Utility;
use AsalaeCore\Model\Entity\Ldap;
use AsalaeCore\TestSuite\TestCase;

class LdapTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
        $adldap = $this->createMock(Adldap::class);
        $adldap->method('addProvider');
        $provider = $this->createMock(Provider::class);
        $adldap->method('connect')->willReturn($provider);
        $guard = $this->createMock(Guard::class);
        $provider->method('auth')->willReturn($guard);
        $guard->method('attempt')->willReturn(true);
        $factory = $this->createMock(SearchFactory::class);
        $provider->method('search')->willReturn($factory);
        $users = $this->createMock(Builder::class);
        $factory->method('users')->willReturn($users);
        $builder = $this->createMock(Builder::class);
        $builder->method('sortBy')->willReturnSelf();
        $builder->method('rawFilter')->willReturnSelf();
        $builder->method('where')->willReturnSelf();
        $factory->method('newQuery')->willReturn($builder);
        $ldapUser = $this->createMock(Model::class);
        $users->method('findBy')->willReturn($ldapUser);
        $ldapUser->method('getAttribute')->willReturn(['foo'], ['bar@baz.fr']);
        Utility::set(Adldap::class, $adldap);
    }

    public function testGetLdapConfig()
    {
        $ldap = new Ldap(['meta' => ['SIZELIMIT' => 125]]);
        $config = $ldap->getLdapConfig();
        $this->assertArrayHasKey('custom_options', $config);
        $this->assertArrayHasKey(LDAP_OPT_SIZELIMIT, $config['custom_options']);
    }

    public function testAdldap()
    {
        $ldap = new Ldap;
        $this->assertInstanceOf(Adldap::class, $ldap->adldap());
    }

    public function testConnect()
    {
        $ldap = new Ldap;
        $this->assertInstanceOf(Provider::class, $ldap->connect());
    }

    public function testPing()
    {
        $ldap = new Ldap;
        $this->assertTrue($ldap->ping());
    }

    public function testAuth()
    {
        $ldap = new Ldap;
        $this->assertInstanceOf(Guard::class, $ldap->auth());
    }

    public function testSearch()
    {
        $ldap = new Ldap;
        $this->assertInstanceOf(SearchFactory::class, $ldap->search());
    }

    public function testSearchWithFilters()
    {
        $ldap = new Ldap(['user_login_attribute' => 'uid', 'ldap_users_filter' => '(memberOf=libriciel)']);
        $this->assertInstanceOf(Builder::class, $ldap->searchWithFilters());
    }

    public function testUsers()
    {
        $ldap = new Ldap;
        $this->assertInstanceOf(Builder::class, $ldap->users());
    }

    public function testLog()
    {
        $ldap = new Ldap;
        $this->assertTrue($ldap->log('test', 'test'));
    }
}
