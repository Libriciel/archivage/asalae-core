<?php

namespace AsalaeCore\Test\TestCase\Model\Entity;

use AsalaeCore\Model\Entity\Session;
use AsalaeCore\TestSuite\TestCase;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;

class SessionTest extends TestCase
{
    public $fixtures = [
        'app.Configurations',
        'app.OrgEntities',
        'app.Sessions',
        'app.TypeEntities',
        'app.Users',
    ];

    public function testSerialize()
    {
        $session = new Session;

        $handle = tmpfile();
        fwrite($handle, 'Config|a:1:{s:4:"time";i:1568813649;}');
        $session->set('data', $handle, ['setter' => false]);
        $this->assertNotFalse($json = json_encode($session));
        $this->assertEquals(
            ['data' => ['Config' => ['time' => (int) 1568813649]]],
            json_decode($json, true)
        );
        fclose($handle);
    }

    public function testUpdateOrgEntities()
    {
        $Sessions = TableRegistry::getTableLocator()->get('Sessions');
        /** @var Session $session */
        $session = $Sessions->find()->where(['id' => 'f45df7be-ca83-44ef-960c-1856a8893eb6'])->first();
        $this->assertNotFalse((bool)$session);
        $rght = Hash::get($session->toArray(), 'data.Auth.org_entity.archival_agency.rght');
        $session->updateOrgEntities();
        $this->assertNotEquals(
            Hash::get($session->toArray(), 'data.Auth.org_entity.archival_agency.rght'),
            $rght
        );
    }
}
