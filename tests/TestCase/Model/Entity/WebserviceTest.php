<?php

namespace AsalaeCore\Test\TestCase\Model\Entity;

use AsalaeCore\Model\Entity\Webservice;
use AsalaeCore\TestSuite\TestCase;

class WebserviceTest extends TestCase
{
    public function testComparePassword()
    {
        $webservice = new Webservice(['password' => 'foo']);
        $this->assertTrue($webservice->comparePassword('foo'));
        $this->assertFalse($webservice->comparePassword('bar'));
    }
}
