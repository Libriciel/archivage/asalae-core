<?php

namespace AsalaeCore\Test\TestCase\Model\Entity;

use AsalaeCore\Model\Entity\Eaccpf;
use AsalaeCore\TestSuite\TestCase;
use Cake\I18n\FrozenTime as Time;
use Cake\Utility\Hash;

class EaccpfTest extends TestCase
{

    public function testGetXml()
    {
        $json = json_encode(
            [
                'Foo' => [
                    'Bar' => [
                        ['baz' => 123],
                        ['biz' => ['@' => 456, '@test' => 'test']],
                    ]
                ]
            ]
        );
        $eac = new Eaccpf(['data' => $json]);
        $expected = <<<EOT
<?xml version="1.0"?>
<Foo>
  <Bar>
    <baz>123</baz>
  </Bar>
  <Bar>
    <biz test="test">456</biz>
  </Bar>
</Foo>

EOT;
        $this->assertEquals($expected, $eac->get('XML'));
    }

    public function testSetFromDate()
    {
        $data = Hash::insert(
            [],
            'eac-cpf.cpfDescription.description.existDates.date',
            '1950-01-01'
        );
        $json = json_encode($data);
        $eac = new Eaccpf(['data' => $json]);

        $eac->set('from_date', '01/01/2000');
        $this->assertInstanceOf(Time::class, $eac->get('from_date'));
        $this->assertEquals('2000-01-01', $eac->get('from_date')->format('Y-m-d'));
        $data = json_decode($eac->get('data'), true);
        $this->assertEquals('2000-01-01', Hash::get($data, 'eac-cpf.cpfDescription.description.existDates.date'));

        $eac->set('from_date', new Time('2001-01-01'));
        $this->assertInstanceOf(Time::class, $eac->get('from_date'));
        $this->assertEquals('2001-01-01', $eac->get('from_date')->format('Y-m-d'));
        $data = json_decode($eac->get('data'), true);
        $this->assertEquals('2001-01-01', Hash::get($data, 'eac-cpf.cpfDescription.description.existDates.date'));

        $eac->set('to_date', new Time('2020-01-01'), ['setter' => false]);
        $eac->set('from_date', new Time('2002-01-01'));
        $this->assertInstanceOf(Time::class, $eac->get('from_date'));
        $data = json_decode($eac->get('data'), true);
        $this->assertNull(Hash::get($data, 'eac-cpf.cpfDescription.description.existDates.date'));
        $this->assertEquals('2002-01-01', Hash::get($data, 'eac-cpf.cpfDescription.description.existDates.dateRange.fromDate'));
        $this->assertEquals('2020-01-01', Hash::get($data, 'eac-cpf.cpfDescription.description.existDates.dateRange.toDate'));

        $data = Hash::insert(
            [],
            'eac-cpf.cpfDescription.description.existDates.dateRange',
            [
                'fromDate' => ['@' => '1950-01-01', '@standardDate' => '1950-01-01'],
                'toDate' => '2000-01-01',
            ]
        );
        $json = json_encode($data);
        $eac = new Eaccpf(['data' => $json]);
        $eac->set('to_date', new Time('2020-01-01'), ['setter' => false]);
        $eac->set('from_date', new Time('2003-01-01'));
        $data = json_decode($eac->get('data'), true);
        $this->assertEquals('2003-01-01', Hash::get($data, 'eac-cpf.cpfDescription.description.existDates.dateRange.fromDate'));

        $eac->set('from_date', new Time('2003-01-01'));
        $data = json_decode($eac->get('data'), true);
        $this->assertEquals('2003-01-01', Hash::get($data, 'eac-cpf.cpfDescription.description.existDates.dateRange.fromDate'));
    }

    public function testSetToDate()
    {
        $data = Hash::insert(
            [],
            'eac-cpf.cpfDescription.description.existDates.date',
            '1950-01-01'
        );
        $json = json_encode($data);
        $eac = new Eaccpf(['data' => $json]);

        $eac->set('to_date', '01/01/2000');
        $this->assertInstanceOf(Time::class, $eac->get('to_date'));
        $this->assertEquals('2000-01-01', $eac->get('to_date')->format('Y-m-d'));
        $data = json_decode($eac->get('data'), true);
        $this->assertEquals('2000-01-01', Hash::get($data, 'eac-cpf.cpfDescription.description.existDates.date'));

        $eac->set('to_date', new Time('2001-01-01'));
        $this->assertInstanceOf(Time::class, $eac->get('to_date'));
        $this->assertEquals('2001-01-01', $eac->get('to_date')->format('Y-m-d'));
        $data = json_decode($eac->get('data'), true);
        $this->assertEquals('2001-01-01', Hash::get($data, 'eac-cpf.cpfDescription.description.existDates.date'));

        $eac->set('from_date', new Time('2002-01-01'), ['setter' => false]);
        $eac->set('to_date', new Time('2020-01-01'));
        $this->assertInstanceOf(Time::class, $eac->get('to_date'));
        $data = json_decode($eac->get('data'), true);
        $this->assertNull(Hash::get($data, 'eac-cpf.cpfDescription.description.existDates.date'));
        $this->assertEquals('2002-01-01', Hash::get($data, 'eac-cpf.cpfDescription.description.existDates.dateRange.fromDate'));
        $this->assertEquals('2020-01-01', Hash::get($data, 'eac-cpf.cpfDescription.description.existDates.dateRange.toDate'));

        $data = Hash::insert(
            [],
            'eac-cpf.cpfDescription.description.existDates.dateRange',
            [
                'fromDate' => null,
                'toDate' => '2000-01-01',
            ]
        );
        $json = json_encode($data);
        $eac = new Eaccpf(['data' => $json]);
        $eac->set('from_date', new Time('2003-01-01'), ['setter' => false]);
        $eac->set('to_date', new Time('2020-01-01'));
        $data = json_decode($eac->get('data'), true);
        $this->assertEquals('2020-01-01', Hash::get($data, 'eac-cpf.cpfDescription.description.existDates.dateRange.toDate'));
    }

    public function testGetTmpFilename()
    {
        $json = json_encode(
            [
                'Foo' => [
                    'Bar' => [
                        ['baz' => 123],
                        ['biz' => ['@' => 456, '@test' => 'test']],
                    ]
                ]
            ]
        );
        $eac = new Eaccpf(['data' => $json]);
        $filename = $eac->get('tmp_filename');
        $this->assertFileExists($filename);
        unset($eac);
        $this->assertFileDoesNotExist($filename);
    }
}
