<?php

namespace AsalaeCore\Test\TestCase\Model\Entity;

use AsalaeCore\Model\Entity\AuthUrl;
use AsalaeCore\TestSuite\TestCase;
use Cake\Http\ServerRequest;
use DateTime;
use PHPUnit\Framework\MockObject\Rule\AnyInvokedCount;

class AuthUrlTest extends TestCase
{

    public function testAllowed()
    {
        $any = new AnyInvokedCount;
        $request = $this->createMock(ServerRequest::class);
        $request->expects($any)
            ->method('getAttribute')
            ->willReturn('/foo/bar-baz/123');

        $authUrl = new AuthUrl;
        $this->assertFalse($authUrl->allowed($request));

        $authUrl = new AuthUrl(
            [
                'code' => 'test',
                'url' => '/Foo/BarBaz/123',
                'expire' => new DateTime('tomorrow'),
            ]
        );
        $this->assertTrue($authUrl->allowed($request));

        $authUrl->set('expire', new DateTime('yesterday'));
        $this->assertFalse($authUrl->allowed($request));

        $authUrl = new AuthUrl(
            [
                'code' => 'test',
                'url' => '/Foo/BarBaz/456',
                'expire' => new DateTime('tomorrow'),
            ]
        );
        $this->assertFalse($authUrl->allowed($request));
    }
}
