<?php

namespace AsalaeCore\Test\TestCase\Model\Entity;

use AsalaeCore\Model\Entity\CronExecution;
use AsalaeCore\TestSuite\TestCase;
use Cake\Core\Configure;
use Cake\I18n\FrozenDate;

class CronExecutionTest extends TestCase
{
    public function testComparePassword()
    {
        $cronExec = new CronExecution;
        $this->assertEmpty($cronExec->get('header'));

        Configure::write(['App.fullBaseUrl' => 'fooUrl']);
        $cronExec->set(
            [
                'id' => 1,
                'date_begin' => new FrozenDate,
                'state' => 'testing',
            ]
        );
        $this->assertContains('Url: fooUrl', $cronExec->get('header'));
    }
}
