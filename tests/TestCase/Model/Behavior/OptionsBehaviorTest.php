<?php

namespace AsalaeCore\Test\TestCase\Model\Behavior;

use AsalaeCore\Test\Mock\Testmodel;
use AsalaeCore\Test\Mock\TestmodelTable;
use AsalaeCore\TestSuite\TestCase;
use Cake\ORM\TableRegistry;
use Cake\I18n\I18n;
use Cake\Core\Configure;
use Cake\Cache\Cache;

class OptionsBehaviorTest extends TestCase
{
    public $fixtures = [
        'app.Agreements',
    ];

    /**
     * @var TestmodelTable
     */
    public $Testmodel;

    public function setUp(): void
    {
        parent::setUp();
        Cache::disable();
        Configure::write('App.paths.locales', [TESTS . 'Locale' . DS]);
        I18n::clear();
        I18n::setLocale('fr_FR');
        $this->Testmodel = TableRegistry::getTableLocator()->get(
            'TestmodelTable',
            [
                'className' => TestmodelTable::class,
                'entityClass' => Testmodel::class,
                'table' => 'agreements',
            ]
        );
    }

    public function testOptions()
    {
        $expected = [
            'value 1' => __dx('testmodel', 'my_field_name', 'value 1'),
            'value 2' => __dx('testmodel', 'my_field_name', 'value 2'),
            'value 3' => __dx('testmodel', 'my_field_name', 'value 3'),
        ];
        $this->assertEquals($expected, $this->Testmodel->options('my_field_name'));
    }
}
