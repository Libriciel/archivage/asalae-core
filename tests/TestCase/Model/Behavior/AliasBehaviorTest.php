<?php

namespace AsalaeCore\Test\TestCase\Model\Behavior;

use AsalaeCore\Model\Behavior\AliasBehavior;
use AsalaeCore\TestSuite\TestCase;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;

class AliasBehaviorTest extends TestCase
{
    public function testAlias()
    {
        /** @var Table|AliasBehavior $table1 */
        $table1 = TableRegistry::getTableLocator()->get(
            'test1',
            [
                'className' => 'AsalaeCore\Test\Mock\Testmodel2Table'
            ]
        );
        $table2 = $table1->withAlias('test2');

        $this->assertEquals('test1', $table1->getAlias());
        $this->assertEquals('test2', $table2->getAlias());
    }
}
