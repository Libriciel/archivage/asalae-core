<?php

namespace AsalaeCore\Test\TestCase\Database\Type;

use AsalaeCore\Database\Type\DateType;
use AsalaeCore\TestSuite\InvokePrivateTrait;
use AsalaeCore\TestSuite\TestCase;
use Cake\Core\Configure;
use Cake\Database\Type;
use Cake\I18n\FrozenTime as Time;
use DateTimeInterface;

class DateTypeTest extends TestCase
{
    use InvokePrivateTrait;

    public function testParseValue()
    {
        Configure::write('App.defaultLocale', 'fr_FR');

        Type::set('date', new DateType('date'));
        /** @var DateType $type */
        $type = Type::build('date');
        $type->useLocaleParser();

        /** @var Time $result */
        $result = $this->invokeMethod($type, '_parseValue', ['1950-01-01']);
        $this->assertInstanceOf(Time::class, $result);
        $this->assertEquals('1950-01-01T00:00:00+00:00', $result->format(Time::ATOM));

        $result = $this->invokeMethod($type, '_parseValue', [new Time('08/07/2013')]);
        $this->assertInstanceOf(DateTimeInterface::class, $result);
        $this->assertEquals('2013-08-07T00:00:00+00:00', $result->format(Time::ATOM));
    }
}
