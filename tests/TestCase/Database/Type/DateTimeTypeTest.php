<?php

namespace AsalaeCore\Test\TestCase\Database\Type;

use AsalaeCore\Database\Type\DateTimeType;
use AsalaeCore\TestSuite\InvokePrivateTrait;
use Cake\Core\Configure;
use Cake\Database\Type;
use Cake\I18n\FrozenTime as Time;
use AsalaeCore\TestSuite\TestCase;
use DateTimeInterface;

class DateTimeTypeTest extends TestCase
{
    use InvokePrivateTrait;

    public function test_parseValue()
    {
        Configure::write('App.defaultLocale', 'fr_FR');

        Type::set('datetime', new DateTimeType('datetime'));
        $type = Type::build('datetime');
        $type->useLocaleParser();

        /** @var Time $result */
        $result = $this->invokeMethod($type, '_parseValue', ['1950-01-01']);
        $this->assertInstanceOf(Time::class, $result);
        $this->assertStringContainsString('1950-01-01T00:00:00', $result->format(DateTimeInterface::ATOM));

        $result = $this->invokeMethod($type, '_parseValue', ['1950-01-01 11:50']);
        $this->assertInstanceOf(Time::class, $result);
        $this->assertEquals('1950-01-01T11:50:00+00:00', $result->format(DateTimeInterface::ATOM));

        $result = $this->invokeMethod($type, '_parseValue', ['1950-01-01T11:50:22+0200']);
        $this->assertInstanceOf(Time::class, $result);
        $this->assertEquals('1950-01-01T11:50:22+02:00', $result->format(DateTimeInterface::ATOM));

        $result = $this->invokeMethod($type, '_parseValue', [new Time('08/07/2013 09:09')]);
        $this->assertInstanceOf(DateTimeInterface::class, $result);
        $this->assertEquals('2013-08-07T09:09:00+00:00', $result->format(DateTimeInterface::ATOM));
    }
}
