<?php

namespace AsalaeCore\Test\TestCase\Datasource;

use AsalaeCore\Datasource\Paginator;
use Cake\Datasource\Paging\Exception\PageOutOfBoundsException;
use Cake\ORM\TableRegistry;
use AsalaeCore\TestSuite\TestCase;

class PaginatorTest extends TestCase
{
    public $fixtures = [
        'app.Users',
    ];

    public function testPaginate()
    {
        $Users = TableRegistry::getTableLocator()->get('Users');
        $paginator = new Paginator;
        $paginator->setConfig('allowedParameters', ['limit', 'sort', 'page', 'direction', 'sortable']);
        $results = $paginator->paginate($Users);
        $this->assertGreaterThanOrEqual(1, $results->count());

        $paginator->setConfig('order', ['name' => 'asc']);
        $query = $Users->find()->order(['username']);
        $results = $paginator->paginate($query, ['sort' => 'id']);
        $this->assertGreaterThanOrEqual(1, $results->count());

        $results = $paginator->paginate($Users, ['sort' => 'foo', 'sortable' => ['foo' => 'Users.id']]);
        $this->assertGreaterThanOrEqual(1, $results->count());

        $this->expectException(PageOutOfBoundsException::class);
        $paginator->paginate($Users, ['page' => 1000, 'limit' => 1000]);
    }
}
