<?php

namespace AsalaeCore\Test\TestCase\Factory;

use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\Factory;
use Exception;

class UtilityTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
        Factory\Utility::addNamespace("AsalaeCore\Test\Mock");
    }

    public function tearDown(): void
    {
        parent::tearDown();
        Factory\Utility::reset();
    }

    public function testGet()
    {
        $utility = Factory\Utility::get('TestUtility');
        $this->assertInstanceOf('AsalaeCore\Test\Mock\TestUtility', $utility);
        $this->assertTrue($utility->main());

        $utility = Factory\Utility::get('TestPrivateUtility');
        /** @var \AsalaeCore\Test\Mock\TestPrivateUtility $instance */
        $instance = $utility->getInstance();
        $this->assertInstanceOf('AsalaeCore\Factory\UtilityObject', $utility);
        $this->assertTrue($instance->main());

        $utility = Factory\Utility::get('TestStaticUtility');
        $this->assertInstanceOf('AsalaeCore\Factory\UtilityObject', $utility);
        $this->assertTrue($utility->main());
    }

    public function testGetException()
    {
        $this->expectException(Exception::class);
        Factory\Utility::get('NotExist');
    }

    public function testSet()
    {
        $instance = 'AsalaeCore\Test\Mock\TestStaticUtility';
        Factory\Utility::set('TestUtility', $instance);
        $this->assertEquals($instance, Factory\Utility::get('TestUtility')->name());

        $instance = 'TestPrivateUtility';
        $set = Factory\Utility::set('TestUtility', $instance);
        $utility = Factory\Utility::get('TestUtility');

        /** @var \AsalaeCore\Test\Mock\TestPrivateUtility $instance */
        $instance = $utility->getInstance();
        $this->assertEquals('AsalaeCore\Test\Mock\TestPrivateUtility', $instance->name());
        $this->assertEquals($utility->getInstance(), $set->getInstance());
    }
}
