<?php

namespace AsalaeCore\Test\TestCase\DataType;

use AsalaeCore\DataType\CommaArrayString;
use AsalaeCore\TestSuite\TestCase;

class CommaArrayStringTest extends TestCase
{
    public function testAll()
    {
        // cas simple
        $expectedStr = 'foo,bar,baz';
        $expectedArray = [
            'foo',
            'bar',
            'baz',
        ];
        $object = CommaArrayString::createFromString($expectedStr);
        $this->assertInstanceOf(CommaArrayString::class, $object);
        $this->assertEquals($expectedArray, (array)$object);
        $this->assertEquals($expectedStr, (string)$object);

        // cas avec caractères d'échapement et de séparation
        $expectedStr = '"foo,test",bar,"baz,""biz""","""buz"""';
        $expectedArray = [
            'foo,test', // virgule
            'bar', // valeur simple
            'baz,"biz"', // virgule et double quote
            '"buz"', // double quote au début et à la fin
        ];
        $object = CommaArrayString::createFromString($expectedStr);
        $this->assertInstanceOf(CommaArrayString::class, $object);
        $this->assertEquals($expectedArray, (array)$object);
        $this->assertEquals($expectedStr, (string)$object);
    }
}
