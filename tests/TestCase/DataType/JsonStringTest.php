<?php

namespace AsalaeCore\Test\TestCase\DataType;

use AsalaeCore\DataType\JsonString;
use AsalaeCore\TestSuite\TestCase;

class JsonStringTest extends TestCase
{
    public function testAll()
    {
        // cas simple
        $expectedStr = '["foo","bar","baz"]';
        $expectedArray = [
            'foo',
            'bar',
            'baz',
        ];
        $object = JsonString::createFromString($expectedStr);
        $this->assertInstanceOf(JsonString::class, $object);
        $this->assertEquals($expectedArray, (array)$object);
        $this->assertEquals($expectedStr, (string)$object);

        // cas avec caractères d'échapement et de séparation
        $expectedStr = '["foo,test","bar","baz,\"biz\"","\"buz\""]';
        $expectedArray = [
            'foo,test', // virgule
            'bar', // valeur simple
            'baz,"biz"', // virgule et double quote
            '"buz"', // double quote au début et à la fin
        ];
        $object = JsonString::createFromString($expectedStr);
        $this->assertInstanceOf(JsonString::class, $object);
        $this->assertEquals($expectedArray, (array)$object);
        $this->assertEquals($expectedStr, (string)$object);
    }
}
