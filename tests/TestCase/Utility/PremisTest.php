<?php

namespace AsalaeCore\Test\TestCase\Utility;

use AsalaeCore\Utility\Premis;
use AsalaeCore\TestSuite\TestCase;

class PremisTest extends TestCase
{

    public function testGenerate()
    {
        $filename = sys_get_temp_dir() . DS . 'testimport_file.txt';
        file_put_contents($filename, 'test');

        $object1 = new Premis\IntellectualEntity('local', 'Archives:1');
        $object1->originalName = 'Archive testunit';
        $object2 = Premis\File::fromFile($filename, 'local', 'StoredFiles:1');
        unlink($filename);

        $agent = new Premis\Agent('local', 'Users:1');
        $agent->name = 'M Foo BAR';

        $event = new Premis\Event('test');
        $event
            ->addObject($object1)
            ->addObject($object2)
            ->addAgent($agent);

        $premis = new Premis;
        $premis->add($event);
        $premisXml = $premis->generate();
        $this->assertNotEmpty($premisXml);

        $dom = new \DOMDocument;
        $dom->loadXML($premisXml);
        $this->assertTrue($dom->schemaValidate(PREMIS_V3));
    }

    public function testAppendEvent()
    {
        // event seul
        $dom = (new Premis())->generateDocument();
        $event1 = new Premis\Event('test');
        Premis::appendEvent($dom, $event1);
        $this->assertEquals(1, $dom->getElementsByTagName('event')->count());

        // event avec un objet et un agent
        $dom = (new Premis())->generateDocument();
        $dom->formatOutput = true;
        $dom->preserveWhiteSpace = false;
        $object1 = new Premis\IntellectualEntity;
        $agent1 = new Premis\Agent;
        $event2 = new Premis\Event('test');
        $event2->addObject($object1);
        $event2->addAgent($agent1);
        Premis::appendEvent($dom, $event2);
        $this->assertEquals(1, $dom->getElementsByTagName('object')->count());
        $this->assertEquals(1, $dom->getElementsByTagName('event')->count());
        $this->assertEquals(1, $dom->getElementsByTagName('agent')->count());
        $this->assertTrue($dom->schemaValidate(PREMIS_V3));

        // ajout d'un event seul
        $event3 = new Premis\Event('test');
        Premis::appendEvent($dom, $event3);
        $this->assertEquals(1, $dom->getElementsByTagName('object')->count());
        $this->assertEquals(2, $dom->getElementsByTagName('event')->count());
        $this->assertEquals(1, $dom->getElementsByTagName('agent')->count());
        $this->assertTrue($dom->schemaValidate(PREMIS_V3));

        // ajout d'un event avec un objet et un agent identiques au 1er
        $event4 = new Premis\Event('test');
        $event4->addObject($object1);
        $event4->addAgent($agent1);
        Premis::appendEvent($dom, $event4);
        $this->assertEquals(1, $dom->getElementsByTagName('object')->count());
        $this->assertEquals(3, $dom->getElementsByTagName('event')->count());
        $this->assertEquals(1, $dom->getElementsByTagName('agent')->count());
        $this->assertTrue($dom->schemaValidate(PREMIS_V3));

        // ajout d'un event avec un objet et un agent != du 1er
        $event5 = new Premis\Event('test');
        $object2 = new Premis\IntellectualEntity;
        $agent2 = new Premis\Agent;
        $event5->addObject($object2);
        $event5->addAgent($agent2);
        Premis::appendEvent($dom, $event5);
        $this->assertEquals(2, $dom->getElementsByTagName('object')->count());
        $this->assertEquals(4, $dom->getElementsByTagName('event')->count());
        $this->assertEquals(2, $dom->getElementsByTagName('agent')->count());
        $this->assertTrue($dom->schemaValidate(PREMIS_V3));
    }
}
