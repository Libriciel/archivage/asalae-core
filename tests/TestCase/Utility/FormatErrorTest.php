<?php

namespace AsalaeCore\Test\TestCase\Utility;

use AsalaeCore\Utility\FormatError;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use AsalaeCore\TestSuite\TestCase;

class FormatErrorTest extends TestCase
{
    public $fixtures = [
        'app.OrgEntities',
    ];

    public function testEntityErrors()
    {
        $entity = new Entity(['test' => true]);
        $entity->setInvalidField('test', true);
        $entity->setError(
            'test',
            [
                "Lorem ipsum dolor sit",
                "Foo bar baz",
            ]
        );
        $entity->setError('empty', "Valeur vide");
        $output = FormatError::entityErrors($entity);
        $expected = <<<EOT
test: (true)
    - Lorem ipsum dolor sit
    - Foo bar baz
empty: (null)
    - Valeur vide
EOT;
        $this->assertEquals($expected, $output);
    }

    public function testEntity()
    {
        $OrgEntities = TableRegistry::getTableLocator()->get('OrgEntities');
        $entity = $OrgEntities->newEntity(['id' => 123, 'name' => 'foo', 'identifier' => 'bar']);

        $expected = 'OrgEntities:123 - bar';
        $this->assertEquals($expected, FormatError::entity($entity));

        $entity->unset('identifier');
        $expected = 'OrgEntities:123 - foo';
        $this->assertEquals($expected, FormatError::entity($entity));
    }

    public function testLogEntityErrors()
    {
        $OrgEntities = TableRegistry::getTableLocator()->get('OrgEntities');
        $entity = $OrgEntities->newEntity(['id' => 123, 'name' => 'foo', 'identifier' => 'bar', 'test' => true]);
        $entity->setError(
            'test',
            [
                "Lorem ipsum dolor sit",
                "Foo bar baz",
            ]
        );
        $entity->setError('empty', "Valeur vide");
        $str = __("Echec de validation de l'entité: {0}", 'OrgEntities:123 - bar');
        $expected = <<<EOT
$str
test: (null)
    - Lorem ipsum dolor sit
    - Foo bar baz
empty: (null)
    - Valeur vide
EOT;
        ;
        $this->assertEquals($expected, FormatError::logEntityErrors($entity));
    }
}
