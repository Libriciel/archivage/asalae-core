<?php

namespace AsalaeCore\Test\TestCase\Utility;

use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\Utility\ValidatorXml;

class ValidatorXmlTest extends TestCase
{
    const XML = TESTS.'Data'.DS.'sample.xml';
    const XML_KO = TESTS.'Data'.DS.'sample_ko.xml';
    const XSD = TESTS.'Data'.DS.'sample.xsd';
    const RNG = TESTS.'Data'.DS.'sample.rng';

    public function testValidate()
    {
        $config = [
            'relaxng' => [
                'use_domdocument' => true,
                'cmd' => 'java -jar "'.APP.'Data'.DS.'jing.jar"',
            ],
            'schema' => [
                'use_domdocument' => true,
                'cmd' => 'xmllint --noout --schema'
            ]
        ];

        $validator = new ValidatorXml(self::XSD, $config);
        $this->assertTrue($validator->validate(self::XML));

        $validator = new ValidatorXml(self::RNG, $config);
        $this->assertTrue($validator->validate(self::XML));

        $validator = new ValidatorXml(self::XSD, $config);
        $this->assertFalse($validator->validate(self::XML_KO));

        $validator = new ValidatorXml(self::RNG, $config);
        $this->assertFalse($validator->validate(self::XML_KO));

        $config['relaxng']['use_domdocument'] = false;
        $config['schema']['use_domdocument'] = false;

        $validator = new ValidatorXml(self::XSD, $config);
        $this->assertTrue($validator->validate(self::XML));

        $validator = new ValidatorXml(self::RNG, $config);
        $this->assertTrue($validator->validate(self::XML));

        $validator = new ValidatorXml(self::XSD, $config);
        $this->assertFalse($validator->validate(self::XML_KO));

        $validator = new ValidatorXml(self::RNG, $config);
        $this->assertFalse($validator->validate(self::XML_KO));

        $config['relaxng']['cmd'] = 'xmllint --noout --relaxng';

        $validator = new ValidatorXml(self::RNG, $config);
        $this->assertTrue($validator->validate(self::XML));

        $validator = new ValidatorXml(self::RNG, $config);
        $this->assertFalse($validator->validate(self::XML_KO));

        // resource comme paramètres
        $handleXsd = fopen(self::XSD, 'r');
        $handleXml = fopen(self::XML, 'r');
        $handleXmlKo = fopen(self::XML_KO, 'r');
        $validator = new ValidatorXml($handleXsd, $config);
        $this->assertTrue($validator->validate($handleXml));
        fclose($handleXml);
        $this->assertFalse($validator->validate($handleXmlKo));
        fclose($handleXsd);
        fclose($handleXmlKo);
    }
}
