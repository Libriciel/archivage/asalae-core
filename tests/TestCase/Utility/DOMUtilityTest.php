<?php

namespace AsalaeCore\Test\TestCase\Utility;

use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\TestSuite\VolumeSample;
use AsalaeCore\Utility\DOMUtility;
use Cake\Utility\Hash;
use DOMDocument;

class DOMUtilityTest extends TestCase
{
    const SEDA_10_XML = TESTS.'Data'.DS.'sample_seda10.xml';

    public function testGetElementXpath()
    {
        $util = DOMUtility::load(self::SEDA_10_XML);
        $element = $util->dom->documentElement->getElementsByTagName('Name')->item(0);
        $this->assertEquals('/*/*[6]/*[4]', $element->getNodePath());
        $elementXpath = $util->getElementXpath($element, '');
        $this->assertEquals('/ArchiveTransfer/Archive/Name', $elementXpath);

        $elementXpath = $util->getElementXpath($element, 'ns:');
        $this->assertEquals('/ns:ArchiveTransfer/ns:Archive/ns:Name', $elementXpath);

        // tests sur l'index
        $element = $util->dom->documentElement->getElementsByTagName('ArchiveObject')->item(0);
        for ($i = 0; $i < 500; $i++) {
            $util->appendChild($element->parentNode, clone $element);
        }
        $this->assertEquals('/*/*[6]/*[9]', $element->getNodePath());
        $elementXpath = $util->getElementXpath($element, '');
        $this->assertEquals('/ArchiveTransfer/Archive/ArchiveObject[1]', $elementXpath);

        $element = $util->node('/ns:ArchiveTransfer/ns:Archive/ns:ArchiveObject[25]');
        $elementXpath = $util->getElementXpath($element, '');
        $this->assertEquals('/ArchiveTransfer/Archive/ArchiveObject[25]', $elementXpath);

        $element = $util->node('/ns:ArchiveTransfer/ns:Archive/ns:ArchiveObject[501]/ns:Name');
        $elementXpath = $util->getElementXpath($element, '');
        $this->assertEquals('/ArchiveTransfer/Archive/ArchiveObject[501]/Name', $elementXpath);
    }

    public function testFindByContent()
    {
        $util = DOMUtility::loadXML(file_get_contents(self::SEDA_10_XML));

        $results = $util->findByContent("Objet d'archive");
        $expected = [
            '/ArchiveTransfer/Archive/ArchiveObject/Name',
            '/ArchiveTransfer/Archive/ArchiveObject/ArchiveObject/Name',
        ];
        $this->assertEquals($expected, $results);

        $results = $util->findByContent('sample.pdf');
        $expected = [
            '/ArchiveTransfer/Archive/ArchiveObject/Document/Attachment@filename',
        ];
        $this->assertEquals($expected, $results);

        $results = $util->findByContent('AR062');
        $expected = [
            '/ArchiveTransfer/Archive/ArchiveObject/ContentDescription/AccessRestrictionRule/Code',
            '/ArchiveTransfer/Archive/ArchiveObject/ContentDescription/Keyword[1]/AccessRestrictionRule/Code',
            '/ArchiveTransfer/Archive/ArchiveObject/AccessRestrictionRule/Code',
            '/ArchiveTransfer/Archive/ArchiveObject/ArchiveObject/ContentDescription/AccessRestrictionRule/Code',
            '/ArchiveTransfer/Archive/ArchiveObject/ArchiveObject/AccessRestrictionRule/Code'
        ];
        $this->assertEquals($expected, $results);

        $results = $util->findByContent('AR038');
        $expected = [
            '/ArchiveTransfer/Archive/AccessRestrictionRule/Code',
            '/ArchiveTransfer/Archive/ArchiveObject/ContentDescription/Keyword[2]/AccessRestrictionRule/Code',
        ];
        $this->assertEquals($expected, $results);

        $util->xpath->query('/ns:ArchiveTransfer/ns:Archive/ns:ArchiveObject/ns:Name')
            ->item(0)
            ->nodeValue = 'test "avec double quotes" et avec single quote (\')';
        $results = $util->findByContent('test "avec double quotes');
        $expected = ['/ArchiveTransfer/Archive/ArchiveObject/Name'];
        $this->assertEquals($expected, $results);

        $results = $util->findByContent('double quotes" et avec single quote (\')');
        $this->assertEquals($expected, $results);
    }

    public function testSetValue()
    {
        $dom = new DOMDocument;
        $dom->loadXML('<root><el></el></root>');
        $element = $dom->getElementsByTagName('el')->item(0);
        DOMUtility::setValue($element, 'foo&bar');
        $this->assertTextEquals('<el>foo&amp;bar</el>', $dom->saveXML($element));
    }
    public function testToArray()
    {
        $util = @DOMUtility::loadXML(
            <<<EOT
<?xml version="1.0" encoding="UTF-8"?>
<DocumentRoot xmlns="test" xmlns:ns2="test2">
  <!-- un commentaire -->
  Une valeur
  <!-- un autre commentaire -->
  <UnNoeud mon-attr="test">
    <!-- un commentaire dans un noeud -->
    <ns2:UnAutreNoeud>test</ns2:UnAutreNoeud>
  </UnNoeud>
  <UnNoeud>
    Valeur1
    <UnAutreNoeud xmlns="test2">test</UnAutreNoeud>
    Valeur2
  </UnNoeud>
</DocumentRoot>
EOT
        );
        $expected = [
            'DocumentRoot' => [
                0 => [
                    '@xmlns' => 'test',
                    '@xmlns:ns2' => 'test2',
                    'UnNoeud' => [
                        0 => [
                            '@mon-attr' => 'test',
                            'ns2:UnAutreNoeud' => [
                                0 => ['@' => 'test'],
                            ],
                        ],
                        1 => [
                            'UnAutreNoeud' => [
                                0 => [
                                    '@xmlns' => 'test2',
                                    '@' => "test",
                                ],
                            ],
                            '@' => "Valeur1\nValeur2",
                        ],
                    ],
                    '@' => 'Une valeur',
                ],
            ],
        ];
        $result = $util->toArray();
        $this->assertEquals($expected, $result);

        // avec option simplify
        $expected = [
            'DocumentRoot' => [
                '@xmlns' => 'test',
                '@xmlns:ns2' => 'test2',
                'UnNoeud' => [
                    0 => [
                        '@mon-attr' => 'test',
                        'ns2:UnAutreNoeud' => 'test',
                    ],
                    1 => [
                        'UnAutreNoeud' => [
                            '@xmlns' => 'test2',
                            '@' => "test",
                        ],
                        '@' => "Valeur1\nValeur2",
                    ],
                ],
                '@' => 'Une valeur',
            ],
        ];
        $result = $util->toArray(true);
        $this->assertEquals($expected, $result);

        // test avec un seda
        $result = DOMUtility::load(VolumeSample::getSamplePath('sa_394_transfer.xml'))->toArray();
        $this->assertEquals(
            'Transfert aléatoire atr_alea_70f90eacdb2d3b53ad7e87f808a82430',
            Hash::get($result, 'ArchiveTransfer.0.Comment.0.@')
        );
        $this->assertEquals(
            'edition 2009',
            Hash::get($result, 'ArchiveTransfer.0.Archive.0.ContentDescription.0.Language.0.@listVersionID')
        );
        $this->assertEquals(
            '97ea8588fc1b99dd777c95008dc489b3058706acfc7c1e75c946f31420882a54',
            Hash::get($result, 'ArchiveTransfer.0.Archive.0.ArchiveObject.0.ArchiveObject.0.Document.0.Integrity.0.@')
        );
    }

    public function testarrayToDOMDocument()
    {
        $initialArray = [
            'DocumentRoot' => [
                0 => [
                    '@xmlns' => 'test',
                    '@xmlns:ns2' => 'test2',
                    'UnNoeud' => [
                        0 => [
                            '@mon-attr' => 'test',
                            'ns2:UnAutreNoeud' => [
                                0 => ['@' => 'test'],
                            ],
                        ],
                        1 => [
                            'UnAutreNoeud' => [
                                0 => [
                                    '@xmlns' => 'test2',
                                    '@' => 'test',
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
        $result = @DOMUtility::arrayToDOMDocument($initialArray)->saveXML();
        $expected = <<<EOT
<?xml version="1.0"?>
<DocumentRoot xmlns="test" xmlns:ns2="test2">
  <UnNoeud mon-attr="test">
    <ns2:UnAutreNoeud>test</ns2:UnAutreNoeud>
  </UnNoeud>
  <UnNoeud>
    <UnAutreNoeud xmlns="test2">test</UnAutreNoeud>
  </UnNoeud>
</DocumentRoot>

EOT;
        $this->assertEquals($expected, $result);

        // test avec array simplifié
        $initialArray = [
            'DocumentRoot' => [
                '@xmlns' => 'test',
                '@xmlns:ns2' => 'test2',
                'UnNoeud' => [
                    0 => [
                        '@mon-attr' => 'test',
                        'ns2:UnAutreNoeud' => 'test',
                    ],
                    1 => [
                        'UnAutreNoeud' => [
                            '@xmlns' => 'test2',
                            '@' => 'test',
                        ],
                    ],
                ],
            ],
        ];
        $result = @DOMUtility::arrayToDOMDocument($initialArray)->saveXML();
        $this->assertEquals($expected, $result);

        // test avec un seda (test de réversibilité)
        $array = DOMUtility::load(VolumeSample::getSamplePath('sa_394_transfer.xml'))->toArray();
        $dom = DOMUtility::arrayToDOMDocument($array);
        $this->assertTrue($dom->schemaValidate(SEDA_V10_XSD));

        $util = new DOMUtility($dom);
        $this->assertEquals(
            'Transfert aléatoire atr_alea_70f90eacdb2d3b53ad7e87f808a82430',
            $util->nodeValue('ns:Comment')
        );
        /** @noinspection PhpPossiblePolymorphicInvocationInspection */
        $this->assertEquals(
            'edition 2009',
            $util->node('ns:Archive/ns:ContentDescription/ns:Language')->getAttribute('listVersionID')
        );
        $this->assertEquals(
            '97ea8588fc1b99dd777c95008dc489b3058706acfc7c1e75c946f31420882a54',
            $util->nodeValue('ns:Archive/ns:ArchiveObject/ns:ArchiveObject/ns:Document/ns:Integrity')
        );
    }
}
