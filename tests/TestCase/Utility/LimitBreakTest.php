<?php

namespace AsalaeCore\Test\TestCase\Utility;

use AsalaeCore\Utility\LimitBreak;
use AsalaeCore\TestSuite\TestCase;

class LimitBreakTest extends TestCase
{
    public function tearDown(): void
    {
        parent::tearDown();
        set_time_limit(0);
        ini_set('memory_limit', -1);
    }

    public function testSettingToBytes()
    {
        $b128M = 1024 * 1024 * 128; // 128 Mo
        $this->assertEquals($b128M, LimitBreak::settingToBytes('128M'));
        $this->assertEquals($b128M, LimitBreak::settingToBytes('128 Mo'));
        $this->assertEquals($b128M, LimitBreak::settingToBytes('   128 m  '));

        $this->assertEquals(456, LimitBreak::settingToBytes(456));
        $this->assertEquals(456, LimitBreak::settingToBytes('456b'));
        $this->assertEquals(1024, LimitBreak::settingToBytes('1k'));
        $this->assertEquals(pow(1024, 3), LimitBreak::settingToBytes('1g'));
    }
    
    public function testSetTimeLimit()
    {
        ini_set('max_execution_time', 10);
        LimitBreak::setTimeLimit(100);
        $expected = 100;
        $this->assertEquals($expected, ini_get('max_execution_time'));

        LimitBreak::setTimeLimit(50);
        $this->assertEquals($expected, ini_get('max_execution_time'));

        LimitBreak::setTimeLimit(200);
        $expected = 200;
        $this->assertEquals($expected, ini_get('max_execution_time'));

        LimitBreak::setTimeLimit(0);
        $this->assertEquals($expected, ini_get('max_execution_time'));
    }

// CI en échec, ini_set sans effets ?
//    public function testSetMemoryLimit()
//    {
//        ini_set('memory_limit', '128M');
//        LimitBreak::setMemoryLimit(0x40000000); // 1go
//        $expected = 0x40000000;
//        $this->assertEquals($expected, ini_get('memory_limit'));
//
//        LimitBreak::setMemoryLimit(50);
//        $this->assertEquals($expected, ini_get('memory_limit'));
//
//        LimitBreak::setMemoryLimit(0x80000000);
//        $expected = 0x80000000;
//        $this->assertEquals($expected, ini_get('memory_limit'));
//
//        LimitBreak::setMemoryLimit(-1);
//        $this->assertEquals($expected, ini_get('memory_limit'));
//    }
}
