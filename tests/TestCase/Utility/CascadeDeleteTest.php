<?php

namespace AsalaeCore\Test\TestCase\Utility;

use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\Utility\CascadeDelete;
use Cake\Database\StatementInterface;
use Cake\ORM\TableRegistry;

class CascadeDeleteTest extends TestCase
{
    public $fixtures = [
        'app.OrgEntities',
        'app.Users',
    ];

    public function testdryrun()
    {
        $results = CascadeDelete::dryrun(TableRegistry::getTableLocator()->get('OrgEntities'));
        $this->assertGreaterThan(1, $results);
    }

    public function testall()
    {
        $OrgEntities = TableRegistry::getTableLocator()->get('OrgEntities');
        $results = CascadeDelete::all($OrgEntities);
        $this->assertGreaterThan(1, $results);
        $this->assertCount(0, $OrgEntities->find());
    }

    public function testdisableConstraints()
    {
        $this->assertInstanceOf(StatementInterface::class, CascadeDelete::disableConstraints());
    }

    public function testenableConstraintsenableConstraints()
    {
        $this->assertInstanceOf(StatementInterface::class, CascadeDelete::enableConstraints());
    }

    public function testtruncate()
    {
        $OrgEntities = TableRegistry::getTableLocator()->get('OrgEntities');
        $this->assertInstanceOf(StatementInterface::class, CascadeDelete::truncate($OrgEntities));
        $this->assertCount(0, $OrgEntities->find());
    }
}
