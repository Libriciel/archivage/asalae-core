<?php

namespace AsalaeCore\Test\TestCase\Utility;

use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\Utility\Parallel;

class ParallelTest extends TestCase
{
    public function testexec()
    {
        $parallel = new Parallel(['threads' => 20, 'timeout' => 2]);
        $uuids = [];
        for ($i = 3; $i < 50; $i++) {
             $uuids[$i] = $parallel->exec([__CLASS__, 'isPrime'], [$i]);
        }
        $parallel->waitUntilAllProcessesAreFinished();
        $results = $parallel->getResults();
        $parallel->clearResults();
        $expected = [
            3,
            5,
            7,
            11,
            13,
            17,
            19,
            23,
            29,
            31,
            37,
            41,
            43,
            47,
        ];
        $primes = [];
        foreach ($uuids as $i => $uuid) {
            if ($results[$uuid]) {
                $primes[] = $i;
            }
        }
        $this->assertEquals($expected, $primes);
    }

    public static function isPrime(int $number)
    {
        for ($i = 2; $i < $number; $i++) {
            if ($number % $i == 0) {
                return false;
            }
        }
        return true;
    }
}
