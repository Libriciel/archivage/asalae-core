<?php

namespace AsalaeCore\Test\TestCase\Utility;

use AsalaeCore\Utility\XsdToArray;
use AsalaeCore\TestSuite\TestCase;

class XsdToArrayTest extends TestCase
{
    public function testXsdToArray()
    {
        $xsd = <<<EOT
<?xml version="1.0" encoding="UTF-8"?>
<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <xsd:element name="identite" >

        <xsd:complexType>

            <xsd:sequence>

                <xsd:group ref="nomPrenom"/>

                <xsd:element name="prenom2" type="xsd:string" minOccurs="0"/>

            </xsd:sequence>

            <xsd:attribute ref="uid"/>

        </xsd:complexType>

    </xsd:element>
    
    <xsd:group name="nomPrenom">

        <xs:sequence>

            <xsd:element name="nom" type="xsd:string" ></xsd:element>

            <xsd:element name="prenom" type="xsd:string" ></xsd:element>

        </xs:sequence>

    </xsd:group>

    <xsd:attribute name="uid">

        <xsd:simpleType>

            <xsd:restriction base="xsd:string">

                <xsd:enumeration value=""/>

            </xsd:restriction>

        </xsd:simpleType>

    </xsd:attribute>

</xsd:schema>
EOT;
        $expected = [
            'elements' => [
                'identite' => [
                    'type' => 'complex',
                    'traduction' => 'identite',
                    'sequence' => [
                        'identite/nom',
                        'identite/prenom',
                        'identite/prenom2',
                    ],
                    'attributes' => [
                        'uid' => [
                            'use' => 'optional'
                        ]
                    ],
                    'elements' => [
                        'identite/nom' => [
                            'minOccurs' => '1',
                            'maxOccurs' => '1',
                        ],
                        'identite/prenom' => [
                            'minOccurs' => '1',
                            'maxOccurs' => '1',
                        ],
                        'identite/prenom2' => [
                            'minOccurs' => '0',
                            'maxOccurs' => '1',
                        ],
                    ],
                    'name' => 'identite',
                    'choice' => [],
                    'enumeration' => [],
                    'content' => 'element-only',
                ],
                'identite/nom' => [
                    'type' => 'xsd:string',
                    'traduction' => 'identite/nom',
                    'elements' => [],
                    'name' => 'identite/nom',
                    'sequence' => [],
                    'choice' => [],
                    'attributes' => [],
                    'enumeration' => [],
                    'content' => 'string',
                ],
                'identite/prenom' => [
                    'type' => 'xsd:string',
                    'traduction' => 'identite/prenom',
                    'elements' => [],
                    'name' => 'identite/prenom',
                    'sequence' => [],
                    'choice' => [],
                    'attributes' => [],
                    'enumeration' => [],
                    'content' => 'string',
                ],
                'identite/prenom2' => [
                    'type' => 'xsd:string',
                    'traduction' => 'identite/prenom2',
                    'elements' => [],
                    'name' => 'identite/prenom2',
                    'sequence' => [],
                    'choice' => [],
                    'attributes' => [],
                    'enumeration' => [],
                    'content' => 'string',
                ],
            ],
        ];

        $result = XsdToArray::build($xsd);
        $this->assertEquals($expected, $result);
    }

    public function testXsdToArrayWithEacCpf()
    {
        $array = XsdToArray::build(EAC_CPF_XSD);
        $compare = [
            $array['elements']['eac-cpf']['elements']['control']['minOccurs'],
            $array['elements']['control']['elements']['otherRecordId']['maxOccurs'],
            $array['elements']['identity']['elements']['identity/nameEntry']['maxOccurs'],
        ];
        $expected = [
            '1',
            'unbounded',
            'unbounded', // cas particulier, maxOccurs est fourni par "choice"
        ];
        $this->assertEquals($expected, $compare);
    }
}
