<?php

namespace AsalaeCore\Test\TestCase\Utility;

use AsalaeCore\Utility\XmlUtility;
use AsalaeCore\TestSuite\TestCase;

class XmlUtilityTest extends TestCase
{
    /**
     * @var string xml pour effectuer des tests
     */
    public $xml = <<<EOT
<?oxygen
    RNGSchema="http://eac.staatsbibliothek-berlin.de/schema/cpf.rng" type="xml"
?>
<repertoire>
  <personne>Bernard</personne>
  <personne>Patrick</personne>
  <!-- NOTE: bug sur les commentaires //-->
  <note>
    <date>2008-01-10</date>
    <body>Don't forget me this weekend!</body>
  </note>
</repertoire>
EOT;

    /**
     * @var array data obtenu du xml pour effectuer les tests
     */
    public $data = [
        'oxygen' => [], // NOTE : est une erreur de conversion
        'repertoire' => [
            'personne' => [
                'Bernard',
                'Patrick'
            ],
            'note' => [
                'date' => '2008-01-10',
                'body' => "Don't forget me this weekend!"
            ],
            '#comment' => [] // NOTE: bug sur les commentaires
        ]
    ];

    public function testXmlStringToJson()
    {
        $expected = $this->data;
        $actual = json_decode(XmlUtility::xmlStringToJson($this->xml), true);
        $this->assertEquals($expected, $actual);
    }

    public function testJsonToXmlString()
    {
        $dom = new \DOMDocument;
        $this->assertTrue($dom->loadXML(XmlUtility::jsonToXmlString(json_encode($this->data))));
        $this->assertStringContainsString("Don't forget me this weekend!", $dom->saveXML());
    }

    public function testValidateXmlString()
    {
        $xsd = <<<EOT
<?xml version="1.0"?>
<xs:schema xmlns:xs = "http://www.w3.org/2001/XMLSchema">

  <xs:element name="repertoire">
    <xs:complexType>
      <xs:sequence>
        <xs:element name="personne" minOccurs="1" maxOccurs="unbounded"/>
        <xs:element ref="note" minOccurs="0" maxOccurs="1"/>
      </xs:sequence>
    </xs:complexType>
  </xs:element>

  <xs:element name="note">
    <xs:complexType>
      <xs:sequence>
        <xs:choice>
          <xs:element name="date" minOccurs="1" maxOccurs="unbounded"/>
          <xs:sequence>
            <xs:element name="fromDate"/>
            <xs:element name="toDate"/>
          </xs:sequence>
        </xs:choice>
        <xs:element name="body" minOccurs="1" maxOccurs="1"/>
      </xs:sequence>
    </xs:complexType>
  </xs:element>

</xs:schema>
EOT;

        $data = $this->data;
        $xml = XmlUtility::jsonToXmlString(json_encode($data));
        $this->assertTrue(XmlUtility::validateXmlString($xml, $xsd)); // Montre que c'es bien valide au départ

        // Dépassement du maxOccurs
        $data['repertoire']['note'] = [$data['repertoire']['note'], $data['repertoire']['note']];
        $xml = XmlUtility::jsonToXmlString(json_encode($data));
        $this->assertFalse(@XmlUtility::validateXmlString($xml, $xsd));

        // minOccurs non respecté
        $data = $this->data;
        unset($data['repertoire']['note']['body']);
        $xml = XmlUtility::jsonToXmlString(json_encode($data));
        $this->assertFalse(@XmlUtility::validateXmlString($xml, $xsd));

        // sequence non respectée
        $data = $this->data;
        $personnes = $data['repertoire']['personne'];
        unset($data['repertoire']['personne']);
        $data['repertoire']['personne'] = $personnes; // Déplace personne vers le bas
        $xml = XmlUtility::jsonToXmlString(json_encode($data));
        $this->assertFalse(@XmlUtility::validateXmlString($xml, $xsd));

        // choix non respecté
        $data = $this->data;
        $data['repertoire']['note'] = [
            'date' => '1920',
            'fromDate' => '1920',
            'toDate' => '1980',
            'body' => 'yes'
        ];
        $xml = XmlUtility::jsonToXmlString(json_encode($data));
        $this->assertFalse(@XmlUtility::validateXmlString($xml, $xsd));
    }
}
