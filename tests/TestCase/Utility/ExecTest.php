<?php

namespace AsalaeCore\Test\TestCase\Utility;

use AsalaeCore\TestSuite\AsyncTrait;
use AsalaeCore\Utility\Exec;
use AsalaeCore\TestSuite\TestCase;

class ExecTest extends TestCase
{
    use AsyncTrait;

    public function testEscapeCommand()
    {
        $utility = new Exec;

        $toEscape = 'foo';
        $expected = "'foo'";
        $this->assertEquals($expected, $utility->escapeCommand($toEscape));

        $toEscape = "foo's";
        $expected = '"foo\'s"';
        $this->assertEquals($expected, $utility->escapeCommand($toEscape));

        $toEscape = <<<eot
echo foo's "bar"
eot;
        $expected = <<<eot
'echo foo'"\'"'s "bar"'
eot;
        $this->assertEquals($expected, $utility->escapeCommand($toEscape));
    }
    
    public function testAsync()
    {
        $tempfile = sys_get_temp_dir() . DS . uniqid('testunit-');
        $utility = new Exec;

        $bytes = base64_encode(random_bytes(10));
        $this->repeatUntilFileExist(
            function () use ($utility, $bytes, $tempfile) {
                $utility->setDefaultStdout($tempfile);
                $utility->async("sleep 0.0001 && echo '$bytes'");
            },
            $tempfile
        );
        $this->assertStringContainsString($bytes, file_get_contents($tempfile));
        if (file_exists($tempfile)) {
            unlink($tempfile);
        }
        $bytes = base64_encode(random_bytes(10));
        $this->repeatUntilFileExist(
            function () use ($utility, $bytes, $tempfile) {
                $utility->setDefaultStdout($tempfile);
                $utility->async(
                    'sleep',
                    [
                        '0.0001' => null, // pas d'échapement d'argument
                        '&&' => null,
                        'echo' => $bytes, // échapement de la valeur uniquement
                    ]
                );
            },
            $tempfile
        );
        $this->assertStringContainsString($bytes, file_get_contents($tempfile));
        if (file_exists($tempfile)) {
            unlink($tempfile);
        }
    }

    public function testCommand()
    {
        $utility = new Exec;
        $result = $utility->command('echo', 'testunit');
        $this->assertTrue($result->success);
        $this->assertEquals(0, $result->code);
        $this->assertStringContainsString('testunit', $result->stdout);
        $this->assertEmpty($result->stderr);

        $result = $utility->command('commande_qui_nexiste_pas');
        $this->assertFalse($result->success);
        $this->assertEquals(127, $result->code);
        $this->assertEmpty($result->stdout);
        $this->assertNotEmpty($result->stderr);
    }
}
