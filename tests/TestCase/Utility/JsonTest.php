<?php /** @noinspection PhpUnusedLocalVariableInspection */

namespace AsalaeCore\Test\TestCase\Utility;

use AsalaeCore\Utility\Json;
use AsalaeCore\TestSuite\TestCase;

class JsonTest extends TestCase
{
    public function testEncode()
    {
        $a = "test";
        $b = ["test", "foo", "bar"];
        $c = ["test" => "test", "foo" => "foo", "bar" => "bar"];
        $d = ["test" => ["foo" => "bar"], "baz"];
        $e = [0 => "test", 2 => "foo", 1 => "bar"];
        $f = new \ArrayObject($b);
        $g = new \ArrayObject($c);
        $h = new \ArrayObject(["test" => new \ArrayObject(["foo" => "bar", "baz"])]);
        $i = new \ArrayObject($h);
        foreach (['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i'] as $v) {
            $normal = json_encode($$v);
            $utility = Json::encode($$v);
            $this->assertEquals($normal, $utility);
            $this->assertEquals(Json::decode($normal), json_decode($utility));
            $this->assertEquals(Json::decode($normal, true), json_decode($utility, true));
        }
    }
}
