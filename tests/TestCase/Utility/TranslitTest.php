<?php

namespace AsalaeCore\Test\TestCase\Utility;

use AsalaeCore\Utility\Translit;
use AsalaeCore\TestSuite\TestCase;

class TranslitTest extends TestCase
{
    public const ACCENTS = [
        'À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î',
        'Ï', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'à', 'á', 'â',
        'ã', 'ä', 'å', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ð', 'ò',
        'ó', 'ô', 'õ', 'ö', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ'
    ];

    public const LETTERS = [
        'A', 'A', 'A', 'A', 'A', 'A', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I',
        'I', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 'a', 'a', 'a',
        'a', 'a', 'a', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'eth', 'o',
        'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y'
    ];

    public function testAscii()
    {
        foreach (self::ACCENTS as $key => $accent) {
            $strip = Translit::ascii($accent);
            self::assertTrue(
                Translit::ascii($accent) === self::LETTERS[$key],
                $strip.' != '.self::LETTERS[$key]
            );
        }
        self::assertTrue(
            Translit::ascii(implode("", self::ACCENTS))
            === implode("", self::LETTERS)
        );
    }

    public function testAsciiToLower()
    {
        $result = Translit::asciiToLower(
            <<<EOT
La lettre Ð, nommée eth ou ed, est utilisée en dalécarlien, féroïen, 
islandais et anciennement vieil anglais et norrois pour transcrire la 
consonne fricative dentale voisée [ð] (comme le « th » dans les mots 
anglais moderne them ou that), caractéristique des anciennes langues 
scandinaves. Son caractère minuscule ressemble à un d cursif ou d insulaire
barré (ð). Sa forme sourde est la lettre Þ (thorn).
EOT
        );
        $expected = <<<EOT
la lettre eth, nommee eth ou ed, est utilisee en dalecarlien, feroien, 
islandais et anciennement vieil anglais et norrois pour transcrire la 
consonne fricative dentale voisee [eth] (comme le << th >> dans les mots 
anglais moderne them ou that), caracteristique des anciennes langues 
scandinaves. son caractere minuscule ressemble a un d cursif ou d insulaire
barre (eth). sa forme sourde est la lettre th (thorn).
EOT;
        $this->assertEquals($expected, $result);

        $result = Translit::asciiToLower("Fᾁὥ", false);
        $expected = 'fhaiho';
        $this->assertEquals($expected, $result);

        $result = Translit::asciiToLower("Fᾁὥ", true);
        $expected = 'fhh';
        $this->assertEquals($expected, $result);
    }

    public function testSafeUri()
    {
        $paths = [
            "\0test/foo.bar",
            "-\/:*?\"<>| ,;[]()^ #%&!@:+={}'~",
            "İ㐀",
            "testé",
        ];
        $expected = [
            "test/foo.bar",
            "/_ ,;[]()^ #%&!@_+={}'",
            "İ㐀",
            "testé",
        ];
        foreach ($paths as $i => $path) {
            $this->assertEquals($expected[$i], Translit::safeUri($path));
        }
    }
}
