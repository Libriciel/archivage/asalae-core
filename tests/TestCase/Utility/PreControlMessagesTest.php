<?php

namespace AsalaeCore\Test\TestCase\Utility;

use AsalaeCore\Utility\PreControlMessages;
use AsalaeCore\TestSuite\TestCase;
use Cake\ORM\TableRegistry;

class PreControlMessagesTest extends TestCase
{
    const SEDA_02_XML = TESTS.'Data'.DS.'sample_seda02.xml';

    public $fixtures = [
        "app.TypeEntities",
        "app.OrgEntities",
        "app.Transfers",
    ];

    public function testValidate()
    {
        TableRegistry::getTableLocator()->get('Transfers')
            ->updateAll(
                ['transfer_identifier' => 'foo'],
                ['transfer_identifier' => 'atr_alea_70f90eacdb2d3b53ad7e87f808a82430']
            );
        $PreControl = new PreControlMessages(self::SEDA_02_XML);
        $PreControl->schemas = [
            'fr:gouv:ae:archive:draft:standard_echange_v0.2' => SEDA_V02_XSD,
        ];
        $success = $PreControl->validate();
        $message = PreControlMessages::getLastErrorMessage();
        $this->assertTrue($success, $message);

        $dom = $PreControl->getDom();
        $dom->getElementsByTagName('Identification')->item(0)->nodeValue = '';
        $tmpFile = sys_get_temp_dir().DS.uniqid('testunit-');
        $dom->save($tmpFile);
        $PreControl = new PreControlMessages($tmpFile);
        $PreControl->schemas = [
            'fr:gouv:ae:archive:draft:standard_echange_v0.2' => SEDA_V02_XSD,
        ];
        $this->assertTrue($PreControl->validateBySchema());
        $success = $PreControl->validate();
        unlink($tmpFile);
        $this->assertFalse($success);
    }
}
