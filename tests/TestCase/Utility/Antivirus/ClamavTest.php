<?php

namespace AsalaeCore\Test\TestCase\Utility\Antivirus;

use AsalaeCore\Factory\Utility;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\Utility\Antivirus\Clamav;
use AsalaeCore\Utility\Exec;
use Cake\Core\Configure;
use Libriciel\Filesystem\Utility\Filesystem;

class ClamavTest extends TestCase
{
    public $pid;

    public function setUp(): void
    {
        parent::setUp();
        Configure::write('Antivirus.host', '127.0.0.1');
        Configure::write('Antivirus.port', 5353);
        Utility::reset();
        $Exec = new Exec();
        $this->pid = $Exec->async(
            CAKE_SHELL . ' clamav_simulator --will-find-virus --port 5353 --time-limit 5'
        );
        $waitingTime = 0;
        do { // laisse le temps au shell de se lancer (0.1s)
            usleep(100000);
            $waitingTime += 0.1;
        } while (!Exec::runningAsync() && $waitingTime < 2.0);
        if (is_dir(TMP_TESTDIR)) {
            Filesystem::remove(TMP_TESTDIR);
        }
        mkdir(TMP_TESTDIR, 0777, true);
    }

    public function tearDown(): void
    {
        parent::tearDown();
        if (is_file(TMP_TESTDIR . '/simulator.log')) {
            debug(file_get_contents(TMP_TESTDIR . '/simulator.log'));
        }
        @exec('kill '.$this->pid);
        if (is_dir(TMP_TESTDIR)) {
            Filesystem::remove(TMP_TESTDIR);
        }
    }

    public function testscan()
    {
        file_put_contents(TMP_TESTDIR . DS . 'test.txt', 'test');
        $expected = [TMP_TESTDIR . DS . 'test.txt' => 'asalae.fake.virus'];
        $this->assertEquals($expected, Clamav::scan(TMP_TESTDIR));
    }

    public function testping()
    {
        $this->assertTrue(Clamav::ping());
    }
}
