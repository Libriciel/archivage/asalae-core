<?php
namespace AsalaeCore\Test\TestCase\Auth\Authenticator;

use AsalaeCore\Auth\Authenticator\KeycloakAuthenticator;
use AsalaeCore\Auth\Identifier\KeycloakIdentifier;
use AsalaeCore\Factory\Utility;
use AsalaeCore\Http\AppServerRequest;
use AsalaeCore\Http\Response;
use AsalaeCore\TestSuite\InvokePrivateTrait;
use AsalaeCore\TestSuite\TestCase;
use Authentication\Authenticator\Result;
use Cake\Http\Client;
use Cake\Http\Session;
use DateInterval;
use DateTime;

/**
 * AsalaeCore\Auth\Authenticator\KeycloakAuthenticator Test Case
 */
class KeycloakAuthenticatorTest extends TestCase
{
    use InvokePrivateTrait;

    public $fixtures = [
        'app.Aros',
        'app.Configurations',
        'app.Ldaps',
        'app.OrgEntities',
        'app.Roles',
        'app.TypeEntities',
        'app.Users',
    ];

    public function testAuthenticate()
    {
        $json = json_encode(['preferred_username' => 'testunit', 'access_token' => 'test']); // user.1
        $response = $this->createMock(Client\Response::class);
        $response->method('getStringBody')->willReturn($json);
        $client = new class extends Client {
            public static Client\Response $response;
            // phpcs:ignore
            public function post(string $url, $data = [], array $options = []): Client\Response
            {
                return self::$response;
            }
        };
        $client::$response = $response;
        Utility::set(Client::class, $client);

        $identity = new KeycloakIdentifier(['access_token' => 'test']);
        $authenticator = new KeycloakAuthenticator(
            $identity,
            [
                'client_secret' => 'test',
                'redirect_uri' => 'test',
                'endpoints' => [
                    'token' => 'http://test',
                    'end_session' => 'http://test',
                ],
            ]
        );

        // Authorization Bearer
        $session = $this->createMock(Session::class);
        $request = $this->createMock(AppServerRequest::class);
        $request->method('getAttribute')->willReturn($session);
        $request->method('getHeaderLine')->willReturn('Bearer test');
        $result = $authenticator->authenticate($request);
        $this->assertEquals(Result::SUCCESS, $result->getStatus());

        // Authorization token
        $session = $this->createMock(Session::class);
        $request = $this->createMock(AppServerRequest::class);
        $request->method('getAttribute')->willReturn($session);
        $queryParams = [
            'code' => 'test',
            'session_state' => 'test',
        ];
        $request->method('getQueryParams')->willReturn($queryParams);
        $result = $authenticator->authenticate($request);
        $this->assertEquals(Result::SUCCESS, $result->getStatus());

        // Déjà connecté
        $session = $this->createMock(Session::class);
        $auth = [
            'access_token' => 'test',
            'refresh_token' => 'test',
            'expires_in' => (new DateTime)->add(new DateInterval('PT1H'))->getTimestamp(),
            'refresh_expires_in' => (new DateTime)->add(new DateInterval('PT1H'))->getTimestamp(),
            'time' => 0,
        ];
        $session->method('read')->willReturn($auth);
        $request = $this->createMock(AppServerRequest::class);
        $request->method('getAttribute')->willReturn($session);
        $result = $authenticator->authenticate($request);
        $this->assertEquals(Result::SUCCESS, $result->getStatus());
    }

    public function testPersistIdentity()
    {
        $json = json_encode(['preferred_username' => 'testunit', 'access_token' => 'test']); // user.1
        $response = $this->createMock(Client\Response::class);
        $response->method('getStringBody')->willReturn($json);
        $client = $this->createMock(Client::class);
        $client->method('post')->willReturn($response);
        Utility::set(Client::class, $client);

        $identity = new KeycloakIdentifier(['access_token' => 'test']);
        $authenticator = new KeycloakAuthenticator(
            $identity,
            [
                'client_secret' => 'test',
                'redirect_uri' => 'test',
                'endpoints' => [
                    'token' => 'http://test',
                    'end_session' => 'http://test',
                ],
            ]
        );
        $session = $this->createMock(Session::class);
        $request = $this->createMock(AppServerRequest::class);
        $request->method('getAttribute')->willReturn($session);
        $response = $this->createMock(Response::class);
        $identityArray = [
            'auth_keycloak' => 'test',
        ];
        $result = $authenticator->persistIdentity($request, $response, $identityArray);
        $this->assertTrue(is_array($result));
        $this->assertArrayHasKey('request', $result);
        $this->assertArrayHasKey('response', $result);

        $identityArray = [
            'auth_keycloak' => null,
        ];
        $result = $authenticator->persistIdentity($request, $response, $identityArray);
        $this->assertTrue(is_array($result));
        $this->assertArrayHasKey('request', $result);
        $this->assertArrayHasKey('response', $result);

        $this->invokeProperty($authenticator, 'auth', 'set', ['test']);
        $identityArray = [
            'auth_keycloak' => 'test',
        ];
        $result = $authenticator->persistIdentity($request, $response, $identityArray);
        $this->assertTrue(is_array($result));
        $this->assertArrayHasKey('request', $result);
        $this->assertArrayHasKey('response', $result);
    }

    public function testClearIdentity()
    {
        $identity = new KeycloakIdentifier(['access_token' => 'test']);
        $authenticator = new KeycloakAuthenticator(
            $identity,
            [
                'client_secret' => 'test',
                'redirect_uri' => 'test',
                'endpoints' => [
                    'token' => 'http://test',
                    'end_session' => 'http://test',
                ],
            ]
        );
        $session = $this->createMock(Session::class);
        $session->method('read')->willReturn(['test']);
        $request = $this->createMock(AppServerRequest::class);
        $request->method('getAttribute')->willReturn($session);
        $response = $this->createMock(Response::class);
        $result = $authenticator->clearIdentity($request, $response);
        $this->assertTrue(is_array($result));
        $this->assertArrayHasKey('request', $result);
        $this->assertArrayHasKey('response', $result);
    }
}
