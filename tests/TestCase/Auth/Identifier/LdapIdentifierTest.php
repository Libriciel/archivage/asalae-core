<?php
namespace AsalaeCore\Test\TestCase\Auth\Identifier;

use Adldap\Adldap;
use Adldap\Auth\Guard;
use Adldap\Connections\Provider;
use Adldap\Models\Model;
use Adldap\Query\Builder;
use Adldap\Query\Factory;
use AsalaeCore\Auth\Identifier\LdapIdentifier;
use AsalaeCore\Factory\Utility;
use AsalaeCore\TestSuite\TestCase;
use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;

/**
 * AsalaeCore\Auth\Identifier\LdapIdentifier Test Case
 */
class LdapIdentifierTest extends TestCase
{
    public $fixtures = [
        'app.Aros',
        'app.Configurations',
        'app.Ldaps',
        'app.OrgEntities',
        'app.Roles',
        'app.TypeEntities',
        'app.Users',
    ];

    public function testIdentify()
    {
        $adldap = $this->createMock(Adldap::class);
        $adldap->method('addProvider');
        $provider = $this->createMock(Provider::class);
        $adldap->method('connect')->willReturn($provider);
        $guard = $this->createMock(Guard::class);
        $provider->method('auth')->willReturn($guard);
        $guard->method('attempt')->willReturn(true);
        $factory = $this->createMock(Factory::class);
        $provider->method('search')->willReturn($factory);
        $users = $this->createMock(Builder::class);
        $factory->method('users')->willReturn($users);
        $ldapUser = $this->createMock(Model::class);
        $users->method('findBy')->willReturn($ldapUser);
        $ldapUser->method('getAttribute')->willReturn(['foo'], ['bar@baz.fr']);
        Utility::set(Adldap::class, $adldap);

        TableRegistry::getTableLocator()->clear();
        TableRegistry::getTableLocator()->get('Users')->updateAll(['ldap_id' => 1], []);
        $identity = new LdapIdentifier;
        $result = $identity->identify(['username' => 'testunit', 'password' => 'test']);
        $this->assertInstanceOf(EntityInterface::class, $result);
    }

    public function testGetLdapConfig()
    {
        TableRegistry::getTableLocator()->get('Users')->updateAll(['ldap_id' => 1], []);
        $identity = new LdapIdentifier;
        $result = $identity->getLdapConfig(TableRegistry::getTableLocator()->get('Users')->get(1));
        $this->assertArrayHasKey('base_dn', $result);
    }
}
