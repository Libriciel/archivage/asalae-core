<?php

namespace AsalaeCore\Test\TestCase\Validation;

use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\Validation\RulesProvider;

class RulesProviderTest extends TestCase
{

    public function testDate()
    {
        $provider = new RulesProvider;
        $this->assertTrue($provider->date('01-01-1900'));
        $this->assertTrue($provider->date('1900-01-01'));
        $this->assertFalse($provider->date('foo'));
    }

    public function testDatetime()
    {
        $provider = new RulesProvider;
        $this->assertTrue($provider->datetime('01-01-1900'));
        $this->assertTrue($provider->datetime('1900-01-01'));
        $this->assertFalse($provider->datetime('foo'));
    }

    public function testBoolean()
    {
        $provider = new RulesProvider;
        $this->assertTrue($provider->boolean('true'));
        $this->assertTrue($provider->boolean('0'));

        $this->assertFalse($provider->boolean('foo'));
        $this->assertFalse($provider->boolean('121'));
    }
}
