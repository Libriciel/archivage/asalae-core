<?php

namespace AsalaeCore\Test\TestCase\Cron;

use AsalaeCore\Cron\Unlocker;
use Cake\ORM\TableRegistry;
use Cake\Console\TestSuite\ConsoleIntegrationTestTrait;
use Cake\TestSuite\Stub\ConsoleOutput;
use AsalaeCore\TestSuite\TestCase;

class UnlockerTest extends TestCase
{
    use ConsoleIntegrationTestTrait;

    public $fixtures = [
        'app.Crons',
        'app.CronExecutions',
    ];

    public function testMain()
    {
        $loc = TableRegistry::getTableLocator();
        // compatibilitée asalae_core/asalae2
        $Crons = $loc->get('AsalaeCore.Crons');
        $loc->set('Crons', $Crons);
        $CronExecutions = $loc->get('AsalaeCore.CronExecutions');
        $loc->set('CronExecutions', $CronExecutions);
        $CronExecutions->belongsTo('Crons')->setClassName('AsalaeCore.Crons');
        $cron = $Crons->get(1);
        $this->_out = new ConsoleOutput();
        $this->_err = new ConsoleOutput();
        $Unlocker = new Unlocker([], $this->_out, $this->_err);

        // cron vérrouillé sans execution
        $CronExecutions->deleteAll([]);
        $this->assertTrue((bool)$cron->get('locked'));
        $Unlocker->work();
        $this->assertOutputContains(__("Le cron ''{0}'' a été vérrouillé sans raisons", $cron->get('name')));
        $cron = $Crons->get(1);
        $this->assertFalse($cron->get('locked'));

        // cron vérrouillé depuis plus d'1h
        $exec = $CronExecutions->newEntity(
            [
                'cron_id' => 1,
                'date_begin' => (new \DateTime())->sub(new \DateInterval('PT10H')),
                'state' => 'running',
                'report' => '',
            ]
        );
        $this->assertNotFalse($CronExecutions->save($exec));
        $this->assertNotNull($cron->get('lastexec'));
        $cron->set('locked', true);
        $cron->set('frequency', 'PT1H');
        $this->assertNotFalse($Crons->save($cron));
        $Unlocker->work();
        $cron = $Crons->get(1);
        $this->assertFalse($cron->get('locked'));
        $exec = $CronExecutions->get($exec->get('id'));
        $this->assertEquals('error', $exec->get('state'));

        // cron vérrouillé depuis plus d'1h mais avec un max_duration custom de 12h
        $exec = $CronExecutions->newEntity(
            [
                'cron_id' => 1,
                'date_begin' => (new \DateTime())->sub(new \DateInterval('PT10H')),
                'state' => 'running',
                'report' => '',
            ]
        );
        $this->assertNotFalse($CronExecutions->save($exec));
        $this->assertNotNull($cron->get('lastexec'));
        $cron->set('locked', true);
        $cron->set('frequency', 'PT1H');
        $cron->set('max_duration', 'PT12H');
        $this->assertNotFalse($Crons->save($cron));
        $Unlocker->work();
        $cron = $Crons->get(1);
        $this->assertTrue((bool)$cron->get('locked'));
        $CronExecutions->delete($exec);
        $cron->set('max_duration', 'PT1H');
        $this->assertNotFalse($Crons->save($cron));

        // cron verrouillé depuis moins d'1h
        $exec = $CronExecutions->newEntity(
            [
                'cron_id' => 1,
                'date_begin' => (new \DateTime()),
                'state' => 'running',
                'report' => '',
            ]
        );
        $this->assertNotFalse($CronExecutions->save($exec));
        $cron->set('locked', true);
        $this->assertNotFalse($Crons->save($cron));
        $Unlocker->work();
        $cron = $Crons->get(1);
        $this->assertTrue((bool)$cron->get('locked'));
        $exec = $CronExecutions->get($exec->get('id'));
        $this->assertEquals('running', $exec->get('state'));
        // apparait comme le dernier exec
        $CronExecutions->delete($exec);

        // cron non verrouillé mais execution non terminée (> 1h)
        $exec = $CronExecutions->newEntity(
            [
                'cron_id' => 1,
                'date_begin' => (new \DateTime())->sub(new \DateInterval('PT9H')),
                'state' => 'running',
                'report' => '',
            ]
        );
        $this->assertNotFalse($CronExecutions->save($exec));
        $cron = $Crons->get(1);
        $cron->set('locked', false);
        $this->assertNotFalse($Crons->save($cron));
        $Unlocker->work();
        $exec = $CronExecutions->get($exec->get('id'));
        $this->assertNotNull($exec->get('date_end'));
        $this->assertEquals('warning', $exec->get('state'));
    }
}
