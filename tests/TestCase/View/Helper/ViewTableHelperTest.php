<?php
namespace AsalaeCore\Test\TestCase\View\Helper;

use AsalaeCore\View\Helper\ViewTableHelper;
use Cake\Datasource\EntityInterface;
use Cake\I18n\FrozenDate;
use Cake\I18n\I18n;
use Cake\ORM\Entity;
use AsalaeCore\TestSuite\TestCase;
use Cake\Utility\Hash;
use Cake\View\View;

/**
 * AsalaeCore\View\Helper\ViewTableHelper Test Case
 */
class ViewTableHelperTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \AsalaeCore\View\Helper\ViewTableHelper
     */
    public $ViewTableHelper;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $view = new View();
        $this->ViewTableHelper = new ViewTableHelper($view);
        I18n::setLocale('fr_FR');
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->ViewTableHelper);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     * @throws \Exception
     */
    public function testGenerate()
    {
        $entity = new Entity(
            [
                'name' => 'Foo',
                'created' => new FrozenDate('2011-09-02'),
                'size' => 4899662,
                'sub_entity' => new Entity(
                    [
                        'name' => 'sub_entity',
                        'active' => true
                    ]
                ),
                'sub_entities' => [
                    new Entity(
                        [
                            'name' => 'sub_entities_1',
                            'active' => false
                        ]
                    ),
                    new Entity(
                        [
                            'name' => 'sub_entities_2',
                            'empty' => 'no'
                        ]
                    )
                ]
            ]
        );
        $table = $this->ViewTableHelper->generate(
            $entity,
            [
                'Name' => 'name',
                'Date' => 'created the {created}',
                'Not set' => '{not_set|default(" N/C ")|trim}',
                'Not set 2' => Hash::get($entity, 'not_set') ? '{name} is set' : '{name} is not set',
                'Not set 3' => null,
                'Not set 4' => '{foo.not_set} ({foo.not_set2})',
                'Sub' => 'sub_entity.name',
                'Sub bool' => 'Is active ? ({sub_entity.active})',
                'multi 1' => 'sub_entities.0.name',
                'multi 2' => 'sub_entities.{n}.empty',
                'custom' => '{do_not_parse}custom field',
                'advanced custom' => function (EntityInterface $entity) {
                    switch ($entity->get('name')) {
                        case 'Foo':
                            return 'nice shot!';
                        default:
                            return 'missed';
                    }
                },
                'Size' => 'size|toReadableSize',
            ]
        );
        $this->assertMatchesRegularExpression('/Name.*Foo/', $table);
        $this->assertMatchesRegularExpression('/Date.*created the 02\/09\/2011/', $table);
        $this->assertMatchesRegularExpression('/Not set.*>N\/C</', $table);
        $this->assertMatchesRegularExpression('/Not set 2.*Foo is not set/', $table);
        $this->assertMatchesRegularExpression('/Not set 3.*></', $table);
        $this->assertMatchesRegularExpression('/Not set 4.*> \(\)</', $table);
        $this->assertMatchesRegularExpression('/Sub.*sub_entity/', $table);
        $this->assertMatchesRegularExpression('/Sub bool.*Is active \? \('.__("Oui").'\)/', $table);
        $this->assertMatchesRegularExpression('/multi 1.*sub_entities_1/', $table);
        $this->assertStringContainsString('<li>no</li>', $table);
        $this->assertStringNotContainsString('do_not_parse', $table);
        $this->assertStringContainsString('nice shot!', $table);
        $this->assertStringContainsString('4,67 MB', $table);
    }
}
