<?php

namespace AsalaeCore\Test\TestCase\View\Helper;

use AsalaeCore\Http\AppServerRequest;
use AsalaeCore\View\Helper\ModalFormHelper;
use AsalaeCore\TestSuite\TestCase;
use Cake\Core\Configure;
use Cake\View\View;

class ModalFormHelperTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
        Configure::write('App.defaultLocale', 'fr_FR');
    }

    private function getModalFormHelper(array $queryParams = []): ModalFormHelper
    {
        return new ModalFormHelper(
            new View(
                (new AppServerRequest)->withQueryParams($queryParams)
            )
        );
    }

    public function testGenerateLoadedFilterContent()
    {
        $modal = $this->getModalFormHelper()
            ->create('id-test')
            ->modal("Message en haut de la modale")
            ->javascriptCallback('callbackJavascript')
            ->output('function', 'funcToOpenModal', '/my-controller/my-action')
            ->generate();
        $this->assertStringContainsString("$('#id-test')", $modal);
        $this->assertStringContainsString("Message en haut de la modale</h4>", $modal);
        $this->assertStringContainsString("callbackJavascript(content, textStatus, jqXHR);", $modal);
        $this->assertStringContainsString("function funcToOpenModal() {", $modal);
        $this->assertStringContainsString("/my-controller/my-action", $modal);

        // Toutes les options
        $Form = $this->getModalFormHelper()->Form;
        $Fa = $this->getModalFormHelper()->Fa;
        $modal = $this->getModalFormHelper()
            ->create(
                'id-test2',
                [
                    'size' => 'modal-xxl',
                    'class' => 'css-class',
                    'acceptButton' => $Form->button(
                        $Fa->i('fa-check', 'test accept button'),
                        ['bootstrap-type' => 'primary', 'class' => 'accept']
                    ),
                    'cancelButton' => $Form->button(
                        $Fa->i('fa-check-o', 'test cancel button'),
                        ['bootstrap-type' => 'primary', 'class' => 'cancel']
                    ),
                ]
            )
            ->modal("Message en haut de la modale")
            ->javascriptCallback('callbackJavascript')
            ->output(
                'button',
                $Fa->charte('Ajouter', "Ajouter un LDAP"),
                '/admin-tenants/addLdap'
            )
            ->generate(
                [
                    'class' => 'btn btn-success',
                    'tabindex' => '0',
                    'type' => 'button',
                    'disabled' => true,
                    'onkeydown' => 'test(event)',
                    'escape' => false,
                    'title' => "test",
                ]
            );
        $this->assertStringContainsString("$('#id-test2')", $modal);
        $this->assertStringContainsString("Ajouter un LDAP", $modal);
        $this->assertStringContainsString("/admin-tenants/addLdap", $modal);
        $this->assertStringContainsString("test accept button", $modal);
        $this->assertStringContainsString("test cancel button", $modal);
        $this->assertStringContainsString("modal-xxl", $modal);
        $this->assertStringContainsString("css-class", $modal);
        $this->assertStringContainsString("btn btn-success", $modal);
        $this->assertStringContainsString("test(event)", $modal);

        // nouvel argument 'buttons'
        $modal = $this->getModalFormHelper()
            ->create(
                'id-test',
                [
                    'buttons' => [
                        $Form->button('btn 1'),
                        $Form->button('btn 2'),
                        $this->getModalFormHelper()->getDefaultAcceptButton(),
                    ]
                ]
            )
            ->modal("Message en haut de la modale")
            ->javascriptCallback('callbackJavascript')
            ->output('function', 'funcToOpenModal', '/my-controller/my-action')
            ->generate();

        $this->assertStringContainsString("btn 1</button>", $modal);
        $this->assertStringContainsString("btn 2</button>", $modal);
        $this->assertStringContainsString(__('Enregistrer'), $modal);

        // ajout d'un bouton
        $modal = $this->getModalFormHelper()
            ->create('id-test');
        $modal->buttons = [
            $modal->buttons[0],
            $Form->button('my btn'),
            $modal->buttons[1],
        ];
        $modal
            ->modal("Message en haut de la modale")
            ->javascriptCallback('callbackJavascript')
            ->output('function', 'funcToOpenModal', '/my-controller/my-action')
            ->generate();
        $this->assertStringContainsString(__('Enregistrer'), $modal);
        $this->assertStringContainsString("my btn</button>", $modal);
    }
}
