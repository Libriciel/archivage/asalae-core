<?php

namespace AsalaeCore\Test\TestCase\View\Helper;

use AsalaeCore\View\Helper\FaHelper;
use AsalaeCore\TestSuite\TestCase;
use Cake\View\View;

class FaHelperTest extends TestCase
{
    /**
     * @var FaHelper
     */
    public $Fa;
    
    public function setUp(): void
    {
        parent::setUp();
        $this->Fa = new FaHelper(new View());
    }

    public function testI()
    {
        $expected = '<i class="fa fa-times" aria-hidden="true"></i>';
        $actual = $this->Fa->i('fa-times');
        $this->assertEquals($expected, $actual);

        $expected = '<i aria-hidden="false" class="fa fa-times"></i>';
        $actual = $this->Fa->i('fa-times', '', ['aria-hidden' => 'false']);
        $this->assertEquals($expected, $actual);

        $expected = '<i data-target="#test" class="fa fa-times fa-2x" aria-hidden="true"></i>';
        $actual = $this->Fa->i('fa-times fa-2x', '', ['data-target' => '#test']);
        $this->assertEquals($expected, $actual);

        $expected = '<i data-target="#test" class="fa fa-times fa-2x fa-space" aria-hidden="true"></i>Test';
        $actual = $this->Fa->i('fa-times fa-2x', 'Test', ['data-target' => '#test']);
        $this->assertEquals($expected, $actual);
    }

    public function testCharte()
    {
        $expected = '<i class="fa fa-folder-open" aria-hidden="true"></i>';
        $actual = $this->Fa->charte('Parcourir');
        $this->assertEquals($expected, $actual);
        $actual = $this->Fa->charte('Parcourir', 0);
        $this->assertEquals($expected, $actual);

        $expected = '<i class="fa fa-folder-open fa-space" aria-hidden="true"></i>Test';
        $actual = $this->Fa->charte('Parcourir', 'Test');
        $this->assertEquals($expected, $actual);
        $actual = $this->Fa->charte('Parcourir', 0, 'Test');
        $this->assertEquals($expected, $actual);

        $expected = '<i class="fa fa-folder-open-o" aria-hidden="true"></i>';
        $actual = $this->Fa->charte('Parcourir', 1);
        $this->assertEquals($expected, $actual);
        $expected = '<i class="fa fa-folder-open-o fa-space" aria-hidden="true"></i>Test';
        $actual = $this->Fa->charte('Parcourir', 1, 'Test');
        $this->assertEquals($expected, $actual);

        $expected = '<i class="fa fa-folder-open fa-2x" aria-hidden="true"></i>';
        $actual = $this->Fa->charte('Parcourir', ['class' => 'fa-2x']);
        $this->assertEquals($expected, $actual);
        $actual = $this->Fa->charte('Parcourir', ['icon' => 'fa-2x']);
        $this->assertEquals($expected, $actual);
        $actual = $this->Fa->charte('Parcourir', '', 'fa-2x');
        $this->assertEquals($expected, $actual);
        $expected = '<i class="fa fa-folder-open fa-2x fa-space" aria-hidden="true"></i>Test';
        $actual = $this->Fa->charte('Parcourir', 'Test', ['class' => 'fa-2x']);
        $this->assertEquals($expected, $actual);
        $actual = $this->Fa->charte('Parcourir', 'Test', ['icon' => 'fa-2x']);
        $this->assertEquals($expected, $actual);
        $actual = $this->Fa->charte('Parcourir', 'Test', 'fa-2x');
        $this->assertEquals($expected, $actual);

        $expected = '<i class="fa fa-folder-open-o fa-2x text-warning" aria-hidden="true"></i>';
        $actual = $this->Fa->charte('Parcourir', 1, ['class' => 'fa-2x text-warning']);
        $this->assertEquals($expected, $actual);
        $actual = $this->Fa->charte('Parcourir', 1, ['icon' => 'fa-2x text-warning']);
        $this->assertEquals($expected, $actual);
        $actual = $this->Fa->charte('Parcourir', 1, '', 'fa-2x text-warning');
        $this->assertEquals($expected, $actual);
        $expected = '<i class="fa fa-folder-open-o fa-2x text-warning fa-space" aria-hidden="true"></i>Test';
        $actual = $this->Fa->charte('Parcourir', 1, 'Test', ['class' => 'fa-2x text-warning']);
        $this->assertEquals($expected, $actual);
        $actual = $this->Fa->charte('Parcourir', 1, 'Test', ['icon' => 'fa-2x text-warning']);
        $this->assertEquals($expected, $actual);
        $actual = $this->Fa->charte('Parcourir', 1, 'Test', 'fa-2x text-warning');
        $this->assertEquals($expected, $actual);
    }
    
    public function testButton()
    {
        $expected = '<button type="button" class="btn-link" title="Test title"><i class="fa fa-times" aria-hidden="true"></i><span class="sr-only">Test title</span></button>';
        $actual = $this->Fa->button('fa-times', 'Test title');
        $this->assertEquals($expected, $actual);

        $expected = '<button id="foo" onclick="bar(\'#foo\')" type="button" class="btn-link" title="Test title"><i class="fa fa-times text-danger" aria-hidden="true"></i><span class="sr-only">Test title</span></button>';
        $actual = $this->Fa->button('fa-times text-danger', 'Test title', ['id' => 'foo', 'onclick' => "bar('#foo')"]);
        $this->assertEquals($expected, $actual);

        $expected = '<button type="button" class="btn-link" title="Test title"><i class="fa fa-times" aria-hidden="true"></i>foo bar</button>';
        $actual = $this->Fa->button('fa-times', 'Test title', ['srOnly' => 'foo', 'text' => " bar"]);
        $this->assertEquals($expected, $actual);
    }

    public function testCharteBtn()
    {
        // Tests de cohérence avec la fonction charte()
        $expected = '<button type="button" class="btn-link" title="Test title"><i class="fa fa-folder-open" aria-hidden="true"></i><span class="sr-only">Test title</span></button>';
        $actual = $this->Fa->charteBtn('Parcourir', 'Test title');
        $this->assertEquals($expected, $actual);
        $actual = $this->Fa->charteBtn('Parcourir', 'Test title', 0);
        $this->assertEquals($expected, $actual);

        $expected = '<button type="button" class="btn-link" title="Test title"><i class="fa fa-folder-open-o" aria-hidden="true"></i><span class="sr-only">Test title</span></button>';
        $actual = $this->Fa->charteBtn('Parcourir', 'Test title', 1);
        $this->assertEquals($expected, $actual);

        $expected = '<button type="button" class="btn-link" title="Test title"><i class="fa fa-folder-open fa-2x" aria-hidden="true"></i><span class="sr-only">Test title</span></button>';
        $actual = $this->Fa->charteBtn('Parcourir', 'Test title', ['icon' => 'fa-2x']);
        $this->assertEquals($expected, $actual);

        $expected = '<button type="button" class="btn-link" title="Test title"><i class="fa fa-folder-open-o fa-2x" aria-hidden="true"></i><span class="sr-only">Test title</span></button>';
        $actual = $this->Fa->charteBtn('Parcourir', 'Test title', 1, ['icon' => 'fa-2x']);
        $this->assertEquals($expected, $actual);

        // Tests de cohérence avec la fonction button()
        $expected = '<button id="foo" onclick="bar(\'#foo\')" type="button" class="btn-link" title="Test title"><i class="fa fa-folder-open" aria-hidden="true"></i><span class="sr-only">Test title</span></button>';
        $actual = $this->Fa->charteBtn('Parcourir', 'Test title', ['id' => 'foo', 'onclick' => "bar('#foo')"]);
        $this->assertEquals($expected, $actual);

        $expected = '<button id="foo" onclick="bar(\'#foo\')" type="button" class="btn-link" title="Test title"><i class="fa fa-folder-open-o fa-2x" aria-hidden="true"></i><span class="sr-only">Test title</span></button>';
        $actual = $this->Fa->charteBtn('Parcourir', 'Test title', 1, ['id' => 'foo', 'onclick' => "bar('#foo')", 'icon' => 'fa-2x']);
        $this->assertEquals($expected, $actual);

        $expected = '<button type="button" class="btn-link" title="Test title"><i class="fa fa-folder-open" aria-hidden="true"></i>foo bar</button>';
        $actual = $this->Fa->charteBtn('Parcourir', 'Test title', ['srOnly' => 'foo', 'text' => " bar"]);
        $this->assertEquals($expected, $actual);
    }
}
