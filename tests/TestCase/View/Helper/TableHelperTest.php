<?php

namespace AsalaeCore\Test\TestCase\View\Helper;

use AsalaeCore\View\Helper\TableHelper;
use AsalaeCore\TestSuite\TestCase;
use Cake\View\View;

/**
 * AsalaeCore\View\Helper\ViewTableHelper Test Case
 */
class TableHelperTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \AsalaeCore\View\Helper\TableHelper
     */
    public $TableHelper;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $view = new View();
        $this->TableHelper = new TableHelper($view);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->TableHelper);

        parent::tearDown();
    }

    public function testGetJsTableObject()
    {
        $jsTableObject = $this->TableHelper->getJsTableObject('foo bar');
        $this->assertTextContains('FooBar', $jsTableObject);
    }

    public function testTable()
    {
        $table = $this->TableHelper->create('foo')
            ->fields(
                [
                    'bar' => 'baz'
                ]
            )
            ->params(
                $params = [
                    'identifier' => 'id',
                ]
            )
            ->actions(
                [
                    [
                        'onclick' => "actionVisualiser({0})",
                        'type' => "button",
                        'class' => 'btn-link',
                        'label' => __('Visualiser'),
                        'title' => __("Visualiser {0}", '{1}'),
                        'aria-label' => __("Visualiser {0}", '{1}'),
                        'params' => ['id']
                    ]
                ]
            );
        $html = $table->generate();

        $this->assertTextContains('id="foo"', $html);
        $this->assertTextContains('"onclick": "actionVisualiser({0})",', $html);
        $this->assertTextContains('data-target="#modal-foo"', $table->getConfigureLink());

        $paramsJson = $table->getParamsJson();

        $this->assertEquals($params, (array)json_decode($paramsJson)->tbody);
    }
}
