<?php

namespace AsalaeCore\Test\TestCase\View\Helper;

use AsalaeCore\Http\AppServerRequest;
use AsalaeCore\View\Helper\FilterHelper;
use AsalaeCore\TestSuite\TestCase;
use Cake\Core\Configure;
use Cake\View\View;

class FilterHelperTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
        Configure::write('App.defaultLocale', 'fr_FR');
    }

    private function getFilterHelper(array $queryParams = []): FilterHelper
    {
        return new FilterHelper(
            new View(
                (new AppServerRequest)->withQueryParams($queryParams)
            )
        );
    }

    public function testGenerateLoadedFilterContent()
    {
        $Filter = $this->getFilterHelper()
            ->create('test')
            ->filter('test', ['label' => 'foo']);

        // Pas de filtre
        $result = $Filter->generateLoadedFilterContent();
        $this->assertEmpty($result);

        // Filtre simple
        $Filter = $this->getFilterHelper(['test' => 'bar'])
            ->create('test')
            ->filter('test', ['label' => 'foo']);
        $result = $Filter->generateLoadedFilterContent();
        $this->assertStringContainsString(
            '<input type="text" name="test[0]"',
            $result
        );

        $Filter = $this->getFilterHelper(
            ['created' => '01/01/2020', 'dateoperator_created' => '<=']
        )
            ->create('test')
            ->filter(
                'created',
                ['label' => 'created', 'type' => 'date']
            );
        $result = $Filter->generateLoadedFilterContent();
        $this->assertStringContainsString(
            '01/01/2020',
            $result
        );
        $this->assertStringContainsString(
            '<input type="text" name="created[0]"',
            $result
        );
        $this->assertStringContainsString(
            '<select name="dateoperator_created[0]"',
            $result
        );
    }
}
