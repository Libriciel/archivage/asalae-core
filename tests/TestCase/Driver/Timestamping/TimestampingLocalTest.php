<?php

namespace AsalaeCore\Test\TestCase\Driver\Timestamping;

use AsalaeCore\Driver\Timestamping\TimestampingLocal;
use AsalaeCore\TestSuite\InvokePrivateTrait;
use AsalaeCore\TestSuite\TestCase;
use InvalidArgumentException;

class TimestampingLocalTest extends TestCase
{
    use InvokePrivateTrait;

    const CA_FILE = TESTS . 'Data' . DS . 'certs' . DS . 'root_ca.crt';
    const CRT_FILE = TESTS . 'Data' . DS . 'certs' . DS . 'ts.crt';
    const PEM_FILE = TESTS . 'Data' . DS . 'keys' . DS . 'ts_pkey.pem';
    const CNF_FILE = TESTS . 'Data' . DS . 'timestamping.cnf';
    const SERIAL_FILE = TESTS . 'Data' . DS . 'tsaserial';
    const TOKENIZE_FILE = TESTS . 'Data' . DS . 'logo.png';
    const TOKEN_SAMPLE = TESTS . 'Data' . DS . 'sample.token';

    private $toDelete = [];

    public function testPing()
    {
        $timestamper = $this->getTimestamper();
        $this->assertTrue($timestamper->ping());
    }

    public function testCheck()
    {
        $timestamper = $this->getTimestamper();
        $this->assertTrue($timestamper->check());
    }

    public function tearDown(): void
    {
        parent::tearDown();
        foreach ($this->toDelete as $file) {
            if (is_file($file)) {
                unlink($file);
            }
        }
        if (is_file(self::SERIAL_FILE)) {
            unlink(self::SERIAL_FILE);
        }
    }

    public function testGenerateToken()
    {
        $timestamper = $this->getTimestamper();
        $token = $timestamper->generateToken(self::TOKENIZE_FILE);
        $badToken = file_get_contents(self::TOKEN_SAMPLE);
        $this->assertTrue($timestamper->verifyTsr(self::TOKENIZE_FILE, $token));
        $this->assertFalse($timestamper->verifyTsr(self::TOKENIZE_FILE, $badToken));
    }

    private function getTimestamper(): TimestampingLocal
    {
        $cnf = file_get_contents(self::CNF_FILE);
        $cnf = str_replace('{{tmp_dir}}', dirname(self::CNF_FILE), $cnf);
        $tmpFile = tempnam(sys_get_temp_dir(), 'testunit-');
        $this->toDelete[] = $tmpFile;
        $this->assertNotFalse(file_put_contents($tmpFile, $cnf));
        return new TimestampingLocal(
            self::CA_FILE,
            self::CRT_FILE,
            self::PEM_FILE,
            $tmpFile
        );
    }

    public function testGetFileSha1()
    {
        $timestamper = $this->getTimestamper();

        // resource
        $tempfile = tmpfile();
        fwrite($tempfile, 'test');
        $hash = $this->invokeMethod($timestamper, 'getFileSha1', [$tempfile]);
        fclose($tempfile);
        $this->assertEquals(sha1('test'), $hash); // NOSONAR

        // filename
        $tempfile = tempnam(sys_get_temp_dir(), 'test-');
        file_put_contents($tempfile, 'test');
        $hash = $this->invokeMethod($timestamper, 'getFileSha1', [$tempfile]);
        unlink($tempfile);
        $this->assertEquals(sha1('test'), $hash); // NOSONAR

        $this->expectException(InvalidArgumentException::class);
        $this->invokeMethod($timestamper, 'getFileSha1', ['foo']);
    }
}
