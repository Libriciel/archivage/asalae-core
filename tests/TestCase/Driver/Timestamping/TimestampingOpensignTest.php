<?php

namespace AsalaeCore\Test\TestCase\Driver\Timestamping;

use AsalaeCore\Driver\Timestamping\TimestampingOpensign;
use AsalaeCore\Test\Mock\SoapClientMocked;
use AsalaeCore\TestSuite\InvokePrivateTrait;
use AsalaeCore\TestSuite\TestCase;
use Exception;
use InvalidArgumentException;
use PHPUnit\Framework\MockObject\MockObject;

class TimestampingOpensignTest extends TestCase
{
    use InvokePrivateTrait;

    const TOKEN_SAMPLE = TESTS . 'Data' . DS . 'sample.token';
    const TOKENIZE_FILE = TESTS . 'Data' . DS . 'logo.png';

    public function setUp(): void
    {
        parent::setUp();
    }

    public function testPing()
    {
        $timestamper = new TimestampingOpensign('http://fake.url');
        $timestamper->soapClient = SoapClientMocked::class;
        $this->assertTrue($timestamper->ping());
    }

    public function testCheck()
    {
        $timestamper = new TimestampingOpensign('http://fake.url');
        $timestamper->soapClient = SoapClientMocked::class;
        $this->assertTrue($timestamper->check());
    }

    public function testGetFileSha1()
    {
        $timestamper = new TimestampingOpensign('http://fake.url');

        // resource
        $tempfile = tmpfile();
        fwrite($tempfile, 'test');
        $hash = $this->invokeMethod($timestamper, 'getFileSha1', [$tempfile]);
        fclose($tempfile);
        $this->assertEquals(sha1('test'), $hash); // NOSONAR

        // filename
        $tempfile = tempnam(sys_get_temp_dir(), 'test-');
        file_put_contents($tempfile, 'test');
        $hash = $this->invokeMethod($timestamper, 'getFileSha1', [$tempfile]);
        unlink($tempfile);
        $this->assertEquals(sha1('test'), $hash); // NOSONAR

        $this->expectException(InvalidArgumentException::class);
        $this->invokeMethod($timestamper, 'getFileSha1', ['foo']);
    }

    public function testGenerateToken()
    {
        $timestamper = new TimestampingOpensign(
            'http://fake.url',
            true,
            'localhost',
            1245,
            'foo:bar'
        );
        $timestamper->soapClient = SoapClientMocked::class;
        $this->assertEquals(
            file_get_contents(self::TOKEN_SAMPLE),
            $timestamper->generateToken(self::TOKENIZE_FILE)
        );

        /** @var TimestampingOpensign|MockObject $timestamper */
        $timestamper = $this->getMockBuilder(TimestampingOpensign::class)
            ->setConstructorArgs(['http://fake.url'])
            ->setMethods(['getSoapClient'])
            ->getMock();
        $client = $this->createMock(SoapClientMocked::class);
        $client->method('__soapCall')->willReturn('KO');
        $timestamper->method('getSoapClient')->willReturn($client);
        $this->expectException(Exception::class);
        $timestamper->generateToken(self::TOKENIZE_FILE);
    }
}
