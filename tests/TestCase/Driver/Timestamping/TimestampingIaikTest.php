<?php

namespace AsalaeCore\Test\TestCase\Driver\Timestamping;

use AsalaeCore\Test\Mock\TimestampingIaikMocked;
use AsalaeCore\TestSuite\TestCase;

class TimestampingIaikTest extends TestCase
{
    const TOKEN_SAMPLE = TESTS . 'Data' . DS . 'sample.token';
    const TOKENIZE_FILE = TESTS . 'Data' . DS . 'logo.png';

    public function setUp(): void
    {
        parent::setUp();
    }

    public function testCheck()
    {
        $timestamper = new TimestampingIaikMocked('http://fake.url');
        $this->assertTrue($timestamper->check());
    }

    public function testGenerateToken()
    {
        $timestamper = new TimestampingIaikMocked('http://fake.url');
        $this->assertEquals(
            file_get_contents(self::TOKEN_SAMPLE),
            $timestamper->generateToken(self::TOKENIZE_FILE)
        );
    }
}
