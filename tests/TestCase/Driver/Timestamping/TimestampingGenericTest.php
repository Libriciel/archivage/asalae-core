<?php

namespace AsalaeCore\Test\TestCase\Driver\Timestamping;

use AsalaeCore\Driver\Timestamping\TimestampingGeneric;
use AsalaeCore\Test\Mock\FakeTrustedTimestampMock;
use AsalaeCore\TestSuite\InvokePrivateTrait;
use Cake\Http\Client;
use Cake\Http\Client\Response;
use AsalaeCore\TestSuite\TestCase;
use InvalidArgumentException;

class TimestampingGenericTest extends TestCase
{
    use InvokePrivateTrait;

    public function testPing()
    {
        $timestamping = new TimestampingGeneric('localhost');
        $timestamping->client = $this->createMock(Client::class);
        $timestamping->trustedTimestampClass = FakeTrustedTimestampMock::class;
        $response = $this->createMock(Response::class);
        $response->method('getStatusCode')->willReturn(200);
        $timestamping->client->method('head')->willReturn($response);
        $timestamping->client->method('head');
        $this->assertTrue($timestamping->ping());
    }

    public function testCheck()
    {
        $timestamping = new TimestampingGeneric('localhost');
        $timestamping->client = $this->createMock(Client::class);
        $timestamping->trustedTimestampClass = FakeTrustedTimestampMock::class;
        $response = $this->createMock(Response::class);
        $response->method('getStatusCode')->willReturn(200);
        $timestamping->client->method('head')->willReturn($response);
        $timestamping->client->method('head');
        $this->assertTrue($timestamping->check());
    }

    public function testGetFileHash()
    {
        // resource
        $timestamping = new TimestampingGeneric('localhost');
        $tempfile = tmpfile();
        fwrite($tempfile, 'test');
        $hash = $this->invokeMethod($timestamping, 'getFileHash', [$tempfile, 'md5']);
        fclose($tempfile);
        $this->assertEquals(md5('test'), $hash); // NOSONAR

        // filename
        $tempfile = tempnam(sys_get_temp_dir(), 'test-');
        file_put_contents($tempfile, 'test');
        $hash = $this->invokeMethod($timestamping, 'getFileHash', [$tempfile, 'md5']);
        unlink($tempfile);
        $this->assertEquals(md5('test'), $hash); // NOSONAR

        $this->expectException(InvalidArgumentException::class);
        $this->invokeMethod($timestamping, 'getFileHash', ['foo', 'md5']);
    }

    public function testGenerateToken()
    {
        $timestamping = new TimestampingGeneric(
            'localhost',
            '',
            '',
            '',
            '',
            true,
            'localhost'
        );
        $timestamping->client = $this->createMock(Client::class);
        $timestamping->trustedTimestampClass = FakeTrustedTimestampMock::class;
        $tempfile = tmpfile();
        fwrite($tempfile, 'test');
        $result = $timestamping->generateToken($tempfile);
        fclose($tempfile);
        $this->assertEquals('test', $result);
    }
}
