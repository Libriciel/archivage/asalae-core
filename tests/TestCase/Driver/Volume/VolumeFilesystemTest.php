<?php

namespace AsalaeCore\Test\TestCase\Driver\Volume;

use AsalaeCore\Driver\Volume\Exception\VolumeException;
use AsalaeCore\Driver\Volume\VolumeFilesystem;
use AsalaeCore\TestSuite\TestCase;
use Libriciel\Filesystem\Utility\Filesystem;

class VolumeFilesystemTest extends TestCase
{
    public function testPing()
    {
        $volume = new VolumeFilesystem(['path' => sys_get_temp_dir()]);
        $this->assertTrue($volume->ping());

        $this->expectException(VolumeException::class);
        new VolumeFilesystem(['path' => 'bad_uri']);
    }

    public function testIsEmpty()
    {
        $testUnitDirname = sys_get_temp_dir().DS.uniqid('testunit-');
        mkdir($testUnitDirname);
        $volume = new VolumeFilesystem(['path' => $testUnitDirname]);

        // test volume vide
        $this->assertTrue($volume->isEmpty());

        // test volume non vide
        $testUnitFileUri = tempnam($testUnitDirname, 'testunit-');
        $this->assertFalse($volume->isEmpty());

        unlink($testUnitFileUri);
        rmdir($testUnitDirname);

        // test volume non existant
        $this->assertFalse($volume->isEmpty());
    }

    public function testfileGetContent()
    {
        $fullPath = tempnam(sys_get_temp_dir(), 'testunit-');
        $file = basename($fullPath);
        file_put_contents($fullPath, 'foo');
        $volume = new VolumeFilesystem(['path' => sys_get_temp_dir()]);
        $this->assertEquals('foo', $volume->fileGetContent($file));
        unlink($fullPath);

        $e = null;
        try {
            $volume->fileGetContent('bad_ref');
        } catch (VolumeException $e) {
        }
        $this->assertInstanceOf(VolumeException::class, $e);
    }

    public function testFileDownload()
    {
        $fullPath = tempnam(sys_get_temp_dir(), 'testunit-');
        $file = basename($fullPath);
        file_put_contents($fullPath, 'foo');
        $volume = new VolumeFilesystem(['path' => sys_get_temp_dir()]);
        $volume->fileDownload($file, $fullPath.'-download');
        $this->assertFileExists($fullPath.'-download');
        $this->assertEquals('foo', file_get_contents($fullPath.'-download'));
        unlink($fullPath);
        unlink($fullPath.'-download');

        $this->expectException(VolumeException::class);
        $volume->fileDownload('not_exists', $fullPath.'-download');
    }

    public function testFileUpload()
    {
        $fullPath = tempnam(sys_get_temp_dir(), 'testunit-');
        $file = basename($fullPath);
        file_put_contents($fullPath, 'foo');
        $volume = new VolumeFilesystem(['path' => sys_get_temp_dir()]);
        $volume->fileUpload($fullPath, $file.'-upload');
        $this->assertFileExists($fullPath.'-upload');
        $this->assertEquals('foo', file_get_contents($fullPath.'-upload'));

        $volume->fileUpload($fullPath, $file.'-upload');
        $this->assertEquals('foo', file_get_contents($fullPath.'-upload'));

        file_put_contents($fullPath, 'bar');
        $e = null;
        try {
            $volume->fileUpload($fullPath, $file.'-upload');
        } catch (VolumeException $e) {
        }
        $this->assertInstanceOf(VolumeException::class, $e);

        unlink($fullPath);
        unlink($fullPath.'-upload');
    }

    public function testFilePutContent()
    {
        $fullPath = sys_get_temp_dir().DS.uniqid('testunit-');
        $file = basename($fullPath);
        $volume = new VolumeFilesystem(['path' => sys_get_temp_dir()]);
        $volume->filePutContent($file, 'foo');
        $this->assertEquals('foo', file_get_contents($fullPath));


        $volume->filePutContent($fullPath, 'foo');
        $this->assertEquals('foo', file_get_contents($fullPath));

        $e = null;
        try {
            $volume->filePutContent($fullPath, 'bar');
        } catch (VolumeException $e) {
        }
        $this->assertInstanceOf(VolumeException::class, $e);

        unlink($fullPath);
    }

    public function testFileDelete()
    {
        $fullPath = tempnam(sys_get_temp_dir(), 'testunit-');
        $file = basename($fullPath);
        file_put_contents($fullPath, 'foo');
        $volume = new VolumeFilesystem(['path' => sys_get_temp_dir()]);
        $volume->fileDelete($file);
        $this->assertFileDoesNotExist($fullPath);

        // suppression des dossiers intermédiaires
        $testdir = sys_get_temp_dir().DS.'testdir';
        if (is_dir($testdir)) {
            Filesystem::remove($testdir);
        }
        mkdir($testdir);
        $fullPath = tempnam($testdir, 'testunit-');
        $file = 'testdir/'.basename($fullPath);
        file_put_contents($fullPath, 'foo');
        $volume->fileDelete($file);
        $this->assertFileDoesNotExist($fullPath);
        $this->assertDirectoryDoesNotExist($testdir);

        // on ne doit pas supprimer le dossier du volume
        mkdir($testdir);
        file_put_contents($fullPath, 'foo');
        $volume = new VolumeFilesystem(['path' => $testdir]);
        $volume->fileDelete(basename($fullPath));
        $this->assertFileDoesNotExist($fullPath);
        $this->assertDirectoryExists($testdir);

        $this->expectException(VolumeException::class);
        $volume->fileDelete(basename($fullPath));
    }

    public function testFileExists()
    {
        $fullPath = tempnam(sys_get_temp_dir(), 'testunit-');
        $file = basename($fullPath);
        file_put_contents($fullPath, 'foo');
        $volume = new VolumeFilesystem(['path' => sys_get_temp_dir()]);
        $this->assertTrue($volume->fileExists($file));
        unlink($fullPath);
    }

    public function testRename()
    {
        $fullPath = tempnam(sys_get_temp_dir(), 'testunit-');
        $file = basename($fullPath);
        file_put_contents($fullPath, 'foo');
        $volume = new VolumeFilesystem(['path' => sys_get_temp_dir()]);
        $volume->rename($file, $file.'.bak');
        $this->assertFileDoesNotExist($fullPath);
        $this->assertFileExists($fullPath.'.bak');
        $this->assertStringContainsString('foo', file_get_contents($fullPath.'.bak'));
        unlink($fullPath.'.bak');

        $this->expectException(VolumeException::class);
        $volume->rename($file, $file.'.bak');
    }

    public function testCopy()
    {
        $fullPath = tempnam(sys_get_temp_dir(), 'testunit-');
        $file = basename($fullPath);
        file_put_contents($fullPath, 'foo');
        $volume = new VolumeFilesystem(['path' => sys_get_temp_dir()]);
        $volume->copy($file, $file.'.bak');
        $this->assertFileExists($fullPath);
        $this->assertFileExists($fullPath.'.bak');
        $this->assertStringContainsString('foo', file_get_contents($fullPath.'.bak'));
        unlink($fullPath);
        unlink($fullPath.'.bak');

        $this->expectException(VolumeException::class);
        $volume->copy($file, $file.'.bak');
    }

    public function testReadfile()
    {
        $fullPath = tempnam(sys_get_temp_dir(), 'testunit-');
        $file = basename($fullPath);
        file_put_contents($fullPath, 'foo');
        $volume = new VolumeFilesystem(['path' => sys_get_temp_dir()]);
        ob_start();
        $volume->readfile($file);
        $content = ob_get_clean();
        $this->assertEquals('foo', $content);
        unlink($fullPath);

        $this->expectException(VolumeException::class);
        $volume->readfile('not_exists');
    }

    public function testHash()
    {
        $fullPath = tempnam(sys_get_temp_dir(), 'testunit-');
        $file = basename($fullPath);
        file_put_contents($fullPath, 'foo');
        $volume = new VolumeFilesystem(['path' => sys_get_temp_dir()]);
        $hash = $volume->hash($file);
        $this->assertEquals(hash('sha256', 'foo'), $hash);
        unlink($fullPath);

        $this->expectException(VolumeException::class);
        $volume->hash('not_exists');
    }

    public function testLs()
    {
        $fullPath = tempnam(sys_get_temp_dir(), 'testunit-');
        $file = basename($fullPath);
        file_put_contents($fullPath, 'foo');
        $volume = new VolumeFilesystem(['path' => sys_get_temp_dir()]);
        $ls = $volume->ls();
        $this->assertStringContainsString($file, implode('', $ls));
        $ls = $volume->ls('', true);
        $this->assertStringContainsString($file, implode('', $ls));
        $ls = $volume->ls($file);
        $this->assertStringContainsString($file, implode('', $ls));
        unlink($fullPath);

        $this->expectException(VolumeException::class);
        $volume->ls('not_exists');
    }

    public function testMetadata()
    {
        $fullPath = tempnam(sys_get_temp_dir(), 'testunit-');
        $file = basename($fullPath);
        file_put_contents($fullPath, 'foo');
        $volume = new VolumeFilesystem(['path' => sys_get_temp_dir()]);
        $meta = $volume->metadata($file);
        unlink($fullPath);
        $this->assertArrayHasKey('mime', $meta);
        $this->assertArrayHasKey('size', $meta);
        $this->assertArrayHasKey('modified', $meta);

        $this->expectException(VolumeException::class);
        $volume->metadata('not_exists');
    }

    public function testStreamTo()
    {
        $dir1 = sys_get_temp_dir().DS.uniqid('testunit-');
        $dir2 = sys_get_temp_dir().DS.uniqid('testunit-');
        if (!is_dir($dir1)) {
            mkdir($dir1);
        }
        if (!is_dir($dir2)) {
            mkdir($dir2);
        }
        $fullPath1 = $dir1.DS.'file.txt';
        $fullPath2 = $dir2.DS.'file.txt';
        $file = 'file.txt';
        file_put_contents($fullPath1, 'foo');
        $volume1 = new VolumeFilesystem(['path' => $dir1]);
        $volume2 = new VolumeFilesystem(['path' => $dir2]);

        $volume1->streamTo($file, $volume2);
        $this->assertFileExists($fullPath2);
        $this->assertEquals('foo', file_get_contents($fullPath2));

        $volume2->fileDelete($file);
        $volume2->filePutContent($file, 'fichier différent + streamTo = exception');
        $e = null;
        try {
            $volume1->streamTo($file, $volume2);
        } catch (VolumeException $e) {
        }
        $this->assertInstanceOf(VolumeException::class, $e);

        unlink($fullPath1);
        unlink($fullPath2);
        rmdir($dir1);
        rmdir($dir2);

        $this->expectException(VolumeException::class);
        $volume1->streamTo('not_exists', $volume2);
    }

    public function testTest()
    {
        $volume = new VolumeFilesystem(['path' => sys_get_temp_dir()]);
        $tests = $volume->test();
        $this->assertArrayHasKey('success', $tests);
        $this->assertTrue($tests['success']);
    }
}
