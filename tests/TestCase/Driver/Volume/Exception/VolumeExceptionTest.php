<?php

namespace AsalaeCore\Test\TestCase\Driver\Volume\Exception;

use AsalaeCore\Driver\Volume\Exception\VolumeException;
use Cake\ORM\Entity;
use AsalaeCore\TestSuite\TestCase;

class VolumeExceptionTest extends TestCase
{
    public function testCreate()
    {
        for ($i = 1; $i < 10; $i++) {
            $exception = new VolumeException($i);
            $this->assertNotEquals($i, $exception->getMessage());
        }
        $exception = new VolumeException('message');
        $this->assertEquals('message', $exception->getMessage());
    }

    public function testWrap()
    {
        $volume = new Entity(['id' => 1, 'name' => 'test-wrap']);
        $exception = new VolumeException(VolumeException::FILE_ALREADY_EXISTS);
        $exception->wrap($volume, '/tmp/testfile');
        $this->assertTextContains('test-wrap', $exception->getMessage());
    }
}
