<?php

namespace AsalaeCore\Test\TestCase\Driver\Volume;

use AsalaeCore\Driver\Volume\Exception\VolumeException;
use AsalaeCore\Driver\Volume\VolumeFilesystem;
use AsalaeCore\Driver\Volume\VolumeS3;
use AsalaeCore\TestSuite\InvokePrivateTrait;
use AsalaeCore\TestSuite\TestCase;
use Aws\Command;
use Aws\CommandInterface;
use Aws\HandlerList;
use Aws\Result;
use Aws\S3\Exception\S3Exception;
use Aws\S3\S3Client;
use DateTime;
use Exception;
use GuzzleHttp\Psr7\Stream;

class VolumeS3Test extends TestCase
{
    use InvokePrivateTrait;

    const TEST_FILE_KEY = 'testFichier.txt';

    private function getVolume($client): VolumeS3
    {
        return new VolumeS3(
            [
                'endpoint' => 'http://http://minio.dev.libriciel.fr:9001',
                'bucket' => 'volume01',
                'credentials_key' => 'fveyres',
                'credentials_secret' => 'fveyresfveyres',
                'client' => $client,
            ]
        );
    }

    public function testPing()
    {
        // test d'existence du bucket
        $S3Client = $this->createMock(S3Client::class);
        $S3Client->method('doesBucketExist')->willReturn(true);
        $volume = $this->getVolume($S3Client);
        $this->assertTrue($volume->ping());

        // test de non existence du bucket
        $S3Client = $this->createMock(S3Client::class);
        $S3Client->method('doesBucketExist')->willReturn(true, false);
        $volume = $this->getVolume($S3Client);
        $this->assertFalse($volume->ping());

        // test exception d'existence du bucket
        /** @var CommandInterface $command */
        $command = $this->createMock(CommandInterface::class);
        $S3Client = $this->createMock(S3Client::class);
        $S3Client->method('doesBucketExist')->willReturn(true);
        $volume = $this->getVolume($S3Client);
        $S3Client = $this->createMock(S3Client::class);
        $S3Client->method('doesBucketExist')->willThrowException(new S3Exception('foo', $command));
        $this->invokeProperty($volume, 'client', 'set', $S3Client);
        $this->assertFalse($volume->ping());
    }

    public function testIsEmpty()
    {
        // test volume vide
        $response = $this->createMock(Result::class);
        $response->method('get')->willReturn(null);
        $S3Client = $this->createMock(S3Client::class);
        $S3Client->method('doesBucketExist')->willReturn(true);
        $S3Client->method('__call')->willReturn($response); // getObject
        $volume = $this->getVolume($S3Client);
        $this->assertTrue($volume->isEmpty());

        // test volume non vide
        $response = $this->createMock(Result::class);
        $response->method('get')->willReturn('contents_not_null');
        $S3Client = $this->createMock(S3Client::class);
        $S3Client->method('__call')->willReturn($response); // getObject
        $S3Client->method('doesBucketExist')->willReturn(true);
        $volume = $this->getVolume($S3Client);
        $this->assertFalse($volume->isEmpty());

        // exception ; erreur fonction listObjects
        /** @var CommandInterface $command */
        $command = $this->createMock(CommandInterface::class);
        $S3Client = $this->createMock(S3Client::class);
        $S3Client->method('doesBucketExist')->willReturn(true);
        $S3Client->method('__call')->willThrowException(new S3Exception('foo', $command));
        $volume = $this->getVolume($S3Client);
        $this->assertFalse($volume->isEmpty());
    }

    public function testFilePutContent()
    {
        // test retour de la fonction
        $S3Client = $this->createMock(S3Client::class);
        $S3Client->method('doesBucketExist')->willReturn(true);
        $S3Client->method('doesObjectExist')->willReturn(false);
        $volume = $this->getVolume($S3Client);
        $this->assertEquals(self::TEST_FILE_KEY, $volume->filePutContent(self::TEST_FILE_KEY, 'contentFichier'));

        // exception : fichier existe déjà dans Minio
        $handle = tmpfile();
        fwrite($handle, $content = 'contentFichier');
        rewind($handle);
        $response = $this->createMock(Result::class);
        $response->method('get')->willReturn(strlen($content), new Stream($handle));
        $S3Client = $this->createMock(S3Client::class);
        $S3Client->method('doesBucketExist')->willReturn(true);
        $S3Client->method('doesObjectExist')->willReturn(true);
        $S3Client->method('__call')->willReturn($response); // getObject
        $volume = $this->getVolume($S3Client);
        try {
            @$volume->filePutContent(self::TEST_FILE_KEY, 'contentFichier');
            // TODO filePutContent sur fichier différent = exception
        } catch (Exception $e) {
            $this->assertTrue($e instanceof VolumeException, $e->getMessage());
        }

        // exception : erreur dans la fonction putObject
        /** @var CommandInterface $command */
        $command = $this->createMock(CommandInterface::class);
        $S3Client = $this->createMock(S3Client::class);
        $S3Client->method('doesObjectExist')->willReturn(false);
        $S3Client->method('doesBucketExist')->willReturn(true);
        $S3Client->method('__call')->willThrowException(new S3Exception('foo', $command));
        $volume = $this->getVolume($S3Client);
        try {
            $volume->filePutContent(self::TEST_FILE_KEY, 'contentFichier');
        } catch (Exception $e) {
            $this->assertTrue($e instanceof VolumeException, $e->getMessage());
        }
    }

    public function testFileGetContent()
    {
        // test de lecture
        $guzzlehttp = $this->createMock(\GuzzleHttp\Psr7\Stream::class);
        $guzzlehttp->method('getContents')->willReturn('contentFichier');
        $response = $this->createMock(Result::class);
        $response->method('get')->willReturn($guzzlehttp);
        $S3Client = $this->createMock(S3Client::class);
        $S3Client->method('doesBucketExist')->willReturn(true);
        $S3Client->method('doesObjectExist')->willReturn(true);
        $S3Client->method('__call')->willReturn($response); // getObject
        $volume = $this->getVolume($S3Client);
        $this->assertEquals('contentFichier', $volume->fileGetContent(self::TEST_FILE_KEY));

        // exception ; fichier non existant
        $S3Client = $this->createMock(S3Client::class);
        $S3Client->method('doesObjectExist')->willReturn(false);
        $S3Client->method('doesBucketExist')->willReturn(true);
        $volume = $this->getVolume($S3Client);
        try {
            $volume->fileGetContent(self::TEST_FILE_KEY.'NON_EXISTANT');
        } catch (Exception $e) {
            $this->assertTrue($e instanceof VolumeException, $e->getMessage());
        }

        // exception ; erreur fonction getObject
        /** @var CommandInterface $command */
        $command = $this->createMock(CommandInterface::class);
        $S3Client = $this->createMock(S3Client::class);
        $S3Client->method('doesObjectExist')->willReturn(true);
        $S3Client->method('doesBucketExist')->willReturn(true);
        $S3Client->method('__call')->willThrowException(new S3Exception('foo', $command));
        $volume = $this->getVolume($S3Client);
        try {
            $volume->fileGetContent(self::TEST_FILE_KEY);
        } catch (Exception $e) {
            $this->assertTrue($e instanceof VolumeException, $e->getMessage());
        }
    }

    public function testCompareFile()
    {
        $handle = tmpfile();
        fwrite($handle, 'foo');
        rewind($handle);
        $response = $this->createMock(Result::class);
        $response->method('get')->willReturnOnConsecutiveCalls(1, new Stream($handle));
        $S3Client = $this->createMock(S3Client::class);
        $S3Client->method('doesBucketExist')->willReturn(true);
        $S3Client->method('doesObjectExist')->willReturn(true);
        $S3Client->method('__call')->willReturn($response); // getObject
        $volume = $this->getVolume($S3Client);
        $this->assertTrue(
            $volume->compareFile(
                self::TEST_FILE_KEY,
                1,
                hash('crc32', 'foo'),
                'crc32'
            )
        );
        $this->assertFalse(
            $volume->compareFile(
                self::TEST_FILE_KEY,
                12345678913,
                'bar',
                'crc32'
            )
        );
    }

    public function testFileUpload()
    {
        // upload d'un fichier
        $fullPath = tempnam(sys_get_temp_dir(), 'testunit-');
        file_put_contents($fullPath, 'foo');
        $handler = $this->createMock(HandlerList::class);
        $handler->method('appendSign')->willReturnSelf();
        $command = $this->createMock(Command::class);
        $command->method('getHandlerList')->willReturn($handler);
        $S3Client = $this->createMock(S3Client::class);
        $S3Client->method('doesObjectExist')->willReturn(false);
        $S3Client->method('doesBucketExist')->willReturn(true);
        $S3Client->method('getCommand')->willReturn($command);
        $volume = $this->getVolume($S3Client);
        $this->assertEquals(self::TEST_FILE_KEY, $volume->fileUpload($fullPath, self::TEST_FILE_KEY));
        unlink($fullPath);

        // exception : fichier existe déjà dans Minio
        $response = $this->createMock(Result::class);
        $response->method('get')->willReturn(null);
        $S3Client = $this->createMock(S3Client::class);
        $S3Client->method('doesBucketExist')->willReturn(true);
        $S3Client->method('doesObjectExist')->willReturn(true);
        $S3Client->method('__call')->willReturn($response); // getObject
        $volume = $this->getVolume($S3Client);
        try {
            @$volume->fileUpload($fullPath, self::TEST_FILE_KEY);
            // TODO fileUpload sur fichier différent = exception
        } catch (Exception $e) {
            $this->assertTrue($e instanceof VolumeException, $e->getMessage());
        }

        // exception : fichier à uploader non existant
        $S3Client = $this->createMock(S3Client::class);
        $S3Client->method('doesObjectExist')->willReturn(false);
        $S3Client->method('doesBucketExist')->willReturn(true);
        $volume = $this->getVolume($S3Client);
        try {
            $volume->fileUpload('/no/file/for/the/test.txt', self::TEST_FILE_KEY);
        } catch (Exception $e) {
            $this->assertTrue($e instanceof VolumeException, $e->getMessage());
        }

        // exception : erreur de la fonction putObject
        /** @var CommandInterface $command */
        $command = $this->createMock(CommandInterface::class);
        $S3Client = $this->createMock(S3Client::class);
        $S3Client->method('doesObjectExist')->willReturn(false);
        $S3Client->method('doesBucketExist')->willReturn(true);
        $S3Client->method('__call')->willThrowException(new S3Exception('foo', $command));
        $volume = $this->getVolume($S3Client);
        try {
            $volume->fileUpload($fullPath, self::TEST_FILE_KEY);
        } catch (Exception $e) {
            $this->assertTrue($e instanceof VolumeException, $e->getMessage());
        }
    }

    /** @noinspection PhpUnusedParameterInspection */
    public function testFileDownload()
    {
        // test de lecture
        $fullPathDownloaded = sys_get_temp_dir() . DS . uniqid('testunit-');
        $S3Client = $this->createMock(S3Client::class);
        $S3Client->method('doesObjectExist')->willReturn(true);
        $S3Client->method('doesBucketExist')->willReturn(true);

        $handle = tmpfile();
        fwrite($handle, 'file_content');
        rewind($handle);
        $response = $this->createMock(Result::class);
        $response->method('get')->willReturn(new Stream($handle));
        $S3Client->method('__call')
            ->with(
                'getObject',
                [
                    [
                        'Bucket' => 'volume01',
                        'Key' => 'testFichier.txt'
                    ]
                ]
            )
            ->willReturnCallback(
                function ($functionName, $params) use ($response) {
                    return $response;
                }
            ); // getObject
        $volume = $this->getVolume($S3Client);
        $volume->fileDownload(self::TEST_FILE_KEY, $fullPathDownloaded);
        $this->assertEquals(file_get_contents($fullPathDownloaded), 'file_content');

        // test exception d'un fichier non existant dans Minio
        $S3Client = $this->createMock(S3Client::class);
        $S3Client->method('doesObjectExist')->willReturn(false);
        $S3Client->method('doesBucketExist')->willReturn(true);
        $volume = $this->getVolume($S3Client);
        try {
            $volume->fileDownload(self::TEST_FILE_KEY.'NON_EXISTANT', $fullPathDownloaded);
        } catch (Exception $e) {
            $this->assertTrue($e instanceof VolumeException, $e->getMessage());
        }

        // test exception d'un fichier existant sur filesystem
        $S3Client = $this->createMock(S3Client::class);
        $S3Client->method('doesObjectExist')->willReturn(true);
        $S3Client->method('doesBucketExist')->willReturn(true);
        $volume = $this->getVolume($S3Client);
        try {
            $volume->fileDownload(self::TEST_FILE_KEY, $fullPathDownloaded);
        } catch (Exception $e) {
            $this->assertTrue($e instanceof VolumeException, $e->getMessage());
        }
        unlink($fullPathDownloaded);

        // test exception erreur de la fonction getObject
        /** @var CommandInterface $command */
        $command = $this->createMock(CommandInterface::class);
        $S3Client = $this->createMock(S3Client::class);
        $S3Client->method('doesObjectExist')->willReturn(true);
        $S3Client->method('doesBucketExist')->willReturn(true);
        $S3Client->method('__call')->willThrowException(new S3Exception('foo', $command));
        $volume = $this->getVolume($S3Client);
        try {
            $volume->fileDownload(self::TEST_FILE_KEY, $fullPathDownloaded);
        } catch (Exception $e) {
            $this->assertTrue($e instanceof VolumeException, $e->getMessage());
        }
    }

    public function testFileDelete()
    {
        // test exception d'un fichier non existant dans Minio
        $S3Client = $this->createMock(S3Client::class);
        $S3Client->method('doesObjectExist')->willReturn(false);
        $S3Client->method('doesBucketExist')->willReturn(true);
        $volume = $this->getVolume($S3Client);
        try {
            $volume->fileDelete(self::TEST_FILE_KEY);
        } catch (Exception $e) {
            $this->assertTrue($e instanceof VolumeException, $e->getMessage());
        }

        // test exception erreur de la fonction deleteObject
        /** @var CommandInterface $command */
        $command = $this->createMock(CommandInterface::class);
        $S3Client = $this->createMock(S3Client::class);
        $S3Client->method('doesObjectExist')->willReturn(true);
        $S3Client->method('doesBucketExist')->willReturn(true);
        $S3Client->method('__call')->willThrowException(new S3Exception('foo', $command));
        $volume = $this->getVolume($S3Client);
        try {
            $volume->fileDelete(self::TEST_FILE_KEY);
        } catch (Exception $e) {
            $this->assertTrue($e instanceof VolumeException, $e->getMessage());
        }
    }


    public function testFileExists()
    {
        // test d'un fichier existant dans Minio
        $S3Client = $this->createMock(S3Client::class);
        $S3Client->method('doesObjectExist')->willReturn(true);
        $S3Client->method('doesBucketExist')->willReturn(true);
        $volume = $this->getVolume($S3Client);
        $this->assertTrue($volume->fileExists(self::TEST_FILE_KEY));

        // test d'un fichier non existant dans Minio
        $S3Client = $this->createMock(S3Client::class);
        $S3Client->method('doesObjectExist')->willReturn(false);
        $S3Client->method('doesBucketExist')->willReturn(true);
        $volume = $this->getVolume($S3Client);
        $this->assertFalse($volume->fileExists(self::TEST_FILE_KEY));

        // test exception d'un fichier non existant dans Minio
        /** @var CommandInterface $command */
        $command = $this->createMock(CommandInterface::class);
        $S3Client = $this->createMock(S3Client::class);
        $S3Client->method('doesBucketExist')->willReturn(true);
        $S3Client->method('doesObjectExist')->willThrowException(new S3Exception('foo', $command));
        $volume = $this->getVolume($S3Client);
        $this->assertFalse($volume->fileExists(self::TEST_FILE_KEY));
    }

    public function testRename()
    {
        $S3Client = $this->createMock(S3Client::class);
        // false -> copy, true -> delete, true -> assert1, false -> assert2
        $S3Client->method('doesObjectExist')->willReturn(true, false, true, true, false);
        $S3Client->method('doesBucketExist')->willReturn(true);
        $volume = $this->getVolume($S3Client);
        $this->assertNotEmpty($volume->rename(self::TEST_FILE_KEY, self::TEST_FILE_KEY.'.bak'));
        $this->assertTrue($volume->fileExists(self::TEST_FILE_KEY.'.bak'));
        $this->assertFalse($volume->fileExists(self::TEST_FILE_KEY));
    }

    public function testCopy()
    {
        $S3Client = $this->createMock(S3Client::class);
        // false -> copy, true -> assert1, true -> assert2
        $S3Client->method('doesObjectExist')->willReturn(true, false, true, true);
        $S3Client->method('doesBucketExist')->willReturn(true);
        $volume = $this->getVolume($S3Client);
        $this->assertNotEmpty($volume->copy(self::TEST_FILE_KEY, self::TEST_FILE_KEY.'.bak'));
        $this->assertTrue($volume->fileExists(self::TEST_FILE_KEY.'.bak'));
        $this->assertTrue($volume->fileExists(self::TEST_FILE_KEY));
    }

    public function testReadfile()
    {
        $handle = tmpfile();
        fwrite($handle, 'foo');
        rewind($handle);
        $response = $this->createMock(Result::class);
        $response->method('get')->willReturn(new Stream($handle));
        $S3Client = $this->createMock(S3Client::class);
        $S3Client->method('doesBucketExist')->willReturn(true);
        $S3Client->method('doesObjectExist')->willReturn(true);
        $S3Client->method('__call')->willReturn($response); // getObject
        $volume = $this->getVolume($S3Client);
        ob_start();
        $volume->readfile(self::TEST_FILE_KEY);
        $content = ob_get_clean();
        $this->assertEquals('foo', $content);
    }

    public function testHash()
    {
        $handle = tmpfile();
        fwrite($handle, 'foo');
        rewind($handle);
        $response = $this->createMock(Result::class);
        $response->method('get')->willReturn(new Stream($handle));
        $S3Client = $this->createMock(S3Client::class);
        $S3Client->method('doesBucketExist')->willReturn(true);
        $S3Client->method('doesObjectExist')->willReturn(true);
        $S3Client->method('__call')->willReturn($response); // getObject
        $volume = $this->getVolume($S3Client);
        $hash = $volume->hash(self::TEST_FILE_KEY);
        $this->assertEquals(hash('sha256', 'foo'), $hash);
    }

    public function testLs()
    {
        $response = $this->createMock(Result::class);
        $response->method('get')->willReturn([['Key' => 'foo'], ['Key' => 'bar']]);
        $S3Client = $this->createMock(S3Client::class);
        $S3Client->method('doesBucketExist')->willReturn(true);
        $S3Client->method('doesObjectExist')->willReturn(true);
        $S3Client->method('getPaginator')->willReturn([$response]); // getPaginator
        $volume = $this->getVolume($S3Client);
        $ls = $volume->ls();
        $this->assertStringContainsString('foo', implode('', $ls));
        $volume = $this->getVolume($S3Client);
        $ls = $volume->ls('', true);
        $this->assertStringContainsString('foo', implode('', $ls));
        $ls = $volume->ls('foo');
        $this->assertStringContainsString('foo', implode('', $ls));
    }

    public function testMetadata()
    {
        $expected = [
            'modified' => new DateTime,
            'mime' => 'text/plain',
            'size' => 3
        ];
        $response = $this->createMock(Result::class);
        $response->method('get')->willReturnOnConsecutiveCalls(
            $expected['modified'],
            $expected['mime'],
            $expected['size']
        );
        $S3Client = $this->createMock(S3Client::class);
        $S3Client->method('doesBucketExist')->willReturn(true);
        $S3Client->method('doesObjectExist')->willReturn(true);
        $S3Client->method('__call')->willReturn($response); // getObject
        $volume = $this->getVolume($S3Client);
        $meta = $volume->metadata(self::TEST_FILE_KEY);
        $this->assertEquals($expected, $meta);
    }

    public function testStreamTo()
    {
        $targetVolume = new VolumeFilesystem(['path' => sys_get_temp_dir()]);
        $file = sys_get_temp_dir().DS.self::TEST_FILE_KEY;
        if (is_file($file)) {
            unlink($file);
        }

        $handle = tmpfile();
        fwrite($handle, 'foo');
        rewind($handle);
        $response = $this->createMock(Result::class);
        $response->method('get')->willReturn(new Stream($handle));
        $S3Client = $this->createMock(S3Client::class);
        $S3Client->method('doesBucketExist')->willReturn(true);
        $S3Client->method('doesObjectExist')->willReturn(true);
        $S3Client->method('__call')->willReturn($response); // getObject
        $volume = $this->getVolume($S3Client);
        $volume->streamTo(self::TEST_FILE_KEY, $targetVolume);
        $this->assertFileExists($file);
        $this->assertEquals('foo', file_get_contents($file));
        unlink($file);
    }

    public function testStreamUpload()
    {
        $S3Client = $this->createMock(S3Client::class);
        $S3Client->method('doesBucketExist')->willReturn(true);
        $S3Client->method('doesObjectExist')->willReturn(false);
        $volume = $this->getVolume($S3Client);
        $handle = tmpfile();
        $this->assertEquals(self::TEST_FILE_KEY, $volume->streamUpload($handle, self::TEST_FILE_KEY));
    }
}
