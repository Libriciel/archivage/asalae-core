<?php

namespace AsalaeCore\Test\TestCase\Driver\Volume;

use AsalaeCore\Driver\Volume\VolumeFilesystem;
use AsalaeCore\TestSuite\TestCase;
use Exception;
use PHPUnit\Framework\MockObject\MockObject;

class VolumeTestTraitTest extends TestCase
{
    public function testTest()
    {
        $volume = new VolumeFilesystem(['path' => sys_get_temp_dir()]);
        $this->assertTrue($volume->test()['success']);

        /** @var VolumeFilesystem|MockObject $volume */
        $volume = $this->getMockBuilder(VolumeFilesystem::class)
            ->setConstructorArgs([['path' => sys_get_temp_dir()]])
            ->setMethods(['filePutContent'])
            ->getMock();
        $this->assertFalse($volume->test()['success']);

        $volume = $this->getMockBuilder(VolumeFilesystem::class)
            ->setConstructorArgs([['path' => sys_get_temp_dir()]])
            ->setMethods(['fileGetContent'])
            ->getMock();
        $this->assertFalse($volume->test()['success']);

        $volume = $this->getMockBuilder(VolumeFilesystem::class)
            ->setConstructorArgs([['path' => sys_get_temp_dir()]])
            ->setMethods(['fileGetContent'])
            ->getMock();
        $volume->method('fileGetContent')->willReturn('bad_value');
        $this->assertFalse($volume->test()['success']);

        $volume = $this->getMockBuilder(VolumeFilesystem::class)
            ->setConstructorArgs([['path' => sys_get_temp_dir()]])
            ->setMethods(['fileGetContent'])
            ->getMock();
        $volume->method('fileGetContent')->willThrowException(new Exception);
        $this->assertFalse($volume->test()['success']);
    }
}
