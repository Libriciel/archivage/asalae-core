<?php

namespace AsalaeCore\Test\TestCase\Command;

use Cake\Console\TestSuite\ConsoleIntegrationTestTrait;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\Command\TraductionCommand;
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\I18n\I18n;
use Libriciel\Filesystem\Utility\Filesystem;

class TraductionCommandTest extends TestCase
{
    use ConsoleIntegrationTestTrait;

    /**
     * ConsoleIo mock
     *
     * @var \Cake\Console\ConsoleIo
     */
    public $io;

    /**
     * Test subject
     *
     * @var TraductionCommand
     */
    public $shell;

    /**
     * Nom du domaine pour le fichier pot et le fichier po
     *
     * @var string
     */
    const PO_DOMAIN = 'testtemplate';

    /**
     * setUp method
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->useCommandRunner();
        Cache::disable();
        Filesystem::reset();
        if (is_dir($dir = sys_get_temp_dir().DS.'test-traductions')) {
            Filesystem::remove($dir);
        }
        Filesystem::mkdir($dir);
        $sfilesys = new \Symfony\Component\Filesystem\Filesystem;
        $sfilesys->mirror(TESTS.'Data'.DS.'Locale', $dir);

        Configure::write('App.paths.locales', [$dir.DS]);
        I18n::clear();
        I18n::setLocale('fr_FR');
        $this->shell = new \AsalaeCore\Test\Mock\TraductionCommandMock;
    }

    public function tearDown(): void
    {
        parent::tearDown();
    }

    public function testMain()
    {
        $this->exec(
            'traduction',
            [
                '0', // choix du domain
                'n', // Provoque la suppression de la traduction exédentaire
                'saisie 1',
                'saisie 2',
                'saisie 3',
                'y', // confirme la dernière traduction
                'y', // confirme l'overwrite
            ]
        );
        $expected = <<<EOT
# HEADER - doit être conservé

#: Randomfile:39
#: Randomfile2:34
msgctxt "random_context"
msgid "This is a msgid with context"
msgstr "Cette phrase ne doit pas être écrasée"

#: Randomfile:37
msgid "This is a msgid"
msgstr "saisie 1"

msgid "This is a {0} with plural \"{1}\""
msgid_plural "This is a {0} with plural(s) \"{1}\""
msgstr[0] "saisie 2"
msgstr[1] "saisie 3"

EOT;
        $this->assertStringContainsString(
            $expected,
            file_get_contents(sys_get_temp_dir() . DS . 'test-traductions' . DS . 'fr_FR' . DS . 'testtemplate.po')
        );
    }
}
