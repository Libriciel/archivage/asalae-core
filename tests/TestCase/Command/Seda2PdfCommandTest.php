<?php

namespace AsalaeCore\Test\TestCase\Command;

use Cake\Console\TestSuite\ConsoleIntegrationTestTrait;
use AsalaeCore\TestSuite\TestCase;
use Libriciel\Filesystem\Utility\Filesystem;

class Seda2PdfCommandTest extends TestCase
{
    use ConsoleIntegrationTestTrait;

    public string $testDir = TMP_TESTDIR . DS . 'seda_2_pdf_command_test';

    public function setUp(): void
    {
        parent::setUp();
        $this->useCommandRunner();
        Filesystem::reset();
        if (is_dir($this->testDir)) {
            Filesystem::remove($this->testDir);
        }
        Filesystem::mkdir($this->testDir);
    }

    public function tearDown(): void
    {
        parent::tearDown();
        if (is_dir($this->testDir)) {
            Filesystem::remove($this->testDir);
        }
    }

    public function testMain()
    {
        $sourceFile = TEST_DATA . 'sample_seda02.xml';
        $outFile = $this->testDir . DS . 'test.pdf';

        $this->exec('seda2_pdf ' . $sourceFile . ' ' . $outFile);
        $this->assertOutputContains('Conversion effectuée');
        $this->assertExitCode(0);
        $this->assertFileExists($outFile);

        $this->exec('seda2_pdf ' . $sourceFile . ' ' . $outFile);
        $this->assertExitCode(1);

        $this->exec('seda2_pdf ' . $sourceFile . ' ' . $outFile . ' --force');
        $this->assertExitCode(0);
    }
}
