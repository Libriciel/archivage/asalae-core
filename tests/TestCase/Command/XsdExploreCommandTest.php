<?php
namespace AsalaeCore\Test\TestCase\Command;

use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\Utility\Exec;
use Cake\Console\TestSuite\ConsoleIntegrationTestTrait;

/**
 * AsalaeCore\Command\XsdExploreCommandTest Test Case
 */
class XsdExploreCommandTest extends TestCase
{
    use ConsoleIntegrationTestTrait;

    public function setUp(): void
    {
        parent::setUp();
        $this->useCommandRunner();
    }

    public function tearDown(): void
    {
        parent::tearDown();
    }

    private function exploreNode(string $nodeName, string $xsd, int $col = 0): array
    {
        $command = sprintf(
            'xsd_explore %s %s',
            Exec::escapeshellarg($xsd),
            $nodeName,
        );
        $this->cleanupConsoleTrait();
        $this->useCommandRunner();
        $this->exec($command);

        $response = array_slice($this->_out->messages(), 3);
        array_pop($response);

        return array_map(
            fn($item) => trim(explode('|', trim($item, ' |'))[$col] ?? ''),
            $response,
        );
    }

    private function explodeNodeWithMetadata(string $nodeName, string $xsd): array
    {
        $command = sprintf(
            'xsd_explore %s %s',
            Exec::escapeshellarg($xsd),
            $nodeName,
        );
        $this->cleanupConsoleTrait();
        $this->useCommandRunner();
        $this->exec($command);

        $response = array_slice($this->_out->messages(), 3);
        array_pop($response);

        $arr = [];
        foreach ($response as $item) {
            $sub = [];
            foreach (explode('|', trim($item, ' |')) as $subitem) {
                $sub[] = trim($subitem, '| ');
            }
            $arr[$sub[0]] = [
                'name' => $sub[0],
                'cardinality' => $sub[1],
                'type' => $sub[2],
            ];
        }
        return $arr;
    }

    /**
     * Test exec method
     *
     * @return void
     */
    public function testexec()
    {
        $result = $this->exploreNode('ArchiveUnit', SEDA_V21_XSD);
        $this->assertContains('ArchiveUnit', $result);

        $result = $this->exploreNode('CodeListVersions', SEDA_V21_XSD);
        $this->assertNotContains('HoldRuleCodeListVersion', $result);
        $result = $this->exploreNode('CodeListVersions', SEDA_V22_XSD);
        $this->assertContains('HoldRuleCodeListVersion', $result);

        $this->exploreNode('LinkingAgentIdentifier', SEDA_V21_XSD);
        $this->assertErrorContains('LinkingAgentIdentifier');
        $result = $this->exploreNode('LinkingAgentIdentifier', SEDA_V22_XSD);
        $this->assertContains('LinkingAgentIdentifierValue', $result);

        $result = $this->exploreNode('Management', SEDA_V22_XSD);
        $this->assertContains('HoldRule', $result);

        $result = $this->exploreNode('Signature', SEDA_V22_XSD);
        $this->assertContains('Signer', $result);

        $result = $this->exploreNode('ArchiveObject', SEDA_V10_XSD);
        $this->assertContains('ArchiveObject', $result);

        $result = $this->exploreNode('ArchiveTransfer', SEDA_V21_XSD);
        $this->assertContains('Comment', $result);

        $result = $this->explodeNodeWithMetadata('ArchiveTransfer', SEDA_V21_XSD);
        $this->assertContains('0..n', $result['Comment']);
        $this->assertContains('1..1', $result['Date']);
        $this->assertContains('xsd:dateTime', $result['Date']);

        $result = $this->explodeNodeWithMetadata('ArchiveUnit', SEDA_V21_XSD);
        $this->assertContains('0..1', $result['Management']);
        $this->assertContains('choice[1]:0..n - 1..1', $result['ArchiveUnit']);
        $this->assertContains('choice[1]:0..n - 1..1', $result['DataObjectReference']);
    }
}
