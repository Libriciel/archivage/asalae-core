<?php

namespace AsalaeCore\Test\TestCase\Command;

use Cake\Console\TestSuite\ConsoleIntegrationTestTrait;
use AsalaeCore\TestSuite\TestCase;
use React\EventLoop\Loop;

class RatchetCommandTest extends TestCase
{
    use ConsoleIntegrationTestTrait;

    public function setUp(): void
    {
        parent::setUp();
        $this->useCommandRunner();
    }

    public function testMain()
    {
        $loop = Loop::get();
        $runOccured = false;
        $loop->futureTick(
            function () use (&$runOccured, $loop) {
                $runOccured = true;
                $loop->stop();
            }
        );
        $this->exec(
            sprintf(
                'ratchet --domain %s --listen-port %d --port %d',
                'localhost',
                $this->findFreePort(),
                $this->findFreePort()
            )
        );
        $this->assertTrue($runOccured);
    }

    private function findFreePort(): int
    {
        $sock = socket_create_listen(0);
        socket_getsockname($sock, $addr, $port);
        socket_close($sock);
        return $port;
    }
}
