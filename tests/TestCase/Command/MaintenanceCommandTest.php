<?php

namespace AsalaeCore\Test\TestCase\Command;

use AsalaeCore\Utility\Config;
use Cake\Console\TestSuite\ConsoleIntegrationTestTrait;
use AsalaeCore\TestSuite\TestCase;
use Cake\Core\Configure;
use DateTime;
use DateTimeInterface;

class MaintenanceCommandTest extends TestCase
{
    use ConsoleIntegrationTestTrait;

    private array $initialConfig;
    public function setUp(): void
    {
        parent::setUp();
        $this->useCommandRunner();

        $this->initialConfig = Configure::read();
        if (!is_dir(TMP_TESTDIR)) {
            mkdir(TMP_TESTDIR, 0755, true);
        }

        $pathToLocalConfigFile = TMP_TESTDIR.DS.'path_to_local.php';
        $appLocalJsonFile = TMP_TESTDIR.DS.'test_local.json';
        file_put_contents($appLocalJsonFile, '{}');
        $appLocalJsonFile = addcslashes($appLocalJsonFile, "'");
        file_put_contents($pathToLocalConfigFile, "<?php return '$appLocalJsonFile';");
        Configure::write('App.paths.path_to_local_config', $pathToLocalConfigFile);
    }

    public function tearDown(): void
    {
        parent::tearDown();
        Configure::write($this->initialConfig);
        Config::reset();
    }

    private function distinctExec(string $command)
    {
        $this->cleanupConsoleTrait();
        $this->useCommandRunner();
        $this->exec($command);
    }

    public function testInstantOnOff()
    {
        $this->distinctExec('maintenance on');
        $this->assertOutputContains('success');
        $this->assertTrue(Configure::read('Interruption.enabled'));

        $this->distinctExec('maintenance status');
        $this->assertErrorContains('error');
        $this->assertErrorContains(__("Interruption immédiate"));

        $this->distinctExec('maintenance off');
        $this->assertOutputContains('success');
        $this->assertFalse(Configure::read('Interruption.enabled'));

        $this->distinctExec('maintenance status');
        $this->assertOutputContains('success');
        $this->assertOutputContains(__("N'est pas actuellement en mode maintenance"));
    }

    public function testSchedule()
    {
        $this->distinctExec('maintenance schedule now 2200-01-01T00:00:00+02:00');
        $this->assertOutputContains('success');
        $this->assertNotEmpty(Configure::read('Interruption.scheduled.begin'));
        $this->assertNotEmpty(Configure::read('Interruption.scheduled.end'));

        $this->distinctExec('maintenance status');
        $this->assertErrorContains('error');

        // NOTE: en cas d'erreur ici pour une seconde de décallage, revoir l'assertion
        $dateNow = new DateTime('now');
        $dateEnd = new DateTime('2200-01-01T00:00:00+02:00');
        $this->assertErrorContains(
            __(
                "Interruption programmée depuis le {0} jusqu'au {1}",
                $dateNow->format(DateTimeInterface::ATOM),
                $dateEnd->format(DateTimeInterface::ATOM),
            )
        );
        $this->assertExitCode(1);

        $this->distinctExec('maintenance off');
        $this->assertErrorContains(
            __(
                "La maintenance a été programmée pour la période {0} à {1}",
                $dateNow->format(DateTimeInterface::ATOM),
                $dateEnd->format(DateTimeInterface::ATOM),
            )
        );
        $this->assertErrorContains(
            __("La maintenance programmée a donc été retirée pour désactiver le mode maintenance")
        );
        $this->assertOutputContains('success');

        $this->distinctExec('maintenance status');
        $this->assertExitCode(0);

        $dateNow = new DateTime('now');
        $this->distinctExec('maintenance schedule now');
        $this->assertOutputContains('success');
        $this->assertNotEmpty(Configure::read('Interruption.scheduled.begin'));
        $this->assertEmpty(Configure::read('Interruption.scheduled.end'));

        $this->distinctExec('maintenance status');
        $this->assertExitCode(1);

        $this->distinctExec('maintenance off');
        $this->assertErrorContains(
            __(
                "La maintenance a été programmée pour à partir de {0}",
                $dateNow->format(DateTimeInterface::ATOM),
            )
        );
        $this->assertErrorContains(
            __("La maintenance programmée a donc été retirée pour désactiver le mode maintenance")
        );
        $this->assertOutputContains('success');

        $dateTomorrow = new DateTime('tomorrow');
        $this->distinctExec('maintenance schedule --no-begin tomorrow');
        $this->assertOutputContains('success');
        $this->assertEmpty(Configure::read('Interruption.scheduled.begin'));
        $this->assertNotEmpty(Configure::read('Interruption.scheduled.end'));

        $this->distinctExec('maintenance status');
        $this->assertExitCode(1);

        $this->distinctExec('maintenance off');
        $this->assertErrorContains(
            __(
                "La maintenance a été programmée jusqu'à {0}",
                $dateTomorrow->format(DateTimeInterface::ATOM),
            )
        );

        $this->distinctExec('maintenance status');
        $this->assertExitCode(0);

        $this->distinctExec('maintenance schedule yesterday yesterday');
        $this->distinctExec('maintenance status');
        $this->assertExitCode(0);
        $this->assertNotEmpty(Configure::read('Interruption.scheduled.begin'));
        $this->assertNotEmpty(Configure::read('Interruption.scheduled.end'));

        $this->distinctExec('maintenance on');
        $this->distinctExec('maintenance status');
        $this->assertExitCode(1);
        $this->assertEmpty(Configure::read('Interruption.scheduled.begin'));
        $this->assertEmpty(Configure::read('Interruption.scheduled.end'));
        $this->distinctExec('maintenance off');
        $this->distinctExec('maintenance status');
        $this->assertExitCode(0);
    }

    public function testScheduleErrors()
    {
        // 2 dates alors que --no-begin
        $this->distinctExec('maintenance schedule --no-begin now tomorrow');
        $this->assertErrorContains('error');

        // format date incorrecte
        $this->distinctExec('maintenance schedule 30/01/2024');
        $this->assertErrorContains('error');
    }

    public function testPeriodic()
    {
        $now = (new DateTime())->format('H:i:s');
        $oneHourAgo = (new DateTime())->modify('-1 hour')->format('H:i:s');
        $inOneHour = (new DateTime())->modify('+1 hour')->format('H:i:s');

        $this->distinctExec("maintenance periodic $oneHourAgo $inOneHour");
        $this->assertOutputContains('success');
        $this->assertNotEmpty(Configure::read('Interruption.periodic.begin'));
        $this->assertNotEmpty(Configure::read('Interruption.periodic.end'));

        $this->distinctExec('maintenance status');
        $this->assertExitCode(1);

        $this->distinctExec('maintenance off');
        $this->assertErrorContains(
            __(
                "Il est actuellement {0} et la maintenance périodique" .
                " a été programmée pour la période {1} à {2}",
                $now,
                $oneHourAgo,
                $inOneHour,
            )
        );

        $this->distinctExec('maintenance status');
        $this->assertExitCode(0);
    }

    public function testPeriodicErrors()
    {
        $this->distinctExec("maintenance periodic pas_une_heure 00:00:00");
        $this->assertErrorContains('error');
        $this->distinctExec("maintenance periodic 00:00:00 pas_une_heure");
        $this->assertErrorContains('error');
    }

    public function testUnsetPeriodic()
    {
        $this->distinctExec("maintenance periodic 02:00:00 03:00:00");
        $this->distinctExec("maintenance unset periodic");
        $this->assertEmpty(Configure::read('Interruption.periodic.begin'));
        $this->assertEmpty(Configure::read('Interruption.periodic.end'));
    }

    public function testUnsetScheduled()
    {
        $this->distinctExec("maintenance scheduled now tomorrow");
        $this->distinctExec("maintenance unset scheduled");
        $this->assertEmpty(Configure::read('Interruption.scheduled.begin'));
        $this->assertEmpty(Configure::read('Interruption.scheduled.end'));
    }
}
