<?php

namespace AsalaeCore\Test\TestCase\Command;

use AsalaeCore\TestSuite\TestCase;
use Cake\Http\Exception\NotFoundException;
use Cake\Console\TestSuite\ConsoleIntegrationTestTrait;
use Libriciel\Filesystem\Utility\Filesystem;

class HashDirCommandTest extends TestCase
{
    use ConsoleIntegrationTestTrait;

    public function setUp(): void
    {
        parent::setUp();
        $this->useCommandRunner();

        // NOTE: doit ce situer dans ROOT pour fonctionner
        if (is_dir(ROOT . DS . 'tmp' . TMP_TESTDIR)) {
            Filesystem::remove(ROOT . DS . 'tmp' . TMP_TESTDIR);
        }
        Filesystem::mkdir(ROOT . DS . 'tmp' . TMP_TESTDIR);
    }

    public function tearDown(): void
    {
        parent::tearDown();
        if (is_dir(ROOT . DS . 'tmp' . TMP_TESTDIR)) {
            Filesystem::remove(ROOT . DS . 'tmp' . TMP_TESTDIR);
        }
    }

    public function testHashDirCommand()
    {
        // test main
        $filename = ROOT . DS . 'tmp' . TMP_TESTDIR . DS . 'export_file.json';
        $this->exec(
            'hash_dir src/ tests/ composer.json -r --filter *Interface* --filter */Abstract* --output '
            .escapeshellarg('tmp'.TMP_TESTDIR . DS . 'export_file.json')
        );
        $this->assertFileExists($filename);
        $this->assertStringContainsString(
            'src/Application.php:'.md5_file(APP.'Application.php'),
            file_get_contents($filename)
        );

        // test check (md5)
        $this->exec('hash_dir check '.escapeshellarg($filename));
        $this->assertOutputContains('OK');

        // test compare
        $this->exec('hash_dir compare '.escapeshellarg($filename));
        $this->assertOutputContains('OK');
    }

    public function testFileNotFound()
    {
        $e = null;
        try {
            $this->exec('hash_dir ./foo');
        } catch (\Exception $e) {
        }
        $this->assertInstanceOf(NotFoundException::class, $e);

        $e = null;
        try {
            $this->exec('hash_dir check ./foo');
        } catch (\Exception $e) {
        }
        $this->assertInstanceOf(\Exception::class, $e);
        $this->assertTextContains('file not found', $e->getMessage());

        $e = null;
        try {
            $this->exec('hash_dir compare ./foo');
        } catch (\Exception $e) {
        }
        $this->assertInstanceOf(\Exception::class, $e);
        $this->assertTextContains('file not found', $e->getMessage());
    }
}
