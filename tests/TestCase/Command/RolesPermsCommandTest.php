<?php
namespace AsalaeCore\Test\TestCase\Command;

use AsalaeCore\Factory\Utility;
use AsalaeCore\Test\Mock\AppMock;
use AsalaeCore\TestSuite\TestCase;
use Cake\Core\App;
use Cake\ORM\TableRegistry;
use Cake\Console\TestSuite\ConsoleIntegrationTestTrait;
use Cake\Utility\Hash;
use Libriciel\Filesystem\Utility\Filesystem;
use const TMP_TESTDIR;

/**
 * AsalaeCore\Command\RolesPermsCommand Test Case
 */
class RolesPermsCommandTest extends TestCase
{
    use ConsoleIntegrationTestTrait;

    public $fixtures = [
        'plugin.AsalaeCore.Acos',
        'plugin.AsalaeCore.Aros',
        'plugin.AsalaeCore.ArosAcos',
        'plugin.AsalaeCore.Roles',
    ];

    public function setUp(): void
    {
        parent::setUp();
        $this->useCommandRunner();
        $dir = TMP_TESTDIR;
        if (is_dir($dir)) {
            Filesystem::remove($dir);
        }
        mkdir($dir);

        Utility::set(App::class, AppMock::class);
    }

    public function tearDown(): void
    {
        parent::tearDown();
        $dir = TMP_TESTDIR;
        if (is_dir($dir)) {
            Filesystem::remove($dir);
        }
    }

    /**
     * Test export method
     *
     * @return void
     */
    public function testExport()
    {
        $Acos = TableRegistry::getTableLocator()->get('Acos');
        $controllers = $Acos->find()->where(
            [
                'model' => 'root',
                'alias' => 'controllers'
            ]
        )->firstOrFail();
        $action = $Acos->find()->where(
            [
                'model' => 'Home',
                'alias' => 'index',
                'lft >' => $controllers->get('lft'),
                'rght <' => $controllers->get('rght'),
            ]
        )->firstOrFail();
        $Aros = TableRegistry::getTableLocator()->get('Aros');
        $aro = $Aros->find()->where(['alias' => 'Archiviste'])->firstOrFail();

        $Permissions = TableRegistry::getTableLocator()->get('ArosAcos');
        $Permissions->deleteAll([]);
        $perm = $Permissions->newEntity(
            [
                'aro_id' => $aro->get('id'),
                'aco_id' => $action->get('id'),
                '_create' => '1',
                '_read' => '1',
                '_update' => '1',
                '_delete' => '1',
            ]
        );
        $Permissions->saveOrFail($perm);

        if (is_file($filename = TMP_TESTDIR.DS.'export_roles.json')) {
            unlink($filename);
        }
        $this->exec(
            'RolesPerms export',
            [
                $filename
            ]
        );
        $content = json_decode(file_get_contents($filename), true);

        $this->assertEquals('1111', Hash::get($content, 'Archiviste.root/controllers/Home/index'));
    }

    /**
     * Test import method
     *
     * @return void
     */
    public function testImport()
    {
        $Permissions = TableRegistry::getTableLocator()->get('ArosAcos');
        $Permissions->deleteAll([]);

        $file = @tempnam(TMP_TESTDIR, 'test-import-');
        file_put_contents(
            $file,
            json_encode(
                [
                    'Archiviste' => [
                        'root/controllers/Home/index' => '1'
                    ]
                ],
                JSON_UNESCAPED_SLASHES
            )
        );
        TableRegistry::getTableLocator()->clear();
        $this->exec(
            'RolesPerms import --datasource test',
            [
                $file,
                'y', // delete existing perms
            ]
        );
        unlink($file);
        $this->assertEquals(1, $Permissions->find()->count());
    }
}
