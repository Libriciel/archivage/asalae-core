<?php

namespace AsalaeCore\Test\TestCase\Command;

use AsalaeCore\Model\Table\OrgEntitiesTable;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\Console\TestSuite\ConsoleIntegrationTestTrait;
use AsalaeCore\TestSuite\TestCase;

class TreeCommandTest extends TestCase
{
    use ConsoleIntegrationTestTrait;

    public $fixtures = [
        'app.Aros',
        'app.Keywords',
        'app.OrgEntities',
        'app.Roles',
    ];

    public function setUp(): void
    {
        parent::setUp();
        $this->useCommandRunner();
        /** @var \Composer\Autoload\ClassLoader $loader */
        $loader = include ROOT.DS.'vendor'.DS.'autoload.php';
        Configure::write(
            'Tree.model_path',
            dirname(realpath($loader->findFile(OrgEntitiesTable::class)))
        );
    }

    public function tearDown(): void
    {
        parent::tearDown();
    }

    public function testRepair()
    {
        $OrgEntities = TableRegistry::getTableLocator()->get('OrgEntities');
        $OrgEntities->updateAll(['lft' => null, 'rght' => null], []);
        $this->assertCount($OrgEntities->find()->count(), $OrgEntities->find()->where(['lft IS' => null]));
        $this->exec('tree repair org_entities');
        $this->assertOutputContains('OrgEntities');
        $this->assertCount(0, $OrgEntities->find()->where(['lft IS' => null]));
    }

    public function testRepairAll()
    {
        $OrgEntities = TableRegistry::getTableLocator()->get('OrgEntities');
        $OrgEntities->updateAll(['lft' => null, 'rght' => null], []);
        $this->assertCount($OrgEntities->find()->count(), $OrgEntities->find()->where(['lft IS' => null]));
        $this->exec('tree repair_all');
        $this->assertOutputContains('OrgEntities');
        $this->assertCount(0, $OrgEntities->find()->where(['lft IS' => null]));
    }
}
