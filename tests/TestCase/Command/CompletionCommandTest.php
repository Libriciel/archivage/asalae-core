<?php

namespace AsalaeCore\Test\TestCase\Command;

use AsalaeCore\TestSuite\TestCase;
use Cake\Console\TestSuite\ConsoleIntegrationTestTrait;
use Libriciel\Filesystem\Utility\Filesystem;

class CompletionCommandTest extends TestCase
{
    use ConsoleIntegrationTestTrait;

    public function setUp(): void
    {
        parent::setUp();
        $this->useCommandRunner();
    }

    public function testCompletionCommand()
    {
        $this->exec('completion commands');
        $this->assertOutputContains('completion');

        $this->exec('completion subcommands routes');
        $this->assertOutputContains('check');

        $this->exec('completion options routes');
        $this->assertOutputContains('--help');
    }
}
