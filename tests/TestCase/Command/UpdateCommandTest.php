<?php
namespace AsalaeCore\Test\TestCase\Command;

use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\Utility\CascadeDelete;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\Console\TestSuite\ConsoleIntegrationTestTrait;
use Libriciel\Filesystem\Utility\Filesystem;

/**
 * AsalaeCore\Command\UpdateCommand Test Case
 */
class UpdateCommandTest extends TestCase
{
    use ConsoleIntegrationTestTrait;

    public $fixtures = [
        'app.Acos',
        'app.Aros',
        'app.ArosAcos',
        'app.Roles',
        'app.Users',
        'app.Webservices',
    ];

    public function setUp(): void
    {
        $this->useCommandRunner();
        if (is_dir(TMP_TESTDIR)) {
            Filesystem::remove(TMP_TESTDIR);
        }
        mkdir(TMP_TESTDIR);
        parent::setUp();
    }

    public function tearDown(): void
    {
        parent::tearDown();
        if (is_dir(TMP_TESTDIR)) {
            Filesystem::remove(TMP_TESTDIR);
        }
    }

    public function testMain()
    {
        $Acos = TableRegistry::getTableLocator()->get('Acos');
        $Aros = TableRegistry::getTableLocator()->get('Aros');
        $ArosAcos = TableRegistry::getTableLocator()->get('ArosAcos');
        $cond1 = ['model' => 'MyPlugin.Sample', 'alias' => 'add'];
        $cond2 = ['model' => 'api', 'alias' => 'MyPlugin.Webservices'];
        CascadeDelete::truncate($Acos);//->deleteAll();
        $this->assertFalse($Acos->exists($cond1));
        $this->assertFalse($Acos->exists($cond2));

        // test de suppression d'aros excédentaire
        $aro = $Aros->findOrCreate(['model' => 'Roles', 'foreign_key' => 0]);
        $this->assertNotEmpty($aro);

        $controllerJson = TMP_TESTDIR.DS.'testunit-controllers.json';
        Configure::write('App.paths.controllers_rules', $controllerJson);
        file_put_contents(
            $controllerJson,
            json_encode(
                [
                    'MyPlugin.Sample' => [
                        'test' => [
                            'invisible' => '0',
                            'accesParDefaut' => '0',
                            'commeDroits' => 'MyPlugin.Sample::add',
                        ],
                        'add' => [
                            'invisible' => '0',
                            'accesParDefaut' => '1',
                            'commeDroits' => '',
                        ]
                    ],
                ]
            )
        );

        $apiJson = TMP_TESTDIR.DS.'testunit-api.json';
        Configure::write('App.paths.apis_rules', $apiJson);
        file_put_contents(
            $apiJson,
            json_encode(
                [
                    'MyPlugin.Webservices' => [
                        'default' => [
                            'invisible' => '0',
                            'accesParDefaut' => '1',
                            'commeDroits' => '',
                        ],
                        'test' => [
                            'invisible' => '0',
                            'accesParDefaut' => '0',
                            'commeDroits' => 'MyPlugin.Webservices::default',
                        ]
                    ]
                ]
            )
        );

        $this->exec('update');
        $this->exec('update'); // meilleur couverture de code

        // test de suppression d'aros excédentaire
        $this->assertFalse($Aros->exists(['id' => $aro->id]));

        // vérifi accesParDefaut
        $aco = $Acos->find()->where($cond1)->first();
        $this->assertNotEmpty($aco);
        $this->assertTrue($ArosAcos->exists(['aco_id' => $aco->id, 'aro_id' => 1, '_create' => '1']));
        $aco = $Acos->find()->where($cond2)->first();
        $this->assertNotEmpty($aco);
        $this->assertTrue($ArosAcos->exists(['aco_id' => $aco->id, 'aro_id' => 1, '_create' => '1']));

        // vérifi le commeDroit
        $aco1 = $Acos->find()->where(['model' => 'MyPlugin.Sample', 'alias' => 'add'])->first();
        $aco2 = $Acos->find()->where(['model' => 'MyPlugin.Sample', 'alias' => 'test'])->first();
        $this->assertNotEmpty($aco1);
        $this->assertNotEmpty($aco2);
        $this->assertEquals($aco1->id, $aco2->get('parent_id'));

        // modification de commeDroit
        file_put_contents(
            $controllerJson,
            json_encode(
                [
                    'MyPlugin.Home' => [
                        'index' => [
                            'invisible' => '0',
                            'accesParDefaut' => '0',
                            'commeDroits' => '',
                        ],
                    ],
                    'MyPlugin.Sample' => [
                        'test' => [
                            'invisible' => '0',
                            'accesParDefaut' => '0',
                            'commeDroits' => 'MyPlugin.Home::index',
                        ],
                        'add' => [
                            'invisible' => '1',
                            'accesParDefaut' => '1',
                            'commeDroits' => '',
                        ]
                    ],
                ]
            )
        );
        file_put_contents(
            $apiJson,
            json_encode(
                [
                    'MyPlugin.Home' => [
                        'default' => [
                            'invisible' => '1',
                            'accesParDefaut' => '0',
                            'commeDroits' => '',
                        ],
                    ],
                    'MyPlugin.Webservices' => [
                        'default' => [
                            'invisible' => '0',
                            'accesParDefaut' => '1',
                            'commeDroits' => '',
                        ],
                        'test' => [
                            'invisible' => '0',
                            'accesParDefaut' => '0',
                            'commeDroits' => 'MyPlugin.Webservices::default',
                        ]
                    ]
                ]
            )
        );
        $this->exec('update');
        $aco1 = $Acos->find()->where(['model' => 'MyPlugin.Home', 'alias' => 'index'])->first();
        $aco2 = $Acos->find()->where(['model' => 'MyPlugin.Sample', 'alias' => 'test'])->first();
        $this->assertNotEmpty($aco1);
        $this->assertNotEmpty($aco2);
        $this->assertEquals($aco1->id, $aco2->get('parent_id'));
        $aco1 = $Acos->find()->where(['model' => 'api', 'alias' => 'MyPlugin.Webservices'])->first();
        $aco2 = $Acos->find()->where(['model' => 'MyPlugin.Webservices', 'alias' => 'test'])->first();
        $this->assertNotEmpty($aco1);
        $this->assertNotEmpty($aco2);
        $this->assertEquals($aco1->id, $aco2->get('parent_id'));
    }
}
