<?php
namespace AsalaeCore\Test\TestCase\Command;

use AsalaeCore\Command\ParallelCommand;
use AsalaeCore\Exception\GenericException;
use AsalaeCore\TestSuite\TestCase;
use AsalaeCore\Utility\Exec;
use Cake\Console\TestSuite\ConsoleIntegrationTestTrait;

/**
 * AsalaeCore\Command\ParallelCommandTest Test Case
 */
class ParallelCommandTest extends TestCase
{
    use ConsoleIntegrationTestTrait;

    public $fixtures = [
    ];

    public function setUp(): void
    {
        parent::setUp();
        $this->useCommandRunner();
    }

    public function tearDown(): void
    {
        parent::tearDown();
    }

    /**
     * Test exec method
     *
     * @return void
     */
    public function testexec()
    {
        // test OK
        $command = sprintf(
            'parallel %s %s',
            Exec::escapeshellarg(ParallelCommand::encode([__CLASS__, 'myPublicStaticFn'])),
            Exec::escapeshellarg(ParallelCommand::encode(['    my text need to be trimmed   ']))
        );
        $this->exec($command);
        $response = ParallelCommand::parseResponse($this->_out->messages()[0]);
        $this->cleanupConsoleTrait();
        $this->useCommandRunner();
        $this->assertIsArray($response);
        $this->assertArrayHasKey('result', $response);
        $this->assertEquals('my text need to be trimmed', $response['result']);

        // test exception
        $command = sprintf(
            'parallel %s %s',
            Exec::escapeshellarg(ParallelCommand::encode([__CLASS__, 'throwingFn'])),
            Exec::escapeshellarg(ParallelCommand::encode(['text for the exception']))
        );
        $this->exec($command);
        $response = ParallelCommand::parseResponse($this->_out->messages()[0]);
        $this->cleanupConsoleTrait();
        $this->useCommandRunner();
        $this->assertIsArray($response);
        $this->assertArrayHasKey('exception', $response);
        /** @var GenericException $e */
        $e = $response['exception'];
        $this->assertInstanceOf(GenericException::class, $e);
        $this->assertEquals('text for the exception', $e->getMessage());

        // test error
        $command = sprintf(
            'parallel %s %s',
            Exec::escapeshellarg(ParallelCommand::encode([__CLASS__, 'errorFn'])),
            Exec::escapeshellarg(ParallelCommand::encode(['text for the error']))
        );
        $this->exec($command);
        $response = ParallelCommand::parseResponse($this->_out->messages()[0]);
        $this->cleanupConsoleTrait();
        $this->useCommandRunner();
        $this->assertIsArray($response);
        $this->assertArrayHasKey('errors', $response);
        $this->assertIsArray($response['errors']);
        $this->assertArrayHasKey(0, $response['errors']);
        $this->assertArrayHasKey('errno', $response['errors'][0]);
        $this->assertEquals(E_USER_NOTICE, $response['errors'][0]['errno']);
        $this->assertArrayHasKey('errstr', $response['errors'][0]);
        $this->assertEquals('text for the error', $response['errors'][0]['errstr']);

        // no params
        $command = sprintf(
            'parallel %s',
            Exec::escapeshellarg(ParallelCommand::encode([__CLASS__, 'noParamsFn']))
        );
        $this->exec($command);
        $response = ParallelCommand::parseResponse($this->_out->messages()[0]);
        $this->cleanupConsoleTrait();
        $this->useCommandRunner();
        $this->assertIsArray($response);
        $this->assertArrayHasKey('result', $response);
        $this->assertTrue($response['result']);

        // empty params
        $command = sprintf(
            'parallel %s %s',
            Exec::escapeshellarg(ParallelCommand::encode([__CLASS__, 'noParamsFn'])),
            Exec::escapeshellarg(ParallelCommand::encode([]))
        );
        $this->exec($command);
        $response = ParallelCommand::parseResponse($this->_out->messages()[0]);
        $this->cleanupConsoleTrait();
        $this->useCommandRunner();
        $this->assertIsArray($response);
        $this->assertArrayHasKey('result', $response);
        $this->assertTrue($response['result']);
    }

    public static function myPublicStaticFn(string $text)
    {
        return trim($text);
    }

    public static function throwingFn(string $text)
    {
        throw new GenericException($text);
    }

    public static function errorFn(string $text)
    {
        trigger_error($text);
        return 'error';
    }

    public static function noParamsFn()
    {
        return true;
    }
}
