<?php

namespace AsalaeCore\Test\TestCase\Command;

use AsalaeCore\Model\Table\ArosAcosTable;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\Console\TestSuite\ConsoleIntegrationTestTrait;
use AsalaeCore\TestSuite\TestCase;
use Libriciel\Filesystem\Utility\Filesystem;

class ApisCommandTest extends TestCase
{
    use ConsoleIntegrationTestTrait;

    public string $testDir = TMP_TESTDIR . DS . 'api_command_test';

    public $fixtures = [
        'plugin.AsalaeCore.Acos',
        'plugin.AsalaeCore.Aros',
        'plugin.AsalaeCore.ArosAcos',
    ];

    /**
     * @var ArosAcosTable
     */
    public $Permissions;

    public function setUp(): void
    {
        parent::setUp();
        $this->useCommandRunner();
        Filesystem::reset();
        if (is_dir($this->testDir)) {
            Filesystem::remove($this->testDir);
        }
        Filesystem::mkdir($this->testDir);
        $json = $this->testDir . DS . 'testunit-apis.json';
        Configure::write('App.paths.apis_rules', $json);
        Configure::write('Apis.namespace', 'MyPlugin');
        Configure::write('Apis.controllers', TESTS.'Application'.DS.'src'.DS.'Controller'.DS.'*Controller.php');
        file_put_contents($json, '{}');
        TableRegistry::getTableLocator()->clear();
        $this->Permissions = TableRegistry::getTableLocator()->get('ArosAcos');
        $this->Permissions->deleteAll([]);
        Configure::write(
            'App.paths.controllers_group',
            $this->testDir . DS . 'controllers_group.json'
        );
        Filesystem::dumpFile(
            $this->testDir . DS . 'controllers_group.json',
            '["Test"]'
        );
    }

    public function tearDown(): void
    {
        parent::tearDown();
        $json = $this->testDir . DS . 'testunit-apis.json';
        if (is_file($json)) {
            unlink($json);
        }
        Configure::write('App.namespace', 'AsalaeCore');
        if (is_dir($this->testDir)) {
            Filesystem::remove($this->testDir);
        }
    }

    public function testMain()
    {
        $this->exec('apis Webservices/test --interactive n --plugin MyPlugin');
        $this->assertFileExists(Configure::read('App.paths.apis_rules'));
        $json = Configure::read('App.paths.apis_rules');
        $data = json_decode(file_get_contents($json));
        $this->assertTrue(!empty($data->Webservices->test));
        $this->assertEquals('0', $data->Webservices->test->invisible ?? null);
        $this->assertEquals('0', $data->Webservices->test->accesParDefaut ?? null);
        $this->assertEquals('', $data->Webservices->test->commeDroits ?? null);

        file_put_contents($json, '{}');
        $this->cleanupConsoleTrait();
        $this->useCommandRunner();
        $this->exec('apis --interactive n --plugin MyPlugin', ['Webservices', 'test']);
        $data = json_decode(file_get_contents($json));
        $this->assertTrue(!empty($data->Webservices->test));

        file_put_contents($json, '{}');
        $this->assertCount(0, $this->Permissions->find());
        $this->cleanupConsoleTrait();
        $this->useCommandRunner();
        $this->exec('apis Webservices/test --invisible --accesParDefaut --interactive n --plugin MyPlugin');
        $data = json_decode(file_get_contents($json));
        $this->assertTrue(!empty($data->Webservices->test));
        $this->assertEquals('1', $data->Webservices->test->invisible ?? null);
        $this->assertEquals('1', $data->Webservices->test->accesParDefaut ?? null);
        $this->assertGreaterThan(0, $this->Permissions->find()->count());

        $this->cleanupConsoleTrait();
        $this->useCommandRunner();
        $this->exec('apis Webservices/default --commeDroits Webservices::test --plugin MyPlugin');
        $data = json_decode(file_get_contents($json));
        $this->assertTrue(!empty($data->Webservices->test));
        $this->assertTrue(!empty($data->Webservices->default));
        $this->assertEquals('Webservices::test', $data->Webservices->default->commeDroits ?? null);

        $this->cleanupConsoleTrait();
        $this->useCommandRunner();
        file_put_contents($json, '{}');
        $this->exec('apis --plugin MyPlugin', ['Webservices', 'test', 'n', 'y', 'n', '0']);
        $data = json_decode(file_get_contents($json));
        $this->assertTrue(!empty($data->Webservices->test));
    }
}
