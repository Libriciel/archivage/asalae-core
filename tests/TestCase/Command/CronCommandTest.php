<?php

namespace AsalaeCore\Test\TestCase\Command;

use AsalaeCore\Test\Mock\FakeCron;
use AsalaeCore\TestSuite\InvokePrivateTrait;
use AsalaeCore\Utility\Notify;
use Cake\Console\TestSuite\ConsoleIntegrationTestTrait;
use AsalaeCore\TestSuite\TestCase;
use DateTime;
use ZMQSocket;

class CronCommandTest extends TestCase
{
    use ConsoleIntegrationTestTrait, InvokePrivateTrait;

    public $fixtures = [
        'app.Crons',
        'app.CronExecutions',
    ];

    public function setUp(): void
    {
        parent::setUp();
        $this->useCommandRunner();
        $socket = $this->createMock(ZMQSocket::class);
        $socket->method('send');
        $Notify = Notify::getInstance();
        $this->invokeProperty($Notify, 'socket', 'set', $socket);
    }

    public function testrun()
    {
        $uniqid = uniqid('testrun');
        $Crons = $this->fetchTable('Crons');
        $cron = $Crons->newEntity(
            [
                'name' => "test cron",
                'description' => "test cron",
                'classname' => FakeCron::class,
                'active' => true,
                'locked' => false,
                'frequency' => 'one_shot',
                'next' => (new DateTime())->format(DATE_RFC3339),
                'app_meta' => json_encode(['test' => $uniqid]),
                'parallelisable' => false,
            ]
        );
        $Crons->saveOrFail($cron);
        $this->exec('cron run ' . $cron->id);
        $this->assertOutputContains('success');

        // one_shot supprimé
        $this->assertCount(0, $Crons->find()->where(['id' => $cron->id]));

        // log écrit
        $this->assertFileExists($cron->get('log_filename'));
        $lastLine = exec('tail -n1 ' . escapeshellarg($cron->get('log_filename')));
        $this->assertStringEndsWith($uniqid, $lastLine);
    }
}
