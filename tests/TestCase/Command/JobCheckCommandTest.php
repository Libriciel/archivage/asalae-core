<?php

namespace AsalaeCore\Test\TestCase\Command;

use Cake\Console\TestSuite\ConsoleIntegrationTestTrait;
use AsalaeCore\TestSuite\TestCase;

class JobCheckCommandTest extends TestCase
{
    use ConsoleIntegrationTestTrait;

    public $fixtures = [
        'plugin.Beanstalk.BeanstalkJobs',
        'app.Users',
    ];

    public function setUp(): void
    {
        parent::setUp();
        $this->useCommandRunner();
    }

    public function testMain()
    {
        $BeanstalkJobs = $this->fetchTable('BeanstalkJobs');
        $BeanstalkJobs->deleteAll([]);
        $BeanstalkJobs->addBehavior('Timestamp');

        $this->exec('job check');
        $this->assertExitCode(0);

        $job = $BeanstalkJobs->newEntity(
            [
                'id' => 1,
                'tube' => 'testunit-tube',
                'job_state' => 'failed',
            ]
        );
        $BeanstalkJobs->saveOrFail($job);
        $this->exec('job check');
        $this->assertExitCode(1);
        $this->assertErrorContains('testunit-tube');
    }
}
