<?php

namespace AsalaeCore\Test\TestCase\Command;

use AsalaeCore\Model\Table\ArosAcosTable;
use Cake\Console\TestSuite\ConsoleIntegrationTestTrait;
use AsalaeCore\TestSuite\TestCase;
use Libriciel\Filesystem\Utility\Filesystem;

class DatabaseFixturesCommandTest extends TestCase
{
    use ConsoleIntegrationTestTrait;

    public $fixtures = [
        'app.Users',
    ];

    /**
     * @var ArosAcosTable
     */
    public $Permissions;

    public function setUp(): void
    {
        parent::setUp();
        $this->useCommandRunner();
        Filesystem::reset();
        if (is_dir(TMP_TESTDIR)) {
            Filesystem::remove(TMP_TESTDIR);
        }
        Filesystem::mkdir(TMP_TESTDIR);
    }

    public function tearDown(): void
    {
        parent::tearDown();
        if (is_dir(TMP_TESTDIR)) {
            Filesystem::remove(TMP_TESTDIR);
        }
    }

    public function testexecute()
    {
        $this->exec('database fixtures --datasource test');
        $this->assertOutputContains(TMP.'Fixture/UsersFixture.php');
    }
}
