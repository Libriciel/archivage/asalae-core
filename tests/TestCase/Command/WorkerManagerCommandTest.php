<?php

namespace AsalaeCore\Test\TestCase\Command;

use AsalaeCore\Factory\Utility;
use AsalaeCore\TestSuite\AsyncTrait;
use AsalaeCore\Utility\Config;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\Console\TestSuite\ConsoleIntegrationTestTrait;
use AsalaeCore\TestSuite\TestCase;

class WorkerManagerCommandTest extends TestCase
{
    use ConsoleIntegrationTestTrait, AsyncTrait;

    public $fixtures = [
        'plugin.Beanstalk.BeanstalkWorkers',
    ];

    private $tmpOutputFile;
    private $initialPtl;

    public function setUp(): void
    {
        parent::setUp();
        $this->useCommandRunner();
        $this->tmpOutputFile = sys_get_temp_dir().DS.uniqid('testunit-');
        Configure::write(
            'Beanstalk.workers',
            [
                'test' => [
                    'run' => 'sh -c "echo created > '.$this->tmpOutputFile.'"',
                    'kill' => 'sh -c "echo killed {{hostname}}:{{pid}} > '.$this->tmpOutputFile.'"',
                    'quantity' => 1,
                    'active' => true,
                ],
            ]
        );
        Configure::write('Wrench.enable', false);
        Utility::reset();

        $this->initialPtl = Configure::read('App.paths.path_to_local_config');
        $ptl = sys_get_temp_dir().DS.'path_to_local.php';
        $local = sys_get_temp_dir().DS.'test_local.json';
        file_put_contents($ptl, '<?php return "'.$local.'";?>');
        file_put_contents($local, '{}');
        Configure::write('App.paths.path_to_local_config', $ptl);
    }

    public function tearDown(): void
    {
        parent::tearDown();
        if (is_file($this->tmpOutputFile)) {
            unlink($this->tmpOutputFile);
        }
        Configure::write('Wrench.enable', false);
        if (is_file($ptl = sys_get_temp_dir().DS.'path_to_local.php')) {
            unlink($ptl);
        }
        if (is_file($local = sys_get_temp_dir().DS.'test_local.json')) {
            unlink($local);
        }
        Configure::write('App.paths.path_to_local_config', $this->initialPtl);
        Config::reset();
    }

    public function testMain()
    {
        $this->exec('WorkerManager');
        $this->assertFileWillContain($this->tmpOutputFile, 'created');
        $this->assertOutputContains('test'); // nom du worker

        $config = [
            'Interruption' => [
                'enabled' => true, // important
                'scheduled' => [
                    'begin' => null,
                    'end' => null,
                ],
                'periodic' => [
                    'begin' => null,
                    'end' => null,
                ],
                'enable_whitelist' => true,
                'whitelist_headers' => [
                    'X-Asalae-Webservice' => true,
                ],
                'message' => 'test',
                'whitelist_file' => null,
                'enable_workers' => false,
            ]
        ];
        $local = sys_get_temp_dir().DS.'test_local.json';
        file_put_contents($local, json_encode($config));
        $this->cleanupConsoleTrait();
        $this->useCommandRunner();
        $this->exec('WorkerManager');
        $this->assertOutputNotContains('test');

        Configure::write('Wrench.enable', false);
        Configure::write('Beanstalk.workers.test.active', false);
        $this->cleanupConsoleTrait();
        $this->useCommandRunner();
        $this->exec('WorkerManager');
        $this->assertOutputNotContains('test');
    }

    public function testListWorkers()
    {
        $BeanstalkWorkers = TableRegistry::getTableLocator()->get('BeanstalkWorkers');
        TableRegistry::getTableLocator()->set('Beanstalk.BeanstalkWorkers', $BeanstalkWorkers);
        $id = uniqid('testunit-');
        $BeanstalkWorkers->saveOrFail(
            $BeanstalkWorkers->newEntity(
                [
                    'name' => $id,
                ]
            )
        );

        $this->exec('WorkerManager listWorkers');
        $this->assertOutputContains($id);
    }

    public function testKill()
    {
        $BeanstalkWorkers = TableRegistry::getTableLocator()->get('BeanstalkWorkers');
        $entity = $BeanstalkWorkers->saveOrFail(
            $BeanstalkWorkers->newEntity(
                [
                    'name' => 'test',
                    'hostname' => 'testunit',
                    'pid' => 123,
                ]
            )
        );

        $this->exec('WorkerManager kill '.$entity->get('id'));
        $this->assertStringContainsString('testunit:123', file_get_contents($this->tmpOutputFile));
    }
}
