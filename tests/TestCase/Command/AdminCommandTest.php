<?php

namespace AsalaeCore\Test\TestCase\Command;

use AsalaeCore\Command\AdminCommand;
use AsalaeCore\Factory\Utility;
use AsalaeCore\Utility\Exec;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\Console\TestSuite\ConsoleIntegrationTestTrait;
use AsalaeCore\TestSuite\TestCase;
use Libriciel\Filesystem\Utility\Filesystem;

class AdminCommandTest extends TestCase
{
    use ConsoleIntegrationTestTrait;

    public $fixtures = [
        'app.Aros',
        'app.Acos',
        'app.ArosAcos',
        'app.OrgEntities',
        'app.TypeEntities',
        'app.Users',
    ];

    public function setUp(): void
    {
        parent::setUp();
        $this->useCommandRunner();
        if (is_dir(TMP_TESTDIR)) {
            Filesystem::remove(TMP_TESTDIR);
        }
        Filesystem::mkdir(TMP_TESTDIR);
        Configure::write('App.paths.administrators_json', TMP_TESTDIR.DS.'admins.json');
    }

    public function testmain()
    {
        $mock = $this->createMock(Exec::class); // pour le prompt password
        $mock->method('rawCommand')->will(
            $this->returnCallback(
                fn ($v) => strpos($v, 'OK') ? 'OK' : 'admin'
            )
        );
        Utility::set('Exec', $mock);

        /**
         * Ajout
         */
        $inputs = [
            AdminCommand::ACTION_ADD, // action
            'admin', // Identifiant
            'admin', // Nom
            '', // Courriel
            'o', // confirm mot de passe faible
            'n', // notifications
            'q', // quitter
        ];
        $this->exec('admin', $inputs);
        $this->assertFileExists(TMP_TESTDIR.DS.'admins.json');
        $json = json_decode(file_get_contents(TMP_TESTDIR.DS.'admins.json'), true);
        $this->assertArrayHasKey(0, $json);
        $this->assertArrayHasKey('username', $json[0]);
        $this->assertEquals('admin', $json[0]['username']);

        /**
         * Modification
         */
        $inputs = [
            AdminCommand::ACTION_EDIT, // action
            'admin', // Identifiant
            'admin', // Nom
            'admin@test.fr', // Courriel
            'o', // confirm mot de passe faible
            'n', // notifications
            'q', // quitter
        ];
        $this->cleanupConsoleTrait();
        $this->useCommandRunner();
        $this->exec('admin', $inputs);
        $json = json_decode(file_get_contents(TMP_TESTDIR.DS.'admins.json'), true);
        $this->assertArrayHasKey(0, $json);
        $this->assertArrayHasKey('email', $json[0]);
        $this->assertEquals('admin@test.fr', $json[0]['email']);

        /**
         * Modification (keep password)
         */
        $mock = $this->createMock(Exec::class); // pour le prompt password
        $mock->method('rawCommand')->will(
            $this->returnCallback(
                fn ($v) => strpos($v, 'OK') ? 'OK' : ''
            )
        );
        Utility::set('Exec', $mock);
        $this->cleanupConsoleTrait();
        $this->useCommandRunner();
        $this->exec('admin', $inputs);
        $json = json_decode(file_get_contents(TMP_TESTDIR.DS.'admins.json'), true);
        $this->assertArrayHasKey(0, $json);
        $this->assertArrayHasKey('email', $json[0]);
        $this->assertEquals('admin@test.fr', $json[0]['email']);

        /**
         * Suppression
         */
        $inputs = [
            AdminCommand::ACTION_DELETE, // action
            'admin', // Identifiant
            'y', // confirm
            'q', // quitter
        ];
        $this->cleanupConsoleTrait();
        $this->useCommandRunner();
        $this->exec('admin', $inputs);
        $json = json_decode(file_get_contents(TMP_TESTDIR.DS.'admins.json'), true);
        $this->assertArrayNotHasKey(0, $json);

        /**
         * Quit
         */
        $inputs = [
            AdminCommand::ACTION_QUIT, // action
        ];
        $this->cleanupConsoleTrait();
        $this->useCommandRunner();
        $this->exec('admin', $inputs);
        $json = json_decode(file_get_contents(TMP_TESTDIR.DS.'admins.json'), true);
        $this->assertArrayNotHasKey(0, $json);
    }

    public function testwebservice()
    {
        $this->exec('admin webservice wsadmin wspassword');
        $Users = TableRegistry::getTableLocator()->get('Users');
        $this->assertTrue($Users->exists(['username' => 'wsadmin']));
    }
}
