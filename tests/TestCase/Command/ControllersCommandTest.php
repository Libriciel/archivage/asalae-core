<?php

namespace AsalaeCore\Test\TestCase\Command;

use AsalaeCore\Model\Table\ArosAcosTable;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\Console\TestSuite\ConsoleIntegrationTestTrait;
use AsalaeCore\TestSuite\TestCase;
use Libriciel\Filesystem\Utility\Filesystem;

class ControllersCommandTest extends TestCase
{
    use ConsoleIntegrationTestTrait;

    public $fixtures = [
        'plugin.AsalaeCore.Acos',
        'plugin.AsalaeCore.Aros',
        'plugin.AsalaeCore.ArosAcos',
    ];

    /**
     * @var ArosAcosTable
     */
    public $Permissions;

    public function setUp(): void
    {
        parent::setUp();
        $this->useCommandRunner();
        if (is_dir(TMP_TESTDIR)) {
            Filesystem::remove(TMP_TESTDIR);
        }
        mkdir(TMP_TESTDIR);
        $json = TMP_TESTDIR.DS.'testunit-controllers.json';
        Configure::write('App.paths.controllers_rules', $json);
        Configure::write(
            'App.paths.controllers_group',
            $ctrlGroups = TMP_TESTDIR . DS . 'controllers_group.json'
        );
        file_put_contents($json, '{}');
        file_put_contents($ctrlGroups, json_encode(['foo', 'bar']));
        $this->Permissions = TableRegistry::getTableLocator()->get('ArosAcos');
        $this->Permissions->deleteAll([]);
    }

    public function tearDown(): void
    {
        parent::tearDown();
        if (is_dir(TMP_TESTDIR)) {
            Filesystem::remove(TMP_TESTDIR);
        }
    }

    public function testMain()
    {
        $this->exec('Controllers MyPlugin.Sample/test --interactive n');
        $this->assertFileExists(Configure::read('App.paths.controllers_rules'));
        $json = Configure::read('App.paths.controllers_rules');
        $data = json_decode(file_get_contents($json));
        $this->assertTrue(!empty($data->{'MyPlugin.Sample'}->test));
        $this->assertEquals('0', $data->{'MyPlugin.Sample'}->test->invisible ?? null);
        $this->assertEquals('0', $data->{'MyPlugin.Sample'}->test->accesParDefaut ?? null);
        $this->assertEquals('', $data->{'MyPlugin.Sample'}->test->commeDroits ?? null);

        file_put_contents($json, '{}');
        $this->cleanupConsoleTrait();
        $this->useCommandRunner();
        $this->exec('Controllers --interactive n --force', ['MyPlugin.Sample', 'test']);
        $data = json_decode(file_get_contents($json));
        $this->assertTrue(!empty($data->{'MyPlugin.Sample'}->test));

        file_put_contents($json, '{}');
        $this->assertCount(0, $this->Permissions->find());
        $this->cleanupConsoleTrait();
        $this->useCommandRunner();
        $this->exec('Controllers MyPlugin.Sample/test --invisible --accesParDefaut --interactive n');
        $data = json_decode(file_get_contents($json));
        $this->assertTrue(!empty($data->{'MyPlugin.Sample'}->test));
        $this->assertEquals('1', $data->{'MyPlugin.Sample'}->test->invisible ?? null);
        $this->assertEquals('1', $data->{'MyPlugin.Sample'}->test->accesParDefaut ?? null);
        $this->assertGreaterThan(0, $this->Permissions->find()->count());

        $this->cleanupConsoleTrait();
        $this->useCommandRunner();
        $this->exec('Controllers MyPlugin.Sample/add --commeDroits MyPlugin.Sample::test');
        $data = json_decode(file_get_contents($json));
        $this->assertTrue(!empty($data->{'MyPlugin.Sample'}->test));
        $this->assertTrue(!empty($data->{'MyPlugin.Sample'}->add));
        $this->assertEquals('MyPlugin.Sample::test', $data->{'MyPlugin.Sample'}->add->commeDroits ?? null);
    }

    public function testMainInteractive()
    {
        $inputs = [
            'Home', // controller
            'index', // action
            'n', // hérite
            'y', // visible
            'y', // accessible par défaut
            '0', // groupe
        ];
        $this->exec('Controllers --plugin MyPlugin', $inputs);
        $this->assertFileExists(Configure::read('App.paths.controllers_rules'));
        $controllers = json_decode(
            file_get_contents(
                Configure::read('App.paths.controllers_rules')
            ),
            true
        );
        $expected = [
            'Home' => [
                'index' => [
                    'invisible' => '0',
                    'accesParDefaut' => '1',
                    'commeDroits' => '',
                    'group' => 'foo'
                ]
            ]
        ];
        $this->assertEquals($expected, $controllers);

        $inputs = [
            'Admins', // controller
            'index', // action
            'y', // hérite
            'Home', // controller (héritage)
            'index', // action (héritage)
        ];
        $this->cleanupConsoleTrait();
        $this->useCommandRunner();
        $this->exec('Controllers --plugin MyPlugin', $inputs);
        $controllers = json_decode(
            file_get_contents(
                Configure::read('App.paths.controllers_rules')
            ),
            true
        );
        unset($controllers['Home']);
        $expected = [
            'Admins' => [
                'index' => [
                    'invisible' => '0',
                    'accesParDefaut' => '0',
                    'commeDroits' => 'Home::index'
                ]
            ],
        ];
        $this->assertEquals($expected, $controllers);
    }

    public function testget()
    {
        $inputs = [
            'Home/index',
        ];
        $this->exec('Controllers get --plugin MyPlugin', $inputs);
        $this->assertOutputContains('[]');

        $this->cleanupConsoleTrait();
        $this->useCommandRunner();
        $controllers = [
            'Home' => [
                'index' => [
                    'invisible' => '0',
                    'accesParDefaut' => '1',
                    'commeDroits' => '',
                    'group' => 'foo'
                ]
            ]
        ];
        file_put_contents(
            Configure::read('App.paths.controllers_rules'),
            json_encode($controllers)
        );
        $this->exec('Controllers get Home/index --plugin MyPlugin');
        $this->assertOutputContains(
            "[
  'invisible' => '0',
  'accesParDefaut' => '1',
  'commeDroits' => '',
  'group' => 'foo'
]"
        );
    }

    public function testlist()
    {
        $controllers = [
            'Home' => [
                'index' => [
                    'invisible' => '0',
                    'accesParDefaut' => '1',
                    'commeDroits' => '',
                    'group' => 'foo'
                ]
            ]
        ];
        file_put_contents(
            Configure::read('App.paths.controllers_rules'),
            json_encode($controllers)
        );
        $this->exec('Controllers list --plugin MyPlugin');
        $this->assertOutputContains('Home/index');
        $this->assertErrorContains('Admins/index');
    }
}
