<?php

namespace AsalaeCore\Test\TestCase\Middleware;

use AsalaeCore\Factory\Utility;
use AsalaeCore\Utility\Config;
use Cake\Core\Configure;
use Cake\Routing\Router;
use AsalaeCore\TestSuite\TestCase;
use Cake\TestSuite\IntegrationTestTrait;
use PHPUnit\Framework\MockObject\MockObject;

class MaintenanceMiddlewareTest extends TestCase
{
    use IntegrationTestTrait;

    private $initialNamespace;
    private $initialDir;
    private $initialTemplates;
    private $initialPtl;

    public function setUp(): void
    {
        parent::setUp();
        Utility::reset();
        $this->initialNamespace = Configure::read('App.namespace');
        $this->initialDir = Configure::read('App.dir');
        $this->initialTemplates = Configure::read('App.paths.templates');
        if ($this->initialNamespace === 'AsalaeCore') {
            Configure::write('App.namespace', 'MyPlugin');
        }
        $whiteList = $this->getWhitelistFilename();
        file_put_contents($whiteList, '/Admins/index');

        $this->initialPtl = Configure::read('App.paths.path_to_local_config');
        $ptl = sys_get_temp_dir().DS.'path_to_local.php';
        $local = sys_get_temp_dir().DS.'test_local.json';
        file_put_contents($ptl, '<?php return "'.$local.'";?>');
        file_put_contents($local, '{}');
        Configure::write('App.paths.path_to_local_config', $ptl);

        Configure::write('App.dir', 'tests'.DS.'MyPlugin'.DS.'src');
        Configure::write(
            'App.paths.templates',
            array_merge(
                $this->initialTemplates,
                [dirname(dirname(__DIR__)).DS.'MyPlugin'.DS.'src'.DS.'Template'.DS]
            )
        );
        Config::reset();
    }

    public function tearDown(): void
    {
        parent::tearDown();
        Configure::drop('Interruption');
        Utility::reset();
        Configure::write('App.namespace', $this->initialNamespace);
        Configure::write('App.dir', $this->initialDir);
        Configure::write('App.paths.templates', $this->initialTemplates);
        $whiteList = $this->getWhitelistFilename();
        if (is_file($whiteList)) {
            unlink($whiteList);
        }
        if (is_file($ptl = sys_get_temp_dir().DS.'path_to_local.php')) {
            unlink($ptl);
        }
        if (is_file($local = sys_get_temp_dir().DS.'test_local.json')) {
            unlink($local);
        }
        Configure::write('App.paths.path_to_local_config', $this->initialPtl);
        Config::reset();
    }

    private function getWhitelistFilename(): string
    {
        return sys_get_temp_dir() . DS . 'whitelist.conf';
    }

    public function testProcess()
    {
        $config = [
            'Interruption' => [
                'enabled' => false,
                'scheduled' => [
                    'begin' => null,
                    'end' => null,
                ],
                'periodic' => [
                    'begin' => null,
                    'end' => null,
                ],
                'enable_whitelist' => true,
                'whitelist_headers' => [
                    'X-Asalae-Webservice' => true,
                ],
                'message' => 'test',
                'whitelist_file' => $this->getWhitelistFilename(),
                'enable_workers' => false,
            ]
        ];
        $local = sys_get_temp_dir().DS.'test_local.json';
        file_put_contents($local, json_encode($config, JSON_UNESCAPED_SLASHES));
        /** @var \Cake\Routing\RouteCollection|MockObject $collection */
        $collection = $this->createMock('\Cake\Routing\RouteCollection');
        $collection->method('parseRequest')->willReturn(
            [
                'pass' => [],
                'controller' => 'Home',
                'action' => 'index',
                'plugin' => null,
                '_matchedRoute' => '/',
                '_route' => $collection,
            ]
        );
        Router::setRouteCollection($collection);
        $this->get('/');
        $this->assertNotEquals(503, $this->_response->getStatusCode());

        // Interruption immédiate
        $config['Interruption']['enabled'] = true;
        file_put_contents($local, json_encode($config, JSON_UNESCAPED_SLASHES));
        $this->get('/');
        $this->assertEquals(503, $this->_response->getStatusCode());
        $config['Interruption']['enabled'] = false;
        file_put_contents($local, json_encode($config, JSON_UNESCAPED_SLASHES));

        // Début d'interruption dans le passé (pas de fin)
        $config['Interruption']['scheduled']['begin'] = '1900-01-01T00:00:00';
        file_put_contents($local, json_encode($config, JSON_UNESCAPED_SLASHES));
        $this->get('/');
        $this->assertEquals(503, $this->_response->getStatusCode());

        // Fin d'interruption dans le passé
        $config['Interruption']['scheduled']['end'] = '1900-01-02T00:00:00';
        file_put_contents($local, json_encode($config, JSON_UNESCAPED_SLASHES));
        $this->get('/');
        $this->assertNotEquals(503, $this->_response->getStatusCode());

        // Début d'interruption dans le futur (pas de fin)
        $now = new \DateTime;
        $tomorow = (clone $now)->add(new \DateInterval('P1D'))->format(DATE_ISO8601);
        Configure::write('Interruption.scheduled.begin', $tomorow);
        Configure::write('Interruption.scheduled.end', null);
        $config['Interruption']['scheduled']['begin'] = $tomorow;
        $config['Interruption']['scheduled']['end'] = null;
        file_put_contents($local, json_encode($config, JSON_UNESCAPED_SLASHES));
        $this->get('/');
        $this->assertNotEquals(503, $this->_response->getStatusCode());

        // Arret journalier
        Utility::set(\DateTime::class, new \DateTime('12:00:00')); // midi
        $config['Interruption']['periodic']['begin'] = '11:00:00'; // 11h
        $config['Interruption']['periodic']['end'] = '13:00:00'; // 13h
        file_put_contents($local, json_encode($config, JSON_UNESCAPED_SLASHES));
        $this->get('/');
        $this->assertEquals(503, $this->_response->getStatusCode());

        // Hors arret journalier
        Utility::set(\DateTime::class, new \DateTime('14:00:00')); // 14h
        $this->get('/');
        $this->assertNotEquals(503, $this->_response->getStatusCode());

        // Arret journalier avec chevauchement de jours
        Utility::set(\DateTime::class, new \DateTime('23:45:00')); // 23h45
        $config['Interruption']['periodic']['begin'] = '23:30:00'; // 23h30
        $config['Interruption']['periodic']['end'] = '00:01:30'; // 01h30
        file_put_contents($local, json_encode($config, JSON_UNESCAPED_SLASHES));
        $this->get('/');
        $this->assertEquals(503, $this->_response->getStatusCode());

        // Arret journalier avec chevauchement de jours (de l'autre coté)
        Utility::set(\DateTime::class, new \DateTime('00:00:15')); // 00h15
        $this->get('/');
        $this->assertEquals(503, $this->_response->getStatusCode());

        // Accès header white-list
        $this->configRequest(['headers' => ['X-Asalae-Webservice' => 'true']]);
        $this->get('/');
        $this->assertNotEquals(503, $this->_response->getStatusCode());

        $collection = $this->createMock('\Cake\Routing\RouteCollection');
        $collection->method('parseRequest')->willReturn(
            [
                'pass' => [],
                'controller' => 'Admins',
                'action' => 'index',
                'plugin' => null,
                '_matchedRoute' => '/',
                '_route' => $collection,
            ]
        );
        Router::setRouteCollection($collection);
        // Accès admin technique
        $this->get('/admins');
        $this->assertNotEquals(503, $this->_response->getStatusCode());
    }
}
