<?php

namespace AsalaeCore\Test\TestCase\Error;

use AsalaeCore\Error\ContextDebugger;
use Cake\Core\Configure;
use Cake\Http\ServerRequest;
use Cake\Http\Uri;
use Cake\Routing\Router;
use Cake\Console\TestSuite\ConsoleIntegrationTestTrait;
use AsalaeCore\TestSuite\TestCase;
use Laminas\Diactoros\Stream;
use Laminas\Diactoros\UploadedFile;
use LogicException;
use ReflectionClass;
use ReflectionException;

class ContextDebuggerTest extends TestCase
{
    use ConsoleIntegrationTestTrait;

    public function testd()
    {
        Configure::write('debug', true);
        ContextDebugger::registerDebugFunction('d');
        ob_start();
        $filename = __FILE__;
        $line = __LINE__ + 1;
        d('test');
        $content = ob_get_clean();
        $this->assertStringContainsString(
            sprintf('%s (line %d)', basename($filename), $line),
            $content
        );
        $this->assertStringContainsString(
            "d('test');",
            $content
        );
    }

    public function testgetContext()
    {
        $result = ContextDebugger::getContext(__FILE__, __LINE__, 1);
        $this->assertStringContainsString(
            '$result = ContextDebugger::getContext(__FILE__, __LINE__, 1);',
            $result
        );
    }

    public function testexportVar()
    {
        $result = ContextDebugger::exportVar('test');
        $this->assertStringContainsString(
            '<span class=sf-dump-str title="4 characters">test</span>',
            $result
        );
        $result = ContextDebugger::exportVar('test', 'cli');
        $this->assertStringContainsString(
            "'test'",
            $result
        );
        $result = ContextDebugger::exportVar('test', 'other');
        $this->assertStringContainsString(
            '&#039;test&#039;',
            $result
        );
    }

    /**
     * Load routes for the application.
     *
     * If no application class can be found an exception will be raised.
     * Routes for plugins will *not* be loaded. Use `loadPlugins()` or use
     * `Cake\TestSuite\IntegrationTestCaseTrait` to better simulate all routes
     * and plugins being loaded.
     *
     * @param array|null $appArgs Constructor parameters for the application class.
     * @return void
     * @since 4.0.1
     */
    public function loadRoutes(?array $appArgs = null): void
    {
        $appArgs = $appArgs ?? [rtrim(CONFIG, DIRECTORY_SEPARATOR)];
        /** @psalm-var class-string */
        $className = Configure::read('App.namespace') . '\\Application';
        try {
            $reflect = new ReflectionClass($className);
            /** @var \Cake\Routing\RoutingApplicationInterface $app */
            $app = $reflect->newInstanceArgs($appArgs);
        } catch (ReflectionException $e) {
            throw new LogicException(sprintf('Cannot load "%s" to load routes from.', $className), 0, $e);
        }
        $builder = Router::createRouteBuilder('/');
        $app->routes($builder);
    }

    public function testlogRequest()
    {
        $this->loadRoutes();
        Configure::write('log_requests.enabled', true);
        Configure::write('log_requests.directory', TMP_TESTDIR);
        Configure::write('log_requests.controllers', ['Admins' => ['index']]);
        /** @noinspection PhpInternalEntityUsedInspection */
        $uri = $this->createMock(Uri::class);
        $uri->method('getPath')->willReturn('/phpunit/admins');
        $request = $this->createMock(ServerRequest::class);
        $request->method('getUri')->willReturn($uri);
        $request->method('getServerParams')->willReturn(['REMOTE_ADDR' => '127.0.0.1']);
        $request->method('getMethod')->willReturn('POST');
        $request->method('getRequestTarget')->willReturn('/admins');
        $request->method('getHeaders')->willReturn(
            [
                'Host' => [0 => 'current'],
                'Accept' => [0 => 'text/html']
            ]
        );
        $request->method('getHeaderLine')->willReturn('blabla; boundary=---123456');
        $body = new Stream('php://memory', 'rw');
        $uid = uniqid('uniq', true);
        $body->write(
            <<<EOT
---123456
Content-Disposition: form-data; name="myfield"

$uid
---123456
Content-Disposition: form-data; name="file"; filename="test.txt"
Content-Type: text/plain

a random content
EOT
        );
        $stream = tmpfile();
        fwrite($stream, 'a random content');
        $size = strlen('a random content');
        $request->method('getBody')->willReturn($body);
        $request->method('getParsedBody')
            ->willReturn(
                [
                    'myfield' => $uid,
                    'otherfield' => new UploadedFile($stream, $size, 0),
                ]
            );
        $filename = ContextDebugger::logRequest($request);
        $content = file_get_contents($filename);
        $this->assertStringContainsString('POST /admins', $content);
        $this->assertStringContainsString($uid, $content);
    }
}
