<?php

namespace AsalaeCore\Test\TestCase\MinkSuite;

use AsalaeCore\MinkSuite\MinkCase;
use AsalaeCore\TestSuite\IntegrationCronTrait;
use AsalaeCore\TestSuite\InvokePrivateTrait;
use AsalaeCore\TestSuite\TestCase;
use Behat\Mink\Element\NodeElement;
use Behat\Mink\Mink;
use Behat\Mink\Selector\SelectorsHandler;
use Behat\Mink\Session;
use Cake\Console\ConsoleIo;
use Cake\TestSuite\Stub\ConsoleInput;
use Cake\TestSuite\Stub\ConsoleOutput;
use Cake\Utility\Hash;
use DMore\ChromeDriver\ChromeDriver;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\MockObject\Stub\ReturnCallback as ReturnCallbackStub;

class MinkCaseDoTraitTest extends TestCase
{
    use IntegrationCronTrait;
    use InvokePrivateTrait;

    public $minkCase;

    public function setUp(): void
    {
        parent::setUp();
    }

    public function tearDown(): void
    {
        parent::tearDown();
    }

    private $values = [];
    private $clicks = [];
    private $waits = [];
    private $scripts = [];
    private $mocks = [];

    private function getNode(string $text, string $value = 'value', array $params = []): NodeElement
    {
        $node = $this->createMock(NodeElement::class);
        $default = [
            'setValue' => $this->returnCallback(fn ($v) => $this->values[] = $v),
            'click' => $this->returnCallback(fn () => $this->clicks[] = true),
            'getParent' => $node,
            'findAll' => [$node],
            'isVisible' => true,
            'getText' => $text,
            'getAttribute' => 'id',
            'getValue' => $value,
            'hasAttribute' => true,
            'hasClass' => true,
        ];
        $params = Hash::merge($default, $params);
        foreach ($params as $fn => $value) {
            if ($value instanceof ReturnCallbackStub) {
                $node->method($fn)->will($value);
            } elseif ($value === $node) {
                $node->method($fn)->willReturnSelf();
            } else {
                $node->method($fn)->willReturn($value);
            }
        }
        return $node;
    }

    /**
     * Donne une instance mocké de mink
     * @param array $params
     * @return MinkCase
     */
    private function getMink(array $params = []): Minkcase
    {
        $out = new ConsoleOutput();
        $err = new ConsoleOutput();
        $io = new ConsoleIo($out, $err, new ConsoleInput([]));

        $driverMink = $this->createMock(ChromeDriver::class);
        $sessionMink = $this->createMock(Session::class);
        $mink = $this->createMock(Mink::class);

        $this->values = [];
        $this->clicks = [];
        $this->waits = [];
        $this->scripts = [];

        $default = [
            'driverMink' => [
                'getCurrentUrl' => 'users/login',
                'find' => [$this->getNode('my_input')],
            ],
            'sessionMink' => [
                'getDriver' => $driverMink,
                'getSelectorsHandler' => new SelectorsHandler(),
                'wait' => $this->returnCallback(fn ($v, $cond) => $this->waits[] = [$v, $cond]),
                'executeScript' => $this->returnCallback(fn ($v) => $this->scripts[] = $v),
            ],
            'mink' => [
                'getSession' => $sessionMink,
            ],
        ];
        $params = Hash::merge($default, $params);

        $mocks = [
            'driverMink' => $driverMink,
            'sessionMink' => $sessionMink,
            'mink' => $mink,
        ];
        /** @var MockObject $mock */
        foreach ($mocks as $name => $mock) {
            foreach ($params[$name] as $fn => $value) {
                if ($value instanceof ReturnCallbackStub) {
                    $mock->method($fn)->will($value);
                } elseif ($value === $mock) {
                    $mock->method($fn)->willReturnSelf();
                } else {
                    $mock->method($fn)->willReturn($value);
                }
            }
            $this->mocks[$name] = $mock;
        }
        return new MinkCase('test', $mink, [], $io);
    }

    public function testdoLogin()
    {
        $minkCase = $this->getMink();
        $this->invokeMethod($minkCase, 'doLogin');
        $this->assertEquals(['admin', 'admin'], $this->values);
    }

    public function testdoClick()
    {
        $minkCase = $this->getMink();
        $this->invokeMethod($minkCase, 'doClick', ['.foo']);
        $this->assertCount(1, $this->clicks);
    }

    public function testwaitModalOpen()
    {
        $minkCase = $this->getMink();
        $this->invokeMethod($minkCase, 'waitModalOpen', [500]);
        $this->assertEquals(
            [
                [500, 'minkModalOpenned()'],
                [400, 'false'],
            ],
            $this->waits
        );
    }

    public function testwaitUploadComplete()
    {
        $minkCase = $this->getMink();
        $this->invokeMethod($minkCase, 'waitUploadComplete', [500]);
        $this->assertEquals(
            [
                [10, 'false'],
                [500, 'minkFileUploading === false'],
                [400, 'false'],
            ],
            $this->waits
        );
    }

    public function testwaitAjaxComplete()
    {
        $minkCase = $this->getMink();
        $this->invokeMethod($minkCase, 'waitAjaxComplete', [500]);
        $this->assertEquals(
            [
                [500, 'minkAjaxRunning === false && customAjaxRunning === false'],
                [500, 'false'],
            ],
            $this->waits
        );
    }

    public function testwaitModalClose()
    {
        $minkCase = $this->getMink();
        $this->invokeMethod($minkCase, 'waitModalClose', [500]);
        $this->assertEquals(
            [
                [500, "$('.modal:visible').length === 0"],
            ],
            $this->waits
        );
    }

    public function testsetFormFieldValue()
    {
        $minkCase = $this->getMink();
        $this->invokeMethod($minkCase, 'setFormFieldValue', ['my_input', 'value']);
        $this->assertEquals(['value'], $this->values);
    }

    public function testsetNamelessFormFieldValue()
    {
        $minkCase = $this->getMink();
        $this->invokeMethod($minkCase, 'setNamelessFormFieldValue', ['my_input', 'value']);
        $this->assertEquals(['value'], $this->values);
    }

    public function testsetFormSelectValueByText()
    {
        $minkCase = $this->getMink();
        $this->invokeMethod($minkCase, 'setFormSelectValueByText', ['my_input', 'my_input']);
        $this->assertEquals(['value'], $this->values);
    }

    public function testsetFormFieldValues()
    {
        $minkCase = $this->getMink();
        $this->invokeMethod($minkCase, 'setFormFieldValues', ['my_input', ['value1', 'value2']]);
        $this->assertCount(2, $this->scripts);
    }

    public function testsetFormFieldValueById()
    {
        $minkCase = $this->getMink();
        $this->invokeMethod($minkCase, 'setFormFieldValueById', ['my_input', 'value']);
        $this->assertEquals(['value'], $this->values);
    }

    public function testsetInputValue()
    {
        $minkCase = $this->getMink();
        $this->invokeMethod($minkCase, 'setInputValue', ['css', '.my_input', 'value']);
        $this->assertEquals(['value'], $this->values);
    }

    public function testrequiredIdName()
    {
        $minkCase = $this->getMink();
        $this->invokeMethod($minkCase, 'requiredIdName', ['.my_input']);
        $this->assertCount(1, $this->scripts);
        $this->assertEquals(
            [
                [10, 'false'],
            ],
            $this->waits
        );
    }

    public function testremoveRequiredIdName()
    {
        $minkCase = $this->getMink();
        $this->invokeMethod($minkCase, 'removeRequiredIdName');
        $this->assertCount(1, $this->scripts);
    }

    public function testsubmitModal()
    {
        $minkCase = $this->getMink();
        $this->invokeMethod($minkCase, 'submitModal', [500]);
        $this->assertCount(1, $this->clicks);
        $this->assertEquals(
            [
                [500, 'minkAjaxRunning === false && customAjaxRunning === false'],
                [500, 'false'],
            ],
            $this->waits
        );
    }

    public function testcloseModal()
    {
        $minkCase = $this->getMink();
        $this->invokeMethod($minkCase, 'closeModal', [500]);
        $this->assertCount(1, $this->clicks);
        $this->assertEquals(
            [
                [500, 'minkAjaxRunning === false && customAjaxRunning === false'],
                [500, 'false'],
            ],
            $this->waits
        );
    }

    public function testdoClickUserMenu()
    {
        $minkCase = $this->getMink();
        $this->invokeMethod($minkCase, 'doClickUserMenu', ['my_input']);
        $this->assertCount(2, $this->clicks);
    }

    public function testdoClickMenu()
    {
        $params = [
            'driverMink' => [
                'find' => [
                    $this->getNode(
                        'title',
                        'value',
                        [
                            'findAll' => [$this->getNode('my_input')],
                            'getParent' => $this->getNode('my_input'),
                        ]
                    )
                ]
            ]
        ];
        $minkCase = $this->getMink($params);
        $this->invokeMethod($minkCase, 'doClickMenu', ['title', 'my_input']);
        $this->assertCount(2, $this->clicks);
    }
}
