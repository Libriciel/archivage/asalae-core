<?php

namespace AsalaeCore\Test\TestCase\Controller\Component;

use AsalaeCore\Controller\Component\Reader\ImageReader;
use AsalaeCore\Controller\Component\Reader\OpenDocumentReader;
use AsalaeCore\Controller\Component\Reader\PdfReader;
use AsalaeCore\Controller\Component\Reader\ReaderInterface;
use AsalaeCore\Controller\Component\ReaderComponent;
use Cake\Core\Configure;
use Cake\Http\Exception\NotImplementedException;
use AsalaeCore\TestSuite\TestCase;

class ReaderComponentTest extends TestCase
{
    use ComponentTestTrait;

    /**
     * @var ReaderComponent $component
     */
    public $component = null;

    /**
     * @var string|ReaderComponent
     */
    public $componentClassname = ReaderComponent::class;

    public function setUp(): void
    {
        parent::setUp();
        Configure::write(
            [
                'Readers' => [
                    'pdf' => PdfReader::class,
                    'open' => OpenDocumentReader::class,
                    'image' => ImageReader::class,
                ],
            ]
        );
    }

    public function testGetReader()
    {
        $this->assertNotEmpty(ReaderComponent::getReaders());
    }

    public function testCanRead()
    {
        $this->setComponent();
        $this->assertTrue($this->component->canRead('application/pdf-a'));
        $this->assertFalse($this->component->canRead('application/foo'));
    }

    public function testGet()
    {
        $this->assertInstanceOf(
            ReaderInterface::class,
            $this->setComponent()->get('application/pdf-a')
        );
        $e = null;
        try {
            $this->component->get('application/foo');
        } catch (NotImplementedException $e) {
        }
        $this->assertInstanceOf(NotImplementedException::class, $e);
    }
}
