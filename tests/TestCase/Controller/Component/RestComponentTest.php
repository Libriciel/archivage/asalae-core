<?php

namespace AsalaeCore\Test\TestCase\Controller\Component;

use AsalaeCore\Controller\Component\AclComponent;
use AsalaeCore\Controller\Component\RestComponent;
use Cake\Controller\ComponentRegistry;
use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;
use AsalaeCore\TestSuite\TestCase;

class RestComponentTest extends TestCase
{
    use ComponentTestTrait;

    /**
     * @var RestComponent $component
     */
    public $component = null;

    /**
     * @var string|RestComponent
     */
    public $componentClassname = RestComponent::class;

    public $fixtures = [
        'app.Users',
        'app.Aros',
        'app.ArosAcos',
        'app.Acos',
    ];

    private function createAcl(string $path)
    {
        $loc = TableRegistry::getTableLocator();
        $Acos = $loc->get('Acos');
        /** @var EntityInterface[] $acos */
        $acos = [];
        $parent_id = null;
        foreach (explode('/', $path) as $alias) {
            $aco = $Acos->find()->where(
                [
                    'alias' => $alias,
                    'parent_id'.($parent_id ? '' : ' IS') => $parent_id,
                ]
            )->first();
            if (!$aco) {
                $aco = $Acos->newEntity(
                    [
                        'parent_id' => $parent_id,
                        'model' => $parent_id ? $acos[$parent_id]->get('alias') : null,
                        'alias' => $alias,
                    ]
                );
                $Acos->save($aco);
            }
            $acos[$aco->id] = $aco;
            $parent_id = $aco->id;
        }
    }

    private function allow(string $path)
    {
        $registry = new ComponentRegistry();
        $Acl = new AclComponent($registry);
        $Acl->allow(['model' => 'Users', 'foreign_key' => 1], $path, '*');
    }

    public function testInitialize()
    {
        $this->setComponent(['action' => 'api']);
        $this->assertFalse($this->controller->isAutoRenderEnabled());
    }

    public function testRoute()
    {
        $this->setComponent(['action' => 'api']);
        $this->component->setApiActions(['foo' => 'bar']);
        $route = $this->component->route();
        $this->assertFalse($route);

        $this->component->setApiActions(['default' => ['*' => ['function' => 'apiAll']]]);
        $route = $this->component->route();
        $expected = [
            'callback' => [$this->controller, 'apiAll'],
            'args' => null,
            'function' => 'apiAll',
            'auth' => false,
            'accept' => '*',
        ];
        $this->assertEquals($expected, $route);
    }
}
