<?php

namespace AsalaeCore\Test\TestCase\Controller\Component;

use AsalaeCore\Controller\Component\FilterComponent;
use AsalaeCore\Factory\Utility;
use Cake\ORM\TableRegistry;
use AsalaeCore\TestSuite\TestCase;

class FilterComponentTest extends TestCase
{
    use ComponentTestTrait;

    /**
     * @var string|FilterComponent
     */
    public $componentClassname = FilterComponent::class;

    /**
     * @var FilterComponent $component
     */
    public $component = null;

    public $fixtures = [
        'app.SavedFilters',
        'app.Filters',
        'app.Notifications',
    ];

    public function setUp(): void
    {
        parent::setUp();
        Utility::set('Notify', 'AsalaeCore\Test\Mock\FakeClass');
    }

    public function testLoadAndSave()
    {
        $loc = TableRegistry::getTableLocator();
        $SavedFilters = $loc->get('SavedFilters');
        $Filters = $loc->get('Filters');

        $this->setComponent(['session' => ['Auth' => ['id' => 1]]]);
        $this->component->loadAndSave();
        $this->assertTrue($this->controller->viewBuilder()->hasVar('savedFilters'));

        // tentative de sauvegarde sans filtres
        $initialCount = $SavedFilters->find()->count();
        $this->setComponent(
            [
                'query' => ['SaveFilterSelect' => '-1', 'SaveFilterName' => 'save-test'],
                'session' => ['Auth' => ['id' => 1]]
            ]
        );
        $this->component->loadAndSave();
        $this->assertCount($initialCount, $SavedFilters->find());

        // sauvegarde d'un filtre
        $this->setComponent(
            [
                'query' => ['SaveFilterSelect' => '-1', 'SaveFilterName' => 'save-test', 'Foo' => ['bar' => ['baz']]],
                'session' => ['Auth' => ['id' => 1]]
            ]
        );
        $this->component->loadAndSave();
        $actualCount = $initialCount +1;
        $this->assertCount($actualCount, $SavedFilters->find());
        $this->assertTrue($Filters->exists(['key' => 'Foo[bar][0]', 'value' => 'baz']));

        // sauvegarde d'un filtre de même nom
        $this->setComponent(
            [
                'query' => ['SaveFilterSelect' => '-1', 'SaveFilterName' => 'save-test', 'Foo' => ['bar' => ['test']]],
                'session' => ['Auth' => ['id' => 1]]
            ]
        );
        $this->component->loadAndSave();
        $this->assertCount($actualCount, $SavedFilters->find());
        $this->assertFalse($Filters->exists(['key' => 'Foo[bar][0]', 'value' => 'test']));

        // sauvegarde d'un filtre de même nom, emplacement different
        $this->setComponent(
            [
                'query' => [
                    'SaveFilterSelect' => '123',
                    'SaveFilterName' => 'save-test',
                    'Foo' => [
                        'bar' => ['test'],
                    ],
                    'OverrideFilter' => 'true',
                ],
                'session' => ['Auth' => ['id' => 1]]
            ]
        );
        $this->component->loadAndSave();
        $this->assertCount($actualCount, $SavedFilters->find());
        $this->assertFalse($Filters->exists(['key' => 'Foo[bar][0]', 'value' => 'test']));

        // écrase le filtre de même nom
        $this->setComponent(
            [
                'query' => [
                    'SaveFilterSelect' => $SavedFilters->find()->order(['id' => 'desc'])->first()->id,
                    'SaveFilterName' => 'save-test',
                    'Foo' => [
                        'bar' => ['test'],
                    ],
                    'OverrideFilter' => 'true',
                ],
                'session' => ['Auth' => ['id' => 1]]
            ]
        );
        $this->component->loadAndSave();
        $this->assertCount($actualCount, $SavedFilters->find());
        $this->assertTrue($Filters->exists(['key' => 'Foo[bar][0]', 'value' => 'test']));

        // même filtre
        $this->setComponent(
            [
                'query' => [
                    'SaveFilterSelect' => $SavedFilters->find()->order(['id' => 'desc'])->first()->id,
                    'SaveFilterName' => 'save-test',
                    'Foo' => [
                        'bar' => ['test'],
                    ],
                    'OverrideFilter' => 'true',
                ],
                'session' => ['Auth' => ['id' => 1]]
            ]
        );
        $this->component->loadAndSave();
        $this->assertCount($actualCount, $SavedFilters->find());
        $this->assertTrue($Filters->exists(['key' => 'Foo[bar][0]', 'value' => 'test']));

        // tentative d'écraser le filtre de même nom sans filtre
        $this->setComponent(
            [
                'query' => [
                    'SaveFilterSelect' => $SavedFilters->find()->order(['id' => 'desc'])->first()->id,
                    'SaveFilterName' => 'save-test',
                    'OverrideFilter' => 'true',
                ],
                'session' => ['Auth' => ['id' => 1]]
            ]
        );
        $this->component->loadAndSave();
        $actualCount--;
        $this->assertCount($actualCount, $SavedFilters->find());
    }
}
