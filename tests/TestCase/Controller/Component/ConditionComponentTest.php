<?php /** @noinspection PhpInternalEntityUsedInspection */

namespace AsalaeCore\Test\TestCase\Controller\Component;

use AsalaeCore\Controller\Component\ConditionComponent;
use AsalaeCore\Controller\Controller;
use Cake\Controller\ComponentRegistry;
use Cake\Core\Configure;
use Cake\Database\Driver;
use Cake\Database\Expression\QueryExpression;
use Cake\Database\ValueBinder;
use Cake\Datasource\ConnectionManager;
use Cake\Http\Exception\BadRequestException;
use Cake\Http\Response;
use Cake\Http\ServerRequest;
use AsalaeCore\TestSuite\TestCase;
use DateTime;

class ConditionComponentTest extends TestCase
{
    use ComponentTestTrait;

    /**
     * @var string|ConditionComponent
     */
    public $componentClassname = ConditionComponent::class;

    /**
     * @var ConditionComponent $component
     */
    public $component = null;

    public function setUp(): void
    {
        parent::setUp();
        $this->setComponent(
            [
                'session' => [
                    'Auth' => [
                        'cookies' => ['table-favorite-test-test-table-3' => '3'],
                    ]
                ],
                'controller' => 'mock-controlle',
                'cookies' => [
                    'table-favorite-test-test-table-1' => '1',
                    'table-favorite-test-test-table-2' => '2',
                    'table-favorite-mock-controlle-test-table-123' => '123',
                ],
            ]
        );
        Configure::write('App.defaultLocale', 'fr_FR');
    }

    public function testIlike()
    {
        $this->component->driverName = 'mysql';
        $conditions = $this->component->ilike('Test.foo', '*foo?bar');
        $expected = ['Test.foo LIKE' => '%foo_bar'];
        $this->assertEquals($expected, $conditions);

        $this->component->driverName = 'pgsql';
        $conditions = $this->component->ilike('Test.foo', '*foo?bar');
        $expected = ['Test.foo ILIKE' => '%foo_bar'];
        $this->assertEquals($expected, $conditions);
    }

    public function testIn()
    {
        $conditions = $this->component->in('Test.foo', []);
        $expected = ['0 = 1'];
        $this->assertEquals($expected, $conditions);

        $conditions = $this->component->in('Test.foo', ['foo', 'bar']);
        $expected = ['Test.foo IN' => ['foo', 'bar']];
        $this->assertEquals($expected, $conditions);
    }

    public function testNotIn()
    {
        $conditions = $this->component->notIn('Test.foo', []);
        $expected = ['1 = 1'];
        $this->assertEquals($expected, $conditions);

        $conditions = $this->component->notIn('Test.foo', ['foo', 'bar']);
        $expected = ['Test.foo NOT IN' => ['foo', 'bar']];
        $this->assertEquals($expected, $conditions);
    }

    public function testDateBetweenFields()
    {
        $date = new DateTime('2000-01-01');
        $conditions = $this->component->dateBetweenFields($date, 'Test.foo', 'Test.bar');
        $expected = [
            'date(Test.foo) <=' => '2000-01-01',
            'date(Test.bar) >=' => '2000-01-01',
        ];
        $this->assertEquals($expected, $conditions);

        $date = '2000-01-01';
        $conditions = $this->component->dateBetweenFields(
            $date,
            'Test.foo',
            'Test.bar',
            'Y-m-d'
        );
        $expected = [
            'date(Test.foo) <=' => '2000-01-01',
            'date(Test.bar) >=' => '2000-01-01',
        ];
        $this->assertEquals($expected, $conditions);

        $date = '01/01/2000';
        $conditions = $this->component->dateBetweenFields(
            $date,
            'Test.foo',
            'Test.bar',
            'auto'
        );
        $expected = [
            'date(Test.foo) <=' => '2000-01-01',
            'date(Test.bar) >=' => '2000-01-01',
        ];
        $this->assertEquals($expected, $conditions);
    }

    public function testDateBetween()
    {
        $dateMin = new DateTime('2020-09-07');
        $dateMax = new DateTime('2020-09-08');
        $conditions = $this->component->dateBetween('Test.foo', $dateMin, $dateMax);
        $this->assertInstanceOf(QueryExpression::class, $conditions);
        $expected = "date(Test.foo) BETWEEN date('2020-09-07') AND date('2020-09-08')";
        $this->assertEquals($expected, $conditions->sql(new ValueBinder));
    }

    public function testDateOperator()
    {
        $date = new DateTime('2000-01-01');
        $conditions = $this->component->dateOperator('Test.foo', '<=', $date);
        $expected = ['date(Test.foo) <=' => '2000-01-01'];
        $this->assertEquals($expected, $conditions);

        $date = '2000-01-01';
        $conditions = $this->component->dateOperator(
            'Test.foo',
            '<=',
            $date,
            'Y-m-d'
        );
        $expected = ['date(Test.foo) <=' => '2000-01-01'];
        $this->assertEquals($expected, $conditions);

        $date = '01/01/2000';
        $conditions = $this->component->dateOperator(
            'Test.bar',
            '>',
            $date,
            'auto'
        );
        $expected = ['date(Test.bar) >' => '2000-01-01'];
        $this->assertEquals($expected, $conditions);

        $e = null;
        try {
            $this->component->dateOperator(
                'Test.foo',
                '!=',
                '01/01/2000'
            );
        } catch (BadRequestException $e) {
        }
        $this->assertInstanceOf(BadRequestException::class, $e);

        $e = null;
        try {
            $this->component->dateOperator(
                'Test.foo',
                '=',
                '01/01/0000'
            );
        } catch (BadRequestException $e) {
        }
        $this->assertInstanceOf(BadRequestException::class, $e);

        $conditions = $this->component->dateOperator(
            'Test.foo',
            'P7D',
            $date
        );
        $this->assertInstanceOf(QueryExpression::class, $conditions);
        $dateMin = (new DateTime)->modify('-7 days')->format('Y-m-d');
        $dateMax = (new DateTime)->format('Y-m-d');
        $expected = "date(Test.foo) BETWEEN date('$dateMin') AND date('$dateMax')";
        $this->assertEquals($expected, $conditions->sql(new ValueBinder));

        $conditions = $this->component->dateOperator(
            'Test.foo',
            'P1M',
            $date
        );
        $this->assertInstanceOf(QueryExpression::class, $conditions);
        $dateMin = (new DateTime)->modify('-1 month')->format('Y-m-d');
        $dateMax = (new DateTime)->format('Y-m-d');
        $expected = "date(Test.foo) BETWEEN date('$dateMin') AND date('$dateMax')";
        $this->assertEquals($expected, $conditions->sql(new ValueBinder));

        $conditions = $this->component->dateOperator(
            'Test.foo',
            'P1Y',
            $date
        );
        $this->assertInstanceOf(QueryExpression::class, $conditions);
        $dateMin = (new DateTime)->modify('-1 year')->format('Y-m-d');
        $dateMax = (new DateTime)->format('Y-m-d');
        $expected = "date(Test.foo) BETWEEN date('$dateMin') AND date('$dateMax')";
        $this->assertEquals($expected, $conditions->sql(new ValueBinder));

        $conditions = $this->component->dateOperator(
            'Test.foo',
            'today',
            $date
        );
        $now = (new DateTime)->format('Y-m-d');
        $expected = ['date(Test.foo)' => $now];
        $this->assertEquals($expected, $conditions);

        $conditions = $this->component->dateOperator(
            'Test.foo',
            'week',
            $date
        );
        $this->assertInstanceOf(QueryExpression::class, $conditions);
        $dateMin = (new DateTime)->modify('monday this week')->format('Y-m-d');
        $dateMax = (new DateTime)->modify('sunday this week')->format('Y-m-d');
        $expected = "date(Test.foo) BETWEEN date('$dateMin') AND date('$dateMax')";
        $this->assertEquals($expected, $conditions->sql(new ValueBinder));

        $conditions = $this->component->dateOperator(
            'Test.foo',
            'month',
            $date
        );
        $this->assertInstanceOf(QueryExpression::class, $conditions);
        $dateMin = (new DateTime)->modify('first day of this month')->format('Y-m-d');
        $dateMax = (new DateTime)->modify('last day of this month')->format('Y-m-d');
        $expected = "date(Test.foo) BETWEEN date('$dateMin') AND date('$dateMax')";
        $this->assertEquals($expected, $conditions->sql(new ValueBinder));

        $conditions = $this->component->dateOperator(
            'Test.foo',
            'year',
            $date
        );
        $this->assertInstanceOf(QueryExpression::class, $conditions);
        $dateMin = (new DateTime)->modify('first day of january')->format('Y-m-d');
        $dateMax = (new DateTime)->modify('last day of december')->format('Y-m-d');
        $expected = "date(Test.foo) BETWEEN date('$dateMin') AND date('$dateMax')";
        $this->assertEquals($expected, $conditions->sql(new ValueBinder));
    }

    public function testInFavorites()
    {
        $conditions = $this->component->inFavorites('Test.foo');
        $expected = ['Test.foo IN' => [1, 2, 3]];
        $this->assertEquals($expected, $conditions);

        $conditions = $this->component->inFavorites('Bar', 'Bar');
        $expected = ['0 = 1'];
        $this->assertEquals($expected, $conditions);

        $conditions = $this->component->inFavorites('foo', 'auto');
        $expected = ['foo IN' => [123]];
        $this->assertEquals($expected, $conditions);
    }

    public function testOrderByFavorites()
    {
        $order = $this->component->orderByFavorites();
        $expected = ['MockControlle.id NOT IN (123)' => 'asc'];
        $this->assertEquals($expected, $order);

        $order = $this->component->orderByFavorites('desc', 'Test');
        $expected = ['Test.id NOT IN (1, 2, 3)' => 'desc'];
        $this->assertEquals($expected, $order);

        $request = new ServerRequest;
        $response = new Response;
        $this->controller = $this->getMockBuilder(Controller::class)
            ->setConstructorArgs([$request, $response])
            ->onlyMethods([])
            ->getMock();
        $registry = new ComponentRegistry($this->controller);
        $this->component = new ConditionComponent($registry);

        $order = $this->component->orderByFavorites();
        $expected = [];
        $this->assertEquals($expected, $order);
    }

    public function testDateDiff()
    {
        $this->component->driverName = 'pgsql';
        $diff = $this->component->dateDiff(
            'Test.foo',
            'Test.bar',
            ConditionComponent::DATE_GAP_TYPE_DAY
        );
        $expected = "DATE_PART('day', Test.foo) - DATE_PART('day', Test.bar)";
        $this->assertEquals($expected, $diff);

        $this->component->driverName = 'sqlite';
        $diff = $this->component->dateDiff(
            'Test.foo',
            'Test.bar',
            ConditionComponent::DATE_GAP_TYPE_DAY
        );
        $expected = "strftime('%d', Test.foo) - strftime('%d', Test.bar)";
        $this->assertEquals($expected, $diff);

        $this->component->driverName = 'sqlite';
        $diff = $this->component->dateDiff(
            'Test.foo',
            'Test.bar',
            ConditionComponent::DATE_GAP_TYPE_MONTH
        );
        $expected = "strftime('%m', Test.foo) - strftime('%m', Test.bar)";
        $this->assertEquals($expected, $diff);

        $this->component->driverName = 'sqlite';
        $diff = $this->component->dateDiff(
            'Test.foo',
            'Test.bar',
            ConditionComponent::DATE_GAP_TYPE_YEAR
        );
        $expected = "strftime('%Y', Test.foo) - strftime('%Y', Test.bar)";
        $this->assertEquals($expected, $diff);

        $this->component->driverName = 'mysql';
        $diff = $this->component->dateDiff(
            'Test.foo',
            'Test.bar',
            ConditionComponent::DATE_GAP_TYPE_DAY
        );
        $expected = "DATEDIFF(day, Test.foo, Test.bar)";
        $this->assertEquals($expected, $diff);
    }

    /**
     * Test la reconnection du driver
     */
    public function test__construct()
    {
        $conn = ConnectionManager::get('test');
        /** @var Driver $driver */
        $driver = $conn->getDriver();
        $driver->disconnect();
        $registry = new ComponentRegistry($this->controller);
        $this->component = new ConditionComponent($registry);
        $this->assertInstanceOf(ConditionComponent::class, $this->component);
    }

    public function testRegex()
    {
        $this->component->driverName = 'pgsql';
        $cond = $this->component->regex('Test.foo', '^foo[bar]+$');
        $expected = ['Test.foo ~' => '^foo[bar]+$'];
        $this->assertEquals($expected, $cond);

        $this->component->driverName = 'mysql';
        $cond = $this->component->regex('Test.foo', '^foo[bar]+$');
        $expected = ['Test.foo REGEXP' => '^foo[bar]+$'];
        $this->assertEquals($expected, $cond);

        $this->component->driverName = 'sqlite';
        $cond = $this->component->regex('Test.foo', '^foo[bar]+$');
        $expected = ['Test.foo REGEXP' => '^foo[bar]+$'];
        $this->assertEquals($expected, $cond);
    }

    public function testStringAgg()
    {
        $this->component->driverName = 'pgsql';
        $out = $this->component->stringAgg('Test.foo');
        $expected = "string_agg(Test.foo, ', ')";
        $this->assertEquals($expected, $out);

        $out = $this->component->stringAgg('Test.foo', '/', 'Test.foo desc');
        $expected = "string_agg(Test.foo, '/' order by Test.foo desc)";
        $this->assertEquals($expected, $out);

        $this->component->driverName = 'sqlite';
        $out = $this->component->stringAgg('Test.foo');
        $expected = "group_concat(Test.foo, ', ')";
        $this->assertEquals($expected, $out);
    }
}
