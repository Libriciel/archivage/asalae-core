<?php

namespace AsalaeCore\Test\TestCase\Controller\Component;

use AsalaeCore\Controller\Component\JstableComponent;
use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;
use AsalaeCore\TestSuite\TestCase;

class JstableComponentTest extends TestCase
{
    use ComponentTestTrait;

    /**
     * @var JstableComponent $component
     */
    public $component = null;

    /**
     * @var string|JstableComponent
     */
    public $componentClassname = JstableComponent::class;

    public $fixtures = [
        'app.Aros',
        'app.Users',
        'app.Roles',
    ];

    public function testConfigurablePagination()
    {
        $session = [
            'Auth' => [
                'cookies' => [
                    'table-generator-config-table-test-max-per-page' => 'foo',
                ],
            ],
        ];
        $this->setComponent(['session' => $session]);
        $this->component->configurablePagination('test');
        $this->assertEquals([], $this->controller->paginate);

        $session = [
            'Auth' => [
                'cookies' => [
                    'table-generator-config-table-test-max-per-page' => '100',
                ],
            ],
        ];
        $this->setComponent(['session' => $session]);
        $this->component->configurablePagination('test');
        $this->assertEquals(['limit' => '100'], $this->controller->paginate);

        $cookies = [
            'table-generator-config-table-test-max-per-page' => '123',
        ];
        $this->setComponent(['cookies' => $cookies]);
        $this->component->configurablePagination('test');
        $this->assertEquals(['limit' => '123'], $this->controller->paginate);
    }

// FIXME passage à cake 4.3 - fixtures Aros impossible: You cannot configure "Aros", it already exists in the registry.
//    public function testSaveCookies()
//    {
//        $session = ['Auth' => ['id' => 1]];
//        $cookies = [
//            'foo' => 'bar',
//            'special' => 'null',
//        ];
//        $this->setComponent(['cookies' => $cookies, 'session' => $session]);
//        $this->component->saveCookies();
//
//        $Users = TableRegistry::getTableLocator()->get('Users');
//        $user = $Users->get(1);
//        $expected = '{"foo":"bar"}';
//        $this->assertEquals($expected, $user->get('cookies'));
//    }

    public function testAjaxPagination()
    {
        $this->setComponent(['environment' => ['HTTP_X_REQUESTED_WITH' => 'XMLHttpRequest']]);
        $this->component->ajaxPagination();
        $this->assertEquals('ajax_test', $this->controller->viewBuilder()->getTemplate());
    }
}
