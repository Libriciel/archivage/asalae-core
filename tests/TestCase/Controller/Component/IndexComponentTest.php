<?php

namespace AsalaeCore\Test\TestCase\Controller\Component;

use AsalaeCore\Controller\Component\IndexComponent;
use AsalaeCore\Factory\Utility;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use AsalaeCore\TestSuite\TestCase;
use Exception;

class IndexComponentTest extends TestCase
{
    use ComponentTestTrait;

    /**
     * @var string|IndexComponent
     */
    public $componentClassname = IndexComponent::class;

    /**
     * @var IndexComponent $component
     */
    public $component = null;

    public $fixtures = [
        'app.Agreements',
        'app.Crons',
        'app.Filters',
        'app.SavedFilters',
    ];

    public function setUp(): void
    {
        parent::setUp();
        Utility::set('Notify', 'AsalaeCore\Test\Mock\FakeClass');
        Configure::write('App.defaultLocale', 'fr_FR');
    }

    public function testInit()
    {
        $this->setComponent(
            [
                'environment' => ['HTTP_X_REQUESTED_WITH' => 'XMLHttpRequest'],
                'session' => ['Auth' => ['id' => 1]]
            ]
        );
        $initialPaginate = $this->controller->paginate;
        $this->component->init();
        $this->assertNotEquals($initialPaginate, $this->controller->paginate);
    }

    public function testFilter()
    {
        $this->setComponent(['query' => ['id' => 1, 'sort' => 'favorite']]);
        $loc = TableRegistry::getTableLocator();
        $Crons = $loc->get('Crons');
        $query = $Crons->find();
        $this->component->setQuery($query)
            ->filter('id');
        $sql = $query->sql();
        $this->assertTextContains('id =', $sql);

        $query = $Crons->find();
        $this->setComponent(
            [
                'query' => [
                    'dateoperator_next' => ['<'],
                    'next' => [
                        '30/01/2020'
                    ]
                ]
            ]
        );
        $this->component->setQuery($query)
            ->filter('next', IndexComponent::FILTER_DATEOPERATOR, 'next');
        $sql = $query->sql();
        $this->assertTextContains('date(next) <', $sql);

        $query = $Crons->find();
        $this->setComponent(
            [
                'query' => ['favorite' => 'true'],
                'controller' => 'mock-controlle',
                'cookies' => [
                    'table-favorite-mock-controlle-test-table-123' => '123',
                ],
            ]
        );
        $this->component->setQuery($query)
            ->filter('favorite', IndexComponent::FILTER_FAVORITES);
        $sql = $query->sql();
        $this->assertTextContains('id in', $sql);

        $query = $Crons->find();
        $this->setComponent(['query' => ['id' => ['%1%']]]);
        $this->component->setQuery($query)
            ->filter('id', IndexComponent::FILTER_ILIKE);
        $sql = $query->sql();
        $this->assertTextContains('like', $sql);

        $query = $Crons->find();
        $this->setComponent(['query' => ['id' => [1, 2, 3]]]);
        $this->component->setQuery($query)
            ->filter('id', IndexComponent::FILTER_IN);
        $sql = $query->sql();
        $this->assertTextContains('id in', $sql);

        $query = $Crons->find();
        $this->setComponent(['query' => ['locked' => ['true']]]);
        $this->component->setQuery($query)
            ->filter('locked', IndexComponent::FILTER_IS, 'locked');
        $sql = $query->sql();
        $this->assertTextContains('locked', $sql);

        $query = $Crons->find();
        $this->setComponent(['query' => ['locked' => ['true']]]);
        $this->component->setQuery($query)
            ->filter('locked', IndexComponent::FILTER_IS_NOT, 'locked');
        $sql = $query->sql();
        $this->assertTextContains('locked', $sql);

        $query = $Crons->find();
        $this->setComponent(['query' => ['id' => [1, 2, 3]]]);
        $this->component->setQuery($query)
            ->filter('id', IndexComponent::FILTER_NOT_IN);
        $sql = $query->sql();
        $this->assertTextContains('id not in', $sql);

        $query = $Crons->find();
        $this->setComponent(['query' => ['id' => [1]]]);
        $this->component->setQuery($query)
            ->filter('id', IndexComponent::FILTER_LESS_OR_EQUAL);
        $sql = $query->sql();
        $this->assertTextContains('id <=', $sql);

        $query = $Crons->find();
        $this->setComponent(['query' => ['id' => [1]]]);
        $this->component->setQuery($query)
            ->filter(
                'id',
                function ($id) {
                    return ['id = '.($id * 10)];
                }
            );
        $sql = $query->sql();
        $this->assertTextContains('id = 10', $sql);

        $e = null;
        try {
            $this->setComponent(['query' => ['id' => [1]]]);
            $this->component->filter('id');
        } catch (Exception $e) {
        }
        $this->assertInstanceOf(Exception::class, $e);

        $query = $Crons->find();
        $this->setComponent(
            [
                'query' => [
                    'next' => [
                        '<'
                    ]
                ]
            ]
        );
        $this->component->setQuery($query)
            ->filter('next', IndexComponent::FILTER_DATECOMPARENOW, 'next');
        $sql = $query->sql();
        $this->assertTextContains('date(next) <', $sql);

        $Agreements = $loc->get('Agreements');
        $query = $Agreements->find();
        $this->setComponent(
            [
                'query' => [
                    'max_size_per_transfer' => [
                        [
                            'operator' => '>',
                            'mult' => 1024 * 1024,
                            'value' => 1000,
                        ]
                    ]
                ]
            ]
        );
        $this->component->setQuery($query)
            ->filter('max_size_per_transfer', IndexComponent::FILTER_SIZEOPERATOR);
        $sql = $query->sql();
        $this->assertTextContains('max_size_per_transfer >', $sql);

        $query = $Agreements->find();
        $this->setComponent(
            [
                'query' => [
                    'max_size_per_transfer' => [
                        [
                            'operator' => '>',
                            'value' => 1000,
                        ]
                    ]
                ]
            ]
        );
        $this->component->setQuery($query)
            ->filter('max_size_per_transfer', IndexComponent::FILTER_COUNTOPERATOR);
        $sql = $query->sql();
        $this->assertTextContains('max_size_per_transfer >', $sql);

        $query = $Agreements->find();
        $this->setComponent(
            [
                'query' => [
                    'identifier' => [
                        [
                            'agr-test01',
                        ]
                    ]
                ]
            ]
        );
        $this->component->setQuery($query)
            ->filter('identifier');
        $sql = $query->sql();
        $this->assertTextContains('identifier =', $sql);
    }
}
