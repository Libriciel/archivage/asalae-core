<?php

namespace AsalaeCore\Test\TestCase\Controller\Component;

use AsalaeCore\Controller\Component\AjaxPaginatorComponent;
use Cake\ORM\TableRegistry;
use AsalaeCore\TestSuite\TestCase;

class AjaxPaginatorComponentTest extends TestCase
{
    use ComponentTestTrait;

    /**
     * @var AjaxPaginatorComponent $component
     */
    public $component = null;

    /**
     * @var string|AjaxPaginatorComponent
     */
    public $componentClassname = AjaxPaginatorComponent::class;

    public $fixtures = [
        'app.Crons',
        'app.CronExecutions',
    ];

    public function testPaginate()
    {
        $this->setComponent(
            [
                'query' => ['sort' => 'name', 'direction' => 'desc']
            ]
        );
        $model = TableRegistry::getTableLocator()->get('Crons');
        $query = $model->query();
        $results = $this->component->paginate($query);
        $this->assertGreaterThanOrEqual(1, $results->count());

        $this->setComponent(
            [
                'query' => ['sort' => 'CronExecutions.state', 'direction' => 'asc']
            ]
        );
        $query = $model->query()->innerJoinWith('CronExecutions');
        $results = $this->component->paginate($query);
        $this->assertGreaterThanOrEqual(1, $results->count());
    }

    public function testJson()
    {
        $this->setComponent(
            [
                'query' => ['sort' => 'name', 'direction' => 'desc'],
                'request_headers' => [
                    'X-Paginator-Count' => 'true',
                ]
            ]
        );
        $model = TableRegistry::getTableLocator()->get('Crons');
        $query = $model->query();
        $results = $this->component->json($query);
        $json = json_decode($results->getBody()->getContents());
        $this->assertGreaterThanOrEqual(1, count($json));
    }
}
