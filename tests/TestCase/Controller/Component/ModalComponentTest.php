<?php

namespace AsalaeCore\Test\TestCase\Controller\Component;

use AsalaeCore\Controller\Component\ModalComponent;
use Cake\ORM\TableRegistry;
use AsalaeCore\TestSuite\TestCase;

class ModalComponentTest extends TestCase
{
    use ComponentTestTrait;

    /**
     * @var ModalComponent $component
     */
    public $component = null;

    /**
     * @var string|ModalComponent
     */
    public $componentClassname = ModalComponent::class;

    public $fixtures = [
        'app.Crons',
        'app.CronExecutions',
    ];

    public function testSuccess()
    {
        $this->setComponent()->success();
        $this->assertEquals('true', $this->controller->getResponse()->getHeaderLine('X-Asalae-Success'));

        $this->setComponent()->success(false);
        $this->assertEquals('false', $this->controller->getResponse()->getHeaderLine('X-Asalae-Success'));
    }

    public function testFail()
    {
        $this->setComponent()->fail();
        $this->assertEquals('false', $this->controller->getResponse()->getHeaderLine('X-Asalae-Success'));
    }

    public function testSave()
    {
        $Crons = TableRegistry::getTableLocator()->get('Crons');
        $cron = $Crons->get(1);
        $callbacked = false;
        $this->setComponent()->save(
            $Crons,
            $cron,
            ['foo' => 'bar'],
            ['checkExisting' => false],
            function ($v) use (&$callbacked) {
                $callbacked = true;
                return $v;
            }
        );
        $this->assertTrue($callbacked);
        $this->assertEquals('true', $this->controller->getResponse()->getHeaderLine('X-Asalae-Success'));
        $this->assertTrue($this->component->lastSaveIsSuccess());

        $cron->set('biz');
        $cron->setError('biz', 'bar');
        $callbacked = false;
        $this->setComponent()->save(
            $Crons,
            $cron,
            ['foo' => 'bar'],
            ['checkExisting' => false],
            function ($v) use (&$callbacked) {
                $callbacked = true;
                return $v;
            }
        );
        $this->assertFalse($callbacked);
        $this->assertEquals('false', $this->controller->getResponse()->getHeaderLine('X-Asalae-Success'));
        $this->assertFalse($this->component->lastSaveIsSuccess());
    }

    public function testStep()
    {
        $this->setComponent()->step('foo("bar")');
        $this->assertEquals('foo("bar")', $this->controller->getResponse()->getHeaderLine('X-Asalae-Step'));
    }
}
