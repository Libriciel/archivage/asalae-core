<?php
namespace AsalaeCore\Test\TestCase\Controller\Component;

use AsalaeCore\Controller\Controller;
use Authentication\Controller\Component\AuthenticationComponent;
use Authorization\Identity;
use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\Http\Response;
use Cake\Http\ServerRequest;
use Cake\Http\Session;
use AsalaeCore\TestSuite\TestCase;
use Cake\Utility\Hash;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * Trait ComponentTestTrait
 * @package AsalaeCore\Test\TestCase\Controller\Component
 * @mixin TestCase
 * @property string|Component componentClassname
 * @property Component component
 */
trait ComponentTestTrait
{

    /**
     * @var MockObject|Controller
     */
    public $controller = null;

    /**
     * Initialize un component
     * @param array $options
     * @return Component
     */
    private function setComponent(array $options = []): Component
    {
        $sessionData = $options['session'] ?? [];
        $session = $this->createMock(Session::class);
        $session->method('read')->will(
            $this->returnCallback(
                function ($data) use ($sessionData) {
                    return $data ? Hash::get($sessionData, $data) : $sessionData;
                }
            )
        );
        $request = new ServerRequest(
            [
                'query' => $options['query'] ?? [],
                'cookies' => $options['cookies'] ?? [],
                'params' => [
                    'controller' => $options['controller'] ?? 'Tests',
                    'action' => $options['action'] ?? 'test',
                    '?' => $options['query'] ?? []
                ],
                'environment' => $options['environment'] ?? [],
                'session' => $session,
                'post' => $options['post'] ?? [],
            ]
        );
        $response = new Response();
        foreach ($options['response_headers'] ?? [] as $header => $value) {
            $response = $response->withHeader($header, $value);
        }
        foreach ($options['request_headers'] ?? [] as $header => $value) {
            $request = $request->withHeader($header, $value);
        }
        $identity = $this->createMock(Identity::class);
        $identity->method('getOriginalData')->willReturn(['id' => 1]);
        $request = $request->withAttribute('identity', $identity);
        $this->controller = new Controller($request, $response);
        $this->controller->cookies = array_merge( // rempli par JstableComponent::saveCookies
            Hash::get($sessionData, 'Auth.cookies', []),
            $options['cookies'] ?? []
        );
        $this->controller->Authentication = $this->createMock(AuthenticationComponent::class);
        $this->controller->Authentication->method('getIdentityData')->willReturn(1);
        $registry = new ComponentRegistry($this->controller);
        $component = $this->componentClassname;
        $this->component = new $component($registry);
        $registry->set(basename($this->componentClassname, 'Component'), $this->component);
        return $this->component;
    }
}
