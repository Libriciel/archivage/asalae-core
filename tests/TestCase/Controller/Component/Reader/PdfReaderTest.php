<?php

namespace AsalaeCore\Test\TestCase\Controller\Component\Reader;

use AsalaeCore\Controller\Component\Reader\OpenDocumentReader;
use AsalaeCore\Controller\Component\Reader\PdfReader;
use Cake\Controller\Controller;
use AsalaeCore\TestSuite\TestCase;

class PdfReaderTest extends TestCase
{
    public function testCanRead()
    {
        $this->assertFalse(PdfReader::canRead('application/json'));
        $this->assertTrue(PdfReader::canRead('application/pdf'));
    }

    public function testSetUrl()
    {
        $reader = new OpenDocumentReader(new Controller);
        $reader->setDownloadUrl('/foo');
        $reponse = $reader->getResponse();
        $this->assertEquals(302, $reponse->getStatusCode());
        $this->assertTextContains('foo', $reponse->getHeaderLine('Location'));
    }
}
