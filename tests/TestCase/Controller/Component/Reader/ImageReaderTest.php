<?php

namespace AsalaeCore\Test\TestCase\Controller\Component\Reader;

use AsalaeCore\Controller\Component\Reader\ImageReader;
use Cake\Controller\Controller;
use AsalaeCore\TestSuite\TestCase;

class ImageReaderTest extends TestCase
{
    public function testCanRead()
    {
        $this->assertFalse(ImageReader::canRead('application/json'));
        $this->assertTrue(ImageReader::canRead('image/png'));
    }

    public function testSetUrl()
    {
        $reader = new ImageReader(new Controller);
        $reader->setDownloadUrl('/foo');
        $reponse = $reader->getResponse();
        $this->assertEquals(302, $reponse->getStatusCode());
        $this->assertTextContains('foo', $reponse->getHeaderLine('Location'));
    }
}
