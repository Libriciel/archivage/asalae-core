<?php

namespace AsalaeCore\Test\TestCase\Controller\Component\Reader;

use AsalaeCore\Controller\Component\Reader\OpenDocumentReader;
use AsalaeCore\TestSuite\TestCase;

class OpenDocumentReaderTest extends TestCase
{
    public function testCanRead()
    {
        $this->assertFalse(OpenDocumentReader::canRead('application/json'));
        $this->assertTrue(OpenDocumentReader::canRead('application/vnd.oasis.opendocument.spreadsheet'));
    }
}
