<?php
namespace AsalaeCore\Test\TestCase\Controller;

use AsalaeCore\Controller\Component\AclComponent;
use Authentication\Controller\Component\AuthenticationComponent;
use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\Controller\Controller;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\Http\Response;
use Cake\Http\ServerRequest;
use Cake\Http\Session;
use Cake\ORM\TableRegistry;
use AsalaeCore\TestSuite\TestCase;
use Cake\Utility\Hash;
use Exception;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * Trait ComponentTestTrait
 * @package AsalaeCore\Test\TestCase\Controller\Component
 * @mixin TestCase
 * @property string|Component componentClassname
 * @property Component component
 */
trait ControllerTestTrait
{

    /**
     * Initialize un controlleur
     * @param string|Controller $classname
     * @param array             $options
     * @return MockObject|Controller
     * @throws Exception
     */
    private function setController(string $classname, array $options = [])
    {
        $sessionData = $options['session'] ?? [];
        $session = $this->createMock(Session::class);
        $session->method('read')->will(
            $this->returnCallback(
                function ($data) use ($sessionData) {
                    return Hash::get($sessionData, $data);
                }
            )
        );
        $request = new ServerRequest(
            [
                'query' => $options['query'] ?? [],
                'cookies' => $options['cookies'] ?? [],
                'params' => [
                    'controller' => $options['controller'] ?? 'Tests',
                    'action' => $options['action'] ?? 'test',
                    '?' => $options['query'] ?? [],
                ] + ($options['params'] ?? []),
                'environment' => $options['environment'] ?? [],
                'session' => $session,
                'post' => $options['post'] ?? [],
            ]
        );
        $response = new Response();
        foreach ($options['response_headers'] ?? [] as $header => $value) {
            $response = $response->withHeader($header, $value);
        }
        foreach ($options['request_headers'] ?? [] as $header => $value) {
            $request = $request->withHeader($header, $value);
        }
        $this->controller = new $classname($request, $response);
        $this->controller->Authentication = $this->createMock(AuthenticationComponent::class);
        $this->controller->Authentication->method('setIdentity');
        $this->controller->Authentication->method('getIdentityData')->willReturn(1);
        $this->controller->loadComponent('Acl');
        if (method_exists($this->controller, 'beforeFilter')) {
            $this->controller->beforeFilter(new Event('Controller.initialize', $this->controller));
        }
        return $this->controller;
    }

    private function createAcl(string $path)
    {
        $loc = TableRegistry::getTableLocator();
        $loc->clear();
        $Acos = $loc->get('Acos');
        $loc->get('Aros');
        $loc->get('ArosAcos');
        /** @var EntityInterface[] $acos */
        $acos = [];
        $parent_id = null;
        foreach (explode('/', $path) as $alias) {
            $aco = $Acos->find()->where(
                [
                    'alias' => $alias,
                    'parent_id'.($parent_id ? '' : ' IS') => $parent_id,
                ]
            )->first();
            if (!$aco) {
                $aco = $Acos->newEntity(
                    [
                        'parent_id' => $parent_id,
                        'model' => $parent_id ? $acos[$parent_id]->get('alias') : null,
                        'alias' => $alias,
                    ]
                );
                $Acos->save($aco);
            }
            $acos[$aco->id] = $aco;
            $parent_id = $aco->id;
        }
    }

    private function allow(string $path)
    {
        $registry = new ComponentRegistry();
        $Acl = new AclComponent($registry);
        $Acl->allow(['model' => 'Users', 'foreign_key' => 1], $path, '*');
    }
}
