<?php /** @noinspection PhpHierarchyChecksInspection - bug, pas de valeur parente sur `public $controller` */

namespace AsalaeCore\Test\TestCase\Controller;

use AsalaeCore\TestSuite\TestCase;
use Cake\Http\Exception\BadRequestException;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\MethodNotAllowedException;
use Cake\Http\Response;
use Cake\ORM\TableRegistry;
use MyPlugin\Controller\WebservicesController;

class ApiTraitTest extends TestCase
{
    use ControllerTestTrait;

    /**
     * @var WebservicesController
     */
    public $controller;

    public $fixtures = [
        'app.Acos',
        'app.Aros',
        'app.ArosAcos',
        'app.SavedFilters',
        'app.Sessions',
        'app.Notifications',
        'app.Users',
    ];

    public function testApi()
    {
        $this->createAcl('api/Users/test');
        $this->allow('api/Users');

        /**
         * GET all
         */
        $this->setController(
            WebservicesController::class,
            [
                'controller' => 'Users',
                'action' => 'api',
                'request_headers' => [
                    'Authorization' => 'Basic: '.base64_encode('testunit:testunit'),
                    'Accept' => 'application/json',
                ],
                'environment' => [
                    'REQUEST_METHOD' => 'GET'
                ]
            ]
        );
        $response = $this->controller->api();
        $this->assertInstanceOf(Response::class, $response);
        $body = $response->getBody();
        $body->rewind();
        $json = json_decode($body->getContents(), true);
        $this->assertGreaterThanOrEqual(1, count($json));

        /**
         * GET id=1
         */
        $this->setController(
            WebservicesController::class,
            [
                'controller' => 'Users',
                'action' => 'api',
                'params' => [
                    'pass' => ['1']
                ],
                'request_headers' => [
                    'Authorization' => 'Basic: '.base64_encode('testunit:testunit'),
                    'Accept' => 'application/json',
                ],
                'environment' => [
                    'REQUEST_METHOD' => 'GET'
                ]
            ]
        );
        $response = $this->controller->api(1);
        $this->assertInstanceOf(Response::class, $response);
        $body = $response->getBody();
        $body->rewind();
        $json = json_decode($body->getContents(), true);
        $this->assertArrayHasKey('id', $json);

        /**
         * PUT id=1 (change username)
         */
        $this->setController(
            WebservicesController::class,
            [
                'controller' => 'Users',
                'action' => 'api',
                'params' => [
                    'pass' => ['1']
                ],
                'request_headers' => [
                    'Authorization' => 'Basic: '.base64_encode('testunit:testunit'),
                    'Accept' => 'application/json',
                ],
                'environment' => [
                    'REQUEST_METHOD' => 'PUT'
                ],
                'post' => [
                    'username' => $username = uniqid('testunit-'),
                ]
            ]
        );
        $response = $this->controller->api(1);
        $this->assertInstanceOf(Response::class, $response);
        $body = $response->getBody();
        $body->rewind();
        $json = json_decode($body->getContents(), true);
        $this->assertArrayHasKey('id', $json);
        $Users = TableRegistry::getTableLocator()->get('Users');
        $this->assertTrue($Users->exists(['username' => $username]));
        $Users->updateAll(['username' => 'testunit'], ['id' => 1]);

        /**
         * POST
         */
        $this->setController(
            WebservicesController::class,
            [
                'controller' => 'Users',
                'action' => 'api',
                'request_headers' => [
                    'Authorization' => 'Basic: '.base64_encode('testunit:testunit'),
                    'Accept' => 'application/json',
                ],
                'environment' => [
                    'REQUEST_METHOD' => 'POST'
                ],
                'post' => [
                    'username' => $username = uniqid('testunit-'),
                ]
            ]
        );
        $response = $this->controller->api();
        $this->assertInstanceOf(Response::class, $response);
        $body = $response->getBody();
        $body->rewind();
        $json = json_decode($body->getContents(), true);
        $this->assertArrayHasKey('id', $json);
        $this->assertTrue($Users->exists(['id' => $json['id'], 'username' => $username]));

        /**
         * GET /api/Users/test
         */
        $this->setController(
            WebservicesController::class,
            [
                'controller' => 'Users',
                'action' => 'test',
                'params' => [
                    'pass' => ['test']
                ],
                'request_headers' => [
                    'Authorization' => 'Basic: '.base64_encode('testunit:testunit'),
                    'Accept' => 'application/json',
                ],
                'environment' => [
                    'REQUEST_METHOD' => 'GET'
                ],
            ]
        );
        $response = $this->controller->api(1);
        $this->assertInstanceOf(Response::class, $response);
        $body = $response->getBody();
        $body->rewind();
        $this->assertTextContains('ok', $body->getContents());

        /**
         * POST /api/Users/test (Webservices::testrender())
         */
        $this->setController(
            WebservicesController::class,
            [
                'controller' => 'Users',
                'action' => 'test',
                'params' => [
                    'pass' => ['test']
                ],
                'request_headers' => [
                    'Authorization' => 'Basic: '.base64_encode('testunit:testunit'),
                    'Accept' => 'application/json',
                ],
                'environment' => [
                    'REQUEST_METHOD' => 'POST'
                ],
            ]
        );
        $response = $this->controller->api(1);
        $this->assertEquals('test', $response);

        /**
         * DELETE id=1
         */
        $this->setController(
            WebservicesController::class,
            [
                'controller' => 'Users',
                'action' => 'api',
                'params' => [
                    'pass' => ['1']
                ],
                'request_headers' => [
                    'Authorization' => 'Basic: '.base64_encode('testunit:testunit'),
                    'Accept' => 'application/json',
                ],
                'environment' => [
                    'REQUEST_METHOD' => 'DELETE'
                ],
            ]
        );
        $response = $this->controller->api(1);
        $this->assertInstanceOf(Response::class, $response);
        $body = $response->getBody();
        $body->rewind();
        $json = json_decode($body->getContents(), true);
        $this->assertArrayHasKey('id', $json);
        $this->assertFalse($Users->exists(['id' => $json['id']]));
    }

    public function testForbidden()
    {
        $this->createAcl('api/Users/forbidden');
        $this->allow('api/Users');
        $this->setController(
            WebservicesController::class,
            [
                'controller' => 'Users',
                'action' => 'api',
                'params' => [
                    'pass' => ['forbidden']
                ],
                'request_headers' => [
                    'Authorization' => 'Basic: '.base64_encode('testunit:testunit'),
                    'Accept' => 'application/json',
                ],
                'environment' => [
                    'REQUEST_METHOD' => 'GET'
                ],
            ]
        );
        $this->expectException(ForbiddenException::class);
        $this->controller->api();
    }

    public function testBadRequest()
    {
        $this->createAcl('api/Users');
        $this->allow('api/Users');
        $this->setController(
            WebservicesController::class,
            [
                'controller' => 'Users',
                'action' => 'api',
                'params' => [
                    'pass' => ['1']
                ],
                'request_headers' => [
                    'Authorization' => 'Basic: '.base64_encode('testunit:testunit'),
                    'Accept' => 'application/json',
                ],
                'environment' => [
                    'REQUEST_METHOD' => 'DELETE' // interdit
                ],
            ]
        );
        $this->expectException(BadRequestException::class);
        $this->controller->api();
    }

    public function testMethodNotAllowedException()
    {
        $this->createAcl('api/Users');
        $this->allow('api/Users');
        $this->setController(
            WebservicesController::class,
            [
                'controller' => 'Users',
                'action' => 'api',
                'request_headers' => [
                    'Authorization' => 'Basic: '.base64_encode('testunit:testunit'),
                    'Accept' => 'application/json',
                ],
                'environment' => [
                    'REQUEST_METHOD' => 'UPDATE'
                ],
            ]
        );
        $this->expectException(MethodNotAllowedException::class);
        $this->controller->api();
    }
}
