<?php

namespace AsalaeCore\Test\TestCase\Controller;

use AsalaeCore\Model\Entity\SavedFilter;
use AsalaeCore\TestSuite\TestCase;
use Cake\ORM\TableRegistry;
use Cake\Routing\Route\Route;
use Cake\Routing\Router;
use MyPlugin\Controller\WebservicesController;

class FiltersControllerTraitTest extends TestCase
{
    use ControllerTestTrait;

    /**
     * @var WebservicesController
     */
    public $controller = null;

    public $fixtures = [
        'app.Filters',
        'app.SavedFilters',
    ];

    public function testAjaxRedirectUrl()
    {
        $this->setController(
            WebservicesController::class,
            ['environment' => ['HTTP_X_REQUESTED_WITH' => 'XMLHttpRequest']]
        );
        $route = new Route('{controller}/{action}');
        Router::getRouteCollection()->add($route);
        $reponse = $this->controller->ajaxRedirectUrl(1);
        $this->assertEquals(302, $reponse->getStatusCode());
    }

    public function testAjaxDeleteSave()
    {
        $this->setController(
            WebservicesController::class,
            ['environment' => ['HTTP_X_REQUESTED_WITH' => 'XMLHttpRequest']]
        );
        $SavedFilters = TableRegistry::getTableLocator()->get('SavedFilters');
        $this->assertTrue($SavedFilters->exists(['id' => 1]));
        $this->controller->ajaxDeleteSave(1);
        $this->assertFalse($SavedFilters->exists(['id' => 1]));
    }

    public function testAjaxGetFilters()
    {
        $this->setController(
            WebservicesController::class,
            ['environment' => ['HTTP_X_REQUESTED_WITH' => 'XMLHttpRequest']]
        );
        $this->controller->ajaxGetFilters(1);
        $viewVars = $this->controller->viewBuilder()->getVars();
        $this->assertNotEmpty($viewVars);
        $this->assertTrue(isset($viewVars['savedFilters']));
        $this->assertInstanceOf(SavedFilter::class, $viewVars['savedFilters']);
    }

    public function testAjaxNewSave()
    {
        $this->setController(
            WebservicesController::class,
            [
                'environment' => [
                    'HTTP_X_REQUESTED_WITH' => 'XMLHttpRequest',
                    'REQUEST_METHOD' => 'POST',
                ],
                'request_headers' => [
                    'Authorization' => 'Basic: '.base64_encode('testunit:testunit'),
                    'Accept' => 'application/json',
                ],
                'post' => [
                    'savename' => 'save',
                    'controller' => 'ctrl',
                    'action' => 'act',
                    'myfilter' => ['mysub' => 'myvalue'],
                ]
            ]
        );
        $SavedFilters = TableRegistry::getTableLocator()->get('SavedFilters');
        $initialCount = $SavedFilters->find()->count();
        $response = $this->controller->ajaxNewSave();
        $body = $response->getBody();
        $body->rewind();
        $this->assertNotEmpty(json_decode($body->getContents()));
        $this->assertGreaterThan($initialCount, $SavedFilters->find()->count());
    }

    public function testAjaxOverwrite()
    {

        $this->setController(
            WebservicesController::class,
            [
                'environment' => [
                    'HTTP_X_REQUESTED_WITH' => 'XMLHttpRequest',
                    'REQUEST_METHOD' => 'POST',
                ],
                'request_headers' => [
                    'Authorization' => 'Basic: '.base64_encode('testunit:testunit'),
                    'Accept' => 'application/json',
                ],
                'post' => [
                    'savename' => 'filter-name', // existing name (fixture)
                    'controller' => 'ctrl',
                    'action' => 'act',
                    'myfilter' => ['mysub' => 'myvalue'],
                ]
            ]
        );
        $SavedFilters = TableRegistry::getTableLocator()->get('SavedFilters');
        $initialCount = $SavedFilters->find()->count();
        $response = $this->controller->ajaxOverwrite(1);
        $body = $response->getBody();
        $body->rewind();
        $this->assertNotEmpty(json_decode($body->getContents()));
        $this->assertEquals($initialCount, $SavedFilters->find()->count());
    }
}
