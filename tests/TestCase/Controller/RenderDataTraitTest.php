<?php

namespace AsalaeCore\Test\TestCase\Controller;

use AsalaeCore\Controller\RenderDataTrait;
use AsalaeCore\TestSuite\InvokePrivateTrait;
use AsalaeCore\TestSuite\TestCase;
use Cake\Http\Exception\NotAcceptableException;
use Cake\Http\Response;
use Cake\ORM\TableRegistry;
use MyPlugin\Controller\WebservicesController;

class RenderDataTraitTest extends TestCase
{
    use ControllerTestTrait, InvokePrivateTrait;

    /**
     * @var WebservicesController
     */
    public $controller = null;

    public $fixtures = [
        'app.Users',
    ];

    private function getStringBody($response)
    {
        if ($response instanceof Response) {
            $body = $response->getBody();
            $body->rewind();
            return $body->getContents();
        }
        return $response;
    }

    public function testRenderJson()
    {
        $this->setController(WebservicesController::class);
        /** @var Response $response */
        $response = $this->invokeMethod($this->controller, 'renderJson', ['{"foo":"bar"}']);
        $this->assertEquals('{"foo":"bar"}', $this->getStringBody($response));
        $this->assertEquals('application/json', $response->getType());
    }

    public function testRenderDataToJson()
    {
        $this->setController(WebservicesController::class);
        /** @var Response $response */
        $response = $this->invokeMethod($this->controller, 'renderDataToJson', [['foo' => 'bar']]);
        $this->assertEquals('{"foo":"bar"}', $this->getStringBody($response));
        $this->assertEquals('application/json', $response->getType());
    }

    public function testRenderXml()
    {
        $this->setController(WebservicesController::class);
        /** @var Response $response */
        $response = $this->invokeMethod($this->controller, 'renderXml', ['<root><foo>bar</foo></root>']);
        $this->assertEquals('<root><foo>bar</foo></root>', $this->getStringBody($response));
        $this->assertEquals('application/xml', $response->getType());
    }

    public function testRenderHtml()
    {
        $this->setController(WebservicesController::class);
        /** @var Response $response */
        $response = $this->invokeMethod($this->controller, 'renderHtml', [['foo' => 'bar']]);
        // WebservicesController::render hardcoded output test
        $this->assertEquals('test', $this->getStringBody($response));
        $this->assertEquals('text/html', $response->getType());
    }

    public function testRenderByAccepts()
    {
        $this->setController(
            WebservicesController::class,
            ['request_headers' => ['Accept' => 'application/json']]
        );
        $request = $this->controller->getRequest();
        $response = $this->controller->getResponse();
        $accepts = $response->mapType($request->parseAccept());
        $accepts = $accepts ? current($accepts) : [];
        /** @var Response $response */
        $response = $this->invokeMethod(
            $this->controller,
            'renderByAccepts',
            [$accepts, ['foo' => 'bar']]
        );
        // WebservicesController::render hardcoded output test
        $this->assertEquals("{\n    \"foo\": \"bar\"\n}", $this->getStringBody($response));
        $this->assertEquals('application/json', $response->getType());

        $Users = TableRegistry::getTableLocator()->get('Users');
        $results = $Users->find()->all();
        $this->setController(
            WebservicesController::class,
            ['request_headers' => ['Accept' => 'application/html']]
        );
        $request = $this->controller->getRequest();
        $response = $this->controller->getResponse();
        $accepts = $response->mapType($request->parseAccept());
        $accepts = $accepts ? current($accepts) : [];
        /** @var Response $response */
        $response = $this->invokeMethod(
            $this->controller,
            'renderByAccepts',
            [$accepts, $results]
        );
        // WebservicesController::render hardcoded output test
        $this->assertEquals('test', $this->getStringBody($response));
        $this->assertEquals('text/html', $response->getType());

        $results = $Users->find()->toArray();
        $this->setController(
            WebservicesController::class,
            ['request_headers' => ['Accept' => 'application/xml']]
        );
        $request = $this->controller->getRequest();
        $response = $this->controller->getResponse();
        $accepts = $response->mapType($request->parseAccept());
        $accepts = $accepts ? current($accepts) : [];
        /** @var Response $response */
        $response = $this->invokeMethod(
            $this->controller,
            'renderByAccepts',
            [$accepts, $results]
        );
        // WebservicesController::render hardcoded output test
        $str = $this->getStringBody($response);
        $this->assertTextContains('<Tests>', $str); // root xml
        $this->assertTextContains('<Entity>', $str); // repeating result node
        $this->assertTextContains('<id>1</id>', $str); // field
        $this->assertEquals('application/xml', $response->getType());

        $this->setController(
            WebservicesController::class,
            ['request_headers' => ['Accept' => 'application/xml']]
        );
        $request = $this->controller->getRequest();
        $response = $this->controller->getResponse();
        $accepts = $response->mapType($request->parseAccept());
        $accepts = $accepts ? current($accepts) : [];
        /** @var Response $response */
        $response = $this->invokeMethod(
            $this->controller,
            'renderByAccepts',
            [$accepts, 'done']
        );
        // WebservicesController::render hardcoded output test
        $str = $this->getStringBody($response);
        $this->assertTextContains('<message>', $str); // root xml
        $this->assertTextContains('done', $str); // field
        $this->assertEquals('application/xml', $response->getType());
    }

    public function testRenderData()
    {
        $Users = TableRegistry::getTableLocator()->get('Users');
        $results = $Users->find()->all();
        $this->setController(
            WebservicesController::class,
            [
                'request_headers' => ['Accept' => 'text/html'],
                'environment' => ['HTTP_X_REQUESTED_WITH' => 'XMLHttpRequest'],
            ]
        );
        /** @var Response $response */
        $response = $this->invokeMethod(
            $this->controller,
            'renderData',
            [$results]
        );
        // WebservicesController::render hardcoded output test
        $str = $this->getStringBody($response);
        $this->assertTextContains('"username": "testunit"', $str);
        $this->assertEquals('application/json', $response->getType());

        $this->setController(
            WebservicesController::class,
            ['request_headers' => ['Accept' => 'application/pdf']]
        );
        $this->expectException(NotAcceptableException::class);
        $this->invokeMethod(
            $this->controller,
            'renderData',
            [$results]
        );
    }

    public function testStaticRenderByAccepts()
    {
        $this->setController(
            WebservicesController::class,
            ['request_headers' => ['Accept' => 'application/json']]
        );
        $request = $this->controller->getRequest();
        $response = $this->controller->getResponse();
        $accepts = $response->mapType($request->parseAccept());
        $accepts = $accepts ? current($accepts) : [];
        $response = RenderDataTrait::staticRenderByAccepts(
            $this->controller,
            $accepts,
            ['foo' => 'bar']
        );
        // WebservicesController::render hardcoded output test
        $this->assertEquals("{\n    \"foo\": \"bar\"\n}", $this->getStringBody($response));
        $this->assertEquals('application/json', $response->getType());

        $Users = TableRegistry::getTableLocator()->get('Users');
        $results = $Users->find()->all();
        $this->setController(
            WebservicesController::class,
            ['request_headers' => ['Accept' => 'application/html']]
        );
        $request = $this->controller->getRequest();
        $response = $this->controller->getResponse();
        $accepts = $response->mapType($request->parseAccept());
        $accepts = $accepts ? current($accepts) : [];
        $response = RenderDataTrait::staticRenderByAccepts(
            $this->controller,
            $accepts,
            $results
        );
        // WebservicesController::render hardcoded output test
        $this->assertEquals('test', $this->getStringBody($response));
        $this->assertEquals('text/html', $response->getType());

        $results = $Users->find()->toArray();
        $this->setController(
            WebservicesController::class,
            ['request_headers' => ['Accept' => 'application/xml']]
        );
        $request = $this->controller->getRequest();
        $response = $this->controller->getResponse();
        $accepts = $response->mapType($request->parseAccept());
        $accepts = $accepts ? current($accepts) : [];
        $response = RenderDataTrait::staticRenderByAccepts(
            $this->controller,
            $accepts,
            $results
        );
        // WebservicesController::render hardcoded output test
        $str = $this->getStringBody($response);
        $this->assertTextContains('<Tests>', $str); // root xml
        $this->assertTextContains('<Entity>', $str); // repeating result node
        $this->assertTextContains('<id>1</id>', $str); // field
        $this->assertEquals('application/xml', $response->getType());

        $this->setController(
            WebservicesController::class,
            ['request_headers' => ['Accept' => 'application/xml']]
        );
        $request = $this->controller->getRequest();
        $response = $this->controller->getResponse();
        $accepts = $response->mapType($request->parseAccept());
        $accepts = $accepts ? current($accepts) : [];
        $response = RenderDataTrait::staticRenderByAccepts(
            $this->controller,
            $accepts,
            'done'
        );
        // WebservicesController::render hardcoded output test
        $str = $this->getStringBody($response);
        $this->assertTextContains('<message>', $str); // root xml
        $this->assertTextContains('done', $str); // field
        $this->assertEquals('application/xml', $response->getType());
    }
}
