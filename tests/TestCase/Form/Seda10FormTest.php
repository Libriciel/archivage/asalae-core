<?php

namespace AsalaeCore\Test\TestCase\Form;

use AsalaeCore\Form\Seda10Form;
use AsalaeCore\Utility\DOMUtility;
use Cake\Cache\Cache;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use AsalaeCore\TestSuite\TestCase;
use Libriciel\Filesystem\Utility\Filesystem;

class Seda10FormTest extends TestCase
{
    const SEDA10_TEST_FILE = TESTS.'Data'.DS.'sample_seda10.xml';

    public string $testDir = TMP_TESTDIR . DS . 'seda_10_form_test';

    public $fixtures = [
        'app.Agreements',
        'app.OrgEntities',
        'app.Profiles',
        'app.ServiceLevels',
        'app.TransferAttachments',
        'app.Transfers',
        'app.TypeEntities',
    ];

    public function setUp(): void
    {
        parent::setUp();
        Cache::disable();
        Filesystem::reset();
        if (is_dir($this->testDir)) {
            Filesystem::remove($this->testDir);
        }
        Filesystem::mkdir($this->testDir);
    }

    public function tearDown(): void
    {
        parent::tearDown();
        if (is_dir($this->testDir)) {
            Filesystem::remove($this->testDir);
        }
    }

    public function testAutoRepair()
    {
        Filesystem::copy(self::SEDA10_TEST_FILE, $file = $this->testDir . DS . 'seda10.xml');
        $this->assertFileExists($file);

        $TransferAttachments = TableRegistry::getTableLocator()->get('TransferAttachments');
        $TransferAttachments->deleteAll([]);

        /**
         * On créé un xml avec des erreurs réparable automatiquement
         */
        $util = DOMUtility::load($file);
        /** @var \DOMElement $attachement */
        foreach ($util->xpath->query('//ns:Document/ns:Attachment') as $attachement) {
            $filename = $attachement->getAttribute('filename');
            // le filename commence par un "/"
            $attachement->setAttribute('filename', '/'.$filename);
            $TransferAttachments->save(
                new Entity(
                    [
                        'transfer_id' => 1,
                        'filename' => $filename,
                        'size' => 1,
                        'hash' => 'foo',
                        'hash_algo' => 'bar'
                    ]
                )
            );
        }
        // Integrity manquant
        /** @var \DOMElement $integrity */
        foreach ($util->xpath->query('//ns:Document/ns:Integrity') as $integrity) {
            $integrity->parentNode->removeChild($integrity);
        }
        $util->dom->save($file);
        $util = DOMUtility::load($file);
        $filename = $util->xpath->query('//ns:Document/ns:Attachment')->item(0)->getAttribute('filename');
        $this->assertEquals('/', $filename[0]);
        $this->assertEquals(0, $util->xpath->query('//ns:Document/ns:Integrity')->count());

        /**
         * Test
         */
        Seda10Form::autoRepair(new Entity(['id' => 1, 'archival_agency_id' => 2, 'xml' => $file]));
        $util = DOMUtility::load($file);
        $filename = $util->xpath->query('//ns:Document/ns:Attachment')->item(0)->getAttribute('filename');
        $this->assertNotEquals('/', $filename[0]);
        $this->assertEquals(3, $util->xpath->query('//ns:Document/ns:Integrity')->count());
    }
}
