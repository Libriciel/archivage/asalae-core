<?php

namespace AsalaeCore\Test\TestCase\Form;

use AsalaeCore\Form\Seda02Form;
use AsalaeCore\Utility\DOMUtility;
use Cake\Cache\Cache;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use AsalaeCore\TestSuite\TestCase;
use Cake\Utility\Hash;
use Libriciel\Filesystem\Utility\Filesystem;

class Seda02FormTest extends TestCase
{
    const SEDA02_TEST_FILE = TESTS.'Data'.DS.'seda02.xml';

    public string $testDir = TMP_TESTDIR . DS . 'seda_02_form_test';

    public $fixtures = [
        'app.Agreements',
        'app.OrgEntities',
        'app.Profiles',
        'app.ServiceLevels',
        'app.TransferAttachments',
        'app.Transfers',
        'app.TypeEntities',
    ];

    public function setUp(): void
    {
        parent::setUp();
        Cache::disable();
        Filesystem::reset();
        if (is_dir($this->testDir)) {
            Filesystem::remove($this->testDir);
        }
        Filesystem::mkdir($this->testDir);
    }

    public function tearDown(): void
    {
        parent::tearDown();
        if (is_dir($this->testDir)) {
            Filesystem::remove($this->testDir);
        }
    }

    public function testBuildForm()
    {
        /** @var Seda02Form $instance */
        $instance = Seda02Form::buildForm(2, 'ArchiveTransfer');
        $this->assertEquals('text', $instance->getSchema()->field('Comment')['type']);
    }

    public function testGetMessageSchema()
    {
        $instance = Seda02Form::buildForm(2, 'ArchiveTransfer');
        $this->assertEquals('1..1', $instance->getMessageSchema()['cardinality']);
        $this->assertEquals('1..n', $instance->getMessageSchema('Contains')['cardinality']);
        $this->assertEquals('0..n', $instance->getMessageSchema('Contains.Contains.Contains')['cardinality']);
        $instance = Seda02Form::buildForm(2);
        $this->assertEquals('1..n', $instance->getMessageSchema('ArchiveTransfer.Contains')['cardinality']);
    }

    public function testGetSchemaTree()
    {
        $instance = Seda02Form::buildForm(2);
        $treeInitial = current(json_decode($instance->getDataJsTree(self::SEDA02_TEST_FILE), true));
        $this->assertStringContainsString(__("Transfert d'archives"), Hash::get($treeInitial, 'text', ''));
        $this->assertStringContainsString(
            'TransferringAgency',
            Hash::get($treeInitial, 'children.0.text', '')
        );

        $dom = new \DOMDocument;
        $dom->load(self::SEDA02_TEST_FILE);
        $uid = uniqid('uid-test-', true);
        $newNode = $dom->createElement('Contains');
        $name = $dom->createElement('Name');
        DOMUtility::setValue($name, $uid);
        $newNode->appendChild($name);
        $dom->getElementsByTagName('Contains')->item(3)->appendChild($newNode);
        $tmpFile = sys_get_temp_dir().DS.$uid.'.xml';
        $dom->save($tmpFile);
        $this->assertFileExists($tmpFile);
        $treeWithAppendix = current(json_decode($instance->getDataJsTree($tmpFile), true));
        $this->assertStringContainsString($uid, file_get_contents($tmpFile));
        unlink($tmpFile);
        $this->assertNotEquals(array_keys(Hash::flatten($treeInitial)), array_keys(Hash::flatten($treeWithAppendix)));
    }

    public function testAppendNode()
    {
        $dom = new \DOMDocument;
        $dom->formatOutput = true;
        $dom->preserveWhiteSpace = false;
        $dom->loadXML('<ArchiveTransfer xmlns="fr:gouv:ae:archive:draft:standard_echange_v0.2"></ArchiveTransfer>');
        $xpath = new \DOMXPath($dom);
        $namespace = $dom->documentElement->getAttributeNode('xmlns')->nodeValue;
        $xpath->registerNamespace('ns', $namespace);
        $parentNode = $xpath->query('/ns:ArchiveTransfer')->item(0);
        $node = Seda02Form::appendNode(
            2,
            'TransferringAgency',
            'ArchiveTransfer',
            $dom,
            $parentNode
        );
        $this->assertInstanceOf(\DOMElement::class, $node);
        $this->assertEquals('TransferringAgency', $node->tagName);

        $form = Seda02Form::buildForm(
            2,
            'ArchiveTransfer.TransferringAgency',
            $dom,
            $node
        );

        $uid = uniqid('uid-test-', true);
        $data = [
            'Identification' => [['@' => $uid]],
        ];
        $this->assertTrue($form->execute($data));

        $saved = $dom->saveXML();
        $this->assertStringContainsString($uid, $saved);
    }

    public function testAutoRepair()
    {
        Filesystem::copy(self::SEDA02_TEST_FILE, $file = sys_get_temp_dir().DS.'testunit'.DS.'seda02.xml');
        $this->assertFileExists($file);

        $TransferAttachments = TableRegistry::getTableLocator()->get('TransferAttachments');
        $TransferAttachments->deleteAll([]);

        /**
         * On créé un xml avec des erreurs réparable automatiquement
         */
        $util = DOMUtility::load($file);
        /** @var \DOMElement $attachement */
        foreach ($util->xpath->query('//ns:Document/ns:Attachment') as $attachement) {
            $filename = $attachement->getAttribute('filename');
            // le filename commence par un "/"
            $attachement->setAttribute('filename', '/'.$filename);
            $TransferAttachments->save(
                new Entity(
                    [
                        'transfer_id' => 1,
                        'filename' => $filename,
                        'size' => 1,
                        'hash' => 'foo',
                        'hash_algo' => 'bar'
                    ]
                )
            );
        }
        // Integrity manquant
        /** @var \DOMElement $integrity */
        foreach ($util->xpath->query('/ns:ArchiveTransfer/ns:Integrity') as $integrity) {
            $integrity->parentNode->removeChild($integrity);
        }
        $util->dom->save($file);
        $util = DOMUtility::load($file);
        $filename = $util->xpath->query('//ns:Document/ns:Attachment')->item(0)->getAttribute('filename');
        $this->assertEquals('/', $filename[0]);
        $this->assertEquals(0, $util->xpath->query('/ns:ArchiveTransfer/ns:Integrity')->count());

        /**
         * Test
         */
        Seda02Form::autoRepair(new Entity(['id' => 1, 'archival_agency_id' => 2, 'xml' => $file]));
        $util = DOMUtility::load($file);
        $filename = $util->xpath->query('//ns:Document/ns:Attachment')->item(0)->getAttribute('filename');
        $this->assertNotEquals('/', $filename[0]);
        $this->assertEquals(2, $util->xpath->query('/ns:ArchiveTransfer/ns:Integrity')->count());
    }
}
