<?php

namespace AsalaeCore\Test\TestCase\Form;

use AsalaeCore\Form\Seda21Form;
use AsalaeCore\Utility\DOMUtility;
use Cake\Cache\Cache;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use AsalaeCore\TestSuite\TestCase;
use Libriciel\Filesystem\Utility\Filesystem;

class Seda21FormTest extends TestCase
{
    const SEDA21_TEST_FILE = TESTS.'Data'.DS.'sample_seda21.xml';

    public string $testDir = TMP_TESTDIR . DS . 'seda_21_form_test';

    public $fixtures = [
        'app.Agreements',
        'app.OrgEntities',
        'app.Profiles',
        'app.ServiceLevels',
        'app.TransferAttachments',
        'app.Transfers',
        'app.TypeEntities',
    ];

    public function setUp(): void
    {
        parent::setUp();
        Cache::disable();
        Filesystem::reset();
        if (is_dir($this->testDir)) {
            Filesystem::remove($this->testDir);
        }
        Filesystem::mkdir($this->testDir);
    }

    public function tearDown(): void
    {
        parent::tearDown();
        if (is_dir($this->testDir)) {
            Filesystem::remove($this->testDir);
        }
    }

    public function testAutoRepair()
    {
        Filesystem::copy(self::SEDA21_TEST_FILE, $file = $this->testDir . 'seda21.xml');
        $this->assertFileExists($file);

        $TransferAttachments = TableRegistry::getTableLocator()->get('TransferAttachments');
        $TransferAttachments->deleteAll([]);

        /**
         * On créé un xml avec des erreurs réparable automatiquement
         */
        $util = DOMUtility::load($file);
        /** @var \DOMElement $BinaryDataObject */
        foreach ($util->xpath->query('//ns:BinaryDataObject') as $BinaryDataObject) {
            $Filename = $BinaryDataObject->getElementsByTagName('Filename')->item(0);
            $TransferAttachments->save(
                new Entity(
                    [
                        'transfer_id' => 1,
                        'filename' => $Filename->nodeValue,
                        'size' => 1,
                        'hash' => 'foo',
                        'hash_algo' => 'bar'
                    ]
                )
            );
            $Filename->nodeValue = '/'.$Filename->nodeValue;
            $util->dom->save($file);
        }
        // Integrity manquant
        /** @var \DOMElement $integrity */
        foreach ($util->xpath->query('//ns:MessageDigest') as $integrity) {
            $integrity->parentNode->removeChild($integrity);
        }
        $util->dom->save($file);
        $util = DOMUtility::load($file);
        $filename = $util->xpath->query('//ns:BinaryDataObject/ns:FileInfo/ns:Filename')->item(0)->nodeValue;
        $this->assertEquals('/sample.pdf', $filename);
        $this->assertEquals(0, $util->xpath->query('//ns:MessageDigest')->count());

        /**
         * Test
         */
        Seda21Form::autoRepair(new Entity(['id' => 1, 'archival_agency_id' => 2, 'xml' => $file]));
        $util = DOMUtility::load($file);
        $filename = $util->xpath->query('//ns:BinaryDataObject/ns:FileInfo/ns:Filename')->item(0)->nodeValue;
        $this->assertEquals('sample.pdf', $filename);
        $this->assertEquals(1, $util->xpath->query('//ns:MessageDigest')->count());
    }

    public function testAppendNodeComplex()
    {
        $dom = new \DOMDocument;
        $dom->formatOutput = true;
        $dom->preserveWhiteSpace = false;
        $dom->loadXML(
            <<<EOT
<ArchiveTransfer xmlns="fr:gouv:culture:archivesdefrance:seda:v2.1">
    <DataObjectPackage xml:id="ID1">
        <ManagementMetadata>
            <StorageRule>
                <Rule>included</Rule>
                <StartDate>2020</StartDate>
            </StorageRule>
        </ManagementMetadata>
    </DataObjectPackage>
</ArchiveTransfer>
EOT
        );
        $xpath = new \DOMXPath($dom);
        $namespace = $dom->documentElement->getAttributeNode('xmlns')->nodeValue;
        $xpath->registerNamespace('ns', $namespace);
        $parentNode = $xpath->query('/ns:ArchiveTransfer/ns:DataObjectPackage/ns:ManagementMetadata/ns:StorageRule')->item(0);

        $node = Seda21Form::appendNode(
            2,
            'Rule',
            'ArchiveTransfer.DataObjectPackage.ManagementMetadata.StorageRule',
            $dom,
            $parentNode
        );
        $this->assertInstanceOf(\DOMElement::class, $node);
        $node->nodeValue = 'test';
        $expected = <<<EOT
<StorageRule>
  <Rule>included</Rule>
  <StartDate>2020</StartDate>
  <Rule>test</Rule>
</StorageRule>
EOT;
        $this->assertEquals($expected, $dom->saveXML($node->parentNode));
    }
}
