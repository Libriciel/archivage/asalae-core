<?php

namespace AsalaeCore\Test\TestCase\Form;

use AsalaeCore\Form\ConfigurationForm;
use AsalaeCore\TestSuite\TestCase;
use Cake\Core\Configure;
use Libriciel\Filesystem\Utility\Filesystem;

class ConfigurationFormTest extends TestCase
{
    public string $testDir = TMP_TESTDIR . DS . 'configuration_form_test';

    public function setUp(): void
    {
        parent::setUp();
        if (is_dir($this->testDir)) {
            Filesystem::remove($this->testDir);
        }
        Filesystem::mkdir($this->testDir);
        Configure::write('App.defaultLocale', 'fr_FR');
        Configure::write('App.paths.path_to_local_config', $this->testDir . DS . 'path_to_local.php');
        Filesystem::reset();
        Filesystem::dumpFile(
            $this->testDir . DS . 'path_to_local.php',
            "<?php return '$this->testDir/app_local.json';?>"
        );
        Filesystem::dumpFile($this->testDir . DS . 'app_local.json', "{}");
    }

    public function tearDown(): void
    {
        parent::tearDown();
        if (is_dir($this->testDir)) {
            Filesystem::remove($this->testDir);
        }
    }

    public function testSchema()
    {
        $form = new ConfigurationForm();
        $fields = $form->getSchema()->fields();
        $this->assertContains('datasources_default_driver', $fields);
    }

    public function testExecute()
    {
        $data = [
            'config' => '{}',
            'hash_algo' => 'sha256',
            'ignore_invalid_fullbaseurl' => '1',
            'app_fullbaseurl' => 'https://test.localhost',
            'datacompressor_encodefilename' => 0,
            'password_complexity' => 49,
            'password_admin_complexity' => 49,
            'filesystem_useshred' => '0',
            'libriciel_login_logo' => $png = 'img.png',
            'libriciel_login_background' => $png,
            'assets_global_background' => WWW_ROOT . DS . $png,
            'assets_global_background_position' => '',
            'emails_from' => 'foo@bar.com',
            'tokens-duration_code' => '',
            'tokens-duration_access-token' => '',
            'tokens-duration_refresh-token' => '',
            'beanstalk_tests_timeout' => '',
            'downloads_asyncfilecreationtimelimit' => '',
            'downloads_tempfileconservationtimelimit' => '',
            'proxy_host' => '',
            'proxy_port' => '',
            'proxy_username' => '',
            'proxy_password' => '',
            'paginator_limit' => 20,
            'ajaxpaginator_limit' => 100,
            'datasources_default_test' => 'test',
            'workers_test_quantity' => 1,
        ];
        $form = new ConfigurationForm;
        $this->assertTrue($form->execute($data));
        $result = json_decode(file_get_contents($this->testDir . DS . 'app_local.json'), true);
        $expected = [
            'AjaxPaginator' => [
                'limit' => 100
            ],
            'App' => [
                'ignore_invalid_fullbaseurl' => true,
                'fullBaseUrl' => 'https://test.localhost',
            ],
            'Email' => [
                'default' => [
                    'from' => 'foo@bar.com'
                ]
            ],
            'FilesystemUtility' => [
                'useShred' => false
            ],
            'Libriciel' => [
                'login' => [
                    'logo-client' => [
                        'image' => 'img.png',
                        'style' => 'background: img.png',
                    ]
                ]
            ],
            'Pagination' => [
                'limit' => 20
            ],
            'Password' => [
                'admin' => [
                    'complexity' => 49
                ],
                'complexity' => 49,
            ],
            'hash_algo' => 'sha256',
            'DataCompressorUtility' => [
                'encodeFilename' => false,
            ],
            'Beanstalk' => [
                'workers' => [
                    'test' => [
                        'quantity' => 1,
                    ],
                ],
            ],
            'Datasources' => [
                'default' => [
                    'test' => 'test',
                ],
            ],
        ];
        $this->assertEquals($expected, $result);
    }
}
