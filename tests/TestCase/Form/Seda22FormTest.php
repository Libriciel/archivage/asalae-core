<?php

namespace AsalaeCore\Test\TestCase\Form;

use AsalaeCore\Form\MessageSchema\MessageSchema;
use AsalaeCore\Form\Seda22Form;
use AsalaeCore\Utility\DOMUtility;
use Cake\Cache\Cache;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use AsalaeCore\TestSuite\TestCase;
use DOMDocument;
use Libriciel\Filesystem\Utility\Filesystem;

class Seda22FormTest extends TestCase
{
    const SEDA22_TEST_FILE = TESTS.'Data'.DS.'sample_seda22.xml';

    public string $testDir = TMP_TESTDIR . DS . 'seda_22_form_test';

    public $fixtures = [
        'app.Agreements',
        'app.OrgEntities',
        'app.Profiles',
        'app.ServiceLevels',
        'app.TransferAttachments',
        'app.Transfers',
        'app.TypeEntities',
    ];

    public function setUp(): void
    {
        parent::setUp();
        Cache::disable();
        Filesystem::reset();
        if (is_dir($this->testDir)) {
            Filesystem::remove($this->testDir);
        }
        Filesystem::mkdir($this->testDir);
    }

    public function tearDown(): void
    {
        parent::tearDown();
        if (is_dir($this->testDir)) {
            Filesystem::remove($this->testDir);
        }
    }

    public function testAutoRepair()
    {
        Filesystem::copy(self::SEDA22_TEST_FILE, $file = $this->testDir . 'seda22.xml');
        $this->assertFileExists($file);

        $TransferAttachments = TableRegistry::getTableLocator()->get('TransferAttachments');
        $TransferAttachments->deleteAll([]);

        /**
         * On créé un xml avec des erreurs réparable automatiquement
         */
        $util = DOMUtility::load($file);
        /** @var \DOMElement $BinaryDataObject */
        foreach ($util->xpath->query('//ns:BinaryDataObject') as $BinaryDataObject) {
            $Filename = $BinaryDataObject->getElementsByTagName('Filename')->item(0);
            $TransferAttachments->save(
                new Entity(
                    [
                        'transfer_id' => 1,
                        'filename' => $Filename->nodeValue,
                        'size' => 1,
                        'hash' => 'foo',
                        'hash_algo' => 'bar'
                    ]
                )
            );
            $Filename->nodeValue = '/'.$Filename->nodeValue;
            $util->dom->save($file);
        }
        // Integrity manquant
        /** @var \DOMElement $integrity */
        foreach ($util->xpath->query('//ns:MessageDigest') as $integrity) {
            $integrity->parentNode->removeChild($integrity);
        }
        $util->dom->save($file);
        $util = DOMUtility::load($file);
        $filename = $util->xpath->query('//ns:BinaryDataObject/ns:FileInfo/ns:Filename')->item(0)->nodeValue;
        $this->assertEquals('/sample.pdf', $filename);
        $this->assertEquals(0, $util->xpath->query('//ns:MessageDigest')->count());

        /**
         * Test
         */
        Seda22Form::autoRepair(new Entity(['id' => 1, 'archival_agency_id' => 2, 'xml' => $file]));
        $util = DOMUtility::load($file);
        $filename = $util->xpath->query('//ns:BinaryDataObject/ns:FileInfo/ns:Filename')->item(0)->nodeValue;
        $this->assertEquals('sample.pdf', $filename);
        $this->assertEquals(1, $util->xpath->query('//ns:MessageDigest')->count());
    }

    public function testAppendNodeComplex()
    {
        $dom = new \DOMDocument;
        $dom->formatOutput = true;
        $dom->preserveWhiteSpace = false;
        $dom->loadXML(
            <<<EOT
<ArchiveTransfer xmlns="fr:gouv:culture:archivesdefrance:seda:v2.1">
    <DataObjectPackage xml:id="ID1">
        <ManagementMetadata>
            <StorageRule>
                <Rule>included</Rule>
                <StartDate>2020</StartDate>
            </StorageRule>
        </ManagementMetadata>
    </DataObjectPackage>
</ArchiveTransfer>
EOT
        );
        $xpath = new \DOMXPath($dom);
        $namespace = $dom->documentElement->getAttributeNode('xmlns')->nodeValue;
        $xpath->registerNamespace('ns', $namespace);
        $parentNode = $xpath->query(
            '/ns:ArchiveTransfer/ns:DataObjectPackage/ns:ManagementMetadata/ns:StorageRule'
        )->item(0);

        $node = Seda22Form::appendNode(
            2,
            'Rule',
            'ArchiveTransfer.DataObjectPackage.ManagementMetadata.StorageRule',
            $dom,
            $parentNode
        );
        $this->assertInstanceOf(\DOMElement::class, $node);
        $node->nodeValue = 'test';
        $expected = <<<EOT
<StorageRule>
  <Rule>included</Rule>
  <StartDate>2020</StartDate>
  <Rule>test</Rule>
</StorageRule>
EOT;
        $this->assertEquals($expected, $dom->saveXML($node->parentNode));
    }

    public function test_buildSchema()
    {
        MessageSchema::$schemas = [];
        Filesystem::copy(self::SEDA22_TEST_FILE, $file = $this->testDir . 'seda22.xml');
        $this->assertFileExists($file);
        $dom = new DOMDocument;
        $dom->load($file);
        $form = Seda22Form::buildForm(2, 'ArchiveTransfer', $dom);
        $schema = $form->getSchema();
        $this->assertTrue(in_array('MessageIdentifier', $schema->fields()));

        $element = $dom->getElementsByTagName('BinaryDataObject')->item(0);
        $this->assertNotEmpty($element);
        $form = Seda22Form::buildForm(2, 'ArchiveTransfer.DataObjectPackage.BinaryDataObject', $dom, $element);
        $schema = $form->getSchema();
        $this->assertTrue(in_array('Attachment_#0_@filename', $schema->fields()));
    }

    public function testvalidation()
    {
        Filesystem::copy(self::SEDA22_TEST_FILE, $file = $this->testDir . 'seda22.xml');
        $this->assertFileExists($file);
        $dom = new DOMDocument;
        $dom->load($file);
        $element = $dom->getElementsByTagName('BinaryDataObject')->item(0);
        $this->assertNotEmpty($element);
        $form = Seda22Form::buildForm(2, 'ArchiveTransfer.DataObjectPackage.BinaryDataObject', $dom, $element);
        $this->assertTrue($form->validate(['Attachment_#0_@filename' => 'test']));
    }

    public function testexecute()
    {
        Filesystem::copy(self::SEDA22_TEST_FILE, $file = $this->testDir . 'seda22.xml');
        $this->assertFileExists($file);
        $dom = new DOMDocument;
        $dom->load($file);
        $element = $dom->getElementsByTagName('BinaryDataObject')->item(0);
        $this->assertNotEmpty($element);
        $form = Seda22Form::buildForm(2, 'ArchiveTransfer.DataObjectPackage.BinaryDataObject', $dom, $element);
        $uid = uniqid('uniq-', true);
        $this->assertTrue($form->execute(['Attachment' => [0 => ['filename' => $uid]]]));
        $this->assertStringContainsString($uid, $dom->saveXML());
    }

    public function testgetDataJsTree()
    {
        Filesystem::copy(self::SEDA22_TEST_FILE, $file = $this->testDir . 'seda22.xml');
        $this->assertFileExists($file);
        $form = Seda22Form::buildForm(2);
        $json = json_decode($form->getDataJsTree($file), true);
        $this->assertIsArray($json);
        $this->assertArrayHasKey(0, $json);
        $this->assertArrayHasKey('path', $json[0]);
        $this->assertEquals('ArchiveTransfer', $json[0]['path']);
    }
}
