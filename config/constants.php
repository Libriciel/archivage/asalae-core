<?php


/**
 * Namespace du seda
 */
if (!defined('NAMESPACE_SEDA_02')) {
    define('NAMESPACE_SEDA_02', 'fr:gouv:ae:archive:draft:standard_echange_v0.2');
    define('NAMESPACE_SEDA_10', 'fr:gouv:culture:archivesdefrance:seda:v1.0');
    define('NAMESPACE_SEDA_20', 'fr:gouv:culture:archivesdefrance:seda:v2.0');
    define('NAMESPACE_SEDA_21', 'fr:gouv:culture:archivesdefrance:seda:v2.1');
    define('NAMESPACE_SEDA_22', 'fr:gouv:culture:archivesdefrance:seda:v2.2');
}

/**
 * change la limite de 65535 lignes d'un xml si $dom->loadXML($xml, XML_PARSE_BIG_LINES);
 */
if (!defined('XML_PARSE_BIG_LINES')) {
    define('XML_PARSE_BIG_LINES', 4194304);
}

if (!defined('CAKE_SHELL')) {
    define('CAKE_SHELL', dirname(__DIR__) . '/bin/cake');
}
