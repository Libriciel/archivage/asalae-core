<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class Core extends AbstractMigration
{
    /**
     * Up Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-up-method
     * @return void
     */
    public function up()
    {
        $this->table('access_rule_codes')
            ->addColumn(
                'org_entity_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'code',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'description',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'duration',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'org_entity_id',
                    'code',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'org_entity_id',
                ]
            )
            ->create();

        $this->table('access_rules')
            ->addColumn(
                'access_rule_code_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'start_date',
                'date',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'end_date',
                'date',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'access_rule_code_id',
                ]
            )
            ->create();

        $this->table('access_tokens')
            ->addColumn(
                'type',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'code',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'expiration_date',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'user_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'user_id',
                ]
            )
            ->addIndex(
                [
                    'expiration_date',
                ]
            )
            ->create();

        $this->table('acos')
            ->addColumn(
                'parent_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'model',
                'string',
                [
                    'default' => '',
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'foreign_key',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'alias',
                'string',
                [
                    'default' => '',
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'lft',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'rght',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'parent_id',
                ]
            )
            ->addIndex(
                [
                    'alias',
                ]
            )
            ->addIndex(
                [
                    'lft',
                    'rght',
                ]
            )
            ->create();

        $this->table('agreements')
            ->addColumn(
                'org_entity_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'identifier',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'description',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'active',
                'boolean',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'modified',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'allow_all_transferring_agencies',
                'boolean',
                [
                    'default' => false,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'allow_all_originating_agencies',
                'boolean',
                [
                    'default' => true,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'allow_all_service_levels',
                'boolean',
                [
                    'default' => true,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'auto_validate',
                'boolean',
                [
                    'default' => true,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'proper_chain_id',
                'integer',
                [
                    'comment' => 'circuit de validation des transferts conformes',
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'improper_chain_id',
                'integer',
                [
                    'comment' => 'circuit de validation des transferts non conformes',
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'date_begin',
                'date',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'date_end',
                'date',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'default_agreement',
                'boolean',
                [
                    'default' => false,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'allowed_formats',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'max_transfers',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'transfer_period',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'max_size_per_transfer',
                'biginteger',
                [
                    'default' => null,
                    'limit' => 20,
                    'null' => true,
                ]
            )
            ->addColumn(
                'allow_all_profiles',
                'boolean',
                [
                    'default' => true,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'notify_delay',
                'integer',
                [
                    'default' => '0',
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'org_entity_id',
                    'identifier',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'improper_chain_id',
                ]
            )
            ->addIndex(
                [
                    'org_entity_id',
                ]
            )
            ->addIndex(
                [
                    'proper_chain_id',
                ]
            )
            ->create();

        $this->table('agreements_originating_agencies')
            ->addColumn(
                'agreement_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'org_entity_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'agreement_id',
                ]
            )
            ->addIndex(
                [
                    'org_entity_id',
                ]
            )
            ->create();

        $this->table('agreements_profiles')
            ->addColumn(
                'agreement_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'profile_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'agreement_id',
                ]
            )
            ->addIndex(
                [
                    'profile_id',
                ]
            )
            ->create();

        $this->table('agreements_service_levels')
            ->addColumn(
                'agreement_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'service_level_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'agreement_id',
                ]
            )
            ->addIndex(
                [
                    'service_level_id',
                ]
            )
            ->create();

        $this->table('agreements_transferring_agencies')
            ->addColumn(
                'agreement_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'org_entity_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'agreement_id',
                ]
            )
            ->addIndex(
                [
                    'org_entity_id',
                ]
            )
            ->create();

        $this->table('appraisal_rule_codes')
            ->addColumn(
                'org_entity_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'code',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'description',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'duration',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'org_entity_id',
                    'code',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'org_entity_id',
                ]
            )
            ->create();

        $this->table('appraisal_rules')
            ->addColumn(
                'appraisal_rule_code_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'final_action_code',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'start_date',
                'date',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'end_date',
                'date',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'appraisal_rule_code_id',
                ]
            )
            ->create();

        $this->table('archive_binaries')
            ->addColumn(
                'is_integrity_ok',
                'boolean',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'type',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'filename',
                'string',
                [
                    'default' => null,
                    'limit' => 2048,
                    'null' => false,
                ]
            )
            ->addColumn(
                'format',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'mime',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'extension',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'stored_file_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'original_data_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'in_rgi',
                'boolean',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'app_meta',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'original_data_id',
                ]
            )
            ->addIndex(
                [
                    'stored_file_id',
                ]
            )
            ->addIndex(
                [
                    'filename',
                ]
            )
            ->create();

        $this->table('archive_binaries_archive_units')
            ->addColumn(
                'archive_binary_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'archive_unit_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'archive_binary_id',
                ]
            )
            ->addIndex(
                [
                    'archive_unit_id',
                ]
            )
            ->create();

        $this->table('archive_binaries_technical_archive_units')
            ->addColumn(
                'archive_binary_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'technical_archive_unit_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'archive_binary_id',
                ]
            )
            ->addIndex(
                [
                    'technical_archive_unit_id',
                ]
            )
            ->create();

        $this->table('archive_binary_conversions')
            ->addColumn(
                'archive_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'archive_binary_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'type',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'format',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'audio_codec',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'video_codec',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addIndex(
                [
                    'archive_binary_id',
                ]
            )
            ->addIndex(
                [
                    'archive_id',
                ]
            )
            ->create();

        $this->table('archive_descriptions')
            ->addColumn(
                'archive_unit_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'access_rule_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'description',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'history',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'file_plan_position',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'oldest_date',
                'date',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'latest_date',
                'date',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'access_rule_id',
                ]
            )
            ->addIndex(
                [
                    'archive_unit_id',
                ]
            )
            ->create();

        $this->table('archive_files')
            ->addColumn(
                'archive_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'stored_file_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'is_integrity_ok',
                'boolean',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'type',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'filename',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'archive_id',
                ]
            )
            ->addIndex(
                [
                    'stored_file_id',
                ]
            )
            ->create();

        $this->table('archive_indicators')
            ->addColumn(
                'archive_date',
                'date',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'archival_agency_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'transferring_agency_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'originating_agency_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'archive_count',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'units_count',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'original_count',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'original_size',
                'biginteger',
                [
                    'default' => null,
                    'limit' => 20,
                    'null' => false,
                ]
            )
            ->addColumn(
                'timestamp_count',
                'integer',
                [
                    'default' => '0',
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'timestamp_size',
                'biginteger',
                [
                    'default' => '0',
                    'limit' => 20,
                    'null' => true,
                ]
            )
            ->addColumn(
                'preservation_count',
                'integer',
                [
                    'default' => '0',
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'preservation_size',
                'biginteger',
                [
                    'default' => '0',
                    'limit' => 20,
                    'null' => true,
                ]
            )
            ->addColumn(
                'dissemination_count',
                'integer',
                [
                    'default' => '0',
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'dissemination_size',
                'biginteger',
                [
                    'default' => '0',
                    'limit' => 20,
                    'null' => true,
                ]
            )
            ->addColumn(
                'agreement_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'profile_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'agreement_id',
                ]
            )
            ->addIndex(
                [
                    'archival_agency_id',
                ]
            )
            ->addIndex(
                [
                    'originating_agency_id',
                ]
            )
            ->addIndex(
                [
                    'profile_id',
                ]
            )
            ->addIndex(
                [
                    'transferring_agency_id',
                ]
            )
            ->create();

        $this->table('archive_keywords')
            ->addColumn(
                'archive_unit_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'access_rule_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'content',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'reference',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'type',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'xml_node_tagname',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'access_rule_id',
                ]
            )
            ->addIndex(
                [
                    'archive_unit_id',
                ]
            )
            ->addIndex(
                [
                    'content',
                ]
            )
            ->create();

        $this->table('archive_units')
            ->addColumn(
                'state',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'archive_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'access_rule_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'appraisal_rule_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'archival_agency_identifier',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'transferring_agency_identifier',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'originating_agency_identifier',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'name',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'original_local_size',
                'biginteger',
                [
                    'default' => null,
                    'limit' => 20,
                    'null' => false,
                ]
            )
            ->addColumn(
                'original_total_size',
                'biginteger',
                [
                    'default' => null,
                    'limit' => 20,
                    'null' => false,
                ]
            )
            ->addColumn(
                'parent_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'lft',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'rght',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'xml_node_tagname',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'original_total_count',
                'integer',
                [
                    'default' => '0',
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'original_local_count',
                'integer',
                [
                    'default' => '0',
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'search',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'expired_dua_root',
                'boolean',
                [
                    'default' => false,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'full_search',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'access_rule_id',
                ]
            )
            ->addIndex(
                [
                    'appraisal_rule_id',
                ]
            )
            ->addIndex(
                [
                    'archive_id',
                ]
            )
            ->addIndex(
                [
                    'parent_id',
                ]
            )
            ->addIndex(
                [
                    'state',
                ]
            )
            ->addIndex(
                [
                    'lft',
                    'rght',
                ]
            )
            ->create();

        $this->table('archive_units_batch_treatments')
            ->addColumn(
                'archive_unit_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'batch_treatment_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'archive_unit_id',
                ]
            )
            ->addIndex(
                [
                    'batch_treatment_id',
                ]
            )
            ->create();

        $this->table('archive_units_delivery_requests')
            ->addColumn(
                'archive_unit_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'delivery_request_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'archive_unit_id',
                ]
            )
            ->addIndex(
                [
                    'delivery_request_id',
                ]
            )
            ->create();

        $this->table('archive_units_destruction_requests')
            ->addColumn(
                'archive_unit_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'destruction_request_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'archive_unit_id',
                ]
            )
            ->addIndex(
                [
                    'destruction_request_id',
                ]
            )
            ->create();

        $this->table('archive_units_outgoing_transfer_requests')
            ->addColumn(
                'archive_unit_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'otr_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'archive_unit_id',
                ]
            )
            ->addIndex(
                [
                    'otr_id',
                ]
            )
            ->create();

        $this->table('archive_units_restitution_requests')
            ->addColumn(
                'archive_unit_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'restitution_request_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'archive_unit_id',
                ]
            )
            ->addIndex(
                [
                    'restitution_request_id',
                ]
            )
            ->create();

        $this->table('archives')
            ->addColumn(
                'state',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'is_integrity_ok',
                'boolean',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'archival_agency_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'transferring_agency_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'originating_agency_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'transferring_operator_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'agreement_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'profile_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'service_level_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'access_rule_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'appraisal_rule_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'archival_agency_identifier',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'transferring_agency_identifier',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'originating_agency_identifier',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'secure_data_space_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'storage_path',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'name',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'description',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'oldest_date',
                'date',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'latest_date',
                'date',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'transferred_size',
                'biginteger',
                [
                    'default' => '0',
                    'limit' => 20,
                    'null' => false,
                ]
            )
            ->addColumn(
                'management_size',
                'biginteger',
                [
                    'default' => '0',
                    'limit' => 20,
                    'null' => false,
                ]
            )
            ->addColumn(
                'original_size',
                'biginteger',
                [
                    'default' => '0',
                    'limit' => 20,
                    'null' => false,
                ]
            )
            ->addColumn(
                'preservation_size',
                'biginteger',
                [
                    'default' => '0',
                    'limit' => 20,
                    'null' => false,
                ]
            )
            ->addColumn(
                'dissemination_size',
                'biginteger',
                [
                    'default' => '0',
                    'limit' => 20,
                    'null' => false,
                ]
            )
            ->addColumn(
                'timestamp_size',
                'biginteger',
                [
                    'default' => '0',
                    'limit' => 20,
                    'null' => false,
                ]
            )
            ->addColumn(
                'transferred_count',
                'integer',
                [
                    'default' => '0',
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'management_count',
                'integer',
                [
                    'default' => '0',
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'original_count',
                'integer',
                [
                    'default' => '0',
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'preservation_count',
                'integer',
                [
                    'default' => '0',
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'dissemination_count',
                'integer',
                [
                    'default' => '0',
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'timestamp_count',
                'integer',
                [
                    'default' => '0',
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'xml_node_tagname',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'last_state_update',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'integrity_date',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'units_count',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'modified',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'filesexist_date',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'next_pubdesc',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'next_search',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'archive_unit_sequence',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'archival_agency_id',
                    'archival_agency_identifier',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'access_rule_id',
                ]
            )
            ->addIndex(
                [
                    'agreement_id',
                ]
            )
            ->addIndex(
                [
                    'appraisal_rule_id',
                ]
            )
            ->addIndex(
                [
                    'archival_agency_id',
                ]
            )
            ->addIndex(
                [
                    'originating_agency_id',
                ]
            )
            ->addIndex(
                [
                    'profile_id',
                ]
            )
            ->addIndex(
                [
                    'secure_data_space_id',
                ]
            )
            ->addIndex(
                [
                    'service_level_id',
                ]
            )
            ->addIndex(
                [
                    'transferring_agency_id',
                ]
            )
            ->addIndex(
                [
                    'transferring_operator_id',
                ]
            )
            ->create();

        $this->table('archives_restitution_requests')
            ->addColumn(
                'archive_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'restitution_request_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'archive_id',
                ]
            )
            ->addIndex(
                [
                    'restitution_request_id',
                ]
            )
            ->create();

        $this->table('archives_transfers')
            ->addColumn(
                'archive_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'transfer_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'archive_id',
                ]
            )
            ->addIndex(
                [
                    'transfer_id',
                ]
            )
            ->create();

        $this->table('archiving_systems')
            ->addColumn(
                'org_entity_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'description',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'url',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'username',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'use_proxy',
                'boolean',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'modified',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'active',
                'boolean',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'password',
                'binary',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'ssl_verify_peer',
                'boolean',
                [
                    'default' => true,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'ssl_verify_peer_name',
                'boolean',
                [
                    'default' => true,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'ssl_verify_depth',
                'integer',
                [
                    'default' => '5',
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'ssl_verify_host',
                'boolean',
                [
                    'default' => true,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'ssl_cafile',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'chunk_size',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'org_entity_id',
                    'name',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'org_entity_id',
                ]
            )
            ->create();

        $this->table('aros')
            ->addColumn(
                'parent_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'model',
                'string',
                [
                    'default' => '',
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'foreign_key',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'alias',
                'string',
                [
                    'default' => '',
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'lft',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'rght',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'alias',
                ]
            )
            ->addIndex(
                [
                    'lft',
                    'rght',
                ]
            )
            ->create();

        $this->table('aros_acos')
            ->addColumn(
                'aro_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'aco_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                '_create',
                'string',
                [
                    'default' => '0',
                    'limit' => 2,
                    'null' => false,
                ]
            )
            ->addColumn(
                '_read',
                'string',
                [
                    'default' => '0',
                    'limit' => 2,
                    'null' => false,
                ]
            )
            ->addColumn(
                '_update',
                'string',
                [
                    'default' => '0',
                    'limit' => 2,
                    'null' => false,
                ]
            )
            ->addColumn(
                '_delete',
                'string',
                [
                    'default' => '0',
                    'limit' => 2,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'aco_id',
                ]
            )
            ->addIndex(
                [
                    'aro_id',
                ]
            )
            ->create();

        $this->table('auth_sub_urls')
            ->addColumn(
                'auth_url_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'url',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'auth_url_id',
                ]
            )
            ->create();

        $this->table('auth_urls')
            ->addColumn(
                'code',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'url',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'expire',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addIndex(
                [
                    'expire',
                ]
            )
            ->create();

        $this->table('batch_treatments')
            ->addColumn(
                'archival_agency_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'user_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'state',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'last_state_update',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'states_history',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'data',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'modified',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addIndex(
                [
                    'archival_agency_id',
                ]
            )
            ->addIndex(
                [
                    'user_id',
                ]
            )
            ->create();

        $this->table('beanstalk_jobs')
            ->addColumn(
                'tube',
                'string',
                [
                    'default' => 'default',
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'priority',
                'integer',
                [
                    'default' => '1024',
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'user_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'delay',
                'integer',
                [
                    'default' => '0',
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'ttr',
                'integer',
                [
                    'default' => '60',
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'errors',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'beanstalk_worker_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'object_model',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'object_foreign_key',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'job_state',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'last_state_update',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'states_history',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'data',
                'binary',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'beanstalk_worker_id',
                ]
            )
            ->addIndex(
                [
                    'user_id',
                ]
            )
            ->create();

        $this->table('beanstalk_workers')
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'tube',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'path',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'pid',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'last_launch',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'hostname',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->create();

        $this->table('change_entity_requests')
            ->addColumn(
                'base_archival_agency_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'target_archival_agency_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'user_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'reason',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addIndex(
                [
                    'base_archival_agency_id',
                ]
            )
            ->addIndex(
                [
                    'target_archival_agency_id',
                ]
            )
            ->addIndex(
                [
                    'user_id',
                ]
            )
            ->create();

        $this->table('composite_archive_units')
            ->addColumn(
                'transfer_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'archive_unit_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'transfer_xpath',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'archive_unit_id',
                ]
            )
            ->addIndex(
                [
                    'transfer_id',
                ]
            )
            ->create();

        $this->table('configurations')
            ->addColumn(
                'org_entity_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'setting',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'org_entity_id',
                ]
            )
            ->addIndex(
                [
                    'org_entity_id',
                    'name',
                ]
            )
            ->create();

        $this->table('conversion_policies')
            ->addColumn(
                'org_entity_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'media',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'usage',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'method',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'format',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'params',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'org_entity_id',
                    'media',
                    'usage',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'org_entity_id',
                ]
            )
            ->create();

        $this->table('counters')
            ->addColumn(
                'org_entity_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'identifier',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'description',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'type',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'definition_mask',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'sequence_reset_mask',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'sequence_reset_value',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'sequence_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'active',
                'boolean',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'modified',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addIndex(
                [
                    'org_entity_id',
                    'identifier',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'org_entity_id',
                ]
            )
            ->addIndex(
                [
                    'sequence_id',
                ]
            )
            ->create();

        $this->table('cron_executions')
            ->addColumn(
                'cron_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'date_begin',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'date_end',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'state',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'report',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'last_update',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'pid',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'cron_id',
                ]
            )
            ->create();

        $this->table('crons')
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'description',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'classname',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'active',
                'boolean',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'locked',
                'boolean',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'frequency',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'next',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'app_meta',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'parallelisable',
                'boolean',
                [
                    'default' => true,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'max_duration',
                'string',
                [
                    'default' => 'PT1H',
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'last_email',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->create();

        $this->table('deliveries')
            ->addColumn(
                'identifier',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'delivery_request_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'state',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'comment',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'archive_units_count',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'original_count',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'original_size',
                'biginteger',
                [
                    'default' => null,
                    'limit' => 20,
                    'null' => false,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'modified',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addIndex(
                [
                    'delivery_request_id',
                ]
            )
            ->create();

        $this->table('delivery_requests')
            ->addColumn(
                'identifier',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'archival_agency_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'requester_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'state',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'comment',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'derogation',
                'boolean',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'created_user_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'modified',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'last_state_update',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'states_history',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'filename',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'validation_chain_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'archive_units_count',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'original_count',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'original_size',
                'biginteger',
                [
                    'default' => null,
                    'limit' => 20,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'archival_agency_id',
                ]
            )
            ->addIndex(
                [
                    'created_user_id',
                ]
            )
            ->addIndex(
                [
                    'requester_id',
                ]
            )
            ->addIndex(
                [
                    'validation_chain_id',
                ]
            )
            ->create();

        $this->table('destruction_notification_xpaths')
            ->addColumn(
                'destruction_notification_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'archive_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'archive_unit_identifier',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'archive_unit_name',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'description_xpath',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'archive_id',
                ]
            )
            ->addIndex(
                [
                    'destruction_notification_id',
                ]
            )
            ->create();

        $this->table('destruction_notifications')
            ->addColumn(
                'identifier',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'destruction_request_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'comment',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addIndex(
                [
                    'destruction_request_id',
                ]
            )
            ->create();

        $this->table('destruction_requests')
            ->addColumn(
                'identifier',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'comment',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'archival_agency_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'originating_agency_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'control_authority_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'state',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'last_state_update',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'states_history',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'archive_units_count',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'original_count',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'original_size',
                'biginteger',
                [
                    'default' => null,
                    'limit' => 20,
                    'null' => false,
                ]
            )
            ->addColumn(
                'created_user_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'modified',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'validation_chain_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'certified',
                'boolean',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'archival_agency_id',
                ]
            )
            ->addIndex(
                [
                    'control_authority_id',
                ]
            )
            ->addIndex(
                [
                    'created_user_id',
                ]
            )
            ->addIndex(
                [
                    'originating_agency_id',
                ]
            )
            ->addIndex(
                [
                    'validation_chain_id',
                ]
            )
            ->create();

        $this->table('eaccpfs')
            ->addColumn(
                'record_id',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'entity_id',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'entity_type',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'data',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'modified',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'agency_name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'org_entity_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'from_date',
                'date',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'to_date',
                'date',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'org_entity_id',
                ]
            )
            ->create();

        $this->table('event_logs')
            ->addColumn(
                'type',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'outcome',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'outcome_detail',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'user_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'cron_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'object_model',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'object_foreign_key',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'exported',
                'boolean',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'object_serialized',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'agent_serialized',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'in_lifecycle',
                'boolean',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'archival_agency_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'archival_agency_id',
                ]
            )
            ->addIndex(
                [
                    'cron_id',
                ]
            )
            ->addIndex(
                [
                    'user_id',
                ]
            )
            ->addIndex(
                [
                    'type',
                ]
            )
            ->addIndex(
                [
                    'created',
                ]
            )
            ->addIndex(
                [
                    'user_id',
                    'cron_id',
                ]
            )
            ->create();

        $this->table('file_extensions')
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'name',
                ],
                ['unique' => true]
            )
            ->create();

        $this->table('file_extensions_pronoms')
            ->addColumn(
                'file_extension_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'pronom_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'file_extension_id',
                ]
            )
            ->addIndex(
                [
                    'pronom_id',
                ]
            )
            ->create();

        $this->table('fileuploads')
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'path',
                'string',
                [
                    'default' => null,
                    'limit' => 2048,
                    'null' => true,
                ]
            )
            ->addColumn(
                'size',
                'biginteger',
                [
                    'default' => null,
                    'limit' => 20,
                    'null' => true,
                ]
            )
            ->addColumn(
                'hash',
                'string',
                [
                    'default' => null,
                    'limit' => 4000,
                    'null' => true,
                ]
            )
            ->addColumn(
                'hash_algo',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'modified',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'user_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'valid',
                'boolean',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'state',
                'string',
                [
                    'default' => null,
                    'limit' => 50,
                    'null' => true,
                ]
            )
            ->addColumn(
                'mime',
                'string',
                [
                    'default' => null,
                    'limit' => 128,
                    'null' => true,
                ]
            )
            ->addColumn(
                'locked',
                'boolean',
                [
                    'default' => false,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'user_id',
                ]
            )
            ->create();

        $this->table('fileuploads_profiles')
            ->addColumn(
                'fileupload_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'profile_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'fileupload_id',
                ]
            )
            ->addIndex(
                [
                    'profile_id',
                ]
            )
            ->create();

        $this->table('filters')
            ->addColumn(
                'saved_filter_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'key',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'value',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addIndex(
                [
                    'saved_filter_id',
                ]
            )
            ->create();

        $this->table('keyword_lists')
            ->addColumn(
                'org_entity_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'identifier',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'description',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'active',
                'boolean',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'modified',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'version',
                'integer',
                [
                    'default' => '0',
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'org_entity_id',
                    'identifier',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'org_entity_id',
                ]
            )
            ->create();

        $this->table('keywords')
            ->addColumn(
                'keyword_list_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'version',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'code',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'app_meta',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'parent_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'lft',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'rght',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'exact_match',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'change_note',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'modified',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addIndex(
                [
                    'keyword_list_id',
                ]
            )
            ->addIndex(
                [
                    'parent_id',
                ]
            )
            ->create();

        $this->table('killables')
            ->addColumn(
                'pid',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'user_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'session_token',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'timeout',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'killed',
                'boolean',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addIndex(
                [
                    'pid',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'session_token',
                ]
            )
            ->addIndex(
                [
                    'user_id',
                ]
            )
            ->create();

        $this->table('ldaps')
            ->addColumn(
                'org_entity_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'host',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'port',
                'integer',
                [
                    'default' => '389',
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'user_query_login',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'user_query_password',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'ldap_root_search',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'user_login_attribute',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => false,
                ]
            )
            ->addColumn(
                'ldap_users_filter',
                'string',
                [
                    'default' => null,
                    'limit' => 65535,
                    'null' => true,
                ]
            )
            ->addColumn(
                'account_prefix',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'account_suffix',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'description',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'use_proxy',
                'boolean',
                [
                    'default' => true,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'use_ssl',
                'boolean',
                [
                    'default' => false,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'use_tls',
                'boolean',
                [
                    'default' => false,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'user_name_attribute',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'user_mail_attribute',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'modified',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'schema',
                'string',
                [
                    'default' => 'Adldap\\Schemas\\ActiveDirectory',
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'follow_referrals',
                'boolean',
                [
                    'default' => false,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'version',
                'integer',
                [
                    'default' => '3',
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'timeout',
                'integer',
                [
                    'default' => '5',
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'custom_options',
                'text',
                [
                    'default' => '[]',
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'user_username_attribute',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'name',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'org_entity_id',
                ]
            )
            ->create();

        $this->table('mails')
            ->addColumn(
                'serialized',
                'binary',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->create();

        $this->table('mediainfo_audios')
            ->addColumn(
                'mediainfo_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'format',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'format_info',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'mode_extension',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'format_settings_endianness',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'codec_id',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'duration',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'bit_rate_mode',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'bit_rate',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'channel_s_',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'channel_positions',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'sampling_rate',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'frame_rate',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'bit_depth',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'compression_mode',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'delay_relative_to_video',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'stream_size',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'title',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'language',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                '_default',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'forced',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'mediainfo_id',
                ]
            )
            ->create();

        $this->table('mediainfo_images')
            ->addColumn(
                'mediainfo_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'format',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'format_info',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'width',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'height',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'bit_depth',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'compression_mode',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'stream_size',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'mediainfo_id',
                ]
            )
            ->create();

        $this->table('mediainfo_texts')
            ->addColumn(
                'mediainfo_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'format',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'codec_id',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'codec_id_info',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'duration',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'bit_rate',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'count_of_elements',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'stream_size',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'title',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'language',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                '_default',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'forced',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'mediainfo_id',
                ]
            )
            ->create();

        $this->table('mediainfo_videos')
            ->addColumn(
                'mediainfo_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'format',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'format_info',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'format_profile',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'format_settings_cabac',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'format_settings_reframes',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'codec_id',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'duration',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'bit_rate',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'width',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'height',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'display_aspect_ratio',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'frame_rate_mode',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'frame_rate',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'color_space',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'chroma_subsampling',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'bit_depth',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'scan_type',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'bits_pixel_frame_',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'stream_size',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'title',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'writing_library',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'encoding_settings',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'language',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                '_default',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'forced',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'color_range',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'color_primaries',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'transfer_characteristics',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'matrix_coefficients',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'mediainfo_id',
                ]
            )
            ->create();

        $this->table('mediainfos')
            ->addColumn(
                'fileupload_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'unique_id',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'complete_name',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'format',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'format_version',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'file_size',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'duration',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'overall_bit_rate',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'movie_name',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'encoded_date',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'writing_application',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'writing_library',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'fileupload_id',
                ]
            )
            ->create();

        $this->table('message_indicators')
            ->addColumn(
                'message_date',
                'date',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'message_type',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'message_count',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'accepted',
                'boolean',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'derogation',
                'boolean',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'validation_duration',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'archival_agency_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'transferring_agency_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'originating_agency_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'original_files_count',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'original_files_size',
                'biginteger',
                [
                    'default' => null,
                    'limit' => 20,
                    'null' => false,
                ]
            )
            ->addColumn(
                'agreement_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'profile_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'agreement_id',
                ]
            )
            ->addIndex(
                [
                    'archival_agency_id',
                ]
            )
            ->addIndex(
                [
                    'originating_agency_id',
                ]
            )
            ->addIndex(
                [
                    'profile_id',
                ]
            )
            ->addIndex(
                [
                    'transferring_agency_id',
                ]
            )
            ->create();

        $this->table('notifications')
            ->addColumn(
                'user_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'text',
                'string',
                [
                    'default' => null,
                    'limit' => 2048,
                    'null' => false,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'css_class',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'user_id',
                ]
            )
            ->create();

        $this->table('oaipmh_archives')
            ->addColumn(
                'oaipmh_token_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'archive_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'datetime',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'deleted',
                'boolean',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'archive_id',
                ]
            )
            ->addIndex(
                [
                    'oaipmh_token_id',
                ]
            )
            ->create();

        $this->table('oaipmh_listsets')
            ->addColumn(
                'oaipmh_token_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'spec',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => false,
                ]
            )
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'oaipmh_token_id',
                ]
            )
            ->create();

        $this->table('oaipmh_tokens')
            ->addColumn(
                'value',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'verb',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'metadata_prefix',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'expiration_date',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'complete_list_size',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'cursor',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'prev_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'version',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'prev_id',
                ]
            )
            ->addIndex(
                [
                    'value',
                    'verb',
                ]
            )
            ->addIndex(
                [
                    'expiration_date',
                ]
            )
            ->create();

        $this->table('org_entities')
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'identifier',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'parent_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'lft',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'rght',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'modified',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'type_entity_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'timestamper_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'active',
                'boolean',
                [
                    'default' => true,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'is_main_archival_agency',
                'boolean',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'description',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'short_name',
                'string',
                [
                    'default' => null,
                    'limit' => 40,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'parent_id',
                ]
            )
            ->addIndex(
                [
                    'timestamper_id',
                ]
            )
            ->addIndex(
                [
                    'type_entity_id',
                ]
            )
            ->addIndex(
                [
                    'id',
                    'name',
                ]
            )
            ->addIndex(
                [
                    'lft',
                    'rght',
                ]
            )
            ->create();

        $this->table('org_entities_timestampers')
            ->addColumn(
                'org_entity_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'timestamper_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'org_entity_id',
                ]
            )
            ->addIndex(
                [
                    'timestamper_id',
                ]
            )
            ->create();

        $this->table('outgoing_transfer_requests')
            ->addColumn(
                'identifier',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'comment',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'archival_agency_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'archiving_system_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'archival_agency_identifier',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'archival_agency_name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'transferring_agency_identifier',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'transferring_agency_name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'agreement_identifier',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'profile_identifier',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'state',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'states_history',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'archives_count',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'archive_units_count',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'original_count',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'original_size',
                'biginteger',
                [
                    'default' => null,
                    'limit' => 20,
                    'null' => false,
                ]
            )
            ->addColumn(
                'created_user_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'modified',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'last_state_update',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'originating_agency_identifier',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'originating_agency_name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'service_level_identifier',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'archival_agency_id',
                ]
            )
            ->addIndex(
                [
                    'archiving_system_id',
                ]
            )
            ->addIndex(
                [
                    'created_user_id',
                ]
            )
            ->create();

        $this->table('outgoing_transfers')
            ->addColumn(
                'identifier',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'comment',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'outgoing_transfer_request_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'archive_unit_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'state',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'last_state_update',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'states_history',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'modified',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'error_msg',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'archive_unit_id',
                ]
            )
            ->addIndex(
                [
                    'outgoing_transfer_request_id',
                ]
            )
            ->create();

        $this->table('profiles')
            ->addColumn(
                'org_entity_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'identifier',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'description',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'active',
                'boolean',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'modified',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addIndex(
                [
                    'org_entity_id',
                    'identifier',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'org_entity_id',
                ]
            )
            ->create();

        $this->table('pronoms')
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'puid',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'mime',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'puid',
                ],
                ['unique' => true]
            )
            ->create();

        $this->table('restitution_requests')
            ->addColumn(
                'identifier',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'comment',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'archival_agency_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'originating_agency_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'state',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'last_state_update',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'states_history',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'archive_units_count',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'original_count',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'original_size',
                'biginteger',
                [
                    'default' => null,
                    'limit' => 20,
                    'null' => false,
                ]
            )
            ->addColumn(
                'created_user_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'modified',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'restitution_certified',
                'boolean',
                [
                    'default' => false,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'destruction_certified',
                'boolean',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'archival_agency_id',
                ]
            )
            ->addIndex(
                [
                    'created_user_id',
                ]
            )
            ->addIndex(
                [
                    'originating_agency_id',
                ]
            )
            ->create();

        $this->table('restitutions')
            ->addColumn(
                'identifier',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'restitution_request_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'state',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'comment',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'archive_units_count',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'original_count',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'original_size',
                'biginteger',
                [
                    'default' => null,
                    'limit' => 20,
                    'null' => false,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'modified',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addIndex(
                [
                    'restitution_request_id',
                ]
            )
            ->create();

        $this->table('roles')
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'active',
                'boolean',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'modified',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'org_entity_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'code',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'parent_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'lft',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'rght',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'hierarchical_view',
                'boolean',
                [
                    'comment' => 'autorise ce rôle à voir les éléments de ses sous-entités',
                    'default' => false,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'description',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'agent_type',
                'string',
                [
                    'default' => 'person',
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'name',
                    'org_entity_id',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'code',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'org_entity_id',
                ]
            )
            ->addIndex(
                [
                    'parent_id',
                ]
            )
            ->create();

        $this->table('roles_type_entities')
            ->addColumn(
                'role_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'type_entity_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'role_id',
                ]
            )
            ->addIndex(
                [
                    'type_entity_id',
                ]
            )
            ->create();

        $this->table('saved_filters')
            ->addColumn(
                'user_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'controller',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'action',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addIndex(
                [
                    'user_id',
                ]
            )
            ->create();

        $this->table('secure_data_spaces')
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'description',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'modified',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'org_entity_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'is_default',
                'boolean',
                [
                    'default' => false,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'name',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'org_entity_id',
                ]
            )
            ->create();

        $this->table('sequences')
            ->addColumn(
                'org_entity_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'description',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'value',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'modified',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addIndex(
                [
                    'org_entity_id',
                    'name',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'org_entity_id',
                ]
            )
            ->create();

        $this->table('service_levels')
            ->addColumn(
                'org_entity_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'identifier',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'description',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'active',
                'boolean',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'modified',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'ts_msg_id',
                'integer',
                [
                    'comment' => 'reference org_entities_timestampers',
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'ts_pjs_id',
                'integer',
                [
                    'comment' => 'reference org_entities_timestampers',
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'ts_conv_id',
                'integer',
                [
                    'comment' => 'reference org_entities_timestampers',
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'default_level',
                'boolean',
                [
                    'default' => false,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'secure_data_space_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'convert_preservation',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'convert_dissemination',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'commitment',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'org_entity_id',
                    'identifier',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'org_entity_id',
                ]
            )
            ->addIndex(
                [
                    'secure_data_space_id',
                ]
            )
            ->addIndex(
                [
                    'ts_conv_id',
                ]
            )
            ->addIndex(
                [
                    'ts_msg_id',
                ]
            )
            ->addIndex(
                [
                    'ts_pjs_id',
                ]
            )
            ->create();

        $this->table('sessions', ['id' => false, 'primary_key' => ['id']])
            ->addColumn(
                'id',
                'string',
                [
                    'default' => null,
                    'limit' => 40,
                    'null' => false,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'modified',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'data',
                'binary',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'expires',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'user_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'token',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'token',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'user_id',
                ]
            )
            ->create();

        $this->table('siegfrieds')
            ->addColumn(
                'fileupload_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'pronom',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'format',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'version',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'mime',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'basis',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'warning',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'fileupload_id',
                ]
            )
            ->create();

        $this->table('stored_files')
            ->addColumn(
                'secure_data_space_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 2048,
                    'null' => false,
                ]
            )
            ->addColumn(
                'size',
                'biginteger',
                [
                    'default' => null,
                    'limit' => 20,
                    'null' => false,
                ]
            )
            ->addColumn(
                'hash',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'hash_algo',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addIndex(
                [
                    'secure_data_space_id',
                    'name',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'secure_data_space_id',
                ]
            )
            ->create();

        $this->table('stored_files_volumes')
            ->addColumn(
                'storage_ref',
                'string',
                [
                    'default' => null,
                    'limit' => 2048,
                    'null' => false,
                ]
            )
            ->addColumn(
                'volume_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'stored_file_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'is_integrity_ok',
                'boolean',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'integrity_date',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addIndex(
                [
                    'stored_file_id',
                ]
            )
            ->addIndex(
                [
                    'volume_id',
                ]
            )
            ->create();

        $this->table('super_archivists_archival_agencies')
            ->addColumn(
                'user_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'archival_agency_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'archival_agency_id',
                ]
            )
            ->addIndex(
                [
                    'user_id',
                ]
            )
            ->create();

        $this->table('technical_archive_units')
            ->addColumn(
                'state',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'technical_archive_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'parent_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'lft',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'rght',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'archival_agency_identifier',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'name',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'description',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'history',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'oldest_date',
                'date',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'latest_date',
                'date',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'original_local_size',
                'biginteger',
                [
                    'default' => null,
                    'limit' => 20,
                    'null' => false,
                ]
            )
            ->addColumn(
                'original_local_count',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'original_total_size',
                'biginteger',
                [
                    'default' => null,
                    'limit' => 20,
                    'null' => false,
                ]
            )
            ->addColumn(
                'original_total_count',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'xml_node_tagname',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'parent_id',
                ]
            )
            ->addIndex(
                [
                    'technical_archive_id',
                ]
            )
            ->create();

        $this->table('technical_archives')
            ->addColumn(
                'state',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'type',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'is_integrity_ok',
                'boolean',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'archival_agency_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'archival_agency_identifier',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'secure_data_space_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'storage_path',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'name',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'original_size',
                'biginteger',
                [
                    'default' => null,
                    'limit' => 20,
                    'null' => false,
                ]
            )
            ->addColumn(
                'original_count',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'timestamp_size',
                'biginteger',
                [
                    'default' => null,
                    'limit' => 20,
                    'null' => false,
                ]
            )
            ->addColumn(
                'timestamp_count',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'management_size',
                'biginteger',
                [
                    'default' => null,
                    'limit' => 20,
                    'null' => false,
                ]
            )
            ->addColumn(
                'management_count',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'last_state_update',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'integrity_date',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'filesexist_date',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addIndex(
                [
                    'archival_agency_id',
                ]
            )
            ->addIndex(
                [
                    'secure_data_space_id',
                ]
            )
            ->addIndex(
                [
                    'type',
                ]
            )
            ->addIndex(
                [
                    'created',
                ]
            )
            ->create();

        $this->table('timestampers')
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'fields',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'modified',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addIndex(
                [
                    'name',
                ],
                ['unique' => true]
            )
            ->create();

        $this->table('transfer_attachments')
            ->addColumn(
                'transfer_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'filename',
                'string',
                [
                    'default' => null,
                    'limit' => 2048,
                    'null' => false,
                ]
            )
            ->addColumn(
                'size',
                'biginteger',
                [
                    'default' => null,
                    'limit' => 20,
                    'null' => false,
                ]
            )
            ->addColumn(
                'hash',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'hash_algo',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'virus_name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'valid',
                'boolean',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'mime',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'extension',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'format',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'xpath',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'transfer_id',
                    'filename',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'transfer_id',
                ]
            )
            ->create();

        $this->table('transfer_errors')
            ->addColumn(
                'transfer_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'level',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'code',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'message',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'transfer_id',
                ]
            )
            ->create();

        $this->table('transfers')
            ->addColumn(
                'transfer_comment',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'transfer_date',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'transfer_identifier',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'archival_agency_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'transferring_agency_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'message_version',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'state',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'state_history',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'last_state_update',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'is_conform',
                'boolean',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'is_modified',
                'boolean',
                [
                    'default' => false,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'is_accepted',
                'boolean',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'agreement_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'profile_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'filename',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => false,
                ]
            )
            ->addColumn(
                'data_size',
                'biginteger',
                [
                    'default' => null,
                    'limit' => 20,
                    'null' => true,
                ]
            )
            ->addColumn(
                'created_user_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'modified',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'service_level_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'files_deleted',
                'boolean',
                [
                    'default' => false,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'data_count',
                'integer',
                [
                    'default' => '0',
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'hidden',
                'boolean',
                [
                    'default' => false,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'originating_agency_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'archive_file_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'agreement_id',
                ]
            )
            ->addIndex(
                [
                    'archival_agency_id',
                ]
            )
            ->addIndex(
                [
                    'archive_file_id',
                ]
            )
            ->addIndex(
                [
                    'created_user_id',
                ]
            )
            ->addIndex(
                [
                    'originating_agency_id',
                ]
            )
            ->addIndex(
                [
                    'profile_id',
                ]
            )
            ->addIndex(
                [
                    'service_level_id',
                ]
            )
            ->addIndex(
                [
                    'transferring_agency_id',
                ]
            )
            ->addIndex(
                [
                    'state',
                    'is_conform',
                    'is_modified',
                    'is_accepted',
                ]
            )
            ->create();

        $this->table('type_entities')
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'active',
                'boolean',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'modified',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'code',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'code',
                ],
                ['unique' => true]
            )
            ->create();

        $this->table('users')
            ->addColumn(
                'username',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'modified',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'password',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'cookies',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'menu',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'high_contrast',
                'boolean',
                [
                    'default' => false,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'active',
                'boolean',
                [
                    'default' => true,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'role_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'org_entity_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'email',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'ldap_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'agent_type',
                'string',
                [
                    'default' => 'person',
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'is_validator',
                'boolean',
                [
                    'default' => true,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'use_cert',
                'boolean',
                [
                    'default' => false,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'ldap_login',
                'string',
                [
                    'default' => null,
                    'limit' => 512,
                    'null' => true,
                ]
            )
            ->addColumn(
                'notify_download_pdf_frequency',
                'integer',
                [
                    'default' => '1',
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'notify_download_zip_frequency',
                'integer',
                [
                    'default' => '1',
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'notify_validation_frequency',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'notify_job_frequency',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'notify_agreement_frequency',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'notify_transfer_report_frequency',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'username',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'ldap_id',
                ]
            )
            ->addIndex(
                [
                    'org_entity_id',
                ]
            )
            ->addIndex(
                [
                    'role_id',
                ]
            )
            ->create();

        $this->table('validation_actors')
            ->addColumn(
                'validation_stage_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'app_type',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'app_key',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'app_meta',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'modified',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'app_foreign_key',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'validation_stage_id',
                    'app_type',
                    'app_foreign_key',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'validation_stage_id',
                ]
            )
            ->create();

        $this->table('validation_chains')
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'description',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'app_type',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'app_meta',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'modified',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'org_entity_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'name',
                    'org_entity_id',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'org_entity_id',
                ]
            )
            ->create();

        $this->table('validation_histories')
            ->addColumn(
                'validation_process_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'validation_actor_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'action',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'comment',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'app_meta',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'current_step',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'validation_actor_id',
                ]
            )
            ->addIndex(
                [
                    'validation_process_id',
                ]
            )
            ->create();

        $this->table('validation_processes')
            ->addColumn(
                'validation_chain_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'current_stage_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'app_subject_type',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'app_subject_key',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'processed',
                'boolean',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'validated',
                'boolean',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'app_meta',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'modified',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'app_foreign_key',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'current_step',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addIndex(
                [
                    'current_stage_id',
                ]
            )
            ->addIndex(
                [
                    'validation_chain_id',
                ]
            )
            ->create();

        $this->table('validation_stages')
            ->addColumn(
                'validation_chain_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'description',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'all_actors_to_complete',
                'boolean',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'ord',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => false,
                ]
            )
            ->addColumn(
                'app_type',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'app_meta',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'modified',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addIndex(
                [
                    'validation_chain_id',
                    'name',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'validation_chain_id',
                ]
            )
            ->create();

        $this->table('versions')
            ->addColumn(
                'subject',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'version',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addIndex(
                [
                    'subject',
                    'version',
                ],
                ['unique' => true]
            )
            ->create();

        $this->table('volumes')
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'fields',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'modified',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'driver',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'description',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'active',
                'boolean',
                [
                    'default' => true,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'alert_rate',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'disk_usage',
                'biginteger',
                [
                    'default' => null,
                    'limit' => 20,
                    'null' => true,
                ]
            )
            ->addColumn(
                'max_disk_usage',
                'biginteger',
                [
                    'default' => null,
                    'limit' => 20,
                    'null' => true,
                ]
            )
            ->addColumn(
                'secure_data_space_id',
                'integer',
                [
                    'default' => null,
                    'limit' => 10,
                    'null' => true,
                ]
            )
            ->addColumn(
                'read_duration',
                'float',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addColumn(
                'location',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addIndex(
                [
                    'name',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'secure_data_space_id',
                ]
            )
            ->create();

        $this->table('webservices')
            ->addColumn(
                'name',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ]
            )
            ->addColumn(
                'description',
                'text',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'username',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'password',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'active',
                'boolean',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->addColumn(
                'created',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'modified',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                    'precision' => 6,
                    'scale' => 6,
                ]
            )
            ->addColumn(
                'homepage',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'callback',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'client_id',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->addColumn(
                'client_secret',
                'string',
                [
                    'default' => null,
                    'limit' => 255,
                    'null' => true,
                ]
            )
            ->create();
    }

    /**
     * Down Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-down-method
     * @return void
     */
    public function down()
    {
        $this->table('access_rule_codes')
            ->dropForeignKey(
                'org_entity_id'
            )->save();

        $this->table('access_rules')
            ->dropForeignKey(
                'access_rule_code_id'
            )->save();

        $this->table('access_tokens')
            ->dropForeignKey(
                'user_id'
            )->save();

        $this->table('acos')
            ->dropForeignKey(
                'parent_id'
            )->save();

        $this->table('agreements')
            ->dropForeignKey(
                'improper_chain_id'
            )
            ->dropForeignKey(
                'org_entity_id'
            )
            ->dropForeignKey(
                'proper_chain_id'
            )->save();

        $this->table('agreements_originating_agencies')
            ->dropForeignKey(
                'agreement_id'
            )
            ->dropForeignKey(
                'org_entity_id'
            )->save();

        $this->table('agreements_profiles')
            ->dropForeignKey(
                'agreement_id'
            )
            ->dropForeignKey(
                'profile_id'
            )->save();

        $this->table('agreements_service_levels')
            ->dropForeignKey(
                'agreement_id'
            )
            ->dropForeignKey(
                'service_level_id'
            )->save();

        $this->table('agreements_transferring_agencies')
            ->dropForeignKey(
                'agreement_id'
            )
            ->dropForeignKey(
                'org_entity_id'
            )->save();

        $this->table('appraisal_rule_codes')
            ->dropForeignKey(
                'org_entity_id'
            )->save();

        $this->table('appraisal_rules')
            ->dropForeignKey(
                'appraisal_rule_code_id'
            )->save();

        $this->table('archive_binaries')
            ->dropForeignKey(
                'original_data_id'
            )
            ->dropForeignKey(
                'stored_file_id'
            )->save();

        $this->table('archive_binaries_archive_units')
            ->dropForeignKey(
                'archive_binary_id'
            )
            ->dropForeignKey(
                'archive_unit_id'
            )->save();

        $this->table('archive_binaries_technical_archive_units')
            ->dropForeignKey(
                'archive_binary_id'
            )
            ->dropForeignKey(
                'technical_archive_unit_id'
            )->save();

        $this->table('archive_binary_conversions')
            ->dropForeignKey(
                'archive_binary_id'
            )
            ->dropForeignKey(
                'archive_id'
            )->save();

        $this->table('archive_descriptions')
            ->dropForeignKey(
                'access_rule_id'
            )
            ->dropForeignKey(
                'archive_unit_id'
            )->save();

        $this->table('archive_files')
            ->dropForeignKey(
                'archive_id'
            )
            ->dropForeignKey(
                'stored_file_id'
            )->save();

        $this->table('archive_indicators')
            ->dropForeignKey(
                'agreement_id'
            )
            ->dropForeignKey(
                'archival_agency_id'
            )
            ->dropForeignKey(
                'originating_agency_id'
            )
            ->dropForeignKey(
                'profile_id'
            )
            ->dropForeignKey(
                'transferring_agency_id'
            )->save();

        $this->table('archive_keywords')
            ->dropForeignKey(
                'access_rule_id'
            )
            ->dropForeignKey(
                'archive_unit_id'
            )->save();

        $this->table('archive_units')
            ->dropForeignKey(
                'access_rule_id'
            )
            ->dropForeignKey(
                'appraisal_rule_id'
            )
            ->dropForeignKey(
                'archive_id'
            )
            ->dropForeignKey(
                'parent_id'
            )->save();

        $this->table('archive_units_batch_treatments')
            ->dropForeignKey(
                'archive_unit_id'
            )
            ->dropForeignKey(
                'batch_treatment_id'
            )->save();

        $this->table('archive_units_delivery_requests')
            ->dropForeignKey(
                'archive_unit_id'
            )
            ->dropForeignKey(
                'delivery_request_id'
            )->save();

        $this->table('archive_units_destruction_requests')
            ->dropForeignKey(
                'archive_unit_id'
            )
            ->dropForeignKey(
                'destruction_request_id'
            )->save();

        $this->table('archive_units_outgoing_transfer_requests')
            ->dropForeignKey(
                'archive_unit_id'
            )
            ->dropForeignKey(
                'otr_id'
            )->save();

        $this->table('archive_units_restitution_requests')
            ->dropForeignKey(
                'archive_unit_id'
            )
            ->dropForeignKey(
                'restitution_request_id'
            )->save();

        $this->table('archives')
            ->dropForeignKey(
                'access_rule_id'
            )
            ->dropForeignKey(
                'agreement_id'
            )
            ->dropForeignKey(
                'appraisal_rule_id'
            )
            ->dropForeignKey(
                'archival_agency_id'
            )
            ->dropForeignKey(
                'originating_agency_id'
            )
            ->dropForeignKey(
                'profile_id'
            )
            ->dropForeignKey(
                'secure_data_space_id'
            )
            ->dropForeignKey(
                'service_level_id'
            )
            ->dropForeignKey(
                'transferring_agency_id'
            )
            ->dropForeignKey(
                'transferring_operator_id'
            )->save();

        $this->table('archives_restitution_requests')
            ->dropForeignKey(
                'archive_id'
            )
            ->dropForeignKey(
                'restitution_request_id'
            )->save();

        $this->table('archives_transfers')
            ->dropForeignKey(
                'archive_id'
            )
            ->dropForeignKey(
                'transfer_id'
            )->save();

        $this->table('archiving_systems')
            ->dropForeignKey(
                'org_entity_id'
            )->save();

        $this->table('aros_acos')
            ->dropForeignKey(
                'aco_id'
            )
            ->dropForeignKey(
                'aro_id'
            )->save();

        $this->table('auth_sub_urls')
            ->dropForeignKey(
                'auth_url_id'
            )->save();

        $this->table('batch_treatments')
            ->dropForeignKey(
                'archival_agency_id'
            )
            ->dropForeignKey(
                'user_id'
            )->save();

        $this->table('beanstalk_jobs')
            ->dropForeignKey(
                'beanstalk_worker_id'
            )
            ->dropForeignKey(
                'user_id'
            )->save();

        $this->table('change_entity_requests')
            ->dropForeignKey(
                'base_archival_agency_id'
            )
            ->dropForeignKey(
                'target_archival_agency_id'
            )
            ->dropForeignKey(
                'user_id'
            )->save();

        $this->table('composite_archive_units')
            ->dropForeignKey(
                'archive_unit_id'
            )
            ->dropForeignKey(
                'transfer_id'
            )->save();

        $this->table('configurations')
            ->dropForeignKey(
                'org_entity_id'
            )->save();

        $this->table('conversion_policies')
            ->dropForeignKey(
                'org_entity_id'
            )->save();

        $this->table('counters')
            ->dropForeignKey(
                'org_entity_id'
            )
            ->dropForeignKey(
                'sequence_id'
            )->save();

        $this->table('cron_executions')
            ->dropForeignKey(
                'cron_id'
            )->save();

        $this->table('deliveries')
            ->dropForeignKey(
                'delivery_request_id'
            )->save();

        $this->table('delivery_requests')
            ->dropForeignKey(
                'archival_agency_id'
            )
            ->dropForeignKey(
                'created_user_id'
            )
            ->dropForeignKey(
                'requester_id'
            )
            ->dropForeignKey(
                'validation_chain_id'
            )->save();

        $this->table('destruction_notification_xpaths')
            ->dropForeignKey(
                'archive_id'
            )
            ->dropForeignKey(
                'destruction_notification_id'
            )->save();

        $this->table('destruction_notifications')
            ->dropForeignKey(
                'destruction_request_id'
            )->save();

        $this->table('destruction_requests')
            ->dropForeignKey(
                'archival_agency_id'
            )
            ->dropForeignKey(
                'control_authority_id'
            )
            ->dropForeignKey(
                'created_user_id'
            )
            ->dropForeignKey(
                'originating_agency_id'
            )
            ->dropForeignKey(
                'validation_chain_id'
            )->save();

        $this->table('eaccpfs')
            ->dropForeignKey(
                'org_entity_id'
            )->save();

        $this->table('event_logs')
            ->dropForeignKey(
                'archival_agency_id'
            )
            ->dropForeignKey(
                'cron_id'
            )
            ->dropForeignKey(
                'user_id'
            )->save();

        $this->table('file_extensions_pronoms')
            ->dropForeignKey(
                'file_extension_id'
            )
            ->dropForeignKey(
                'pronom_id'
            )->save();

        $this->table('fileuploads')
            ->dropForeignKey(
                'user_id'
            )->save();

        $this->table('fileuploads_profiles')
            ->dropForeignKey(
                'fileupload_id'
            )
            ->dropForeignKey(
                'profile_id'
            )->save();

        $this->table('filters')
            ->dropForeignKey(
                'saved_filter_id'
            )->save();

        $this->table('keyword_lists')
            ->dropForeignKey(
                'org_entity_id'
            )->save();

        $this->table('keywords')
            ->dropForeignKey(
                'keyword_list_id'
            )
            ->dropForeignKey(
                'parent_id'
            )->save();

        $this->table('killables')
            ->dropForeignKey(
                'session_token'
            )
            ->dropForeignKey(
                'user_id'
            )->save();

        $this->table('ldaps')
            ->dropForeignKey(
                'org_entity_id'
            )->save();

        $this->table('mediainfo_audios')
            ->dropForeignKey(
                'mediainfo_id'
            )->save();

        $this->table('mediainfo_images')
            ->dropForeignKey(
                'mediainfo_id'
            )->save();

        $this->table('mediainfo_texts')
            ->dropForeignKey(
                'mediainfo_id'
            )->save();

        $this->table('mediainfo_videos')
            ->dropForeignKey(
                'mediainfo_id'
            )->save();

        $this->table('mediainfos')
            ->dropForeignKey(
                'fileupload_id'
            )->save();

        $this->table('message_indicators')
            ->dropForeignKey(
                'agreement_id'
            )
            ->dropForeignKey(
                'archival_agency_id'
            )
            ->dropForeignKey(
                'originating_agency_id'
            )
            ->dropForeignKey(
                'profile_id'
            )
            ->dropForeignKey(
                'transferring_agency_id'
            )->save();

        $this->table('notifications')
            ->dropForeignKey(
                'user_id'
            )->save();

        $this->table('oaipmh_archives')
            ->dropForeignKey(
                'archive_id'
            )
            ->dropForeignKey(
                'oaipmh_token_id'
            )->save();

        $this->table('oaipmh_listsets')
            ->dropForeignKey(
                'oaipmh_token_id'
            )->save();

        $this->table('oaipmh_tokens')
            ->dropForeignKey(
                'prev_id'
            )->save();

        $this->table('org_entities')
            ->dropForeignKey(
                'parent_id'
            )
            ->dropForeignKey(
                'timestamper_id'
            )
            ->dropForeignKey(
                'type_entity_id'
            )->save();

        $this->table('org_entities_timestampers')
            ->dropForeignKey(
                'org_entity_id'
            )
            ->dropForeignKey(
                'timestamper_id'
            )->save();

        $this->table('outgoing_transfer_requests')
            ->dropForeignKey(
                'archival_agency_id'
            )
            ->dropForeignKey(
                'archiving_system_id'
            )
            ->dropForeignKey(
                'created_user_id'
            )->save();

        $this->table('outgoing_transfers')
            ->dropForeignKey(
                'archive_unit_id'
            )
            ->dropForeignKey(
                'outgoing_transfer_request_id'
            )->save();

        $this->table('profiles')
            ->dropForeignKey(
                'org_entity_id'
            )->save();

        $this->table('restitution_requests')
            ->dropForeignKey(
                'archival_agency_id'
            )
            ->dropForeignKey(
                'created_user_id'
            )
            ->dropForeignKey(
                'originating_agency_id'
            )->save();

        $this->table('restitutions')
            ->dropForeignKey(
                'restitution_request_id'
            )->save();

        $this->table('roles')
            ->dropForeignKey(
                'org_entity_id'
            )
            ->dropForeignKey(
                'parent_id'
            )->save();

        $this->table('roles_type_entities')
            ->dropForeignKey(
                'role_id'
            )
            ->dropForeignKey(
                'type_entity_id'
            )->save();

        $this->table('saved_filters')
            ->dropForeignKey(
                'user_id'
            )->save();

        $this->table('secure_data_spaces')
            ->dropForeignKey(
                'org_entity_id'
            )->save();

        $this->table('sequences')
            ->dropForeignKey(
                'org_entity_id'
            )->save();

        $this->table('service_levels')
            ->dropForeignKey(
                'org_entity_id'
            )
            ->dropForeignKey(
                'secure_data_space_id'
            )
            ->dropForeignKey(
                'ts_conv_id'
            )
            ->dropForeignKey(
                'ts_msg_id'
            )
            ->dropForeignKey(
                'ts_pjs_id'
            )->save();

        $this->table('sessions')
            ->dropForeignKey(
                'user_id'
            )->save();

        $this->table('siegfrieds')
            ->dropForeignKey(
                'fileupload_id'
            )->save();

        $this->table('stored_files')
            ->dropForeignKey(
                'secure_data_space_id'
            )->save();

        $this->table('stored_files_volumes')
            ->dropForeignKey(
                'stored_file_id'
            )
            ->dropForeignKey(
                'volume_id'
            )->save();

        $this->table('super_archivists_archival_agencies')
            ->dropForeignKey(
                'archival_agency_id'
            )
            ->dropForeignKey(
                'user_id'
            )->save();

        $this->table('technical_archive_units')
            ->dropForeignKey(
                'parent_id'
            )
            ->dropForeignKey(
                'technical_archive_id'
            )->save();

        $this->table('technical_archives')
            ->dropForeignKey(
                'archival_agency_id'
            )
            ->dropForeignKey(
                'secure_data_space_id'
            )->save();

        $this->table('transfer_attachments')
            ->dropForeignKey(
                'transfer_id'
            )->save();

        $this->table('transfer_errors')
            ->dropForeignKey(
                'transfer_id'
            )->save();

        $this->table('transfers')
            ->dropForeignKey(
                'agreement_id'
            )
            ->dropForeignKey(
                'archival_agency_id'
            )
            ->dropForeignKey(
                'archive_file_id'
            )
            ->dropForeignKey(
                'created_user_id'
            )
            ->dropForeignKey(
                'originating_agency_id'
            )
            ->dropForeignKey(
                'profile_id'
            )
            ->dropForeignKey(
                'service_level_id'
            )
            ->dropForeignKey(
                'transferring_agency_id'
            )->save();

        $this->table('users')
            ->dropForeignKey(
                'ldap_id'
            )
            ->dropForeignKey(
                'org_entity_id'
            )
            ->dropForeignKey(
                'role_id'
            )->save();

        $this->table('validation_actors')
            ->dropForeignKey(
                'validation_stage_id'
            )->save();

        $this->table('validation_chains')
            ->dropForeignKey(
                'org_entity_id'
            )->save();

        $this->table('validation_histories')
            ->dropForeignKey(
                'validation_actor_id'
            )
            ->dropForeignKey(
                'validation_process_id'
            )->save();

        $this->table('validation_processes')
            ->dropForeignKey(
                'current_stage_id'
            )
            ->dropForeignKey(
                'validation_chain_id'
            )->save();

        $this->table('validation_stages')
            ->dropForeignKey(
                'validation_chain_id'
            )->save();

        $this->table('volumes')
            ->dropForeignKey(
                'secure_data_space_id'
            )->save();

        $this->table('access_rule_codes')->drop()->save();
        $this->table('access_rules')->drop()->save();
        $this->table('access_tokens')->drop()->save();
        $this->table('acos')->drop()->save();
        $this->table('agreements')->drop()->save();
        $this->table('agreements_originating_agencies')->drop()->save();
        $this->table('agreements_profiles')->drop()->save();
        $this->table('agreements_service_levels')->drop()->save();
        $this->table('agreements_transferring_agencies')->drop()->save();
        $this->table('appraisal_rule_codes')->drop()->save();
        $this->table('appraisal_rules')->drop()->save();
        $this->table('archive_binaries')->drop()->save();
        $this->table('archive_binaries_archive_units')->drop()->save();
        $this->table('archive_binaries_technical_archive_units')->drop()->save();
        $this->table('archive_binary_conversions')->drop()->save();
        $this->table('archive_descriptions')->drop()->save();
        $this->table('archive_files')->drop()->save();
        $this->table('archive_indicators')->drop()->save();
        $this->table('archive_keywords')->drop()->save();
        $this->table('archive_units')->drop()->save();
        $this->table('archive_units_batch_treatments')->drop()->save();
        $this->table('archive_units_delivery_requests')->drop()->save();
        $this->table('archive_units_destruction_requests')->drop()->save();
        $this->table('archive_units_outgoing_transfer_requests')->drop()->save();
        $this->table('archive_units_restitution_requests')->drop()->save();
        $this->table('archives')->drop()->save();
        $this->table('archives_restitution_requests')->drop()->save();
        $this->table('archives_transfers')->drop()->save();
        $this->table('archiving_systems')->drop()->save();
        $this->table('aros')->drop()->save();
        $this->table('aros_acos')->drop()->save();
        $this->table('auth_sub_urls')->drop()->save();
        $this->table('auth_urls')->drop()->save();
        $this->table('batch_treatments')->drop()->save();
        $this->table('beanstalk_jobs')->drop()->save();
        $this->table('beanstalk_workers')->drop()->save();
        $this->table('change_entity_requests')->drop()->save();
        $this->table('composite_archive_units')->drop()->save();
        $this->table('configurations')->drop()->save();
        $this->table('conversion_policies')->drop()->save();
        $this->table('counters')->drop()->save();
        $this->table('cron_executions')->drop()->save();
        $this->table('crons')->drop()->save();
        $this->table('deliveries')->drop()->save();
        $this->table('delivery_requests')->drop()->save();
        $this->table('destruction_notification_xpaths')->drop()->save();
        $this->table('destruction_notifications')->drop()->save();
        $this->table('destruction_requests')->drop()->save();
        $this->table('eaccpfs')->drop()->save();
        $this->table('event_logs')->drop()->save();
        $this->table('file_extensions')->drop()->save();
        $this->table('file_extensions_pronoms')->drop()->save();
        $this->table('fileuploads')->drop()->save();
        $this->table('fileuploads_profiles')->drop()->save();
        $this->table('filters')->drop()->save();
        $this->table('keyword_lists')->drop()->save();
        $this->table('keywords')->drop()->save();
        $this->table('killables')->drop()->save();
        $this->table('ldaps')->drop()->save();
        $this->table('mails')->drop()->save();
        $this->table('mediainfo_audios')->drop()->save();
        $this->table('mediainfo_images')->drop()->save();
        $this->table('mediainfo_texts')->drop()->save();
        $this->table('mediainfo_videos')->drop()->save();
        $this->table('mediainfos')->drop()->save();
        $this->table('message_indicators')->drop()->save();
        $this->table('notifications')->drop()->save();
        $this->table('oaipmh_archives')->drop()->save();
        $this->table('oaipmh_listsets')->drop()->save();
        $this->table('oaipmh_tokens')->drop()->save();
        $this->table('org_entities')->drop()->save();
        $this->table('org_entities_timestampers')->drop()->save();
        $this->table('outgoing_transfer_requests')->drop()->save();
        $this->table('outgoing_transfers')->drop()->save();
        $this->table('profiles')->drop()->save();
        $this->table('pronoms')->drop()->save();
        $this->table('restitution_requests')->drop()->save();
        $this->table('restitutions')->drop()->save();
        $this->table('roles')->drop()->save();
        $this->table('roles_type_entities')->drop()->save();
        $this->table('saved_filters')->drop()->save();
        $this->table('secure_data_spaces')->drop()->save();
        $this->table('sequences')->drop()->save();
        $this->table('service_levels')->drop()->save();
        $this->table('sessions')->drop()->save();
        $this->table('siegfrieds')->drop()->save();
        $this->table('stored_files')->drop()->save();
        $this->table('stored_files_volumes')->drop()->save();
        $this->table('super_archivists_archival_agencies')->drop()->save();
        $this->table('technical_archive_units')->drop()->save();
        $this->table('technical_archives')->drop()->save();
        $this->table('timestampers')->drop()->save();
        $this->table('transfer_attachments')->drop()->save();
        $this->table('transfer_errors')->drop()->save();
        $this->table('transfers')->drop()->save();
        $this->table('type_entities')->drop()->save();
        $this->table('users')->drop()->save();
        $this->table('validation_actors')->drop()->save();
        $this->table('validation_chains')->drop()->save();
        $this->table('validation_histories')->drop()->save();
        $this->table('validation_processes')->drop()->save();
        $this->table('validation_stages')->drop()->save();
        $this->table('versions')->drop()->save();
        $this->table('volumes')->drop()->save();
        $this->table('webservices')->drop()->save();
    }
}
