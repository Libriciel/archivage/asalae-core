#!/usr/bin/php -q
<?php
/**
 * Permet d'utiliser session_decode() dans n'importe quel contexte (ex testunit)
 * utilisé par SessionsTable pour récupérer les données de session
 */
session_start();

$currentSession = session_encode();
$_SESSION = [];
session_decode(
    base64_decode($argv[1])
);
$decodedSession = $_SESSION;
$_SESSION = [];
session_decode($currentSession);

if (empty($argv[2])) {
    $serialized = serialize($decodedSession);
    header('Content-Type', 'text/plain');
    print $serialized.PHP_EOL;
    exit;
}
elseif ($argv[2] === 'json') {
    $json = json_encode($decodedSession, JSON_PRETTY_PRINT);
    header('Content-Type', 'application/json');
    print $json.PHP_EOL;
    exit;
}
