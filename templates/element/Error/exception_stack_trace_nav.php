<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         3.0.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 * @var array $trace
 */
?>
<a href="#" class="toggle-link toggle-vendor-frames">Toggle Vendor Stack Frames</a>

<ul class="stack-trace">
    <?php foreach ($trace as $i => $stack) : ?>
        <?php
        $class = in_array($stack, $local_trace) ? 'app-frame' : 'vendor-frame';
        $class .= $i == 0 ? ' active' : '';
        ?>
        <li class="stack-frame <?= $class ?>">
            <span class="stack-file">
                <?=$stack?>
            </span>
        </li>
    <?php endforeach; ?>
</ul>
