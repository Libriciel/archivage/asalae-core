<?php
/**
 * @var AsalaeCore\View\AppView $this
 */

use Cake\Core\Configure;

$this->extend('/layout/html');
$this->assign(
    'logo',
    $this->Html->image(
        'asalae-color.svg',
        [
            'alt' => 'LOGO',
            'title' => Configure::read('App.name'),
            'style' => 'height: 45px; padding-top: 5px;'
        ]
    )
);
$this->start('main-header');
?>
    <header class="navbar-fixed-top">
        <nav class="navbar navbar-asalae">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <a class="navbar-brand logo" href="<?=$this->Url->build('/')?>" data-mode="no-ajax">
                        <?=$this->fetch('logo')?>
                    </a>
                </div>

            </div><!-- /.container-fluid -->
        </nav>
    </header>
<?php
$this->end();

$this->start('main-content');
?>
    <!-- Contenu de la page -->

    <div class="clearfix main-content">
        <div id="zone-debug"></div>
        <?= $this->Flash->render() ?>

        <?= $this->fetch('content') ?>
    </div>
<?php
$this->end();
