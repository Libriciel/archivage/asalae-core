<?php
/**
 * @var AsalaeCore\View\AppView $this
 */
use Cake\Core\Configure;
?>
    <section class="container bg-white">
        <h1><i aria-hidden="true" class="fa fa-warning"></i> <?=$this->fetch('error-h1') ?: $this->fetch('title')?></h1>
    </section>
    <section class="container bg-white">
        <header>
            <h2 class="h4"><?=$this->fetch('error-h2') ?: $this->fetch('templateName')?></h2>
        </header>
        <p><?=$this->fetch('error-p')?><?=$this->fetch('subheading')?></p>
        <div><?=$this->fetch('file')?></div>
        <?=$this->element('AsalaeCore.Error/report-table')?>
    </section>

<?php if (Configure::read('debug')): ?>

    <?=$this->element('AsalaeCore.Error/section-error')?>
    <?=$this->element('AsalaeCore.Error/stack-trace')?>
<?php endif;