<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 * @var AsalaeCore\View\AppView $this
 */

use Cake\I18n\I18n;
use Cake\Core\Configure;

$logoUrl = Configure::read(
    'AsalaeCore.Template.Layout.Email.html.default.logoUrl',
    'https://ressources.libriciel.fr/public/asalae/img/asalae-color-white.png'
);
$logoAlt = Configure::read('AsalaeCore.Template.Layout.Email.html.default.logoAlt', 'asalae');
$headerColor = Configure::read('AsalaeCore.Template.Layout.Email.html.default.headerColor', '#3e4c5b');
/** @var string $htmlSignature */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html lang="<?=str_replace('_', '-', I18n::getLocale())?>">
    <head>
        <title><?=$title ?? $this->fetch('title')?></title>
    </head>
    <body style="padding:0; margin:0; background:#f1f1f1;">
        <table width="100%" cellspacing="0" cellpadding="0" border="0">
            <tbody>
                <tr>
                    <td>
                        <table width="100%" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                            <tr>
                                <td width="20" valign="top" align="center" bgcolor="<?=$headerColor?>">&nbsp;</td>
                                <td height="50" bgcolor="<?=$headerColor?>" width="660">
                                    <img alt="<?=$logoAlt?>" style="height: 40px;" src="<?=$logoUrl?>" border="0">
                                </td>
                                <td width="20" valign="top" align="center" bgcolor="<?=$headerColor?>">&nbsp;</td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <table width="600" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                                <tr>
                                    <td style="padding-top:10px;" bgcolor="#f1f1f1"></td>
                                </tr>
                                <?php if (!empty($title)): ?>
                                    <tr>
                                        <td style="padding-top:10px; padding-bottom: 10px; border-bottom: 1px solid #d9d9d9; font-family: arial, sans-serif; color: #4c575f; font-size: 32px; font-weight: 500" bgcolor="#f1f1f1">
                                            <?=$title?>
                                        </td>
                                    </tr>
                                <?php endif; ?>
                                <tr>
                                    <td style="padding-top:20px;" bgcolor="#f1f1f1"></td>
                                </tr>
                                <tr>
                                    <td valign="top" bgcolor="#ffffff" align="center" style="color: #4c575f; box-shadow: 0 0 1px rgb(210, 210, 210);">
                                        <table style="padding-bottom:10px; padding-top:10px;margin-bottom: 20px;" width="100%" cellspacing="0" cellpadding="0" border="0">
                                            <tbody>
                                                <tr valign="bottom">
                                                    <td width="20" valign="top" align="center">&nbsp;</td>
                                                    <td style="font-family:Calibri, Trebuchet, Arial, sans serif; font-size:15px; line-height:22px; color:#333333;" valign="top">
                                                        <?= $this->fetch('content') ?>
                                                    </td>
                                                    <td width="20" valign="top" align="center">&nbsp;</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center">

<?php if (!empty($htmlSignature)): ?>

                        <table width="600" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                                <tr>
                                    <td bgcolor="#f1f1f1"><?=$htmlSignature?></td>
                                </tr>
                            </tbody>
                        </table>

<?php else: ?>

                        <table width="600" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                                <tr>
                                    <td style="color: #4c575f; font-family: arial, sans-serif; padding-bottom: 5px; border-bottom: 1px solid #d9d9d9;" bgcolor="#f1f1f1">
                                        <?=__("Merci de ne pas répondre à cet email.")?>
                                    </td>
                                </tr>
                                <tr><td height="20" style="font-size:0;" bgcolor="#f1f1f1">&nbsp;</td></tr>
                                <tr>
                                    <td bgcolor="#f1f1f1">
                                        <a href="https://www.libriciel.fr" target="_blank">
                                            <img alt="Logo Libriciel SCOP" src="https://www.libriciel.fr/wp-content/uploads/2023/01/ls-signature-w-40px.png" />
                                        </a>
                                    </td>
                                </tr>
                                <tr><td height="10" style="font-size:0;" bgcolor="#f1f1f1">&nbsp;</td></tr>
                                <tr>
                                    <td style="color: #4c575f; font-family: arial, sans-serif;" bgcolor="#f1f1f1">
                                        <span style="font-size: 8pt; font-family: arial, helvetica, sans-serif; color: #242a54;"><?=__("Portail client")?> :
                                            <a style="color: #42a1e8;" href="https://libriciel.fr/espace-client/" target="_blank">se connecter</a>
                                        </span>
                                        <br />
                                        <span style="font-size: 8pt; font-family: arial, helvetica, sans-serif; color: #242a54;">140, rue Aglaonice de Thessalie, 34170 CASTELNAU-LE-LEZ</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="color: #4c575f; font-family: arial, sans-serif;" bgcolor="#f1f1f1">
                                        <div><span style="font-size: 8pt;">&nbsp;</span></div>
                                        <div>
                                            <span style="color: #42a1e8; font-size: 14pt;"><strong>|</strong></span>
                                            <span style="font-size: 8pt; font-family: arial, helvetica, sans-serif; color: #242a54;">Pour en savoir plus sur l'ensemble de nos solutions, rendez-vous sur&nbsp;<a style="color: #42a1e8;" href="https://www.libriciel.fr" target="_blank">www.libriciel.fr</a></span>
                                        </div>
                                        <div><span style="font-size: 8pt;">&nbsp;</span></div>
                                        <div><a href="https://twitter.com/Libriciel_SCOP"><img src="https://www.libriciel.fr/wp-content/uploads/2022/11/logo-twitter.png" alt="Twitter" width="16" height="16" /></a>&nbsp;&nbsp;<a href="https://fr.linkedin.com/company/libriciel-scop"><img style="margin-left: 3px;" src="https://www.libriciel.fr/wp-content/uploads/2022/11/logo-linkedin.png" alt="LinkedIn" width="16" height="16" /></a>&nbsp;&nbsp;<a href="https://www.youtube.com/user/adullactprojet"><img style="margin-left: 3px;" src="https://www.libriciel.fr/wp-content/uploads/2022/11/logo-youtube.png" alt="Youtube" width="16" height="16" /></a>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

<?php endif; ?>

                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <table width="600" cellspacing="0" cellpadding="0" border="0">
                            <tbody><?=$this->fetch('after-signature')?></tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </body>
</html>
