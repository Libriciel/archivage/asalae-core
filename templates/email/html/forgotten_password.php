<?php
/**
 * @var AsalaeCore\View\AppView $this
 */

use Cake\Core\Configure;

?>
<div style="color: #4c575f">
    <h4 style="margin: 0; padding-bottom: 5px; border-bottom: 1px solid #e5e5e5"><?=__("Changement du mot de passe")?></h4>

    <?php if (!empty($user->get('ldap_id'))) { ?>
        <p><?=__("Bonjour")?>,<br>
        <?=__(
                "Votre compte utilisateur sur {0} est lié à un annuaire d'entreprise (LDAP, AD) de votre collectivité.
                Merci de prendre contact avec l'administrateur technique de votre collectivité.",
                Configure::read('App.name', __("l'application"))
        )?><br>
        <?=__("Bien cordialement.")?></p>

    <?php } else { ?>
    <p>
        <?=__(
                "Vous avez demandé le changement du mot de passe pour votre compte {0}",
                Configure::read('App.name', __("l'application"))
        )?>
    </p>

    <ul>
        <li><b><?=__("Identifiant")?>:</b> <?=$user->get(Configure::read('Libriciel.login.input.username', 'username'))?></li>
        <li><b><?=__("Email")?>:</b> <?=$user->get('email')?></li>
    </ul>
    <p><?=__(
        "Merci de {0} pour changer votre mot de passe.",
            $this->Html->link(__("cliquer sur ce lien"),
            trim(Configure::read('App.fullBaseUrl'), '/ ').'/auth-urls/activate/'.$code)
    )?></p>

    <p>
        <strong><?=__("REMARQUE")?>:</strong>
        <?=__("Ne tenez pas compte de ce mail si vous n'êtes pas à l'origine de cette demande.")?>
    </p>
    <?php } ?>

    <p><?=$this->Html->link(__("Accéder à la plateforme"), Configure::read('App.fullBaseUrl'))?></p>
</div>
