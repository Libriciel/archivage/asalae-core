<?php
/**
 * @var AsalaeCore\View\AppView $this
 */

use AsalaeCore\Model\Table\CronExecutionsTable;

$report = $exec->get('report');
$report = str_replace('<span class="console success">', '<span style="color:green">', $report);
$report = str_replace('<span class="console error">', '<span style="color:red">', $report);
$report = str_replace('<span class="console warning">', '<span style="color:orange">', $report);
$state = $exec->get('state');
switch ($state) {
    case CronExecutionsTable::S_SUCCESS:
        $state = '<span style="color:green;font-weight:bold">'.$state.'</span>';
        break;
    case CronExecutionsTable::S_ERROR:
        $state = '<span style="color:red;font-weight:bold">'.$state.'</span>';
        break;
    case CronExecutionsTable::S_WARNING:
        $state = '<span style="color:orange;font-weight:bold">'.$state.'</span>';
        break;
}
?>
<div style="color: #4c575f">
    <h4 style="margin: 0; padding-bottom: 5px; border-bottom: 1px solid #e5e5e5"><?=__("Rapport de tâche planifiée")?></h4>
    <p><?=__("La tâche planifiée ''{0}'' s'est terminée avec le statut {1}", '<b>'.$cron->get('name').'</b>', $state)?></p>
    <hr>
    <p><?=implode('<br>', $exec->get('header'))?></p>
    <hr>

    <p><?=nl2br($report)?></p>
</div>
