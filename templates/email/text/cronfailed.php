<?=__("Rapport de tâche planifiée")?>

<?=__("La tâche planifiée ''{0}'' s'est terminée avec le statut {1}", $cron->get('name'), $exec->get('state'))?>

--------------------------------------------------------------------------------
<?=implode(PHP_EOL, $exec->get('header'))?>
--------------------------------------------------------------------------------

<?php
$report = preg_replace('/<span class="[^"]+">/', '', $exec->get('report'));
$report = str_replace('</span>', '', $report);
echo $report;
