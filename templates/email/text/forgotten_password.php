<?php
/**
 * @var AsalaeCore\View\AppView $this
 */

use Cake\Core\Configure;

?>
<?=__("Changement du mot de passe")?>

<?php if (!empty($user->get('ldap_id'))) { ?>
<?=__("Bonjour")?>,
<?=__(
        "Votre compte utilisateur sur {0} est lié à un annuaire d'entreprise (LDAP, AD) de votre collectivité.
            Merci de prendre contact avec l'administrateur technique de votre collectivité.",
        Configure::read('App.name', __("l'application"))
    )?>

<?=__("Bien cordialement.")?>
<?php } else { ?>
<?=__("Vous avez demandé le changement du mot de passe pour votre compte {0}", Configure::read('App.name'))?>

- <?=__("Identifiant")?>: <?=$user->get('name')?>
- <?=__("Email")?>: <?=$user->get('email')?>


<?=__("Merci de cliquer sur le lien ci-dessous pour changer votre mot de passe.")?>
<?=trim(Configure::read('App.fullBaseUrl'), '/ ').'/auth-urls/activate/'.$code?>

====================
===== <?=__("REMARQUE")?>: ======
<?=__("Ne tenez pas compte de ce mail si vous n'êtes pas à l'origine de cette demande.")?>
<?php } ?>

<?=Configure::read('App.fullBaseUrl')?>
