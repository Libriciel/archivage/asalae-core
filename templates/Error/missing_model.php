<?php
$pluginDot = empty($plugin) ? null : $plugin . '.';


$this->assign('title', 'Missing Model');
$this->assign('templateName', 'missing_model.php');

$this->start('subheading');
?>
<strong>Error: </strong>
Model class <em><?= h($pluginDot . $class) ?></em> could not be found.
<?php if (isset($message)):  ?>
    <?= h($message); ?>
<?php endif; ?>
<?php $this->end() ?>
