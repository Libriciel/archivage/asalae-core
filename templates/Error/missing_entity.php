<?php
/** @noinspection PhpUndefinedNamespaceInspection PhpUndefinedConstantInspection PhpUnusedAliasInspection */
use Cake\Core\Plugin;
use Cake\Core\Configure;

$namespace = Configure::read('App.namespace');
if (!empty($plugin)) {
    $namespace = str_replace('/', '\\', $plugin);
}

$pluginPath = Configure::read('App.paths.plugins.0');
$pluginDot = empty($plugin) ? null : $plugin . '.';
if (empty($plugin)) {
    $filePath = APP_DIR . DIRECTORY_SEPARATOR;
}
if (!empty($plugin) && Plugin::loaded($plugin)) {
    $filePath = Plugin::classPath($plugin);
}
if (!empty($plugin) && !Plugin::loaded($plugin)) {
    $filePath = $pluginPath . h($plugin) . DIRECTORY_SEPARATOR . 'src' . DIRECTORY_SEPARATOR;
}

$this->assign('title', 'Missing Entity');
$this->assign('templateName', 'missing_entity.php');

$this->start('subheading');
?>
<strong>Error: </strong>
<em><?= h($pluginDot . $class) ?></em> could not be found.
<?= $this->element('plugin_class_error', ['pluginPath' => $pluginPath]) ?>
<?php $this->end() ?>

<?php $this->start('file') ?>
<p class="error">
    <strong>Error: </strong>
    <?= sprintf('Create the class <em>%s</em> below in file: %s', h($class), $filePath . 'Model' . DIRECTORY_SEPARATOR . 'Entity' . DIRECTORY_SEPARATOR . h($class) . '.php'); ?>
</p>
<?php
$code = <<<PHP
<?php
namespace {$namespace}\Model\Entity;

use Cake\ORM\Entity;

class {$class} extends Entity
{

}
PHP;
?>
<div class="code-dump"><?php highlight_string($code) ?></div>
<?php $this->end() ?>
