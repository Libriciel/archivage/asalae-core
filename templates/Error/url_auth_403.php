<?php
/**
 * @var AsalaeCore\View\AppView $this
 */

echo $this->Html->tag('div', null, ['class' => 'container']);
echo $this->Html->tag(
    'div',
    __(
        "La page demandée n'est pas/plus valide. La durée d'accès "
        ."peut être dépassée ou le traitement déjà effectué."
    ),
    ['class' => 'alert alert-info']
);
echo $this->Html->tag('/div');
