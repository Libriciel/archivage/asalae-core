<?php

/**
 * @var AsalaeCore\View\AppView $this
 * @var AsalaeCore\Auth\Identifier\UnknownUserException $error
 */
$userMessage = $error->username ? ' ' . __("pour l'utilisateur {0}", h($error->username)) : '';
?>
<section class="container bg-white">
    <header>
        <h2 class="h4"><?=__("Accès refusé")?><?=$userMessage?></h2>
    </header>
    <br>
    <div>
        <p class="alert alert-warning"><?=$message?></p>
    </div>
</section>
