<?php
$pluginDot = empty($plugin) ? null : $plugin . '.';


$this->assign('title', 'Missing Dispatcher');
$this->assign('templateName', 'missing_mailer.php');

$this->start('subheading');
?>
<strong>Error: </strong>
Mailer class <em><?= h($pluginDot . $class) ?></em> could not be found.
<?php if (isset($message)):  ?>
    <?= h($message); ?>
<?php endif; ?>
<?php $this->end() ?>
