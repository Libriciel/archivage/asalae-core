<?php
/**
 * @var AsalaeCore\View\AppView $this
 */

switch ($code) {

    case 401:
        $title = __("Unauthorized");
        $msg = __("Une authentification est nécessaire pour accéder à la ressource.");
        break;
    case 402:
        $title = __("Payment Required");
        $msg = __("Paiement requis pour accéder à la ressource.");
        break;
    case 403:
        $title = __("Forbidden");
        $msg = __("Le serveur a compris la requête, mais refuse de l'exécuter. Contrairement à l'erreur 401, s'authentifier ne fera aucune différence. Sur les serveurs où l'authentification est requise, cela signifie généralement que l'authentification a été acceptée mais que les droits d'accès ne permettent pas au client d'accéder à la ressource.");
        break;
    case 404:
        $title = __("Not Found");
        $msg = __("Ressource non trouvée.");
        break;
    case 405:
        $title = __("Method Not Allowed");
        $msg = __("Méthode de requête non autorisée.");
        break;
    case 406:
        $title = __("Not Acceptable");
        $msg = __("La ressource demandée n'est pas disponible dans un format qui respecterait les en-têtes \"Accept\" de la requête.");
        break;
    case 409:
        $title = __("Conflict");
        $msg = __("La requête ne peut être traitée en l’état actuel.");
        break;
    case 429:
        $title = __("Too Many Requests");
        $msg = __(" 	L'utilisateur a envoyé trop de requêtes en un temps donné..");
        break;
    case 499:
        $title = __("Client Closed Request");
        $msg = __(" 	Le client a fermé la connexion avant de recevoir la réponse.");
        break;
    case 400:
    default:
        $title = __("Bad Request");
        $msg = __("La syntaxe de la requête est erronée.");
        break;
}
$this->assign('error-h1', __("Impossible d'accéder à {0}.", "<strong>'{$url}'</strong>"));
$this->assign('error-h2', $code.' '.$title);
$this->assign('error-p', $msg);
