<?php
$pluginDot = empty($plugin) ? null : $plugin . '.';


$this->assign('title', 'Missing Driver');
$this->assign('templateName', 'missing_driver.php');

$this->start('subheading');
?>
<strong>Error: </strong>
Driver class <em><?= h($pluginDot . $class) ?></em> could not be found.
<?php if (isset($message)):  ?>
    <?= h($message); ?>
<?php endif; ?>
<?php $this->end() ?>
