<?php
/**
 * @var AsalaeCore\View\AppView $this
 */
$this->assign('error-h1', __("Unexpected Error :("));
$this->assign('error-h2', __("{0} Internal Server Error", $code));
$this->assign('error-p',
    __("The server encountered an internal error or misconfiguration and was unable to complete your request.")."<br>"
    .__("Please contact the server administrator, and inform them of the time the error occured, and anything you might have done that may have caused the error.")."<br>"
    .__("More information about this error may be available in the server error log.")
);
