# asalae-core

[![License](https://img.shields.io/badge/license-AGPL-blue)](https://www.gnu.org/licenses/agpl-3.0.txt)
[![build status](https://gitlab.libriciel.fr/libriciel/pole-archivage/asalae/asalae-core/badges/php83/pipeline.svg)](https://gitlab.libriciel.fr/libriciel/pole-archivage/asalae/asalae-core/pipelines/latest)
[![coverage report](https://gitlab.libriciel.fr/libriciel/pole-archivage/asalae/asalae-core/badges/php83/coverage.svg?min_medium=50&min_acceptable=70&min_good=80)](https://asalae-master.dev.libriciel.net/coverage/asalae-core/index.html)

## Dépendence de

| Projet                                                                        | Build                                                                                                                                                                                                 | Coverage                                                                                                                                                                                                                             |
|-------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| [asalae2](https://gitlab.libriciel.fr/libriciel/pole-archivage/asalae/asalae) | [![build status](https://gitlab.libriciel.fr/libriciel/pole-archivage/asalae/asalae/badges/master/pipeline.svg)](https://gitlab.libriciel.fr/libriciel/pole-archivage/asalae/asalae/pipelines/latest) | [![coverage report](https://gitlab.libriciel.fr/libriciel/pole-archivage/asalae/asalae/badges/master/coverage.svg?min_medium=50&min_acceptable=70&min_good=80)](https://asalae-master.dev.libriciel.net/coverage/asalae2/index.html) |
| [versae](https://gitlab.libriciel.fr/libriciel/pole-archivage/versae/versae)  | [![build status](https://gitlab.libriciel.fr/libriciel/pole-archivage/versae/versae/badges/master/pipeline.svg)](https://gitlab.libriciel.fr/libriciel/pole-archivage/versae/versae/pipelines/latest) | [![coverage report](https://gitlab.libriciel.fr/libriciel/pole-archivage/versae/versae/badges/master/coverage.svg?min_medium=50&min_acceptable=70&min_good=80)](https://versae-master.dev.libriciel.net/coverage/versae/index.html)  |
| [refae](https://gitlab.libriciel.fr/libriciel/pole-archivage/refae/refae)     | [![build status](https://gitlab.libriciel.fr/libriciel/pole-archivage/refae/refae/badges/master/pipeline.svg)](https://gitlab.libriciel.fr/libriciel/pole-archivage/refae/refae/pipelines/latest)     | [![coverage report](https://gitlab.libriciel.fr/libriciel/pole-archivage/refae/refae/badges/master/coverage.svg?min_medium=50&min_acceptable=70&min_good=80)](https://refae-master.dev.libriciel.net/coverage/refae/index.html)      |


## Description

Afin d'être utilisé dans d'autres projets, le code d'asalae est copié ici.
Ça permet d'avoir une version bien plus compacte, de ne récupérer que l'essentiel.

## Installation

```bash
composer require libriciel/asalae-core
```
